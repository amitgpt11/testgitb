<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>2016 Release Q2 - DRAFT US-2465 (Rally ID: US2274)

The function of this object is to display the associated referring Urologist and the number of Rxs for a written by a Primary Care Physician within a certain timeframe. This a junction object with M:M relationship to Contact object.

Updated the name of the object due to Jira story # 1003 - LAAC.  Q4 2016 Qtr Release - JM</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AMS_Migrated__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Checked for AMS migrated records.</description>
        <externalId>false</externalId>
        <label>AMS Migrated</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Current_Referring_Physician__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Used to track who is referring to urologist.</description>
        <externalId>false</externalId>
        <label>Current Referring Physician</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>LAAC_ABLATION_Patient_Count__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>ABLATION Patient Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_AFIB_Patient_Count__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>AFIB Patient Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Coumadin_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Coumadin NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Coumadin_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Coumadin TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Decile_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Decile NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Decile_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Decile TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Eliquis_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Eliquis NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Eliquis_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Eliquis TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_IMS_ID__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>IMS ID</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Jantoven_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Jantoven NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Jantoven_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Jantoven TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_ME_Number__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>ME Number</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_NOAC_Total_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>NOAC Total NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_NOAC_Total_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>NOAC Total TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_NPI_Number__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>true</externalId>
        <label>NPI Number</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Pradaxa_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Pradaxa NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Pradaxa_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Pradaxa TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Savaysa_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Savaysa NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Savaysa_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Savaysa TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_TAVR_Patient_Count__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>TAVR Patient Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Total_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Total NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Total_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Total TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Warafin_Total_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Warafin Total NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Warafin_Total_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Warafin Total TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Warfarin_Sodium_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Warfarin Sodium NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Warfarin_Sodium_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Warfarin Sodium TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Xarelto_NRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Xarelto NRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LAAC_Xarelto_TRx__c</fullName>
        <description>Created as part of Jira Story # 1003 - LAAC.  Q4 2016 Qtr Release - JM  Used during the import of IMS data.</description>
        <externalId>false</externalId>
        <label>Xarelto TRx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prescription_Timeframe__c</fullName>
        <description>Used to track the range of dates the RX is prescribed.</description>
        <externalId>false</externalId>
        <label>Prescription Timeframe</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Primary_Care_Physician_Name__c</fullName>
        <description>Used to track who the Contact is.</description>
        <externalId>false</externalId>
        <label>Physician Name</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Physician Rxs</relationshipLabel>
        <relationshipName>AMS_Primary_Care_Physician_Rxs</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Total_of_Rx__c</fullName>
        <description>This is a total of all prescription for MC control.</description>
        <externalId>false</externalId>
        <formula>of_Avodart_Rx__c  +  of_Cardura_Rx__c  +  of_Cardura_XL_Rx__c  +  of_Flomax_Rx__c  +   of_Hytrin_Rx__c  +  of_Proscar_Rx__c  +  of_Uroxatral_Rx__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total # of Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Urologist_Target__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Used to track who the PCP is referring.</description>
        <externalId>false</externalId>
        <label>Urologist Target</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>AMS_Primary_Care_Physician_Rxs1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>of_Avodart_Rx__c</fullName>
        <description>Used to track the number of Avodart prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Avodart Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Cardura_Rx__c</fullName>
        <description>Used to track the number of Cardura prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Cardura Rx</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Cardura_XL_Rx__c</fullName>
        <description>Used to track the number of Cardura XL prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Cardura XL Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Flomax_Rx__c</fullName>
        <description>Used to track the number of Flomax prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Flomax Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Hytrin_Rx__c</fullName>
        <description>Used to track the number of Hytrin prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Hytrin Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Proscar_Rx__c</fullName>
        <description>Used to track the number of Proscar prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Proscar Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Uroxatral_Rx__c</fullName>
        <description>Used to track the number of Uroxatral prescriptions written by the PCP.</description>
        <externalId>false</externalId>
        <label># of Uroxatral Rx</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Physician Rx</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CREATED_DATE</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>RECORDTYPE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Watchman_Rx_Data</fullName>
        <columns>NAME</columns>
        <columns>LAAC_NPI_Number__c</columns>
        <columns>Primary_Care_Physician_Name__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>AMS_Primary_Care_Physician_Rx__c.LAAC_IMS_Rx_Data</value>
        </filters>
        <label>Watchman Rx Data</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Physician Rx ID#</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Physician Rxs</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>LAAC_IMS_Rx_Data</fullName>
        <active>true</active>
        <description>Watchman IMS Physician Rx Data</description>
        <label>LAAC IMS Rx Data</label>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>

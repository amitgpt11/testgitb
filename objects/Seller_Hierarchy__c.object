<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Contains the Territory information and hierarchy.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ASP_Implant__c</fullName>
        <description>Field to set the ASP Implant value</description>
        <externalId>false</externalId>
        <label>ASP Implant</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ASP_Trial__c</fullName>
        <description>Field to set the ASP Trial value on Seller Hierarchy</description>
        <externalId>false</externalId>
        <label>ASP Trial</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Area_Name__c</fullName>
        <externalId>false</externalId>
        <label>Area Name</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BU__c</fullName>
        <externalId>false</externalId>
        <label>BU</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Count_of_TM1__c</fullName>
        <description>Calculates the Count of Assignees where Rep Designation=TM1</description>
        <externalId>false</externalId>
        <label>Count of TM1</label>
        <summaryFilterItems>
            <field>Assignee__c.Rep_Designation__c</field>
            <operation>equals</operation>
            <value>&quot;TM1&quot;</value>
        </summaryFilterItems>
        <summaryForeignKey>Assignee__c.Territory__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <externalId>false</externalId>
        <label>Country</label>
        <length>2</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Current_TM1_Assignee__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Current TM1 Assignee</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Seller_Hierarchies</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Current_TM2_Assignee__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Current TM2 Assignee</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Seller_Hierarchies1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Division__c</fullName>
        <externalId>false</externalId>
        <label>Division</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field_Support_Rep__c</fullName>
        <description>New text field created to view and assign the Field Support Rep to all seller hierarchy so that the value can be maintained in the SAP integration for physician-territory integration for NMD - US2705</description>
        <externalId>false</externalId>
        <label>Field Support Rep</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Region_Name__c</fullName>
        <externalId>false</externalId>
        <label>Region Name</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rep_Code__c</fullName>
        <externalId>false</externalId>
        <label>Rep Code</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reports_to__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Reports to</label>
        <referenceTo>Seller_Hierarchy__c</referenceTo>
        <relationshipName>BSC_Seller_Hierarchies</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Sales_Group__c</fullName>
        <externalId>false</externalId>
        <label>Sales Group</label>
        <length>3</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Office__c</fullName>
        <externalId>false</externalId>
        <label>Sales Office</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Org__c</fullName>
        <externalId>false</externalId>
        <label>Sales Org</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Territory_Description__c</fullName>
        <externalId>false</externalId>
        <label>Territory Description</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Territory_Name__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Territory Name</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Seller Hierarchy</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Shared_Territories</fullName>
        <columns>Region_Name__c</columns>
        <columns>NAME</columns>
        <columns>Territory_Name__c</columns>
        <columns>Count_of_TM1__c</columns>
        <columns>Current_TM1_Assignee__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Count_of_TM1__c</field>
            <operation>greaterThan</operation>
            <value>1</value>
        </filters>
        <label>Shared Territories</label>
        <sharedTo>
            <role>Neuromod_US</role>
        </sharedTo>
    </listViews>
    <nameField>
        <label>Seller Hierarchy Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Seller Hierarchies</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Territory_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Area_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>BU__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Division__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Region_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Sales_Group__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Sales_Office__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Sales_Org__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Country__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Territory_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Region_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Area_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Division__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BU__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Sales_Office__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Sales_Group__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Sales_Org__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Territory_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Region_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Area_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Division__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BU__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Sales_Office__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Sales_Group__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Sales_Org__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Territory_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Region_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Area_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Division__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>BU__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Sales_Group__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Sales_Office__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Sales_Org__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>

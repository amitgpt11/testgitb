<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>MobileRep</compactLayoutAssignment>
    <compactLayouts>
        <fullName>MobileRep</fullName>
        <fields>USEP_Invoice_Amount__c</fields>
        <fields>USEP_Invoice_Date__c</fields>
        <fields>USEP_Family_Code__c</fields>
        <label>MobileRep</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Created to support USEP Division as part of Q4 2016 Release (SFDC-1007). Read-only object to store sales history records from JDE.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Q3-2016 Release. Added to support US EP Integration need.</description>
        <externalId>true</externalId>
        <label>ExternalID</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Shared_Sales_Rep_ID__c</fullName>
        <description>Q3-2016 Release. Added to initial support US EP integration.</description>
        <externalId>false</externalId>
        <label>Sales Rep ID</label>
        <length>8</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Account_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>SAP Account Name</description>
        <externalId>false</externalId>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Sales Histories</relationshipLabel>
        <relationshipName>USEP_Sales_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>USEP_Account_Number__c</fullName>
        <description>SAP SOLD TO NUM</description>
        <externalId>true</externalId>
        <label>Account Number</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Account_Type_Code__c</fullName>
        <description>ACCOUNT TYPE CODE field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Account Type Code</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Account_Type__c</fullName>
        <description>ACCOUNT TYPE CODE</description>
        <externalId>false</externalId>
        <label>Account Type</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Category_Code__c</fullName>
        <description>Category Code field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Category Code</label>
        <length>45</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Category_Name__c</fullName>
        <description>Category Name field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Category Name</label>
        <length>45</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Customer_PO__c</fullName>
        <description>Customer PO (PO NUM) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Customer PO</label>
        <length>25</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Date_Extracted__c</fullName>
        <description>Date Extracted field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Date Extracted</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>USEP_Date_Shipped__c</fullName>
        <description>Date Shipped field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Date Shipped</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>USEP_District_Name__c</fullName>
        <description>District Name(SALES REGION) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Region Name</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_District__c</fullName>
        <description>District # (SALES REGION NUM) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Region #</label>
        <length>8</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Family_Code__c</fullName>
        <description>Family Code field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Family Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Family_Name__c</fullName>
        <description>Family Name field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Family Name</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Invoice_Amount__c</fullName>
        <description>Invoice Amount field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Invoice Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>USEP_Invoice_Date__c</fullName>
        <description>Invoice Date field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Invoice Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>USEP_Invoice_Number__c</fullName>
        <description>Invoice Number(INVOICE DOC NUM) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Invoice Number</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Invoice_YYYY_MM__c</fullName>
        <description>Invoice YYYY-MM field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Invoice YYYY-MM</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_JDE_Sales_Period__c</fullName>
        <description>JDE Sales(SAP SALES PERIOD) Period field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Sales Period</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_JDE_Sales_Year__c</fullName>
        <description>JDE Sales Year (SAP SALES YEAR) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Sales Year</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Legacy_Acct_Num__c</fullName>
        <description>Legacy Acct Num field added as a part of Q4 release (SFDC-1007)
JDE Account Number for Legacy Bard Customer</description>
        <externalId>true</externalId>
        <label>Legacy Acct Num</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Legacy_Product_Code__c</fullName>
        <description>Legacy Product Code field added as a part of Q4 release (SFDC-1007).
Read only field for Legacy Bard Product Code.</description>
        <externalId>false</externalId>
        <label>Legacy Product Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Order_Line_Type_Name__c</fullName>
        <description>Order Line Type Name field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Order Line Type Name</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Order_Line_Type__c</fullName>
        <description>Order Line Type field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Order Line Type</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Order_Line__c</fullName>
        <description>Order Line #(SALES LINE NUM) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Order Line #</label>
        <precision>9</precision>
        <required>false</required>
        <scale>3</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Order_Number__c</fullName>
        <description>Order Number field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Order Number</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Order_Type_Name__c</fullName>
        <description>Order Type Name field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Order Type Name</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Order_Type__c</fullName>
        <description>Order Type field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Order Type</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Product_Code__c</fullName>
        <description>Product Code(MATERIAL NUM) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Product Code</label>
        <length>25</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Product_Name__c</fullName>
        <description>Product Name(MATERIAL DESC) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Product Name</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Sales_U_M__c</fullName>
        <description>Sales U/M field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Sales U/M</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Ship_To_City__c</fullName>
        <description>Ship To City field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Ship To City</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Ship_To_Postal_Code__c</fullName>
        <description>Ship To Postal Code field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Ship To Postal Code</label>
        <length>12</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Ship_To_State__c</fullName>
        <description>Ship To State(REGION) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Ship To State</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Ship_To_Street__c</fullName>
        <description>Ship To Street(Address2) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Ship To Street</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Stock_U_M__c</fullName>
        <description>Stock U/M field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Stock U/M</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Strategic_Product__c</fullName>
        <description>Strategic Product field added as a part of Q4 release (SFDC-1007).</description>
        <externalId>false</externalId>
        <formula>CASE(USEP_Family_Code__c,
&apos;161&apos;,&apos;Yes&apos;,
&apos;164&apos;,&apos;Yes&apos;,
&apos;250&apos;,&apos;Yes&apos;,
&apos;337&apos;,&apos;Yes&apos;,
&apos;No&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Strategic Product?</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Territory_Name__c</fullName>
        <description>Territory Name (SALES TERRITORY) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Territory Name</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Territory__c</fullName>
        <description>Territory #(SALES TERR NUM) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Territory #</label>
        <length>8</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Type__c</fullName>
        <description>Type field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <formula>CASE(USEP_Family_Code__c, &quot;LAB SYSTEM CAPITAL&quot;, &quot;Capital&quot;, &quot;BSC CAPITAL&quot;, &quot;Capital&quot;,&quot;RHYTHMIA CAPITAL&quot;, &quot;Capital&quot;, &quot;Disposable&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_U_M_Conv_Factor__c</fullName>
        <description>U/M Conv Factor (U/M CONVERSION FACTOR) field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>U/M Conv Factor</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_Units_Shipped__c</fullName>
        <description>Units Shipped field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <label>Units Shipped</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>USEP_X3_Mo_Trend_End__c</fullName>
        <description>3 Mo Trend End field added as a part of Q4 release (SFDC-1007).</description>
        <externalId>false</externalId>
        <formula>DATE( 
(IF (Month(USEP_Invoice_Date__c)&lt;9,Year(USEP_Invoice_Date__c),Year(USEP_Invoice_Date__c)+1)), 
(CASE(MONTH(USEP_Invoice_Date__c), 9, 1, 10, 2, 11, 3, 12, 4, MONTH(USEP_Invoice_Date__c)+4)), 
1)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>3 Mo Trend End</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>USEP_X3_Mo_Trend_Start__c</fullName>
        <description>3 Mo Trend Start field added as a part of Q4 release (SFDC-1007).</description>
        <externalId>false</externalId>
        <formula>DATE( 
(IF (Month(USEP_Invoice_Date__c)=12,Year(USEP_Invoice_Date__c)+1,Year(USEP_Invoice_Date__c))), 
(IF(MONTH(USEP_Invoice_Date__c)=12, 1, MONTH(USEP_Invoice_Date__c)+1)), 
1)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>3 Mo Trend Start</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>USEP_YYYY_QQ__c</fullName>
        <description>YYYY-QQ field added as a part of Q4 release (SFDC-1007)</description>
        <externalId>false</externalId>
        <formula>CASE(  USEP_JDE_Sales_Period__c , 
      1,  USEP_JDE_Sales_Year__c &amp;&quot;-Q1&quot;, 
      2, USEP_JDE_Sales_Year__c &amp;&quot;-Q1&quot;,
      3, USEP_JDE_Sales_Year__c &amp;&quot;-Q1&quot;,
      4,  USEP_JDE_Sales_Year__c &amp;&quot;-Q2&quot;, 
      5, USEP_JDE_Sales_Year__c &amp;&quot;-Q2&quot;,
      6, USEP_JDE_Sales_Year__c &amp;&quot;-Q2&quot;,
      7,  USEP_JDE_Sales_Year__c &amp;&quot;-Q3&quot;, 
      8, USEP_JDE_Sales_Year__c &amp;&quot;-Q3&quot;,
      9, USEP_JDE_Sales_Year__c &amp;&quot;-Q3&quot;,
      USEP_JDE_Sales_Year__c &amp;&quot;-Q4&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>YYYY-QQ</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Material_Hierarchy_Level_1__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Material Hierarchy Level 1</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Material_Hierarchy_Level_2__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Material Hierarchy Level 2</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Material_Hierarchy_Level_3__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Material Hierarchy Level 3</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Material_Hierarchy_Level_4__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Material Hierarchy Level 4</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Material_Hierarchy_Level_5__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Material Hierarchy Level 5</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Order_Reason_Code__c</fullName>
        <description>Q3-2016 Release. Added to support US EP Integration</description>
        <externalId>false</externalId>
        <label>Order Reason Code</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Reason_Code_Description__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Reason Code Description</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>US_EP_Rebate_Accrual__c</fullName>
        <description>Q3-2016 Release. Added to support US EP integration</description>
        <externalId>false</externalId>
        <label>Rebate Accrual</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Sales History</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>SH-{00000}</displayFormat>
        <label>Sales History</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sales Histories</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>

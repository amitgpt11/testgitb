<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to allow for the individual dispositions of Cycle Count Line Items where the quantity may be more than 1. The user needs to be able to ID if one of the products was lost, another transferred, another in inventory, etc.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Actual_Lot_Number__c</fullName>
        <externalId>false</externalId>
        <formula>IF(
    NOT(
           ISBLANK(Cycle_Count_Line_Item__r.Other_Inventory_Item__c)
           ),
    
    Cycle_Count_Line_Item__r.Other_Inventory_Item__r.Lot_Number__c,
    
    IF(
        NOT(
               ISBLANK(Cycle_Count_Line_Item__r.Surgery_Item__c)),
    Cycle_Count_Line_Item__r.Surgery_Item__r.Lot_Number__c,
    Cycle_Count_Line_Item__r.Inventory_Item__r.Lot_Number__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual Lot Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Actual_Model_Number__c</fullName>
        <externalId>false</externalId>
        <formula>IF(
    NOT(
           ISBLANK(Cycle_Count_Line_Item__r.Other_Inventory_Item__c)
           ),
    
    Cycle_Count_Line_Item__r.Other_Inventory_Item__r.Material_Number_UPN__c,
    
    IF(
        NOT(
               ISBLANK(Cycle_Count_Line_Item__r.Surgery_Item__c)),
    Cycle_Count_Line_Item__r.Surgery_Item__r.Model_Number__r.EAN_UPN__c,
    Cycle_Count_Line_Item__r.Inventory_Item__r.Material_Number_UPN__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual Model Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Actual_Quantity__c</fullName>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Actual_Quantity__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Actual_Serial_Number__c</fullName>
        <externalId>false</externalId>
        <formula>IF(
    NOT(
           ISBLANK(Cycle_Count_Line_Item__r.Other_Inventory_Item__c)
           ),
    
    Cycle_Count_Line_Item__r.Other_Inventory_Item__r.Serial_Number__c,
    
    IF(
        NOT(
               ISBLANK(Cycle_Count_Line_Item__r.Surgery_Item__c)),
    Cycle_Count_Line_Item__r.Surgery_Item__r.Serial_Number__c,
    Cycle_Count_Line_Item__r.Inventory_Item__r.Serial_Number__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual Serial Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cycle_Count_Line_Item__c</fullName>
        <externalId>false</externalId>
        <label>Cycle Count Line Item</label>
        <referenceTo>Cycle_Count_Line_Item__c</referenceTo>
        <relationshipLabel>Verifications</relationshipLabel>
        <relationshipName>Verifications</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Cycle_Count_Name__c</fullName>
        <description>Cycle Count Name - SFDC-1444</description>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Cycle_Count_Response_Analysis__r.Cycle_Count__r.Name</formula>
        <label>Cycle Count Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cycle_Count_Response_Analysis_Owner__c</fullName>
        <description>Owner from &quot;Cycle Count Response Analysis&quot; Detail screen - SFDC-1444</description>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Cycle_Count_Response_Analysis__r.Owner:User.Full_Name__c</formula>
        <label>Cycle Count Response Analysis Owner</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Investigation_Review__c</fullName>
        <externalId>false</externalId>
        <label>Investigation Review #</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Auto_Updated__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field is used to flag that Reconcile Type Field is set problematically- SFDC-1445</description>
        <externalId>false</externalId>
        <label>Is Auto Updated</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Item_Description__c</fullName>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Item_Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Item Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lot_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Scanned_Lot_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Entered Lot Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Model_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Scanned_Model_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Entered Model Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <externalId>false</externalId>
        <label>Notes</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reconcile_Type__c</fullName>
        <externalId>false</externalId>
        <label>Reconcile Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Lost</fullName>
                    <default>false</default>
                    <label>Lost</label>
                </value>
                <value>
                    <fullName>Shipment in Transit</fullName>
                    <default>false</default>
                    <label>Shipment in Transit</label>
                </value>
                <value>
                    <fullName>Transfer</fullName>
                    <default>false</default>
                    <label>Transfer</label>
                </value>
                <value>
                    <fullName>RMA</fullName>
                    <default>false</default>
                    <label>RMA</label>
                </value>
                <value>
                    <fullName>Part Number Error</fullName>
                    <default>false</default>
                    <label>Part Number Error</label>
                </value>
                <value>
                    <fullName>Used as Sample</fullName>
                    <default>false</default>
                    <label>Used as Sample</label>
                </value>
                <value>
                    <fullName>Stolen</fullName>
                    <default>false</default>
                    <label>Stolen</label>
                </value>
                <value>
                    <fullName>Product on Hand</fullName>
                    <default>false</default>
                    <label>Product on Hand</label>
                </value>
                <value>
                    <fullName>Used in Procedure</fullName>
                    <default>false</default>
                    <label>Used in Procedure</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Serial_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Cycle_Count_Line_Item__r.Scanned_Serial_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Entered Serial Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Verification_Tracking__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Use to record the RGA, Sales Order Number, or Tracking Number</inlineHelpText>
        <label>Verification Tracking</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Verification__c</fullName>
        <externalId>false</externalId>
        <label>Verification Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Corrected System Entry Error</fullName>
                    <default>false</default>
                    <label>Corrected System Entry Error</label>
                </value>
                <value>
                    <fullName>FedEx Not Received</fullName>
                    <default>false</default>
                    <label>FedEx Not Received</label>
                </value>
                <value>
                    <fullName>In Transit</fullName>
                    <default>false</default>
                    <label>In Transit</label>
                </value>
                <value>
                    <fullName>Issue RMA</fullName>
                    <default>false</default>
                    <label>Issue RMA</label>
                </value>
                <value>
                    <fullName>Launched Review Lost</fullName>
                    <default>false</default>
                    <label>Launched Review Lost</label>
                </value>
                <value>
                    <fullName>Launched Review Stolen</fullName>
                    <default>false</default>
                    <label>Launched Review Stolen</label>
                </value>
                <value>
                    <fullName>Pending Transfer</fullName>
                    <default>false</default>
                    <label>Pending Transfer</label>
                </value>
                <value>
                    <fullName>Product on Hand</fullName>
                    <default>false</default>
                    <label>Product on Hand</label>
                </value>
                <value>
                    <fullName>Receipt Discrepancy</fullName>
                    <default>false</default>
                    <label>Receipt Discrepancy</label>
                </value>
                <value>
                    <fullName>Reported in Error</fullName>
                    <default>false</default>
                    <label>Reported in Error</label>
                </value>
                <value>
                    <fullName>Rep Transfer</fullName>
                    <default>false</default>
                    <label>Rep Transfer</label>
                </value>
                <value>
                    <fullName>Returned on RMA</fullName>
                    <default>false</default>
                    <label>Returned on RMA</label>
                </value>
                <value>
                    <fullName>Used as Sample</fullName>
                    <default>false</default>
                    <label>Used as Sample</label>
                </value>
                <value>
                    <fullName>Used in Surgery</fullName>
                    <default>false</default>
                    <label>Used in Surgery</label>
                </value>
                <value>
                    <fullName>Verified Part Number</fullName>
                    <default>false</default>
                    <label>Verified Part Number</label>
                </value>
                <value>
                    <fullName>Verified Quantity</fullName>
                    <default>false</default>
                    <label>Verified Quantity</label>
                </value>
                <value>
                    <fullName>Verified Serial/Lot Number</fullName>
                    <default>false</default>
                    <label>Verified Serial/Lot Number</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Verification</label>
    <nameField>
        <displayFormat>VER-{0}</displayFormat>
        <label>Verification ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Verifications</pluralLabel>
    <recordTypes>
        <fullName>NMD_Verification</fullName>
        <active>true</active>
        <label>NMD Verification</label>
        <picklistValues>
            <picklist>Reconcile_Type__c</picklist>
            <values>
                <fullName>Lost</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Part Number Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Product on Hand</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>RMA</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Shipment in Transit</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Stolen</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Transfer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Used as Sample</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Used in Procedure</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Verification__c</picklist>
            <values>
                <fullName>Corrected System Entry Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>FedEx Not Received</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Transit</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Issue RMA</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Launched Review Lost</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Launched Review Stolen</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending Transfer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Product on Hand</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Receipt Discrepancy</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Rep Transfer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Reported in Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Returned on RMA</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Used as Sample</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Used in Surgery</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Verified Part Number</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Verified Quantity</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Verified Serial%2FLot Number</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Prevent_Reconcile_Type_change</fullName>
        <active>true</active>
        <description>If Reconcile Type is set automatically by code, then Field Rep cant change its value.</description>
        <errorConditionFormula>AND( 
Is_Auto_Updated__c, 
NOT(ISBLANK(TEXT(PRIORVALUE(Reconcile_Type__c)))),
ISCHANGED( Reconcile_Type__c ), 
$Profile.Name=&quot;NMD Field Rep&quot;, 
NOT(OR( 
$Profile.Name = &quot;System Administrator&quot;, 
$Profile.Name = &quot;API User Profile&quot;, 
$Profile.Name = &quot;NMD Super User&quot;, 
$Profile.Name = &quot;Support Admin&quot;, 
$Profile.Name = &quot;BSci System Admin&quot; 
) 
) 

)</errorConditionFormula>
        <errorDisplayField>Reconcile_Type__c</errorDisplayField>
        <errorMessage>You cannot modify/change the Reconcile Type.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>

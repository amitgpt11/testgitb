<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Application specific to Corporate Accounts group</description>
    <formFactors>Large</formFactors>
    <label>Corporate Accounts</label>
    <tab>standard-Account</tab>
    <tab>Account_Plan__c</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Contract</tab>
    <tab>standard-report</tab>
    <tab>Shared_Contract_Amendment_Status__c</tab>
    <tab>Shared_Competitor_Product__c</tab>
</CustomApplication>

<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Footprints</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>footprints__Data_Gem__c</tab>
    <tab>footprints__Action_Plans</tab>
    <tab>footprints__ActionPlanTemplate__c</tab>
    <tab>footprints__Action_Plans_Template_Import</tab>
    <tab>footprints__ActionPlansAbout</tab>
    <tab>footprints__Visit__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>

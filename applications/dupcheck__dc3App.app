<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Duplicate Check</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Lead</tab>
    <tab>dupcheck__dc3Search</tab>
    <tab>dupcheck__dc3Setup</tab>
    <tab>dupcheck__dcBatch</tab>
    <tab>dupcheck__dcDiscard__c</tab>
    <tab>dupcheck__dcEntry</tab>
</CustomApplication>

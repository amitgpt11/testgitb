<!--
Action Plans v3
Force.com Labs
http://appexchange.salesforce.com/listingDetail?listingId=a0N30000003HcINEA0

Copyright (c) 2011, salesforce.com, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution.
    * Neither the name of the salesforce.com, Inc. nor the names of its contributors 
    may be used to endorse or promote products derived from this software 
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.

-->
<apex:page id="detailPage" tabStyle="footprints__ActionPlan__c" standardController="footprints__ActionPlan__c" extensions="footprints.ActionPlanDetailController">

	<script type="text/javascript" src="{!$Resource.ActionPlan_Utilities}"></script>
    <script type="text/javascript" src="{!$Resource.ActionPlans_sortable}"></script>
	<script type="text/javascript" src="{!$Resource.ActionPlan_ActionPlanDetailScripts}"></script>  
	  
    <link href="{!$Resource.ActionPlan_ActionPlanDetailStyles}" rel="stylesheet" type="text/css"/>
   
    
	<apex:sectionHeader title="{!$Label.footprints__ap_Action_Plan}" subtitle="{!actionPlan.Name}" help="/help/doc/user_ed.jsp?loc=help" printUrl="/apex/ActionPlanPrint?id={!actionPlan.Id}"/>
    
    <apex:form id="apForm">
    
        <apex:pageBlock id="detailBlock" title="{!$Label.footprints__ap_ActionPlanDetails}" mode="detail">
        	
        	<!-- PAGE BUTTONS -->
            <apex:pageBlockButtons id="buttons">
                <apex:commandButton id="edit" action="/apex/ActionPlanCreation?id={!actionPlan.Id}" value="{!$Label.footprints__ap_Edit}"/>
                <apex:commandButton id="delete" action="{!deletePlan}" value="{!$Label.footprints__ap_Delete}" onclick="return confirm('{!$Label.footprints__ap_AreYouSureAsk}');" />
                <apex:commandButton id="share" action="/p/share/CustomObjectSharingDetail?parentId={!actionPlan.Id}" value="{!$Label.footprints__ap_Sharing}"/>
            </apex:pageBlockButtons>
            <!-- END PAGE BUTTTONS -->
            
            <!-- INFORMATION SECTION -->
            <apex:pageBlockSection title="{!$Label.footprints__ap_Information}" columns="1">
                <apex:panelGrid columns="5" id="theGrid" style="width:100%" columnClasses="labelCol, dataCol, spacerCol, labelCol, dataCol">
                    <apex:outputText value="{!$Label.footprints__ap_Owner}" />
                    <apex:outputText >{!actionPlan.Owner.Name}&nbsp;[<apex:outputLink value="/{!actionPlan.Id}/a?retURL={!$CurrentPage.URL}">{!$Label.footprints__ap_Change}</apex:outputLink>]</apex:outputText>
                    <apex:outputText >&nbsp;</apex:outputText>
                    <apex:outputText value="{!$Label.footprints__ap_Related_To}" />
                    <apex:outputText >
                        <apex:outputField value="{!actionPlan.footprints__Account__c}" 		rendered="{!(actionPlan.footprints__Account__c != null)}" />
                        <apex:outputField value="{!actionPlan.footprints__Contact__c}" 		rendered="{!(actionPlan.footprints__Contact__c != null)}" />
						
						<!-- Customization Area : Add here your custom Objects -->
						<!-- <apex:outputField value="{!actionPlan.CustomObject__c}" 	rendered="{!(actionPlan.CustomObject__c != null)}" /> -->
						<!-- End of Customization Area -->
						
                    </apex:outputText>
                    
                    <apex:outputText value="{!$Label.footprints__ap_Action_Plan} {!$Label.footprints__ap_Name}" />
                    <apex:outputText value="{!actionPlan.Name}" />
                    <apex:outputText >&nbsp;</apex:outputText>
                    <apex:outputText value="{!$Label.footprints__ap_Status}" />
                    <apex:outputText >{!Completed} {!$Label.footprints__ap_of} {!TaskSize} {!$Label.footprints__ap_TasksComplete}</apex:outputText>
                    
                    <apex:outputText value="{!$Label.footprints__ap_PlanStartDate}" />
                    <apex:outputText >
                    	<apex:pageBlockSectionItem >
                    		<apex:outputField value="{!actionPlan.footprints__StartDate__c}" />
                    	</apex:pageBlockSectionItem>
                    </apex:outputText>
                    <apex:outputText >&nbsp;</apex:outputText>
                    <apex:outputText value="" />
                    <apex:outputText value="" />  
                </apex:panelGrid>
            </apex:pageBlockSection>
            <!-- END INFORMATION SECTION -->
            
            <!-- TASKS SECTION -->
            <apex:pageBlockSection id="taskSection" title="{!$Label.footprints__ap_TaskHeaderSortable}" columns="1">
                <apex:panelGrid columns="5" columnClasses="labelCol, dataCol, spacerCol, labelCol, dataCol">
                    <apex:outputText value="{!$ObjectType.footprints__ActionPlan__c.fields.footprints__SkipWeekends__c.label}"/>
                    <apex:outputText ><apex:inputCheckbox value="{!ActionPlan.footprints__SkipWeekends__c}" disabled="true" /></apex:outputText>
                    <apex:outputText ></apex:outputText>
                    <apex:outputText value="{!$Label.footprints__ap_Action_Plan_Template}" rendered="{!(ActionPlan.footprints__Action_Plan_Template__c != null)}"/>
                    <apex:outputText rendered="{!(ActionPlan.footprints__Action_Plan_Template__c != null)}">
                        <apex:outputLink value="/apex/ActionPlanTemplateDetail?Id={!ActionPlan.footprints__Action_Plan_Template__c}">{!ActionPlan.Action_Plan_Template__r.Name}</apex:outputLink>
                        <apex:outputText rendered="{!IF(Version == true, true, false)}"><br/><span style="color:#cc0000"><strong>{!$Label.footprints__ap_Note}:</strong> {!$Label.footprints__ap_APModifyError}</span></apex:outputText>
                    </apex:outputText>
                </apex:panelGrid>
                
                <apex:panelGrid columns="3" columnClasses="labelCol, dataCol, spacerCol"rendered="{!(ActionPlan.footprints__SkipDay__c != null && ActionPlan.footprints__SkipWeekends__c == true)}">
                    <apex:outputText value="{!$ObjectType.footprints__ActionPlan__c.fields.footprints__SkipDay__c.label}"/>
                    <apex:outputField value="{!actionPlan.footprints__SkipDay__c}" />
                </apex:panelGrid>
                 
                <apex:dataTable value="{!tasks}" var="task" id="theTable" styleClass="sortable">
                    <apex:column id="columnOne" headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__Subject__c.label}" headerClass="subjectCol">
                    	<apex:outputText id="subject">{!task.footprints__Subject__c}</apex:outputText>
                    </apex:column>
                    <apex:column id="dependency" headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__APTaskTemplate__c.label}" headerClass="dependencyColumn">
                    	<apex:outputText >{!task.APTaskTemplate__r.footprints__Subject__c}</apex:outputText>
                    </apex:column>
                    <apex:column value="{!task.footprints__DaysFromStart__c}" headerClass="daysCol">
						<apex:facet name="header">{!$ObjectType.APTaskTemplate__c.fields.DaysFromStart__c.label}
							<span id="daysAfterTtip_help" class="helpButton">
								<img class="helpOrb" title="{!$Label.ap_days_after_msg}" src="/s.gif"/>
							</span>
						</apex:facet>
                   </apex:column>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__ActivityDate__c.label}" headerClass="dateCol">
                        <span style="color:#cc0000"><apex:outputField value="{!task.footprints__ActivityDate__c}" rendered="{!IF(task.footprints__ActivityDate__c < TODAY() && task.footprints__Status__c != 'Completed', true, false)}" /></span>
                        <apex:outputField value="{!task.footprints__ActivityDate__c}" rendered="{!IF(task.ActivityDate__c >= TODAY() || task.Status__c == 'Completed', true, false)}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__User__c.label}" styleClass="assigned_to_field" >
                    	<apex:outputField value="{!task.footprints__User__c}"  />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__Type__c.label}" value="{!task.footprints__Type__c}" headerClass="typeCol"/>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__Status__c.label}" value="{!task.footprints__Status__c}" headerClass="statusCol"/>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__Priority__c.label}" value="{!task.footprints__Priority__c}" headerClass="priorityCol"/>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__Reminder__c.label}" headerClass="reminderCol" >
                    	<apex:outputPanel layout="block" >
                    		<apex:outputField value="{!task.footprints__Reminder__c}" />
                    		<apex:outputField value="{!task.footprints__Time_Reminder__c}" />
						</apex:outputPanel>
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.footprints__APTaskTemplate__c.fields.footprints__Comments__c.label}" headerClass="commentsCol">
                    	
                    	<apex:outputPanel id="commentPanel" style="display:none" >
                       		<div id="commentContainer" onmouseout="javascript:ActionPlanDetailScripts.hideComments('{!$Component.theTable.columnOne}');">
                        		<div class="hd">
                        			<div class="hd-left">{!$Label.ap_Comments}</div>
                        			<div class="hd-right"></div>
                        		</div>
                        		<div class="bd">
                        			<textarea readonly="true" id="Comments">{!task.Comments__c}</textarea>
                        		</div>
                       		</div>
                       	</apex:outputPanel>
                       	
						<apex:outputPanel rendered="{!task.footprints__Comments__c != null}">(<a onmouseover="javascript:ActionPlanDetailScripts.showComments('{!$Component.theTable.columnOne}');" style="text-decoration:underline">...</a>)</apex:outputPanel>
                    </apex:column>
                </apex:dataTable>
                
            </apex:pageBlockSection>
            <!-- END TASKS SECTION -->
            
            <!-- SYSTEM INFORMATION SECTION -->
            <apex:pageBlockSection title="System Information" columns="1">
	            <apex:panelGrid columns="7" columnClasses="labelCol, dataCol2, dataCol2, spacerCol, labelCol, dataCol2, dataCol2">
		            <apex:outputText value="{!$Label.footprints__ap_CreatedBy}" />
		            <apex:outputField value="{!ActionPlan.CreatedById}" />
		            <apex:outputField value="{!ActionPlan.CreatedDate}" />
		            <apex:outputText />
		            <apex:outputText value="{!$Label.footprints__ap_LastModifiedBy}" />
		            <apex:outputField value="{!ActionPlan.LastModifiedById}" />
		            <apex:outputField value="{!ActionPlan.LastModifiedDate}" />
	            </apex:panelGrid>
           	</apex:pageBlockSection>
           	<!-- END SYSTEM INFORMATION SECTION -->
           	
        </apex:pageBlock>        
        
    </apex:form>
    
    <script>
        ActionPlanDetailScripts.formatFields();
        ActionPlanDetailScripts.removeHover();
    </script>

</apex:page>
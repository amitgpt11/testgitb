<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Manager_of_Potential_Change</fullName>
        <description>Alert Manager of Potential Change</description>
        <protected>false</protected>
        <recipients>
            <field>Last_Modified_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Potential_Total_Potential_has_been_changed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Division_Controlling_Field_EP</fullName>
        <description>sets the Potential.Division_Controlling_Field__c to the value of Potential.Division__c</description>
        <field>Division_Controlling_Field__c</field>
        <literalValue>EP</literalValue>
        <name>Set Division Controlling Field - EP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Division_Controlling_Field_PI</fullName>
        <description>sets the Potential.Division_Controlling_Field__c to the value of Potential.Division__c - PI specific</description>
        <field>Division_Controlling_Field__c</field>
        <literalValue>PI</literalValue>
        <name>Set Division Controlling Field - PI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Division_Controlling_Field_To_IC</fullName>
        <description>sets the Potential.Division_Controlling_Field__c to the value of Potential.Division__c - IC Specific</description>
        <field>Division_Controlling_Field__c</field>
        <literalValue>IC</literalValue>
        <name>Set Division Controlling Field To IC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Division_Controlling_Field_U_WH</fullName>
        <description>sets the Potential.Division_Controlling_Field__c to the value of Potential.Division__c - Uro/WH specific</description>
        <field>Division_Controlling_Field__c</field>
        <literalValue>Uro/WH</literalValue>
        <name>Set Division Controlling Field - U/WH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Potential_Estimated_to_BLANK</fullName>
        <description>Since new Quantity and ASP values have been entered, Potential - Estimated is blanked out.</description>
        <field>Potential_Estimated__c</field>
        <name>Set Potential - Estimated to BLANK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPDATE_PREVIOUS_POTENTIAL</fullName>
        <field>Previous_Potential__c</field>
        <formula>IF 
(
(NOT(ISNULL(Potential_Estimated__c))&amp;&amp;ISCHANGED(Potential_Estimated__c)), Old_Potential_Estimated_Value__c, 

(IF((ISNULL(Potential_Estimated__c)&amp;&amp;NOT(ISNULL(Potential_Calculated__c))&amp;&amp;ISCHANGED(Potential_Estimated__c)),PRIORVALUE(Potential_Calculated__c),

(IF ((NOT(ISNULL(Potential_Estimated__c))&amp;&amp;ISCHANGED(Potential_Calculated__c)), Old_Potential_Estimated_Value__c,

(IF ((ISNULL(Potential_Estimated__c)&amp;&amp;NOT(ISNULL(Potential_Calculated__c))&amp;&amp;ISCHANGED(Potential_Calculated__c)),PRIORVALUE(Potential_Calculated__c),0)
))
))
))</formula>
        <name>UPDATE PREVIOUS POTENTIAL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Old_Potential_Estimated</fullName>
        <description>update with prior value for Potential-Estimated</description>
        <field>Old_Potential_Estimated_Value__c</field>
        <formula>PRIORVALUE(Potential_Estimated__c)</formula>
        <name>Update Old Potential - Estimated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Text</fullName>
        <field>EU_Product_Text__c</field>
        <formula>Product__r.Name</formula>
        <name>Update Product Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Manager when Total Potential has changed</fullName>
        <actions>
            <name>Alert_Manager_of_Potential_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule will send an alert to the user&apos;s manager when the user has updated a Potential record that changes the Total Potential</description>
        <formula>ISCHANGED(Total_Potential__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Previous Potential</fullName>
        <actions>
            <name>UPDATE_PREVIOUS_POTENTIAL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Potentials are changed, the Previous Potential is changed to the prior value of the Revenue Potential field.</description>
        <formula>OR  (ISCHANGED( Potential_Calculated__c ), ISCHANGED( Potential_Estimated__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove Potential - Estimated Value</fullName>
        <actions>
            <name>Set_Potential_Estimated_to_BLANK</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Old_Potential_Estimated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If a Potential - Estimated value exists, and Quantity and ASP exist and/or are entered, the existing Potential - Estimated value will be removed.</description>
        <formula>OR (NOT(ISBLANK(Quantity__c)),NOT(ISBLANK(ASP__c))) &amp;&amp; NOT(ISBLANK(Potential_Estimated__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Division Controlling Field - EP</fullName>
        <actions>
            <name>Set_Division_Controlling_Field_EP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Performance__c.Division__c</field>
            <operation>equals</operation>
            <value>EP</value>
        </criteriaItems>
        <description>this field will take the value of the Potential.Division__c field - two separate fields because this one is used in the field dependencies - EP Specific</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Division Controlling Field - IC</fullName>
        <actions>
            <name>Set_Division_Controlling_Field_To_IC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Performance__c.Division__c</field>
            <operation>equals</operation>
            <value>IC</value>
        </criteriaItems>
        <description>this field will take the value of the Potential.Division__c field - two separate fields because this one is used in the field dependencies - IC Specific</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Division Controlling Field - PI</fullName>
        <actions>
            <name>Set_Division_Controlling_Field_PI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Performance__c.Division__c</field>
            <operation>equals</operation>
            <value>PI</value>
        </criteriaItems>
        <description>this field will take the value of the Potential.Division__c field - two separate fields because this one is used in the field dependencies - PI Specific</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Division Controlling Field - U%2FWH</fullName>
        <actions>
            <name>Set_Division_Controlling_Field_U_WH</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Performance__c.Division__c</field>
            <operation>equals</operation>
            <value>Uro/WH</value>
        </criteriaItems>
        <description>this field will take the value of the Potential.Division__c field - two separate fields because this one is used in the field dependencies - U/WH Specific</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updated Potential Product</fullName>
        <actions>
            <name>Update_Product_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DRAFT US-3983 - CRM/EP Potentials &amp; Procedures Review</description>
        <formula>OR(ISCHANGED(Product__c), ISNEW(), NOT(ISBLANK(Product__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

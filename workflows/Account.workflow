<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_ASP_Values_Changed</fullName>
        <field>ASP_Values_Changed__c</field>
        <literalValue>1</literalValue>
        <name>Check ASP Values Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_CUSTOMER_RECORD_TYPE</fullName>
        <description>Updates the Account Record Type to &quot;Customer&quot; when the SAP Account Number is populated.</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SET_CUSTOMER_RECORD_TYPE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Record_Type</fullName>
        <description>Set Account Record Type</description>
        <field>RecordTypeId</field>
        <lookupValue>Consignment_Inventory</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Account Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Footprints_Off_to_True</fullName>
        <description>Will update the Footprints Off field to True so the account is not synced to the Footprints server for Geocoded.</description>
        <field>footprints__Footprints_OFF__c</field>
        <literalValue>1</literalValue>
        <name>Update Footprints Off to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UroPH_account_checkbox_update</fullName>
        <field>UroPH_Account__c</field>
        <literalValue>1</literalValue>
        <name>UroPH account checkbox update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change ASP Values</fullName>
        <actions>
            <name>Check_ASP_Values_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>this rule will identify if the ASP values are changed for an account or not.</description>
        <formula>OR( ISCHANGED(Trial_ASP_Value__c), ISCHANGED( Implant_ASP_Value__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set_Footprint_Off_to_True</fullName>
        <actions>
            <name>Update_Footprints_Off_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.footprints__Footprints_OFF__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Created as part of the Q2 2016 release.  Rally # 2172 – JM
This rule sets the Footprints off field to true on creation on any account.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UPDATE_ACCOUNT_TO_CUSTOMER</fullName>
        <actions>
            <name>SET_CUSTOMER_RECORD_TYPE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Classification__c</field>
            <operation>notEqual</operation>
            <value>NM Sales Rep</value>
        </criteriaItems>
        <description>When an Account is created or edited, this rule will check to see if the SAP Account Number is populated. If it is, the Account Record Type will be updated to &quot;Customer.&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Record Type</fullName>
        <actions>
            <name>Set_Account_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer,Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Classification__c</field>
            <operation>equals</operation>
            <value>NM Sales Rep</value>
        </criteriaItems>
        <description>Update account Record Type to Consignment Inventory if Classification=NM Sales Rep</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UroPH_Update_checkbox_upon_creation</fullName>
        <actions>
            <name>UroPH_account_checkbox_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>User.Profile_Name__c</field>
            <operation>equals</operation>
            <value>Uro Sales Operations User</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Profile_Name__c</field>
            <operation>equals</operation>
            <value>Uro Sales/Marketing User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-Buying</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.UroPH_Account__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Q3 2016 - US2701 - This rule will set UroPH_Account checkbox to true when an account is created by Uro user through UI</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

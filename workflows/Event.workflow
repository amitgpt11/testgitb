<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>InTouch_Alert_Appointment_Request_Approved</fullName>
        <description>InTouch Alert: Appointment Request Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/InTouch_Community_Appointment_Confirmed</template>
    </alerts>
    <alerts>
        <fullName>InTouch_Alert_Appointment_Request_Declined</fullName>
        <description>InTouch Alert: Appointment Request Declined</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/InTouch_Community_Appointment_Declined</template>
    </alerts>
    <alerts>
        <fullName>InTouch_Appointment_Request_Created_Status_Pending</fullName>
        <description>InTouch Appointment Request Created: Status Pending</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/Appointment_Request_Pending</template>
    </alerts>
    <fieldUpdates>
        <fullName>ANZ_Update_Interaction_Type</fullName>
        <description>US2582- DRAFT US-3839 - Activity master story</description>
        <field>Interaction_Type__c</field>
        <literalValue>Remote</literalValue>
        <name>ANZ_Update_Interaction_Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ANZ_Update_Interaction_Type_2</fullName>
        <field>Interaction_Type__c</field>
        <literalValue>In-Person</literalValue>
        <name>ANZ_Update_Interaction_Type_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ANZ_Update_Interaction_Type</fullName>
        <actions>
            <name>ANZ_Update_Interaction_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Event.Subject</field>
            <operation>equals</operation>
            <value>Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Subject</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Event Record</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Interaction_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>US2582 DRAFT US-3839 - Activity master story</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ANZ_Update_Interaction_Type_2</fullName>
        <actions>
            <name>ANZ_Update_Interaction_Type_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Event.Subject</field>
            <operation>notEqual</operation>
            <value>Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Subject</field>
            <operation>notEqual</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Event Record</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Interaction_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>US2582 DRAFT US-3839 - Activity master story</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

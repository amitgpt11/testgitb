<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Shared_InTouch_Contact_Details_Updated</fullName>
        <description>InTouch Contact Details Updated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/Contact_Edited_by_Community_User</template>
    </alerts>
    <fieldUpdates>
        <fullName>SET_CUSTOMER_RECORD_TYPE</fullName>
        <description>Updates the Contact Record Type to &quot;Customer Physician&quot; when the SAP ID is populated.</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Physician</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SET_CUSTOMER_RECORD_TYPE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prior_Email_Field</fullName>
        <field>Shared_Prior_Email__c</field>
        <formula>PRIORVALUE(Email)</formula>
        <name>Update Prior Email Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Contact Email Changed</fullName>
        <actions>
            <name>Shared_InTouch_Contact_Details_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Fires when the prior email field is set on the Contact with a new email address by a Community User.</description>
        <formula>ISCHANGED( Shared_Prior_Email__c ) &amp;&amp; Shared_Prior_Email__c != Email &amp;&amp; NOT(ISPICKVAL($User.UserType, &quot;Standard&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UPDATE_CONTACT_TO_CUSTOMER</fullName>
        <actions>
            <name>SET_CUSTOMER_RECORD_TYPE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.SAP_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a Contact is created or edited, this rule will check to see if the SAP ID is populated. If it is, the Contact Record Type will be updated to &quot;Customer Physician.&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Prior Email</fullName>
        <actions>
            <name>Update_Prior_Email_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggered when the Email field on the contact is updated to a new value.  Used with the InTouch Community</description>
        <formula>ISCHANGED( Email ) &amp;&amp; PRIORVALUE(Email) != Email &amp;&amp; NOT(ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

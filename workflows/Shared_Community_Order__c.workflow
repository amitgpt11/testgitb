<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Community_Order_Submitted_VTM_Notification</fullName>
        <ccEmails>customerservice.nordic@bsci.com</ccEmails>
        <description>InTouch Community Order Submitted VTM Notification</description>
        <protected>false</protected>
        <recipients>
            <field>VTM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/InTouch_Community_Endo_Order_Submitted</template>
    </alerts>
    <alerts>
        <fullName>InTouch_Community_Order_Submitted_Customer_Notification</fullName>
        <description>InTouch Community Order Submitted Customer Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/InTouch_Community_Order_Email_Customer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Community_Order_Date_Submitted</fullName>
        <description>Sets the Date Submitted on Community Order records to be Today</description>
        <field>Shared_Date_Submitted__c</field>
        <formula>NOW()</formula>
        <name>Community Order Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_VTM_Email_on_Community_Order</fullName>
        <description>Sets the VTM Email on Community Order records as the Contact Owner Email address</description>
        <field>VTM_Email__c</field>
        <formula>Contact__r.Owner.Email</formula>
        <name>Set VTM Email on Community Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CO Test</fullName>
        <active>false</active>
        <formula>NOT(ISPICKVAL( Owner:User.UserType, &quot;Standard&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Community Order Submitted</fullName>
        <actions>
            <name>Community_Order_Submitted_VTM_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>InTouch_Community_Order_Submitted_Customer_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Community_Order_Date_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Shared_Community_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Processing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Community Order VTM Email</fullName>
        <actions>
            <name>Set_VTM_Email_on_Community_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISPICKVAL( Status__c, &quot;Completed&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

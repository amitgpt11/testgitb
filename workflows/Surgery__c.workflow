<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Model_Lot_Serial</fullName>
        <field>Serial_Lot__c</field>
        <formula>Model_Number__r.EAN_UPN__c + &quot;/&quot; + BLANKVALUE(Lot_Number__c,&apos;&apos;) + &quot;/&quot; + BLANKVALUE(Serial_Number__c,&apos;&apos;)</formula>
        <name>Populate Model/Lot/Serial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Surgery_Name</fullName>
        <field>Name</field>
        <formula>TEXT(Order__r.Procedure_Type__c) &amp; &quot; &quot; &amp; TEXT(Order__r.Surgery_Date__c)</formula>
        <name>Populate Surgery Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Model%2FLot%2FSerial</fullName>
        <actions>
            <name>Populate_Model_Lot_Serial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   ISNEW(),   ISCHANGED(Model_Number__c),   ISCHANGED(Lot_Number__c),   ISCHANGED(Serial_Number__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Surgery Name</fullName>
        <actions>
            <name>Populate_Surgery_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Concatenation of the Procedure Type and the Procedure Date</description>
        <formula>AND(         $RecordType.DeveloperName=&quot;NMD_Surgery&quot;,         NOT(ISBLANK(Order__c))         )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

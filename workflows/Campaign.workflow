<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CA_Set_Active_to_True</fullName>
        <description>Sets the Active checkbox to true for all Corporate Account Product Launch Campaigns</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Set Active to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Activate Campaign</fullName>
        <actions>
            <name>CA_Set_Active_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Account Product Launch</value>
        </criteriaItems>
        <description>Activate the Product Launch Campaign type for Corporate Accounts upon saving the record.
Created as part of Jira Story # 925 - CA.  Q4 2016 Qtr Release</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Activities to Backbone Campaign</fullName>
        <actions>
            <name>Schedule_Customer_Visit</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Send_Confirmation_Email</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Send_Recap_Email</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X1st_Deal_Update_Email</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X1st_Monthly_Call</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X2nd_Deal_Update_Email</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X2nd_Monthly_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>equals</operation>
            <value>Backbone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule assigns specific Activities if Campaign of Type Backbone is created (Europe NC requirement).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Activities to New Product Launch Campaign</fullName>
        <actions>
            <name>New_Product_Launch_Confirmation_Email</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>New_Product_Launch_Confirmation_Field_Visit</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>New_Product_Launch_Email</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>New_Product_Launch_Phone_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>equals</operation>
            <value>New Product Launch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule assigns specific Activities if Campaign of Type New Product Launch is created (Europe NC requirement).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>New_Product_Launch_Confirmation_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Day 4: new product confirmation email</description>
        <dueDateOffset>4</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Product Launch Confirmation Email</subject>
    </tasks>
    <tasks>
        <fullName>New_Product_Launch_Confirmation_Field_Visit</fullName>
        <assignedToType>owner</assignedToType>
        <description>Day 15 to Day 45: new product field visit</description>
        <dueDateOffset>45</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Product Launch Confirmation Field Visit</subject>
    </tasks>
    <tasks>
        <fullName>New_Product_Launch_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Day 1: new product email</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Product Launch Email</subject>
    </tasks>
    <tasks>
        <fullName>New_Product_Launch_Phone_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Day 3: new product phone call</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Product Launch Phone Call</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Customer_Visit</fullName>
        <assignedToType>owner</assignedToType>
        <description>NCAM with Inside Sales (if possible) visiting the customer and explaining the whole concept and advantages, and capturing customer&apos;s info (contacts, preferences).</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Customer Visit</subject>
    </tasks>
    <tasks>
        <fullName>Send_Confirmation_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Enrollment confirmation email, recap of added value and advantages, and confirmation of slot for next phone call.</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Confirmation Email</subject>
    </tasks>
    <tasks>
        <fullName>Send_Recap_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Email including recap of monthly call + relevant medical news and new product launch info. If new product launch: connect with New Product launch campaign.</description>
        <dueDateOffset>25</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Monthly Call Recap Email</subject>
    </tasks>
    <tasks>
        <fullName>X1st_Deal_Update_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Deal (or current sales) numbers update + Comments and advice around deal targets achievement</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>1st Deal Update Email</subject>
    </tasks>
    <tasks>
        <fullName>X1st_Monthly_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Follow-up on 1st Deal Update Email.</description>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>1st Monthly Call</subject>
    </tasks>
    <tasks>
        <fullName>X2nd_Deal_Update_Email</fullName>
        <assignedToType>owner</assignedToType>
        <description>2nd Deal (or current sales) numbers update + Comments and advice around deal targets achievement.</description>
        <dueDateOffset>35</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>2nd Deal Update Email</subject>
    </tasks>
    <tasks>
        <fullName>X2nd_Monthly_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Start to elaborate on specific proposals to expand from current deal - Test the most relevant approach (P1 / P2 / P4 / P5), open for discussions.</description>
        <dueDateOffset>42</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Campaign.StartDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>2nd Monthly Call</subject>
    </tasks>
</Workflow>

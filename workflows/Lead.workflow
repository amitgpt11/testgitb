<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Educated_Date</fullName>
        <description>For NMD Lead record types if the Education Date is Less than or Equal to today, the Lead Stage is updated to Educated</description>
        <field>Status</field>
        <literalValue>Educated</literalValue>
        <name>Educated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contacted_date_to_Educated_date</fullName>
        <field>First_Contacted_Date__c</field>
        <formula>Educated_Date__c</formula>
        <name>Set Contacted date to Educated date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Disqualified_Date</fullName>
        <description>Set Disqualified Date when criteria is met.</description>
        <field>Disqualified_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Disqualified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LastName</fullName>
        <description>Sets the LastName to the related Patient Id.</description>
        <field>LastName</field>
        <formula>BLANKVALUE(Patient__c, LastName)</formula>
        <name>Set LastName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_First_Name</fullName>
        <description>Set the value as blank</description>
        <field>FirstName</field>
        <name>Set Lead First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Last_Name</fullName>
        <description>Set Opportunity First Name as: Trialing Physician.Last Name + - + Patient First Name (Left 2 characters) +.+ Patient Last name (left 3 characters)</description>
        <field>LastName</field>
        <formula>Trialing_Physician__r.LastName &amp; 
&quot;-&quot; &amp; 
LEFT(  Patient__r.Patient_First_Name__c , 2)&amp; 
&quot;.&quot; &amp; 
LEFT( Patient__r.Patient_Last_Name__c , 3)</formula>
        <name>Set Lead Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Record_Type</fullName>
        <description>Set Lead record Type when Criteria is met</description>
        <field>RecordTypeId</field>
        <lookupValue>NMD_Patient</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_contacted</fullName>
        <field>Status</field>
        <literalValue>Contacted</literalValue>
        <name>Set Status to contacted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Disqualified</fullName>
        <field>Status</field>
        <literalValue>Disqualified (Closed)</literalValue>
        <name>Set Status to Disqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Educated</fullName>
        <description>Set Lead status to Educated</description>
        <field>Status</field>
        <literalValue>Educated</literalValue>
        <name>Set Status to Educated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Undecided</fullName>
        <field>Status</field>
        <literalValue>Undecided</literalValue>
        <name>Set Status to Undecided</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Uncertain_Date</fullName>
        <description>Set the date time field to current date time when conditions are met.</description>
        <field>Uncertain_Date__c</field>
        <formula>now()</formula>
        <name>Set Uncertain Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Care_Card_Physician</fullName>
        <description>Update CARE Card physician with First Name and Last Name of Trialing Physician if CARE CARD physician is NULL and Trialing Physician &lt;&gt;Null</description>
        <field>CARE_Card_Physician__c</field>
        <formula>Trialing_Physician__r.FirstName&amp;&quot; &quot;&amp; Trialing_Physician__r.LastName</formula>
        <name>Update Care Card Physician</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Advance Lead when Educated Date Past</fullName>
        <actions>
            <name>Educated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <description>For NMD Lead record types if the Education Date is Less than or Equal to today, the Lead Stage is updated to Educated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Perform Patient Education</fullName>
        <actions>
            <name>Patient_Education_Physician_Patient_Initials</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>NMD-When Educated Date&gt;Today and Status=Contacted, Task should be assigned to Lead Owner w/subject  &quot;Patient Education+ Physician + Patient Initials&quot; and due date=&quot;Education Date&quot;</description>
        <formula>AND(      OR(        ISPICKVAL( Status ,&quot;New&quot;),        ISPICKVAL( Status ,&quot;Contacted&quot;),        ISPICKVAL( Status ,&quot;Educated&quot;)       )      ,DATEVALUE(Educated_Date__c)   &gt;  TODAY()      )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Contacted</fullName>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.First_Contacted_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Disable_Workflow__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Set Lead Status to Contacted when contacted Date is greater than Today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Stage_to_contacted</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.First_Contacted_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Status to Educated</fullName>
        <actions>
            <name>Set_Status_to_Educated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Disable_Workflow__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Set Lead Status to Educated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Care Card Physician</fullName>
        <actions>
            <name>Update_Care_Card_Physician</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update CARE Card physician with First Name and Last Name of Trialing Physician if CARE CARD physician is NULL and Trialing Physician &lt;&gt;Null</description>
        <formula>AND( $RecordType.DeveloperName=&apos;NMD_Patient&apos;,      ISBLANK(CARE_Card_Physician__c),      NOT(ISBLANK(Trialing_Physician__c))         )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Contacted Date to Educated Date</fullName>
        <actions>
            <name>Set_Contacted_date_to_Educated_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.First_Contacted_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Contacted Date to Educated Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Disqualified Date</fullName>
        <actions>
            <name>Set_Disqualified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Disqualified (Closed)</value>
        </criteriaItems>
        <description>Update disqualified date when Lead Status is set to disqualified(closed)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Name</fullName>
        <actions>
            <name>Set_LastName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <description>Sets the LastName of the NMD Patient Lead to the record Id of the associated Patient.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Name</fullName>
        <actions>
            <name>Set_Lead_First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lead_Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <description>Set Oportunity First Name as: Trialing Physician.Last Name + - + Patient First Name (Left 2 characters) +.+ Patient Last name (left 3 characters)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type</fullName>
        <actions>
            <name>Set_Lead_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>CARE Card,Care Online,Office,Patient Care,Patient Event,Referral,SCS Patient Referral</value>
        </criteriaItems>
        <description>Update lead Record type based on Lead Source</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Disqualified</fullName>
        <actions>
            <name>Set_Status_to_Disqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Uncertain_Status__c</field>
            <operation>equals</operation>
            <value>Not Interested</value>
        </criteriaItems>
        <description>Update Stage to Disqualified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Closed</fullName>
        <actions>
            <name>Set_Status_to_Disqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Duplicate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <description>Update Lead Status to closed when duplicate field is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Contacted</fullName>
        <actions>
            <name>Set_Stage_to_contacted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.First_Contacted_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.First_Contacted_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Update Lead Status to Contacted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Educated</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Educated_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Disable_Workflow__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Update Lead Status to Educated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Educated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.Educated_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Status to Undecided</fullName>
        <actions>
            <name>Set_Status_to_Undecided</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Uncertain_Status__c</field>
            <operation>equals</operation>
            <value>Undecided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <description>Update Lead Stage to Undecided</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Uncertain Date</fullName>
        <actions>
            <name>Set_Uncertain_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Undecided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Patient</value>
        </criteriaItems>
        <description>Updates the Uncertain Date when the Lead status changes to &quot;Uncertain&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Patient_Education_Physician_Patient_Initials</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.Educated_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Patient Education+Physician+Patient Initials</subject>
    </tasks>
</Workflow>

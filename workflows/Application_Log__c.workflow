<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Application_Error</fullName>
        <ccEmails>SFDCSystemLogs@bsci.com</ccEmails>
        <description>Send Application Error</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Neuromod/SFDC_Support_Team</template>
    </alerts>
    <alerts>
        <fullName>Send_Communication_Error</fullName>
        <description>Send Communication Error</description>
        <protected>false</protected>
        <recipients>
            <recipient>colleen.farrell@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michael.lu@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tina.day@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Neuromod/Communication_Error_Log</template>
    </alerts>
    <alerts>
        <fullName>Send_Integration_Error</fullName>
        <description>Send Integration Error</description>
        <protected>false</protected>
        <recipients>
            <recipient>colleen.farrell@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michael.lu@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tina.day@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Neuromod/Integration_Error_Log</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Record_Type</fullName>
        <description>Changes the Record Type to Limited Release</description>
        <field>RecordTypeId</field>
        <lookupValue>Limited_Release</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Communication</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Communication</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Communication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Integration</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Integration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Integration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Application Error</fullName>
        <actions>
            <name>Send_Application_Error</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application_Log__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Application</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type</fullName>
        <actions>
            <name>Change_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application_Log__c.Record_Object_Type__c</field>
            <operation>equals</operation>
            <value>List__c</value>
        </criteriaItems>
        <description>Determines if the Application_Log__c.Record_Object_Type__c is populated with the text string &quot;List__c&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Integration Error</fullName>
        <actions>
            <name>Send_Integration_Error</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Record_Type_to_Integration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application_Log__c.Message_Type__c</field>
            <operation>equals</operation>
            <value>E,A</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Application Log Record Type</fullName>
        <actions>
            <name>Send_Communication_Error</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Record_Type_to_Communication</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application_Log__c.Name</field>
            <operation>equals</operation>
            <value>Communication Error</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

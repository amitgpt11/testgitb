<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Discrepancy_Amount</fullName>
        <description>Populates newrecords with DiscrepancyAmount=InventoryItem.SAPQty since ActualQty&apos;s value is populated after record creation &amp; isn&apos;t available for use in initial field update.If Scanned &amp; Actual have values,Scanned-Actual, if scanned=0, then actual,else 0</description>
        <field>Discrepancy_Amount__c</field>
        <formula>ABS(CASE(1,
  IF(AND(
             ISNEW(), 
             NOT(Scanned_Quantity__c=Inventory_Item__r.SAP_Quantity__c), 
             NOT(Scanned_Quantity__c&lt;Inventory_Item__r.SAP_Quantity__c)),1,0),
             Scanned_Quantity__c-Inventory_Item__r.SAP_Quantity__c,
  IF(AND( 
             NOT(ISBLANK(Scanned_Quantity__c)), 
             NOT(ISBLANK(Actual_Quantity__c)),
             NOT(Scanned_Quantity__c=Actual_Quantity__c), 
             NOT(Scanned_Quantity__c&lt;Actual_Quantity__c)),1,0),
             Scanned_Quantity__c-Actual_Quantity__c,
  IF(AND(
             ISBLANK(Scanned_Quantity__c), 
             NOT(ISBLANK(Actual_Quantity__c))),1,0),
             Actual_Quantity__c, 0
))</formula>
        <name>Populate Discrepancy Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Current_Quantity</fullName>
        <field>Current_Quantity__c</field>
        <formula>IF(NOT(ISBLANK(Inventory_Item__c)),
    Inventory_Item__r.SAP_Quantity__c,
    Other_Inventory_Item__r.SAP_Quantity__c)</formula>
        <name>Set Current Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Exception_Reason_to_In_Inventory</fullName>
        <field>Exception_Reason__c</field>
        <literalValue>In Inventory</literalValue>
        <name>Set Exception Reason to In Inventory</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Exception_Reason_to_Missing</fullName>
        <field>Exception_Reason__c</field>
        <literalValue>Missing (Serial)</literalValue>
        <name>Set Exception Reason to Missing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Exception_Reason_to_Shortage</fullName>
        <field>Exception_Reason__c</field>
        <literalValue>Shortage (Lot items)</literalValue>
        <name>Set Exception Reason to Shortage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Exception_Reason_to_Surplus</fullName>
        <field>Exception_Reason__c</field>
        <literalValue>Surplus</literalValue>
        <name>Set Exception Reason to Surplus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Expected_Value_Number</fullName>
        <field>Expected_Value_Number__c</field>
        <formula>Expected_Value__c</formula>
        <name>Set Expected Value Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Expiration_Date</fullName>
        <field>Scanned_Expiration_Date__c</field>
        <formula>IF(NOT(ISBLANK(Inventory_Item__c)),
Inventory_Item__r.Expiration_Date__c,
Other_Inventory_Item__r.Expiration_Date__c)</formula>
        <name>Set Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Surplus_Number</fullName>
        <field>Surplus_Number__c</field>
        <formula>IF(  
BLANKVALUE(Scanned_Quantity__c,0)&gt;BLANKVALUE(Actual_Quantity__c,0),
BLANKVALUE(Scanned_Quantity__c,0)-BLANKVALUE(Actual_Quantity__c,0), 
0 
)</formula>
        <name>Set Surplus Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Value_Matched_Number</fullName>
        <field>Value_Matched_Number__c</field>
        <formula>IF(NOT(ISBLANK(Matched__c)),
Matched__c * Inventory_Item__r.Model_Number__r.Cost__c,
0)</formula>
        <name>Set Value Matched Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Value_Not_Lost_Number</fullName>
        <field>Value_Not_Lost_Number__c</field>
        <formula>IF(
    AND( 
            NOT( 
                    ISBLANK(TEXT(Reconcile_Type__c))), 
            NOT( 
                    ISPICKVAL(Reconcile_Type__c,&quot;Lost&quot;)), 
            NOT(Actual_Quantity__c=Inventory_Item__r.SAP_Quantity__c)),
            Inventory_Item__r.Model_Number__r.Cost__c * Inventory_Item__r.SAP_Quantity__c,
            Value_Not_Lost__c)</formula>
        <name>Set Value Not Lost Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Value_Not_Matched_Number</fullName>
        <field>Value_Not_Matched_Number__c</field>
        <formula>IF(
    AND( 
            NOT(ISBLANK(TEXT(Reconcile_Type__c))), 
            NOT(ISPICKVAL(Exception_Reason__c,&quot;In Inventory&quot;)),  
            NOT(Actual_Quantity__c=Inventory_Item__r.SAP_Quantity__c)),
            Inventory_Item__r.Model_Number__r.Cost__c * Inventory_Item__r.SAP_Quantity__c,
           Value_Not_Matched__c)</formula>
        <name>Set Value Not Matched Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Lot</fullName>
        <field>NMD_External_Lot_Number_Actual__c</field>
        <formula>NMD_Actual_Lot_Number__c</formula>
        <name>Update Actual Lot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Quantity</fullName>
        <description>Update the Actual_Quantity__c to match the parent Inventory Item&apos;s SAP Quantity</description>
        <field>Actual_Quantity__c</field>
        <formula>Inventory_Item__r.SAP_Quantity__c</formula>
        <name>Update Actual Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Serial</fullName>
        <field>External_serial_number_actual__c</field>
        <formula>NMD_Actual_Serial_number__c</formula>
        <name>Update Actual Serial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stolen_and_Lost_Value_Number</fullName>
        <description>Copies value from Stolen and Lost value</description>
        <field>Stolen_and_Lost_Value_Number__c</field>
        <formula>Stolen_and_Lost__c * Inventory_Item__r.Model_Number__r.Cost__c</formula>
        <name>Update Stolen and Lost Value Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>NMD_update_Lot_actual</fullName>
        <actions>
            <name>Update_Actual_Lot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND(ISNEW(),NOT(ISBLANK( Inventory_Item__c ))),  ISCHANGED( Inventory_Item__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NMD_update_Serial_actual</fullName>
        <actions>
            <name>Update_Actual_Serial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND(ISNEW(),NOT(ISBLANK( Inventory_Item__c ))),  ISCHANGED( Inventory_Item__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Discrepancy Amount</fullName>
        <actions>
            <name>Populate_Discrepancy_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evaluates a Line Item with a NMD record type every time it is edited and populates the Discrepancy Amount field so that it can be used in various roll-up summaries on the Response Analysis since Discrepancy is a formula which can&apos;t be used in a rollup.</description>
        <formula>AND(         $RecordType.DeveloperName=&quot;NMD_Cycle_Count_Line_Item&quot;,          OR(                Discrepancy__c != Discrepancy_Amount__c,               NOT(ISBLANK(Actual_Quantity__c)),               NOT(ISBLANK(Scanned_Quantity__c)),               ISCHANGED(Actual_Quantity__c),               ISCHANGED(Scanned_Quantity__c) )               )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Exception Reason Shortage</fullName>
        <actions>
            <name>Set_Exception_Reason_to_Shortage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT (ISBLANK( Inventory_Item__c)), Inventory_Item__r.Model_Number__r.Serial_Indicator__c=False)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Exception Reason to In Inventory</fullName>
        <actions>
            <name>Set_Exception_Reason_to_In_Inventory</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT (ISNULL(Inventory_Item__c )),         OR(               ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Scanning&quot;),               ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Analyzing&quot;)),         Scanned_Quantity__c = Actual_Quantity__c                 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Exception Reason to Missing on Create</fullName>
        <actions>
            <name>Set_Exception_Reason_to_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(Inventory_Item__c)), Inventory_Item__r.Model_Number__r.Serial_Indicator__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Exception Reason to Shortage</fullName>
        <actions>
            <name>Set_Exception_Reason_to_Shortage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Inventory_Item__r.Model_Number__r.Serial_Indicator__c = False, NOT(ISBLANK( Inventory_Item__c )), Actual_Quantity__c&gt;0,  Scanned_Quantity__c &lt; Actual_Quantity__c,      OR(        ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Scanning&quot;), ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Analyzing&quot;))      )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Exception Reason to Surplus</fullName>
        <actions>
            <name>Set_Exception_Reason_to_Surplus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(         OR(               ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Scanning&quot;),               ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Analyzing&quot;)),               ISBLANK(Other_Inventory_Item__c),               BLANKVALUE(Scanned_Quantity__c,0) &gt; BLANKVALUE(Actual_Quantity__c,0)                              )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Expected Value Number</fullName>
        <actions>
            <name>Set_Expected_Value_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Formula fields cannot be used in rollup summaries.Therefore, each formula field below has a corresponding open number field. This rule ensures that the number field equivalent is accurate so that subsequent rollup summary values are also accurate.</description>
        <formula>AND(          $RecordType.DeveloperName=&quot;NMD_Cycle_Count_Line_Item&quot;,           OR(               NOT(ISBLANK(Actual_Quantity__c)),               ISCHANGED(Actual_Quantity__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Value Matched Number</fullName>
        <actions>
            <name>Set_Surplus_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Value_Matched_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Formula fields cannot be used in rollup summaries.Therefore, each formula field below has a corresponding open number field. This rule ensures that the number field equivalent is accurate so that subsequent rollup summary values are also accurate.</description>
        <formula>AND(         RecordType.DeveloperName=&quot;NMD_Cycle_Count_Line_Item&quot;,          OR(               NOT(ISBLANK(Scanned_Quantity__c)),               ISCHANGED(Scanned_Quantity__c),                NOT(ISBLANK(Actual_Quantity__c)),               ISCHANGED(Actual_Quantity__c)             )         )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Value Not Lost Number</fullName>
        <actions>
            <name>Set_Value_Not_Lost_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Formula fields cannot be used in rollup summaries.Therefore, each formula field below has a corresponding open number field. This rule ensures that the number field equivalent is accurate so that subsequent rollup summary values are also accurate.</description>
        <formula>AND( RecordType.DeveloperName=&quot;NMD_Cycle_Count_Line_Item&quot;,  OR(        NOT(ISBLANK(Actual_Quantity__c)),       ISCHANGED(Actual_Quantity__c)       )       )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Value Not Matched Number</fullName>
        <actions>
            <name>Set_Value_Not_Matched_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Formula fields cannot be used in rollup summaries.Therefore, each formula field below has a corresponding open number field. This rule ensures that the number field equivalent is accurate so that subsequent rollup summary values are also accurate.</description>
        <formula>AND( RecordType.DeveloperName=&quot;NMD_Cycle_Count_Line_Item&quot;,  OR(        NOT(ISBLANK(Actual_Quantity__c)),       ISCHANGED(Actual_Quantity__c)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Actual Quantity</fullName>
        <actions>
            <name>Update_Actual_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Keeps the Actual_Quantity__c aligned with the parent Inventory Item&apos;s SAP Quantity.</description>
        <formula>AND( NOT(ISBLANK(Inventory_Item__c)), OR(  ISBLANK(Actual_Quantity__c),  Actual_Quantity__c != Inventory_Item__r.SAP_Quantity__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Current Quantity when Scanning</fullName>
        <actions>
            <name>Set_Current_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks to see if the Response Analysis is in the Scanning Status and an item has been scanned or entered.</description>
        <formula>And(       Cycle_Count_Response_Analysis__r.RecordType.DeveloperName = &apos;NMD_Response_Analysis&apos;,       ISPICKVAL(Cycle_Count_Response_Analysis__r.Status__c,&quot;Scanning&quot;),       NOT(ISBLANK(Scanned_Quantity__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Line Item Current Quantity</fullName>
        <actions>
            <name>Set_Current_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(       Cycle_Count_Response_Analysis__r.RecordType.DeveloperName = &apos;NMD_Response_Analysis&apos;,       OR(             NOT(ISBLANK(Inventory_Item__c )) ,             NOT(ISBLANK(Other_Inventory_Item__c ))       ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stolen and Lost Value Number</fullName>
        <actions>
            <name>Update_Stolen_and_Lost_Value_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Formula fields cannot be used in rollup summaries.Therefore, each formula field below has a corresponding open number field. This rule ensures that the number field equivalent is accurate so that subsequent rollup summary values are also accurate.</description>
        <formula>AND( $RecordType.DeveloperName=&quot;NMD_Cycle_Count_Line_Item&quot;,  OR(        NOT(ISBLANK(Stolen_and_Lost__c)),       ISCHANGED(Stolen_and_Lost__c)      ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Overall_Contract_Average</fullName>
        <field>Overall_UroPH_Score__c</field>
        <formula>ROUND((Consulting_Average_Form__c + Training_Education_Average_Form__c)/2, 0)</formula>
        <name>Populate Overall Contract Average UroPH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Overall_Contract_Average_Endo</fullName>
        <field>Overall_Endo_Score__c</field>
        <formula>ROUND((Consulting_Average_Endo__c + Training_Education_Average_Endo__c)/2, 0)</formula>
        <name>Populate Overall Contract Average Endo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate Overall Contract Average Endo</fullName>
        <actions>
            <name>Populate_Overall_Contract_Average_Endo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Overall Contract Average UroPH</fullName>
        <actions>
            <name>Populate_Overall_Contract_Average</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

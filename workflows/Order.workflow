<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_Implant_Form_Notice</fullName>
        <ccEmails>customerserviceanz@bsci.com</ccEmails>
        <description>ANZ Implant Form Notice</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/New_Implant_Form_Submission</template>
    </alerts>
    <alerts>
        <fullName>Notify_Order_Owner</fullName>
        <description>Notify Order Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Neuromod/Orders_Pending_SAP_ID</template>
    </alerts>
    <alerts>
        <fullName>Return_Order_Confirmed_Email</fullName>
        <description>Return Order Confirmed Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Neuromod/Return_Order_Confirmed_Email</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_AT_OR_DE</fullName>
        <ccEmails>amscsgermanyaustria@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = AT OR DE</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_AU_OR_NZ</fullName>
        <ccEmails>AMS-CustomerServiceAustralia@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = AU OR NZ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_ES_OR_PT</fullName>
        <ccEmails>amscsiberica@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = ES OR PT</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_FR_OR_BE</fullName>
        <ccEmails>amscsfrance@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = FR OR BE</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_GB</fullName>
        <ccEmails>amscsuk@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = GB</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_NL_OR_LU</fullName>
        <ccEmails>amscsnl@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = NL OR LU</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Order_Canceled_Country_US</fullName>
        <ccEmails>AMS-CustomerOps-Implantables@bsci.com</ccEmails>
        <ccEmails>AMS-CSLEADS-AMS@bsci.com</ccEmails>
        <description>Uro/PH - Loaner Kit Order Canceled, Country = US</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Cancelation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Shipping_Notification</fullName>
        <description>Uro/PH - Loaner Kit Shipping Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Shipping_Notification</template>
    </alerts>
    <alerts>
        <fullName>Uro_PH_Loaner_Kit_Submission_Notification</fullName>
        <description>Uro/PH - Loaner Kit Submission Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Uro_PH_Alternate_BSCI_Resource__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Uro_PH/Loaner_Kit_Submission_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activate_Order</fullName>
        <field>Status</field>
        <literalValue>Activated</literalValue>
        <name>Activate Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_NULL_PO_to_PO_to_Follow</fullName>
        <description>When the PO Number is changed to null, the system will populate it with &quot;PO to Follow&quot;</description>
        <field>PoNumber</field>
        <formula>CASE( $RecordType.DeveloperName ,&quot;Patient_Order&quot;,&quot;PO to Follow&quot;, &quot;Product_Order&quot;,&quot;CI Order&quot;, &quot;Transfer_Order&quot;, &quot;Rep to Rep&quot;,null)</formula>
        <name>Change NULL PO to PO to Follow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_App_Logs</fullName>
        <description>This will check Create App Logs to trigger the Order Submitted Process Builder to create an Application Log record.</description>
        <field>Create_App_Logs__c</field>
        <literalValue>1</literalValue>
        <name>Create App Logs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Indicate_Surgery_Failed</fullName>
        <field>Surgery_Failed__c</field>
        <literalValue>1</literalValue>
        <name>Indicate Surgery Failed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Sent_Return_Order_Email_true</fullName>
        <field>Sent_Return_Order_Email__c</field>
        <literalValue>1</literalValue>
        <name>Mark Sent Return Order Email = true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nullify_FSR_Completed_Date</fullName>
        <description>Changes FSR Completed to Null</description>
        <field>FSR_Completed__c</field>
        <formula>IF(ISPICKVAL(PRIORVALUE(Special_Processing__c),&quot;Complete&quot;),NULL,FSR_Completed__c)</formula>
        <name>Nullify FSR Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nullify_FSR_Field</fullName>
        <description>Nullifies FSR field</description>
        <field>FSR__c</field>
        <name>Nullify FSR Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Overdue</fullName>
        <description>Marks the Overdue Checkbox to True</description>
        <field>Is_Overdue__c</field>
        <literalValue>1</literalValue>
        <name>Order Overdue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO_is_Updated</fullName>
        <field>PO_Update_is_Sent__c</field>
        <literalValue>1</literalValue>
        <name>PO is Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Po_Date_Equals_Now</fullName>
        <description>When PO Number is changed, PO Date is set to the current date</description>
        <field>PoDate</field>
        <formula>TODAY()</formula>
        <name>Po Date Equals Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Method_to_Approval</fullName>
        <description>Set Order method to Approval when the criteria are met</description>
        <field>Order_Method__c</field>
        <literalValue>Approval</literalValue>
        <name>Set Order Method to Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Method_to_Automated</fullName>
        <description>Set Order Method to Automated when criteria are met</description>
        <field>Order_Method__c</field>
        <literalValue>Automated</literalValue>
        <name>Set Order Method to Automated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Stage_to_Declined</fullName>
        <field>Stage__c</field>
        <literalValue>Declined</literalValue>
        <name>Set Order Stage to Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Stage_to_Submitted</fullName>
        <field>Stage__c</field>
        <literalValue>Submitted</literalValue>
        <name>Set Order Stage to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PO_Number</fullName>
        <field>PoNumber</field>
        <formula>CASE(Procedure_Type__c,&quot;Revision&quot;, &quot;Revision&quot;, &quot;Explant&quot;,&quot;Explant&quot;,PoNumber)</formula>
        <name>Set PO Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Processing_Required</fullName>
        <field>Special_Processing__c</field>
        <literalValue>Processing Required</literalValue>
        <name>Set Processing Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Confirmed</fullName>
        <field>Stage__c</field>
        <literalValue>Confirmed</literalValue>
        <name>Set Stage to Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Pending</fullName>
        <field>Stage__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Stage to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Updating_Surgery</fullName>
        <field>Stage__c</field>
        <literalValue>Updating Surgery</literalValue>
        <name>Set Stage to Updating Surgery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_to_SAP_Date_to_now</fullName>
        <description>Set the date_Submitted_to_SAP__c field to system.now()</description>
        <field>date_Submitted_to_SAP__c</field>
        <formula>IF(ISBLANK(date_Submitted_to_SAP__c),NOW(),date_Submitted_to_SAP__c)</formula>
        <name>Set Submitted to SAP Date to now()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_to_SAP_to_TRUE</fullName>
        <description>Set the &quot;Submitted to SAP&quot; checkbox field to TRUE.</description>
        <field>is_Submitted_to_SAP__c</field>
        <literalValue>1</literalValue>
        <name>Set Submitted to SAP to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_to_FSR_Date_IsNow</fullName>
        <description>Sets &quot;Submitted to FSR&quot; to Now</description>
        <field>Submitted_to_FSR__c</field>
        <formula>NOW()</formula>
        <name>Submitted to FSR Date IsNow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Surgery_Submitted_Date_to_NOW</fullName>
        <field>Surgery_Submitted_Date__c</field>
        <formula>NOW()</formula>
        <name>Surgery Submitted Date to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Surgery_Submitted_to_True</fullName>
        <field>Surgery_Submitted__c</field>
        <literalValue>1</literalValue>
        <name>Surgery Submitted to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_block_to_Null</fullName>
        <field>Shared_Billing_Block__c</field>
        <name>Update Billing block to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Cancelled</fullName>
        <description>This Updates the field: Order.Stage__c to Cancelled 3 days after the Stage has been Declined</description>
        <field>Stage__c</field>
        <literalValue>Cancelled</literalValue>
        <name>Update Stage to Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_PH_Set_Order_Stage_to_Ordered</fullName>
        <description>Created for SFDC-1489</description>
        <field>Stage__c</field>
        <literalValue>Ordered</literalValue>
        <name>Uro/PH - Set Order Stage to Ordered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_PH_Set_Order_Status_to_Submitted</fullName>
        <description>Created for US2279.  Changes order status to &quot;Submitted&quot;</description>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Uro/PH - Set Order Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_PH_Update_Billing_block_to_56</fullName>
        <field>Shared_Billing_Block__c</field>
        <formula>&quot;56&quot;</formula>
        <name>Update Billing block to 56</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_PH_Update_Billing_block_to_57</fullName>
        <field>Shared_Billing_Block__c</field>
        <formula>&quot;57&quot;</formula>
        <name>Update Billing block to 57</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Remove_57_Block</fullName>
        <description>Created for SFDC-1489</description>
        <field>Shared_Billing_Block__c</field>
        <name>Uro/Ph - Remove 57 Block</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Set_Order_Stage_to_Shipped</fullName>
        <description>Created for SFDC-1489</description>
        <field>Stage__c</field>
        <literalValue>Shipped</literalValue>
        <name>Uro/Ph - Set Order Stage to Shipped</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>dummyAmountUpdate</fullName>
        <field>dummyAmount__c</field>
        <name>dummyAmountUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Message_to_SAP_Surgery_Record</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://gateway.bsci.com:4080/BscSOAPService/v1/SFDC/Surgery</endpointUrl>
        <fields>Id</fields>
        <fields>Type</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>integration.user@bsci.com</integrationUser>
        <name>Send Message to SAP Surgery Record</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Submitted_Order</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://gateway.bsci.com:4080/BscSOAPService/v1/SFDC/Order</endpointUrl>
        <fields>Id</fields>
        <fields>Type</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>integration.user@bsci.com</integrationUser>
        <name>Send Submitted Order</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Update_Order_Message_to_SAP</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://gateway.bsci.com:4080/BscSOAPService/v1/SFDC/OrderUpdate</endpointUrl>
        <fields>Id</fields>
        <fields>PoNumber</fields>
        <fields>Type</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>integration.user@bsci.com</integrationUser>
        <name>Send Update Order Message to SAP</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>3 Days Declined to Cancelled</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <description>When the Order Stage is Declined and three days has passed, the Order Stage will be changed to Cancelled.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Stage_to_Cancelled</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ANZ Implant Form Submission</fullName>
        <actions>
            <name>ANZ_Implant_Form_Notice</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Implant Form</value>
        </criteriaItems>
        <description>Created for SFDC-840. Alert that goes to customer service and implant form submitter when form is completed (order is created)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Activate Patient Order</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Confirmed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>notEqual</operation>
            <value>PO to Follow</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Activate_Order</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cancelled Test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <description>When the Order Stage is Declined and three days has passed, the Order Stage will be changed to Cancelled.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Stage_to_Cancelled</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Order.CreatedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Confirm Return Orders</fullName>
        <actions>
            <name>Set_Stage_to_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(         $RecordType.DeveloperName=&quot;Return_Order&quot;,         NOT(ISBLANK(Sales_Order_No__c)),         Count_of_Quantity_Match__c=Count_of_Order_Item__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mark PO Update Sent %3D TRUE</fullName>
        <actions>
            <name>PO_is_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an Order has a PO Number and has been sent to SAP,  set PO_Update_Is_Sent__c to true, if it is not currently true.</description>
        <formula>AND(   $RecordType.DeveloperName = &quot;Patient_Order&quot;,   PoNumber != &quot;PO to Follow&quot;,   ISCHANGED(is_Submitted_to_SAP__c),   is_Submitted_to_SAP__c = true,    NOT(PO_Update_is_Sent__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order Confirmation Not Received</fullName>
        <active>true</active>
        <description>If a Patient, Return, or Transfer is Submitted to SAP and the Order Items have not been confirmed an hour after Submitted to SAP Date, Stage=Pending, Special Processing=Processing Required, and Create App Logs = true</description>
        <formula>AND(         is_Submitted_to_SAP__c, NOT(IsFired__c) ,        OR(               AND(                       $RecordType.DeveloperName=&quot;Patient_Order&quot;,                       Count_of_Quantity_Match__c &lt;&gt; Count_of_Valid_Order_Items__c,                       OR(                             ISPICKVAL(Stage__c,&apos;Submitted&apos;),                             ISPICKVAL(Stage__c,&apos;In Process&apos;),                             ISPICKVAL(Stage__c,&apos;Billing Block&apos;))),                AND(                        OR(                              $RecordType.DeveloperName=&quot;Return_Order&quot;,                              $RecordType.DeveloperName=&quot;Transfer_Order&quot;),                        Count_of_Quantity_Match__c &lt;&gt; Count_of_Order_Item__c,                        NOT(ISPICKVAL(Stage__c,&quot;Confirmed&quot;))                        )           )    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Create_App_Logs</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Processing_Required</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Stage_to_Pending</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Order.date_Submitted_to_SAP__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Order Overdue</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Return Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.date_Submitted_to_SAP__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If Record Type = Return Order, then set the Is Overdue checkbox =True if Now() is Greater than Submitted to SAP Date/Time + 48 Hours and Stage=Submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Order_Overdue</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Order.date_Submitted_to_SAP__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Patient Order Status Management</fullName>
        <actions>
            <name>Notify_Order_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Pending SAP ID</value>
        </criteriaItems>
        <description>Send an email alert to the Patient Order Owner if the Stage = Pending SAP ID</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Product Order Confirmed</fullName>
        <actions>
            <name>Set_Stage_to_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Product Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Special_Processing__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Order record type = Product Order and Special Processing = Complete, update the Order.Stage = Confirmed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Return Order Confirmed</fullName>
        <actions>
            <name>Set_Order_Stage_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Return Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Special_Processing__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Order record type = Product Order and Special Processing = Complete, update the Order.Stage = Confirmed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Order Updates to SAP</fullName>
        <actions>
            <name>PO_is_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Update_Order_Message_to_SAP</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When a Patient Order&apos;s Stage is not new, and PO Number is changed and is not PO to Follow, the system will send an outbound message to update SAP with the salesforce ID of the record, the PO Number, and the Order Type</description>
        <formula>AND(                  $RecordType.DeveloperName = &quot;Patient_Order&quot;,                  is_Submitted_to_SAP__c,                  OR(                                  AND(                                                 PoNumber != &quot;PO to Follow&quot;,                                                  NOT(ISBLANK(Sales_Order_No__c )),                                                  OR(                                                                  ISCHANGED(Item_Last_Modified__c),                                                                  Order_Item_Last_Modified__c &lt;= NOW()                                                  ),                                                  NOT(ISPICKVAL(Stage__c, &quot;Confirmed&quot;)),                                                  NOT(PO_Update_is_Sent__c),                                                  Count_of_Sales_Order_Numbers__c = Count_of_Valid_Order_Items__c                                  ),                                  AND(                                                  ISCHANGED(PoNumber),                                                  PO_Update_is_Sent__c,                                                  Count_of_Sales_Order_Numbers__c = Count_of_Valid_Order_Items__c                                  ),                                  AND(                                                  NOT(PO_Update_is_Sent__c),                                                  ISPICKVAL(Stage__c, &quot;Confirmed&quot;),                                                  Count_of_Sales_Order_Numbers__c = Count_of_Valid_Order_Items__c                                  ),                                  AND(                                                 ISCHANGED(Item_Last_Modified__c),                                                  Item_Last_Modified__c = &quot;Send PO Now&quot;,                                                 NOT(PO_Update_is_Sent__c)                                 )                  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Outbound Message to Patient Order</fullName>
        <actions>
            <name>Set_Submitted_to_SAP_Date_to_now</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Submitted_to_SAP_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Submitted_Order</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Changes to this Workflow Rule may require changes in the following Rule:
Order.Set Stage to Confirmed</description>
        <formula>AND(        $RecordType.DeveloperName = &quot;Patient_Order&quot;,        ISPICKVAL(Stage__c, &quot;Submitted&quot;),        Count_of_Valid_Order_Items__c &gt; 0,        OR(NOT(Is_Exception__c),         Exception_Count__c = Count_of_Verified__c),        is_Submitted_to_SAP__c=FALSE )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Outbound Message to Surgery and Order</fullName>
        <actions>
            <name>Surgery_Submitted_Date_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Surgery_Submitted_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Message_to_SAP_Surgery_Record</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Updating Surgery</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Surgery_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends the Outbound Message to Surgery End Point</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Outbound Message to Transfer Order</fullName>
        <actions>
            <name>Set_Submitted_to_SAP_Date_to_now</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Submitted_to_SAP_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Submitted_Order</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transfer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.is_Submitted_to_SAP__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends the Outbound Message to the Order End Point for Transfer Orders when Order Stage = Submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Return Order Email</fullName>
        <actions>
            <name>Return_Order_Confirmed_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Mark_Sent_Return_Order_Email_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Return Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Stage__c</field>
            <operation>equals</operation>
            <value>Confirmed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Sales_Order_No__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Sent_Return_Order_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends the Return Order Email to Shipping Location Owner when the Sales Order Number is populated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage to Confirmed</fullName>
        <actions>
            <name>Set_Stage_to_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(      $RecordType.DeveloperName = &quot;Patient_Order&quot;,      Surgery_Submitted__c,       OR(          AND(              Count_of_Valid_Order_Items__c &gt; 0,               Count_of_Valid_Order_Items__c = Count_of_Sales_Order_Numbers__c,              Count_of_Valid_Order_Items__c = Count_of_Quantity_Match__c,              Count_of_Billing_Blocks__c = 0,              PoNumber != &quot;PO to Follow&quot;,              NOT(ISBLANK(PoNumber)),              NOT(ISPICKVAL(Special_Processing__c, &quot;Processing Required&quot;)),              OR(                  ISCHANGED(PoNumber),                  ISCHANGED(Special_Processing__c)              )          ),          AND(              Count_of_Valid_Order_Items__c = 0,              NOT(ISBLANK(Your_Reference__c)),              OR(                  ISCHANGED(Item_Last_Modified__c),                  ISCHANGED(Your_Reference__c)              )          )      )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage to Submitted</fullName>
        <actions>
            <name>Set_Order_Stage_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(       $RecordType.DeveloperName=&quot;Patient_Order&quot;,       Is_Exception__c=true,       NOT(ISBLANK(Your_Reference__c)),       ISPICKVAL(Special_Processing__c, &quot;Complete&quot;),       Count_of_Verified__c=Exception_Count__c,       Surgery_Submitted__c=True,  is_Submitted_to_SAP__c =False       )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage to Updating Surgery</fullName>
        <actions>
            <name>Set_Stage_to_Updating_Surgery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(       $RecordType.DeveloperName=&quot;Patient_Order&quot;,       Is_Exception__c=true,       ISBLANK(Your_Reference__c),       ISPICKVAL(Special_Processing__c, &quot;Complete&quot;),       Count_of_Verified__c=Exception_Count__c,       Surgery_Submitted__c=False,       Count_of_Order_Item__c &gt; 0       )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Submitted to FSR Date</fullName>
        <actions>
            <name>Nullify_FSR_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Nullify_FSR_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Submitted_to_FSR_Date_IsNow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evaluates whether or not Special Processing is Processing Required</description>
        <formula>AND(         OR(               $RecordType.DeveloperName=&quot;Patient_Order&quot;,               $RecordType.DeveloperName=&quot;Product_Order&quot;,               $RecordType.DeveloperName=&quot;Return_Order&quot;,               $RecordType.DeveloperName=&quot;Transfer_Order&quot;),               ISPICKVAL(Special_Processing__c,&quot;Processing Required&quot;)               )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submitted Order to SAP</fullName>
        <actions>
            <name>Set_Submitted_to_SAP_Date_to_now</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Submitted_to_SAP_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Submitted_Order</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Submitted Return Orders send outbound message to SAP</description>
        <formula>AND(         $RecordType.DeveloperName=&quot;Return_Order&quot;,          ISPICKVAL(Stage__c,&quot;Submitted&quot;),          is_Submitted_to_SAP__c=false         )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Surgery Not Updated</fullName>
        <active>true</active>
        <formula>AND(         $RecordType.DeveloperName=&quot;Patient_Order&quot;,         ISPICKVAL(Stage__c,&quot;Updating Surgery&quot;),          NOT(ISBLANK(Surgery_Submitted_Date__c)),         ISBLANK(Your_Reference__c)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Indicate_Surgery_Failed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Order.Surgery_Submitted_Date__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Order Method to Approval</fullName>
        <actions>
            <name>Set_Order_Method_to_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transfer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Transfer_Type__c</field>
            <operation>equals</operation>
            <value>Request Transfer</value>
        </criteriaItems>
        <description>Update the Order method to Approval when Order Record Type is Transfer Order and its &apos;Request Transfer&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Method to Automated</fullName>
        <actions>
            <name>Set_Order_Method_to_Automated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR 3</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transfer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Transfer_Type__c</field>
            <operation>equals</operation>
            <value>Initiate Transfer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Product Order</value>
        </criteriaItems>
        <description>Update Order Method to Automated when Order Owner is equal to Shipping Account&apos;s Owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update PO Date</fullName>
        <actions>
            <name>Po_Date_Equals_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the PO Number is changed and populated, the system will update the PO Date to today</description>
        <formula>AND( $RecordType.DeveloperName = &quot;Patient_Order&quot;, ISCHANGED( PoNumber ), NOT(ISBLANK( PoNumber )  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PO Number</fullName>
        <actions>
            <name>Change_NULL_PO_to_PO_to_Follow</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Po_Date_Equals_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the PO Number of a Patient Order is changed to null, the system shall populate it with &quot;PO to Follow&quot;</description>
        <formula>AND(  OR($RecordType.DeveloperName = &quot;Patient_Order&quot;,    $RecordType.DeveloperName = &quot;Product_Order&quot;,    $RecordType.DeveloperName = &quot;Transfer_Order&quot;),   ISBLANK( PoNumber )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Revision or Explant Orders</fullName>
        <actions>
            <name>Set_PO_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.TotalAmount</field>
            <operation>equals</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>equals</operation>
            <value>PO to Follow</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Procedure_Type__c</field>
            <operation>equals</operation>
            <value>Revision,Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Surgery_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Procedure Type= Explant, PO Number=PO to Follow, Order Amount=0, &amp; Stage =Confirmed, PO Number will update to Explant OR
If Procedure Type= Revision, PO Number=PO to Follow, Order Amount=0, Submitted to SAP=True ,and Stage =Confirmed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Submitted</fullName>
        <actions>
            <name>Set_Order_Stage_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Your_Reference__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.is_Submitted_to_SAP__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>After Surgery ID is received, this will update the Order to Submitted for Order Creation in SAP.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro PH Update BillingBlock field to 56</fullName>
        <actions>
            <name>Uro_PH_Update_Billing_block_to_56</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>equals</operation>
            <value>x</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.ShippingCountry</field>
            <operation>equals</operation>
            <value>US,GB</value>
        </criteriaItems>
        <description>SFDC-1489 : When POnumber equals X and shipping country is US/GB, then Billing block =  56</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uro PH Update BillingBlock field to 57</fullName>
        <actions>
            <name>Uro_PH_Update_Billing_block_to_57</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>notEqual</operation>
            <value>x</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Ship_Date__c</field>
            <operation>notEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.ShippingCountry</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>SFDC-1489 : When POnumber notequals X and shipping country is US and ship date not equals today, then Billing block = 57</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uro PH Update BillingBlock field to Null</fullName>
        <actions>
            <name>Update_Billing_block_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3) OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>notEqual</operation>
            <value>x</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Ship_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.ShippingCountry</field>
            <operation>notEqual</operation>
            <value>US,GB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.PoNumber</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>SFDC-1489 : When POnumber notequals X and  ship date equals today, OR shipping Country notequals US,UK OR PO = null,then Billing block = null</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPH - Loaner Kit Order Shipped</fullName>
        <actions>
            <name>Uro_PH_Loaner_Kit_Shipping_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Status</field>
            <operation>equals</operation>
            <value>Shipped</value>
        </criteriaItems>
        <description>Created for US2279.  Fires when a Uro/PH Loaner Kit Order status = &quot;Shipped&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPH - Loaner Kit Order Submitted</fullName>
        <actions>
            <name>Uro_PH_Loaner_Kit_Submission_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Status</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>Created for US2279. Fires when Uro/PH Loaner Kit Order status set to &quot;Submitted&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPH - Order Submitted to SAP</fullName>
        <actions>
            <name>Uro_PH_Set_Order_Stage_to_Ordered</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uro_PH_Set_Order_Status_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Shared_SAP_Order_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <description>Created for US2279.  Fires when SAP Order Number field is populated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPH - Shipping information added</fullName>
        <actions>
            <name>Uro_Ph_Set_Order_Stage_to_Shipped</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Tracking_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Created for US2279.  Fires when values courier AND tracking number are added</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPh - 57 Block</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Loaner Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Shared_Billing_Block__c</field>
            <operation>equals</operation>
            <value>57</value>
        </criteriaItems>
        <description>Created for SFDC-1489</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Uro_Ph_Remove_57_Block</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Order.Ship_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>

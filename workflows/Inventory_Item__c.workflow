<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Inventory_Is_Available</fullName>
        <description>Updates Inventory Disposition when Inventory Available Qty &gt;0</description>
        <field>Disposition__c</field>
        <literalValue>Available</literalValue>
        <name>Inventory Is Available</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inventory_in_Transit</fullName>
        <description>Sets the Disposition to Pending when Items are In Transit.</description>
        <field>Disposition__c</field>
        <literalValue>Pending</literalValue>
        <name>Inventory in Transit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inventory_is_Used</fullName>
        <field>Disposition__c</field>
        <literalValue>Used</literalValue>
        <name>Inventory is Used</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Inventory Is Available</fullName>
        <actions>
            <name>Inventory_Is_Available</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inventory_Item__c.Available_Quantity__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Updates the Disposition to Available when Inventory is Available. (Available QTY &gt;0)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inventory in Transit</fullName>
        <actions>
            <name>Inventory_in_Transit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inventory_Item__c.Available_Quantity__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory_Item__c.In_Transit__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Updates Inventory Disposition when Inventory Available Qty =0 and In Transit Qty &gt;0</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inventory is Used</fullName>
        <actions>
            <name>Inventory_is_Used</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Inventory_Item__c.Available_Quantity__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory_Item__c.In_Transit__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory_Item__c.In_Transit__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the Inventory Item Disposition to Used when items are no longer available. Available Qty=0</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

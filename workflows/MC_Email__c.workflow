<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NM_MC_Rep_Bounced</fullName>
        <description>NM MC Rep Bounced Email notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Neuromod/NM_MC_Bounce</template>
    </alerts>
    <alerts>
        <fullName>NM_MC_Rep_Unsubscribe_Notification</fullName>
        <description>NM MC Rep Unsubscribe Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Neuromod/NM_MC_Unsubscribe</template>
    </alerts>
    <rules>
        <fullName>MC Send Rep Notification</fullName>
        <actions>
            <name>NM_MC_Rep_Bounced</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>MC_Email__c.Event__c</field>
            <operation>equals</operation>
            <value>Bounce</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notContain</operation>
            <value>User</value>
        </criteriaItems>
        <description>Send Notification Emails to the Opportunity Rep on Bounce</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MC Send Rep Unsub</fullName>
        <actions>
            <name>NM_MC_Rep_Unsubscribe_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>MC_Email__c.Event__c</field>
            <operation>equals</operation>
            <value>Unsubscribe</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notContain</operation>
            <value>User</value>
        </criteriaItems>
        <description>Send Notification Emails to the Opportunity Rep on UnSubscribe</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

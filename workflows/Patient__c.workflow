<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NMD_BSN_Update</fullName>
        <ccEmails>tina.day@bsci.com</ccEmails>
        <ccEmails>BSN.Update@bsci.com</ccEmails>
        <description>BSN Update on Patient</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Neuromod/NMD_BSN_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>Blank_BSN_Update</fullName>
        <field>BSN_Update__c</field>
        <name>Blank BSN Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PainAreaTrackerUpdate</fullName>
        <field>Pain_Area_Tracking__c</field>
        <formula>IF(INCLUDES( Pain_Area__c , &quot;Lower Right Limb&quot;), &quot;Lower Right Limb&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;Lower Left Limb&quot;), &quot;Lower Left Limb&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;Low Back &amp; Right Limb&quot;), &quot;Low Back &amp; Right Limb&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;Low Back &amp; Left Limb&quot;), &quot;Low Back &amp; Left Limb&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;Low Back&quot;), &quot;Low Back&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;Low Back &amp; Both Lower Limbs&quot;), &quot;Low Back &amp; Both Lower Limbs&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;Other&quot;), &quot;Other&quot;, NULL) + BR() + 
IF(INCLUDES( Pain_Area__c , &quot;None&quot;), &quot;None&quot;, NULL)</formula>
        <name>PainAreaTrackerUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Submitted_to_SAP</fullName>
        <field>is_Submitted_to_SAP__c</field>
        <literalValue>0</literalValue>
        <name>Reset Submitted to SAP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Submitted_to_SAP_Date</fullName>
        <field>date_Submitted_to_SAP__c</field>
        <name>Reset Submitted to SAP Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_CUSTOMER_RECORD_TYPE</fullName>
        <description>Updates the Patient Record Type to &quot;Patient Customer&quot; when the SAP ID is populated.</description>
        <field>RecordTypeId</field>
        <lookupValue>Patient_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SET_CUSTOMER_RECORD_TYPE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_to_SAP_Date_to_now</fullName>
        <field>date_Submitted_to_SAP__c</field>
        <formula>NOW()</formula>
        <name>Set Submitted to SAP Date to now()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_to_SAP_to_TRUE</fullName>
        <field>is_Submitted_to_SAP__c</field>
        <literalValue>1</literalValue>
        <name>Set Submitted to SAP to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Email_Status_Opt_Out</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_Email_Status__c</field>
        <literalValue>Opt-Out</literalValue>
        <name>Email Status Opt-Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Mailing_Status_Opt_Out</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_Mailing_Status__c</field>
        <literalValue>Opt-Out</literalValue>
        <name>Mailing Status Opt-Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Phone_Status_Opt_out</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_Phone_Status__c</field>
        <literalValue>Opt-Out</literalValue>
        <name>Phone Status Opt-out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Update_ED_Treatment_to_No</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_ED_Treatment__c</field>
        <literalValue>No</literalValue>
        <name>Uro/Ph - Update ED Treatment to No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Update_ED_Treatment_to_Yes</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_ED_Treatment__c</field>
        <literalValue>Yes</literalValue>
        <name>Uro/Ph - Update ED Treatment to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Update_MC_Treatment_to_No</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_MC_Treatment__c</field>
        <literalValue>No</literalValue>
        <name>Uro/Ph - Update MC Treatment to No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Update_MC_Treatment_to_Yes</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_MC_Treatment__c</field>
        <literalValue>Yes</literalValue>
        <name>Uro/Ph - Update MC Treatment to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uro_Ph_Update_Opt_in_Date</fullName>
        <description>Created for SFDC-988</description>
        <field>Uro_Ph_Opt_in_Date__c</field>
        <formula>TODAY()</formula>
        <name>Uro/Ph - Update Opt-in Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_BSN_Update</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://gateway.bsci.com:4080/BscSOAPService/v1/SFDC/Patient</endpointUrl>
        <fields>Address_1__c</fields>
        <fields>City__c</fields>
        <fields>Country__c</fields>
        <fields>Id</fields>
        <fields>Patient_Date_of_Birth__c</fields>
        <fields>Patient_Email_Address__c</fields>
        <fields>Patient_First_Name__c</fields>
        <fields>Patient_Gender__c</fields>
        <fields>Patient_Last_Name__c</fields>
        <fields>Patient_Phone_Number__c</fields>
        <fields>State__c</fields>
        <fields>Zip__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>integration.user@bsci.com</integrationUser>
        <name>Send BSN Update</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>BSN Update</fullName>
        <actions>
            <name>NMD_BSN_Update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An email will be generated to BSN if the BSN Update field is changed and is not null.</description>
        <formula>AND(         $RecordType.DeveloperName =&apos;Patient_Customer&apos;,         ISCHANGED(BSN_Update__c),         BSN_Update__c != NULL,         BSN_Update__c != &apos;Request SAP ID&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PainAreaTracker</fullName>
        <actions>
            <name>PainAreaTrackerUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tracker pain_area history</description>
        <formula>ISCHANGED(Pain_Area__c)   /*ISBLANK(Pain_Area__c)*/</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Patient BSN Submit</fullName>
        <actions>
            <name>Set_Submitted_to_SAP_Date_to_now</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Submitted_to_SAP_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_BSN_Update</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patient__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.BSN_Update__c</field>
            <operation>equals</operation>
            <value>Request SAP ID</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.is_Submitted_to_SAP__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset SAP ID Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Patient__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.BSN_Update__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.SAP_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When a user selects &quot;Request SAP ID,&quot; the BSN update is populated with &quot;Request SAP ID&quot; which triggers an email to initiate the update on the Patient record. This rule will reset the field so that this process can be repeated in case of an error.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Blank_BSN_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Reset_Submitted_to_SAP</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Reset_Submitted_to_SAP_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>UPDATE_PATIENT_TO_CUSTOMER</fullName>
        <actions>
            <name>Blank_BSN_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_CUSTOMER_RECORD_TYPE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patient__c.SAP_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a Patient is created or edited, this rule will check to see if the SAP ID is populated. If it is, the Patient Record Type will be updated to &quot;Patient Customer.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPh - ED Treatment</fullName>
        <actions>
            <name>Uro_Ph_Update_ED_Treatment_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for SFDC-988</description>
        <formula>AND(       RecordType.DeveloperName  = &quot;Uro_PH_Men_s_Health&quot;,       NOT($Permission.Shared_Exclude_from_Validation_Rules),       OR(            NOT(ISPICKVAL(Uro_Ph_ED_Oral_Medications__c, &quot;&quot;)),            NOT(ISPICKVAL(Uro_Ph_Penile_Injections__c, &quot;&quot;)),            NOT(ISPICKVAL(Uro_Ph_Suppositories_MUSE__c, &quot;&quot;)),            NOT(ISPICKVAL(Uro_Ph_Vacuum_Erection_Device_VED__c, &quot;&quot;))        )            )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPh - MC Treatment</fullName>
        <actions>
            <name>Uro_Ph_Update_MC_Treatment_to_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for SFDC-988</description>
        <formula>AND(       RecordType.DeveloperName  = &quot;Uro_PH_Men_s_Health&quot;,       NOT($Permission.Shared_Exclude_from_Validation_Rules),       OR(            NOT(ISPICKVAL(Uro_Ph_MC_Oral_Medications__c, &quot;&quot;)),            NOT(ISPICKVAL(Uro_Ph_Pads_Diapers__c, &quot;&quot;)),            NOT(ISPICKVAL(Uro_Ph_Catheter__c, &quot;&quot;)),            NOT(ISPICKVAL(Uro_Ph_Penile_Clamp__c, &quot;&quot;))        )            )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPh - No ED Treatment</fullName>
        <actions>
            <name>Uro_Ph_Update_ED_Treatment_to_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for SFDC-988</description>
        <formula>AND(       RecordType.DeveloperName  = &quot;Uro_PH_Men_s_Health&quot;,       NOT($Permission.Shared_Exclude_from_Validation_Rules),       ISPICKVAL(Uro_Ph_ED_Oral_Medications__c, &quot;&quot;),       ISPICKVAL(Uro_Ph_Penile_Injections__c, &quot;&quot;),       ISPICKVAL(Uro_Ph_Suppositories_MUSE__c, &quot;&quot;),       ISPICKVAL(Uro_Ph_Vacuum_Erection_Device_VED__c, &quot;&quot;)           )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPh - No MC Treatment</fullName>
        <actions>
            <name>Uro_Ph_Update_MC_Treatment_to_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for SFDC-988</description>
        <formula>AND(       RecordType.DeveloperName  = &quot;Uro_PH_Men_s_Health&quot;,       NOT($Permission.Shared_Exclude_from_Validation_Rules),       ISPICKVAL(Uro_Ph_MC_Oral_Medications__c, &quot;&quot;),       ISPICKVAL(Uro_Ph_Pads_Diapers__c, &quot;&quot;),       ISPICKVAL(Uro_Ph_Catheter__c , &quot;&quot;),       ISPICKVAL(Uro_Ph_Penile_Clamp__c, &quot;&quot;)      )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uro%2FPh - Patient Opt-In</fullName>
        <actions>
            <name>Uro_Ph_Update_Opt_in_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Patient__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Uro/PH Men&apos;s Health</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.Uro_Ph_Email_Status__c</field>
            <operation>equals</operation>
            <value>Opt-In</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.Uro_Ph_Mailing_Status__c</field>
            <operation>equals</operation>
            <value>Opt-In</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.Uro_Ph_Phone_Status__c</field>
            <operation>equals</operation>
            <value>Opt-In</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patient__c.Uro_Ph_Opt_in_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Created for SFDC-988</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

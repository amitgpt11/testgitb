<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PI_Sample_Request_Approval_Email_Notification</fullName>
        <ccEmails>LPFI-Shared@bsci.com</ccEmails>
        <ccEmails>Larry.Davis@bsci.com</ccEmails>
        <description>PI Sample Request Approval Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Onbehalf_Of__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PI/PISampleRequest_EmailTemplate_Approved</template>
    </alerts>
    <alerts>
        <fullName>PI_Sample_Request_Rejection_Email_Notification</fullName>
        <ccEmails>LPFI-Shared@bsci.com</ccEmails>
        <ccEmails>Larry.Davis@bsci.com</ccEmails>
        <description>PI Sample Request Rejection Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Onbehalf_Of__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PI/PISampleRequest_EmailTemplate_Rejected</template>
    </alerts>
    <alerts>
        <fullName>PI_Sample_Request_Submission_Email_Notification</fullName>
        <ccEmails>QuincySalesSupport@bsci.com</ccEmails>
        <ccEmails>Larry.Davis@bsci.com</ccEmails>
        <description>PI Sample Request Submission Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Onbehalf_Of__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PI/PISampleRequest_EmailTemplate_Submitted</template>
    </alerts>
    <alerts>
        <fullName>UroPH_Speaker_Request_Approval_Email_Alert1</fullName>
        <description>UroPH Speaker Request Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mary.novak@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Uro_PH/UroPH_Speaker_Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>UroPH_Speaker_Request_Denied_Email_Alert</fullName>
        <description>UroPH Speaker Request Denied Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Uro_PH/UroPH_Speaker_Request_Denied</template>
    </alerts>
    <alerts>
        <fullName>UroPH_Speaker_Request_Email_Alert</fullName>
        <description>UroPH Speaker Request Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mary.novak@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Uro_PH/UroPH_Speaker_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>PI_Approval_Date_Update</fullName>
        <description>This field updates the Approved/Rejected Date field of Sample Request Form to today&apos;s date.</description>
        <field>PI_Approved_Rejected_Date__c</field>
        <formula>Today()</formula>
        <name>PI Approval Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PI_Rejected_Date_Update</fullName>
        <field>PI_Approved_Rejected_Date__c</field>
        <formula>Today()</formula>
        <name>PI Rejected Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PI_Sample_Product_Request_Rejection</fullName>
        <description>Pi Sample Product Request Status Filed Update Upon Rejection</description>
        <field>PI_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>PI Sample Product Request Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PI_Sample_Request_Submission</fullName>
        <description>Update Approval Status field to Submitted</description>
        <field>PI_Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>PI Sample Request Submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PI_Sample_Status_Field_Update_Approved</fullName>
        <description>When Approved - Update status field for PI Sample Product Approval form</description>
        <field>PI_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>PI Sample Status Field Update Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LAAC_Inventory_Verification_Reminder</fullName>
        <description>LAAC - Inventory Verification Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>LAAC_Lead_WCS__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Inventory_Verification_Reminder</template>
    </alerts>
    <alerts>
        <fullName>LAAC_Lead_WCS_Assignment_Notification</fullName>
        <description>LAAC - Lead WCS Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LAAC_Lead_WCS__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Patient_Case_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>LAAC_Lead_WCS_Post_45_Day_Post_Successful_Case_Reminder</fullName>
        <description>LAAC - Lead WCS Post 45 Day Post Successful Case Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>LAAC_Lead_WCS__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Post_Successful_Case_Reminder</template>
    </alerts>
    <alerts>
        <fullName>LAAC_Lead_WCS_Referring_Physician_Owner_45_Day_Post_Successful_Case_Reminder</fullName>
        <description>LAAC - Lead WCS/Referring Physician Owner 45 Day Post Successful Case Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Shared_Referring_Physician_Contact_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Post_Successful_Case_Reminder</template>
    </alerts>
    <alerts>
        <fullName>LAAC_Referring_Physician_Owner_Case_Failure_Notification</fullName>
        <description>LAAC - Referring Physician Owner Case Failure Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Shared_Referring_Physician_Contact_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Case_Failure_Notification</template>
    </alerts>
    <alerts>
        <fullName>LAAC_Referring_Physician_Owner_Successful_Case_Notification</fullName>
        <description>LAAC - Referring Physician Owner Successful Case Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Shared_Referring_Physician_Contact_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Successful_Case_Notification</template>
    </alerts>
    <alerts>
        <fullName>LAAC_WCS_Trainee_1_Assignment_Notification</fullName>
        <description>LAAC - WCS Trainee 1 Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LAAC_WCS_Trainee_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Patient_Case_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>LAAC_WCS_Trainee_2_Assignment_Notification</fullName>
        <description>LAAC - WCS Trainee 2 Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LAAC_WCS_Trainee_2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Patient_Case_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>LAAC_WCS_Trainee_3_Assignment_Notification</fullName>
        <description>LAAC - WCS Trainee 3 Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>LAAC_WCS_Trainee_3__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>LAAC/Patient_Case_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Rhythmia_Sr_RMS_Assignment_Notification</fullName>
        <description>Rhythmia - Sr RMS Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>USEP_Sr_RMS_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/USEP_Patient_Case_Assignment_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>AutoPopulateEndTime</fullName>
        <field>Shared_End_Date_Time__c</field>
        <formula>Shared_Start_Date_Time__c + (1/24)</formula>
        <name>AutoPopulateEndTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AutoUpdateEndTime</fullName>
        <actions>
            <name>AutoPopulateEndTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for US2493 to auto populate the End_Time.</description>
        <formula>AND( ISNEW(),  $Profile.Id = &apos;00eo0000000f0Ip&apos;/*Watchman Field User*/ )||   AND( (PRIORVALUE(Shared_Start_Date_Time__c) &lt;&gt; Shared_Start_Date_Time__c ),  $Profile.Id = &apos;00eo0000000f0Ip&apos;/*Watchman Field User*/ ) /*AND( NOT(ISBLANK(Shared_Start_Date_Time__c)), ISCHANGED(Shared_Start_Date_Time__c)  )*/</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LAAC - Case Failed%2C Implanting Physician %21%3D Referring Physician</fullName>
        <actions>
            <name>LAAC_Referring_Physician_Owner_Case_Failure_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created for US2014.  Fires if a case status is moved to Closed - Failure and the Implanting Physician and the Referring Physician are different</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, ISPICKVAL( Shared_Status__c , &quot;Closed - Failure&quot;), Shared_Referring_Physician__c  &lt;&gt;  Shared_Implanting_Physician__c, NOT( ISNULL(Shared_Referring_Physician__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LAAC - Case Successful%2C Implanting Physician %21%3D Referring Physician</fullName>
        <actions>
            <name>LAAC_Referring_Physician_Owner_Successful_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created for US2014.Fires When a case is marked as &quot;Closed - Success&quot; and the Implanting Physician is not equal to the Referring Physician.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, ISPICKVAL( Shared_Status__c , &quot;Closed - Success&quot;), Shared_Referring_Physician__c &lt;&gt; Shared_Implanting_Physician__c,  NOT(ISNULL(Shared_Referring_Physician__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LAAC_Lead_WCS_Referring_Physician_Owner_45_Day_Post_Successful_Case_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Shared_Patient_Case__c.Shared_Start_Date_Time__c</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LAAC - Case Successful%2C Implanting Physician %3D Referring Physician</fullName>
        <active>false</active>
        <description>Created for US2014.Fires When a case is marked as &quot;Closed - Success&quot; and the Implanting Physician is equal to the Referring Physician.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, ISPICKVAL( Shared_Status__c , &quot;Closed - Success&quot;), Shared_Referring_Physician__c = Shared_Implanting_Physician__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LAAC_Lead_WCS_Post_45_Day_Post_Successful_Case_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Shared_Patient_Case__c.Shared_Start_Date_Time__c</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LAAC - Future Case Scheduled</fullName>
        <active>false</active>
        <description>Created for US2014. Fires if a case is created or edited with a date greater than or equal to today.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, DATEVALUE(Shared_Start_Date_Time__c)   &gt;=  TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LAAC_Inventory_Verification_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Shared_Patient_Case__c.Shared_Start_Date_Time__c</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LAAC - Lead WCS Assigned</fullName>
        <actions>
            <name>LAAC_Lead_WCS_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Created for US2014.  Fires when a value for Lead WCS is added or changed.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, OR(AND(ISNEW(), NOT( ISBLANK( LAAC_Lead_WCS__c ))), ISCHANGED(LAAC_Lead_WCS__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LAAC - WCS Trainee 1 Assigned</fullName>
        <actions>
            <name>LAAC_WCS_Trainee_1_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Created for US2014.  Fires when a value for WCS Trainee 1 is added or changed.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, OR(AND(ISNEW(), NOT( ISBLANK(  LAAC_WCS_Trainee_1__c ))), ISCHANGED( LAAC_WCS_Trainee_1__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LAAC - WCS Trainee 2 Assigned</fullName>
        <actions>
            <name>LAAC_WCS_Trainee_2_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Created for US2014.  Fires when a value for WCS Trainee 2 is added or changed.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, OR(AND(ISNEW(), NOT( ISBLANK(  LAAC_WCS_Trainee_2__c ))), ISCHANGED( LAAC_WCS_Trainee_2__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LAAC - WCS Trainee 3 Assigned</fullName>
        <actions>
            <name>LAAC_WCS_Trainee_3_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Created for US2014.  Fires when a value for WCS Trainee 3 is added or changed.</description>
        <formula>AND(RecordType.DeveloperName  = &apos;Watchman_Patient_Case&apos;, OR(AND(ISNEW(), NOT( ISBLANK(  LAAC_WCS_Trainee_3__c ))), ISCHANGED( LAAC_WCS_Trainee_3__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>USEP_Rhythmia - Sr RMS Assigned Case Notification</fullName>
        <actions>
            <name>Rhythmia_Sr_RMS_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created for 2016 Q4 US1378  Notify Sr. RMS when assigned to Rhythmia case.</description>
        <formula>AND(OR( AND( ISNEW(),  NOT(ISBLANK(USEP_Sr_RMS_Name__c))), ISCHANGED(USEP_Sr_RMS_Name__c)), RecordType.DeveloperName=&apos;USEP_Rhythmia_Case&apos;, OR(USEP_Sr_RMS_Name__r.Profile.Name=&apos;US EP Clinical User&apos;, USEP_Sr_RMS_Name__r.Profile.Name=&apos;US EP Non Sales User&apos;, USEP_Sr_RMS_Name__r.Profile.Name =&apos;US EP Sales User&apos; )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

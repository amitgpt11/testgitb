<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_IsPatient</fullName>
        <description>Marks IsPatient True</description>
        <field>IsPatient__c</field>
        <literalValue>1</literalValue>
        <name>Check IsPatient</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_IsVirtualKit</fullName>
        <field>IsVirtualKit__c</field>
        <literalValue>1</literalValue>
        <name>Check IsVirtualKit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Rep_Initiated</fullName>
        <field>Rep_Initiated__c</field>
        <literalValue>1</literalValue>
        <name>Check Rep Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Lot_Number</fullName>
        <field>Lot_Number__c</field>
        <formula>Inventory_Source__r.Lot_Number__c</formula>
        <name>Copy Lot Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Material_Number_UPN</fullName>
        <field>Material_Number_UPN__c</field>
        <formula>Inventory_Source__r.Material_Number_UPN__c</formula>
        <name>Copy Material Number UPN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Model_Number</fullName>
        <field>Model_Number__c</field>
        <formula>Inventory_Source__r.Model_Number__r.Model_Number__c</formula>
        <name>Copy Model Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Serial_Number</fullName>
        <field>Serial_Number__c</field>
        <formula>Inventory_Source__r.Serial_Number__c</formula>
        <name>Copy Serial Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Indicate_Item_Not_Confirmed</fullName>
        <description>Checks the Not Confirmed Checkbox, which is the criteria used to fire the Order Confirmation Not Received Process Builder.</description>
        <field>Not_Confirmed__c</field>
        <literalValue>1</literalValue>
        <name>Indicate Item Not Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Indicate_Item_Partiallly_Confirmed</fullName>
        <field>Partially_Confirmed__c</field>
        <literalValue>1</literalValue>
        <name>Indicate Item Partially Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prepurchased_Items_are_Confirmed</fullName>
        <description>When the Your Reference is populated on a Patient Order then update OrderItems.Confirmed-Quantity_-c where the No Charge Reason = Pre purchased with the value from the Quantity field.</description>
        <field>Confirmed_Quantity_Number__c</field>
        <formula>Quantity</formula>
        <name>Prepurchased Items are Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Charge_Type_to_No_Charge</fullName>
        <field>Charge_Type__c</field>
        <literalValue>No Charge</literalValue>
        <name>Set Charge Type to No Charge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Charge_Type_to_Region</fullName>
        <field>Charge_Type__c</field>
        <literalValue>Region</literalValue>
        <name>Set Charge Type to Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Customer_Price_to_Zero</fullName>
        <field>Customer_Price__c</field>
        <formula>0</formula>
        <name>Set Customer Price to Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Default_Charge_Type</fullName>
        <field>Charge_Type__c</field>
        <literalValue>Center</literalValue>
        <name>Set Default Charge Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Item_Accepted_Date</fullName>
        <field>Item_Accepted_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Item Accepted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Reason_to_B07</fullName>
        <field>Order_Reason__c</field>
        <literalValue>B07</literalValue>
        <name>Set OrderItem Reason to B07</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Reason_to_E16</fullName>
        <field>Order_Reason__c</field>
        <literalValue>E16</literalValue>
        <name>Set OrderItem Reason to E16</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Reason_to_F05</fullName>
        <field>Order_Reason__c</field>
        <literalValue>F05</literalValue>
        <name>Set OrderItem Reason to F05</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Reason_to_G44</fullName>
        <field>Order_Reason__c</field>
        <literalValue>G44</literalValue>
        <name>Set OrderItem Reason to G44</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Type_to_KE</fullName>
        <field>Type__c</field>
        <literalValue>KE</literalValue>
        <name>Set OrderItem Type to KE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Type_to_Surgery</fullName>
        <field>Type__c</field>
        <literalValue>Surgery</literalValue>
        <name>Set OrderItem Type to Surgery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OrderItem_Type_to_ZKI</fullName>
        <field>Type__c</field>
        <literalValue>ZKI</literalValue>
        <name>Set OrderItem Type to ZKI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Products_External_ID</fullName>
        <field>External_ID__c</field>
        <formula>OrderId+&quot;-&quot;+
TEXT(Order_Item_Id__c)</formula>
        <name>Set Order Products External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Reason_to_Null</fullName>
        <field>Order_Reason__c</field>
        <name>Set Order Reason to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Unit_Price_to_Zero</fullName>
        <field>UnitPrice</field>
        <formula>0</formula>
        <name>Set Unit Price to Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Item_ID_from_SAP</fullName>
        <description>Update order item Id which is coming from SAP</description>
        <field>Order_Item_Id__c</field>
        <formula>VALUE(RIGHT(External_ID__c,  (LEN(External_ID__c)-FIND(&apos;-&apos; , External_ID__c))))</formula>
        <name>Update Order Item ID from SAP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check IsPatient</fullName>
        <actions>
            <name>Check_IsPatient</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks if the Order&apos;s record type=Patient on create</description>
        <formula>Order.RecordType.DeveloperName=&quot;Patient_Order&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Check IsVirtualKit</fullName>
        <actions>
            <name>Check_IsVirtualKit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.IsVirtualKit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Unit Price and Customer Price = $0 when No Charge Reason = Bundle</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check Rep Initiated</fullName>
        <actions>
            <name>Check_Rep_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Xfer_Type__c</field>
            <operation>equals</operation>
            <value>Rep Initiated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Inventory Source Values</fullName>
        <actions>
            <name>Copy_Lot_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Material_Number_UPN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Model_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Serial_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Some fields are copies of data and not formulas due to spanning limits.</description>
        <formula>NOT(ISBLANK(Inventory_Source__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order Confirmation Not Received</fullName>
        <active>false</active>
        <formula>AND(  Order.RecordType.DeveloperName&lt;&gt;&quot;Product_Order&quot;,  Submitted_to_SAP__c,  OR(  ISBLANK(Confirmed_Quantity_Number__c),  ISBLANK(Sales_Order_Number__c)   )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Indicate_Item_Not_Confirmed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>OrderItem.Order_Submitted__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Order Item Explanted or Trial</fullName>
        <actions>
            <name>Set_Customer_Price_to_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Unit_Price_to_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Order.RecordType.DeveloperName=&quot;Patient_Order&quot;, OR( Order.Opportunity.RecordType.DeveloperName=&quot;NMD_SCS_Trial_Implant&quot;,  Order.Opportunity.RecordType.DeveloperName=&quot;NMD_SCS_Trial&quot;,  Order.Opportunity.RecordType.DeveloperName=&quot;NMD_SCS_Implant&quot;, Order.Opportunity.RecordType.DeveloperName=&quot;NMD_SCS_Explant&quot;, Order.Opportunity.RecordType.DeveloperName=&quot;NMD_SCS_Revision&quot;), OR( ISPICKVAL(Item_Status__c, &quot;Explanted&quot;), ISPICKVAL(Item_Status__c, &quot;Field External - Trial&quot;), ISPICKVAL(No_Charge_Reason__c, &quot;Bundle&quot;), ISPICKVAL(No_Charge_Reason__c, &quot;Pre Purchase&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partial Confirmation Received</fullName>
        <actions>
            <name>Indicate_Item_Partiallly_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(        Order.RecordType.DeveloperName&lt;&gt;&quot;Product_Order&quot;,        Submitted_to_SAP__c,        NOT(ISBLANK(Confirmed_Quantity_Number__c)),        NOT(Confirmed_Quantity_Number__c=Quantity)             )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Item Accepted Date</fullName>
        <actions>
            <name>Set_Item_Accepted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OrderItem.Sales_Order_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prepurchased Items are Confirmed</fullName>
        <actions>
            <name>Prepurchased_Items_are_Confirmed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Patient Order has OrderItems where the No Charge Reason = Pre purchased, Confirmed_Quantity__c will be updated with the value from the Quantity field every time quantity is changed.</description>
        <formula>AND( Order.RecordType.DeveloperName=&quot;Patient_Order&quot;, ISPICKVAL(No_Charge_Reason__c,&quot;Pre Purchase&quot;), ISCHANGED(Quantity))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Charge Type to No Charge</fullName>
        <actions>
            <name>Set_Charge_Type_to_No_Charge</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Pre Purchase,Warranty Replacement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Surgery_Submitted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If No Charge Reason is set to Warranty or Pre-purchase, set to No Charge</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Charge Type to Region</fullName>
        <actions>
            <name>Set_Charge_Type_to_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Evaluation,Charge to Region</value>
        </criteriaItems>
        <description>If No Charge Reason is set to &quot;Charge to Region&quot; or &quot;Evaluation&quot; then update Charge Type to &quot;Region&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Default Charge Type</fullName>
        <actions>
            <name>Set_Default_Charge_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Charge_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sets the Charge Type to Center on create</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Reason to B07</fullName>
        <actions>
            <name>Set_OrderItem_Reason_to_B07</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Type__c</field>
            <operation>equals</operation>
            <value>KE</value>
        </criteriaItems>
        <description>If OrderItem.Type = ZKI and OrderItem.No Charge Reason = Charge to Region, then OrderItem.Order Reason = B07</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Reason to E16</fullName>
        <actions>
            <name>Set_OrderItem_Reason_to_E16</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Type__c</field>
            <operation>equals</operation>
            <value>ZKI</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Charge to Region</value>
        </criteriaItems>
        <description>If OrderItem.Type = ZKI and OrderItem.No Charge Reason = Charge to Region, then OrderItem.Order Reason = E16</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Reason to F05</fullName>
        <actions>
            <name>Set_OrderItem_Reason_to_F05</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Type__c</field>
            <operation>equals</operation>
            <value>ZKI</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Warranty Replacement</value>
        </criteriaItems>
        <description>If OrderItem.Type = ZKI and OrderItem.No Charge Reason = Warranty Replacement, then OrderItem.Order Reason = F05</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Reason to G44</fullName>
        <actions>
            <name>Set_OrderItem_Reason_to_G44</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Type__c</field>
            <operation>equals</operation>
            <value>ZKI</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Evaluation</value>
        </criteriaItems>
        <description>If OrderItem.Type = ZKI and OrderItem.No Charge Reason = Evaluation, then OrderItem.Order Reason = G44</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Reason to Null</fullName>
        <actions>
            <name>Set_Order_Reason_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Type__c</field>
            <operation>equals</operation>
            <value>Surgery</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Pre Purchase</value>
        </criteriaItems>
        <description>If OrderItem.Type = Surgery and OrderItem.No Charge Reason = Pre Purchase, then OrderItem.Order Reason = Null</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Type to KE</fullName>
        <actions>
            <name>Set_OrderItem_Type_to_KE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 AND 3 AND 4) OR 5 OR 6</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Item_Status__c</field>
            <operation>equals</operation>
            <value>Implanted,Used/Consumed</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Charge_Type__c</field>
            <operation>equals</operation>
            <value>Center</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>notEqual</operation>
            <value>Evaluation,Warranty Replacement,Charge to Region</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Bundle</value>
        </criteriaItems>
        <criteriaItems>
            <field>PricebookEntry.IsVirtualKit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Item Status = Implanted or Used/Consumed and Charge Type = Center, then Order Item.Type__c = KE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Type to Surgery</fullName>
        <actions>
            <name>Set_OrderItem_Type_to_Surgery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) OR 3</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Pre Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.Item_Status__c</field>
            <operation>equals</operation>
            <value>Explanted,Field External</value>
        </criteriaItems>
        <description>When Item Status = Explanted or Field External and Charge Type = Center, or when Item Status = Implanted, Explanted, Used/Consumed, Field External and Charge Type = No Charge and No Charge Reason = Pre Purchase, then setOrder item.Type __c to Surgery</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set OrderItem Type to ZKI</fullName>
        <actions>
            <name>Set_OrderItem_Type_to_ZKI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Order.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>OrderItem.No_Charge_Reason__c</field>
            <operation>equals</operation>
            <value>Evaluation,Warranty Replacement,Charge to Region</value>
        </criteriaItems>
        <description>A No Charge Reason of Charge to Region, Evaluation or Warranty will result in a ZKI.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Item Id SAP</fullName>
        <actions>
            <name>Update_Order_Item_ID_from_SAP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To update order Item ID of  order Products which are inserted from SAP</description>
        <formula>AND(Order.RecordType.DeveloperName  =  &apos;Uro_PH_Loaner_Kit&apos;,  		NOT(ISNULL(External_ID__c)),NOT(ISBLANK(External_ID__c)),  		(Text(Order_Item_Id__c) &lt;&gt; RIGHT(External_ID__c,  (LEN(External_ID__c)-FIND(&apos;-&apos; , External_ID__c)))) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

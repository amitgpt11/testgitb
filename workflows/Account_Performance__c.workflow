<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Performance_RT_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CRM_EP_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account Performance RT Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Last_Updated</fullName>
        <field>Sales_Data_Last_Updated__c</field>
        <formula>NOW()</formula>
        <name>Date Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Div_Performance_value</fullName>
        <field>Div_Performance_Value__c</field>
        <formula>text(Division__c)  &amp;  CY__c &amp; Facility_Name__r.Id</formula>
        <name>Set Div Performance value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QBR_Last_Changed_to_TODAY</fullName>
        <field>QBR_Last_Changed__c</field>
        <formula>TODAY()</formula>
        <name>Set QBR Last Changed to TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Situational_Analysis_Timestamp</fullName>
        <field>Situation_Analysis__c</field>
        <formula>br()+TEXT(TODAY()) &amp; &quot; &quot; +TEXT(FLOOR(MOD( NOW()- ($System.OriginDateTime - 2/24),1) * 24 ))+ &quot;:&quot; +LEFT(RIGHT(TEXT(NOW()),6),2)+ BR() 
+ &quot; &quot;+ Situation_Analysis__c</formula>
        <name>Situational Analysis Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPDATE_PREVIOUS_POTENTIAL</fullName>
        <field>Previous_Potential__c</field>
        <formula>PRIORVALUE(Revenue_Potential__c)</formula>
        <name>UPDATE PREVIOUS POTENTIAL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Revenue_Potential</fullName>
        <field>Revenue_Potential__c</field>
        <formula>Overall_Product_Potential__c</formula>
        <name>Update Revenue Potential</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Revenue_Potential_with_Totals</fullName>
        <field>Revenue_Potential__c</field>
        <formula>Total_Potential_Calculated__c +  Total_Potential_Estimated__c</formula>
        <name>Update Revenue Potential with Totals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Situational_Analysis_Timestamp</fullName>
        <description>Used to set Date/Timestamp to Updated field for Situational Analysis for Europe</description>
        <field>Updated__c</field>
        <formula>NOW()</formula>
        <name>Update Situational Analysis Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>111</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account_Performance__c.Flagship__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Date%2FTimestamp Situational Analysis</fullName>
        <actions>
            <name>Situational_Analysis_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to add a date/timestamp to text entries made by the user to the Situational Analysis field on the Account Performance record</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EU Update Record Type</fullName>
        <actions>
            <name>Account_Performance_RT_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Performance__c.Division__c</field>
            <operation>equals</operation>
            <value>CRM,EP</value>
        </criteriaItems>
        <description>US2614 - DRAFT US-3983 - CRM/EP Potentials &amp; Procedures Review</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Previous Potential</fullName>
        <actions>
            <name>UPDATE_PREVIOUS_POTENTIAL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged( Revenue_Potential__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Data Update</fullName>
        <actions>
            <name>Date_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(  Revenue_Amount__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Situational Analysis Timestamp</fullName>
        <actions>
            <name>Update_Situational_Analysis_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update Date/Timestamp field for Situational Analysis on Account Performance record for Europe</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update QBR Last Changed</fullName>
        <actions>
            <name>Set_QBR_Last_Changed_to_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This field gets updated when the QBR field on Account Performance is modified.</description>
        <formula>ISCHANGED( QBR__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Revenue Potential</fullName>
        <actions>
            <name>UPDATE_PREVIOUS_POTENTIAL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Revenue_Potential_with_Totals</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Total Potentials are changed, the Revenue Potential and Previous Potentials are updated.</description>
        <formula>ISCHANGED( Total_Potential_Calculated__c ) ||  ISCHANGED( Total_Potential_Estimated__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Tiering on Account - PI</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account_Performance__c.Division__c</field>
            <operation>equals</operation>
            <value>PI</value>
        </criteriaItems>
        <description>this rule updates the Tiering fields on the Account record according to changes on the Account Performance records (by Division)

(the tiering fields are hidding on the Account layouts and are only used for reporting)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

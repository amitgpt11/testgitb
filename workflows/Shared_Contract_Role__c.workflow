<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_30_days_before_Contract_End</fullName>
        <description>ANZ 90 days before Contract End</description>
        <protected>false</protected>
        <recipients>
            <field>Shared_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/ANZ_Alert_30_days_before_Contract_End_Date</template>
    </alerts>
</Workflow>

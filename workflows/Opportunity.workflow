<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_for_demo_on_opportunity_for_Capital_Equipment_Evaluation</fullName>
        <ccEmails>Evan.Kasemeotes@bsci.com</ccEmails>
        <ccEmails>Genevieve.Hagerty@bsci.com</ccEmails>
        <ccEmails>deborah.burton@bsci.com</ccEmails>
        <ccEmails>robert.kinkade@bsci.com</ccEmails>
        <ccEmails>Erin.Sanor@bsci.com</ccEmails>
        <description>Email for demo on opportunity for Capital Equipment Evaluation</description>
        <protected>false</protected>
        <recipients>
            <recipient>Capital Specialist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Customer_Service_Team</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Demo_for_Capital_Equipment_Evaluation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Advance_Stage</fullName>
        <description>Advance the StageName to the value below it.</description>
        <field>StageName</field>
        <name>Advance Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>NextValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_field_update_to_Closed_lost</fullName>
        <description>This is to change the Trial opportunity Stage to &quot;Lost (Closed Lost)&quot; a
As per US2475, Refer Q3 release</description>
        <field>StageName</field>
        <literalValue>Closed Lost(Trial)</literalValue>
        <name>Change field update to Closed lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_It</fullName>
        <field>Name</field>
        <formula>&quot;Foo&quot;</formula>
        <name>Check It</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Three_Day</fullName>
        <description>This field update is working to help trigger the SAP Procedure Physician Reminder and SAP Trial Physician Reminder</description>
        <field>Three_Day_Trial__c</field>
        <literalValue>1</literalValue>
        <name>Check Three Day Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Three_Day_Procedure</fullName>
        <field>Three_Day_Procedure__c</field>
        <literalValue>1</literalValue>
        <name>Check Three Day Procedure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Decrease_Stage</fullName>
        <description>Decreases the StageName value to the value above.</description>
        <field>StageName</field>
        <name>Decrease Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>PreviousValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Europe_Capital_Oppty_Prob_Fix</fullName>
        <description>US2170</description>
        <field>Probability</field>
        <formula>PRIORVALUE( Probability )</formula>
        <name>Europe Capital Oppty Prob Fix</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LAAC_Clear_Postponed_by</fullName>
        <description>Clears the value in the Postponed by picklist.</description>
        <field>LAAC_Postponed_by__c</field>
        <name>LAAC Clear Postponed by</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LAAC_Update_Close_Date_when_Postponed_by</fullName>
        <description>Updates close date on Watchman New Business Opportunities when the opportunity has a sub stage of Contract Postponed.</description>
        <field>CloseDate</field>
        <formula>IF(ISPICKVAL( LAAC_Postponed_by__c , &apos;3 months&apos;), CloseDate + 90,
IF(ISPICKVAL( LAAC_Postponed_by__c , &apos;6 months&apos;),CloseDate + 180,
IF(ISPICKVAL( LAAC_Postponed_by__c , &apos;9 months&apos;), CloseDate + 270, CloseDate + 0)))</formula>
        <name>Update Close Date when Postponed by</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NMD_Cool_Oppy_Name</fullName>
        <description>Updates the NMD Cool Opportunity Name with NMD Cool + Physician</description>
        <field>Name</field>
        <formula>&quot;NMD Cool-&quot;+ Procedure_Physician__r.FirstName + &quot; &quot;+Procedure_Physician__r.LastName</formula>
        <name>NMD Cool Oppy Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Gate_Keeper_Stage_2</fullName>
        <field>Shared_Opportunity_Gate_Keeper_Stage_2__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Gate Keeper Stage 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Gate_Keeper_Stage_3</fullName>
        <field>Shared_Opportunity_Gate_Keeper_Stage_3__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Gate Keeper Stage 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Gate_Keeper_Stage_4</fullName>
        <field>Shared_Opportunity_Gate_Keeper_Stage_4__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Gate Keeper Stage 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Gate_Keeper_Stage_5</fullName>
        <field>Shared_Opportunity_Gate_Keeper_Stage_5__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Gate Keeper Stage 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Gate_Keeper_Stage_6</fullName>
        <field>Shared_Opportunity_Gate_Keeper_Stage_6__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Gate Keeper Stage 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Gate_Keeper_Stage_7</fullName>
        <field>Shared_Opportunity_Gate_Keeper_Stage_7__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Gate Keeper Stage 7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Failed_Date</fullName>
        <description>The system will track the date/time Trial Status set to Failed</description>
        <field>Failed_Date__c</field>
        <formula>IF( ISNULL( Failed_Date__c ) ,  NOW() , null)</formula>
        <name>Populate Failed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Inconclusive_Date</fullName>
        <description>The system will track the date/time Trial Status set to Inconclusive</description>
        <field>Inconclusive_Date__c</field>
        <formula>IF( ISNULL( Inconclusive_Date__c ) ,  NOW() , null)</formula>
        <name>Populate Inconclusive Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ANZ_Opportunity_name</fullName>
        <description>Set ANZ Opportunity name</description>
        <field>Name</field>
        <formula>LEFT( Patient__r.Patient_First_Name__c , 2)&amp;
 &quot;.&quot; &amp;
 LEFT(  Patient__r.Patient_Last_Name__c , 3)&amp;
 &quot;-&quot;&amp;
 RecordType.Name
&amp;
&quot;-&quot;
&amp;
TEXT(DATEVALUE(CreatedDate))</formula>
        <name>Set ANZ Opportunity name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Actual_Trial_Date</fullName>
        <description>Scheduled Trial should update Actual Trial Date.</description>
        <field>Actual_Trial_Date__c</field>
        <formula>DATEVALUE(Scheduled_Trial_Date_Time__c)</formula>
        <name>Set Actual Trial Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_to_Actual_Trial</fullName>
        <description>If Scheduled procedure date is deleted, then the close date will revert to Actual Trial Date</description>
        <field>CloseDate</field>
        <formula>Actual_Trial_Date__c</formula>
        <name>Set Close Date to Actual Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_to_Procedure</fullName>
        <description>The Scheduled procedure date will update the close date with the Schedueld procedure date when Scheduled Procedure is not null</description>
        <field>CloseDate</field>
        <formula>DATEVALUE(Scheduled_Procedure_Date_Time__c)</formula>
        <name>Set Close Date to Procedure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_to_Trial</fullName>
        <description>The Scheduled Trial Date will update the close date, unless the scheduled procedure date is populated.</description>
        <field>CloseDate</field>
        <formula>DATEVALUE(Scheduled_Trial_Date_Time__c)</formula>
        <name>Set Close Date to Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_to_Now_30</fullName>
        <field>CloseDate</field>
        <formula>NOW() + 30</formula>
        <name>Set Closed to Now+30</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_to_Today_30</fullName>
        <field>CloseDate</field>
        <formula>NOW() + 30</formula>
        <name>Set Closed to Today+30</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Pull_Date_to_NOW</fullName>
        <field>Lead_Pull_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Lead Pull Date to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_National_Tiering_on_Opportunity</fullName>
        <field>National_Tiering__c</field>
        <formula>IF ( $User.Division = &quot;CRM&quot;, Account.National_Tiering_CRM__c ,
(IF ($User.Division = &quot;EP&quot;, Account.National_Tiering_EP__c ,
(IF ($User.Division = &quot;NC&quot;, Account.National_Tiering_IC__c ,
(IF ($User.Division = &quot;PI&quot;, Account.National_Tiering_PI__c ,
(IF ($User.Division = &quot;U/WH&quot;, Account.National_Tiering_U_WH__c ,&quot;&quot;)))))))))</formula>
        <name>Set National Tiering on Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Close_Date</fullName>
        <field>Opportunity_Close_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Opportunity Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Stage_to_Cancelled_NMD</fullName>
        <description>Set Opportunity Stage to Cancelled(Lost) for NMD Trial. Refer US2452 Q3 Release</description>
        <field>StageName</field>
        <literalValue>Cancelled (Lost)</literalValue>
        <name>Set Opportunity Stage to Cancelled NMD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_name</fullName>
        <description>Set Opportunity Name as: Trialing Physician.Last Name +&quot;/&quot;+  Procedure Physician.Last name+ &quot;-&quot; +Patient First Name (Left 2 characters)+&quot; .&quot;+Patient Last name (left 3 characters)+ &quot;- &quot;+ Record Type Name (Trial/Implant)</description>
        <field>Name</field>
        <formula>IF(RecordType.DeveloperName = &quot;NMD_SCS_Implant&quot;,  Related_Opportunity__r.Trialing_Physician__r.LastName, Trialing_Physician__r.LastName) &amp; 
 &quot;/&quot; &amp;
 Procedure_Physician__r.LastName &amp;
 &quot;-&quot; &amp;
 LEFT( Patient__r.Patient_First_Name__c , 2)&amp;
 &quot;.&quot; &amp;
 LEFT(  Patient__r.Patient_Last_Name__c , 3)&amp;
 &quot;-&quot;&amp;
 RecordType.Name</formula>
        <name>Set Opportunity name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_name_for_Implant</fullName>
        <description>Set Opportunity Name as: Procedure Physician.Last name+ &quot;-&quot; +Patient First Name (Left 2 characters)+&quot; .&quot;+Patient Last name (left 3 characters)+ &quot;- &quot;+ Record Type Name (Implant)</description>
        <field>Name</field>
        <formula>Procedure_Physician__r.LastName &amp;
&quot;-&quot; &amp;
LEFT( Patient__r.Patient_First_Name__c , 2)&amp;
&quot;.&quot; &amp;
LEFT( Patient__r.Patient_Last_Name__c , 3)&amp;
&quot;-&quot;&amp;
RecordType.Name</formula>
        <name>Set Opportunity name for Implant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_name_for_Trial</fullName>
        <description>Set Opportunity Name as: Trialing Physician.Last Name +&quot;/&quot;+ &quot;-&quot; +Patient First Name (Left 2 characters)+&quot; .&quot;+Patient Last name (left 3 characters)+ &quot;- &quot;+ Record Type Name (Trial)</description>
        <field>Name</field>
        <formula>Trialing_Physician__r.LastName &amp; 
&quot;/&quot; &amp;
&quot;-&quot; &amp;
LEFT( Patient__r.Patient_First_Name__c , 2)&amp;
&quot;.&quot; &amp;
LEFT( Patient__r.Patient_Last_Name__c , 3)&amp;
&quot;-&quot;&amp;
RecordType.Name</formula>
        <name>Set Opportunity name for Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_stage_to_candidate</fullName>
        <field>StageName</field>
        <literalValue>Candidate (Trial implant)</literalValue>
        <name>Set Opportunity stage to candidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Originating_field_Rep</fullName>
        <description>Update Originating Rep with Opportunity Owner as long as the stage&lt;&gt;Closed Won or Closed Lost.</description>
        <field>Originating_Rep__c</field>
        <formula>Owner.Full_Name__c</formula>
        <name>Set Originating field Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PO_Time_Stamp</fullName>
        <field>PO_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Set PO Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Procedure_Stage_Time_Stamp</fullName>
        <description>Set Procedure Stage Time Stamp when Opportunity moves to Scheduled Explant or Revision</description>
        <field>Procedure_Stage_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Set Procedure Stage Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Schedule_Trial_Stage_Time_Stamp</fullName>
        <description>Set Schedule Trial Stage Time Stamp when the Opportunity stage moves to Schedule Trial</description>
        <field>Schedule_Trial_Stage_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Set Schedule Trial Stage Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_To_Cancel</fullName>
        <field>StageName</field>
        <literalValue>Cancelled (Lost)</literalValue>
        <name>Set Stage To Cancel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Cancelled</fullName>
        <description>Set Stage to cancelled when the  No Longer Opportunity field is updated to Cancelled
Denial/Health Issues/Cost/Duplicate</description>
        <field>StageName</field>
        <literalValue>Cancelled (Lost)</literalValue>
        <name>Set Stage to Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Candidate</fullName>
        <field>StageName</field>
        <literalValue>Candidate (Trial implant)</literalValue>
        <name>Set Stage to Candidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Closed</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Set Stage to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Complete_NMD_trial</fullName>
        <description>Set Stage to Complete(Closed won) . As per US 2452 Q3 release</description>
        <field>StageName</field>
        <literalValue>Complete (Closed Won)</literalValue>
        <name>Set Stage to Complete NMD trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Current_Trial</fullName>
        <field>StageName</field>
        <literalValue>Current Trial</literalValue>
        <name>Set Stage to Current Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Current_Trial_for_NMD_Trial</fullName>
        <description>Set Stage to Current Trial. As per US 2452 Q3 release</description>
        <field>StageName</field>
        <literalValue>Current Trial</literalValue>
        <name>Set Stage to Current Trial for NMD Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Implant_for_NMD_Implant</fullName>
        <description>Set Stage to Implant for NMD Implant, Refer US2453 Q3 Release</description>
        <field>StageName</field>
        <literalValue>Implant</literalValue>
        <name>Set Stage to Implant for NMD Implant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Open</fullName>
        <field>StageName</field>
        <literalValue>Open</literalValue>
        <name>Set Stage to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Procedure_Candidate</fullName>
        <description>Set Stage to  Procedure Candidate when criteria is met</description>
        <field>StageName</field>
        <literalValue>Procedure Candidate (Revision/Explant)</literalValue>
        <name>Set Stage to  Procedure Candidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Scheduled_trial_NMD_Trial</fullName>
        <description>Set Stage to Scheduled trial. As per US 2452 Q3 release</description>
        <field>StageName</field>
        <literalValue>Scheduled Trial</literalValue>
        <name>Set Stage to Scheduled trial- NMD Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Trial_Complete_NMD_Trial</fullName>
        <description>Set Stage to Trial complete. As per US 2452 Q3 release</description>
        <field>StageName</field>
        <literalValue>Trial Complete</literalValue>
        <name>Set Stage to Trial Complete- NMD Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_candidate_SCS_Trial</fullName>
        <description>Set/Update Opportunity Stage to Candidate</description>
        <field>StageName</field>
        <literalValue>Candidate (Trial implant)</literalValue>
        <name>Set Stage to candidate SCS Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Trial_PO_Time_Stamp</fullName>
        <field>Trial_PO_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Set Trial PO Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Trial_Status</fullName>
        <field>Trial_Status__c</field>
        <literalValue>Successful</literalValue>
        <name>Set Trial Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Trial_Status_Date</fullName>
        <description>Set the Trial Status Date when the criteria are met</description>
        <field>Trial_Status_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Trial Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_stage_to_Closed_Lost_Trail</fullName>
        <description>This will set stage to Lost Closed Lost (Trial) after 160 days</description>
        <field>StageName</field>
        <literalValue>Closed Lost(Trial)</literalValue>
        <name>Set stage to Closed Lost (Trail)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_trial_Status_to_OMG</fullName>
        <field>Trial_Status__c</field>
        <literalValue>OMG</literalValue>
        <name>Set trial Status to OMG</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Shared_Set_Close_Date_To_Today</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Set Close Date To Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Six_Month_Inconclusive_to_Lost</fullName>
        <field>StageName</field>
        <literalValue>Lost (Closed Lost)</literalValue>
        <name>Six Month Inconclusive to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Six_Months_Inconclusive_to_Lost</fullName>
        <description>If Stage = Current Trial and After 6 months from Inconclusive date the system will set the opportunity to Lost (closed lost)</description>
        <field>StageName</field>
        <literalValue>Lost (Closed Lost)</literalValue>
        <name>Six Months Inconclusive to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sixth_Month_Inconclusive_to_Lost</fullName>
        <description>If Stage = Current Trial and After 6 months from Inconclusive date the system will set the opportunity to Lost (closed lost)</description>
        <field>StageName</field>
        <literalValue>Lost (Closed Lost)</literalValue>
        <name>Sixth Month Inconclusive to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Complete</fullName>
        <description>System will update Stage = Complete when Stage =Explant or Revision, Actual Procedure Date &lt;=Today and PO is populated</description>
        <field>StageName</field>
        <literalValue>Complete (Closed Won)</literalValue>
        <name>Stage Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Test</fullName>
        <field>StageName</field>
        <literalValue>Lost (Closed Lost)</literalValue>
        <name>Test</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Third_Month_Failed_to_Lost</fullName>
        <description>If Stage = Current Trial and After 3 months from Failed date the system will set the opportunity to Lost (closed lost)</description>
        <field>StageName</field>
        <literalValue>Lost (Closed Lost)</literalValue>
        <name>Third Month Failed to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPDATE_SCHEDULED_PROCEDURE</fullName>
        <field>Scheduled_Procedure_Date__c</field>
        <formula>DATEVALUE( Scheduled_Procedure_Date_Time__c )</formula>
        <name>UPDATE_SCHEDULED_PROCEDURE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPDATE_SCHEDULED_TRIAL</fullName>
        <description>This field update sets the Scheduled_Trial_Date__c equal to the user&apos;s input into the SCHEDULED_TRIAL_DATE_TIME__C field</description>
        <field>Scheduled_Trial_Date__c</field>
        <formula>DATEVALUE( Scheduled_Trial_Date_Time__c )</formula>
        <name>UPDATE_SCHEDULED_TRIAL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Procedure_Date</fullName>
        <description>Update the Actual Procedure Date with the Scheduled Procedure Date every time Scheduled Procedure Date is populated/edited.</description>
        <field>Actual_Procedure_Date__c</field>
        <formula>DATEVALUE(Scheduled_Procedure_Date_Time__c)</formula>
        <name>Update Actual Procedure Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amount</fullName>
        <field>Amount</field>
        <formula>Trial_Revenue__c + Procedure_Revenue__c</formula>
        <name>Update Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source</fullName>
        <description>Update Lead Source when Criteria are met</description>
        <field>LeadSource</field>
        <literalValue>OMG</literalValue>
        <name>Update Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Name_for_RFP_RFI_type</fullName>
        <description>Will be used to update the Opportunity Name with the following convention

Mailing Name - Division - Product Group</description>
        <field>Name</field>
        <formula>Account.Mailing_Name__c &amp; &quot; - &quot; &amp; Division_Concatenate__c  &amp; &quot; - &quot; &amp;  Product_Group_Concatenate__c</formula>
        <name>Update Opportunity Name for RFP_RFI type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Explant</fullName>
        <field>StageName</field>
        <literalValue>Explant</literalValue>
        <name>Update Stage to Explant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Implant</fullName>
        <field>StageName</field>
        <literalValue>Implant</literalValue>
        <name>Update Stage to Implant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Revision</fullName>
        <description>Updates Stage to &apos;Revision&apos;</description>
        <field>StageName</field>
        <literalValue>Revision</literalValue>
        <name>Update Stage to Revision</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Scheduled_Explant</fullName>
        <field>StageName</field>
        <literalValue>Scheduled Explant</literalValue>
        <name>Update Stage to Scheduled Explant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Scheduled_Implant</fullName>
        <field>StageName</field>
        <literalValue>Scheduled Implant</literalValue>
        <name>Update Stage to Scheduled Implant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Scheduled_Revision</fullName>
        <description>Updates stage to &apos;Scheduled Revision&apos;</description>
        <field>StageName</field>
        <literalValue>Scheduled Revision</literalValue>
        <name>Update Stage to Scheduled Revision</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Scheduled_Trial</fullName>
        <field>StageName</field>
        <literalValue>Scheduled Trial</literalValue>
        <name>Update Stage to Scheduled Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_of_Reasons_Closed_Won_Endo</fullName>
        <field>Shared_Closed_Reason__c</field>
        <literalValue>N/A</literalValue>
        <name>Endo Update of Reasons Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_stage_to_closed_Lost_Trial</fullName>
        <description>For Trial Opportunity, update stage to lost 90 days after failed date</description>
        <field>StageName</field>
        <literalValue>Closed Lost(Trial)</literalValue>
        <name>Update stage to closed Lost Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Advance Opportunity Stage to Cancelled %28 Lost%29 for NMD SCS Trial</fullName>
        <actions>
            <name>Set_Opportunity_Stage_to_Cancelled_NMD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Opportunity Stage to Cancelled(Lost). Refer US2452 Q3 Release</description>
        <formula>AND(  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;,   Not(ISBLANK( TEXT( No_Longer_Opportunity__c ))),   AND(  Not( ISPICKVAL(No_Longer_Opportunity__c , &apos;Implanted&apos;) ),  Not( ISPICKVAL(No_Longer_Opportunity__c , &apos;Patient Choice&apos;) )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Cancelled%28Lost%29 for NMD SCS Implant</fullName>
        <actions>
            <name>Set_Stage_To_Cancel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change stage to Cancelled (LOST) if No Longer Opportunity field is not null
As per US2453 Q3 Release, Refer Notes</description>
        <formula>AND( $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos;, Not(ISBLANK( TEXT( No_Longer_Opportunity__c )))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Candidate for NMD SCSImplant</fullName>
        <actions>
            <name>Set_Stage_to_Candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set/Update Opportunity Stage to Candidate if:
 **Scheduled Trial Date/Time** is null, Record Type=NMD SCS Implant
Stage = Scheduled Implant and **SCHEDULED PROCEDURE DATE/TIME** is changed and is null
Refer US2453 Q3 Release</description>
        <formula>( (  ISNULL(   Scheduled_Procedure_Date_Time__c )  &amp;&amp;   $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos;)   ||   ( $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos; &amp;&amp; ( ISPICKVAL( StageName ,&quot;Scheduled Implant&quot;) || ISPICKVAL( StageName ,&quot;Implant&quot;) )   &amp;&amp;  ISNULL(Scheduled_Procedure_Date_Time__c)  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Closed Lost %28Trial%29 for NMD SCS Trial after Failed date</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Trial Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Failed_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set Opportunity Stage to Closed Lost (Trial) after Failed date. Refer US2452 Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_stage_to_closed_Lost_Trial</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Failed_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Closed Lost %28Trial%29 for NMD SCS Trial after Inconclusive date</fullName>
        <active>true</active>
        <description>After 6 months from Inconclusive date the system will set the opportunity to Closed Lost (Trial). Refer US2452 Q3 Release</description>
        <formula>AND(  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;, ISPICKVAL(StageName, &apos;Trial Complete&apos;), NOT(ISNULL( Inconclusive_Date__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_stage_to_Closed_Lost_Trail</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Inconclusive_Date__c</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Complete %28Closed won%29 for NMD SCS Implant</fullName>
        <actions>
            <name>Stage_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Stage to Complete (Closed Won) if:
**PRIMARY INSURANCE**
**PAIN AREA**
**PROCEDURE PHYSICIAN**
**PROCEDURE ACCOUNT**
**PROCEDURE DISTAL LEAD PLACEMENT**
**PRODUCT SYSTEM** 
 PO
 Procedure Order Number are not null

Refer US2453 Q3 Release</description>
        <formula>NOT(ISNULL(Primary_Insurance__c) || ISBLANK(Primary_Insurance__c))  &amp;&amp; NOT(ISNULL( Pain_Area__c )) &amp;&amp; NOT(ISNULL( Procedure_Physician__c   )) &amp;&amp; NOT(ISNULL(Procedure_Account__c )) &amp;&amp; NOT(ISNULL(PO__c) || ISBLANK(PO__c)) &amp;&amp; NOT(ISNULL(  Procedure_Order_Number__c )) &amp;&amp; NOT(ISBLANK(TEXT( Product_System__c))) &amp;&amp; NOT(ISBLANK(TEXT(Procedure_Distal_Lead_Placement__c))) &amp;&amp; $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Complete %28Closed won%29 for NMD SCS Trial</fullName>
        <actions>
            <name>Set_Stage_to_Complete_NMD_trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Opportunity Stage to Complete (Closed Won). Refer US2452 Q3 Release</description>
        <formula>( $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp; ( NOT(ISBLANK(Actual_Trial_Date__c)) &amp;&amp; Actual_Trial_Date__c&lt; Today() ) &amp;&amp; NOT( ISNULL( Primary_Insurance__c ) )  &amp;&amp; Not(ISNULL(Trialing_Physician__c ))  &amp;&amp;  NOT( ISNULL(  Trialing_Account__c ))   &amp;&amp; NOT( ISNULL(  Pain_Area__c ))   &amp;&amp; NOT( ISPICKVAL(Trial_Lead_Placement_Distal__c , &apos;&apos;)) &amp;&amp; NOT( ISPICKVAL(Product_System__c , &apos;&apos;)) &amp;&amp; NOT(  ISBLANK( Trial_PO__c ) )   &amp;&amp; ISPICKVAL(Trial_Status__c , &apos;Successful&apos;) &amp;&amp; NOT( ISPICKVAL(StageName , &apos;&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Current trial for NMD SCS Trial</fullName>
        <actions>
            <name>Set_Stage_to_Current_Trial_for_NMD_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stage = Candidate and **Actual Trial Date &lt;= Today Date and Trial Order number is not null and Trial status = null 
Stage = Scheduled Trial, **Actual Trial Date &lt;=Today Date and Trial Order number is not null and Trial status = null
Refer US2452 Q3</description>
        <formula>AND(OR(ISPICKVAL( StageName ,&quot;Candidate (Trial implant)&quot;),ISPICKVAL( StageName ,&quot;Scheduled Trial&quot;)),$RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;,Actual_Trial_Date__c &lt;= TODAY(),Trial_Order_Number__c!=null,ISBLANK(TEXT(Trial_Status__c )) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Implant for NMD SCS Implant</fullName>
        <actions>
            <name>Set_Stage_to_Implant_for_NMD_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Stage to Implant if:
Stage = Candidate and Actual Procedure Date &lt;= TODAY and Procedure Order Number is not null
Stage = Scheduled Implant and Actual Procedure Date &lt;= TODAY and Procedure Order Number is not null 
US2453</description>
        <formula>AND(   OR  ( 	ISPICKVAL( StageName ,&quot;Candidate (Trial implant)&quot;), 	ISPICKVAL( StageName ,&quot;Scheduled Implant&quot;)  ), 	 	NOT(ISBLANK( Procedure_Order_Number__c )), 	Actual_Procedure_Date__c &lt;= Today(), 	$RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Scheduled Implant for NMD SCS Implant</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set stage to Scheduled Implant if:
**SCHEDULED PROCEDURE DATE/TIME**Not Null and Procedure order number ISBLANK

Stage = Implant and **SCHEDULED PROCEDURE DATE/TIME** is changed  Procedure Order Number is null
US2453 Q3 release</description>
        <formula>OR  (  AND(  $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos; ,  (ISBLANK( Procedure_Order_Number__c )),  NOT(ISBLANK(Scheduled_Procedure_Date_Time__c )) ),  AND(  $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos; ,  ISPICKVAL( StageName ,&quot;Implant&quot;) ,  (ISBLANK( Procedure_Order_Number__c )),    NOT(ISBLANK(Scheduled_Procedure_Date_Time__c )) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Scheduled trial for NMD SCS Trial</fullName>
        <actions>
            <name>Set_Stage_to_Scheduled_trial_NMD_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>**Scheduled Trial Date/Time** is not null and  Trial Order Number isNull and Record Type=NMD SCS Trial.  
Stage = Current Trial and **Schedule Trial Date/Time** is changed and Trial Order Number is Null and Record Type=NMD SCS Trial.</description>
        <formula>(   $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp;  NOT(ISNULL(Scheduled_Trial_Date_Time__c) || ISBLANK(Scheduled_Trial_Date_Time__c))  &amp;&amp;  (ISBLANK( Trial_Order_Number__c ))   )    ||   (  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp;  ISPICKVAL( StageName ,&quot;Current Trial&quot;)  &amp;&amp;  NOT(ISNULL(Scheduled_Trial_Date_Time__c) || ISBLANK(Scheduled_Trial_Date_Time__c)) &amp;&amp; (ISBLANK( Trial_Order_Number__c ))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to Trial Complete for NMD SCS Trial</fullName>
        <actions>
            <name>Set_Stage_to_Trial_Complete_NMD_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Stage to Trial Complete . Refer US2452 Q3 Release</description>
        <formula>AND( $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;, ISPICKVAL( StageName ,&quot;Current Trial&quot;),  NOT(ISBLANK(Actual_Trial_Date__c)),  Actual_Trial_Date__c &lt; Today(),   NOT(ISBLANK(Lead_Pull_Date__c)),  DateValue(Lead_Pull_Date__c) &lt;= Today(),  NOT(ISNULL( Trial_Order_Number__c)),  NOT(ISNULL( Primary_Insurance__c )),  NOT(ISNULL( Trialing_Account__c )),  NOT(ISNULL( Pain_Area__c )),  NOT(ISNULL( Trialing_Physician__c )),  NOT(ISBLANK(TEXT(Trial_Lead_Placement_Distal__c))), NOT(ISBLANK( TEXT(Product_System__c))), NOT(ISBLANK( TEXT(Trial_Status__c))) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to schedule trial</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Candidate (Trial implant)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set Opportunity Stage to Scheduled Trial if Stage = Candidate, Opportunity Type= Standard,  Scheduled Trial Date is not null, Record Type=NMD SCS Trial-Implant</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity Stage to schedule trial_Updated</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set Opportunity Stage to Scheduled Trial if Stage = Candidate, Opportunity Type= Standard,  Scheduled Trial Date is not null, Record Type=NMD SCS Trial-Implant_Updated</description>
        <formula>(  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp;  (NOT(ISNULL(Scheduled_Trial_Date_Time__c)) || NOT(ISBLANK(Scheduled_Trial_Date_Time__c)))  &amp;&amp;  Scheduled_Trial_Date_Time__c&gt; NOW()  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity to Scheduled Implant</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Successful</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <description>Set stage to Scheduled Implant if the Opportunity Stage = Current Trial,  the Scheduled Procedure Date is populated and the Trial Status = Successful record type is NMD_SCS_Trial_Implant, Stage=Current Trial</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Opportunity to Scheduled Implant_updated</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set stage to Scheduled Implant if the Opportunity Stage = Current Trial,  the Scheduled Procedure Date is populated and the Trial Status = Successful record type is NMD_SCS_Trial_Implant, Stage=Current Trial</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Stage</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND ((3 AND 4) OR (5 AND 6) OR (7 AND 8))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Revision</value>
        </criteriaItems>
        <description>If the Actual Procedure Date has occurred, and the record is in one of the follow states (NMD_SCS_Explant / Scheduled Explant, NMD_SCS_Trial_Implant / Schedule Trial, NMD_SCS_Revision / Schedule Revision) then move the Stage forward (move to value below).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Advance_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Advance Stage 2</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ((3 AND 4) OR (5 AND 6) OR (7 AND 8))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Revision</value>
        </criteriaItems>
        <description>If the Actual Procedure Date has occurred, and the record is in one of the follow states (NMD_SCS_Explant / Scheduled Explant, NMD_SCS_Trial_Implant / Schedule Trial, NMD_SCS_Revision / Schedule Revision) then move the Stage forward (move to value below).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Advance_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Advance Stage 2_updated</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ((3 AND 4) OR (5 AND 6) OR (7 AND 8 AND 9 AND 10))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Implant,Candidate (Trial implant)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Procedure_Order_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the Actual Procedure Date has occurred, and the record is in one of the follow states (NMD SCS Implant / NMD_SCS_Explant / Scheduled Explant, NMD_SCS_Revision / Schedule Revision/) then move the Stage forward.
Updated As Per US2472 &amp;2453, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Advance_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Advance Stage to Scheduled Explant</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Explant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Procedure Candidate (Revision/Explant)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Stage to Scheduled Explant when Stage = Procedure Candidate and Scheduled Procedure Date is not null.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Stage to Scheduled Revision</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Revision</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Procedure Candidate (Revision/Explant)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Advance the Stage to Scheduled Revision when Stage = Procedure Candidate and Scheduled Procedure Date is not null</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Stage to current Trial</fullName>
        <actions>
            <name>Advance_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <description>Advance Stage to current Trail when Actual Trail Date is in the past.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Stage to current Trial_update2_immediate action</fullName>
        <actions>
            <name>Set_Stage_to_Current_Trial_for_NMD_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 7) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Order_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Candidate (Trial implant)</value>
        </criteriaItems>
        <description>Advance Stage to current Trial when Actual Trail Date is in the past.
As per US2452</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Advance Stage to current Trial_updated</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial,Candidate (Trial implant)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Advance Stage to current Trail when Actual Trail Date is in the future</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.Actual_Trial_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Capital Equipment Evaluation</fullName>
        <actions>
            <name>Email_for_demo_on_opportunity_for_Capital_Equipment_Evaluation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created for US2559.</description>
        <formula>AND( OR ( RecordType.Name  = &apos;EP Capital&apos;, RecordType.Name  = &apos;EP Disposable&apos; ), EP_Demo_Needed__c = True, NOT(ISBLANK(Product_Demo_Date__c)),   IsClosed = False,  Product_Demo_Date__c   &gt; TODAY(), OR( ISCHANGED(Product_Demo_Date__c), ISCHANGED(EP_Demo_Needed__c), ISNEW()) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Reprogramming Opportunities</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Reprogramming</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_Longer_Opportunity__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>For Reprogramming Opportunities, the stage should be set to Closed Won 24 hours after the Scheduled(Procedure) Date/Time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Stage_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Scheduled_Procedure_Date_Time__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Closed to Actual Trial</fullName>
        <actions>
            <name>Set_Close_Date_to_Actual_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Scheduled procedure date is deleted, then the close date will revert to Actual Trial Date</description>
        <formula>AND(          $RecordType.DeveloperName=&quot;NMD_SCS_Trial_Implant&quot;,           AND(                  ISCHANGED(Scheduled_Procedure_Date_Time__c ),                  ISBLANK(Scheduled_Procedure_Date_Time__c ),                  NOT(ISBLANK(Scheduled_Trial_Date_Time__c )) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed to Procedure</fullName>
        <actions>
            <name>Set_Close_Date_to_Procedure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The Scheduled procedure date will update the close date with the Schedueld procedure date when Scheduled Procedure is not null
Updated As Per US2472, Q3 Release</description>
        <formula>AND( OR(  $RecordType.DeveloperName=&quot;NMD_SCS_Revision&quot;,  $RecordType.DeveloperName=&quot;NMD_SCS_Explant&quot;,  $RecordType.DeveloperName=&quot;NMD_SCS_Implant&quot; ), OR(  AND(ISCHANGED(Scheduled_Procedure_Date_Time__c), NOT(ISNULL(Scheduled_Procedure_Date_Time__c))), AND(ISNEW(),NOT(ISBLANK(Scheduled_Procedure_Date_Time__c))) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed to Trial</fullName>
        <actions>
            <name>Set_Close_Date_to_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The Scheduled Trial Date will update the close date, unless the scheduled procedure date is populated.

Updated as Per US2472 , Q3 Release</description>
        <formula>AND(    $RecordType.DeveloperName=&quot;NMD_SCS_Trial&quot;,    OR( AND(ISNEW(),NOT(ISBLANK(Scheduled_Trial_Date_Time__c))),   AND(NOT(ISBLANK(Scheduled_Trial_Date_Time__c)), ISCHANGED(Scheduled_Trial_Date_Time__c) )    )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Decrease stage as Order number deleted</fullName>
        <actions>
            <name>Decrease_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is to decrease stage if order number is deleted and the stage is current trial or implant</description>
        <formula>(  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp;  ISCHANGED( Trial_Order_Number__c) &amp;&amp;  ISBLANK(Trial_Order_Number__c)  &amp;&amp; ISPICKVAL( StageName , &apos;Current Trial&apos;) )  ||  (   $RecordType.DeveloperName = &apos;NMD_SCS_Implant&apos;  &amp;&amp;  ISCHANGED(Procedure_Order_Number__c) &amp;&amp;  ISBLANK(Procedure_Order_Number__c)  &amp;&amp; ISPICKVAL( StageName , &apos;Implant&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Descrease Stage</fullName>
        <actions>
            <name>Decrease_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5) OR (6 AND 7))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Revision</value>
        </criteriaItems>
        <description>If the Actual Procedure Date has been moved to the future, and the record is in one of the follow states (NMD_SCS_Explant / Explant, NMD_SCS_Trial_Implant / Trial, NMD_SCS_Revision / Revision), then move the Stage back (move to value above).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Descrease Stage_updated</fullName>
        <actions>
            <name>Decrease_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5) OR (6 AND 7))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Revision</value>
        </criteriaItems>
        <description>If the Actual Procedure Date has been moved to the future, and the record is in one of the follow states (NMD_SCS_Explant / Explant, NMD_SCS_Trial_Implant / Trial, NMD_SCS_Revision / Revision), then move the Stage back (move to value above).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Endo Update Reasons when Closed Won</fullName>
        <actions>
            <name>Update_of_Reasons_Closed_Won_Endo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Endo New Disposable Business,Endo Capital Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Acceptance-3422- Required that Endo must select a reason if the Opportunity is marked as Closed Won. Pre-defined list of reasons when Closed Won is selected to &quot;N/A&quot; by default.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Europe_EP_Capital_Oppty_Probability_Fix</fullName>
        <actions>
            <name>Europe_Capital_Oppty_Prob_Fix</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT(IsClosed),  ISCHANGED(StageName),  AND($RecordType.DeveloperName =&apos;Europe_EP_Capital_Equipment&apos;),  Probability &lt; PRIORVALUE(Probability)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Failed to Lost</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Failed_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NMD-If Stage = Current Trial and After 3 months from Failed date the system will set the opportunity to Lost (closed lost)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Third_Month_Failed_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Failed_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Failed to Lost_updated</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Failed_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NMD-If Stage = Current Trial and After 3 months from Failed date the system will set the opportunity to Closed Lost(Trial)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_stage_to_Closed_Lost_Trail</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Failed_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Inconclusive to Lost</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Inconclusive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Inconclusive_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NMD-If Stage = Current Trial and After 6 months from Inconclusive date the system will set the opportunity to Lost (closed lost)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sixth_Month_Inconclusive_to_Lost</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Inconclusive_Date__c</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Inconclusive to Lost_updated</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Inconclusive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Inconclusive_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NMD-If Stage = Current Trial and After 6 months from Inconclusive date the system will set the opportunity to Closed Lost(Trial)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_stage_to_Closed_Lost_Trail</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Inconclusive_Date__c</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LAAC_Clear_Postponed_by_if_Sub_Stage_Is_Not_Contract_Postponed</fullName>
        <actions>
            <name>LAAC_Clear_Postponed_by</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears the value in Postponed by field if a Watchman New Business opportunity does not have the sub stage of Contract Postponed.</description>
        <formula>AND($RecordType.DeveloperName =&apos;Watchman_New_Business&apos;, NOT(ISPICKVAL( Shared_Sub_Stage__c ,&apos;Contract Postponed&apos;)),  NOT(ISPICKVAL( LAAC_Postponed_by__c ,&apos;&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LAAC_Update_Close_Date_by_Postponed_Length</fullName>
        <actions>
            <name>LAAC_Update_Close_Date_when_Postponed_by</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the close date of a Watchman New Business opportunity is postponed. Close date is updated by formula based on the pick val chosen in the Postponed by field.</description>
        <formula>AND($RecordType.DeveloperName =&apos;Watchman_New_Business&apos;, ISPICKVAL( Shared_Sub_Stage__c ,&apos;Contract Postponed&apos;),  ISCHANGED( LAAC_Postponed_by__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Move Stage to complete</fullName>
        <actions>
            <name>Stage_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8) OR (1 AND 2 AND 3 AND 4 AND 5 AND 9)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Procedure_Distal_Lead_Placement__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Successful</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>OMG,Competitive Swap,BSC Swap</value>
        </criteriaItems>
        <description>Opportunity Type = Standard and Stage=Implant  and Trial Status=Successful and Actual Procedure Date &lt;=Today and Trial PO and PO are not null, Record type = Trial Implant,then the system will set Stage = Complete</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Move the Stage to Scheduled Implant</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>OMG,Competitive Swap,Implant Only</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set stage to Scheduled Implant if Opportunity Type = Competitive Swap,Scheduled Procedure date!=null, competitor!= null OR Opportunity Type = BSC Swap/scheduled Procedure date is not null, Procedure Product System!=NULL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Move the Stage to Scheduled Implant_updated</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>OMG,Competitive Swap,Implant Only</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set stage to Scheduled Implant if Opportunity Type = Competitive Swap,Scheduled Procedure date!=null, competitor!= null OR Opportunity Type = BSC Swap/scheduled Procedure date is not null, Procedure Product System!=NULL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>No Longer Opportunity</fullName>
        <actions>
            <name>Set_Stage_to_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision,NMD Reprogramming,NMD SCS Implant,NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_Longer_Opportunity__c</field>
            <operation>equals</operation>
            <value>Cancelled,Denial,Health Issues,Cost,Duplicate</value>
        </criteriaItems>
        <description>If No Longer Opportunity field = Cancelled, Denial, Health Issues, Cost, Duplicate, then system will Set Opportunity Stage to Cancelled (Lost)
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>No Longer Opportunity picklist for Implanted</fullName>
        <actions>
            <name>Set_Stage_to_Complete_NMD_trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change stage after these fields are populated:
Primary Insurance
Pain Area
Trialing Physician
Trialing Account
Trial Lead Distal Placement
Product System
Trial Order Number
Trial PO is not NULL or &quot;PO to Follow&quot;

As per US2475, Q3 release</description>
        <formula>(  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp;    ISPICKVAL( No_Longer_Opportunity__c ,&apos;Implanted&apos;)  &amp;&amp;  NOT( ISBLANK( Primary_Insurance__c ) )  &amp;&amp;  NOT( ISBLANK( Pain_Area__c ))  &amp;&amp;  Not(ISBLANK(Trialing_Physician__c ))  &amp;&amp;  NOT( ISBLANK( Trialing_Account__c ))  &amp;&amp;  NOT(ISBLANK(TEXT(Trial_Lead_Placement_Distal__c)))  &amp;&amp;   NOT(ISBLANK(TEXT(Product_System__c)))   &amp;&amp;  NOT(ISBLANK( Trial_Order_Number__c))  &amp;&amp;  NOT(ISBLANK( Trial_PO__c ))  &amp;&amp;  Trial_PO__c!=&apos;PO to Follow&apos;   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>No Longer Opportunity picklist for Patient Choice</fullName>
        <actions>
            <name>Change_field_update_to_Closed_lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change stage after these fields are populated:
Primary Insurance
Pain Area
Trialing Physician
Trialing Account
Trial Lead Distal Placement
Product System
Trial Order Number
Trial PO is not NULL or &quot;PO to Follow&quot;
As per US2475, Q3 release</description>
        <formula>(  $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos;  &amp;&amp;    ISPICKVAL( No_Longer_Opportunity__c ,&apos;Patient Choice&apos;)  &amp;&amp;  NOT( ISBLANK( Primary_Insurance__c ) )  &amp;&amp;  NOT( ISBLANK( Pain_Area__c ))  &amp;&amp;  Not(ISBLANK(Trialing_Physician__c ))  &amp;&amp;  NOT( ISBLANK( Trialing_Account__c ))  &amp;&amp;  NOT(ISBLANK(TEXT(Trial_Lead_Placement_Distal__c)))  &amp;&amp;   NOT(ISBLANK(TEXT(Product_System__c)))   &amp;&amp;  NOT(ISBLANK( Trial_Order_Number__c))  &amp;&amp;  NOT(ISBLANK( Trial_PO__c ))  &amp;&amp;  Trial_PO__c!=&apos;PO to Follow&apos;   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Failed Date</fullName>
        <actions>
            <name>Populate_Failed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <description>The system will track the date/time Trial Status set to Failed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Inconclusive Date</fullName>
        <actions>
            <name>Populate_Inconclusive_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Inconclusive</value>
        </criteriaItems>
        <description>The system will track the date/time Trial Status set to Inconclusive</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Return Reprogramming Stage to Open</fullName>
        <actions>
            <name>Set_Stage_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Reprogramming</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.No_Longer_Opportunity__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update the Stage to Open if Scheduled Procedure Date = Null</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Revert Opportunity Stage to Candidate</fullName>
        <actions>
            <name>Set_Opportunity_stage_to_candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4) OR (3 AND 4 AND 5 )</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <description>Set the stage to Candidate If the Opportunity Type = Standard ,Scheduled Trial Date is NULL,  Stage=Scheduled Trial, Record type is  NMD SCS Trial Implant</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Revert Opportunity Stage to Candidate_Updated</fullName>
        <actions>
            <name>Set_Opportunity_stage_to_candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4) OR (3 AND 4 AND 5 ) OR (6 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Standard,OMG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set the stage to Candidate If the Opportunity Type = Standard ,Scheduled Trial Date is NULL,  Stage=Scheduled Trial, Record type is  NMD SCS Trial
US2452</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Revert Opportunity Stage to Procedure Candidate</fullName>
        <actions>
            <name>Set_Stage_to_Procedure_Candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant,Scheduled Revision,Explant,Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If scheduled procedure date is deleted,Opportunity Record Type is NMD SCS Explant or NMD SCS Revision, Stage is Scheduled Explant orScheduled Revision, update the stage to Procedure Candidate.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Revert Stage to Current Trial</fullName>
        <actions>
            <name>Set_Stage_to_Current_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Implant,Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If Stage = Scheduled Implant or Implant and Scheduled Procedure Date = blank, the new stage = Current Trial</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SAP Procedure Physician Reminder</fullName>
        <actions>
            <name>Action_Needed_Physician_Setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>A user should be notified 3 days before a Scheduled Procedure Date to submit the  Procedure Physician without an SAP ID to Quincy
Updated As Per US2472, Q3 Release</description>
        <formula>AND(       OR( $RecordType.DeveloperName=&quot;NMD_SCS_Revision&quot;,              $RecordType.DeveloperName=&quot;NMD_SCS_Explant&quot;,              $RecordType.DeveloperName=&quot;NMD_SCS_Implant&quot;),                     AND(                            NOT(ISBLANK(Procedure_Physician__c)),                            ISBLANK(Procedure_Physician__r.SAP_ID__c),                            Three_Day_Procedure__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SAP Trial Physician Reminder</fullName>
        <actions>
            <name>Action_Needed_Physician_Set_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>A user should be notified 3 days before a Scheduled Procedure Date to submit the  Procedure Physician without an SAP ID to Quincy

Updated As Per US2472, Q3 Release</description>
        <formula>AND(         $RecordType.DeveloperName=&quot;NMD_SCS_Trial&quot;,         NOT(ISBLANK(Trialing_Physician__c)),         ISBLANK(Trialing_Physician__r.SAP_ID__c ),         Three_Day_Trial__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SET_SCHEDULED_PROCEDURE_DATE_TO_TIME</fullName>
        <actions>
            <name>UPDATE_SCHEDULED_PROCEDURE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Uses the value input into the SCHEDULED_PROCEDURE_DATE_TIME__c  &quot;Date/Time&quot; field to populate the Scheduled_Procedure_Date__c &quot;Date&quot; field with the Date value that corresponds to the Date/Time value.</description>
        <formula>ISCHANGED( Scheduled_Procedure_Date_Time__c ) || ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SET_SCHEDULED_TRIAL_DATE_TO_TIME</fullName>
        <actions>
            <name>UPDATE_SCHEDULED_TRIAL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Uses the value input into the SCHEDULED_PROCEDURE_DATE_TIME__c  &quot;Date/Time&quot; field to populate the Scheduled_Trial_Date__c &quot;Date&quot; field with the Date value that corresponds to the Date/Time value.</description>
        <formula>ISCHANGED( Scheduled_Trial_Date_Time__c ) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set ANZ Opportunity name</fullName>
        <actions>
            <name>Set_ANZ_Opportunity_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Revision,ANZ Trial,ANZ Implant,ANZ Reprogramming,ANZ Explant,ANZ Revision</value>
        </criteriaItems>
        <description>Set Opportunity name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Closed Date 30 Days Out</fullName>
        <actions>
            <name>Set_Closed_to_Today_30</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Works or Implant,Explant and Revision.
When the Opportunity is created the Closed Date will be set to Today + 30 days if scheduled procedure date is blank or is deleted after creation. 

Updated As Per US2472, Q3 Release</description>
        <formula>AND(  OR( $RecordType.DeveloperName=&quot;NMD_SCS_Revision&quot;, $RecordType.DeveloperName=&quot;NMD_SCS_Explant&quot;,  $RecordType.DeveloperName=&quot;NMD_SCS_Implant&quot; ), OR(  AND (ISNEW(),ISBLANK(Scheduled_Procedure_Date_Time__c)),  AND(ISCHANGED(Scheduled_Procedure_Date_Time__c), ISBLANK(Scheduled_Procedure_Date_Time__c )) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set closed date 30 days out Trial Or Implat Opty</fullName>
        <actions>
            <name>Set_Closed_to_Now_30</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Works only for Trial: set close date to today +30 days if - scheduled trail date is null 
OR
if is it deleted after record creation and scheduled procedure date is null.</description>
        <formula>AND(  OR(  AND (ISNEW(),$RecordType.DeveloperName=&quot;NMD_SCS_Trial&quot;,ISBLANK(Scheduled_Trial_Date_Time__c)),  AND(ISCHANGED(Scheduled_Trial_Date_Time__c), ISBLANK(Scheduled_Trial_Date_Time__c ), ISBLANK(Scheduled_Procedure_Date_Time__c ) )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Shared_Set_Close_Date_On_Close</fullName>
        <actions>
            <name>Shared_Set_Close_Date_To_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule create for EP as a part of US2561, but can be utilized by other sites in the future.</description>
        <formula>IsClosed=TRUE &amp;&amp;(  $RecordType.DeveloperName =&quot;EP_Capital&quot;  || $RecordType.DeveloperName =&quot;EP_Disposable&quot;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stage 2 Activity - Europe EP Capital Equipment</fullName>
        <actions>
            <name>Opportunity_Gate_Keeper_Stage_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Schedule_Site_Visit</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This workflow will trigger an Event Task when the Europe EP Capital Equipment Opportunity enters Stage 2 - 02 Physician Commitment.</description>
        <formula>AND ( ISPICKVAL(StageName,&quot;02 Physician Commitment&quot;), ISCHANGED(StageName),  (Shared_Opportunity_Gate_Keeper_Stage_2__c = FALSE))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage 3 Activity - Europe EP Capital Equipment</fullName>
        <actions>
            <name>Opportunity_Gate_Keeper_Stage_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Schedule_Site_Visit</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This workflow will trigger an Event Task when the Europe EP Capital Equipment Opportunity enters Stage 3 - 03 Administration Commitment</description>
        <formula>AND ( ISPICKVAL(StageName,&quot;03 Administration Commitment&quot;), ISCHANGED(StageName),  (Shared_Opportunity_Gate_Keeper_Stage_3__c = FALSE))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage 4 Activity - Europe EP Capital Equipment</fullName>
        <actions>
            <name>Opportunity_Gate_Keeper_Stage_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Inform_and_Introduce_Clinical_Team</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Inform_and_Introduce_Technical_Team</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Schedule_Steering_Committee_Meeting</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This workflow will trigger an Event Task when the Europe EP Capital Equipment Opportunity enters Stage 4 - 04 Budget Approval</description>
        <formula>AND ( ISPICKVAL(StageName,&quot;04 Budget Approval&quot;), ISCHANGED(StageName),  (Shared_Opportunity_Gate_Keeper_Stage_4__c = FALSE))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage 7 Activity - Europe EP Capital Equipment</fullName>
        <actions>
            <name>Opportunity_Gate_Keeper_Stage_7</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clinical_Handover</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Schedule_Steering_Committee_Meeting</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Schedule_Training</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>This workflow will trigger an Event Task when the Europe EP Capital Equipment Opportunity enters Stage 7 - 07 Handover</description>
        <formula>AND ( ISPICKVAL(StageName,&quot;07 Handover&quot;), ISCHANGED(StageName),  (Shared_Opportunity_Gate_Keeper_Stage_7__c = FALSE))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Three Day Threshold Procedure</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>A user should be notified 3 days before Scheduled Trial or Procedure Date to submit the Trialing or Procedure Physician without an SAP ID to Quincy</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Three_Day_Procedure</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Scheduled_Procedure_Date_Time__c</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Three Day Threshold Trial</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Scheduled_Trial_Date_Time__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>A user should be notified 3 days before Scheduled Trial or Procedure Date to submit the Trialing or Procedure Physician without an SAP ID to Quincy</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Three_Day</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Scheduled_Trial_Date_Time__c</offsetFromField>
            <timeLength>-3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Actual Procedure Date</fullName>
        <actions>
            <name>Update_Actual_Procedure_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Actual Procedure Date with the Scheduled Procedure Date every time Scheduled Procedure Date is populated/edited.</description>
        <formula>OR(   AND(     ISNEW(),      NOT(ISBLANK( Scheduled_Procedure_Date_Time__c ))   ),   ISCHANGED( Scheduled_Procedure_Date_Time__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Actual Trial Date</fullName>
        <actions>
            <name>Set_Actual_Trial_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Scheduled Trial Date should update Actual Trial Date.</description>
        <formula>OR(   ISNEW(),    ISCHANGED( Scheduled_Trial_Date_Time__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Amount</fullName>
        <actions>
            <name>Update_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(         RecordType.DeveloperName=&quot;NMD_SCS_Trial_Implant&quot;,         OR(               NOT(ISBLANK(Trial_Revenue__c)),               NOT(ISBLANK(Procedure_Revenue__c))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Explant or Revision to Complete</fullName>
        <actions>
            <name>Stage_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Explant,Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NMD-Stage = Explant or Revision, Actual Procedure Date &lt;=Today and PO is populated, system will update Stage = Comple</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Pull Date</fullName>
        <actions>
            <name>Set_Lead_Pull_Date_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Lead_Pull_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When the Trial Status is not null and the Lead Pull Date is Null on a NMD TrialOpportunity
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Closed Date</fullName>
        <actions>
            <name>Set_Opportunity_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision,NMD SCS Implant,NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Complete (Closed Won),Lost (Closed Lost)</value>
        </criteriaItems>
        <description>Once a Trial/Implant/Explant or Revision Opportunity is set to closed lost/closed won update the Opportunity Closed Date with current time
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity GPO Field</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Europe EP Disposable</value>
        </criteriaItems>
        <description>Europe EP Disposable sales process requirement - this rule will update the GPO field on the Opportunity with the value of the Account.GPO__c field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Name</fullName>
        <actions>
            <name>Set_Opportunity_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR ( 2 AND 3 )</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision,NMD Reprogramming</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Standard,OMG,Perm-Trial</value>
        </criteriaItems>
        <description>Update Opportunity Name as: Trialing Physician.Last Name +&quot; / Procedure Physician.Last name&quot; - &quot;Patient First Name (Left 2 characters). + &quot; - &quot;+Patient Last name (left 3 characters)+&quot; - &quot; Record Type Name (Trial/Implant)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Name for Implant %28Type %3D Competitive Swap or Implant Only%29</fullName>
        <actions>
            <name>Set_Opportunity_name_for_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Competitive Swap,Implant Only</value>
        </criteriaItems>
        <description>Update Opportunity Name as: Procedure Physician.Last name&quot; - &quot;Patient First Name (Left 2 characters). + &quot; - &quot;+Patient Last name (left 3 characters)+&quot; - &quot; Record Type Name (Implant)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Name for RFP_RFI type</fullName>
        <actions>
            <name>Update_Opportunity_Name_for_RFP_RFI_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Opportunity Name for CA RFP/RFI types as:  Account Name + Division + Product Group

Created as part of Jira Story # 1334 - CA.  Sept 16 Monthly Release – JM</description>
        <formula>AND($RecordType.DeveloperName =&quot;Product_RFP_RFI&quot;,  OR (ISCHANGED( Division_Concatenate__c ),  ISCHANGED( Product_Group_Concatenate__c ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Name for Trail record type</fullName>
        <actions>
            <name>Set_Opportunity_name_for_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <description>Update Opportunity Name as: Trialing Physician.Last Name +&quot; /  - &quot;Patient First Name (Left 2 characters). +Patient Last name (left 3 characters)+&quot; - &quot; Record Type Name (Trial)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Name2</fullName>
        <actions>
            <name>NMD_Cool_Oppy_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Cool</value>
        </criteriaItems>
        <description>Update Opportunity Name for Cool to NMD Cool + Physician</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Originating Rep</fullName>
        <actions>
            <name>Set_Originating_field_Rep</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision,NMD SCS Implant,NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Complete (Closed Won),Lost (Closed Lost),Closed Lost(Trial)</value>
        </criteriaItems>
        <description>Update Originating Rep with the owner of the opportunity every time it is edited so long as the stage&lt;&gt;Closed Won or Closed Lost.
Updated As Per US2472, Q3 Release</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PO Time Stamp</fullName>
        <actions>
            <name>Set_PO_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision,NMD SCS Implant</value>
        </criteriaItems>
        <description>Once a PO is entered, update PO time stamp
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Procedure Stage time Stamp</fullName>
        <actions>
            <name>Set_Procedure_Stage_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant,NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant,Scheduled Revision</value>
        </criteriaItems>
        <description>Update Procedure Stage time Stamp when Opportunity moves to Scheduled Revision or Scheduled Explant</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Schedule Trial Stage Time Stamp</fullName>
        <actions>
            <name>Set_Schedule_Trial_Stage_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <description>Update Schedule Trial Stage Time Stamp when Trial Opportunity moves to  Schedule Trial
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage</fullName>
        <actions>
            <name>Advance_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ((3 AND 4) OR (5 AND 6) OR (7 AND 8))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Revision</value>
        </criteriaItems>
        <description>Advance stage when Actual Procedure Date is less than or equal to TODAY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Current Trial</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>If Stage = Scheduled Trial, Actual Trial Date &lt;=Today and PO=Null, then update stage to Current Trial.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Advance_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Trial_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Stage to Current Trial 2</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <description>If Stage = Scheduled Trial, Actual Trial Date &lt;=Today and PO=Null, then update stage to Current Trial.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Advance_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Trial_Date__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Stage to Explant</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Explant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Explant</value>
        </criteriaItems>
        <description>If Stage = Scheduled Explant, Actual Procedure Date &lt;=Today and PO=Null, then update stage to Explant.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Stage_to_Explant</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Stage to Implant</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <description>If Stage = Scheduled Implant, Actual Procedure Date &lt;=Today and PO=Null, then update stage to Implant.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Stage_to_Implant</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Stage to Revision</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Scheduled Revision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Revision</value>
        </criteriaItems>
        <description>If Stage = Scheduled Revision, Actual Procedure Date &lt;=Today and PO=Null, then update stage to Revision.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Stage_to_Revision</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Stage to Scheduled Explant</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Explant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If RecordType = Explant and Actual procedure date is changed and Actual Procedure Date &gt;Today, then update stage to Scheduled Explant.</description>
        <formula>AND(   $RecordType.DeveloperName = &apos;NMD_SCS_Explant&apos;,   ISPICKVAL( StageName, &apos;Explant&apos; ),   ISCHANGED( Actual_Procedure_Date__c ),   Actual_Procedure_Date__c &gt; TODAY() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Scheduled Implant</fullName>
        <actions>
            <name>Update_Stage_to_Scheduled_Implant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Stage = Trial/Implant and Actual procedure date is changed and Actual Procedure Date &gt;Today, then update stage to Scheduled Implant.</description>
        <formula>AND(   $RecordType.DeveloperName = &apos;NMD_SCS_Trial_Implant&apos;,   ISPICKVAL( StageName, &apos;Implant&apos; ),   ISCHANGED( Actual_Procedure_Date__c ),   Actual_Procedure_Date__c &gt; TODAY() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Scheduled Trial</fullName>
        <actions>
            <name>Decrease_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Trial_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Current Trial</value>
        </criteriaItems>
        <description>If Stage = Current Trial and Actual Trial date is changed and is &gt;Today, then update stage to Scheduled Trial.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Scheduled Trial_Updated</fullName>
        <actions>
            <name>Decrease_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Stage = Current Trial and Actual Trial date is changed and is &gt;Today, then update stage to Scheduled Trial.
US2452</description>
        <formula>AND (ISPICKVAL( StageName , &apos;Current Trial&apos;)  , $RecordType.DeveloperName = &apos;NMD_SCS_Trial&apos; , ISCHANGED(  Scheduled_Trial_Date_Time__c ) , Scheduled_Trial_Date_Time__c &gt; Now() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to complete</fullName>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8) OR (1 AND 2 AND 3 AND 4 AND 5 AND 9)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial-Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Implant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Actual_Procedure_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Procedure_Distal_Lead_Placement__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Successful</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>,OMG,Competitive Swap,Implant Only</value>
        </criteriaItems>
        <description>Opportunity Type = Standard and Stage=Implant  and Trial Status=Successful and Actual Procedure Date &lt;=Today and Trial PO and PO are not null, Record type = Trial Implant,then the system will set Stage = Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Stage_Complete</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Actual_Procedure_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Tiering on Opportunity</fullName>
        <actions>
            <name>Set_National_Tiering_on_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.National_Tiering__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Territory_Tiering__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>used for reporting purposes only - populates Tiering fields from Account (by Division) on Opportunities</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Trial PO Time Stamp</fullName>
        <actions>
            <name>Set_Trial_PO_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_PO__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Trial PO Time Stamp, when Trial PO is entered and record type is NMD SCS Trial.
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Trial Status</fullName>
        <actions>
            <name>Set_Trial_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Scheduled_Procedure_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <description>Update Trial Status  to Successful if the Scheduled Procedure Date not equal to NULL, record type = NMD SCS Implant
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Trial Status Date</fullName>
        <actions>
            <name>Set_Trial_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Trial_Status__c</field>
            <operation>equals</operation>
            <value>Successful,Inconclusive,Failed</value>
        </criteriaItems>
        <description>Update Trial Status Date when  Trial Status = Successful, Inconclusive or Failed

Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Trial status to OMG</fullName>
        <actions>
            <name>Set_trial_Status_to_OMG</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD SCS Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>OMG</value>
        </criteriaItems>
        <description>If Opportunity Type = OMG, RecordType=NMD SCS Trial, 
update Trial Status to OMG
Updated As Per US2472, Q3 Release</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Action_Needed_Physician_Set_up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Action Required</status>
        <subject>Action Needed - Physician Setup</subject>
    </tasks>
    <tasks>
        <fullName>Action_Needed_Physician_Setup</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Action Required</status>
        <subject>Action Needed - Physician Setup</subject>
    </tasks>
    <tasks>
        <fullName>Clinical_Handover</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Clinical Handover</subject>
    </tasks>
    <tasks>
        <fullName>Inform_and_Introduce_Clinical_Team</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Inform and Introduce Clinical Team</subject>
    </tasks>
    <tasks>
        <fullName>Inform_and_Introduce_Technical_Team</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Inform and Introduce Technical Team</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Site_Visit</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Site Visit</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Steering_Committee_Meeting</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Steering Committee Meeting</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Training</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Training</subject>
    </tasks>
</Workflow>

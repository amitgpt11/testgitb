/**
* Manager class for the Cycle_Count_Line_Item__c SObject
*
* @Author salesforce Services
* @Date 2015-07-24
*/
public with sharing class CycleCountLineItemManager {

    static final String ANALYZING = 'Analyzing';
    static final String IN_INVENTORY = 'In Inventory';
    static final String IN_TRANSIT = 'In Transit';
    static final String LOCATED = 'Located (Serial)';
    static final String PROD_ONHAND = 'Product on Hand';
    static final String SCANNING = 'Scanning';
    static final String SHIP_INTRANSIT = 'Shipment in Transit';
    static final String SHORTAGE = 'Shortage (Lot Items)';
    static final String SURPLUS = 'Surplus';
    static final String TRANSFER = 'Transfer';
    static final String USED_INSURGERY = 'Used in Surgery';
    
    private Map<Id,Cycle_Count_Response_Analysis__c> parents = new Map<Id,Cycle_Count_Response_Analysis__c>();
    private Map<String,Map<Id,Inventory_Item__c>> matchingItems = new Map<String,Map<Id,Inventory_Item__c>>();
    private Map<String,Surgery__c> matchingSurgeries = new Map<String,Surgery__c>();

    /**
    *  @desc    pre-load related records
    *  @param   LIST items
    */
    public void fetchMatchingRecords(List<Cycle_Count_Line_Item__c> items) {

        Set<Id> parentIds = new Set<Id>();
        Set<Id> itemIds = new Set<Id>();
        Set<Id> surgIds = new Set<Id>();
        Set<String> serials = new Set<String>();
        Set<String> models = new Set<String>();
        Set<String> lots = new Set<String>();
        Set<String> mlss = new Set<String>();

        for (Cycle_Count_Line_Item__c item : items) {

            if (item.Cycle_Count_Response_Analysis__c != null) {
                parentIds.add(item.Cycle_Count_Response_Analysis__c);
            }

            if (item.Surgery_Item__c != null) {
                surgIds.add(item.Surgery_Item__c);
            }

            if (item.Other_Inventory_Item__c != null) {
                itemIds.add(item.Other_Inventory_Item__c);
            }
            else {

                if (String.isNotBlank(item.Scanned_Serial_Number__c)) {
                    serials.add(item.Scanned_Serial_Number__c);
                    mlss.add(buildItemKey(item));
                }

                if (String.isNotBlank(item.Scanned_Lot_Number__c)) {
                    lots.add(item.Scanned_Lot_Number__c);
                }

                if (String.isNotBlank(item.Scanned_Model_Number__c)) {
                    models.add(item.Scanned_Model_Number__c);
                }
            }

        }

        if (!parentIds.isEmpty()) {
            this.parents = new Map<Id,Cycle_Count_Response_Analysis__c>([
                select 
                    Status__c,
                    OwnerId
                from Cycle_Count_Response_Analysis__c
                where Id in :parentIds
            ]);
        }

        if (serials.isEmpty() && lots.isEmpty() && models.isEmpty()) {
            return;
        }

        // matching inventory items
        for (Inventory_Item__c invItem : [
            select
                Lot_Number__c,
                Serial_Number__c,
                Model_Number__r.EAN_UPN__c,
                Model_Number__r.Model_Number__c,
                Model_Number__r.UPN_Material_Number__c,
                Inventory_Location__r.Inventory_Data_ID__r.OwnerId
            from Inventory_Item__c
            where (
                    Model_Number__r.EAN_UPN__c in :models
                 or Model_Number__r.Model_Number__c in :models
                 or Model_Number__r.UPN_Material_Number__c in :models
            )
            and (
                    Lot_Number__c in :lots
                 or Serial_Number__c in :serials
            )
            and SAP_Calculated_Quantity__c > 0
        ]) {

            //this.matchingItems.put(buildItemKey(invItem), invItem);
            for (String itemKey : buildItemKeys(invItem)) {
                if (this.matchingItems.containsKey(itemKey)) {
                    this.matchingItems.get(itemKey).put(invItem.Inventory_Location__r.Inventory_Data_ID__r.OwnerId, invItem);
                }
                else {
                    this.matchingItems.put(itemKey, new Map<Id,Inventory_Item__c> {
                        invItem.Inventory_Location__r.Inventory_Data_ID__r.OwnerId => invItem
                    });
                }
            }

        }

        if (mlss.isEmpty()) return;

        // matching surgery records.
        // retrieve 'I'mplants first, then overwrite with 'E'xplants if present
        for (Surgery__c surgery : [
            select
                Serial_Lot__c
            from Surgery__c
            where Serial_Lot__c in :mlss
            and Status__c in ('I', 'E')
            order by Status__c desc 
        ]) {

            this.matchingSurgeries.put(surgery.Serial_Lot__c, surgery);

        }

    }

    /**
    *  @desc    update record to matching record's values
    *  @param   Cycel_Count_Line_Item__c item
    */
    public void matchRelatedRecords(Cycle_Count_Line_Item__c item) {

        Cycle_Count_Response_Analysis__c parent = this.parents.get(item.Cycle_Count_Response_Analysis__c);
        if (parent == null || parent.Status__c != SCANNING) return;

        String itemKey = buildItemKey(item);
        if (this.matchingItems.containsKey(itemKey)) {

            Map<Id,Inventory_Item__c> invItems = this.matchingItems.get(itemKey);
            Inventory_Item__c invItem = invItems.get(parent.OwnerId);
            
            if (invItem == null 
             && String.isNotBlank(item.Scanned_Serial_Number__c) 
             && !invItems.isEmpty()
            ) {
                invItem = invItems.values()[0];
            }

            if (invItem != null) {

                item.Other_Inventory_Item__c = invItem.Id;

                if (invItem.Inventory_Location__r.Inventory_Data_ID__r.OwnerId == parent.OwnerId) {

                    item.Inventory_Item__c = invItem.Id;

                    if (item.Scanned_Quantity__c < item.Actual_Quantity__c) {
                        item.Exception_Reason__c = SHORTAGE;
                    }
                    else if (item.Scanned_Quantity__c > item.Actual_Quantity__c) {
                        item.Exception_Reason__c = SURPLUS;
                    }
                    else {
                        item.Exception_Reason__c = IN_INVENTORY;
                    }

                }
                else {

                    item.Exception_Reason__c = LOCATED;

                }  

            }

        }
        
        if (String.isNotBlank(item.Scanned_Serial_Number__c) && this.matchingSurgeries.containsKey(itemKey)) {

            Surgery__c surgery = this.matchingSurgeries.get(itemKey);

            item.Surgery_Item__c = surgery.Id;
            item.Exception_Reason__c = LOCATED;
            
        }

    }

    //// ITEM KEYS ////

    private List<String> buildItemKeys(Inventory_Item__c item) {
        return new List<String> { 
            (buildItemKey(item, item.Model_Number__r.EAN_UPN__c)),
            (buildItemKey(item, item.Model_Number__r.UPN_Material_Number__c)),
            (buildItemKey(item, item.Model_Number__r.Model_Number__c))
        };
    }

    private String buildItemKey(Inventory_Item__c item, String prefix) {
        return String.format('{0}/{1}/{2}', new List<String> {
            prefix,
            (String.isBlank(item.Lot_Number__c) ? '' : item.Lot_Number__c),
            (String.isBlank(item.Serial_Number__c) ? '' : item.Serial_Number__c)
        });
    }

    private String buildItemKey(Cycle_Count_Line_Item__c item) {
        return String.format('{0}/{1}/{2}', new List<String> {
            item.Scanned_Model_Number__c,
            (String.isBlank(item.Scanned_Lot_Number__c) ? '' : item.Scanned_Lot_Number__c),
            (String.isBlank(item.Scanned_Serial_Number__c) ? '' : item.Scanned_Serial_Number__c)
        });
    }

    //// VERIFICATIONS ////

    public static Set<Id> createVerifications(Id ccraId) {

        // purge any existing verifications
        List<Verification__c> verifications = new List<Verification__c>();

        Boolean isScanning = false;
        Set<Id> itemIds = new Set<Id>();
        for (Cycle_Count_Line_Item__c item : [
            select
                Actual_Quantity__c,
                Cycle_Count_Response_Analysis__r.OwnerId,
                Cycle_Count_Response_Analysis__r.Status__c,
                Other_Inventory_Item__c,
                Other_Inventory_Item__r.In_Transit__c,
                Other_Inventory_Item__r.SAP_Quantity__c,
                Other_Inventory_Item__r.Inventory_Location__r.Inventory_Data_ID__r.OwnerId,
                Scanned_Serial_Number__c,
                Scanned_Quantity__c,
                Surgery_Item__c
            from Cycle_Count_Line_Item__c
            where Cycle_Count_Response_Analysis__c = :ccraId
        ]) {
            itemIds.add(item.Id);
            if (item.Cycle_Count_Response_Analysis__r.Status__c == SCANNING) {
                isScanning = true;
                verifications.addAll(createVerifications(item));
            }
        }

        if (!verifications.isEmpty()) {
            //insert verifications;
            DML.save('CycleCountLineItemManager', verifications, false);
        }

        // push parent to Analyzing
        if (isScanning) {
            
            Cycle_Count_Response_Analysis__c ccra = new Cycle_Count_Response_Analysis__c(
                Id = ccraId,
                Status__c = ANALYZING
            );
            DML.save('CycleCountLineItemManager', ccra, false);
        }

        return itemIds;

    }

    private static List<Verification__c> createVerifications(Cycle_Count_Line_Item__c item) {

        List<Verification__c> newVerifications = new List<Verification__c>();

        if (item.Other_Inventory_Item__c != null) {     

            // is the item in the users inventory?
            if (item.Other_Inventory_Item__r.Inventory_Location__r.Inventory_Data_ID__r.OwnerId == item.Cycle_Count_Response_Analysis__r.OwnerId) {

                if (item.Scanned_Quantity__c == item.Other_Inventory_Item__r.SAP_Quantity__c) {

                    Double qty = String.isBlank(item.Scanned_Serial_Number__c)
                        ? item.Scanned_Quantity__c
                        : 1;

                    newVerifications.add(prodOnHandInInv(item.Id, qty));

                }
                else {

                    if (String.isBlank(item.Scanned_Serial_Number__c)) {

                        Integer remaining = 0;

                        if (item.Scanned_Quantity__c < item.Other_Inventory_Item__r.SAP_Quantity__c) {

                            newVerifications.add(prodOnHandInInv(item.Id, item.Scanned_Quantity__c));
                            remaining = (item.Other_Inventory_Item__r.SAP_Quantity__c - item.Scanned_Quantity__c).intValue();

                        }
                        else /* if (item.Scanned_Quantity__c > item.Other_Inventory_Item__r.SAP_Quantity__c) */{

                            if (item.Other_Inventory_Item__r.SAP_Quantity__c > 0) {
                                newVerifications.add(prodOnHandInInv(item.Id, item.Other_Inventory_Item__r.SAP_Quantity__c));
                                //*** added below if condition to avoid null pointer exception-- Amitabh
								if(item.Scanned_Quantity__c == null ){
									item.Scanned_Quantity__c = 0;
								}
                                remaining = (item.Scanned_Quantity__c - item.Other_Inventory_Item__r.SAP_Quantity__c).intValue();
                            }
                            else {
                                remaining = item.Scanned_Quantity__c.intValue();
                            }

                        }

                        if (item.Other_Inventory_Item__r.In_Transit__c > 0) {
                            newVerifications.add(newVerification(item.Id, item.Other_Inventory_Item__r.In_Transit__c, IN_TRANSIT, SHIP_INTRANSIT));
                            remaining -= item.Other_Inventory_Item__r.In_Transit__c.intValue();
                        }

                        for (Integer i = 0; i < Math.abs(remaining); i++) {
                            newVerifications.add(singleEmptyVerification(item.Id));
                        }

                    }
                    else {

                        newVerifications.add(singleEmptyVerification(item.Id));

                    }

                }

            }
            else if (String.isNotBlank(item.Scanned_Serial_Number__c)) {

                newVerifications.add(newVerification(item.Id, 1, '', TRANSFER));

            }
            else {

                for (Integer i = 0; i < item.Scanned_Quantity__c; i++) {
                    newVerifications.add(singleEmptyVerification(item.Id));
                }

            }


        }
        else if (item.Surgery_Item__c != null) {

            newVerifications.add(newVerification(item.Id, 1, USED_INSURGERY, USED_INSURGERY));

        }
        else {

            Decimal qty = item.Actual_Quantity__c != null
                ? item.Actual_Quantity__c
                : (item.Scanned_Quantity__c != null
                    ? item.Scanned_Quantity__c
                    : 1);

            for (Integer i = 0; i < qty; i++) {
                newVerifications.add(singleEmptyVerification(item.Id));
            }

        }

        return newVerifications;

    }

    //// Verification Helpers ////

    private static Verification__c prodOnHandInInv(Id parentId, Decimal qty) {
        return newVerification(parentId, qty, PROD_ONHAND, IN_INVENTORY);
    }

    private static Verification__c singleEmptyVerification(Id parentId) {
        return newVerification(parentId, 1, '', '');
    }

    private static Verification__c newVerification(Id parentId, Decimal qty, String verificationType, String reconcileType) {
        return new Verification__c(
            Cycle_Count_Line_Item__c = parentId,
            Quantity__c = qty,
            Verification__c = verificationType,
            Reconcile_Type__c = reconcileType
        );
    }

}
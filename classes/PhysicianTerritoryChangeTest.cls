// physTer.Pageupdate6Physician(); by AKG 
@isTest(SeeAllData=false)
public class PhysicianTerritoryChangeTest
{
    static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME = '/-.-NMD SCS Trial-Implant';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static final String OPPORTUNITY_TYPE1 = 'Implant Only';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LEAD_SOURCE = 'Care Card';
    static final NMD_TestDataManager td = new NMD_TestDataManager();
    
    static Account acctMaster= new Account();
    static Account insurance = new Account();
    static Account acctTrialing = new Account();
    static Account acctProcedure = new Account();
    static Contact trialing= new Contact();
    static Contact procedure= new Contact();
    static User usr = new User();
    static Seller_Hierarchy__c territory= new Seller_Hierarchy__c();
    static Assignee__c assign= new Assignee__c();
    static Opportunity trialOpty= new Opportunity();
    static Opportunity implantOpty= new Opportunity();
    static Opportunity implantOpty1= new Opportunity();
    static Patient__c patient  = new Patient__c();
    static   User CCRUser   = new User();
    static   User NMDRBMUser   = new User();
    
    static List <Seller_Hierarchy__c> FinalTerritoryList = new List<Seller_Hierarchy__c>();


    
    //@TestSetup
    static void setup() {
    //A Test.startTest();


  List<RowCauseObjects__c> settingList  = new List<RowCauseObjects__c>();
RowCauseObjects__c setting1 = new RowCauseObjects__c();
setting1.Name = 'Opportunity';
setting1.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,Team';
settingList.add(setting1);


RowCauseObjects__c setting2 = new RowCauseObjects__c();
setting2.Name = 'Patient';
setting2.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,NMDTerritoryChange__c';
settingList.add(setting2);

RowCauseObjects__c setting3 = new RowCauseObjects__c();
setting3.Name = 'Lead';
setting3.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
settingList.add(setting3);


RowCauseObjects__c setting4 = new RowCauseObjects__c();
setting4.Name = 'Contact';
setting4.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
settingList.add(setting4);


insert settingList;


         acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

         insurance = td.newAccount();
        insurance.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
        insurance.Status__c = 'Active';
        
         acctTrialing = td.newAccount(ACCT_TRIALING);
        acctTrialing.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing.AccountNumber = '1234';
        acctTrialing.Type = 'Sold to';

         acctProcedure = td.newAccount(ACCT_PROCEDURE);
        acctProcedure.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctProcedure.Type = 'Sold to';
        
        insert new List<Account> { acctMaster, acctTrialing, acctProcedure, insurance };

         trialing = td.newContact(acctTrialing.Id);
        trialing.AccountId = acctTrialing.id;
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

         procedure = td.newContact(acctProcedure.Id);
        procedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { trialing, procedure };
        
        /*Opportunity oppty1 = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        
        Opportunity oppty2 = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty2.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        
        insert new List<Opportunity> { oppty1, oppty2};*/
        
         territory = td.newSellerHierarchy();
         territory.Current_TM1_Assignee__c = NMDRBMUser.id;
        insert territory;
        FinalTerritoryList.add(territory);
       
       
        usr.Email = 'suser@boston.com';
        usr.LastName = 'LNAMETEST'; 
        usr.ProfileId = Label.NMD_RBM_Profile_Id;
        usr.UserName ='abc@boston.com';
        usr.Alias = 'standt'; 
        usr.EmailEncodingKey = 'UTF-8';
        usr.LanguageLocaleKey = 'en_US'; 
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Los_Angeles';
       // /*Cost_Center_Code__c = randomString5()*/;
        insert usr;  
        
        assign = td.newAssignee(usr.id,territory.id); 
        assign.Assignee__c =NMDRBMUser.id;
        insert assign;
        
        //assigning same master account to both opportunities.
         trialOpty = td.newOpportunity(acctMaster.Id, OPPTY_NAME_TRIAL);
        trialOpty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        trialOpty.Trialing_Account__c = acctTrialing.Id;
        trialOpty.Trialing_Physician__c = trialing.Id;
        trialOpty.Procedure_Account__c = acctProcedure.Id;
        trialOpty.Procedure_Physician__c = procedure.Id;
        trialOpty.Scheduled_Trial_Date_Time__c = system.today();
        trialOpty.Territory_ID__c = territory.Id;
        trialOpty.closeDate = System.today()+30;
        trialOpty.LeadSource = LEAD_SOURCE;
        trialOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
        trialOpty.CloseDate = system.today();
        //insert trialOpty;
        
         implantOpty = td.newOpportunity(acctMaster.Id, OPPTY_NAME_IMPLANT);
        implantOpty.Territory_ID__c = territory.Id;
        implantOpty.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        implantOpty.Scheduled_Trial_Date_Time__c = system.today();
        implantOpty.closeDate = System.today()+30;
        implantOpty.LeadSource = LEAD_SOURCE;
        implantOpty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        implantOpty.Procedure_Account__c = acctProcedure.Id;
        implantOpty.Procedure_Physician__c = procedure.Id;
        implantOpty.CloseDate = system.today();
        //insert implantOpty;
        
         implantOpty1 = td.newOpportunity(acctMaster.Id, OPPTY_NAME_IMPLANT);
        implantOpty1.Territory_ID__c = territory.Id;
        implantOpty1.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        implantOpty1.Scheduled_Trial_Date_Time__c = null;
        implantOpty1.Scheduled_Procedure_Date_Time__c = null;
        implantOpty1.closeDate = null;
        implantOpty1.LeadSource = LEAD_SOURCE;
        implantOpty1.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        implantOpty1.Procedure_Account__c = null;
        implantOpty1.Procedure_Physician__c = null;
        implantOpty1.CloseDate = system.today();
        
        insert new List<Opportunity> { trialOpty, implantOpty, implantOpty1};
        
         patient = td.newPatient('Fname', 'Lname');
        patient.Territory_ID__c=territory.Id;
        patient.Primary_Insurance__c = insurance.id;
        patient.Secondary_Insurance__c = insurance.id;
        patient.Physician_of_Record__c = trialing.id;
        patient.Referring_Physician__c = trialing.id;
        patient.RecordTypeId = PatientManager.RECTYPE_PROSPECT;
        insert patient;
        
        
                                                  
    }

  static testMethod void PhysicianTerritoryChangeTest()
  {
  
     //  List<PatientWrapper> PatientListSinglePhy = new List<PatientWrapper>();
  
     CCRUser =  NMD_TestDataManager.setupUser();
insert CCRUser;
       NMDRBMUser =  NMD_TestDataManager.setupUserNMDRBM();
insert NMDRBMUser;

        NMD_TestDataManager.createRowCauseObjects(); // Added by Ashish to resovle RowCasue error due to no data in Custom Setting

        system.runAs(CCRUser) 
        {
        
        setup();
        system.debug( trialing+'__procedure__'+procedure );
        Profile prof = [select id from profile where name = 'NMD RBM'];
    /*//List<User> user = [select id,ProfileId From User where ProfileId =: Label.NMD_RBM_Profile_Id];
    //Contact cont = [Select id,FirstName From Contact where RecordTypeId=: ContactManager.RECTYPE_PROSPECT Limit 1 ];
    //Seller_Hierarchy__c currentUserAsTM1 = [select id, Name, Current_TM1_Assignee__c from Seller_Hierarchy__c];
     //system.debug('currentUserAsTM1****'+currentUserAsTM1.size());
    Assignee__c currentUserAsAssignee = [select id, Name, Assignee__c,Territory__c,Territory__r.Name from Assignee__c];
    Patient__c patient = [Select id,name,Territory_ID__c,Patient_First_Name__c,Primary_Insurance__c,
                                                  Secondary_Insurance__c,Physician_of_Record__c,Referring_Physician__c,
                                                  Physician_of_Record__r.Name, Territory_ID__r.Name,Patient_Last_Name__c,
                                                  Physician_of_Record__r.Account.Account_Number__c,Physician_of_Record__r.Account.Name
                                                  From Patient__c where Physician_of_Record__c =: Cont.id];*/
  //  ApexPages.StandardController sc = new ApexPages.StandardController(Cont);  
  
      Test.startTest();
    ApexPages.StandardController sc = new ApexPages.StandardController(trialing);
    

    PhysicianTerritoryChange physTer = new PhysicianTerritoryChange(sc);
    
  string finalDate;
    //PatientListSinglePhy
    
          date d = date.today()-1;
       if(d.month()<10){
            system.debug('***In***'+d.month());
            physTer.datename  = d.year()+'-0'+d.month()+'-'+d.day();
            finalDate = physTer.datename;
        }
        else{
            physTer.datename  = d.year()+'-'+d.month()+'-'+d.day();
            finalDate = physTer.datename;
        }
    
    
   physTer.PatientList = new List<PhysicianTerritoryChange.Patientwrapper>();   
    
  PhysicianTerritoryChange.Patientwrapper PtWrapper = new PhysicianTerritoryChange.Patientwrapper(patient); 

  // List<Patientwrapper> PtWrapper.PatientListSinglePhy = new List<  Patientwrapper>();
     // List<PtWrapper> PatientListSinglePhy = new List<PtWrapper> ();
      
      
     // List<PhysicianTerritoryChange.Patientwrapper> physTer.PatientListSinglePhy;
     PhysicianTerritoryChange.Patientwrapper PtWrapper2 = new PhysicianTerritoryChange.Patientwrapper(patient,territory.Id,territory.Name,patient.Physician_of_Record__c ,trialing.name,''); 
     
           PtWrapper2.selected1 =true;
           

       //   PtWrapper2.Edoc =finalDate;
         // PtWrapper2.con =trialing;
             
    //        public Patientwrapper(Patient__c p, STring selectedVal1, string selectedName1, string selectedConName1, string FinalContactName, string moveoption)
     
         PhysicianTerritoryChange.Physicianwrapper PhysiciWrapper = new PhysicianTerritoryChange.Physicianwrapper(trialing);
         
         PhysicianTerritoryChange.Physicianwrapper PhysiciWrapper2 = new PhysicianTerritoryChange.Physicianwrapper(trialing,territory.Id,territory.Name,trialing.AccountId,acctTrialing.name,finalDate); 
         
          physTer.PhysicianList = new List<PhysicianTerritoryChange.Physicianwrapper>();
        
          /*
          PhysicianTerritoryChange.Physicianwrapper newOrderc  =  new PhysicianTerritoryChange.Physicianwrapper(trialing);
          newOrderc.selected =true;
          newOrderc.Edoc =finalDate;
          newOrderc.con =trialing;
          
          physTer.PhysicianList.add(newOrderc);
          
          */
          //physTer.PhysicianList = new List<PhysicianTerritoryChange.Physicianwrapper>();
          
          PhysicianTerritoryChange.Physicianwrapper newOrderc  =  new PhysicianTerritoryChange.Physicianwrapper(trialing,territory.Id,territory.Name,trialing.AccountId,acctTrialing.name,finalDate);
          
          newOrderc.selected =true;
          newOrderc.Edoc =finalDate;
          newOrderc.con =trialing;
          
/*
___newOrderc___Physicianwrapper:[Edoc=2016-07-28, accname=0011100001HaRFNAA3, con=Contact:{AccountId=0011100001HaRFNAA3, LastName=LastNameded24, RecordTypeId=012o0000000Sjm6AAC, Id=0031100001BVODhAAP}, name=40cec, seAccountName=TrialingAccount, selected=true, str=a0W11000004PI3CEAW]


______physTer.PhysicianList_____(Physicianwrapper:[Edoc=2016-07-28, accname=0011100001HaRbbAAF, con=Contact:{AccountId=0011100001HaRbbAAF, LastName=LastNamefd864, RecordTypeId=012o0000000Sjm6AAC, Id=0031100001BVObeAAH}, name=2ea8b, seAccountName=TrialingAccount, selected=true, str=a0W11000004PID9EAO], (already output))




*/        
          system.debug('___newOrderc___'+newOrderc);
         // physTer.PhysicianList.add(newOrderc);
         
           
            system.debug('______physTer.PhysicianList_____'+physTer.PhysicianList);
          
          
          
       /*  PhysiciWrapper.selected =true;
        
         PhysiciWrapper.Edoc =finalDate;
         PhysiciWrapper.con =trialing;
          PhysiciWrapper2.selected =true;
         PhysiciWrapper2.Edoc =finalDate;

         
         PhysiciWrapper2.con =trialing; */
    /*   physTer.PhysicianList.add(PhysiciWrapper);
         physTer.PhysicianList.add(PhysiciWrapper2);*/
         
         
        // displayList
         //PhysiciWrapper.selected =true;
         
//Physicianwrapper(Contact c, STring selectedVal, string selectedName, string selectedaccName, string FinalAccountName, string finalDate)        

    
    /*
selectedVal1 = territoryId
selectedName1 = territory name
selectedConName1 = Patients contact ID
FinalContactName = Patietns contact name
moveoption = its move option.. getSelected
    */  
    
    
    
    
    physTer.SelectedItemfilter =null;
    physTer.selectedPatients.add(patient);
    physTer.SelectedPatientList.add(patient);
    physTer.PatientListwithOutPhysician.add(patient);
    physTer.IscheckedPatient = true;
    physTer.newPatient=patient;
    physTer.newPatient1=patient;
    physTer.newContact = trialing;
    physTer.SelectedPhysicianList.add(trialing);
    physTer.FinalOutPutSelectedPhysicianList.add(trialing);
    physTer.getPatientsPage2();
    physTer.ChildMoveTypes = 'select a different territory and/or physician';
    physTer.ContactID = trialing.id;
    physTer.IsSelectedPatients1 = true;
    physTer.getChildMoveType();
    physTer.getselectedMoveType();
  // PatientListSinglePhy.add(physTer.getPatientsPage2());
    physTer.getFinalPatientsforPage3();
    
     physTer.getTerritories(); 
     physTer.getFinalContacts(); 
    
    
    physTer.getContact();
    physTer.getSelected12();
    physTer.getMovePatientToPage3();
   
    //physTer.filterPatient();
   // physTer.filterContact();
    physTer.getContacts();
    
              physTer.PhysicianList.add(newOrderc);
                          system.debug('2______physTer.PhysicianList_____'+physTer.PhysicianList);
    physTer.getSelected();
    
    
    system.debug('3______physTer.PhysicianList_____'+physTer.PhysicianList);
    physTer.getmovedCOntact();
    physTer.getPatients();
               physTer.PatientList.add(PtWrapper2);
    physTer.getSelected1();
    physTer.getmovedPatients();
    physTer.getFinalPatients();
    physTer.Pageupdate6Physician();
   // physTer.Pagepdate3();
    physTer.SelectedItem5 =insurance.id;
    physTer.SelectedItem6 =patient.Territory_ID__c;
     physTer.loggedInUserProfile =prof.id;

 physTer.filterPatient();
  



   // physTer.datename =Date.today();
  
        system.runAs(NMDRBMUser) {
        
            physTer.SelectedItemfilter =territory.Id;
           
                system.debug('____NMDRBMUser.id_____ '+prof.id);
 physTer.getTerritories(); 

  //physTer.filterContact();
  physTer.validateEffectiveDate();
 }
    }
       
  
      Test.stopTest();
  }
  
  static testMethod void PhysicianTerritoryChangeTest2()
  {
  
     CCRUser =  NMD_TestDataManager.setupUser();
insert CCRUser;
       NMDRBMUser =  NMD_TestDataManager.setupUserNMDRBM();
insert NMDRBMUser;


        system.runAs(CCRUser) 
        {
        
        setup();
          string finalDate;
        system.debug( trialing+'__procedure__'+procedure );
        Profile prof = [select id from profile where name = 'NMD RBM'];
    /*//List<User> user = [select id,ProfileId From User where ProfileId =: Label.NMD_RBM_Profile_Id];
    //Contact cont = [Select id,FirstName From Contact where RecordTypeId=: ContactManager.RECTYPE_PROSPECT Limit 1 ];
    //Seller_Hierarchy__c currentUserAsTM1 = [select id, Name, Current_TM1_Assignee__c from Seller_Hierarchy__c];
     //system.debug('currentUserAsTM1****'+currentUserAsTM1.size());
    Assignee__c currentUserAsAssignee = [select id, Name, Assignee__c,Territory__c,Territory__r.Name from Assignee__c];
    Patient__c patient = [Select id,name,Territory_ID__c,Patient_First_Name__c,Primary_Insurance__c,
                                                  Secondary_Insurance__c,Physician_of_Record__c,Referring_Physician__c,
                                                  Physician_of_Record__r.Name, Territory_ID__r.Name,Patient_Last_Name__c,
                                                  Physician_of_Record__r.Account.Account_Number__c,Physician_of_Record__r.Account.Name
                                                  From Patient__c where Physician_of_Record__c =: Cont.id];*/
  //  ApexPages.StandardController sc = new ApexPages.StandardController(Cont);  
  
          system.runAs(NMDRBMUser) {
          Test.startTest();
    ApexPages.StandardController sc = new ApexPages.StandardController(trialing);
    

    PhysicianTerritoryChange physTer = new PhysicianTerritoryChange(sc);
    
    
        PhysicianTerritoryChange.Patientwrapper PtWrapper = new PhysicianTerritoryChange.Patientwrapper(patient); 
        PhysicianTerritoryChange.Patientwrapper PtWrapper2 = new PhysicianTerritoryChange.Patientwrapper(patient,territory.Id,territory.Name,patient.Physician_of_Record__c ,trialing.name,''); 
  // List<Patientwrapper> PtWrapper.PatientListSinglePhy = new List<  Patientwrapper>();
     // List<PtWrapper> PatientListSinglePhy = new List<PtWrapper> ();
      
      
     // List<PhysicianTerritoryChange.Patientwrapper> physTer.PatientListSinglePhy;
     
    //        public Patientwrapper(Patient__c p, STring selectedVal1, string selectedName1, string selectedConName1, string FinalContactName, string moveoption)
    //PhysicianTerritoryChange.displayList1.get(0).pat = patient;
    //PtWrapper2.displayList1 =territory.Id;
    
        physTer.PhysicianList = new List<PhysicianTerritoryChange.Physicianwrapper>();
        
              PhysicianTerritoryChange.Physicianwrapper newOrderc1  =  new PhysicianTerritoryChange.Physicianwrapper(trialing,territory.Id,territory.Name,trialing.AccountId,acctTrialing.name,finalDate);
    
    
    physTer.SelectedItemfilter =territory.Id;
    physTer.selectedPatients.add(patient);
    physTer.PatientListwithOutPhysician.add(patient);
        physTer.SelectedPatientList.add(patient);
    physTer.IscheckedPatient = true;
    physTer.newPatient=patient;
    physTer.newPatient1=patient;
    physTer.newContact = trialing;
    physTer.SelectedPhysicianList.add(trialing);
    physTer.FinalOutPutSelectedPhysicianList.add(trialing);

    
    physTer.ChildMoveTypes = 'select a different territory and/or physician';
    physTer.ContactID = trialing.id;
    physTer.IsSelectedPatients1 = true;
    physTer.getChildMoveType();
    physTer.getselectedMoveType();
    physTer.getPatientsPage2();
   //  PatientListSinglePhy =   physTer.getPatientsPage2();
    physTer.getFinalPatientsforPage3();
    

    physTer.getContact();
    physTer.getSelected12();
    physTer.getMovePatientToPage3();

    //physTer.filterPatient();
   // physTer.filterContact();
    physTer.getContacts();
    physTer.getFinalContacts(); 
    
               date    d = date.today()+2;
       if(d.month()<10){
            system.debug('***In***'+d.month());
            physTer.datename  = d.year()+'-0'+d.month()+'-'+d.day();
            finalDate = physTer.datename;
        }
        else{
        
            physTer.datename  = d.year()+'-'+d.month()+'-'+d.day();
            finalDate = physTer.datename;
          }
          
      
          newOrderc1.selected =true;
          newOrderc1.Edoc =finalDate;
          newOrderc1.con =trialing;
    
     //    physTer.PhysicianList.add(newOrderc1);
    
    
       physTer.getSelected();
   // physTer.getSelected();
    physTer.getmovedCOntact();
    physTer.getPatients();
    
    physTer.getSelected1();
    physTer.getmovedPatients();
    physTer.getFinalPatients();

    physTer.Pagepdate3();
    physTer.SelectedItem5 =insurance.id;
    physTer.SelectedItem6 =patient.Territory_ID__c;
     physTer.loggedInUserProfile =prof.id;

  
//filterPatient

       d = date.today()-1;
       if(d.month()<10){
            system.debug('***In***'+d.month());
            physTer.datename  = d.year()+'-0'+d.month()+'-'+d.day();
        }
        else{
            physTer.datename  = d.year()+'-'+d.month()+'-'+d.day();
        }
   // physTer.datename =Date.today();
       physTer.validateEffectiveDate();
  

           
                system.debug('____NMDRBMUser.id_____ '+prof.id);
        physTer.getTerritories(); 
        physTer.filterPatient();
        physTer.filterContact();
        physTer.Pageupdate6Physician();
 }
    }
       
  
      Test.stopTest();
  }
  
  
}
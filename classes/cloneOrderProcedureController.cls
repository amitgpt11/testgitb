/*
 @CreatedDate     13Sept2016                                  
 @ModifiedDate    13Sept2016                                  
 @author          Ashish-Accenture
 @Description     Clone Orders and Procedures from the Order Detail page. 
 @Methods         
  
 */
public with sharing class cloneOrderProcedureController{
    Id orderId;
    Id procedureId;
    public list<Shared_Patient_Case__c> lstProcedures;
    public list<Order> lstOrderClone;
    public Order orderObj{get;set;}
    public Shared_Patient_Case__c procedureObj{get;set;}
    public Shared_Patient_Case__c procedureObjDef{get;set;}
    public list<orderitem> lstOrderItem{get;set;}
    public string ShippingAddress{get;set;}
    
    public String EstShippingDate{get;set;}
    public date EstShipDate;
    public string shippingCountry;
    public String ErrorMsg{get;set;}
    public boolean isOrderReq{get;set;} 
    public String modeOfTransport{get;set;}
    public boolean isShipToUser;
    public Id cloneOrderId{get;set;}
    public string defCourier;
     public string attentionLine{get;set;}
     public Boolean buttonEnabled {get;set;} 
     public Date finalDeliveryDate;
    
    public cloneOrderProcedureController(ApexPages.StandardController controller) {
        buttonEnabled = false; 
        orderId = controller.getId();
        orderObj = new Order();
        isOrderReq = true;
        isShipToUser = false;
        procedureObj = new Shared_Patient_Case__c();
        procedureObjDef = new Shared_Patient_Case__c();
        list<Order> lstOrder = [Select id,name,AccountId,Account.Name,ownerId,Owner.Name,Uro_PH_Patient_Case__c,EffectiveDate,Status,
                                shippingStreet,shippingCity,shippingPostalCode,shippingState,shippingCountry,PriceBook2Id,Uro_PH_Mode_of_Transport__c,Uro_PH_IsShipToUser__c,Uro_PH_Kit_Name__c,RecordTypeId,Shared_Attention_line__c,Shared_Billing_Block__c  from Order where id=:orderId];
        if(lstOrder.size() > 0){
           // orderObj = lstOrder.get(0).clone(false);
          orderObj  = lstOrder.get(0);
          ShippingAddress = (String.IsNotBlank(orderObj.shippingStreet) ? orderObj.shippingStreet+', ' : '') +
                             (String.IsNotBlank(orderObj.shippingCity) ? orderObj.shippingCity+' ' : '')+
                             (String.IsNotBlank(orderObj.shippingPostalCode) ? orderObj.shippingPostalCode+', ' : '')+
                             (String.IsNotBlank(orderObj.shippingState) ? orderObj.shippingState+', ' : '')+
                             (String.IsNotBlank(orderObj.shippingCountry) ? orderObj.shippingCountry : '');
          
          attentionLine = orderObj.Shared_Attention_line__c;                   
           isShipToUser = orderObj.Uro_PH_IsShipToUser__c;
            procedureId = lstOrder.get(0).Uro_PH_Patient_Case__c;
            lstProcedures = [select id,name,Uro_PH_Alternate_BSCI_Resource__c,Uro_PH_BSCI_Per_Diem_Contact__c,RecordTypeId, /*Shared_Start_Date_Time__c,Shared_End_Date_Time__c,*/
                            Shared_Primary_Case_Contact__c,Shared_Hospital_Contact__c,Shared_Facility__c,Shared_Facility__r.ShippingCountry,Uro_PH_PO__c,UROPH_Implanting_Physician__c,UROPH_Implanting_Physician__r.Name from Shared_Patient_Case__c where id = :procedureId];
            if(lstProcedures.size() > 0){
              //  procedureObj = lstProcedures.get(0).clone(false);
               procedureObj = lstProcedures.get(0);
               procedureObj.Uro_PH_PO__c = '';
               procedureObj.Uro_PH_BSCI_Per_Diem_Contact__c = null;
               procedureObj.Uro_PH_Alternate_BSCI_Resource__c = null;
               shippingCountry = procedureObj.Shared_Facility__r.shippingCountry;
               System.debug('==procedureObj=='+procedureObj);
            }        
                            
        }
        
        lstOrderItem = [Select id,UnitPrice,Quantity,OrderId,ServiceDate,EndDate,PricebookEntryId,PricebookEntry.ProductCode,PricebookEntry.Product2.Name,Uro_Ph_Loaner_Kit_Id__c,PricebookEntry.Product2.UPN_Material_Number__c from orderitem where orderId = :orderId];
        
        Map<String,RestrictedLoanerKitProduct__c> resProd =  RestrictedLoanerKitProduct__c.getall();
         
        for(orderitem item : lstOrderItem){
            if(resProd.containskey(item.PricebookEntry.Product2.UPN_Material_Number__c)){
                item.Quantity = 0;   
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Restricted_Loaner_Kit_Product_Message)); 
            }
        }
        
        BusinessCloseTimeCountry__c businessCust = BusinessCloseTimeCountry__c.getValues(shippingCountry);
        if(businessCust != null ){
           if(businessCust.Default_MOT__c != null){ 
               modeOfTransport = businessCust.Default_MOT__c;
           }
           if(businessCust.Default_Courier__c != null){
               defCourier = businessCust.Default_Courier__c;
           }
        }
    }
    
    public pagereference saveClone(){
    buttonEnabled = true;
        Savepoint sp = Database.setSavepoint();
        Pagereference pg;
        try{
            Shared_Patient_Case__c procedureObjClone = new Shared_Patient_Case__c();
            procedureObjClone = procedureObj.clone(false);
            procedureObjClone.Shared_Status__c = 'Open';
            procedureObjClone.Shared_Start_Date_Time__c = procedureObjDef.Shared_Start_Date_Time__c;
            procedureObjClone.Shared_End_Date_Time__c = procedureObjDef.Shared_End_Date_Time__c;
            System.debug('===procedureObjClone=='+procedureObjClone);
            if(procedureObjClone.Shared_Start_Date_Time__c > procedureObjClone.Shared_End_Date_Time__c){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Your End Date/Time must be greater than or equal to your Start Date/Time.'));
                buttonEnabled = false;
                return null;
            }
            insert procedureObjClone;
            
            Order orderObjClone = new Order();
            orderObjClone = orderObj.clone(false);
            System.debug('===orderObjClone=='+orderObjClone);
            orderObjClone.Uro_PH_Patient_Case__c = procedureObjClone.Id;
            orderObjClone.Uro_PH_Alternate_BSCI_Resource__c = procedureObjClone.Uro_PH_Alternate_BSCI_Resource__c;
            orderObjClone.PoNumber =  procedureObjClone.Uro_PH_PO__c;
            orderObjClone.Status = 'Draft';
            orderObjClone.ANZ_Implanting_Physician__c = procedureObjClone.UROPH_Implanting_Physician__c;
            orderObjClone.Surgeon__c = procedureObjClone.UROPH_Implanting_Physician__c;
            orderObjClone.OwnerId = UserInfo.getUserId();
            orderObjClone.Uro_PH_Mode_of_Transport__c=modeOfTransport;
            orderObjClone.Type = 'ZKB';
            orderObjClone.Order_Reason__c = 'G35';
            orderObjClone.Surgery_Date__c = procedureObjDef.Shared_Start_Date_Time__c.Date();
            orderObjClone.Ship_Date__c =  EstShipDate;
            orderObjClone.Shared_Courier__c = defCourier;
            orderObjClone.Shared_Attention_line__c = attentionLine;
            orderObjClone.Uro_PH_Delivery_Date__c = finalDeliveryDate;
            //orderObjClone.Shared_Line_Items_Complete__c = true; // Defect sfdc - 24062
            
            String billingblock;
            if(orderObjClone.ShippingCountry != null){
                if((orderObjClone.ShippingCountry == 'US' || orderObjClone.ShippingCountry == 'GB') && String.IsNotBlank(orderObjClone.PoNumber) && orderObjClone.PoNumber.equalsIgnoreCase('x')){
                    billingblock = '56';
                }else if((orderObjClone.ShippingCountry == 'US') && String.IsNotBlank(orderObjClone.PoNumber) && !orderObjClone.PoNumber.equalsIgnoreCase('x') && EstShipDate != Date.Today()){
                    billingblock = '57';
                }
            }
            System.debug('====billingblock=='+billingblock);
            orderObjClone.Shared_Billing_Block__c = billingblock;
            
            insert orderObjClone;
            cloneOrderId = orderObjClone.Id;
            
            list<orderitem> lstOrderItemClone = new list<orderitem>();
            orderitem itemObj;
            for(orderitem item : lstOrderItem){
                if(item.Quantity !=  null && item.Quantity > 0){
                    itemObj = new orderitem();
                    itemObj = item.Clone(false);
                    itemObj.OrderId = orderObjClone.Id;
                   // itemObj.PricebookEntryId = item.PricebookEntryId;
                    lstOrderItemClone.add(itemObj);
                }
            }
            System.debug('===lstOrderItemClone==='+lstOrderItemClone);
            if(lstOrderItemClone.size() > 0){
                insert lstOrderItemClone;
            }
            pg = new pagereference('/'+orderObjClone.Id);
        }
        catch(Exception e){
            System.debug('###==e=='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            Database.rollback(sp);
            buttonEnabled = false;
        }
       // return pg;
       return null;
    }
    
    
    
     //=====Added by Ashish==To call REST API===Sfdc-1489==Start===
    //@future(callout=true)
    public void callOrderRESTService(){
       try{
           System.debug(LoggingLevel.Info,'===cloneOrderId===='+cloneOrderId);
           if (this.cloneOrderId != null){
            Order ord = OrderAPIService.submitOrderRequest(this.cloneOrderId,'Insert');
            System.debug('====Respnonse Order========'+ord);
            if(ord != null && String.IsNotBlank(ord.Shared_SAP_Order_Number__c)){
                ord.Sales_Order_No__c = ord.Shared_SAP_Order_Number__c;
                ord.Stage__c = 'Ordered';
                Update ord;
                
                list<OrderItem> lstOrderItem = [Select id,Order.Sales_Order_No__c,External_Id__c,Order_Item_Id__c from OrderItem where OrderId =: ord.id];
                for(OrderItem oi : lstOrderItem){
                    oi.External_Id__c = oi.Order.Sales_Order_No__c + '-' +(oi.Order_Item_Id__c != null ? String.ValueOf(oi.Order_Item_Id__c) : '');
                }
                if(lstOrderItem.size() > 0){
                    update lstOrderItem;
                }
                
            }
          //  return new PageReference('/' + this.cloneOrderId);
           }
       }Catch(exception e){
           System.debug('###===e==='+e);
           Application_Log__c error = new Application_Log__c();
           // error.Integration_Payload__c = req.getBody();
            error.Stack_Trace__c = e.getStackTraceString();
            error.Order__c = (Id)cloneOrderId;
            error.Log_Code__c = e.getTypeName();
            
            System.debug('===e.getMessage()===='+e.getMessage());
            error.Message__c = e.getMessage().length() > 255 ? e.getMessage().substring(0,254) : e.getMessage();
             
            insert error;
           // throw e;      
          // Database.rollback( sp );
       }
       // return null;
    }
    //=====Added by Ashish==To call REST API===Sfdc-1489==End===
    
    public void calcShippingDate(){
        System.debug('===procedureObjDef.Shared_Start_Date_Time__c==='+procedureObjDef.Shared_Start_Date_Time__c);
        System.debug('===shippingCountry==='+shippingCountry);
        ErrorMsg ='';
        isOrderReq =true;
        //modeOfTransport = '';
        Date tempEstShippingDate;
        boolean isDeliveryDateCalculated = false;
        
        // To change Start date to previouse working day ===Start===11/24===
          list<Uro_PH_Non_Working_Day__c> lstNonWorkingDays = OrderCreationController.fetchLstNonWorkingDays(shippingCountry,procedureObjDef.Shared_Start_Date_Time__c);
          Set<String> setNonWorkingDate = new Set<String>();
          for(Uro_PH_Non_Working_Day__c NWD : lstNonWorkingDays){
              if(!shippingCountry.equalsIgnoreCase('GB') || (shippingCountry.equalsIgnoreCase('GB') && (String.IsBlank(NWD.Uro_PH_Description__c) || (String.IsNoTBlank(NWD.Uro_PH_Description__c) && !NWD.Uro_PH_Description__c.equalsIgnoreCase('Saturday'))))){
                  setNonWorkingDate.add(datetime.newInstance(NWD.Uro_PH_Date__c.year(), NWD.Uro_PH_Date__c.month(),NWD.Uro_PH_Date__c.day()).format('yyyyMMdd'));
              }
          }
          Datetime ProcStartDatetime = procedureObjDef.Shared_Start_Date_Time__c;
          if(!shippingCountry.equalsIgnoreCase('US')){ // Added condition after Mike's call. Scenario #2 for US has been changed in "Ship date Rules - All Countries" excel file.==6th december==by Ashish=
              for(integer i=1; i<= 1; i++){
                    
                    System.debug('====ProcStartDatetime===='+ProcStartDatetime);
                    System.debug('====Set===='+datetime.newInstance(ProcStartDatetime.year(), ProcStartDatetime.month(),ProcStartDatetime.day()).format('yyyyMMdd'));
                    if(setNonWorkingDate.contains(datetime.newInstance(ProcStartDatetime.year(), ProcStartDatetime.month(),ProcStartDatetime.day()).format('yyyyMMdd'))){
                        i--;
                        ProcStartDatetime = ProcStartDatetime.addDays(-1);
                    }
               }
           } 
            System.debug('====ProcStartDatetime===='+ProcStartDatetime);
            // To change Start date to previouse working day ===End===11/24===
        
        String todayDateTime = datetime.newInstance(Date.today().year(), Date.today().month(),Date.today().day()).format('yyyyMMdd');
        if(isShipToUser && (shippingCountry.equalsIgnoreCase('US') || shippingCountry.equalsIgnoreCase('AU') || shippingCountry.equalsIgnoreCase('NZ')) && !setNonWorkingDate.contains(todayDateTime)
               && ProcStartDatetime.format('EEEE').equalsIgnoreCase('Monday') && System.Now().format('EEEE').equalsIgnoreCase('Friday') &&
               System.Now().Date().daysbetween(ProcStartDatetime.Date()) == 3){
               
                tempEstShippingDate = Date.today();    
                finalDeliveryDate = Date.today().adddays(1);  // Added for Ship to me #ShipDateRefactor ==11/25==
                isDeliveryDateCalculated = true;
        }else{
            tempEstShippingDate = OrderCreationController.calculateShippingDate(shippingCountry, ProcStartDatetime, lstNonWorkingDays);
            isDeliveryDateCalculated = false;
        }
        if(tempEstShippingDate != null){
            EstShipDate = tempEstShippingDate;
            EstShippingDate = tempEstShippingDate.format();
            BusinessCloseTimeCountry__c businessCust = BusinessCloseTimeCountry__c.getValues(shippingCountry);
            Datetime finalCutOffTime= OrderCreationController.calculatefinalCutOffDateTime(shippingCountry,tempEstShippingDate);
            if(finalCutOffTime != null){
                Datetime currentDateTimeGMT = System.now();
                System.debug('==currentDateTimeGMT =='+currentDateTimeGMT );
                System.debug('==finalCutOffTime =='+finalCutOffTime );
                if(finalCutOffTime < currentDateTimeGMT ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'You have past the deadline to order a kit for this procedure.  Please call customer service at '+businessCust.Customer_Service_Number__c));
                    ErrorMsg = 'You have passed the deadline to order a kit for this procedure.  Please call customer service at '+businessCust.Customer_Service_Number__c;
                    isOrderReq =false;
                    return;
                }
             } 
         }
         System.debug('===modeOfTransport ==='+modeOfTransport );
         if(tempEstShippingDate != null){
             String modeOfTransportNew = OrderCreationController.calcModeOfTransport(shippingCountry,ProcStartDatetime,tempEstShippingDate,isShipToUser,lstNonWorkingDays);
             
             System.debug('===modeOfTransportNew==='+modeOfTransportNew);
             if(String.IsnotBlank(modeOfTransportNew)){
                 System.debug('===modeOfTransportNew in==='+modeOfTransportNew);
                   modeOfTransport = modeOfTransportNew;
             }
             System.debug('===modeOfTransport ==='+modeOfTransport );
             
             System.debug('====finalDeliveryDate1===='+finalDeliveryDate);
             if(!isDeliveryDateCalculated){
                 finalDeliveryDate = OrderCreationController.calculateDeliveryDate(shippingCountry,tempEstShippingDate,lstNonWorkingDays);
             }
             if(finalDeliveryDate > ProcStartDatetime.Date()){
                   finalDeliveryDate = ProcStartDatetime.Date();
             }
               System.debug('====finalDeliveryDate===='+finalDeliveryDate);
         }
    }


}
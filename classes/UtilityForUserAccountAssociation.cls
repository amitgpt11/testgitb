/*
 @CreatedDate     30MAR2016                                  
 @ModifiedDate    04APR2016                                  
 @author          Mayuri-Accenture
 @Description     Utility class used by batch classes batchOpptyTerritoryUpdate,batchMyObjTerritoryUpdate,batchAssignMyObjectivesToEuropeUsers,batchAccountTerritoryUpdate,batchUserTerritoryUpdate.
 @Methods         batchAccountTerritoryUpdate, 
 @Requirement Id  
 */
public without sharing class UtilityForUserAccountAssociation{
    
    /*
    @MethodName    userAccountTerritoryUpdate                                  
    @Parameters    atmLst: List of Account_team_Members records.
                   tempAtmLst: List of Account_team_Members records that are either to be removed or not removed.
                   isAccount: True/False. If TRUE, update ObjectTerritory2Association object, if FALSE, update UserTerritoryAssociation object
                   isDelete: True/False. If TRUE, remove the existing territory association, if FALSE, insert new territory association
    @Return Type   boolean
    @Description   Adds/Removes the Account/User territory association.
    */
    public boolean userAccountTerritoryUpdate(list<Account_Team_Member__c> atmLst,list<Account_Team_Member__c> tempAtmLst,boolean isAccount,boolean isDelete)
    {
        boolean isSuccess = false;
        integer successCount = 0;
        set<string> terrotoryNamelst = new set<string>();
        map<string,Id> territoryNameIdMap = new map<string,Id>();
        map<Id,Territory2> territory2Map;
        set<Id> acctTerritoryIdList = new set<Id>();
        set<Id> userTerritoryIdList = new set<Id>();
        list<ObjectTerritory2Association> acctTerritoryLst = new list<ObjectTerritory2Association>();
        list<UserTerritory2Association> userTerritoryLst = new list<UserTerritory2Association>();
        map<Id,ObjectTerritory2Association> acctTerritoryDeleteMap = new map<Id,ObjectTerritory2Association>();
        map<Id,UserTerritory2Association> userTerritoryDeleteMap = new map<Id,UserTerritory2Association>();
        list<ObjectTerritory2Association> assignAcctToTerritory = new list<ObjectTerritory2Association>(); 
        list<UserTerritory2Association> assignUserToTerritory = new list<UserTerritory2Association>(); 
        map<Id,set<Id>> acctTerritoryMap = new map<Id,set<Id>>();
        map<Id,set<Id>> userTerritoryMap = new map<Id,set<Id>>();
        //added to resolve stabalization issue (dulicate value for user association)
        map<Id,User> userInfoMap = new map<Id,User>();   //map to hold user Id and user info
        
        for(Account_Team_Member__c a : atmLst)
        {   
            terrotoryNamelst.add(a.Territory_ID__c);
            acctTerritoryIdList.add(a.Account_Team_Share__c);
            if(a.Personnel_ID__r.User_for_account_team__c != null){                 //Added for Q3 2016 to skip ENDO ATM records which do not have Personnel Ids
                userTerritoryIdList.add(a.Personnel_ID__r.User_for_account_team__c);
            }
        }
        system.debug('Lists: '+terrotoryNamelst+'\n'+acctTerritoryIdList+'\n'+userTerritoryIdList);
        system.debug('terrotoryNamelst.count:'+terrotoryNamelst.size()+' and acctTerritoryIdList count'+acctTerritoryIdList.size()+' and userTerritoryIdList count'+userTerritoryIdList.size());
        if(terrotoryNamelst.size()>0){
                //Query Id and other fields related to Territory2 object
                territory2Map = new map<Id,Territory2>();
                territory2Map = getTerritoryDetailList(terrotoryNamelst);
         }                  
         if(territory2Map != null && territory2Map.keyset().size() > 0){
             for(Id tId : territory2Map.keyset()){
                 territoryNameIdMap.put(territory2Map.get(tId).name,tId);
             }
         }
        system.debug('territoryNameIdMap size: '+territoryNameIdMap.size());
         
         //Query for existing AccountTerritory2Associaton records
         if(acctTerritoryIdList!= null && acctTerritoryIdList.size() > 0){

            acctTerritoryMap = getAcctnTeritryAssctnList(acctTerritoryIdList);
            acctTerritoryLst = getObject2AssctnList(acctTerritoryIdList);                                               
         }

         //Query for existing UserTerritory2Associaton records
         if(userTerritoryIdList != null && userTerritoryIdList.size() > 0){
                
                userTerritoryLst = getUserTeritryAssctnList(userTerritoryIdList,true);                                                       
         }
         system.debug('userTerritoryLst :'+userTerritoryLst);
         //Create a map of Account Id and its list of assigned territories
         if(userTerritoryLst != null && userTerritoryLst.size()>0){
             for(UserTerritory2Association obj : userTerritoryLst){
                 if(userTerritoryMap.containsKey(obj.UserId)){
                     userTerritoryMap.get(obj.UserId).add(obj.Territory2Id);
                 }
                 else
                 {
                     userTerritoryMap.put(obj.UserId,new set<Id>{obj.Territory2Id});
                 }
             }
         }
         system.debug('userTerritoryMap :'+userTerritoryMap);
         system.debug('--isAccount--'+isAccount);
         system.debug('--isDelete--'+isDelete);
         if(isAccount == TRUE && isDelete == TRUE){ 
            if(acctTerritoryLst != null && acctTerritoryLst.size()>0){            
                for(Account_Team_Member__c atm : tempAtmLst){
                    system.debug('--atm--'+atm);
                    for(ObjectTerritory2Association obj : acctTerritoryLst){
                        system.debug('--obj--'+obj);
                        if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                            if(atm.Account_Team_Share__c == obj.ObjectId && obj.Territory2Id == territoryNameIdMap.get(atm.Territory_ID__c)){
                                acctTerritoryDeleteMap.put(obj.Id,obj);
                            }
                        }
                    }                
                }        
            }            
           
            if(acctTerritoryDeleteMap != null && acctTerritoryDeleteMap.keyset().size() > 0){
                List<Id> IdList = new List<Id>();
                IdList.addAll(acctTerritoryDeleteMap.keyset());
                isSuccess = DeleteShareRecords(IdList);                           
            }
            else{
                isSuccess = true;
            }           
                     
        }
        else if(isAccount == TRUE && isDelete == FALSE){
        
            map<Id,set<Id>> tempInsertRecordMap = new map<id,set<Id>>();            
            
                system.debug(tempAtmLst+'<--tempAtmLst');
                for(Account_Team_Member__c atm : tempAtmLst){
                    if(acctTerritoryMap.keyset().contains(atm.Account_Team_Share__c)){                          
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                                if(acctTerritoryMap.get(atm.Account_Team_Share__c).contains(territoryNameIdMap.get(atm.Territory_ID__c)) == false){
                                    if(tempInsertRecordMap.containsKey(atm.Account_Team_Share__c)){
                                      tempInsertRecordMap.get(atm.Account_Team_Share__c).add(territoryNameIdMap.get(atm.Territory_ID__c));
                                    }else{
                                      tempInsertRecordMap.put(atm.Account_Team_Share__c,new set<id>{territoryNameIdMap.get(atm.Territory_ID__c)});
                                    }
                                }
                            }
                    }
                    else{
                        if(tempInsertRecordMap.containsKey(atm.Account_Team_Share__c)){
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                                if(tempInsertRecordMap.get(atm.Account_Team_Share__c).contains(territoryNameIdMap.get(atm.Territory_ID__c)) == false){
                                    tempInsertRecordMap.get(atm.Account_Team_Share__c).add(territoryNameIdMap.get(atm.Territory_ID__c));
                                }
                            }    
                         }else{
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                              tempInsertRecordMap.put(atm.Account_Team_Share__c,new set<id>{territoryNameIdMap.get(atm.Territory_ID__c)});
                            }  
                         }
                    }
                    
                }        
            

            if(tempInsertRecordMap!= null){
                for(Id aId : tempInsertRecordMap.keyset()){
                    if(tempInsertRecordMap.get(aId).size() > 0){
                        for(Id tId : tempInsertRecordMap.get(aId)){
                            ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
                            accountAssociation.ObjectId = aId;
                            accountAssociation.Territory2Id = tId;
                            accountAssociation.AssociationCause = 'Territory2Manual';
                            assignAcctToTerritory.add(accountAssociation);
                        }
                    }
                }
            }
            system.debug('assignAcctToTerritory --'+assignAcctToTerritory +'--');
            if(assignAcctToTerritory != NULL && assignAcctToTerritory.size()>0){
                    Database.SaveResult[] AccountInsertResult = Database.insert(assignAcctToTerritory,TRUE);
                            
                    // Error handling code Iterate through each returned result
                    for (Database.SaveResult er : AccountInsertResult) {
                        if (er.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully inserted Share Record. Record ID: ' + er.getId());
                            successCount++;
                        }
                        else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : er.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Record fields that affected this error: ' + err.getFields());
                            }
                        }
                    }                   
             } 
             if(successCount == assignAcctToTerritory.size()){
                 isSuccess = true;
             } 
             else{
                isSuccess = false;
             }           
        
        }
        else if(isAccount == FALSE && isDelete == TRUE){
        
            if(userTerritoryLst != null && userTerritoryLst.size()>0){
                for(Account_Team_Member__c atm : tempAtmLst){
                    if(atm.Personnel_ID__c != null){        //Added for Q3 2016 to skip ENDO ATM records which do not have Personnel Ids
                        for(UserTerritory2Association obj : userTerritoryLst){
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                                if(atm.Personnel_ID__r.User_for_account_team__c == obj.UserId && obj.Territory2Id == territoryNameIdMap.get(atm.Territory_ID__c)){
                                    userTerritoryDeleteMap.put(obj.Id,obj);
                                }
                            }
                        }
                    }
                }        
            }
            
            system.debug('userTerritoryDeleteMap--'+userTerritoryDeleteMap+'--');
            if(userTerritoryDeleteMap != null && userTerritoryDeleteMap.keyset().size() > 0){
                List<Id> IdList = new List<Id>();
                IdList.addAll(userTerritoryDeleteMap.keyset());
                isSuccess = DeleteShareRecords(IdList);
            } 
            else{
                isSuccess = true;
            } 
            system.debug('isSuccess--'+isSuccess+'--');           
        }
        else if(isAccount == FALSE && isDelete == FALSE){
            map<Id,set<Id>> tempInsertRecordMap = new map<id,set<Id>>();  
            map<Id,string> ATMRolemap = new map<Id,string>();          
                       
        system.debug('tempAtmLst '+tempAtmLst);
            for(Account_Team_Member__c atm : tempAtmLst){
                if(atm.Personnel_ID__c != null){        //Added for Q3 2016 to skip ENDO ATM records which do not have Personnel Ids
                    if(userTerritoryMap.keyset().contains(atm.Personnel_ID__r.User_for_account_team__c)){
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                                if(userTerritoryMap.get(atm.Personnel_ID__r.User_for_account_team__c).contains(territoryNameIdMap.get(atm.Territory_ID__c)) == false){
                                    if(tempInsertRecordMap.containsKey(atm.Personnel_ID__r.User_for_account_team__c)){
                                      tempInsertRecordMap.get(atm.Personnel_ID__r.User_for_account_team__c).add(territoryNameIdMap.get(atm.Territory_ID__c));
                                        system.debug(' IF >>adding in tempInsertRecordMap territory: '+territoryNameIdMap.get(atm.Territory_ID__c)+' for user:'+ atm.Personnel_ID__r.User_for_account_team__c);
                                    }else{
                                        if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                                            tempInsertRecordMap.put(atm.Personnel_ID__r.User_for_account_team__c,new set<id>{territoryNameIdMap.get(atm.Territory_ID__c)});
                                            system.debug(' ELSE >>adding in tempInsertRecordMap territory: '+territoryNameIdMap.get(atm.Territory_ID__c)+' for user:'+ atm.Personnel_ID__r.User_for_account_team__c);
                                            ATMRolemap.put(atm.Personnel_ID__r.User_for_account_team__c,atm.Account_Team_Role__c);
                                        }
                                    }
                                }   
                            }
                    } 
                    else{                
                        system.debug('inside impossible else 3---');
                        if(tempInsertRecordMap.containsKey(atm.Personnel_ID__r.User_for_account_team__c)){
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                                if(tempInsertRecordMap.get(atm.Personnel_ID__r.User_for_account_team__c).contains(territoryNameIdMap.get(atm.Territory_ID__c)) == false){
                                    system.debug('inside else loop 3---');
                                  tempInsertRecordMap.get(atm.Personnel_ID__r.User_for_account_team__c).add(territoryNameIdMap.get(atm.Territory_ID__c));
                                }
                            }
                         }else{                         
                            if(territoryNameIdMap.get(atm.Territory_ID__c) != null){
                              tempInsertRecordMap.put(atm.Personnel_ID__r.User_for_account_team__c,new set<id>{territoryNameIdMap.get(atm.Territory_ID__c)});
                              ATMRolemap.put(atm.Personnel_ID__r.User_for_account_team__c,atm.Account_Team_Role__c);
                            }  
                         }
                    }  
                }
            }        
            
            system.debug('final temp records :'+tempInsertRecordMap);
            if(tempInsertRecordMap!= null){
                if(tempInsertRecordMap.keyset().size() > 0){
                userinfoMap = getUserInfoMap(tempInsertRecordMap.keyset());
                }
                for(Id aId : tempInsertRecordMap.keyset()){
                    if(tempInsertRecordMap.get(aId).size() > 0){
                        for(Id tId : tempInsertRecordMap.get(aId)){                         
                                UserTerritory2Association userAssociation = new UserTerritory2Association();                            
                                userAssociation.UserId = aId;
                                userAssociation.Territory2Id = tId;
                                if(ATMRolemap.keyset().contains(aId) && ATMRolemap.get(aId) != null){
                                    userAssociation.RoleInTerritory2 = ATMRolemap.get(aId); 
                                }  
                                if(userInfoMap != null && userinfoMap.keyset().contains(aId) && userinfoMap.get(aId).isActive == true){                        
                                    assignUserToTerritory.add(userAssociation);
                                }
                        }
                    }
                }
            }
            system.debug('assignUserToTerritory--'+assignUserToTerritory+'--');
            if(assignUserToTerritory != NULL && assignUserToTerritory.size()>0){
                    Database.SaveResult[] UserInsertResult = Database.insert(assignUserToTerritory,TRUE);
                            
                    // Error handling code Iterate through each returned result
                    for (Database.SaveResult er : UserInsertResult) {
                        if (er.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully inserted Share Record. Record ID: ' + er.getId());
                            successCount++;
                        }
                        else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : er.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Record fields that affected this error: ' + err.getFields());
                            }
                        }
                    }                   
             } 
             if(successCount == assignUserToTerritory.size()){
                 isSuccess = true;
             }  
             else{
                isSuccess = false;
             }          
        }
         
       return isSuccess;
    }
    
    
    /*
    @MethodName    DeleteShareRecords                                  
    @Parameters    deletedIdLst: List of Account/UserTerritory2Association records.
    @Return Type   boolean
    @Description   Method to delete the TerritoryShare records of User and Account
    */
    public boolean DeleteShareRecords(list<Id> deletedIdLst){
        integer successCount = 0;       
        boolean isSuccess;
        Database.DeleteResult[] drList;
        if(deletedIdLst != null && deletedIdLst.size()>0){
            drList = Database.delete(deletedIdLst,true);
                  
            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully deleted Terrtory2Associaton with ID: ' + dr.getId());
                    successCount++;
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : dr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Terrtory2Associaton fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
       
       if(successCount == deletedIdLst.size()){
           isSuccess = true;
       }
       return isSuccess;
    }     
    
     
    /*
    @MethodName    getUserTeritryAssctnList                                  
    @Parameters    IdLst: Set of Territory/User ids.
                   isUserLst: boolean to distinguish between Territory and User Id list
    @Return Type   List of UserTerritory2Association records.
    @Description   Method to query list of UserTerritory2Association records based on the Id set provided.
    */
    public list<UserTerritory2Association> getUserTeritryAssctnList(set<Id> IdLst,boolean isUserIdLst){
        list<UserTerritory2Association> usrTerrLst = new list<UserTerritory2Association>();
        string query = 'SELECT Id, UserId, Territory2Id,Territory2.No_Of_Parents_1_To_6__c, Territory2.Name,Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.Name, Territory2.ParentTerritory2.ParentTerritory2Id,Territory2.ParentTerritory2.ParentTerritory2.Name, User.isActive FROM UserTerritory2Association ';
        if(IdLst != null && IdLst.size()>0){
            if(isUserIdLst == false){
                query +=  ' WHERE Territory2Id IN : IdLst';
            }
            else if(isUserIdLst == true){
                query +=  ' WHERE UserId IN : IdLst';
            }
        }
        
        system.debug('query@@'+query);
        if(string.IsNotEmpty(query)){
            usrTerrLst = database.query(query);
        }
        return usrTerrLst;
    }  
    
    /*
    @MethodName    getAcctnTeritryAssctnList                                  
    @Parameters    IdLst: Set of Account ids
    @Return Type   List of ObjectTerritory2Association records.
    @Description   Method to query list of ObjectTerritory2Association records based on the Id set provided.
    */
    public map<Id,set<id>> getAcctnTeritryAssctnList(set<Id> IdLst){
        list<ObjectTerritory2Association> acctTerrLst = new list<ObjectTerritory2Association>();
        map<Id,set<id>> acctTerritoryMap = new map<Id,set<id>>();
        acctTerrLst = [select Id, ObjectId, Territory2Id 
                                 FROM ObjectTerritory2Association 
                                 WHERE ObjectId IN : IdLst];
            //Create a map of Account Id and its list of assigned territories
         if(acctTerrLst != null && acctTerrLst.size()>0){
             for(ObjectTerritory2Association obj : acctTerrLst){
                 if(acctTerritoryMap.containsKey(obj.ObjectId)){
                     acctTerritoryMap.get(obj.ObjectId).add(obj.Territory2Id);
                 }
                 else
                 {
                     acctTerritoryMap.put(obj.ObjectId,new set<Id>{obj.Territory2Id});
                 }
             }         
        }                               
        
        return acctTerritoryMap;
    }   
    
    /*
    @MethodName    getObject2AssctnList                                  
    @Parameters    IdLst: Set of Account ids
    @Return Type   List of ObjectTerritory2Association records.
    @Description   Method to query list of ObjectTerritory2Association records based on the Id set provided.
    */
    public list<ObjectTerritory2Association> getObject2AssctnList(set<Id> IdLst){
        list<ObjectTerritory2Association> acctTerrLst = new list<ObjectTerritory2Association>();
        
        acctTerrLst = [select Id, ObjectId, Territory2Id, Territory2.Name
                                 FROM ObjectTerritory2Association 
                                 WHERE ObjectId IN : IdLst];
                                    
        
        return acctTerrLst;
    }   
    
    /*
    @MethodName    getTerritoryDetailList                                  
    @Parameters    terrotoryNamelst: List of Territory names.
    @Return Type   map of Territory2Id and its detail records.
    @Description   Method to query list of Territory2 records based on the name list provided.
    */
    public map<Id,Territory2> getTerritoryDetailList(set<string> terrotoryNamelst){
        map<Id,Territory2> territory2Map;
        system.debug('@@terrotoryNamelst--'+terrotoryNamelst); 
        if(terrotoryNamelst != null && terrotoryNamelst.size() > 0){
            territory2Map = new map<Id,Territory2>([SELECT Id, Name, Territory2TypeId, Territory2ModelId, ParentTerritory2Id, Description, AccountAccessLevel, 
                                                    No_Of_Parents_1_To_6__c,No_Of_Parents_7_To_10__c,OpportunityAccessLevel, CaseAccessLevel, ContactAccessLevel, DeveloperName 
                                                    FROM Territory2
                                                    WHERE Name IN : terrotoryNamelst]);
        } 
        system.debug('@@territory2Map--'+territory2Map);                                               
        return territory2Map;
    }       
    
    /*
    @MethodName    removeMyObjectvesShares                                  
    @Parameters    myObjIdLst: List of My_Objectives__c Id records.
    @Return Type   boolean
    @Description   Method to remove all My_Objectives__Share records for the list of My_Objectives__c Id list specified
    */
    public boolean removeMyObjectvesShares(list<Id> myObjIdLst){
        list<My_Objectives__share> objectShareLst;
        integer successCount = 0;       
        boolean isSuccess;
        Database.DeleteResult[] drList;
        // Get a list of My_Objectives__Share records
        if(myObjIdLst != null && myObjIdLst.size()>0){
            objectShareLst = new list<My_Objectives__share>([SELECT Id,AccessLevel,ParentId,RowCause,UserOrGroupId 
                                                                FROM My_Objectives__Share 
                                                                WHERE ParentId IN : myObjIdLst and RowCause =: Schema.My_Objectives__Share.rowCause.Territory_Share__c]);
        }    
        
        if(objectShareLst != null && objectShareLst.size()>0){
            drList = Database.delete(objectShareLst,true);
                  
            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully deleted My_Objectives__share with ID: ' + dr.getId());
                    successCount++;
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : dr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('My_Objectives__share fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            if(successCount == objectShareLst.size()){
                isSuccess = true;
            }
            else{
                isSuccess = false;
            }
        }
        else{
            isSuccess = true;
        }
       
       
       return isSuccess;
    }
    
    /*
    @MethodName    assignMyObjectvesToAllUsers                                  
    @Parameters    myObjLst: List of My_Objectives__c records.
    @Return Type   boolean.
    @Description   Method to assign all My_Objectives__c records to all the Users of its Territory and its Parent Territories.
    */
    public boolean assignMyObjectvesToAllUsers(list<My_Objectives__c> myObjLst){
        boolean isSuccess = false;
        set<string> terrNameLst = new set<string>();
        set<Id> myObjIdLst = new set<Id>();
        map<Id,set<Id>> terrMyObjMap =  new map<Id,set<Id>>();
        map<Id,set<Id>> pTerrIdMap = new map<Id,set<Id>>();
        map<Id,set<Id>> myObjTerrSetMap = new map<Id,set<Id>>();
        list<Territory2> objTerrLst = new list<Territory2>();       
        map<string,Id> territoryNameIdMap = new map<string,Id>();
        map<Id,Territory2> territoryDetailMap = new map<Id,Territory2>();
        map<Id,User> userMap = new map<Id,User>();
        
        for(My_Objectives__c mo : myObjLst){
            terrNameLst.add(mo.Territory__c);               //List of Territory2Ids to query on Territory2 object
            myObjIdLst.add(mo.Id);                      //List of My_Objectives__c Ids to query on My_Objectives__Share 
        }
        
        
        
        // Get a list of Territory reocrds
        if(terrNameLst != null && terrNameLst.size()>0){
            
            
            territoryDetailMap = getTerritoryDetailList(terrNameLst);
            if(territoryDetailMap != null){
             for(Id tId : territoryDetailMap.keyset()){
                 territoryNameIdMap.put(territoryDetailMap.get(tId).name,tId);
             }
            }
            
            if(territoryDetailMap != null &&  territoryDetailMap.keyset().size() > 0){
                objTerrLst = territoryDetailMap.values();
                // create a map of My_Objectives__c.Territory__c and set of all its parents  territories              
                for(Territory2 tr :objTerrLst){
                    if(string.IsNotEmpty(tr.No_Of_Parents_1_To_6__c)){
                        list<Id> tempLst = new list<Id>();
                        set<Id> tempSet = new set<Id>();
                        tempLst = tr.No_Of_Parents_1_To_6__c.split(',');
                        tempSet.addAll(tempLst);
                        if(pTerrIdMap.containsKey(tr.Id)){
                            pTerrIdMap.get(tr.Id).addAll(tempSet);
                        }
                        else
                        {
                            pTerrIdMap.put(tr.Id,new set<id>{});
                            pTerrIdMap.get(tr.Id).addAll(tempSet);
                        }
                    }
                    if(string.IsNotEmpty(tr.No_Of_Parents_7_To_10__c)){
                        list<Id> tempLst = new list<Id>();
                        set<Id> tempSet = new set<Id>();
                        tempLst = tr.No_Of_Parents_7_To_10__c.split(',');
                        tempSet.addAll(tempLst);
                        if(pTerrIdMap.containsKey(tr.Id)){
                            pTerrIdMap.get(tr.Id).addAll(tempSet);
                        }
                        else
                        {
                            pTerrIdMap.put(tr.Id,new set<id>{});
                            pTerrIdMap.get(tr.Id).addAll(tempSet);
                        }
                    }
                }
            }
        }
        
        //Create a map of My_Objectives__c.Id and set of all territories
        for(My_Objectives__c mo : myObjLst){
            if(territoryNameIdMap.get(mo.Territory__c) != null){
                myObjTerrSetMap.put(mo.Id,new set<Id>{territoryNameIdMap.get(mo.Territory__c)});                // Put My_Objectives__c record's own territory
                if(pTerrIdMap.keyset().contains(territoryNameIdMap.get(mo.Territory__c)) && pTerrIdMap.get(territoryNameIdMap.get(mo.Territory__c)).size() > 0){                
                    myObjTerrSetMap.get(mo.Id).addAll(pTerrIdMap.get(territoryNameIdMap.get(mo.Territory__c)));             //Put all territories
                }
            }   
        }
        
        
        // Get a list of all territoies of each My_Objectives__c record to query all the users assigned to these terrioties
        set<Id> allTerritories = new set<id>();
        for(My_Objectives__c mo : myObjLst){
            if(myObjTerrSetMap.keyset().contains(mo.Id) && myObjTerrSetMap.get(mo.Id).size() > 0){
                allTerritories.addAll(myObjTerrSetMap.get(mo.Id));
            }
        }
        
        
        // Get a list of users 
        list<UserTerritory2Association> terrUserLst = new list<UserTerritory2Association>();
        if(allTerritories != null && allTerritories.size()>0){
            terrUserLst = getUserTeritryAssctnList(allTerritories,false);                          
        }
        
        
        // Create a map of Territory and set of its Users
        map<Id,set<Id>> terrUserMap = new map<Id,set<Id>>();
        if(terrUserLst != null && terrUserLst.size()>0){
            for(UserTerritory2Association utr : terrUserLst){
                if(terrUserMap.containsKey(utr.Territory2Id)){
                    terrUserMap.get(utr.Territory2Id).add(utr.UserId);
                }
                else{
                    terrUserMap.put(utr.Territory2Id,new set<Id>{utr.UserId});
                }
            }
        }
        map<Id,set<Id>> objUserMap = new map<Id,set<Id>>();
           
        
        // Create a map of My_Objectives__c.Id and set of all its Users to whom the My_Objectives__c record to be shared    
        for(Id objId : myObjTerrSetMap.keyset()){
            for(Id terId : terrUserMap.keyset()){
                if(myObjTerrSetMap.get(objId).contains(terId)){
                    if(objUserMap.containsKey(objId)){
                        set<Id> tempSet = new set<Id>();
                        tempSet = terrUserMap.get(terId);
                        objUserMap.get(objId).addAll(tempSet);
                    }
                    else{
                        objUserMap.put(objId,new set<id>{});
                        set<Id> tempSet = new set<Id>();
                        tempSet = terrUserMap.get(terId);
                        objUserMap.get(objId).addAll(tempSet);
                    }
                }
            }           
        }
        
        
    
        //Insert new set of My_Objective__Share records to each user
        list<My_Objectives__Share> objShareInsrtLst = new list<My_Objectives__Share>();
        if(objUserMap != null && objUserMap.size() > 0){
            set<Id> userIdSet = new set<Id>();
            for(Id objId : objUserMap.keyset()){                
                userIdSet.addAll(objUserMap.get(objId));
            }
            if(userIdSet.size() > 0){
                userMap = getUserInfoMap(userIdSet);
            }
            for(Id objId : objUserMap.keyset()){
                    if(objUserMap.get(objId).size() >  0){
                        for(Id usrId : objUserMap.get(objId)){
                            My_Objectives__Share myObjShare = new My_Objectives__Share();
                            myObjShare.AccessLevel = 'Edit';                            // 'All' permission is forbidden when you share a record through Apex
                            myObjShare.ParentId = objId;
                            myObjShare.RowCause = Schema.My_Objectives__Share.rowCause.Territory_Share__c;
                            myObjShare.UserOrGroupId = usrId;
                            if(userMap != null && userMap.keyset().size()>0 && userMap.keyset().contains(usrId) && userMap.get(usrId).isActive == true){
                            objShareInsrtLst.add(myObjShare);
                            }
                        }
                 }              
            }
        }
             
     
        integer successCount = 0;  
        if(objShareInsrtLst != NULL && objShareInsrtLst.size()>0){
                Database.SaveResult[] objInsertResult = Database.insert(objShareInsrtLst,TRUE);
                      
                // Error handling code Iterate through each returned result
                for (Database.SaveResult er : objInsertResult) {
                    if (er.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Share Record. Record ID: ' + er.getId());
                        successCount++;
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : er.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Record fields that affected this error: ' + err.getFields());
                        }
                    }
                }                   
         } 
         
         if(successCount == objShareInsrtLst.size()){
               isSuccess = true;
         }
         
        return isSuccess;
    }   
    
    /*
    @MethodName    getUserInfoMap                                  
    @Parameters    IdLst: Set of User ids.
    @Return Type   User Map records.
    @Description   Method to query list of User records based on the Id set provided.
    */
    public Map<Id,User> getUserInfoMap(set<Id> IdLst){
        Map<Id,User> userMap;
        if(IdLst.size() > 0){
            userMap  = new map<Id,user>([Select Id,name,isActive,Profile.name FROM User WHERE Id IN : IdLst]);                                                   
        
        }
        return userMap;
        
    }
    
    /*
    @MethodName    getAcctUserMap                                  
    @Parameters    AcctTerritoryMap: map of account Id and its corresponding territory Id.
    @Return Type   map of Account Id and its territory manager Id.
    @Description   Q3 2016- US2489 - Method to extract territory manager for account and territory provided.
    */
    public Map<Id,Id> getAcctUserMap(map<Id,List<Id>> AcctTerritoryIdMap){
        
        map<Id,Id> territoryUserMap = new map<Id,Id>(); //map to store Territory Id and corresponding Territory Manager(User) Id
        map<Id,Id> acctUserMap = new map<Id,Id>(); //map to store Account Id and corresponding Territory Manager(User) Id
        system.debug('@acctTerritoryIdMap-->'+acctTerritoryIdMap.values());
        List<Id> allterrValues = new List<Id>();
        for (List<Id> terrIds : acctTerritoryIdMap.values()){
            allterrValues.addAll(terrIds);
        }
        if(acctTerritoryIdMap != null && acctTerritoryIdMap.size() > 0){
            for(UserTerritory2Association usr : [
                SELECT Id,UserId,Territory2Id
                FROM UserTerritory2Association
                WHERE Territory2Id IN: allterrValues AND RoleInTerritory2 != null AND RoleInTerritory2 = 'Territory Manager' and User.Profile.Name='Watchman Field User'

            ]){
                territoryUserMap.put(usr.Territory2Id,usr.UserId);
            }
            system.debug('@territoryUserMap-->'+territoryUserMap);
             //Create a map of  Account Id and corresponding Territory Manager(User) Id
            for(Id aId : acctTerritoryIdMap.keyset()){

                for (Id terrId : acctTerritoryIdMap.get(aId)){
                    if(territoryUserMap.get(terrId) != null){
                        acctUserMap.put(aId,territoryUserMap.get(terrId));
                    }
                }
            }
        }
        return acctUserMap;
    }
    
}
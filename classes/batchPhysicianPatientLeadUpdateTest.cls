@isTest(SeeAllData=false)
@testVisible
private class batchPhysicianPatientLeadUpdateTest {

  
  static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME = '/-.-NMD SCS Trial-Implant';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static final String OPPORTUNITY_TYPE1 = 'Implant Only';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LEAD_SOURCE = 'Care Card';
    static final NMD_TestDataManager td = new NMD_TestDataManager();
  static public  List<Patient__c> ptList = new List<Patient__c>();
  static public  String SelectedItem4;
  static public  String SelectedItem5;
 static public  Id currentContactId;
 static public List<Physician_Territory_Realignment__c> ptrList = new  List<Physician_Territory_Realignment__c>();
 
  static public List<Contact> cList = new List<Contact>();
 
  static public set<id> oldPatientTerri = new   set<id>();  
    static public List<ID> oldAccIds = new List<ID>();
    static public set<id> conId = new set<id>();

  static public set<id> oldPatientTerriMulti = new set<id>();
   static public set<id> oldContactTerriMulti = new set<id>();
  static public Map<id,String> ContactwiseOldTerritory = new Map<id,String>();
  static public Map<id,String> ContactwiseOldAccount = new Map<id,String>();
  



    public static   User NMDRBMUser   = new User();

    public static Date myDate ;
    public static Seller_Hierarchy__c territory1= new Seller_Hierarchy__c();
    public static Seller_Hierarchy__c territory2= new Seller_Hierarchy__c();
    public static Account NewAccount= new Account();
    public static Account OldAccount= new Account();
   

static void setupCommonMockData(){
      
      
      
        List<RowCauseObjects__c> settingList  = new List<RowCauseObjects__c>();
RowCauseObjects__c setting1 = new RowCauseObjects__c();
setting1.Name = 'Opportunity';
setting1.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,Team';
settingList.add(setting1);


RowCauseObjects__c setting2 = new RowCauseObjects__c();
setting2.Name = 'Patient';
setting2.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,NMDTerritoryChange__c';
settingList.add(setting2);

RowCauseObjects__c setting3 = new RowCauseObjects__c();
setting3.Name = 'Lead';
setting3.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
settingList.add(setting3);


RowCauseObjects__c setting4 = new RowCauseObjects__c();
setting4.Name = 'Contact';
setting4.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
settingList.add(setting4);


insert settingList;
      
      /*
        Account acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

        Account insurance = td.newAccount();
        insurance.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
        insurance.Status__c = 'Active';
        
        Account acctTrialing = td.newAccount(ACCT_TRIALING);
        acctTrialing.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing.AccountNumber = '1234';
        acctTrialing.Type = 'Sold to';

        Account acctProcedure = td.newAccount(ACCT_PROCEDURE);
        acctProcedure.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctProcedure.Type = 'Sold to';
        
        insert new List<Account> { acctMaster, acctTrialing, acctProcedure, insurance };

        Contact trialing = td.newContact(acctTrialing.Id);
        trialing.AccountId = acctTrialing.id;
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        Contact procedure = td.newContact(acctProcedure.Id);
        procedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { trialing, procedure };
        conId.add(trialing.id);
        conId.add(procedure.id);
   
        
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        insert territory;
        
        
        

        Patient__c patient = td.newPatient('Fname', 'Lname');
        patient.Territory_ID__c=territory.Id;
        patient.Primary_Insurance__c = insurance.id;
        patient.Secondary_Insurance__c = insurance.id;
        patient.Physician_of_Record__c = trialing.id;
        patient.Referring_Physician__c = trialing.id;
        patient.RecordTypeId = PatientManager.RECTYPE_PROSPECT;
        insert patient;




    
        for(integer i =1;i<4;i++){
        
            Patient__c p = td.newPatient('Fname'+i, 'Lname'+i);
        p.Territory_ID__c=territory.Id;
        p.Primary_Insurance__c = insurance.id;
        p.Secondary_Insurance__c = insurance.id;
        p.Physician_of_Record__c = trialing.id;
        p.Referring_Physician__c = trialing.id;
        p.RecordTypeId = PatientManager.RECTYPE_PROSPECT;
        oldPatientTerri.add(territory.Id);
        oldPatientTerriMulti.add(territory.Id);
       // insert patient;
        ptList.add(p);
        
        }
        
    


insert ptList;
        
       
        User usr = new User(
        Email = 'suser@boston.com', 
        LastName = 'LNAMETEST',  
        ProfileId = Label.NMD_RBM_Profile_Id, 
        UserName ='abc@boston.com',
        Alias = 'standt', 
        EmailEncodingKey = 'UTF-8',  
        LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US',  
        TimeZoneSidKey = 'America/Los_Angeles'
        //Cost_Center_Code__c = randomString5()
        );
        insert usr;  
        
        Assignee__c assign = td.newAssignee(usr.id,territory.id); 
        insert assign;
        
        //assigning same master account to both opportunities.
        Opportunity trialOpty = td.newOpportunity(acctMaster.Id, OPPTY_NAME_TRIAL);
        trialOpty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        trialOpty.Trialing_Account__c = acctTrialing.Id;
        trialOpty.Trialing_Physician__c = trialing.Id;
        trialOpty.Procedure_Account__c = acctProcedure.Id;
        trialOpty.Patient__c = ptList[0].Id;
        trialOpty.Procedure_Physician__c = procedure.Id;
        trialOpty.Scheduled_Trial_Date_Time__c = system.today();
        trialOpty.Territory_ID__c = territory.Id;
        trialOpty.closeDate = System.today()+30;
        trialOpty.LeadSource = LEAD_SOURCE;
        trialOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
        trialOpty.CloseDate = system.today();
        //insert trialOpty;
        
        Opportunity implantOpty = td.newOpportunity(acctMaster.Id, OPPTY_NAME_IMPLANT);
        implantOpty.Territory_ID__c = territory.Id;
        implantOpty.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        implantOpty.Scheduled_Trial_Date_Time__c = system.today();
        implantOpty.closeDate = System.today()+30;
        implantOpty.LeadSource = LEAD_SOURCE;
        implantOpty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        implantOpty.Patient__c = ptList[0].Id;
        implantOpty.Procedure_Account__c = acctProcedure.Id;
        implantOpty.Procedure_Physician__c = procedure.Id;
        implantOpty.CloseDate = system.today();
        //insert implantOpty;
        
        Opportunity implantOpty1 = td.newOpportunity(acctMaster.Id, OPPTY_NAME_IMPLANT);
        implantOpty1.Territory_ID__c = territory.Id;
        implantOpty1.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        implantOpty1.Scheduled_Trial_Date_Time__c = null;
        implantOpty1.Scheduled_Procedure_Date_Time__c = null;
        implantOpty1.closeDate = null;
        implantOpty1.LeadSource = LEAD_SOURCE;
        implantOpty1.Patient__c = ptList[0].Id;
        implantOpty1.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        implantOpty1.Procedure_Account__c = null;
        implantOpty1.Procedure_Physician__c = null;
        implantOpty1.CloseDate = system.today();
        
        insert new List<Opportunity> { trialOpty, implantOpty, implantOpty1};
  
      
       */
       
      DateTime dT = System.now();
        myDate = date.newinstance(dT.year(), dT.month(), dT.day());
       // NMD_TestDataManager td = new NMD_TestDataManager();
        territory1 = td.newSellerHierarchy('T1');
        territory1.Current_TM1_Assignee__c = NMDRBMUser.id;
        insert territory1;
        
        territory2 = td.newSellerHierarchy('T2');
        territory2.Current_TM1_Assignee__c = NMDRBMUser.id;
        insert territory2;
        
        
        NewAccount =td.newAccount('New Account');
        insert NewAccount;
        OldAccount = td.newAccount('Old Account');
        insert OldAccount;
        //String oldAccId=OldAccount.Id;
        for(Integer i=0;i<4;i++){
            Contact temp= td.newContact(OldAccount.Id);
            temp.Territory_ID__c =territory1.Id;
            cList.add(temp);
        }
        if(cList.size()>0){
            insert cList;
            
        }
        
        For(Integer i=0;i<4;i++){
            Physician_Territory_Realignment__c ptrTemp = new Physician_Territory_Realignment__c();
            ptrTemp.Contact__c=cList[1].Id;
            ptrTemp.New_Account_Id__c= NewAccount.Id;
            ptrTemp.Current_Territory__c=territory1.Id;
            ptrTemp.New_Territory__c =territory2.Id;
            if(1== 2){
            ptrTemp.Realignment_Status__c = 'Moving To SAP';
            }
            else{
            ptrTemp.Realignment_Status__c = 'readyforpatient';
            }
            ptrTemp.Effective_Realignment_Date__c=myDate; 
            PTRList.add(ptrTemp);
            
        }
        insert PTRList;
        
         
        system.debug('__PTRList___'+PTRList);
    }

        static TestMethod void TestbatchPhysicianPatientLeadUpdate(){ 
        setupCommonMockData();
        
        /*For(Integer i=0;i<4;i++){
            Physician_Territory_Realignment__c ptrTemp = new Physician_Territory_Realignment__c();
            ptrTemp.Contact__c=cList[1].Id;
            ptrTemp.New_Account_Id__c= NewAccount.Id;
            ptrTemp.Current_Territory__c=territory1.Id;
            ptrTemp.New_Territory__c =territory2.Id;
            ptrTemp.Realignment_Status__c = 'Moving To SAP';
            ptrTemp.Effective_Realignment_Date__c=myDate; 
            PTRList.add(ptrTemp);
            
        }
        insert PTRList;*/
        List<Physician_Territory_Realignment__c> phys=[SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c ='readyforpatient' ];
        
        
                List<Physician_Territory_Realignment__c> phys1=[SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c ='Moving To SAP' ];
        Test.startTest();
        ID batchprocessid = Database.executeBatch(new batchPhysicianPatientLeadUpdate());
        ID batchprocessid2 = Database.executeBatch(new batchPhysicianSAPUpdate());
       // System.schedule('NM PhysicianToSAP Batch Job4', '0 45 * * * ?', new batchPhysicianSAPUpdate());
 


        Test.stopTest();
    
    }
    

//  String query = 'SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c = \'readyforpatient\'' ;
         
}
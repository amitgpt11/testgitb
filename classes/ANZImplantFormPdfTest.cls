/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class ANZImplantFormPdfTest {
   final static String LOT1 = 'LOT1';
  final static String MODEL123 = 'MODEL123';
  final static String SERIAL1 = '11111';
  final static String SERIAL2 = 'SERIAL2';
  final static String CCRANAME = 'CCRANAME';
  final static String SURGERYNAME = 'SURGERYNAME';
  final static String UPNMATERIALNUM = '1234567';
  final static String INVENLOCATION = 'LOC123';
    final static String ACCTNAME ='TestAccount';
  final static Date EXPIRATION = System.today().addDays(10);
  public static  Account acct = new account();
  public static     Product2 prod  = new Product2();
  public static     Inventory_Item__c invItem  = new Inventory_Item__c();
  public static  Inventory_Transaction__c invTrans   = new Inventory_Transaction__c();
     public static     Inventory_Location__c invLoc = new Inventory_Location__c ();
  
  //@TestSetup
  static Inventory_Location__c setupInventoryLocation(){
   

    // inventory tree
    Inventory_Data__c invData = new Inventory_Data__c(
      Account__c = acct.Id,
      Customer_Class__c = 'YY'
    );
    insert invData;

    invLoc = new Inventory_Location__c(
      Inventory_Data_ID__c = invData.Id,
      Name = INVENLOCATION
    );
    insert invLoc;
        return invLoc;
  }

  static Order setupOrder() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();

        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
            user0, user1
        };

        //System.runAs(new User(Id = UserInfo.getUserId())) {

            //Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
        //insert pb;

            Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
            insert hierarchy;

            //create two assignees
            Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
            assignee0.Rep_Designation__c = 'TM1';
            Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
            assignee1.Rep_Designation__c = 'TM2';
            insert new List < Assignee__c > {
                assignee0, assignee1
            };

            hierarchy.Current_TM1_Assignee__c = user0.Id;
            hierarchy.Current_TM2_Assignee__c = user1.Id;
            update hierarchy;

       
            
     

        
        
        acct= td.newAccount(ACCTNAME);  

        system.debug('___acct____Before '+acct);
insert acct;

        
        
 //   insert acct;
    system.debug('___acct____'+acct);
            
      Account acc= [Select Id, Name From Account where Name =:ACCTNAME];
      
      

    // product
     prod = td.newProduct('Trial');
    prod.Model_Number__c = MODEL123;
    prod.EAN_UPN__c = MODEL123;
    prod.UPN_Material_Number__c = UPNMATERIALNUM;
    insert prod;

    invLoc =setupInventoryLocation();

     invItem = new Inventory_Item__c(
      Inventory_Location__c = invLoc.Id,
      Model_Number__c = prod.Id,
      Serial_Number__c = SERIAL1,
      Lot_Number__c = LOT1,
      Expiration_Date__c = EXPIRATION,
      SAP_Quantity__c = 1
    );
    insert invItem;

     invTrans = new Inventory_Transaction__c(
      Inventory_Item__c = invItem.Id,
      Disposition__c = 'In Transit',
      Quantity__c = 1      
      );
    insert invTrans;

        
    Cycle_Count_Response_Analysis__c ccra = td.newCycleCountResponseAnalysis();
        ccra.OwnerId=user1.Id;
    insert ccra;
        system.debug('ccra'+ccra);
    
          system.debug('___acct____'+acc);
          system.debug('___prod____'+prod);
          system.debug('invLoc______'+invLoc);
            //create oppty
            Opportunity oppty = td.newOpportunity(acc.Id);
            oppty.Territory_Id__c = hierarchy.Id;
            oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
            
            insert oppty;
   
        // create two products, pricebook, and entries
      Id pricebookId = Test.getStandardPricebookId();

      Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
        insert pb;
        

            PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
          PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
          insert new List<PricebookEntry> { standardPrice0 };

        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        insert new List<PricebookEntry> { pbe0};
            
      Order ord = new Order(
                AccountId = acc.id,
                Shipping_Location__c=invLoc.id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today(),
                Pricebook2Id = pb.Id,
                Status = 'Draft',
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                Stage__c = 'Pending'
      );
        insert ord;
        OrderItem ordItem = new OrderItem(
          OrderId=ord.id,
            Material_Number_UPN__c=UPNMATERIALNUM,
            Serial_Number__c = SERIAL1,
            PricebookEntryId = pbe0.Id,
            Quantity=1,
            UnitPrice = 0
            //Product = p2.id
        );
        system.debug('Order details===>'+ord);
      insert ordItem;
      return ord;
        //}
    
    }
  
  
    static TestMethod void TestANZImplantFormExtensionPdf(){ 
//Account acc = [Select id,name from Account limit 1];
Test.StartTest();
  Order olist= setupOrder();    
    PageReference pageRef = Page.ANZImplantFormPDF;
 pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
 Test.setCurrentPage(pageRef);

 ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(olist);   
 ANZImplantFormExtensionPdf classInstanat = new ANZImplantFormExtensionPdf(sc);
 ANZImplantFormExtensionPdfTemp classInstanat1 = new ANZImplantFormExtensionPdfTemp (sc);
    Test.StopTest();


}  

    static TestMethod void TestANZImplantFormExtensionPdf2(){ 
//Account acc = [Select id,name from Account limit 1];
Test.StartTest();
  Order olist= setupOrder();    
    PageReference pageRef = Page.ANZImplantFormPDFTemp;
 pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
 Test.setCurrentPage(pageRef);

 ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(olist);   

 ANZImplantFormExtensionPdfTemp classInstanat1 = new ANZImplantFormExtensionPdfTemp (sc);
 
 //classInstanat1 .attach() ;
    Test.StopTest();


}  



}
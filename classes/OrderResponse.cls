public class OrderResponse{
	public cls_OrderResponse OrderResponse;
	public class cls_OrderResponse {
		public String orderNumber;	//0500064998
		public cls_item[] item;
	}
	public class cls_item {
		public String acknowledgement;	//S
		public String responseID;	//V4
		public String responseNumber;	//233
		public String responseDescription;	//    SALES_HEADER_IN has been processed successfully    VBAKKOM    09/30/2016    17:52:02
	}
	public static OrderResponse parse(String json){
		return (OrderResponse) System.JSON.deserialize(json, OrderResponse.class);
	}
}
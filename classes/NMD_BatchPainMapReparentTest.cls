@isTest(SeeAllData=false)
public class NMD_BatchPainMapReparentTest {

    static testmethod void test_NMD_BatchPainMapReparentTest(){
      
        NMD_TestDataManager testData = new NMD_TestDataManager();
        
        //create user
        User user = testData.newUser('System Administrator');
        insert user;
        
        //create Patient 
       // Patient__c patient = testData.newPatient();
        Patient__c patient1 = testData.newPatient(); 
        Patient__c patient2 = testData.newPatient();
        insert new List<Patient__c> {patient1, patient2};
        
        //deleting patient's CDSS.
        List<Clinical_Data_Summary__c> cdssPat = [select id, isActive__c from Clinical_Data_Summary__c where patient__c in (:patient1.Id, :patient2.id)];
        delete cdssPat;
        
        //create account        
        Account acc = testData.newAccount();
        insert acc;
        
         //create opportunity
        Opportunity oppo = testData.newOpportunity(acc.Id);
        oppo.Patient__c = patient1.Id;
        oppo.recordtypeId=OpportunityManager.RECTYPE_TRIAL;
        oppo.CreatedDate= dateTime.newInstance(2016, 05, 03);
        
        Opportunity oppo1 = testData.newOpportunity(acc.Id);
        oppo1.Patient__c = patient1.Id;
        oppo1.recordtypeId=OpportunityManager.RECTYPE_IMPLANT;
        
        Opportunity oppo2 = testData.newOpportunity(acc.Id);
        oppo2.Patient__c = patient1.Id;
        oppo2.recordtypeId=OpportunityManager.RECTYPE_REVISION;
        
        Opportunity oppo3 = testData.newOpportunity(acc.Id);
        oppo3.Patient__c = patient1.Id;
        oppo3.recordtypeId=OpportunityManager.RECTYPE_TRIAL;
        oppo3.CreatedDate= dateTime.newInstance(2016, 05, 05);
        
        
        Opportunity oppo4 = testData.newOpportunity(acc.Id);
        oppo4.Patient__c = patient1.Id;
        oppo4.recordtypeId=OpportunityManager.RECTYPE_REPROGRAMMING;
        
        List<Opportunity> opptys= new List<Opportunity> {oppo,oppo1,oppo2,oppo3,oppo4};
        insert opptys;
        
        List<Clinical_Data_Summary__c> cdss = new List<Clinical_Data_Summary__c>();
        for(Opportunity o: opptys){
            Clinical_Data_Summary__c cds = new Clinical_Data_Summary__c();
            cds.Opportunity__c = o.id;
            cds.isActive__c = true;
           cds.Patient__c = patient1.Id;
            cds.RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS;
            cdss.add(cds);
            Clinical_Data_Summary__c cds1 = new Clinical_Data_Summary__c();
            cds1.Opportunity__c = o.id;
          cds1.Patient__c = patient1.Id;
            cds1.RecordTypeId = ClinicalDataSummaryManager.RECTYPE_PAINMAP;
            cdss.add(cds1);
        }
        
       system.debug('#########  :'+cdss.size());
        insert cdss;
       // String testquery = 'select id,name,RecordTypeId,PainMapReparented__c from patient__c where recordtype.name in (\'Patient Prospect\',\'Patient Customer\') and  PainMapReparented__c= false ' ;

        test.startTest();

         NMD_BatchPainMapReparent bpmr1 = new NMD_BatchPainMapReparent(0);
         
          List<Patient__c> ptSel = [select id, PainMapReparented__c from Patient__c where id in (:patient1.Id, :patient2.id)];
          ptSel[0].PainMapReparented__c = false;
          update ptSel;
        Database.executeBatch(bpmr1);
                 
        test.stopTest();
        
        List<Clinical_Data_Summary__c> cdssFinal = [select id, isActive__c from Clinical_Data_Summary__c where patient__c = :patient1.Id];
        system.debug('################## :'+cdssFinal);
        system.assertEquals(10,cdssFinal.size());
        List<Clinical_Data_Summary__c> cdssFinalActive = new List<Clinical_Data_Summary__c> ();
        
        for(Clinical_Data_Summary__c c: cdssFinal){
            if(c.isActive__c){
                cdssFinalActive.add(c);
            }
        }
     //    system.assertEquals(cdssFinalActive.size(), 5);
        
        List<PainMapReparentTemp__c> pmrs = [SELECT Id, Patient__c, OldOpportunity__r.id, isActive__c, Clinical_Data_Summary__c FROM PainMapReparentTemp__c];
        //system.assertEquals(10,pmrs.size());
        
        for(PainMapReparentTemp__c p : pmrs){
            if(p.isActive__c){
             //   system.assertEquals(p.OldOpportunity__r.id,oppo3.id);
            }
        }
        
        
    }
}
public class CustomerJsonRequest {

    public CustomerInfo CustomerInfo {get; set;}

    public class CustomerInfo {
        public String partnerAddressDetail {get; set;}
        public String soldToAccountNumber {get; set;}
        public String salesOrganization {get; set;}
    }

public static CustomerJsonRequest parse(String json) {
        return (CustomerJsonRequest) System.JSON.deserialize(json, CustomerJsonRequest.class);
    }
}
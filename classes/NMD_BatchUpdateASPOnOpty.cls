global class NMD_BatchUpdateASPOnOpty implements  Database.Batchable<sObject>, Schedulable{

    List<Opportunity> AllOptys = new List<Opportunity>();
    Map<Id,OpportunityLineItem> optyProductMap = new Map<Id,OpportunityLineItem>{};
    Map<Id,List<Order>> optyOrderMap = new Map<Id,List<Order>>();
    List<Opportunity> OptysWithoutOrder = new List<Opportunity>();
    List<OpportunityLineItem> prodsToUpdate = new List<OpportunityLineItem>();
    Map<id, AccountASPRecord> accountASPRecordsMap = new Map<id, AccountASPRecord>();
    Set<String> orderStages = new Set<String>{'New','Cancelled'};
    
    
    Set<Id> allOptyIds = new Set<Id>();
    Set<Id> accIds = new Set<Id>();
    
    integer queryLimit = 0;
    
    public String query = 'select id,Trial_ASP_Value__c, Implant_ASP_Value__c, ASP_Values_Changed__c from Account where ASP_Values_Changed__c=true'; 
    
    // Constructor
    global NMD_BatchUpdateASPOnOpty ()  
    {} 
    global NMD_BatchUpdateASPOnOpty (Integer queryLim)  
    {
        this.queryLimit=queryLim;
    } 
    /****************************************************************
    *  start(Database.BatchableContext BC)
    *****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('Inside Start method');
        if(Test.isRunningTest()){
        this.query= this.query+' Limit 10';
        }
        if(queryLimit!=0){
            this.query=this.query+' Limit '+queryLimit;
        }
                                
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
    *  execute(Database.BatchableContext BC, List scope)
    **********************************************************************/
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        System.debug('#########Scope####### :'+scope);
        
        for(Account a: scope){
            accIds.add(a.id);
            AccountASPRecord tempAcc= new AccountASPRecord(a.id, a.Trial_ASP_Value__c, a.Implant_ASP_Value__c);
            accountASPRecordsMap.put(a.id, tempAcc);
        }
       
        AllOptys = [select id,Trialing_Account__c, Procedure_Account__c,Procedure_Type__c, probability, (select id, UnitPrice from opportunityLineItems), RecordTypeId, IsAmountChangedManually__c, (Select id, Stage__c from Orders) from Opportunity where ((recordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW AND Trialing_Account__c in :accIds) OR ((recordTypeId IN (:OpportunityManager.RECTYPE_IMPLANT, :OpportunityManager.RECTYPE_REVISION)) AND Procedure_Account__c in :accIds)) and RecordTypeId in :OpportunityManager.RECTYPES_NMD_ONLY];
        System.debug('************** AllOptys Size ************'+AllOptys.size());
        for(Opportunity o:AllOptys){
            boolean isValidOpty = true;
          //  allOptyIds.add(o.id);
            if(o.OpportunityLineItems.size()>0)
                optyProductMap.put(o.id, o.OpportunityLineItems[0]); //an opty can have only 1 product
                
            if(o.orders== null ||o.orders.size()==0){
                OptysWithoutOrder.add(o);
            }else{
                for(Order o1:o.orders){
                    if(!orderStages.contains(o1.stage__c)){
                        isValidOpty= false;
                        break;
                    }
                }
                if(isValidOpty){
                    //List<Order> os = new List<Order>();
                    optyOrderMap.put(o.id, o.orders);
                }
                    
            }
        
        }
        System.debug('************** OptysWithoutOrder Size ************'+OptysWithoutOrder.size());
        System.debug('************** Valid Opty Size ************'+optyOrderMap.KeySet().size());
        
        if(optyOrderMap!=null && optyOrderMap.keySet()!=null && optyOrderMap.keySet().size()>0){
            //optys with valid orders only
            for(Order odr : [SELECT Id, OpportunityId, Opportunity.RecordTypeId, Opportunity.IsAmountChangedManually__c, Opportunity.Trialing_Account__c, Opportunity.Procedure_Account__c,Opportunity.Procedure_Type__c, Opportunity.probability,Stage__c,RecordTypeId FROM Order WHERE OpportunityId IN :optyOrderMap.KeySet()]){
                if((odr.Stage__c== 'New' || odr.Stage__c=='Cancelled') && odr.RecordTypeId == OrderManager.RECTYPE_PATIENT){
                    processOpportunity(odr.Opportunity);
                }
            }
        }
        //optys without orders
        for(Opportunity o: OptysWithoutOrder){
            System.debug('************** ProcessOptyWithoutOrder***********'+o.Id);
			//Processing Only Open Opty i.e. the probability of the opty should be between 0% and 100%. 
			if(o.probability>0 && o.probability<100){
				processOpportunity(o);
			}
        }
        
        if(!prodsToUpdate.isEmpty()){
            //REMOVE DUPLICATES, if any
            System.debug('*************Final List to update Before Duplicate Removal*********'+prodsToUpdate.size());
            Set<OpportunityLineItem> myset = new Set<OpportunityLineItem>();
            List<OpportunityLineItem> result = new List<OpportunityLineItem>();
            myset.addAll(prodsToUpdate);
            result.addAll(myset);
            System.debug('*************Final List to update Before Duplicate Removal*********'+prodsToUpdate.size());
            DML.save(this, result);
        }
        
        //Resetting Account Flags. 
        List<Account> accts = new List<Account>();
        for(Account a: Scope){
            a.ASP_Values_Changed__c= false; 
            accts.add(a);
        }
        DML.save(this, accts);
        
    }   
        private void processOpportunity(Opportunity opr){
            
            if(!opr.IsAmountChangedManually__c){
                    if(opr.RecordTypeId==OpportunityManager.RECTYPE_TRIAL_NEW && opr.Trialing_Account__c!=null ){
                        updateOliAmount(opr);
                    }
                    else if(opr.RecordTypeId==OpportunityManager.RECTYPE_IMPLANT && opr.Procedure_Account__c!=null){
                        updateOliAmount(opr);
                    }
                    else if(opr.RecordTypeId==OpportunityManager.RECTYPE_REVISION && opr.Procedure_Account__c!=null && opr.Procedure_Type__c!=null && (opr.Procedure_Type__c.contains('BSC IPG Swap') || opr.Procedure_Type__c.contains('BSC IPG Addition'))){
                        updateOliAmount(opr);                    
                    }
                }
        }
    
    
    private void updateOliAmount(Opportunity opp){
        OpportunityLineItem oli = optyProductMap.get(opp.id);
        if(oli!=null){
         if(Opp.recordTypeId==OpportunityManager.RECTYPE_TRIAL_NEW)
            oli.UnitPrice= accountASPRecordsMap.get(opp.Trialing_Account__c).trialValue==null?0:accountASPRecordsMap.get(opp.Trialing_Account__c).trialValue;
         else        
            oli.UnitPrice= accountASPRecordsMap.get(opp.Procedure_Account__c).implantValue==null?0:accountASPRecordsMap.get(opp.Procedure_Account__c).implantValue;
            System.debug('***************Updating Amount for Opportunity*********'+opp.Id);
            prodsToUpdate.add(oli);
        }
    }
    
     /****************************************************
    *  finish(Database.BatchableContext BC)
    *****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Batch Execution finished.');
    }
        
    global void execute(SchedulableContext sc) {
      NMD_BatchUpdateASPOnOpty b = new NMD_BatchUpdateASPOnOpty(); 
      Database.executebatch(b, 20);//Batch size 200.
   }
    
    public class AccountASPRecord{
        public Id accountId;
        public decimal trialValue;
        public decimal implantValue;
        
        public AccountASPRecord(Id accId, decimal trialValue, decimal implantValue){
            this.accountId=accId;
            this.trialValue=trialValue;
            this.implantValue=implantValue;
        }
        
    }
}
/**
* Test class for the Convert Lead extension class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_LeadConversionManagerTest {
  
  static final String FNAME = 'FNOM';
  static final String LNAME = 'LNOM';
  static final String FACIL = 'FACIL';
  static final String TRIAL = 'TRIAL';
  static final String IMPLANTTYPE = 'Implant';
  static final String TRIALTYPE = 'Trial';  

  @testSetup
  static void setup() {

    NMD_TestDataManager td = new NMD_TestDataManager();

    Account facilityAcct = td.createConsignmentAccount();
    facilityAcct.Name = FACIL;
    Account trialingAcct = td.newAccount();
    trialingAcct.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
    trialingAcct.Type = 'Ship To';
    trialingAcct.Name = TRIAL;
    insert new List<Account> { facilityAcct, trialingAcct };

    Contact cont = td.newContact(trialingAcct.Id, LNAME);
    cont.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
    insert cont;

    Lead lead = td.newLead(LNAME);
    insert lead;

    Patient__c patient = td.newPatient(FNAME, LNAME);
    insert patient;

  }

  static List<SObject> getTestData() {

    List<SObject> result = new List<SObject>{};

    Set<String> names = new Set<String> { FACIL, TRIAL };
    for (Account acct : [select Name from Account order by Name]) {
      result.add(acct);
    }

    result.add([select Id from Contact limit 1]);
    result.add([select Id from Lead limit 1]);
    result.add([select Id from Patient__c limit 1]);

    return result;

  }

  static testMethod void test_ConvertLead() {

    NMD_TestDataManager td = new NMD_TestDataManager();
    User testuser = td.newUser('System Administrator');
    insert testuser;

    System.runAs(testuser) {
    
      List<SObject> result = getTestData();
      Id trialId = result[0].Id;
      Id facilId = result[1].Id;
      Id physId = result[2].Id;
      Id leadId = result[3].Id;
      Id patientId = result[4].Id;
        
      Seller_Hierarchy__c territory = td.newSellerHierarchy();
      insert territory;
      
      Lead lead = td.newLead();
      lead.Id = leadId;
      lead.Account_Facility__c = facilId;
      lead.Trialing_Physician__c = physId;
      lead.Patient__c = patientId;
      lead.LeadSource  = 'Office'  ;
      lead.Territory_ID__c = territory.id;
      lead.ConvertTo__c = TRIALTYPE;  
      update lead;        
      
      Test.startTest();
      NMD_LeadConversionManager ext = new NMD_LeadConversionManager(leadId);
      ext.convertLead();    
      ext.convertLead();
      Test.stopTest();

    }

  }
     static testMethod void test_ConvertLead1() {

    NMD_TestDataManager td = new NMD_TestDataManager();
    User testuser = td.newUser('System Administrator');
    insert testuser;

    System.runAs(testuser) {
    
      List<SObject> result = getTestData();
      Id trialId = result[0].Id;
      Id facilId = result[1].Id;
      Id physId = result[2].Id;
      Id leadId = result[3].Id;
      Id patientId = result[4].Id;
        
      Seller_Hierarchy__c territory = td.newSellerHierarchy();
      insert territory;
      
      Lead lead = td.newLead();
      lead.Id = leadId;
      lead.Account_Facility__c = facilId;
      lead.Trialing_Physician__c = physId;
      lead.Patient__c = patientId;
      lead.LeadSource  = 'Office'  ;
      lead.Territory_ID__c = territory.id;
      lead.ConvertTo__c = IMPLANTTYPE;  
      update lead;        
      
      Test.startTest();
      NMD_LeadConversionManager ext = new NMD_LeadConversionManager(leadId);
      ext.convertLead();    
      ext.convertLead();
      Test.stopTest();

    }

  }

 
}
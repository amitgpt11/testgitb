/*
 @CreatedDate     13 APR 016                                                                
 @author          Mayuri-Accenture
 @Description     This  is test Class for 'UtilityForUserAccountAssociation'.
   
 */


@isTest(SeeAllData=false)
private class UtilityForUserAccountAssociationTest{
    @testSetup
    static void setupMockData1(){
        BatchAccountTerritoryUpdateTest.setupTerritoryMockData();
        
    }

     static TestMethod void Test_UtilityMethod1(){ 
         BatchAccountTerritoryUpdateTest.TestbatchAccountTerritoryUpdate();     
     }

     static TestMethod void Test_UtilityMethod2(){
         //BatchUserTerritoryUpdateTest.setupMockData();
         BatchUserTerritoryUpdateTest.testbatchUserTerritoryUpdate();
     }
     
     static TestMethod void Test_UtilityMethod3(){
         BatchAssignMyObjectivesToEuropeUsersTest.TestbatchAssignMyObjectivesToEuropeUsers();
     }
     
     static TestMethod void Test_UtilityMethod4(){ 
         BatchAccountTerritoryUpdateTest.TestbatchMyObjTerritoryUpdate();
     
     }
     
     static TestMethod void Test_UtilityMethod5(){ 
         BatchAccountTerritoryUpdateTest.TestbatchAccountTerritoryUpdate();
     
     }
     
     static TestMethod void Test_UtilityMethod6(){ 
          Test.startTest();
               Territory2 trLst = [Select name From Territory2 where name Like : 'AA%' LIMIT 1 ];  
         //list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%' limit 1 ]);
         Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
         insert acct1;   
         
         ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
         accountAssociation.ObjectId = acct1.Id;
         accountAssociation.Territory2Id = trLst.Id;
         accountAssociation.AssociationCause = 'Territory2Manual';
         insert accountAssociation;
         
         list<Shared_Patient_Case__c> pcLst = new list<Shared_Patient_Case__c>();
         for(integer i=0;i<100;i++){
             Shared_Patient_Case__c pc = new Shared_Patient_Case__c();
             pc.Shared_End_Date_Time__c = system.today()+1;
             pc.Shared_Start_Date_Time__c = system.today();
             pc.Shared_Facility__c = acct1.Id;
             pcLst.add(pc);
         }
         insert pcLst;
             ID batchprocessid = Database.executeBatch(new BatchPatientCaseTMUpdate());
             
         Test.stopTest();
     
     }
     
}
/**
* Without Sharing Manager class for the Task object
*
* @Author salesforce Services
* @Date 2016/02/12
*/
global without sharing class TaskManagerNoSharing {

	final static Set<String> profileNames = new Set<String> {
        'NMD IBU-PC', 'System Administrator'
    };
	
	webservice static String makeUserTaskOwner(Id taskId, Id userId) {

        User u = [select Profile.Name from User where Id = :userId];
        Task t = [select Status from Task where Id = :taskId];
        String userProfileName = u.Profile.Name; 

    	if (profileNames.contains(userProfileName)) {
        	if (t.Status != 'Completed') {
        		Database.SaveResult sr = DML.save('TaskManagerNoSharing', new Task(
                    Id = taskId,
                    OwnerId = userId
                ), false);

                if (!sr.isSuccess()) {
                    return sr.getErrors()[0].getMessage();
                }
    	    } 
            else {
    	    	return Label.TaskManagerNoSharing_CompletedError;
            }
    	} 
        else {
    		return Label.TaskManagerNoSharing_InvalidProfileError;
    	}

        return '';

    }

}
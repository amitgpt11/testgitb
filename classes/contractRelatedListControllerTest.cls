/*
 @CreatedDate     25 June 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Apex class 'contractRelatedListController'
 
 */

@isTest
public class contractRelatedListControllerTest{
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Contract.getRecordTypeInfosByName();    
    public final Id RECTYPE_EUROPE_CONTRACT = RECTYPES.get('Europe Contracts').getRecordTypeId();

    static TestMethod void TestcontractRelatedListController(){ 
        Account acc = new Account();
        acc.Name = 'Test Releted List';
        insert acc;
        
        Contract con = new Contract();
        con.Name = 'test';
        con.Shared_divisions__c = 'IC';
        con.Contract_Type__c = 'Service';
        con.accountid = acc.id;
        con.Status = 'Active';
        Insert con;
        
       // con.Status = 'Activated';
       // update con;
              
        ContractConfiguration__c CS = ContractConfiguration__c.getOrgDefaults();
        cs.Visible_Fields__c = 'Name';
        cs.Contract_Divisions__c = 'IC';
        cs.Contract_Type__c = 'Service';
        cs.Contract_Status__c = 'Active';
        upsert cs;
        
        Test.StartTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(acc);
         contractRelatedListController contr = new contractRelatedListController(sc);
         
         cs.Visible_Fields__c = '';
         update cs;
         
         contr = new contractRelatedListController(sc);
         System.assertequals(contr.lstContractRec.get(0).id, con.id); 
        Test.StopTest();
    }
}
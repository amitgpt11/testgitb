global class PhysicianRequestHandler {
    
    public static final String CLIENT_ID = '';
    public static final String CLIENT_SECRET = '';
    public static List<Application_Log__c> appLogs  = new List<Application_Log__c>();
    

    
    @future (callout=true)
    @RestResource(urlMapping='/PhysicianTerritory')
    Webservice static void SendHttpRequest(String reqString)
    {
        String CertificateName= Label.CertificateInfo;
System.debug('Label CertificateName__c: '+CertificateName); 

        HttpRequest req = new HttpRequest();
        Application_Log__c appLog= new Application_Log__c();
        appLog.Record_Object_Type__c = 'Physician Territory';
        appLog.Source_Class__c = 'PhysicianRequestHandler';
        appLog.Source_Function__c = 'SendHttpRequest';
        appLog.Source__c = 'SF';
        appLog.Timestamp__c =  System.now();
        applog.Integration_Payload__c= reqString;
        HTTPResponse resp= null;
        Http http = new Http();
        req.setMethod('POST');
        String url = System.Label.Physician_Territory_Change_SAP_End_Point_URL;  
        String integrationUsername = System.Label.Integration_Username; 
        String integrationPassword = System.Label.Intergration_UserPassword;
        if(Test.isRunningTest()){
            req.setEndpoint('https://login.salesforce.com');
        }else
        req.setEndpoint(url);
        appLog.Integration_Resource__c= url;
        
        //Added By Rohit for Basic Authorization
        Blob headerValue = Blob.valueOf(integrationUsername+ ':' + integrationPassword);
        String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
        system.debug('authorizationHeader-----'+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('content-type', 'application/json');
        req.setHeader('Accept', 'application/json'); 
        //req.setHeader('Authorization', 'OAuth '+UserInfo.getSessionId());
        //req.setClientCertificateName('tstsalesforce');
        req.setClientCertificateName(CertificateName);
        req.setBody(reqString);
        System.debug('**********REQUEST to EAI :'+reqString);
        System.debug('********** Complete REQUEST '+req);
        if (!Test.isRunningTest()){
            //sending request and receiving response.
            resp = http.send(req);
            system.debug('********** RESPONSE FROM EAI:'+resp.getBody());
        }
        else{
            resp = new HTTPResponse();
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"ResponseDescription":"Success","Acknowledgement":"Error"}');
            resp.setStatusCode(200);            
        }
     
     //logging response
     appLog.Severity__c = 'info';
     Schema.DescribeFieldResult aField = Application_Log__c.Message__c.getDescribe();
     Integer lengthOfField = aField.getLength();
     System.debug('Length of Message Field :'+lengthOfField);
     appLog.Message__c = (resp.getBody().Length()>lengthOfField)?resp.getBody().subString(0,lengthOfField):resp.getBody();
     appLogs.add(appLog);
     insert appLogs;
   
}

    @RestResource(urlMapping='/PhysicianTerritory')
    Webservice static void SendHttpRequestFromBatch(String reqString)
    {
        String CertificateName= Label.CertificateInfo;
System.debug('Label CertificateName__c: '+CertificateName); 


        HttpRequest req = new HttpRequest();
        Application_Log__c appLog= new Application_Log__c();
        appLog.Record_Object_Type__c = 'Physician Territory';
        appLog.Source_Class__c = 'PhysicianRequestHandler';
        appLog.Source_Function__c = 'SendHttpRequest';
        appLog.Source__c = 'SF';
        appLog.Timestamp__c =  System.now();
        applog.Integration_Payload__c= reqString;
        HTTPResponse resp= null;
        Http http = new Http();
        req.setMethod('POST');
        String url = System.Label.Physician_Territory_Change_SAP_End_Point_URL;  
        String integrationUsername = System.Label.Integration_Username; 
        String integrationPassword = System.Label.Intergration_UserPassword;

        if(Test.isRunningTest()){
            req.setEndpoint('https://login.salesforce.com');
        }else
        req.setEndpoint(url);
        appLog.Integration_Resource__c= url;
        //Added By Rohit for Basic Authrization
        Blob headerValue = Blob.valueOf(integrationUsername+ ':' + integrationPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        system.debug('authorizationHeader-----'+authorizationHeader);
        
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('content-type', 'application/json');
        req.setHeader('Accept', 'application/json'); 
        //req.setHeader('Authorization', 'OAuth '+UserInfo.getSessionId());
       // req.setClientCertificateName('tstsalesforce');
       req.setClientCertificateName(CertificateName);
        req.setBody(reqString);
        System.debug('**********REQUEST to EAI :'+reqString);
        System.debug('********** Complete REQUEST '+req);
        if (!Test.isRunningTest()){
            //sending request and receiving response.
            resp = http.send(req);
            system.debug('********** RESPONSE FROM EAI:'+resp.getBody());
        }
        else{
            resp = new HTTPResponse();
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"ResponseDescription":"Success","Acknowledgement":"Error"}');
            resp.setStatusCode(200);            
        }
     
     //logging response
     appLog.Severity__c = 'info';
     Schema.DescribeFieldResult aField = Application_Log__c.Message__c.getDescribe();
     Integer lengthOfField = aField.getLength();
     System.debug('Length of Message Field :'+lengthOfField);
     appLog.Message__c = (resp.getBody().Length()>lengthOfField)?resp.getBody().subString(0,lengthOfField):resp.getBody();
     appLogs.add(appLog);
     insert appLogs;
   
}
     
}
/*
 @CreatedDate     19APR2016                                  
 @ModifiedDate    25APR2016                                  
 @author          Mayuri-Accenture
 @Description     This class is invoked by VF page 'Reset Indicator Mobile Page'. 
 @Methods         SaveMO() 
 @Requirement Id  
 */

public without sharing class ResetIndicatorController_MyObjective{
    
    public  My_Objectives__c MO;    
   @testvisible private static string status;
    private final ApexPages.StandardController CONTROL;
    public ResetIndicatorController_MyObjective(ApexPages.StandardController stdController) {
              this.MO = (My_Objectives__c)stdController.getRecord();
              this.CONTROL = stdController;              
              
      }
    
     public pageReference SaveMO(){
         
         My_Objectives__c moRecord = [ SELECT Id,Name,Territory__c,Territory_Assigned__c  FROM My_Objectives__c where Id =: MO.id];
         
         If(moRecord!=Null){
        
        if(string.IsEmpty(moRecord.Territory__c)){           
            status  = 'showError';
        }
        else if(string.IsNotEmpty(moRecord.Territory__c) && moRecord.Territory_Assigned__c == true){
            status  = 'showMessage';
        }
        else 
        if(string.IsNotEmpty(moRecord.Territory__c) && moRecord.Territory_Assigned__c == false){                  
            moRecord.Territory_Assigned__c = true;
            status = 'Success';    
                   
            try{
                update moRecord;                
                 if(Test.isRunningTest()) /*For test class coverage*/
                   integer intTest =1/0; 
            }
             
            catch(Exception e){
                System.debug('Exception Caught in Apex Class : ResetIndicatorController'+e);
                status = 'Exception';
            }
        
        
        }
        }
        
        if(status == 'Success'){ 
           
           PageReference  pageRef = new PageReference(CONTROL.SAVE().getUrl());
           pageRef.setRedirect(true);
           return pageRef;  
           
        }
        else if(status == 'showMessage'){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,system.Label.EUROPE_ETM_Reset_Territory_Indicator_Message);
            ApexPages.addMessage(myMsg);           
            return null;
        }
        else if(status == 'showError'){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,system.Label.EUROPE_ETM_Reset_Territory_Indicator_Error);
            ApexPages.addMessage(myMsg);           
            return null;
        }
     
        else{
               
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong, contact your system admin');
            ApexPages.addMessage(myMsg);           
            
            
            return null;
            }
        
        
    }    
}
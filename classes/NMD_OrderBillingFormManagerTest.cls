@IsTest(SeeAllData=false)
private class NMD_OrderBillingFormManagerTest {

    static final String STAGE_NAME='Candidate (Trial implant)'; 
    static final String OPTY_TYPE='Standard'; 
    static final String LEADSOURCE = 'CARE Card';
    static final String PAIN_AREA = 'None';
    
    @TestSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();
        User testUser = td.newUser('NMD Field Rep');
        insert testUser;
        System.runAs(new User(Id = UserInfo.getUserId())) {

            Seller_Hierarchy__c territory = td.newSellerHierarchy();
            insert territory;

            Patient__c patient = new Patient__c(
                RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
                Patient_First_Name__c = 'fname',
                Patient_Last_Name__c = 'lname',
                Territory_ID__c = territory.Id,
                SAP_ID__c = td.randomString5()
            );
            insert patient;

            // create one consignment acct for context user, one for a test user
            Account acct1 = td.createConsignmentAccount();
            acct1.OwnerId = testUser.Id;

            Account acct2 = td.newAccount();
            acct2.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
            acct2.Status__c = 'Active';
            acct2.OwnerId = testUser.Id;

            Account acctTrialing = td.newAccount();
            acctTrialing.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
            acctTrialing.Type = 'Ship To';
            acctTrialing.Account_Number__c = td.randomString5();

            Account acctProdcedure = td.newAccount();
            acctProdcedure.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
            acctProdcedure.Account_Number__c = td.randomString5();

            insert new List<Account> { acct1, acct2, acctTrialing, acctProdcedure };

            Contact contTrialing = td.newContact(acctTrialing.Id);
            contTrialing.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            contTrialing.SAP_ID__c = td.randomString5();

            Contact contProcedure = td.newContact(acctProdcedure.Id);
            contProcedure.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            contProcedure.SAP_ID__c = td.randomString5();

            insert new List<Contact> { contTrialing, contProcedure };
            
            Opportunity oppty = td.newOpportunity(acct1.Id);
            oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            oppty.Primary_Insurance__c = acct2.Id;
            oppty.Territory_ID__c = territory.Id;
            oppty.Contact__c=contTrialing.id;            
            oppty.StageName = STAGE_NAME;
            oppty.Opportunity_Type__c = OPTY_TYPE;
            oppty.LeadSource = LEADSOURCE;
            oppty.CloseDate = System.today().addDays(7);
            oppty.Patient__c = patient.Id;
            oppty.Patient_SAP_ID__c = patient.SAP_ID__c;
            oppty.Trialing_Account__c = acctTrialing.Id;
            oppty.Trialing_Account_SAP_ID__c = acctTrialing.Account_Number__c;
            oppty.Trialing_Physician__c = contTrialing.Id;
            oppty.Trialing_Physician_SAP_ID__c = contTrialing.SAP_ID__c;
            oppty.Procedure_Account__c = acctProdcedure.Id;
            oppty.Procedure_Physician__c = contProcedure.Id;
            oppty.Pain_Area__c = PAIN_AREA;
            oppty.Trial_Revenue__c = 10;
                    
           
            Opportunity implantOppty = td.newOpportunity(acct1.Id);
            implantOppty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
            implantOppty.Primary_Insurance__c = acct2.Id;
            implantOppty.Territory_ID__c = territory.Id;
            implantOppty.Opportunity_Type__c = 'Standard';
            implantOppty.StageName = 'Candidate (Trial implant)'; 
            implantOppty.LeadSource = 'CARE Card';
            implantOppty.CloseDate = System.today().addDays(1);
            implantOppty.Patient__c = patient.Id;
            implantOppty.Patient_SAP_ID__c = patient.SAP_ID__c;
            implantOppty.Procedure_Account__c = acctProdcedure.Id;
            implantOppty.Procedure_Account_SAP_ID__c = acctProdcedure.Account_Number__c;
            implantOppty.Procedure_Physician__c = contProcedure.Id;
            implantOppty.Procedure_Physician_SAP_ID__c = contProcedure.SAP_ID__c;
            implantOppty.Pain_Area__c = 'None';
         
            insert new List<Opportunity>{oppty,implantOppty};
             /*Opportunity oppty = td.newOpportunity(acct1.Id);
            oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
            oppty.Primary_Insurance__c = acct2.Id;
            oppty.Territory_ID__c = territory.Id;
            oppty.StageName = 'Implant';
            oppty.CloseDate = System.today().addDays(1);
            oppty.Patient__c = patient.Id;
            oppty.Patient_SAP_ID__c = patient.SAP_ID__c;
            oppty.Trialing_Account__c = acctTrialing.Id;
            oppty.Trialing_Account_SAP_ID__c = acctTrialing.Account_Number__c;
            oppty.Trialing_Physician__c = contTrialing.Id;
            oppty.Trialing_Physician_SAP_ID__c = contTrialing.SAP_ID__c;
            oppty.Procedure_Account__c = acctProdcedure.Id;
            oppty.Procedure_Account_SAP_ID__c = acctProdcedure.Account_Number__c;
            oppty.Procedure_Physician__c = contProcedure.Id;
            oppty.Procedure_Physician_SAP_ID__c = contProcedure.SAP_ID__c;
            oppty.Pain_Area__c = 'None';
            insert oppty;*/

            // create one inv data per acct
            Inventory_Data__c invData1 = new Inventory_Data__c(
                Account__c = acct1.Id,
                Customer_Class__c = 'YY'
            );
          
            insert new List<Inventory_Data__c>{ invData1};

            // create one inv location per inv data
            Inventory_Location__c invLoc1 = new Inventory_Location__c(
                Inventory_Data_ID__c = invData1.Id
            );
            insert new List<Inventory_Location__c>{ invLoc1};

            Order order1 = new Order(
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                AccountId = acct1.Id,
                OpportunityId = oppty.Id,
                Shipping_Location__c = invLoc1.Id,
                EffectiveDate = System.today(),
                Status = 'Draft'
            );
            
              Order order2 = new Order(
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                AccountId = acct1.Id,
                OpportunityId = implantOppty.Id,
                Shipping_Location__c = invLoc1.Id,
                EffectiveDate = System.today(),
                Status = 'Draft'
            );
          
           insert new List<Order>{order1,order2 };
        }

    }

    static testMethod void test_OrderBillingFormManager() {

        
        Order ord = [
            select Id 
            from Order 
            where Opportunity.RecordType.Name = 'NMD SCS Implant'
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT
        ];

        Test.startTest();
        {
            NMD_OrderBillingFormManager.generateAndEmailBillingFormFuture(new Set<Id> { ord.Id });
        }
        Test.stopTest();

    }
     static testMethod void test_OrderBillingFormManager1() {

        Order ord = [
            select Id 
            from Order 
            where Opportunity.RecordType.Name = 'NMD SCS Trial'
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT
        ];

        Test.startTest();
        {
            NMD_OrderBillingFormManager.generateAndEmailBillingFormFuture(new Set<Id> { ord.Id });
        }
        Test.stopTest();

    }
   
}
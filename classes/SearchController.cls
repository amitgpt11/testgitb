/**
 * Description: This class is used as controller for the SearchResults page.
*/
public class SearchController extends AuthorizationUtil{
    
    public Boolean isEnglishUser           {get;set;}
    public Boolean isOtherUser             {get;set;}
    public List<Product2> lstProducts      {get;set;}
    public List<Shared_Community_Product_Translation__c> lstProductTranslation       {get;set;}
    
    /*
    * Description: The construtor initilizes the lstProducts or lstProductTranslation based on User's language to be displayed on Documents page
    */
     public override void fetchRequestedData() {
        
        isEnglishUser = false;
        isOtherUser = false;
        String strSearch = '*'+ apexpages.currentpage().getparameters().get('query') +'*';
        List<List<sObject>> lstsObjects = new List<List<sObject>>();
        lstProducts = new List<Product2>();
        lstProductTranslation = new List<Shared_Community_Product_Translation__c>();
        Set<Id> setParentProdIds = new Set<Id>();
        Set<Id> setParentProdIdsTrsl = new Set<Id>();
      
        User objUser = [Select Shared_Community_Division__c, LanguageLocaleKey, Contact.AccountId
            From User 
            Where Id =: UserInfo.getUserId()];
        
        //Check if user's Shared_Community_Division__c field is not null or blank
        if(objUser.Shared_Community_Division__c != null && objUser.Shared_Community_Division__c != '') {
            
           String query = 'FIND :strSearch IN ALL FIELDS '+
                          'RETURNING Product2 '+
                          '(Id, Product_Label__c, Division__c, ProductCode, Family, Shared_Community_Description__c, Shared_Parent_Product__c,'+
                          'Shared_Parent_Product__r.Id, Shared_Parent_Product__r.Product_Label__c, Shared_Parent_Product__r.Division__c, Shared_Parent_Product__r.ProductCode, Shared_Parent_Product__r.Family, Shared_Parent_Product__r.Shared_Community_Description__c '+
                          'Where Shared_Parent_Product__c != null and ';
            
            Set<String> setUserSharedSalesOrganization = new Set<String>();
            Set<String> setSharedProductExtension = new Set<String>();
            Boolean isCommunityUser = BSC_Util.isCommunityUser();
            
            if(isCommunityUser){
            
                setUserSharedSalesOrganization = BSC_Util.fetchSharedOrgForCommunityUser(objUser.Contact.AccountId);  
                setSharedProductExtension = BSC_Util.fetchProductExtensionsForCommunityUser(setUserSharedSalesOrganization);
                query += 'Id =: setSharedProductExtension and ';
            }
            
            query += 'IsActive = True and '+
                     'Division__c =\''+ objUser.Shared_Community_Division__c +'\''+
                     ' ORDER BY Product_Label__c limit 1000)';
            
            system.debug('setSharedProductExtension======= '+setSharedProductExtension);
            system.debug('query======= '+query);
            
            lstsObjects = search.query(query);

            //if user language is english, add parent product to the list that's displayed on the page
            if(objUser.LanguageLocaleKey == 'en_US') { 
                //Shared_Parent_Product__c IN () and
                system.debug('lst prod records======= '+lstsObjects);
                
                for(Product2 objProduct : (List<Product2>)lstsObjects[0]){
                    
                    if(!setParentProdIds.contains(objProduct.Shared_Parent_Product__c)){
                        
                        setParentProdIds.add(objProduct.Shared_Parent_Product__c);
                        lstProducts.add(objProduct);
                    }
                }
                
                isEnglishUser = true;
            }

            //if user language is not english, add translation record for the parent product & current user's language 
            //to the list that's displayed on the page
            else{
                for(Product2 objProduct : (List<Product2>)lstsObjects[0]){
                    if(!setParentProdIdsTrsl.contains(objProduct.Shared_Parent_Product__c)){
                        setParentProdIdsTrsl.add(objProduct.Shared_Parent_Product__c);
                    }
                }
                for (Shared_Community_Product_Translation__c prdTrsl : [SELECT Id, Shared_Product_Label__c, Shared_Description__c, Shared_Product__c 
                                                FROM Shared_Community_Product_Translation__c 
                                                WHERE Shared_Product__c IN :setParentProdIdsTrsl 
                                                AND Shared_Language__c =: mapLocaleToLanguage.get(objUser.LanguageLocaleKey)
                                                ORDER BY Shared_Product__r.Product_Label__c]){
                    lstProductTranslation.add(prdTrsl);
                } 
                isOtherUser = true;
            }
        }
    }    
}
public class OrderItemSummaryController{
    
    //variable to store order
    public Shared_Community_Order__c receivedOrder{get;set;}
    //Map of order Item Id and orderItem Object
    public Map<string,Shared_Community_Order_Item__c > orderItemMap {get;set;}
    //Map of order Item Id and product Map (product map contains api name and value for field that are not null)
    public Map<String,Map<String,Object>> orderToProductFieldsMap{get;set;}
    
    //Fetch Order Line Items
    public Map<string,Shared_Community_Order_Item__c> getOrderLineItems(){
        
        orderItemMap = new Map<string,Shared_Community_Order_Item__c>(); 
        orderToProductFieldsMap = new Map<String,Map<String,Object>>();
        Map<string,object> productMap = new Map<string,Object>();
        set<Id> productIds = new set<Id>();
        
        //fetch order line items of received order and fill products ids set and orderItemMap
        for(Shared_Community_Order_Item__c orderItem : [select id,Product__c,Shared_Quantity__c,Product__r.Product_Label__c,Product__r.ProductCode from Shared_Community_Order_Item__c where Shared_Order__c =: receivedOrder.Id]){
            
            orderItemMap.put(orderItem.Id,orderItem);
            productIds.add(orderItem.Product__c);
        }
        
        //Create dynamic query to fetch products as per fields in field set named Product_Attribute_Fields
        String Query = 'Select ';
        
        for(Schema.FieldSetMember f : SObjectType.Product2.FieldSets.Product_Attribute_Fields.getFields()) {
            
            Query += f.getFieldPath() + ', ';
        }
        
        Query += 'Id From product2 where Id IN : productIds';
        
        //Iterate over products list of order line item
        for(product2 product : Database.query(Query)){
            
            productMap = new Map<string,Object>();
            
            //Iterate over field set member of field set
            for(Schema.FieldSetMember f : SObjectType.Product2.FieldSets.Product_Attribute_Fields.getFields()) {
                
                //if value of a particular field is not null in product record then add to product map
                if(product.get(f.fieldPath) != null){
                    
                    productMap.put(f.fieldPath,product.get(f.fieldPath));
                }
            }
            
            //fill map of order item id and productMap to display in template
            orderToProductFieldsMap.put(product.Id,productMap);
        }
        
        return orderItemMap;
    }
}
/**
* Extension class for the QuincyProspectReport page
*
* @author   Salesforce services
* @date     2015-02-03
*/
@isTest(SeeAllData=false) 
private class NMD_QuincyReportExtensionTest {

	static final String PROSPECTACCOUNT = 'ProspectAccount';

	static testMethod void test_QuincyProspectReport() {
		//setup test data
		NMD_TestDataManager testData = new NMD_TestDataManager();
		Account acct = testData.newAccount(PROSPECTACCOUNT);
		acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		insert acct;

		List<Contact> contacts = new List<Contact>{};
		for (Integer i = 0; i < 5; i++) {
			contacts.add(testData.newContact(acct.Id));
		}
		insert contacts;

		String contactIdsStr = '';
		for (Contact con : contacts) {
			contactIdsStr += (contactIdsStr.length() > 0 ? ',' : '') + con.Id;
		}

		//build page
		PageReference pr = Page.QuincyProspectReport;
		pr.getParameters().putAll(new Map<String,String> {
			'id' => acct.Id,
			'cid' => contactIdsStr
		});
		Test.setCurrentPage(pr);
		NMD_QuincyReportExtension extension = new NMD_QuincyReportExtension(
			new ApexPages.StandardController(acct)
		);

		//assertions
		System.assertEquals(contacts.size(), extension.children.size());

	}

	static testMethod void test_QuincyPhysicianReport() {
		//setup test data
		NMD_TestDataManager testData = new NMD_TestDataManager();
		Account acct = testData.newAccount(PROSPECTACCOUNT);
		acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		insert acct;

		Contact contact = testData.newContact(acct.Id);
		insert contact;

		//build page
		PageReference pr = Page.QuincyPhysicianReport;
		pr.getParameters().putAll(new Map<String,String> {
			'id' => contact.Id,
			'cid' => acct.Id
		});
		Test.setCurrentPage(pr);
		NMD_QuincyReportExtension extension = new NMD_QuincyReportExtension(
			new ApexPages.StandardController(contact)
		);

		String rbm = extension.userRBMName;
		String asd = extension.userASDName;

		extension.fetchRelatedAttachments(new Set<String>{});

		//assertions
		System.assertEquals(acct.Id, extension.children[0].data.Id);

	}

	static testMethod void test_SendQuincyEmailError() {

		final String errMsg = 'test error';
		System.Savepoint sp = Database.setSavepoint();

		NMD_TestDataManager testData = new NMD_TestDataManager();
		Account acct = testData.newAccount(PROSPECTACCOUNT);
		acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		insert acct;

		NMD_SendQuincyEmailManager manager = new NMD_SendQuincyEmailManager(acct.Id, null);
		manager.error(errMsg, sp);

		//assert correct message was captured
		System.assertEquals(errMsg, manager.lastErrorMessage);
		//assert rollback occurred
		System.assert((new List<Account>([select Id from Account where Id = :acct.Id])).isEmpty());

	}

}
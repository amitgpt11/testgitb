/*

test class for PotentialTriggerHandler
*/
@isTest(SeeAllData=false)

public class PotentialTriggerTest{

static TestMethod void TestPotentialTriggerHandler(){ 
Test.StartTest();
ANZ_TestDataManager tdata = new ANZ_TestDataManager();


account acct= tdata.newAccountANZ('ACCTNAME');  
insert acct;


Account_Performance__c accPerfobj = tdata.newAccountPerformanceANZ(acct.id);
insert accPerfobj ;

Potential__c potObj = tdata.newPotentialANZ(accPerfobj.id);
insert potObj ;
List<Potential__c> potList= new List<Potential__c> ();

potList.add(potObj );

PotentialTriggerHandler pth = new PotentialTriggerHandler();
pth .OnBeforeInsert(potList);
    Test.StopTest();
}



}
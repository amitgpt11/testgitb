public class DivisionAttachmentLinkPageExtension{

public string PAId{get;set;}
public string PAName{get;set;}
public string accId{get;set;}
private final Account acct; 
public list<Shared_Private_Attachments__c> shareAttList = new List<Shared_Private_Attachments__c>();   
public List<User> currUserProfileList = new List<User>();
public string profilename = '';

ApexPages.StandardController stdController;
public id docid {get; set;}
public DivisionAttachmentLinkPageExtension(ApexPages.StandardController stdController)
{ 
      
    this.stdController = stdController;
    accId= ApexPages.currentPage().getParameters().get('id');
    
} 

public pagereference onPageLoad(){
 
      currUserProfileList = [SELECT Profile.Name, Division, Name, Id, Division_Multiselect__c FROM User WHERE Id =:UserInfo.getUserId()/* AND (Profile.Name LIKE 'US PI%' OR Profile.Name LIKE 'US IC%') */];
        profilename = currUserProfileList[0].Profile.Name;
        system.debug('****profilename***'+profilename);
         system.debug('****profil***'+Divisional_Attachments_Division_Mapping__c.getValues(profilename)); 
        string division  = '';
        if(Divisional_Attachments_Division_Mapping__c.getValues(profilename).Division__c != null){
            division = Divisional_Attachments_Division_Mapping__c.getValues(profilename).Division__c;
        } 
        else{
            division = 'IC';
        }   
        system.debug('****division ***'+division );  
        if(accId!= null && accId.contains('001')){
            shareAttList = [SELECT Id,ICPI_Division__c,Account__c FROM Shared_Private_Attachments__c WHERE Account__c =: accId AND ICPI_Division__c =: division limit 1];
            system.debug('****shareAttList ***'+shareAttList ); 
            if(!shareAttList.isEmpty()){
                docid = shareAttList[0].Id;
            }else{
            Shared_Private_Attachments__c o = new Shared_Private_Attachments__c();
            o.Account__c= accId;
            if(string.isNotEmpty(profilename)){
                o.ICPI_Division__c = Divisional_Attachments_Division_Mapping__c.getValues(profilename).Division__c != null ? Divisional_Attachments_Division_Mapping__c.getValues(profilename).Division__c : 'IC';
            }
            try{
                Insert o;
                docid = o.Id;
            }catch(Exception e){
                system.debug('Exception caught-->'+e);
            }            
        }
    }
    return new pageReference('/'+docid);
 }
 


}
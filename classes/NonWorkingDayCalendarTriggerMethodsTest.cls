/*
@ModifiedDate    21SEPT2016
@author          Ashish-Accenture
@Description     The class holds test class for NonWorkingDayCalendarTriggerMethods.
@UserStories     SFDC-1488
*/

@isTest

public class NonWorkingDayCalendarTriggerMethodsTest {
     static testMethod void createDataRecords(){  
         Uro_PH_Non_Working_Day_Calendar__c NWDC = new Uro_PH_Non_Working_Day_Calendar__c();
         NWDC.Uro_PH_Year__c = '2016';
         NWDC.Name = 'US';
         Insert NWDC;
         
         NWDC.Uro_PH_Year__c = '2017';
         Update NWDC;
         
         list<Uro_PH_Non_Working_Day__c> nonWorkingDays = [Select id from Uro_PH_Non_Working_Day__c where Uro_PH_Non_Working_Day_Calendar__c =:NWDC.id];
         System.assertequals(nonWorkingDays.size(),104);
     }
}
/*
 @CreatedDate     12Sept2016                                  
 @ModifiedDate    12Sept2016                                  
 @author         -Accenture
 @Description     
 @Methods         start(),Execute(),Finish() 
 @Requirement Id  
 */
global without sharing class batchUpdatePatientsTrialImplantFlag implements Database.Batchable<sObject>{
 
    List<Opportunity> Allscope = new List<Opportunity>();
    UpdatePatientsTrialImplantFlag upt = new UpdatePatientsTrialImplantFlag();
        
        
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //DateTime dT = System.now();
        //Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        List<Id> OpptyRTIds=new List<Id>{OpportunityManager.RECTYPE_TRIAL_NEW,OpportunityManager.RECTYPE_IMPLANT };
        system.debug('In Start method');    
        String query = 'SELECT Id,Name,Patient__c,Patient__r.Trial_Yes__c,Patient__r.Implant_Yes__c,RecordTypeId,Actual_Trial_Date__c,Actual_Procedure_Date__c  FROM Opportunity where RecordTypeId In: OpptyRTIds';
        system.debug('Query in start'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
    	system.debug('In Execute method');
        if(!scope.isEmpty()){
        Allscope.addAll(scope);
        upt.PatientInfo(scope);
        }
    }   
    
    global void finish(Database.BatchableContext BC) {

        
    }
}
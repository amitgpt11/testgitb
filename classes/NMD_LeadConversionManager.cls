/**
* NMD_LeadConversionManager
*
* @author   Salesforce services
* @date     2014-03-01
*/
public without sharing class NMD_LeadConversionManager {

    final Lead convert;
    final String convertStatus;
    final sObjectType OBJECT_TYPE = Lead.sObjectType;
    public String lastErrorMessage { get; private set; }
    public Id newOpportunityId { get; private set; }

    public NMD_LeadConversionManager(Id leadId) {

        this.lastErrorMessage = '';
        this.newOpportunityId = null;

        try {
            
            this.convert = [
                select
                    Company,
                    ConvertedDate,
                    CreatedDate,
                    LeadSource,
                    Account_Facility__c,
                    Communication_Preference__c,
                    DVD_Received__c,
                    Educated_Date__c,
                    First_Contacted_Date__c,
                    Patient__c,
                    Patient__r.Pain_Area__c,
                    Patient__r.Physician_of_Record__r.AccountId,
                    Patient__r.Primary_Insurance__c,
                    Patient__r.Secondary_Insurance__c,
                    Territory_ID__c,
                    Trialing_Physician__c,
                    Trialing_Physician__r.AccountId,
                    CARE_Card_Physician__c,
                    Secondary_Source__c,
                    Referring_Physician__c,
                    ConvertTo__c,
                    Procedure_Type__c
                
                from Lead 
                where Id = :leadId
            ];

            List<LeadStatus> ls = new List<LeadStatus>([select MasterLabel from LeadStatus where IsConverted = true]);
            this.convertStatus = Test.isRunningTest() ? 'Converted' : ls[0].MasterLabel;

        }
        catch (System.QueryException qe) {
            this.lastErrorMessage = qe.getMessage();
        }

        if (this.convert != null && this.convert.ConvertedDate != null) {
            this.lastErrorMessage = Label.NMD_Lead_Already_Converted + ' ' + this.convert.ConvertedDate.format();
            throw new ConvertLeadException(this.lastErrorMessage);
        }

    }

    // returns the Id of the created Opportunity
    public Boolean convertLead() {
        
        //***Calling method to copy task from lead to patient- dipil
        copyLeadTaskOnPatient(this.convert.Id);
        
        //**What will be account if converted to implant 
        Id acctId = this.convert.Trialing_Physician__c != null
            ? this.convert.Trialing_Physician__r.AccountId
            : this.convert.Account_Facility__c;

        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setAccountId(acctId);
        lc.setConvertedStatus(this.convertStatus); 
        lc.setLeadId(this.convert.Id);

        System.Savepoint sp = Database.setSavepoint();
        DML.deferLogs();
        Boolean success = true;

        try {

            //terminates any pending time-based workflow actions
            Lead l = new Lead(
                Id = this.convert.Id,
                Disable_Workflow__c = true
            );
            DML.save(this, l);
            
            Database.LeadConvertResult lcr = Database.convertLead(lc);

            // catch reason why if it failed
            if (!lcr.isSuccess()) {
                return error(lcr.getErrors()[0].getMessage(), sp);
            }
            
            //find the ASP_Trial_amount value of Territory_ID__c, if it is present. 
            Double aspAmount = 0;
            //**need to add aspAmount for Impant opty with shc.ASP_Implant__c
            
            /*if( acctId!= null){   
                Account acc;      
                try{
                    acc = [select id, Trial_ASP_Value__c, Implant_ASP_Value__c from Account where id= :acctId limit 1];
                    //**added by amitabh 11/9
                    if(this.convert.ConvertTo__c == 'Trial'){
                        aspAmount = acc.Trial_ASP_Value__c;
                    }else{
                        aspAmount = acc.Implant_ASP_Value__c;
                    }   
                }catch(QueryException qe){
                    System.debug(qe);
                }
                 
            }*/

            this.newOpportunityId = lcr.getOpportunityId();
            system.debug('Required Trail Id=='+ OpportunityManager.RECTYPE_TRIAL_NEW);
            //map our custom fields
            //**map oppty as per the selection of converion to trial or Implant Oppty
            Opportunity newOppty = new Opportunity(
                Id = this.newOpportunityId,
                Name = this.convert.Company,
                //changes for US2436 started
                //RecordTypeId = OpportunityManager.RECTYPE_TRIAL,
                
                //**Below line commented by Amitabh and set the RT as per the converted oppty type as per Q4 changes
                //RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW,
                //changes for US2436 ends
                DVD_Received__c = this.convert.DVD_Received__c, 
                First_Contacted_Date__c = safeDate(this.convert.First_Contacted_Date__c),
                Lead_Created_Date__c = this.convert.CreatedDate,
                Lead_Educated_Date__c = this.convert.Educated_Date__c,
                Patient__c = this.convert.Patient__c,
                Territory_ID__c = this.convert.Territory_ID__c,
                ASP_Trial_Amount__c = aspAmount, 
                //changes for US2436 started
                //Trialing_Account__c = this.convert.Account_Facility__c,
                //Trialing_Account__c = this.convert.Patient__r.Physician_of_Record__r.AccountId,
                Pain_Area__c =  this.convert.Patient__r.Pain_Area__c,
                Primary_Insurance__c = this.convert.Patient__r.Primary_Insurance__c,
                Secondary_Insurance__c = this.convert.Patient__r.Secondary_Insurance__c,
                Opportunity_Type__c = 'Standard',                   
            //changes for US2436 ends
            
            //changes for US2458 started      
                Referral_MD__c = this.convert.Referring_Physician__c,
            //changes for US2458 ends  
            
            //**trailing physician is set only if oppty type is Trail ,so commented below line
                //Trialing_Physician__c = this.convert.Trialing_Physician__c,
                
                CARE_Card_Physician__c = this.convert.CARE_Card_Physician__c,
                Secondary_Source__c = this.convert.Secondary_Source__c,
                IsExcludedFromTerritory2Filter = True
            );
            //**added by amitabh 11/9
            system.debug('Convert to==>'+this.convert.ConvertTo__c);
            if(this.convert.ConvertTo__c == 'Trial'){
                system.debug('Convert to==>'+this.convert.ConvertTo__c);
                newOppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
                newOppty.Trialing_Physician__c = this.convert.Trialing_Physician__c;
            } 
            else if(this.convert.ConvertTo__c == 'Implant'){
                system.debug('Convert to==>'+this.convert.ConvertTo__c);
                newOppty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
                newOppty.Procedure_Physician__c = this.convert.Trialing_Physician__c;
            }
            else if(this.convert.ConvertTo__c == 'Revision'){
                system.debug('Convert to==>'+this.convert.ConvertTo__c);
                newOppty.RecordTypeId = OpportunityManager.RECTYPE_REVISION;
                newOppty.Procedure_Physician__c = this.convert.Trialing_Physician__c;
                newOppty.Procedure_Type__c =this.convert.Procedure_Type__c; 
                newOppty.I_Acknowledge__c = true;
                newOppty.StageName='Procedure Candidate (Revision/Explant)';
            }
            
            
            //update newOppty;
            DML.save(this, newOppty);
            system.debug('Leas conversion oppy  '+newOppty);
            system.debug('Required Trail Id=='+newOppty.RecordTypeId+' required RT'+OpportunityManager.RECTYPE_TRIAL_NEW);
            //do not want the contact that was created by the conversion process
            Contact c = new Contact(
                Id = lcr.getContactId()
            );
            DML.remove(this, c);

            // create share records for associated territory assignees
            new OpportunityManager().opportunitySharing(new List<Opportunity> { newOppty });
            System.debug('Calling Method to Update Opty Line item ==>:'+newOppty);
            new OpportunityManager().updateOptyLineItem(new List<Opportunity> { newOppty });
            System.debug('Calling Method to Update Opty Line item call ends==>:'+newOppty);
            System.debug('Calling Method to Update Opty Line Item Price :'+newOppty);
            new OpportunityManager().updateOptyLineItemPrice(new List<Opportunity> { newOppty });
            System.debug('Called Method to Update Opty Line Item Price :'+newOppty);
            
            //new OpportunityManager().opportunitySharing(new List<Opportunity> { newOppty });
            //added as per Q3 Sprint 1 -Amitabh
           // new OpportunityManager().createClinicalDataSummary(newOppty);
            //new OpportunityManager().commitClinicalDataSummaries();
            

        }
        catch (System.DmlException de) {
            success = error(de.getDmlMessage(0), sp);
        }

        DML.flushLogs(false);

        return success;

    }

    private Date safeDate(DateTime dt) {
        return dt == null ? null : dt.date();
    }

    @testVisible
    private Boolean error(String errMsg, System.Savepoint sp) {
        this.lastErrorMessage = errMsg;
        if (sp != null) {
            Database.rollback(sp);
        }
        return false;
    }

    public class ConvertLeadException extends Exception {}
    
    //before converting the lead - copy all tasks to associated patient. 
    public void copyLeadTaskOnPatient(Id leId){                                             
        List<Task> Tasks =[Select CreatedById,CreatedDate,CurrencyIsoCode,DB_Activity_Type__c,DDL1__c,DDL2__c,Description,Division__c,ENDO_Other_Type__c,Estimated_Cost__c,EU_Event_Comment__c,EU_Start_Date__c,Event_Status__c,Event_Subject__c,FMM__c,footprints__TaskTemplateId__c,Id,Interaction_Type__c,IsArchived,IsClosed,IsDeleted,IsHighPriority,IsRecurrence,IsReminderSet,IsVisibleInSelfService,LAAC_Meeting_Objective__c,LAAC_TAX_Type__c,LastModifiedById,LastModifiedDate,Mailed_Invitations_Y_N__c,Main_Objective__c,Make_Me_Owner__c,Marketing_Activity_Type__c,Meeting__c,Message__c,National_Tiering__c,Newspaper_Ads_Y_N__c,NMD_Event__c,NMD_Physician_Event_Type__c,NMD_Sub_event__c,Notes__c,Num_Ad_Runs__c,Num_Female_Attendees__c,Num_Invitations_Mailed__c,Num_Male_Attendees__c,Num_Opt_In_Forms_Collected__c,openq__Activity_and_Objective_ID__c,openq__Date_Completed__c,openq__Event_Topic__c,openq__Event_Type__c,openq__Event__c,openq__Interaction_Topic_1__c,openq__Interaction_Topic_2__c,openq__Interaction_Topic_3__c,openq__Interaction_Type_1__c,openq__Interaction_Type_2__c,openq__Interaction_Type_3__c,openq__Interaction_Type__c,openq__Interaction__c,openq__Owner_Name__c,Outcome__c,OwnerId,Patient_Advocate_Name__c,Patient_Advocate_Y_N__c,Percent_Complete__c,Physicians_List_Utilized_Y_N__c,Priority,Program_ID__c,Program_Type__c,Purchased_List_Y_N__c,Reason_for_Closure__c,RecordTypeId,RecurrenceActivityId,RecurrenceDayOfMonth,RecurrenceDayOfWeekMask,RecurrenceEndDateOnly,RecurrenceInstance,RecurrenceInterval,RecurrenceMonthOfYear,RecurrenceRegeneratedType,RecurrenceStartDateOnly,RecurrenceTimeZoneSidKey,RecurrenceType,ReminderDateTime,Shared_Event_Status__c,Status,Subject,SystemModstamp,Task_Close_Date__c,Territory_Tiering__c,Topics__c,Type,Type__c,Uro_Ph_Call_Attempt__c,Uro_Ph_Contact_Action__c,Uro_Ph_Contact_Type__c,Uro_Ph_Resources__c,Uro_Ph_Topics__c,Uro_Ph_Topic_Source__c,USEP_Completion_Date__c,User_Division_Multi__c,User_Division__c,User_Region__c,US_EP_Migrated__c,WhatCount,WhatId,WhoCount,WhoId from Task where WhoId=:leId];   
        createTasksOnPatients(Tasks);
    } 
    
    public void createTasksOnPatients(List<Task> newTasks){
        List<Task> patientsTasksToCreate = new List<Task>();    
        Map<Id,Lead> lds = new Map<Id,Lead>(); 
        
        for (Task task : newTasks) {
        if (task.WhoId != null
             && task.WhoId.getSObjectType() == OBJECT_TYPE ){
             lds.put(task.WhoId, null);
            }
         }
        system.debug('############ :'+lds.KeySet());
        for (Lead lead : [
                select
                Id,
                Patient__c,
                Patient__r.id,
                Trialing_Physician__c
            from Lead
            where Id in :lds.keySet()
        ]) {
            lds.put(lead.Id, lead);
           // patients.put(lead.Patient__c, null);
        }
        system.debug('############ AFTER :'+lds);
        //system.debug('############ patients :'+patients);
        for (Task task : newTasks) {
            if (task.WhoId != null
             && task.WhoId.getSObjectType() == OBJECT_TYPE ){
                Lead parentLead = lds.get(task.WhoId);
                    if (parentLead == null) continue;
                system.debug('############ parentLead :'+parentLead);
               // Patient__c patient = patients.get(parentLead.Patient__c);
                Task t1= task.clone(false, true, true, false);
                 system.debug('############ patient:'+parentLead.patient__c);
                system.debug('############ t1.WhatId'+t1.WhatId);
               
                t1.whoId=null;
                t1.WhatId = parentLead.patient__c;
                system.debug('############ AFTER t1.WhatId'+t1.WhatId);
                patientsTasksToCreate.add(t1);
                system.debug('############ patientsTasksToCreate'+patientsTasksToCreate);
             }
         }
                                
            if(!patientsTasksToCreate.isEmpty()){
                    DML.save(this, patientsTasksToCreate);
            }
    
    }
    
}
@isTest
private class NMD_OrderHistoryManagerTest {

    static final String ACCTNAME = 'AcctName';
    static final RestDispatcherV1 dispatcher = new RestDispatcherV1(null);

    static {
        RestContext.response = new RestResponse();
    }

    @testSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();

        Pricebook2 nmdBook = td.newNMDPricebook();
        insert nmdBook;

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create account
        Account acct = td.createConsignmentAccount();
        acct.Name = ACCTNAME;
        insert acct;

        //create oppty
        Opportunity oppty0 = td.newOpportunity(acct.Id);
        oppty0.Territory_Id__c = hierarchy.Id;
        oppty0.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        insert oppty0;

        Order order = new Order(
            AccountId = oppty0.AccountId,
            OpportunityId = oppty0.Id,
            EffectiveDate = System.today(),
            Pricebook2Id = nmdBook.Id,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT
        );
        insert order;

    }

    static testMethod void test_GetOrderHistory() {

        Test.startTest();

            NMD_OrderHistoryManager.getOrdersHistory();

        Test.stopTest();

    }

    static testMethod void test_OrdersHistoryDispatcher(){
        
        Test.startTest();

            NMD_OrdersHistoryDispatcher handler = new NMD_OrdersHistoryDispatcher();

            handler.getURIMapping();

            handler.execute(dispatcher, null, null);

        Test.stopTest();

    }
    
}
/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/manageMyKOLList/*')
global class RestManageMyKOLList {
    global RestManageMyKOLList() {

    }
    @HttpPost
    global static openq.OpenMSL.ADD_REMOVE_KOL_RESPONSE_element manageMyKOLs(openq.OpenMSL.ADD_REMOVE_KOL_REQUEST_element request_x) {
        return null;
    }
}

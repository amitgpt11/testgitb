@isTest
private class CustomerCalendarController_Test {
    
    private static testMethod void test() {
        
        system.Test.setCurrentPage(Page.CustomerCalendar);
        CustomerCalendarController objCustomerCalendarController = new CustomerCalendarController();
        
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        objUser = [SELECT Id, Contact.ownerId ,contact.owner.TimeZoneSidKey,TimeZoneSidKey
                    FROM User
                    WHERE id =: objUser.Id];
        
        Test.StartTest();
        
            objCustomerCalendarController.init();
            
            system.runAs(objUser){
    
                //create Boston scientific cofig test data using Test_DataCreator class
                Id eventID = 'a2nR000000055WA';
                Test_DataCreator.cerateBostonCSConfigTestData(eventID); 
                
                DateTime start_Date = datetime.now();
                DateTime end_Date = datetime.now();
                Meeting__c objMeeting = Test_DataCreator.createMeetingForCalendar('TestEvent', start_Date, end_Date.addDays(5), 'Accepted', objUser.Contact.ownerId);
                String dataInJSONString = '{"AppointmentTopic":null,"ContactId":"'+ objUser.ContactId +'","Description":"Test event","Start_TimeSelected":"08:00 am","EventId":"'+ eventID +'","isAllDayMeeting":false,"Meeting_Request_Status":"test","Start_DateSelected":"October 16, 2016","Name":"test"}';
                
                Meeting__c meetingObj = new Meeting__c(Name='Community Site Appointment Request', Start_Time__c = system.now(), End_Time__c= system.now().addHours(1),Meeting_Request_Status__c = 'Pending',AssignedTo__c = objUser.Contact.ownerId);
                insert meetingObj;
                
                Holiday hn= new holiday();
                hn.name='Test';
                hn.activitydate=date.today();
                insert hn;
                
                objCustomerCalendarController = new CustomerCalendarController();
                objCustomerCalendarController.init();
                CustomerCalendarController.getCurrentUserdetail();
                
                system.assertNotEquals(CustomerCalendarController.getMeetings().size(),0);            
                system.assertNotEquals(CustomerCalendarController.insertMeeting(dataInJSONString),null);
                
                CustomerCalendarController.getMeetingsOfSelectedDate('2016-10-18T08:00:00');
            }
        
        Test.StopTest();
    }
}
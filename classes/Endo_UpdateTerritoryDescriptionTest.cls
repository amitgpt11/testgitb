@isTest(SeeAllData=false)
public class Endo_UpdateTerritoryDescriptionTest {
   public map<string,Territory2> territoryNameMap = new map<string,Territory2>();
    public static testmethod void updateTerrDescription(){
         
        Test.startTest();
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
            
            List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();
            Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
            
            Territory2 t1 = new Territory2(
                Name = '900689254162',
                AccountAccessLevel = 'Edit',
                ContactAccessLevel = 'None',
                Territory2ModelId = Territory2ModelIDActive,
                DeveloperName= 'Xxxxxx9006892',
                Territory2TypeId = Ttype[0].Id,
                Description = 'Miami Pulm',
                ParentTerritory2Id = System.Label.ENUSTerritory
            );

            insert new List<Territory2> {t1};
            //territoryNameMap.put(t1.Name, t1);
            
            Territory_Model__c tm = new Territory_Model__c();
            tm.Territory_Description__c = 'TestGI';
            tm.Territory_Name__c = t1.Name;
            tm.Type__c = 'Territory';
            tm.Region_Name__c = 'TestABCD';
            tm.Area_Name__c = 'TestEFGH';
            
            List<Territory_Model__c> tmlist= new List<Territory_Model__c> {tm};
            insert tmlist;
            
            Territory_Model_Record_Pick__c tmrp = new Territory_Model_Record_Pick__c();
            tmrp.Pick_Last_Modified_Today__c = true;
            tmrp.Name = 'Last Modified Today';
            
            insert new List<Territory_Model_Record_Pick__c> {tmrp};
            
            Endo_UpdateTerritoryDescription b = new Endo_UpdateTerritoryDescription(); 
            database.executebatch(b,2);
            
            ScheduleEndo_UpdateTerritoryDescription sh1 = new ScheduleEndo_UpdateTerritoryDescription();      
            String sch = '0  00 1 3 * ?';
            system.schedule('Test', sch, sh1);
     
        Test.stopTest();
    }
}

public static testmethod void updateTerrDescription1(){
       
        Test.startTest();
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
            
            List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();
            Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
            
            Territory2 t2 = new Territory2(
                Name = 'Test24352',
                AccountAccessLevel = 'Edit',
                ContactAccessLevel = 'None',
                Territory2ModelId = Territory2ModelIDActive,
                DeveloperName= 'Xxxxxx9006892xxx',
                Territory2TypeId = Ttype[0].Id,
                ParentTerritory2Id = System.Label.ENUSTerritory,
                Description ='tedet'
            );
            Territory2 t3 = new Territory2(
                Name = 'Test352232345',
                AccountAccessLevel = 'Edit',
                ContactAccessLevel = 'None',
                Territory2ModelId = Territory2ModelIDActive,
                DeveloperName= 'Xxxxxx90068YYY',
                Territory2TypeId = Ttype[0].Id,
                ParentTerritory2Id = t2.Id,
                Description ='tedet'
            );
            insert new List<Territory2> {t2, t3};
            //territoryNameMap.put(t1.Name, t1);
            
            Territory_Model__c tm = new Territory_Model__c();
            tm.Territory_Description__c = 'TestGI';
            tm.Territory_Name__c = '54252633xxxxxx';
            tm.Type__c = 'Territory';
            tm.Region_Name__c = t2.Name;
            tm.Area_Name__c = t3.Name;
            
            List<Territory_Model__c> tmlist= new List<Territory_Model__c> {tm};
            insert tmlist;
            
            /*Territory2 t1 = new Territory2(
                Name = tm.Territory_Name__c,
                AccountAccessLevel = 'Edit',
                ContactAccessLevel = 'None',
                Territory2ModelId = Territory2ModelIDActive,
                DeveloperName= 'Xxxxxx9006892',
                Territory2TypeId = Ttype[0].Id,
                ParentTerritory2Id = System.Label.ENUSTerritory,
                Description = tm.Territory_Description__c
            );

            insert new List<Territory2> {t1};*/
            
            Territory_Model_Record_Pick__c tmrp2 = new Territory_Model_Record_Pick__c();
            tmrp2.Pick_Last_Modified_Today__c = false;
            tmrp2.Name = 'Last Modified Today';
            
            insert new List<Territory_Model_Record_Pick__c> {tmrp2};
            
            Endo_UpdateTerritoryDescription b = new Endo_UpdateTerritoryDescription(); 
            database.executebatch(b,2);
            
            ScheduleEndo_UpdateTerritoryDescription sh1 = new ScheduleEndo_UpdateTerritoryDescription();      
            String sch = '0  00 1 3 * ?';
            system.schedule('Test', sch, sh1);
     
        Test.stopTest();
    }
}
}
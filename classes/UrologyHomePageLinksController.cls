Public with sharing class UrologyHomePageLinksController {
  public List<Urology_Links__c> urologyLinks{get;set;}
  
  
 public UrologyHomePageLinksController ()
    {
        Map<String,Urology_Links__c> allLinks= Urology_Links__c.getAll();
        urologyLinks= [Select Name,Urology_url__c,Name_del__c from Urology_Links__c Order By Name_del__c ASC];
       
        system.debug('====urologyLinks==='+urologyLinks);
    }  
}
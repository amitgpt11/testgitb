/*
 @CreatedDate     04APR2016                                  
 @ModifiedDate    12APR2016                                  
 @author          Mayuri-Accenture
 @Description     This Batch Apex class checks for Territory single match/multiple match on Opportunity object from Opportunity Owner's territory and Opportunity's Account territory.
                  If single match found, assign the territory to Oppty and mark the Territory_Assigned__c checkbox to TRUE.
                  If multiple/no matches are found leave the Territory field on Oppty as is and mark Territory_Assigned__c checkbox to FALSE.
                  This happens when Account_Team_Member__c are added to Salesforce fom SAP. 
 @Methods         start(),Execute(),Finish() 
 @Requirement Id  
 */
 
 global class batchOpptyTerritoryUpdate implements Database.Batchable<sObject> { 
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
    Date dt;
    if(Test.isRunningTest()){   
        dt = system.today();
    }
    else{
        dt = system.today()-1;
        //Integer.valueOf(system.Label.ETM_Batch_Integer_Subtractor_For_Date);
    }
    String query =  'select ' +
                    'Account_Team_Role__c, ' +
                    'Personnel_ID__r.User_for_account_team__c, ' +
                    'Personnel_ID__r.User_for_account_team__r.IsActive, ' +
                    'Account_Team_Share__c, ' +
                    'Deleted__c, ' +
                    'Territory_ID__c ' +
                    'from Account_Team_Member__c ' +
                    'where Territory_ID__c != null ' ;
                    //'and Account_Team_Share__c != null ' +                                   //Commented Q4, as ENDO ATMs do not always have Accounts
                    //'and Personnel_ID__r.User_for_account_team__c != null ' +                //Commneted during Q3 2016, as ENDO ATM resords do not have personal Ids in their records
                    //'and Account_Team_Role__c != null ';                                     //Commented Q4, as ENDO ATMs do not always have Personnel IDs
        if(Test.isRunningTest()){            
               query += 'and LastModifiedDate =: dt';
       }
       else{
             query += 'and LastModifiedDate >=: dt';
       }             
                     
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account_Team_Member__c> scope) {
        set<Id> userIdLst = new set<Id>();
        set<Id> accountIdLst = new set<Id>();
        list<ObjectTerritory2Association> acctTerritoryLst = new list<ObjectTerritory2Association>();
        list<UserTerritory2Association> userTerritoryLst = new list<UserTerritory2Association>();
        map<id,set<Id>> acctTerritorymap = new map<id,set<Id>>();
        map<id,set<Id>> userTerritorymap = new map<id,set<Id>>();
        map<Id,Opportunity> opptyMap;
        list<Opportunity> opptyLst = new list<Opportunity>();
        list<Opportunity> AllopptyLst = new list<Opportunity>();
        list<Opportunity> ENDOopptyLst = new list<Opportunity>();
        UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
        set<Id> userIdLst1 = new set<Id>();
        set<Id> accountIdLst1 = new set<Id>();
        set<string> recordTypeIdLst = new set<string>();
        final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName(); 
         final Id RECTYPE_ENDO_DISPOSABLE_OPTY = RECTYPES.get('Endo New Disposable Business').getRecordTypeId();
         final Id RECTYPE_ENDO_CAPITAL_OPTY = RECTYPES.get('Endo Capital Business').getRecordTypeId();
        
        system.debug('--scope'+scope+'--'+scope.size());
        for(Account_Team_Member__c a: scope){
            if(a.Personnel_ID__r.User_for_account_team__c != null){                  //Added during Q3 2016, as ENDO ATM resords do not have personal Ids in their records
                userIdLst.add(a.Personnel_ID__r.User_for_account_team__c);          //create a list of all the User Ids
            }
            accountIdLst.add(a.Account_Team_Share__c);                          //Create a list of all Account Ids
        }
        system.debug('--userIdLst--'+userIdLst);
        system.debug('--accountIdLst--'+accountIdLst);
 
        recordTypeIdLst = ETM_Opportunity_Record_Types__c.getall().keyset();
        //Query on Oppty whose owner is in UserList OR whose Account is in AccountList created above
        //if(accountIdLst.size()>0 && userIdLst.size()>0){
            opptyMap = new map<Id,Opportunity>([SELECT id,Name,AccountId,Territory2Id,Territory_Realignment_Status__c,OwnerId,StageName,Probability,recordTypeId,ENDO_Secondary_Territory__c,
                                                SecondaryTerritoryDescription__c,Secondary_Territory_Area__c,SecondaryTerritoryRegion__c 
                                                FROM Opportunity
                                                WHERE ( OwnerId IN : userIdLst OR AccountId IN : accountIdLst ) AND RecordTypeId IN : recordTypeIdLst]); // AND Probability != 100 ***Commented out 11-30 JD to include closed opportunities
                                                
            AllopptyLst.addAll(opptyMap.values());                                 
        //}    
        system.debug('--opptyMap --'+opptyMap);   
        system.debug('--AllopptyLst--'+AllopptyLst);
        system.debug('--AllopptyLst - size--'+AllopptyLst.size());
        // -- Added during Q3 2016 - Logic for ENDO
        for(Opportunity o : AllopptyLst){
            if(o.recordTypeId != null && (o.recordTypeId == RECTYPE_ENDO_DISPOSABLE_OPTY || o.recordTypeId == RECTYPE_ENDO_CAPITAL_OPTY)){
                ENDOopptyLst.add(o);                //Collect ENDO oppty
             } 
             else{
              if(o.Probability != 100){             // Added back after removing condition from AllopptyLst 11-30 JD
                 opptyLst.add(o);                    //Collect Europe oppty
              }   
             }
        
        }
        system.debug('--ENDOopptyLst--'+ENDOopptyLst);
        system.debug('--opptyLst--'+opptyLst);
        for(Opportunity op : opptyLst){
            if(op.AccountId != null){
                accountIdLst1.add(op.AccountId);
            }
            if(op.OwnerId != null){
                userIdLst1.add(op.OwnerId);
            }
        }
        
        //Query On ObjectTerritory2Association for all the Territories assigned to Accounts mentioned in accountIdLst
        if(accountIdLst1!=null && accountIdLst1.size()>0){
            
            acctTerritorymap = util.getAcctnTeritryAssctnList(accountIdLst1);                       //Call a Utility method to query ObjectTerritory2Association records                        
                     
        }
        system.debug('--acctTerritorymap--'+acctTerritorymap);
        //Query On UserTerritory2Association for all the Territories assigned to Users mentioned in userIdLst
        if(userIdLst1!=null && userIdLst1.size()>0){
                    
            userTerritoryLst = util.getUserTeritryAssctnList(userIdLst1,true);                           //Call a Utility method to query UserTerritory2Association records      
            if(userTerritoryLst != null && userTerritoryLst.size() > 0){
                for(UserTerritory2Association ut : userTerritoryLst){
                    if(userTerritorymap.containsKey(ut.UserId)){
                        userTerritorymap.get(ut.UserId).add(ut.Territory2Id);                           //Create a map of User Id-->List of Territories
                    }
                    else{
                        userTerritorymap.put(ut.UserId,new set<Id>{ut.Territory2Id});
                    }
                }
                
            }       
        }

        
        system.debug('--userTerritorymap--'+userTerritorymap);
        map<Id,Id> opptyTerAssignmap = new map<Id,Id>();
        map<Id,Id>  tempOpttyLst;       
        set<Id> opptyIdLstWthFalseCheckbx = new set<Id>();
        for(Opportunity opt : opptyLst){        
            if(userTerritorymap.keyset().Contains(opt.OwnerId) && acctTerritorymap.keyset().Contains(opt.AccountId)){       //If User to Territories map doesn't contain Oppty's owner and Account to Territories map doesn't contain Oppty's Account               
                if(userTerritorymap.get(opt.OwnerId).size() > 0 && acctTerritorymap.get(opt.AccountId).size() > 0){         //If User to Territories map doesn't have territories for Oppty's owner and Account to Territories map doesn't have territories for Oppty's Account 
                    //Find a match for Territory                
                    integer cnt = 0;
                    for(Id tId : userTerritorymap.get(opt.OwnerId)){
                        if(acctTerritorymap.get(opt.AccountId).contains(tId)){
                            cnt++;                                                                              //Increment the count when multiple matches are found
                            if(cnt == 1){
                                tempOpttyLst = new map<Id,Id>();
                                tempOpttyLst.put(opt.Id,tId);
                            }
                        }                           
                    }
                    if(cnt == 1){                                                                           //If only one match is found put it in a map of <Oppty Id-->Territory id>
                        opptyTerAssignmap.putAll(tempOpttyLst);
                    }
                    if(cnt>1 || cnt == 0){
                        OpptyIdLstWthFalseCheckbx.add(opt.id);                                              //If multiple matches are found put Oppty id in another map
                    }  
                    
                    if(opt.Territory2Id  != null){                                                                          //If Oppty's Territory field is not null--
                        if(userTerritorymap.get(opt.OwnerId).Contains(opt.Territory2Id) == false &&  acctTerritorymap.get(opt.AccountId).Contains(opt.Territory2Id) == false){      //--and the two maps does not conatin the Oppty's Territory mark the checkbox on Oppty to false--
                            if(!opptyTerAssignmap.keyset().contains(opt.id)){
                                opptyIdLstWthFalseCheckbx.add(opt.id);  
                            }                                                                                                               //--if it has then do nothing
                        }
                    }                       
                }
                else{
                    opptyIdLstWthFalseCheckbx.add(opt.id);                          //If both UserTerritoryMap and AccountTerritoryMap haveno Territories for Oppty owner and Oppty Account in it, mark the checkbox in Oppty to false
                }
            }
            else{
                    opptyIdLstWthFalseCheckbx.add(opt.id);                          //If both UserTerritoryMap and AccountTerritoryMap have no Oppty owner and no Oppty Account in it, mark the checkbox in Oppty to false
            }           
        }
        list<Opportunity> updatedOpptyLst = new list<Opportunity>();
        Set<Opportunity> updatedOpptySet = new Set<Opportunity>(); // Added to avoid duplicate Id issue
        if(opptyTerAssignmap!= null && opptyTerAssignmap.keyset().size() > 0){
            for(Id opId : opptyTerAssignmap.keyset()){ 
                Opportunity newOppty = new Opportunity(); 
                newOppty.Id =  opId;            
                newOppty.Territory2Id = opptyTerAssignmap.get(opId);
                newOppty.Territory_Assigned__c = true;
                updatedOpptyLst.add(newOppty);
            }
        }
        
        if(opptyIdLstWthFalseCheckbx.size()>0){
            for(Id opt : opptyIdLstWthFalseCheckbx){
                Opportunity newOppty = new Opportunity();
                newOppty.Id =  opt;  
                newOppty.Territory_Assigned__c = false;
                updatedOpptyLst.add(newOppty);
            }
        }
        
        // -- Added during Q3 2016 - Logic for ENDO
        list<Opportunity> ENDOUpdateOpptyLst = new list<Opportunity>();
        UtilForAssignTerritorytoOpportunity utoppty = new UtilForAssignTerritorytoOpportunity();         
        if(ENDOopptyLst!=null && ENDOopptyLst.size() > 0){
            ENDOUpdateOpptyLst = utoppty.batchOpptyTerritoryUpdateENDO(endoOpptyLst);
        }
        if(ENDOUpdateOpptyLst!= null && ENDOUpdateOpptyLst.size() > 0){
            updatedOpptySet.addAll(ENDOUpdateOpptyLst); // Added to avoid duplicate Id issue
            //updatedOpptyLst.addAll(ENDOUpdateOpptyLst);
        }
        // Added to avoid duplicate Id issue
        if(updatedOpptySet.size() > 0){
            updatedOpptyLst.addAll(updatedOpptySet);
        }
        if(updatedOpptyLst != NULL && updatedOpptyLst.size()>0){
                    Database.SaveResult[] InsertResult = Database.update(updatedOpptyLst,TRUE);
                            
                    // Error handling code Iterate through each returned result
                    for (Database.SaveResult er : InsertResult) {
                        if (er.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully updated the Record. Record ID: ' + er.getId());
                            
                        }
                        else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : er.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Record fields that affected this error: ' + err.getFields());
                            }
                        }
                    }                   
             } 
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}
/**
* Trigger handler for the Cycle_Count_Response_Analysis__c object
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class CycleCountResponseAnalysisTriggerHandler extends TriggerHandler {
    
    final CycleCountResponseAnalysisManager manager = new CycleCountResponseAnalysisManager();

    public override void bulkBefore() {

       
    }
    
    public override void bulkAfter() {
       List<Cycle_Count_Response_Analysis__c> ccras = (List<Cycle_Count_Response_Analysis__c>)trigger.new;
        manager.fetchRelatedVerifications(ccras);
    }
    
    public override void beforeInsert(SObject so) {
        
    }

    public override void afterInsert(SObject so) {

 
    }
    
    public override void beforeUpdate(SObject oldObj, SObject obj) {

       

    }

    public override void afterUpdate(SObject oldObj, SObject newObj) {
        Cycle_Count_Response_Analysis__c oldCcra = (Cycle_Count_Response_Analysis__c)oldObj;
        Cycle_Count_Response_Analysis__c newCcra = (Cycle_Count_Response_Analysis__c)newObj;
        manager.processVerifications(oldCcra,newCcra);
    }

    public override void andFinally() {
        manager.commitVerificationChanges();     
    }


}
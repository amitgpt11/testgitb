public class OrderRequest{
    public cls_Order_v1 Order_v1;
    public class cls_Order_v1 {
        public cls_OrderBasic OrderBasic;
        public cls_OrderDelivery OrderDelivery;
        public cls_OrderBilling OrderBilling;
        public cls_OrderSalesInfo OrderSalesInfo;
        public cls_CustPOInfo CustPOInfo;
        public cls_ItemInfo[] ItemInfo;
        public cls_Price Price;
        public cls_Inventory Inventory;
        public cls_SurgeryInfo SurgeryInfo;
        public cls_OrderPartners OrderPartners;
        public cls_Text Text;
    }
    public class cls_OrderBasic {
        public String orderNumber;  //500064757
        public String orderReason;  //G35
        public String orderType;    //ZKB
        public String operationType;
    }
    public class cls_OrderDelivery {
        public String shippingCondition;    //YB
        public String deliveryBlock;    //true
        public String requestedDeliveryDate;    //2016-09-27
    }
    public class cls_OrderBilling {
        public String billingBlock; //false
    }
    public class cls_OrderSalesInfo {
        public String salesOrganization;    //US10
    }
    public class cls_CustPOInfo {
        public String customerPurchaseOrderNumber;  //Pending PO
        public String customerPurchaseOrderType;    //ConsignmentOrder
    }
    public class cls_ItemInfo {
        public String itemNumber;   //10
        public String itemProposalNumber;   //50000819
        public String orderQuantity;    //1
        public String lineItemStatus;
        public cls_MaterialInfo MaterialInfo;
    }
    public class cls_MaterialInfo {
        public String materialNumber;   //M000100003
    }
    public class cls_Price {
        public cls_orderPrice orderPrice;
    }
    public class cls_orderPrice {
    }
    public class cls_Inventory {
    }
    public class cls_SurgeryInfo {
        public String surgeryDate;  //2016-09-30
    }
    public class cls_OrderPartners {
        public cls_SoldToPartner SoldToPartner;
        public cls_ShipToPartner ShipToPartner;
    }
    public class cls_SoldToPartner {
        public cls_SoldTopartnerID SoldTopartnerID;
    }
    public class cls_SoldTopartnerID {
        public String ns4;  //http://bsci.com/CIM/Base/v2
        public String ID;   //11899
    }
    public class cls_ShipToPartner {
        public cls_partnerID partnerID;
        public cls_partnerName partnerName;
        public cls_partnerAddress partnerAddress;
    }
    public class cls_partnerID {
        public String ns4;  //http://bsci.com/CIM/Base/v2
        public String ID;   //11899
    }
    public class cls_partnerName {
        public String name; 
        
    }
    public class cls_partnerAddress {
        public String addressLine1; 
        public String cityName; 
        public String stateCode; 
        public String postalCode; 
        public String countryCode; 
        public String attentionLine1; 
        public String attentionLine2; 
    }
    public static OrderRequest parse(String json){
        return (OrderRequest) System.JSON.deserialize(json, OrderRequest.class);
    }
    
    public class cls_text {
        public String textID;
        public String textLine;        
    }
}
public class CopyrightTerms extends AuthorizationUtil{
    
    public Shared_community_Translation__c pageText { get; set; }
    
    public CopyrightTerms() {}
    
    public override void fetchRequestedData() {      
        
        String strPageName = ApexPages.CurrentPage().getURL();
        
        if(strPageName.contains('TermsofUse')){
               
           strPageName = 'Terms of Use';
        }else{
        
           strPageName = 'Copyright';
        }

        User objUser = [Select LanguageLocaleKey 
            From User 
            Where Id =: UserInfo.getUserId()];
                
        pageText = [SELECT Shared_Page__c, Shared_Language__c, Shared_Text__c
                                                    FROM Shared_Community_Translation__c
                                                    Where Shared_Page__c =: strPageName 
                                                    AND Shared_Language__c =: objUser.LanguageLocaleKey
                                                    LIMIT 1];
    }
    
    public Shared_community_Translation__c getpageText(){
        return pageText;
    }
}
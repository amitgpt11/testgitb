/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PageLayoutCreate_Webservice {
    global PageLayoutCreate_Webservice() {

    }
    webService static String insertPageLayoutNew(List<openq__Attribute__c> listAttributes, String categoryName, String recordTypeName, List<openq__Related_List__c> relatedLists, List<openq__Related_List_Attribute__c> relatedListAttributes) {
        return null;
    }
    webService static String insertPageLayout(List<openq__Attribute__c> listAttributes, String categoryName, List<openq__Related_List__c> relatedLists, String recordTypeName, List<openq__Related_List_Attribute__c> relatedListAttributes) {
        return null;
    }
}

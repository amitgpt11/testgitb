/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/createinteraction/*')
global class RestCreateInteraction {
    global RestCreateInteraction() {

    }
    @HttpPost
    global static openq.OpenMSL.INTERACTION_CREATE_RESPONSE_element createInteraction(openq.OpenMSL.INTERACTION_CREATE_REQUEST_element request_x) {
        return null;
    }
}

/**
* Batch class, Change Object Owner Based on Territory Id 
*
* @author   Accenture
* @Description  This is scheduled class for batchUpdatePatientsTrialImplantFlag batch class
* @date     2016-09-13
*/
global class batchUpdatePatientsTIFlagSchedule implements Schedulable{
	
	/**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        Id batchId = Database.executeBatch(new batchUpdatePatientsTrialImplantFlag());
    }

}
/**
* REST dispatcher for generic SObjects
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class NMD_SObjectDispatcher implements IRestHandler {

	final static String KEY_ACTION = 'action';
	
	// mapping, ie: /nmd/products/{pricebookName}
	final static String URI_MAPPING = (String.format('/nmd/sobjects/\'{\'{0}\'}\'', new List<String> {
		KEY_ACTION
	}));

	final static String ACTION_UPDATE = 'update';
	final static String ACTION_INSERT = 'insert';

	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		// check for valid action
		String action = parameters.get(KEY_ACTION);
		if (action != ACTION_UPDATE && action != ACTION_INSERT) {
			caller.setResponse(400, 'Invalid/Missing Action.');
			return;
		}

		// check for a request body
		if (requestBody == null || requestBody.size() == 0) {
			caller.setResponse(400, 'Missing Request Body.');
			return;
		}

		List<SObject> items;

		try {
			// attempt to cast the body into a list of records
			items = (List<SObject>)JSON.deserialize(requestBody.toString(), List<SObject>.class);

			// perform requested DML
			//if (action == ACTION_INSERT) {
			//	insert items;
			//}
			//else if (action == ACTION_UPDATE) {
			//	update items;
			//}
			DML.save(this, items);

		}
		catch(System.JSONException je) {
			// return JSON error
			caller.setResponse(400, je.getMessage());
			return;
		}
		catch (System.DMLException de) {
			// return DML error
			caller.setResponse(400, de.getDmlMessage(0));
			return;
		}

		caller.setResponse(200, JSON.serialize(items));

	}

}
/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ActionPlansBatchBuilder implements Database.Batchable<SObject>, Database.Stateful {
    global void execute(Database.BatchableContext bc, List<SObject> data) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global System.Iterable start(Database.BatchableContext bc) {
        return null;
    }
}

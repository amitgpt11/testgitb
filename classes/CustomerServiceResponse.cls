global class CustomerServiceResponse {

public CustomerResponse CustomerResponse;

public class CustomerResponse {
        public CustomerPartners CustomerPartners;
    }

public class CustomerPartners {
        public list<SalesOrganizationPartners> SalesOrganizationPartners;
    }
    
public class SalesOrganizationPartners {
        public String SalesOrganization;
        public List<ShipToPartner> ShipToPartner;
    }

    public class ShipToPartner {
        public PartnerID partnerID;
        public PartnerName partnerName;
        public PartnerAddress partnerAddress;
        public String defaultPartner;
        public String legalStatus;
    }

public class PartnerID {
        public String ID;
    }

Global class PartnerAddress {
        public String addressLine1;
        public String addressLine2;
        public String cityName;
        public String stateCode;
        public String countryCode;
        public String postalCode;
    }

public class PartnerName {
        public String firstName;
    }
    

    
    public class DefaultPartner{
        public String defaultPartner;
    }
    
    public class LegalStatus{
         public String legalStatus;
    }
    
public static CustomerServiceResponse parse(String json) {
        return (CustomerServiceResponse) System.JSON.deserialize(json, CustomerServiceResponse.class);
    }
    
    Global class ShipToAddress
    {
        public String PartnerID;
        public String addressLine1;
        public String addressLine2;
        public String cityName;
        public String stateCode;
        public String countryCode;
        public String postalCode;
        public String PartnerName;
    }
}
/*****  
        Class:  DemoRequestFormExtension  (Controller for Page - DemoRequestForm)
        Created By: Accenture Team
        Created Date: 16th-June-2016
        Last Modified By: Narendra Accenture
        Last Modify Date: 29th-July-2016
        Description: This page is used to create Demo Request form and send email to quincy as pdf.
****/
public class DemoRequestFormExtension {
    public String loggedInUserName{get;set;}
      
    public String SelectedItem3{get;set;}
    public String SelectedItem5{get;set;}
    public String SelectedItem6{get;set;}
    
    public String shippingAddress{get;set;}
    public String facilityID{get;set;}
    public Shared_Request_Form__c requestForm{get;set;}
    public List<Shared_Request_Form_Line_Item__c> requestFormItemList{get;set;}
    public List<Product2> products_LEVEL4{get;set;}
    public List<Product2> products_LEVEL5{get;set;}
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Product2.getRecordTypeInfosByName();
    public static final Id RECTYPE_LEVEL3 = RECTYPES_OPPTY.get('Standard Product Hierarchy - Level 3').getRecordTypeId();
    public static final Id RECTYPE_LEVEL5 = RECTYPES_OPPTY.get('Standard Product Hierarchy - Level 5').getRecordTypeId();
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_RequestForm = Schema.SObjectType.Shared_Request_Form__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_Demo_Request = RECTYPES_RequestForm.get('Demo Request').getRecordTypeId();
    public User userRecord;
    Set<String> productLevel5Set = new Set<String>();
    List<String> productLevel5List = new List<String>();
    
    
    Set<String> productLevel3Set = new Set<String>();
    List<String> productLevel3List = new List<String>();
    
    
    public transient list<Product2> allPI3ProductsLst{get;set;}
    public list<Product2> allPI5ProductsLst{get;set;}
    public list<Product2> allPI6ProductsLst{get;set;}
    map<Id,Product2> allCPG6productMap = new map<Id,Product2>();
    public set<Id> tempPrdIds{get;set;} 
    public string message{get;set;}
    public string onBehalfUser{get;set;}
    public string errORsuccessMsg{get;set;}
    
    public map<Id,string> allCPG6StringMap{get;set;}  
    
    public DemoRequestFormExtension(){
        this.requestForm = new Shared_Request_Form__c();
        requestFormItemList = new list<Shared_Request_Form_Line_Item__c>();
        tempPrdIds = new set<Id>();
        getAllPI3Products();
        message = '';
        
        if(allPI3ProductsLst != null && allPI3ProductsLst.size() > 0){
            for(Product2 prd : allPI3ProductsLst){
                if(prd.RecordTypeId == RECTYPE_LEVEL3 && prd.Shared_Custom_Product_Group_Level_3__c != ''){
                    productLevel3Set.add(prd.Shared_Custom_Product_Group_Level_3__c);
                }
            }
            
            productLevel3List.addAll(productLevel3Set); 
            productLevel3List.sort();
        }     
        
        
        loggedInUserName = UserInfo.getName();
        userRecord = [Select ID,Name,Cost_Center_Code__C from User Where Id =:UserInfo.getUserId() limit 1];
        loggedInUserName = UserInfo.getName();
        requestForm.Cost_Center__c = userRecord.Cost_Center_Code__C;
    }
   
    
    public pagereference save () {
        return null;
    }
    
    
    public PageReference sendPdf(){
        System.debug('Request Form : '+this.requestForm);
        System.debug('Facility ID '+facilityID);
        System.debug('requestFormItemList '+requestFormItemList);
        System.debug('requestForm.Onbehalf_Of__c '+onBehalfUser);
        
        if(requestFormItemList.size() > 0){
            requestForm = this.requestForm;
            requestForm.Facility__C = facilityID;
            requestForm.RecordTypeId = RECTYPE_Demo_Request;
            requestForm.Requesters_Name__c = UserInfo.getUserId();
            requestForm.Onbehalf_Of__c = onBehalfUser;

            try{
    
            insert this.requestForm;
            }catch(Exception ex){
                    errORsuccessMsg = 'Something went wrong, please contact your administartor.';
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,ex.getMessage());
                    ApexPages.addMessage(myMsg);
                    System.debug('Error '+ex.getMessage());
                    return null;
            }
        
         if(this.requestForm.Id != null){

            for(Shared_Request_Form_Line_Item__c r : requestFormItemList){            
                r.Request_Form__c = this.requestForm.Id;            
            }     
        
            try{   
            System.debug('<<<<<<Inserting requestFormItemList :'+requestFormItemList);
            
            List<Shared_Request_Form_Line_Item__c> requestFormItemFilteredList = new List<Shared_Request_Form_Line_Item__c> ();
                
                if(requestFormItemList.size() > 0){
                   // insert requestFormItemList;
                   for(Shared_Request_Form_Line_Item__c reqFrmLineItem : requestFormItemList ){
                        if(reqFrmLineItem.Quantity__c > 0){
                            requestFormItemFilteredList.add(reqFrmLineItem);
                        }//End of If
                    }//End of Loop
    
                insert requestFormItemFilteredList;
                   
                }//End of If
                }catch(Exception e){
                    errORsuccessMsg = 'Something went wrong, please contact your administartor.';
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
                        ApexPages.addMessage(myMsg);
                        system.debug('Exception while inserting line items-'+e);
                        return null;
                }         
           
            Shared_Request_Form__c reqForm = [Select Id,Name,Requesters_Name__c,Requesters_Name__r.Name from Shared_Request_Form__c  Where Id =:this.requestForm.Id limit 1];
       
             PageReference pdf = Page.callPdfPage;
            // add parent id to the parameters for standardcontroller
            system.debug('<<<>>>' + pdf);
            pdf.getParameters().put('id',this.requestForm.Id);
            system.debug('<<<>>>>>>>' + pdf);       
      
            System.debug('PDF url : '+pdf);
            // the contents of the attachment from the pdf
            Blob body;
            pdf.setRedirect(true);
             return pdf;  
         }
         if(errORsuccessMsg == ''){
               return new PageReference('/'+this.requestForm.Id);  
            }else{
                
                return null;
            }   
        
        //return new PageReference('/'+this.requestForm.id);
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,'Add at least one prodcut before submitting.');
            ApexPages.addMessage(myMsg);
            //errORsuccessMsg = 'Add at least one prodcut before submitting.';
            system.debug('errORsuccessMsgs-'+errORsuccessMsg);
            return null;
        }
    }
 
    public PageReference ValidateValues(){
            facilityID = requestForm.Facility__c;
            List<Account> acclist = [SELECT Id,Name,Account_Number__c,ShippingCity,ShippingPostalCode,ShippingState,ShippingStreet,Sold_to_Attn__c,Sold_to_City__c,Sold_to_Country__c,Sold_to_Fax__c,Sold_to_Phone__c,Sold_to_State__c,Sold_to_Street__c,Sold_to_Zip_Postal_Code__c FROM Account where id =:requestForm.Facility__c ];
            //List<Account> acclist = [Select id,Name,ShippingAddress,ShippingCity,ShippingPostalCode,ShippingState,ShippingStreet  from Account where id =:requestForm.Facility__c ];
           if(acclist.Size() > 0  ){
                requestForm.Shipping_Address__c = (acclist[0].ShippingStreet == null ? acclist[0].Sold_to_Street__c: acclist[0].ShippingStreet);
                requestForm.City__c = (acclist[0].ShippingCity == null ? acclist[0].Sold_to_City__c: acclist[0].ShippingCity);
                requestForm.State__c = (acclist[0].ShippingState == null ? acclist[0].Sold_to_State__c : acclist[0].ShippingState);
                requestForm.Zip_Code__c = (acclist[0].ShippingPostalCode == null ? acclist[0].Sold_to_Zip_Postal_Code__c : acclist[0].ShippingPostalCode);
                requestForm.SAP_Account_Number__c = acclist[0].Account_Number__c;
           }
       return null;
    }
    
    /*
    public PageReference changeProductLEVEL4(){
        
        products_LEVEL5 = [SELECT EAN_UPN__c,Id,Name,RecordTypeId, UPN_Material_Number__c, Description FROM Product2 WHERE Shared_Custom_Product_Group_Level_5__c  =:SelectedItem ];
        requestFormItemList.clear();
        for(Product2 prt : products_LEVEL5){
           Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
            rItem.Full_UPN__c = prt.UPN_Material_Number__c;
            rItem.Description__c = prt.Description;
            rItem.Quantity__c = 0;
            rItem.Unit_Of__c = 'EA';
           requestFormItemList.add(rItem); 
        }
        return null;
    } 
 
    public List<SelectOption> getLEVEL4Products(){
        List<SelectOption> options = new List<SelectOption>();
        
        for(String prd : productLevel5List){
            options.add(new SelectOption(prd, prd));
        }
        return options;
    }//End of Method    
    
    */
    
   public PageReference changeCostCenter(){
    system.debug('requestForm.Onbehalf_Of__c-->'+requestForm.Onbehalf_Of__c);
    if(requestForm.Onbehalf_Of__c!=null){
        onBehalfUser = requestForm.Onbehalf_Of__c;
        list<User> usrlst = new list<User>([Select ID,Name,Cost_Center_Code__C from User Where Id =:requestForm.Onbehalf_Of__c limit 1]);
        if(usrlst != null && usrlst.size() >0){
        requestForm.Cost_Center__c = usrlst[0].Cost_Center_Code__C;   
        system.debug('@@@--'+usrlst);
        system.debug('requestForm.Cost_Center__c-->'+requestForm.Cost_Center__c);     
        }
    } 
    return null;
    }
    
    
    
    //Added during Q4 2016
    public void getAllPI3Products(){
        allPI3ProductsLst = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_3__c,Shared_Custom_Product_Group_Level_6__c,Description  
                                FROM Product2 
                                WHERE Division__c='PI' AND RecordTypeId =:RECTYPE_LEVEL3 AND Shared_Custom_Product_Group_Level_3__c != '' AND Shared_Geography__c ='US'];
        system.debug(allPI3ProductsLst+'@@--this.allPI3ProductsLst');
    }
    
     public pagereference getAllPI5Products(){
        system.debug('selectedItem3-->'+selectedItem3);
        
        if(string.isNotEmpty(selectedItem3)){
            allPI5ProductsLst = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_3__c,Shared_Custom_Product_Group_Level_5__c,Description  
                                                FROM Product2 
                                                WHERE Division__c='PI' AND RecordTypeId =:RECTYPE_LEVEL5 AND Shared_Custom_Product_Group_Level_3__c =: SelectedItem3 AND Shared_Custom_Product_Group_Level_5__c != NULL AND Shared_Geography__c ='US'];
            system.debug(allPI5ProductsLst+'@@--this.allPI5ProductsLst');
            
        }
    return null;
    }
    
    public pagereference getAllCPG6Products(){
        system.debug('selectedItem5-->'+selectedItem5);
        allCPG6StringMap = new map<Id,string>();
        if(string.isNotEmpty(selectedItem5)){
            allPI6ProductsLst = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_3__c,Shared_Custom_Product_Group_Level_6__c,Description  
                                                            FROM Product2 
                                                            WHERE Division__c='PI'  AND Shared_Custom_Product_Group_Level_6__c =: SelectedItem5  AND Shared_Geography__c ='US' order by Name];
            system.debug(allPI6ProductsLst+'@@--this.allPI6ProductsLst');
            for(Product2 p : allPI6ProductsLst){
                allCPG6productMap.put(p.Id,p);
                allCPG6StringMap.put(p.Id,p.UPN_Material_Number__c+'-'+p.Description);
            }
            
        }
        
    return null;
    }
    public void selectedItem6(){
        system.debug(selectedItem6+'-->selectedItem6');
    }
    public PageReference addProductToLst(){
        //RequestFrmUtemLst.clear();
        system.debug(selectedItem6+'-->selectedItem6');        
        
       // system.debug(tempPrdIds.contains(selectedItem6)+'-->tempPrdIds');
         message = '';
         if(string.isNotEmpty(selectedItem6)){
             if(tempPrdIds.contains(selectedItem6) == false){
                message = '';
                tempPrdIds.add(selectedItem6);
                system.debug(tempPrdIds+'-->tempPrdIds');
                Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
                rItem.Full_UPN__c = allCPG6productMap.get(selectedItem6).UPN_Material_Number__c;
                rItem.Description__c = allCPG6productMap.get(selectedItem6).Description;
                rItem.Quantity__c = 1;
                rItem.Unit_Of__c = 'EA';
                requestFormItemList.add(rItem);            
             }
             else{
                message = 'This Product has already been added to the list!!';
             }
         }
         else{
            message = 'Select at least one UPN to add to the list';             
         }
         system.debug(message+'-->message');  
        return null;
    } 
    
     public List<SelectOption> getLEVEL3Products(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for(String prd : productLevel3List){
            options.add(new SelectOption(prd, prd));
        }
        system.debug('getLEVEL3Products--'+options);
        return options;
    }//End of Method
    
    public List<SelectOption> getLEVEL5Products(){
        Set<String> productLevel5Set = new Set<String>();
        list<String> productLevel5List = new list<String>();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        
        if(allPI5ProductsLst != null && allPI5ProductsLst.size() > 0){
            for(Product2 prd : allPI5ProductsLst){                  
                    productLevel5Set.add(prd.Shared_Custom_Product_Group_Level_5__c);                   
            }
            
            productLevel5List.addAll(productLevel5Set); 
            productLevel5List.sort();
            for(String prd : productLevel5List){
             options.add(new SelectOption(prd, prd));
            }
        }  
        system.debug('getLEVEL5Products--'+options);
        return options;
    }//End of Method
    
    public List<SelectOption> getCPG6Products(){
        Set<String> productLevel6Set = new Set<String>();
        list<String> productLevel6List = new list<String>();
        List<SelectOption> options = new List<SelectOption>();
        map<Id,string> prdIdNameMap = new map<Id,string>();
        options.add(new SelectOption('', '--None--'));
        if(allPI6ProductsLst != null && allPI6ProductsLst.size() > 0){
            for(Product2 prd : allPI6ProductsLst){   
                    prdIdNameMap.put(prd.Id,prd.UPN_Material_Number__c+'-'+prd.Description);              
                    //productLevel6Set.add(prd.Name+'-'+prd.Description);                   
            }           
            //productLevel6List.addAll(prdIdNameMap.values()); 
            //productLevel6List.sort();
            for(Id prd : prdIdNameMap.keyset()){
                options.add(new SelectOption(prd,prdIdNameMap.get(prd)));
            }
        }  
        system.debug('getCPG6Products--'+options);
        
        return options;
    }//End of Method
}
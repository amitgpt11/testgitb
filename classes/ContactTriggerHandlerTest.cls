/**
* Test Class for the TriggerHandler methods for Contact triggers.       
*
* @Author salesforce Services
* @Date 04/20/2015
*/
@isTest(SeeAllData=false)
private class ContactTriggerHandlerTest {

    static final String ACCTNAME = 'AcctName';
    
    static testMethod void test_ContactTriggerHandlerTest() {
        System.Test.startTest();
        NMD_TestDataManager td = new NMD_TestDataManager();

        //create seller hierarchy
        Seller_Hierarchy__c sellerH = td.newSellerHierarchy('TestSH');
        insert sellerH;             
        //physician account
        Account phyAcct = td.createConsignmentAccount();
        phyAcct.Name = 'PHYSICIANTESTER';
        insert phyAcct;
        //physician contact
        Contact phyContact = td.newContact(phyAcct.Id);
        phyContact.Email = 'this@goes.nowhere';
        phyContact.Territory_ID__c=sellerH.Id;
        insert phyContact;  
        //paritent account          
        Account acct = td.createConsignmentAccount();
        acct.Name = 'PATIENTTEST';
        insert acct;        
        //owner contact
        //update physician contact      
        phyContact.AccountId = phyAcct.Id;
        phyContact.OwnerId = UserInfo.getUserId();      
        update phyContact;      
        //create patient                
        Patient__c patient = td.newPatient('TestL', 'TestF');
        insert patient;
        //update patient        
        //patient.Physician_of_Record__c = phyContact.Id;
        patient.OwnerId = UserInfo.getUserId();     
        update patient;
        
        //test null
        //phyContact.Territory_ID__c=null;
        //update phyContact;        
        //test not null
        phyContact.Territory_ID__c=sellerH.Id;
        update phyContact;              
        //test not null to null 
        phyContact.Territory_ID__c=null;
        update phyContact;              
        
        //assert
        //System.assertEquals();s       
        System.Test.stopTest();
    }

    static testMethod void test_cascadeSAPIDInfo(){
        NMD_TestDataManager td = new NMD_TestDataManager();

        Account acct = td.createConsignmentAccount();
        acct.Name = ACCTNAME;
        insert acct;

        Contact con0 = td.newContact(acct.Id);
        con0.SAP_ID__c = '00000';
        insert con0;

        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.Trialing_Physician__c = con0.id;
        insert oppty;

        Test.startTest();

            con0.SAP_ID__c = '11111';
            update con0;

        Test.stopTest();

    }
    
}
public without sharing class UpdatePatientsTrialImplantFlag {
        public List<Opportunity> optyFinalList = new List<Opportunity>(); 
        public List<Patient__c> plist= new List<Patient__c>();    
        public set<Id> pIdList= new Set<Id>();    
        public List<Id> optyIdList = new List<Id>();
		public List<String> types = new List<String>();
		public map<Id,List<String>> PatientIdsbyType = new map<Id,List<String>>();
		public List<Patient__c> plistfinal= new List<Patient__c>(); 
    	public map<id,id> patientidsbyopptyid= new map<id,Id>();
  
    
    public void PatientInfo(List<Opportunity> optyyList)
    {
        system.debug('In Method PatientInfo');
            
            for(Opportunity o:optyyList ){
                optyIdList.add(o.Id);
            }   
        List<Opportunity> optyList = [SELECT RecordTypeId,Actual_Trial_Date__c,Actual_Procedure_Date__c,Patient__r.Implant_Yes__c,Patient__r.Trial_Yes__c,Patient__c FROM Opportunity where Id In:optyIdList  ];                  

                                  
        //**updated by amitabh to loop on parameter passed by batch instead of above list optyList
        for(Opportunity op : optyyList){
            system.debug('In Method PatientInfo'+op.RecordTypeId+'A Trial Date'+op.Actual_Trial_Date__c+'trial yes'+op.Patient__r.Trial_Yes__c);
            if(op.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW && op.Actual_Trial_Date__c <= date.today() && op.Patient__r.Trial_Yes__c == false )
                {
                    system.debug('Match the trial optty criterion');
                    //op.Patient__r.Trial_Yes__c = true;  
                    optyFinalList.add(op);
                    pIdList.add(op.Patient__c);
                    patientidsbyopptyid.put(op.id,op.Patient__c);
					if(PatientIdsbyType.ContainsKey(op.Patient__c)){
						List<String> tempTypes=PatientIdsbyType.get(op.Patient__c);
						tempTypes.add('Trial');	
                        PatientIdsbyType.put(op.Patient__c,tempTypes);
					}else{
						//types.add('Trial');	
                        PatientIdsbyType.put(op.Patient__c,new List<String>{'Trial'});
					}	
                }
                 else if(op.RecordTypeId == OpportunityManager.RECTYPE_IMPLANT && op.Actual_Procedure_Date__c <= date.today() && op.Patient__r.Implant_Yes__c == false ) 
                {
                 //op.Patient__r.Implant_Yes__c = true;
                 optyFinalList.add(op);   
                 pIdList.add(op.Patient__c);
				 if(PatientIdsbyType.ContainsKey(op.Patient__c)){
						List<String> tempTypes1=PatientIdsbyType.get(op.Patient__c);
						tempTypes1.add('Implant');
                     	PatientIdsbyType.put(op.Patient__c,tempTypes1);
					}else{
						types.add('Implant');
						PatientIdsbyType.put(op.Patient__c,new List<String>{'Implant'});
					}
                 //break;
                }
          }
            if(!pIdList.isEmpty()){
                system.debug('pIdList'+pIdList);
                plist =[Select Id,Trial_Yes__c,Implant_Yes__c From Patient__c Where Id In:PatientIdsbyType.keyset() ];
				for(Patient__c p:Plist){
					if(PatientIdsbyType.ContainsKey(p.Id)){
						List<String> tempTypes=PatientIdsbyType.get(p.Id);
						for(String type:tempTypes){
							if(type=='Trial'){
								p.Trial_Yes__c =True;
							}
							if(type=='Implant'){
								p.Implant_Yes__c =True;
							}
							
						}
						plistfinal.add(p);
					}
					/*if(PatientIdsbyType.get(p.id)== 'Trial'){
						p.Trial_Yes__c =True;
						plistfinal.add(p);
					}
                    if(PatientIdsbyType.get(p.id)== 'Implant'){
						p.Implant_Yes__c =True;
						plistfinal.add(p);
					}*/
                    
				}
            }
            if(!plistfinal.isEmpty()) {
                //update optyFinalList;
                update plistfinal;   
            }
    }
}
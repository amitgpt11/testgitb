@isTest(SeeAllData=False)
public class Shared_ChatterUploadControllerTest {
    static testMethod void test_Shared_ChatterUploadController(){
        
        Test.startTest();
        List<PrivateAttachmentShared_ProfileData__c> pList = new List<PrivateAttachmentShared_ProfileData__c>();
        
        PrivateAttachmentShared_ProfileData__c profile = new PrivateAttachmentShared_ProfileData__c();
        profile.Name = 'IC';
        profile.Profile_Names__c = 'US IC User';
        pList.add(profile);
        
        PrivateAttachmentShared_ProfileData__c profile1 = new PrivateAttachmentShared_ProfileData__c();
        profile1.Name = 'PI';
        profile1.Profile_Names__c = 'US PI User';
        pList.add(profile1);
        
        Insert pList;
        
        Account acc = new Account(Name ='Test Name',Classification__c='Other');
        insert acc;
        
       /* FeedItem fd = new FeedItem();
        fd.Title = 'TestName';
        fd.Body = 'TestBody';
        fd.ParentId =acc.id;
        insert fd;
        */

        PageReference pg = Page.Shared_ChatterUpload;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('Id',acc.Id);
        
        Account acc1 = new Account();
        ApexPages.StandardController st = new ApexPages.standardController(acc1);
        Shared_ChatterUploadController e = new Shared_ChatterUploadController(st);
        e.onLoad();
        //e.insertinCustom();
        //e.insertinCustom1();
      
        Test.stopTest();
        
    }
}
/**
* Base class for common extension methods
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public virtual class NMD_ExtensionBase {
	
	protected void logError(String classNameDotMethodName, String errorMessage, String objId, String objName) {
		List<String> parts = classNameDotMethodName.split('.');
		Logger.error(
            parts.size() > 0 ? parts[0] : '<>',
            parts.size() > 1 ? parts[1] : '<>', 
            objId,
            objName,
            errorMessage, 
            '', 
            null, 
            0
        );
	}

	protected void newErrorMessage(String message) {
		newMessage(ApexPages.Severity.ERROR, message);
	}

	protected void newInfoMessage(String message) {
		newMessage(ApexPages.Severity.INFO, message);
	}

	protected void newWarningMessage(String message) {
		newMessage(ApexPages.Severity.WARNING, message);
	}

	private void newMessage(ApexPages.Severity severity, String message) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
	}

	@RemoteAction
    public static String fetchAttachment(Id attachmentId) {

        try {
            Attachment attach = [
                select Body
                from Attachment
                where Id = :attachmentId
            ];

            return EncodingUtil.base64Encode(attach.Body);
        }
        catch (System.QueryException qe) {
            return '';
        }

    }

	@RemoteAction
	public static String fetchFeedItem(Id feedItemId) {

		try {
			FeedItem item = [
				select ContentData
				from FeedItem
				where Id = :feedItemId
			];

			return EncodingUtil.base64Encode(item.ContentData);
		}
		catch (System.QueryException qe) {
			return '';
		}

	}

}
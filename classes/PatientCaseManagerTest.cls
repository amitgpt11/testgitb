/*
@CreatedDate     29JUNE2016
@author          Mike Lankfer
@Description     Contains test coverage for PatientCaseManager class
@Requirement Id  User Story : DRAFT US2484
*/

@isTest
public class PatientCaseManagerTest {
    
    private static Account primaryAccount = new Account();
    private static String primaryAccountNumber = '123456';
    private static Shared_Patient_Case__c primaryPatientCase = new Shared_Patient_Case__c();
    private static Shared_Patient_Case__c updatedPrimaryPatientCase = new Shared_Patient_Case__c();
    private static Shared_Patient_Case__c fosterParentPatientCase = new Shared_Patient_Case__c();
    private static Datetime fosterParentStartDateTime = Datetime.newInstance(2000, 01, 01, 00, 00, 00);
    private static Datetime primaryPatientCaseStartDateTime = Datetime.now();
    private static LAAC_Implant__c primaryImplant = new LAAC_Implant__c();
    private static LAAC_Implant__c updatedPrimaryImplant = new LAAC_Implant__c();
    private static String successStatus = 'Closed - Success';
    private static String failureStatus = 'Closed - Failure';
    private static String abortReason = 'Aborted';    
    
    //insert an aborted implant and verify the parent patient case was updated to closed = failure with a failure reason of aborted
    static testMethod void updateSuccessfulCaseFromImplant(){
        
        Test.startTest();
        
        //create test account
        primaryAccount = TestDataFactory.createTestAccount();        
        primaryAccount.Account_Number__c = primaryAccountNumber;
        insert primaryAccount;
        
        
        //create test patient cases
        primaryPatientCase = TestDataFactory.createTestPatientCase();        
        primaryPatientCase.Shared_Start_Date_Time__c = primaryPatientCaseStartDateTime;
        primaryPatientCase.Shared_End_Date_Time__c = primaryPatientCaseStartDateTime.addHours(1);
        primaryPatientCase.Shared_Facility__c = primaryAccount.Id;
        fosterParentPatientCase.Shared_Start_Date_Time__c = fosterParentStartDateTime;
        fosterParentPatientCase.Shared_End_Date_Time__c = fosterParentStartDateTime.addHours(1);
        fosterParentPatientCase.Shared_Facility__c = primaryAccount.Id;
        insert primaryPatientCase;
        insert fosterParentPatientCase;
        
        
        //create test implant records
        primaryImplant = TestDataFactory.createTestImplant();        
        primaryImplant.LAAC_Account_Number__c = primaryAccountNumber;
        primaryImplant.LAAC_Date_of_Procedure__c = primaryPatientCaseStartDateTime.date();
		primaryImplant.LAAC_Case_Aborted__c = false;        
        insert primaryImplant;       
        
        //query updated parent patient case
        updatedPrimaryPatientCase = [SELECT Id, Shared_Status__c, Shared_Failure_Reason__c FROM Shared_Patient_Case__c WHERE Id = :primaryPatientCase.Id];
        
        Test.stopTest();
        
        System.assertEquals(successStatus, updatedPrimaryPatientCase.Shared_Status__c);
        
    }
    
    
    //insert an aborted implant and verify the parent patient case was updated to closed = failure with a failure reason of aborted
    static testMethod void updateAbortedCaseFromImplant(){
        
        Test.startTest();
        
        //create test account
        primaryAccount = TestDataFactory.createTestAccount();        
        primaryAccount.Account_Number__c = primaryAccountNumber;
        insert primaryAccount;
        
        
        //create test patient cases
        primaryPatientCase = TestDataFactory.createTestPatientCase();        
        primaryPatientCase.Shared_Start_Date_Time__c = primaryPatientCaseStartDateTime;
        primaryPatientCase.Shared_End_Date_Time__c = primaryPatientCaseStartDateTime.addHours(1);
        primaryPatientCase.Shared_Facility__c = primaryAccount.Id;
        fosterParentPatientCase.Shared_Start_Date_Time__c = fosterParentStartDateTime;
        fosterParentPatientCase.Shared_End_Date_Time__c = fosterParentStartDateTime.addHours(1);
        fosterParentPatientCase.Shared_Facility__c = primaryAccount.Id;
        insert primaryPatientCase;
        insert fosterParentPatientCase;
        
        
        //create test implant records
        primaryImplant = TestDataFactory.createTestImplant();        
        primaryImplant.LAAC_Account_Number__c = primaryAccountNumber;
        primaryImplant.LAAC_Date_of_Procedure__c = primaryPatientCaseStartDateTime.date();
		primaryImplant.LAAC_Case_Aborted__c = true;        
        insert primaryImplant;       
        
        //query updated parent patient case
        updatedPrimaryPatientCase = [SELECT Id, Shared_Status__c, Shared_Failure_Reason__c FROM Shared_Patient_Case__c WHERE Id = :primaryPatientCase.Id];
        
        Test.stopTest();
        
        System.assertEquals(failureStatus, updatedPrimaryPatientCase.Shared_Status__c);
        System.assertEquals(abortReason, updatedPrimaryPatientCase.Shared_Failure_Reason__c);
        
    }
}
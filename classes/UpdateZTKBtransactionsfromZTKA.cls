global class UpdateZTKBtransactionsfromZTKA implements Database.Batchable<sObject>
{
    public static final String ZTKA = 'ZTKA';
    public static final String ZTKB = 'ZTKB';
    public static final String EAI_USER = 'EAI User';
    
    public String query = 'SELECT Id,Order_Type__c,Order__c,Model_Number__c ,Lot_Number__c ,Serial_Number__c ,Order_Item__r.Order_Item_Id__c,ZTKA_No__c FROM Inventory_Transaction__c WHERE Order_Type__c =: '+ ZTKB +' AND Order__c = null and ZTKA_No__c!=null';
   // public String query = 'select Order__c,Order_Item__c,Order_Type__c,Inventory_Item__c,Lot_Number__c,Model_Number__c,Sales_Order_No__c,Serial_Number__c,ZTKA_No__c,Order_Item__r.Order_Item_Id__c from Inventory_Transaction__c where Order_Type__c =: ZTKA AND Order__c != null AND CreatedBy.Name =: EAI_USER';
    
    public Map<String,Inventory_Transaction__c> transZTKAs = new Map<String,Inventory_Transaction__c>();
    public Map<String,Inventory_Transaction__c> transZTKAs1 = new Map<String,Inventory_Transaction__c>();
    
   // public String ztkaOrderItemId='';
   // public String ='';
    
   // public final string PULLONLYZTKAINDICATOR = 'PullOnlylastMonthZTKAs';
        
    //Public Map<Id,Inventory_Transaction__c> UpdateZTKBTransactionMap = new Map<Id,Inventory_Transaction__c>();
    Public List<Inventory_Transaction__c> UpdateZTKBTransactionList = new List<Inventory_Transaction__c>();
    Public Set<String> uniqueSalesOrderNumbers = new Set<String>();
    Public List<Inventory_Transaction__c> ZTKATransactionsForZTKB = new List<Inventory_Transaction__c>();
    Public List<Inventory_Transaction__c> UpdateZTKBTransactionsZTKB = new List<Inventory_Transaction__c>();
    
    Integer queryLimit=0;
    
    // Constructor
    global UpdateZTKBtransactionsfromZTKA()
    {}
    global UpdateZTKBtransactionsfromZTKA(Integer queryLim)  
    {
        this.queryLimit=queryLim;
    }      
    
    /****************************************************************
*  start(Database.BatchableContext BC)
*****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        system.debug('Inside Start method');
              
        if(Test.isRunningTest()){
            queryLimit=10;
            //this.query= this.query+' Limit 10';
        }
        if(queryLimit>0){
            this.query= this.query+' Limit '+queryLimit;
        }
             
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
*  execute(Database.BatchableContext BC, List scope)
**********************************************************************/
    global void execute(Database.BatchableContext BC, List<Inventory_Transaction__c> scope)
    { 
    system.debug('***Scope Query**'+scope);               
    //Collecting Associated ZTKAs
        if(!scope.isEmpty()){
            for (Inventory_Transaction__c trans : scope){
                if(trans.ZTKA_No__c!=null)
                    uniqueSalesOrderNumbers.add(trans.ZTKA_No__c);
                else{
                    system.debug('****!!NO ZTKA Number for ZTKB!!!**** :'+trans.id);
                }
            }
        }
    
    //pulling Associated ZTKAs
        
    ZTKATransactionsForZTKB= [select Order__c,Order_Item__c,Order_Type__c,Inventory_Item__c,Lot_Number__c,Model_Number__c,Sales_Order_No__c,Serial_Number__c,ZTKA_No__c,Order_Item__r.Order_Item_Id__c from Inventory_Transaction__c where Order_Type__c =: ZTKA AND CreatedBy.Name =: EAI_USER and Order__c!=null and Sales_Order_No__c in :uniqueSalesOrderNumbers];
    
      //creating ZTKA's keys
        if(!ZTKATransactionsForZTKB.isEmpty()){
            for (Inventory_Transaction__c trans : ZTKATransactionsForZTKB){
              
               if (trans.Order_Type__c == ZTKA) {
                    if(trans.Sales_Order_No__c != null){
                        this.transZTKAs.put(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c  + trans.Sales_Order_No__c, trans);
                    }
                    else{
                        this.transZTKAs1.put(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c  , trans);
                    }
                }
              //  system.debug('trans***'+trans);
              //  system.debug('Key ZTKA***'+trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + ztkaOrderItemId + trans.Sales_Order_No__c);
            }
        }
        
        
        for(String a:transZTKAs.keySet()){
            system.debug('Key ZTKA***'+a);
        }
        for(String a:transZTKAs1.keySet()){
            system.debug('Key ZTKA1***'+a);
        }
        
        //system.debug('transZTKAs***'+transZTKAs);
        //system.debug('transZTKAs1***'+transZTKAs1);
        
        if(!transZTKAs.isEmpty() || !transZTKAs1.isEmpty()){
                 
            if(scope != null){
                for(Inventory_Transaction__c ztkb1: scope){
                    system.debug('***Looking at ZTKB ID: '+ztkb1.id);
                    
                    //if(ztkb1.Order_Item__r.Order_Item_Id__c!=null)
                    //= String.ValueOf(Integer.valueOf(ztkb1.Order_Item__r.Order_Item_Id__c));
                    
                    if (ztkb1.Order_Type__c == ZTKB) {
                        if(ztkb1.ZTKA_No__c != null){
                        system.debug('**Key ZTKB **'+(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c + ztkb1.ZTKA_No__c));   
                        system.debug('**Match found? **'+transZTKAs.containsKey(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c + ztkb1.ZTKA_No__c));
                       
                            if(transZTKAs.containsKey(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c + ztkb1.ZTKA_No__c)){          
                                Inventory_Transaction__c temp = transZTKAs.get(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c  + ztkb1.ZTKA_No__c);
                                
                                if(ztkb1.Order__c == null && temp.Order__C!=null){
                                    system.debug('****Updateing ZTKB:'+ztkb1.id+' with Order :'+temp.Order__c );
                                    ztkb1.Order__c = temp.Order__c;
                                    ztkb1.Is_Recursive_Approval__c = true;
                                    //UpdateZTKBTransactionMap.put(ztkb1.id, ztkb1);
                                    UpdateZTKBTransactionList.add(ztkb1);
                                }else{
                                    system.debug('****NOT ABLE TO UPDATE ZTKB:'+ztkb1.id+' with Order :'+temp.Order__c );
                                }
                            }
                        }
                        else{
                        system.debug('**else Key ZTKB ****'+(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c ));
                        system.debug('**else Match Found? **'+transZTKAs1.containsKey(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c ));
                        if(transZTKAs1.containsKey(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c  )){
                                Inventory_Transaction__c temp = transZTKAs1.get(ztkb1.Model_Number__c + ztkb1.Lot_Number__c + ztkb1.Serial_Number__c );
                                
                                if(ztkb1.Order__c == null && temp.Order__C!=null){
                                    system.debug('****Updateing ZTKB:'+ztkb1.id+' with Order :'+temp.Order__c );
                                    ztkb1.Order__c = temp.Order__c;
                                    ztkb1.Is_Recursive_Approval__c = true;
                                    //UpdateZTKBTransactionMap.put(ztkb1.id, ztkb1);
                                    UpdateZTKBTransactionList.add(ztkb1);
                                }else{
                                    system.debug('****NOT ABLE TO UPDATE ZTKB:'+ztkb1.id+' with Order :'+temp.Order__c );
                                }
                            }
                        }
                    }
                }
            }            
        }
        
        system.debug('terrList'+UpdateZTKBTransactionList);
        if(!UpdateZTKBTransactionList.isEmpty()){
            DML.evaluateResults(this, Database.update(UpdateZTKBTransactionList, false));
            UpdateZTKBTransactionList.clear();
        }
    }
    
    /****************************************************
*  finish(Database.BatchableContext BC)
*****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Inside Finish method');
    }
    
    
}
/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_BsciSynergyRedirectExtensionTest {
	
	static final String BSCISYNERGY = 'bscisynergy';
	static final String URLPATH = 'foo';

	@TestSetup
	static void setup() {

		insert new NMD_Inventory_Settings__c(
			SetupOwnerId = UserInfo.getOrganizationId(),
			URI_Scheme__c = BSCISYNERGY
		);

	}

	static testMethod void test_BsciSynergyRedirect() {

		PageReference pr = Page.BsciSynergyRedirect;
		pr.getParameters().put(SynergyPageReference.PATH_PARAM, URLPATH);
		Test.setCurrentPage(pr);

		NMD_BsciSynergyRedirectExtension ext = new NMD_BsciSynergyRedirectExtension();
		String redirectUrl = ext.redirectUrl;
		System.assert(redirectUrl.startsWith(BSCISYNERGY));
		System.assert(redirectUrl.contains(URLPATH));

	}

}
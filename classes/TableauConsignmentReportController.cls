/*  
    CreatedDate     6Jul2016                                  
    ModifiedDate    6Jul2016                                  
    author          Julianne Diamond
    Description     This class is used to fetch a tableau link for logged in user and display the link on Account detail page section "Consignment Report"
    Methods         TableauConsignmentReportController(),getTableauLinkInfo()
    Requirement Id  Q3 2016-US2622
    Modified On : 26AUG2016
    Modified By : Mayuri 
    Modification comments : Added two new methods myCustomFieldMapper() and getSignedIdentity() to support tableau reports in SF1    
 */ 
public with sharing class TableauConsignmentReportController {  
    
     public Id profileId;   
     public string SAPAccountId{get;set;}   
     public string profileName{get;set;}    
     public string link{get;set;}   
     public string username{get;set;}   
     map<string,string> profileLinkMap = new map<string,string>();  
     map<string,string> profileUserMap = new map<string,string>();      
     list<string> excludedProfileNames = new list<string>();    
     map<Id,Profile> profileMap{get;set;}   
     map<string,TableauConsignmentReport__c> reportValues{get;set;} 
     public Account AC; 
     public boolean isError{get;set;}       
        
        
     public TableauConsignmentReportController(ApexPages.StandardController controller) {   
         profileId = UserInfo.getProfileId();   
         this.AC = (Account)controller.getRecord(); 
         getTableauLinkInfo();  
            
     }  
        
    /*  
        MethodName    getTableauLinkInfo
        Return Type   void
        Description   Method to extract tableau link from cutom setting 'TableauConsignmentReport__c' for the logged in user
    */  
     public void getTableauLinkInfo(){  
         SAPAccountId = [SELECT Id,name,Account_SAP_ID__c FROM Account WHERE Id =: AC.Id].Account_SAP_ID__c; //query sap account number of current account  
         System.debug('SAP Account ID: ' + SAPAccountId);   
         profileName = [SELECT Id,Name FROM Profile WHERE Id =:profileId].Name;             
         reportValues = TableauConsignmentReport__c.getAll();   
         for(string st : reportValues.keyset()){    
             string link = reportValues.get(st).Tableau_Link__c;    
             list<string> pfNames = reportValues.get(st).Profile_Names__c.split(',');   
             if(pfNames.size()>0){  
                 for(string pf : pfNames){  
                     profileLinkMap.put(pf,link);   
                     profileUserMap.put(pf,st); 
                 }  
             }              
         }  
            
         if(profileLinkMap != null && profileLinkMap.size() > 0){   
             if(profileName != null && profileLinkMap.keyset().contains(profileName)){  
                 if(profileLinkMap.get(profileName) != null){   
                     if(SAPAccountId!=null){    
                         link = profileLinkMap.get(profileName) + SAPAccountId; 
                         isError = false;   
                     }  
                     else{  
                         isError = true;    
                     }  
                 }  
             }  
         }          
         if(profileUserMap!= null && profileUserMap.size() > 0){    
             if(profileName != null && profileUserMap.keyset().contains(profileName)){  
                 if(profileUserMap.get(profileName) != null){   
                     username = profileUserMap.get(profileName); //get logged in user profile name to display it in VF page     
                 }  
             }  
         }  
            
     }  
      private String myCustomFieldMapper() {
        // This customer written class does the "work" need to map and get a Tableau username
        // In our example we use a static string "sfUser" as the string to map.
        //public StringMyUserId = UserInfo.getUserId();
        List<User> u = [Select FederationIdentifier from user where id=:UserInfo.getUserId()];
        System.debug('FEderated Id is ---'+u[0].FederationIdentifier);
        return u[0].FederationIdentifier;
    }
    
    /**
    * Gets the signed identity; always want to generate this in a getter since the constructor
    * only gets called on original page load and timestamp will skew
    */
    public String getSignedIdentity() {
        String signedIdentity = TableauSparklerUtilities.generateSignedIdentity(this.myCustomFieldMapper());
        system.debug('Singned Entity========'+signedIdentity);
        return signedIdentity;
    }
    
}
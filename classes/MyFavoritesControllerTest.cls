/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Tests Controller for MyFavorites VF Page
@Requirement Id  User Story : US2674

@ModifiedDate     16Sept2016
@author          Ashish
@Description     Test coverage for Order Favorite
@Requirement Id  SFDC - 1490
*/


@isTest

public class MyFavoritesControllerTest {
    
    //====Added by Ashish ====Start===
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Shared_Patient_Case__c.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROMENSHEALTH = RECTYPES.get('Urology Men\'s Health').getRecordTypeId();
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPESORD = Schema.SObjectType.Order.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROPHLOANERKIT = RECTYPESORD.get('Uro/PH Loaner Kit').getRecordTypeId();
    
    //====Added by Ashish ====End===
    
    static testmethod void getAccountFavoriteTest(){
        Test.startTest();
        Account account = new Account(Name = 'Test Account');
        insert account;
        
        MyFavoritesController.getAccountFavorites();
        
        //===Added By Ashish===Start==
        MyFavoritesController myCon = new MyFavoritesController();
        myCon.toggleSort();
        MyFavoritesController.getAccountFavorites();
        //===Added By Ashish===End==
        Test.stopTest();
    }  
    
    static testmethod void getContactFavoriteTest(){
        
        Test.startTest();
        Contact contact = new Contact(LastName = 'Test Contact');
        insert contact;
        
        MyFavoritesController.getContactFavorites();
        
        //===Added By Ashish===Start==
        MyFavoritesController myCon = new MyFavoritesController();
        myCon.toggleSort();
        MyFavoritesController.getContactFavorites();
        //===Added By Ashish===End==
        
        Test.stopTest();
    }
    
    static testmethod void getOpportunityFavoriteTest(){
        
        Test.startTest();
        Opportunity opty = new Opportunity(Name = 'Test Opty', CloseDate = Date.Today(), StageName = 'Prospecting');
        insert opty;
        
        MyFavoritesController.getOpportunityFavorites();
        
        //===Added By Ashish===Start==
        MyFavoritesController myCon = new MyFavoritesController();
        myCon.toggleSort();
        MyFavoritesController.getOpportunityFavorites();
        //===Added By Ashish===End==
        
        Test.stopTest();
    }
    //====Added by Ashish ====Start===
    @testSetup
    static void setupMockData(){
        list<Account> testAccLst = new list<Account>();
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId(),Account_Number__c = 'Test123',shippingCountry='US'); 
        testAccLst.add(acct1);
        insert testAccLst;   
        
         Shared_Patient_Case__c UMHPatientCaseobj = new Shared_Patient_Case__c(Shared_Facility__c = testAccLst[0].Id,Shared_Start_Date_Time__c = system.today()+6,Shared_end_Date_Time__c = system.today()+8,
                                                          RecordTypeId = RECTYPE_UROMENSHEALTH);
         insert UMHPatientCaseobj;
         
         Order ordObj = new Order(AccountId = testAccLst.get(0).id,Uro_PH_Patient_Case__c = UMHPatientCaseobj.id, RecordTypeId = RECTYPE_UROPHLOANERKIT,
                                  EffectiveDate= date.Today(),Status='Draft'); 
        
         Insert ordObj; 
     }        
    
    static testmethod void getOrderFavoriteTest(){
        
        Test.startTest();
        
        MyFavoritesController.getOrderFavorites();
        
        //===Added By Ashish===Start==
        MyFavoritesController myCon = new MyFavoritesController();
        myCon.toggleSort();
        MyFavoritesController.getOrderFavorites();
        //===Added By Ashish===End==
        
        Test.stopTest();
    }
    
    static testmethod void removeOrderFavoriteTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            
            List<Order_Favorite__c> ordFavs =new List <Order_Favorite__c>{};
            list<Order> ordLst = [Select id from Order];
            String OrderID;
            OrderID = ordLst[0].id;
            
            Order_Favorite__c orFav = new Order_Favorite__c(Uro_PH_Order__c=OrderID,Uro_PH_User__c=stdUser.ID);
            ordFavs.add(orFav);
            insert ordFavs;
            
            string orFavId;
            orFavId = ordFavs[0].id;
        
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('orderFavoriteId', orFavId);
            Test.setCurrentPage(pageRef);
            MyFavoritesController.removeFromOrderFavorites();
             
            Test.stopTest();
      }
    
    //====Added by Ashish ====End===
    
    static testmethod void removeAccountFavoriteTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            List<Account> accounts = new List<Account>{};
            List<Account_Favorite__c> accFavs =new List <Account_Favorite__c>{};
            
            Account acc = new Account(Name='test account');
            accounts.add(acc);
            insert accounts;
            
            String accountId;
            accountID = accounts[0].id;
            
            Account_Favorite__c aFav = new Account_Favorite__c(Account__c=accountID,User__c=stdUser.ID);
            accFavs.add(aFav);
            insert accFavs;
            
            string aFavId;
            aFavID = accFavs[0].id;
        
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('acctFavoriteId', aFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromAccountFavorites();
            Test.stopTest();
      }

    static testmethod void removeContactFavoriteTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            List<Contact> contacts = new List<Contact>{};
            List<Contact_Favorite__c> conFavs =new List <Contact_Favorite__c>{};
            
            Contact con = new Contact(LastName='test contact');
            contacts.add(con);
            insert contacts;
            
            String contactId;
            contactID = contacts[0].id;
            
            Contact_Favorite__c cFav = new Contact_Favorite__c(Contact__c=contactID,User__c=stdUser.ID);
            conFavs.add(cFav);
            insert conFavs;
            
            string cFavId;
            cFavID = conFavs[0].id;
        
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('contactFavoriteId', cFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromContactFavorites();
            Test.stopTest();
      }
      
    static testmethod void removeOpportunityFavoriteTest(){
            List<Opportunity> opportunities = new List<Opportunity>{};
            List<Opportunity_Favorite__c> oppFavs =new List <Opportunity_Favorite__c>{};
            
            Opportunity opp = new Opportunity(Name = 'Test Opty', CloseDate = Date.Today(), StageName = 'Prospecting');
            opportunities.add(opp);
            insert opportunities;
            
            String opportunityId;
            opportunityID = opportunities[0].id;
            
            Opportunity_Favorite__c oFav = new Opportunity_Favorite__c(Opportunity__c=opportunityID,User__c=UserInfo.getUserId());
            oppFavs.add(oFav);
            insert oppFavs;
            
            string oFavId;
            oFavID = oppFavs[0].id;
           
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('optyFavoriteId', oFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromOpportunityFavorites();
            Test.stopTest();
      }       
      
    static testmethod void removeFromFavoritesTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            List<Opportunity> opportunities = new List<Opportunity>{};
            List<Opportunity_Favorite__c> oppFavs =new List <Opportunity_Favorite__c>{};
            
            Opportunity opp = new Opportunity(Name = 'Test Opty', CloseDate = Date.Today(), StageName = 'Prospecting');
            opportunities.add(opp);
            insert opportunities;
            
            String opportunityId;
            opportunityID = opportunities[0].id;
            
            Opportunity_Favorite__c oFav = new Opportunity_Favorite__c(Opportunity__c=opportunityID,User__c=UserInfo.getUserId());
            oppFavs.add(oFav);
            insert oppFavs;
            
            string oFavId;
            oFavID = oppFavs[0].id;
           
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('optyFavoriteId', oFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromFavorites(oFavID);
             
             //====Added by Ashish== to Cover Catch code ===Start===    
            pageRef.getParameters().put('optyFavoriteId', null);
            Test.setCurrentPage(pageRef);
            MyFavoritesController.removeFromFavorites(null);
            //====Added by Ashish== to Cover Catch code ===End===  
            Test.stopTest();
      }              
     
}
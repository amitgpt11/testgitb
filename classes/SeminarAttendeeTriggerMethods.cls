/*
@modifiedDate    5 Oct2016
 @author         Ashish Master
 @Description    To map patient with Seminar Attandee record
 @Methods        
 @Requirement ID Sfdc-988
 */


public class SeminarAttendeeTriggerMethods {
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Patient__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_UROMENSHEALTH = RECTYPES.get('Uro/PH Men\'s Health').getRecordTypeId();
    
     public static void mapPatientToSeminarAttandee(List<Shared_Seminar_Attendee__c> newSemAttandeelst){
         Map<String,Patient__c> mapEmailPatient = new Map<String,Patient__c>();
         Set<String> setEmail = new Set<String>();
         Set<String> setSeminarId = new Set<string>();
         
         for(Shared_Seminar_Attendee__c att : newSemAttandeelst){
             if(String.IsNotBlank(att.Shared_Patient_Email_Address__c)){
                 setEmail.add(att.Shared_Patient_Email_Address__c);
             }
             if(String.IsNotBlank(att.Shared_Seminar_ID__c)){
                 setSeminarId.add(att.Shared_Seminar_ID__c);
             }
         }
         
         //get Seminar record
         list<Shared_Seminar__c> lstSem = [Select id,name from Shared_Seminar__c where name in: setSeminarId];
         Map<String,string> mapSeminarId = new Map<String,string>();
         for(Shared_Seminar__c sem : lstSem){
             mapSeminarId.put(sem.name,sem.Id);
         }
         
         // get Patient Record
         list<Patient__c> lstPatient = [Select id,Patient_Email_Address__c from Patient__c where Patient_Email_Address__c in : setEmail and recordtypeId =:RECTYPE_UROMENSHEALTH];
         for(Patient__c pt : lstPatient){
             mapEmailPatient.put(pt.Patient_Email_Address__c,pt);
         }
         
         list<Patient__c> lstPatientToInsert = new list<Patient__c>();
         Patient__c objPatient;
         
         Set<string> dupcheck = new Set<String>();
         boolean isDummySeminarNeeded = false;
         for(Shared_Seminar_Attendee__c att : newSemAttandeelst){
             if(att.Shared_Seminar__c == null){
                 if(String.IsNotBlank(att.Shared_Seminar_ID__c) ){ 
                     if(mapSeminarId != null && mapSeminarId.containsKey(att.Shared_Seminar_ID__c)){
                         att.Shared_Seminar__c = mapSeminarId.get(att.Shared_Seminar_ID__c);
                     }else{
                         
                         isDummySeminarNeeded = true;
                     }
                 }else{
                     isDummySeminarNeeded = true;
                 }
             }
             
             
             if(dupcheck.add(att.Shared_Patient_Email_Address__c)){
                 objPatient = new Patient__c();
                 if(mapEmailPatient.ContainsKey(att.Shared_Patient_Email_Address__c)){
                     objPatient.id = mapEmailPatient.get(att.Shared_Patient_Email_Address__c).id;
                     
                 }
                 // Added on 13 Oct by Ashish==Start 
                 if(String.IsNotBlank(att.Shared_Address_1__c)){
                     objPatient.Address_1__c = att.Shared_Address_1__c;
                 }
                 if(String.IsNotBlank(att.Shared_Address_2__c)){
                     objPatient.Address_2__c = att.Shared_Address_2__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_City__c)){
                     objPatient.City__c = att.Shared_City__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_State__c)){
                     objPatient.State__c = att.Shared_State__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Zip__c)){
                     objPatient.Zip__c = att.Shared_Zip__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Country__c)){
                     objPatient.Country__c = att.Shared_Country__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Mobile_Number__c)){
                     objPatient.Patient_Mobile_Number__c = att.Shared_Mobile_Number__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Daytime_Phone__c)){
                     objPatient.Shared_Daytime_Phone__c = att.Shared_Daytime_Phone__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Evening_Phone__c)){
                     objPatient.Shared_Evening_Phone__c = att.Shared_Evening_Phone__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_ED_Treatment__c)){
                     objPatient.Uro_Ph_ED_Treatment__c = att.Shared_ED_Treatment__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Source__c)){
                      objPatient.Source__c = att.Shared_Source__c;
                 }
                
                 if(String.IsNotBlank(att.Shared_ED_Duration__c)){
                     objPatient.Uro_Ph_ED_Duration__c = att.Shared_ED_Duration__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Preferred_Contact_Method__c)){
                     objPatient.Shared_Preferred_Contact_Method__c = att.Shared_Preferred_Contact_Method__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_ED_Oral_Medications__c)){
                     objPatient.Uro_Ph_ED_Oral_Medications__c = att.Shared_ED_Oral_Medications__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Penile_Injections__c)){
                     objPatient.Uro_Ph_Penile_Injections__c= att.Shared_Penile_Injections__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Suppositories_MUSE__c)){
                     objPatient.Uro_Ph_Suppositories_MUSE__c = att.Shared_Suppositories_MUSE__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Vacuum_Erection_Device_VED__c)){
                     objPatient.Uro_Ph_Vacuum_Erection_Device_VED__c = att.Shared_Vacuum_Erection_Device_VED__c;
                 }
                 
                 if(att.Shared_ED_Implant_Received__c != null){
                     objPatient.Uro_Ph_ED_Implant_Received__c = att.Shared_ED_Implant_Received__c;
                 }
                 
                 if(att.Shared_Opt_in_Date__c != null){
                     objPatient.Uro_Ph_Opt_in_Date__c = att.Shared_Opt_in_Date__c;
                 }
                 
                 // Added on 13 Oct by Ashish==End
                 
                 if(String.IsNotBlank(att.Shared_Patient_First_Name__c)){
                     objPatient.Patient_First_Name__c = att.Shared_Patient_First_Name__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Patient_Last_Name__c)){
                     objPatient.Patient_Last_Name__c = att.Shared_Patient_Last_Name__c;
                 }
                 
                 if(String.IsNotBlank(att.Shared_Patient_Email_Address__c)){
                     objPatient.Patient_Email_Address__c = att.Shared_Patient_Email_Address__c;
                 }
                 
                 if(String.IsNotBlank(att.Uro_Ph_Requested_Information__c)){
                     objPatient.Uro_Ph_Requested_Information__c = att.Uro_Ph_Requested_Information__c ;
                 }
                 
                 objPatient.recordtypeId = RECTYPE_UROMENSHEALTH; 
                         
                 lstPatientToInsert.add(objPatient);
             }
         }
         System.debug('===lstPatientToInsert==='+lstPatientToInsert);
         if(lstPatientToInsert.size() > 0){
             upsert lstPatientToInsert;
         } 
         for(Patient__c pt : lstPatientToInsert){
             mapEmailPatient.put(pt.Patient_Email_Address__c,pt);
         }
         
         Id dummySeminarId;
         if(isDummySeminarNeeded){
             Shared_Seminar__c dummySeminar = new Shared_Seminar__c();
             dummySeminar.Name = 'DummySeminar'+UserInfo.getUserId();
             Insert dummySeminar;
             
             dummySeminarId = dummySeminar.Id;
         }
         
         for(Shared_Seminar_Attendee__c att : newSemAttandeelst){
                 try{
                 if(String.IsNotBlank(att.Shared_Patient_Email_Address__c)){    
                     att.Shared_Patient__c = mapEmailPatient.get(att.Shared_Patient_Email_Address__c).id;
                 }
                 if(att.Shared_Seminar__c == null){
                     if(String.IsNotBlank(att.Shared_Seminar_ID__c)){ 
                         if(mapSeminarId != null && mapSeminarId.containsKey(att.Shared_Seminar_ID__c)){
                             att.Shared_Seminar__c = mapSeminarId.get(att.Shared_Seminar_ID__c);
                         }else{
                             
                            // att.adderror('Your event registration has failed due to an invalid Seminar Id.');
                             att.Shared_Seminar__c = dummySeminarId;
                             att.Shared_Seminar_ID__c = 'DummySeminar'+UserInfo.getUserId();
                         }
                     }else{
                         //att.adderror('Your information request has been successfully processed.  As this was not an event registration, the seminar attendee record has been blocked from insert.');
                         att.Shared_Seminar__c = dummySeminarId;
                         att.Shared_Seminar_ID__c = 'DummySeminar'+UserInfo.getUserId();
                     }
                 }
                 }Catch(exception e){
                     system.debug('#####===e=='+e);
                 }
                 System.debug('==att==='+att);
                /* if(att.Shared_Source__c.equalsIgnoreCase('EDCure.org') || att.Shared_Source__c.equalsIgnoreCase('AMSMensHealth.com') || att.Shared_Source__c.equalsIgnoreCase('fixincontinence.com')){
                     att.Shared_Source__c.addError('You can\'t insert this record.');
                 }*/
             
         }
         
     }
     
     public static void DeleteDummySeminar(List<Shared_Seminar_Attendee__c> newSemAttandeelst){
         Set<ID> seminarIdSet = new Set<ID>();
         String dummySeminarString = 'DummySeminar'+UserInfo.getUserId();
         for(Shared_Seminar_Attendee__c att : newSemAttandeelst){
             if(att.Shared_Seminar_ID__c == dummySeminarString){
                 seminarIdSet.add(att.Shared_Seminar__c);
             }
         }
         System.debug('===seminarIdSet==='+seminarIdSet);
         if(seminarIdSet.size() > 0){
             Delete [Select id from Shared_Seminar__c where id in :seminarIdSet];
         }
     }
     
    /* public static void DeleteSeminarAttendee(List<Shared_Seminar_Attendee__c> newSemAttandeelst){
         Set<id> lstSeminarAttendeeIds = new set<id>();
         for(Shared_Seminar_Attendee__c att : newSemAttandeelst){
             if(String.IsNotBlank(att.Shared_Source__c) && (att.Shared_Source__c.equalsIgnoreCase('EDCure.org') || att.Shared_Source__c.equalsIgnoreCase('AMSMensHealth.com') || att.Shared_Source__c.equalsIgnoreCase('fixincontinence.com'))){
                 lstSeminarAttendeeIds.add(att.id);
             }   
         }
         System.debug('===lstSeminarAttendeeIds===='+lstSeminarAttendeeIds);
         if(lstSeminarAttendeeIds.size() > 0){
             Delete [Select id from Shared_Seminar_Attendee__c where id in : lstSeminarAttendeeIds];
         }
     }*/
}
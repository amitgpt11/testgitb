/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/getcustomerproperties/*')
global class RestGetCustomerProperties {
    global RestGetCustomerProperties() {

    }
    @HttpPost
    global static openq.OpenMSL.CUSTOMER_PROPERTIES_RESPONSE_element getCustomerProperties(openq.OpenMSL.CUSTOMER_PROPERTIES_REQUEST_element request_x) {
        return null;
    }
}

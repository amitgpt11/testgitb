global class batchPhysicianOpportunityUpdate implements Schedulable,Database.Batchable<sObject>,Database.Stateful {

            List<contact> AllNewAccWiseContactList = new List<contact>();
            List<Contact> cFinalwithOwner = new List<Contact>();
            List<Patient__c> selectedPList = new List<Patient__c>();
            Set<Id> pOldTerr = new Set<Id>();
            List<Physician_Territory_Realignment__c> Allscope = new List<Physician_Territory_Realignment__c>();
            List<Patient__c> Allpatients = new List<Patient__c>();
            List<id> AlloldAccountIdList = new List<id>();
            set<id> AlloldContactTerriMulti = new set<id>();
            set<id> AlloldPatientTerriMulti = new set<id>();
            static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{}; 
            Public static final String EDITACCESS = 'Edit';
            
             //Opportunity
RowCauseObjects__c RowCauseObj = RowCauseObjects__c.getInstance('Opportunity');
String      RowCauseName      = RowCauseObj.RowCauses__c;

List<String>     lstRowCauseOppy=  RowCauseName.split(',');
//lstRowCauseOppy.add(RowCauseName);
       
        global Database.QueryLocator start(Database.BatchableContext BC) {
           
            String query = 'SELECT Id,Name,New_Territory_Patient__c,Old_Territory_Patient__c,Opportunity_Id__c,Patient_Id__c,Record_Status__c,Remark__c  FROM Opportunity_Territory_Realignment__c where Record_Status__c = \'Pending\'' ;
            
            return Database.getQueryLocator(query);
        }
       
        global void execute(Database.BatchableContext BC, List<Opportunity_Territory_Realignment__c> scope) {
            system.debug('___RowCauseName__'+RowCauseName);
            system.debug('======lstRowCauseOppy'+lstRowCauseOppy);    
            
            set<id> oppyIdList = new set<id> ();
            system.debug('----Opportunity_Territory_Realignment__c scope--'+scope+'____ scope size ____'+scope.size());
            
    set<id> oppSet = new set<id>();
    set<id> newTerritory = new set<id>();
    set<Id> OpptyOldTerritory = new Set<Id>();
    Map<id,id> oppyTM  = new Map<id,id>();
    Map<id,id> oppyTMOwner  = new Map<id,id>();
    List<Opportunity_Territory_Realignment__c> oppyTerReloList = new  List<Opportunity_Territory_Realignment__c>();
    List<Opportunity>  oppoList= new List<Opportunity> ();
    
        if(!scope.isEmpty())

        {       
            for(Opportunity_Territory_Realignment__c oppyTerRelo : scope)
            {
            oppSet.add(oppyTerRelo.Opportunity_Id__c);
            newTerritory.add(oppyTerRelo.New_Territory_Patient__c);
            OpptyOldTerritory.add(oppyTerRelo.Old_Territory_Patient__c);
            oppyTM.put(oppyTerRelo.Opportunity_Id__c,oppyTerRelo.New_Territory_Patient__c);

            oppyTerRelo.Record_Status__c ='Processed';
            oppyTerReloList.add(oppyTerRelo);

            }
        }        
        
    List<Seller_Hierarchy__c> SellertHierarchyList= [SELECT Id,Name,Current_TM1_Assignee__c ,ownerid FROM Seller_Hierarchy__c where id in: newTerritory ];
     
    for (Opportunity oppo : [SELECT Id, Territory_ID__c, Patient__c, Patient__r.Physician_of_Record__c,ownerid FROM Opportunity where id in : oppSet  ]) //and RecordTypeId != :  RECTYPE_Trial_Implant
    {     
        for(Seller_Hierarchy__c sellobj :SellertHierarchyList)
        {
       
        if(oppyTM.get(oppo.id)== sellobj.Id && (sellobj.Current_TM1_Assignee__c != null || sellobj.Current_TM1_Assignee__c != '')){
        
         oppo.ownerid = sellobj.Current_TM1_Assignee__c;
         oppo.Territory_ID__c = sellobj.id;
         oppoList.add(oppo);
        
        }
        
        }
    }           
 system.debug('***************opprotunity  update OpportunityTerritoryRealignmentList '+oppoList);
            
            if(!oppoList.isEmpty()){
            
                      List<Database.SaveResult> oppoListSaveResults = Database.update(oppoList, false);
                    for (Database.SaveResult result : oppoListSaveResults) {
                    //system.debug('====result oppourtunitySaveResults  =='+result );
                    if (!result.isSuccess()) {
                    for (Database.Error err : result.getErrors()) {
                        logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Update of opportunity Obj On NMD_ObjectTerritoryByContactBatch.execute'));
                        system.debug('***************Opportunity_Territory_Realignment__c update error'+err);
                    }
                    }
                }  
            }
                
//------ oppprtunity sharing starts
            
        Set<id> PhysicianaAssigneeUserListopp = new Set<id>();

        List<Assignee__c> PhysicianAssigneeListopp= [Select id,Name,Territory__c,Assignee__c From  Assignee__c where Territory__c in :OpptyOldTerritory  and Assignee__r.isActive =true and REP_DESIGNATION__C!='TM1'];
        system.debug('---- PhysicianAssigneeListopp---'+PhysicianAssigneeListopp);    
              
         
         for(Assignee__c PasObj : PhysicianAssigneeListopp)
         {
           PhysicianaAssigneeUserListopp.add(PasObj.Assignee__c);
            
         }  
         system.debug('---- PhysicianaAssigneeUserListopp---'+PhysicianaAssigneeUserListopp);
         

//PhysicianaAssigneeUserListopp

      List<OpportunityShare > lstOppSahre= [SELECT OpportunityAccessLevel,Id,LastModifiedById,OpportunityId,RowCause,UserOrGroupId FROM  OpportunityShare where UserOrGroupId in : PhysicianaAssigneeUserListopp and RowCause not in :lstRowCauseOppy and OpportunityId IN :oppSet]; 
      system.debug('---- lstOppSahre before Delete ---'+lstOppSahre); 
    if(!lstOppSahre.isEmpty()){

    delete lstOppSahre;
    system.debug('---- lstOppSahre after Delete ---'+lstOppSahre); 
    }
    

List<Assignee__c> leadAssigneeList= [Select id,Name,Territory__c,Assignee__c From  Assignee__c where Territory__c =: newTerritory  and Assignee__r.isActive =true and REP_DESIGNATION__C!='TM1'];  

set<id> leadAssigneeIdSet = new set<id>();
List<LeadShare> LeadShareIntList = new  List<LeadShare>();
for(Assignee__c  assi:leadAssigneeList)
{

leadAssigneeIdSet.add(assi.Assignee__c);
}
   

      List<OpportunityShare> OppyShareIntList = new  List<OpportunityShare>();
     
     
     system.debug('~~~~~~~~~~~~~~~~contactAssigneeList !!!!!!  '+leadAssigneeList+'====size==='+leadAssigneeList.size());
   
     system.debug('~~~~~~~~~~~~~~~~oppoList sharing  !!!!!!  '+oppoList+'====size==='+oppoList.size());
     

    for(Assignee__c asbj : leadAssigneeList){

    
        for(Opportunity l : oppoList){
         if(l.Territory_ID__c==asbj.Territory__c)
        {
        //SELECT Id,IsDeleted,LastModifiedById,LastModifiedDate,LeadAccessLevel,LeadId,RowCause,UserOrGroupId FROM LeadShare

        //single.RowCause = Schema.Patient__Share.RowCause.NMDTerritoryChange; //'NMDTerritoryChange';
        OpportunityShare single = new OpportunityShare();
        single.OpportunityId = l.id; //'a0L11000006G6ECEA0';     
        single.OpportunityAccessLevel = EDITACCESS;
        single.UserOrGroupId = asbj.Assignee__c ;//'005o0000002oDQRAA2';

        OppyShareIntList.add(single);     
        }
       }

    }
    system.debug('~~~~~~~~~~~~~~~~OppyShareIntList before Insertion  !!!!!!  '+OppyShareIntList+'====size==='+OppyShareIntList.size());
    
    if(!OppyShareIntList.isEmpty())
    {

    insert OppyShareIntList;
    system.debug('~~~~~~~~~~~~~~~~OppyShareIntList after Insertion  !!!!!!  '+OppyShareIntList+'====size==='+OppyShareIntList.size());

}
    //+++++++ sharing ends opportunityShare
    
     if(!oppyTerReloList.isEmpty())
            update oppyTerReloList;
        
//------ oppprtunity sahring ends

        }  


    global void execute(SchedulableContext context) {
    integer batchSize = Integer.valueOf(10);
    batchPhysicianOpportunityUpdate b = new batchPhysicianOpportunityUpdate();
    database.executebatch(b,batchSize);

    }       
        
    global void finish(Database.BatchableContext BC) {
           
    }
}
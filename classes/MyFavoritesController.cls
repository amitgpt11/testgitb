/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Controller for MyFavorites VF Page
@Requirement Id  User Story : US2674
*/


//Gets current user's Favorites
public class MyFavoritesController {
    public static String sortDir = 'ASC';
    public string sortVar;
    public static string sortAccField{get;set;}
    public static string sortOppField{get;set;}
    public static string sortConField{get;set;}
    public static string sortOrdField{get;set;}
    
    Static{
        sortAccField = 'Account__r.Name';
        sortOppField = 'Opportunity__r.Account.Name';
        sortOrdField = 'Uro_PH_User__r.Account.Name';
        sortConField = 'contact__r.Lastname';
    }
    public MyFavoritesController(){
        
        sortVar = 'ASC';
       
    }
    
    public static List<Opportunity_Favorite__c> getOpportunityFavorites(){
       /* List<Opportunity_Favorite__c> results = [SELECT Id, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Account.Id,Opportunity__r.Account.Name, Opportunity__r.Amount, Opportunity__r.CloseDate, Opportunity__r.Owner.Name,Opportunity__r.StageName,Opportunity__r.LastModifiedDate 
                                                FROM Opportunity_Favorite__c WHERE User__r.Id = :UserInfo.getUserId() order by Opportunity__r.Account.Name ASC]; */
        List<Opportunity_Favorite__c> results = new List<Opportunity_Favorite__c>();
        try{
        String query = 'SELECT Id, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Account.Id,Opportunity__r.Account.Name, Opportunity__r.Amount, Opportunity__r.CloseDate, Opportunity__r.Owner.Name,Opportunity__r.StageName,Opportunity__r.LastModifiedDate FROM Opportunity_Favorite__c WHERE User__r.Id = \''+UserInfo.getUserId() +'\' order by '+sortOppField+' '+sortDir;
        results = Database.query(query);
        
        }Catch(exception e){
            System.debug('###==e==='+e);
        }
        return results;       
    }
    
    public static List<Account_Favorite__c> getAccountFavorites(){
       /* List<Account_Favorite__c> results = [SELECT Id, Account__r.Id,Account__r.Name,Account__r.GPO_Name__c,Account__r.GPO_ID__c,Account__r.IDN_Name__c,Account__r.IDN_ID__c,Account__r.Favorite__c 
                                             FROM Account_Favorite__c WHERE User__r.Id = :UserInfo.getUserId()  order by Account__r.Name ASC];*/
        List<Account_Favorite__c> results = new List<Account_Favorite__c>();
        try{
        System.debug('===sortDir==='+sortDir);
        System.debug('===sortAccField==='+sortAccField);
       
        String query = 'SELECT Id, Account__r.Id,Account__r.Name,Account__r.GPO_Name__c,Account__r.GPO_ID__c,Account__r.IDN_Name__c,Account__r.IDN_ID__c,Account__r.Favorite__c FROM Account_Favorite__c WHERE User__r.Id = \''+UserInfo.getUserId() +'\' order by '+sortAccField+' '+sortDir;
        System.debug('==query==='+query);
        results = Database.query(query);
        
        }Catch(exception e){
            System.debug('###==e==='+e);
        }
        return results;  
        
    }
    
    public static List<Contact_Favorite__c> getContactFavorites(){
        /*List<Contact_Favorite__c> results = [SELECT Id, contact__r.Id, contact__r.Name, contact__r.Lastname,contact__r.FirstName,contact__r.Account.Name
                                             FROM Contact_Favorite__c WHERE User__r.Id = :UserInfo.getUserId() order by contact__r.Lastname ASC]; */
        
        List<Contact_Favorite__c> results = new List<Contact_Favorite__c>();
        try{
        
        String query = 'SELECT Id, contact__r.Id, contact__r.Name, contact__r.Lastname,contact__r.FirstName,contact__r.Account.Name FROM Contact_Favorite__c WHERE User__r.Id = \''+UserInfo.getUserId() +'\' order by '+sortConField +' '+sortDir;
        System.debug('==query==='+query);
        results = Database.query(query);
        
        }Catch(exception e){
            System.debug('###==e==='+e);
        }
        return results;  
    }
    
    public static List<Order_Favorite__c> getOrderFavorites(){
        /*List<Order_Favorite__c> results = [SELECT Id, Uro_PH_Order__r.Id, Uro_PH_Order__r.Name,Uro_PH_Order__r.PoNumber,Uro_PH_Order__r.OrderNumber,Uro_PH_Order__r.ANZ_Implanting_Physician__c,Uro_PH_Order__r.ANZ_Implanting_Physician__r.Name, Uro_PH_Order__r.Uro_PH_Kit_Name__c, Uro_PH_Order__r.Account.Name
                                             FROM Order_Favorite__c WHERE Uro_PH_User__r.Id = :UserInfo.getUserId() order by Uro_PH_User__r.Account.Name ASC]; */
        
        List<Order_Favorite__c> results = new List<Order_Favorite__c>();
        try{
        
        String query = 'SELECT Id, Uro_PH_Order__r.Id, Uro_PH_Order__r.Name,Uro_PH_Order__r.PoNumber,Uro_PH_Order__r.OrderNumber,Uro_PH_Order__r.ANZ_Implanting_Physician__c,Uro_PH_Order__r.ANZ_Implanting_Physician__r.Name, Uro_PH_Order__r.Uro_PH_Kit_Name__c, Uro_PH_Order__r.Account.Name FROM Order_Favorite__c WHERE Uro_PH_User__r.Id = \''+UserInfo.getUserId() +'\' order by '+sortOrdField+' '+sortDir;
        System.debug('==query==='+query);
        results = Database.query(query);
        
        }Catch(exception e){
            System.debug('###==e==='+e);
        }
        return results;  
    }

    
//Remove record selected from MyFavorites page

    public static void removeFromAccountFavorites(){
        Id acctFavoriteId = Apexpages.currentPage().getParameters().get('acctFavoriteId');
        Account_Favorite__c myAcctFav = [SELECT Id FROM Account_Favorite__c WHERE Id = :acctFavoriteId ];
                                        

        try {
            delete myAcctFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
    public static void removeFromContactFavorites(){
        Id contactFavoriteId = Apexpages.currentPage().getParameters().get('contactFavoriteId');
        Contact_Favorite__c myContFav = [SELECT Id FROM Contact_Favorite__c WHERE Id = :contactFavoriteId ];
                                        
        try {
            delete myContFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
    public static void removeFromOpportunityFavorites(){
        Id optyFavoriteId = Apexpages.currentPage().getParameters().get('optyFavoriteId');
        Opportunity_Favorite__c myOptyFav = [SELECT Id FROM Opportunity_Favorite__c WHERE Id = :optyFavoriteId ];

        try {
            delete myOptyFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
    public static void removeFromOrderFavorites(){
        Id orderFavoriteId = Apexpages.currentPage().getParameters().get('orderFavoriteId');
        list<Order_Favorite__c> myOrderFav = [SELECT Id FROM Order_Favorite__c WHERE Id = :orderFavoriteId ];

        try {
            delete myOrderFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
// Inline vfPage Remove favorites
    Public Static boolean removeFromFavorites(Id favoriteId){
        //sObject sObj = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;
        boolean returnFlag = true;
        try{
            String sObjName = favoriteId.getSObjectType().getDescribe().getName();
            String query = 'Select id from '+String.escapeSinglequotes(sObjName)+' where id =:favoriteId';
            System.debug('==query ==='+query );
            
            list<sobject> listToDelete = Database.query(query);
            System.debug('==listToDelete==='+listToDelete);
            if(listToDelete.size() > 0){
                delete listToDelete;
            }
        }Catch(Exception e){
            System.debug('###==e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Please Contact your Administrator'));
            returnFlag = false;
        }
        return returnFlag;
    }
    
    //Sort method
    public pagereference toggleSort(){
        System.debug('===sortAccField==='+sortAccField);
        sortDir = sortVar.equalsIgnoreCase('asc') ? 'desc' : 'asc';
        sortVar = sortDir;
        System.debug('===sortDir==='+sortDir);
        
        return null;
    }
      
}
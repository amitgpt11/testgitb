/**
 * Name : ENDOUpdateTerritoryOpptyController 
 * Author : Accenture - Mayuri
 * Description : This class is invoked by Visual Force page ENDOUpdateTerritoryOnOppty
 * Date : 01JUN2016
 * Requirement : Q3 2016 - US2508
 */
public with sharing class ENDOUpdateTerritoryOpptyController {
    public Opportunity oppty{get;set;}
    public Id accId{get;set;}
    public list<Territory2> GIresults{get;set;} // search results
    public list<Territory2> PULMresults{get;set;} // search results
    public string GIsearchString{get;set;} // search keyword 
    public string PULMsearchString{get;set;}
    public list<Id> terrIdLst{get;set;}
    public list<Territory2> terrLst{get;set;}
    public list<ObjectTerritory2Association> objT2ALst{get;set;}
    public String terrSelected{get;set;}
    private final ApexPages.StandardController CONTROL;
    public boolean isShow{get;set;}
    public boolean isError{get;set;}
    public string customLabel{get;set;}
    string selectedType;

    public ENDOUpdateTerritoryOpptyController(ApexPages.StandardController controller) {
        terrIdLst = new list<Id>();
        terrLst = new list<Territory2>();
        objT2ALst = new list<ObjectTerritory2Association>();
        oppty = (Opportunity)controller.getRecord();  
        
        this.CONTROL = controller;  
        isShow = false;
        isError = false;   
        retrieveQueryResults();
    }
    // *************************************** //
    public String getSelectedType() {
        return selectedType;
    }
                    
    public void setSelectedType(String selectedType) { this.selectedType = selectedType; }
    public List<SelectOption> getTypes() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('GI Territory','GI Territory')); 
        options.add(new SelectOption('PULM Territory','PULM Territory')); 
        return options; 
    }
    
    // ****************************************  //
    public void retrieveQueryResults(){
    
    if(oppty.Id !=null){
    
            accId = [SELECT Id,AccountId FROM Opportunity WHERE Id =: oppty.Id].AccountId ;//Get the accountId related to Oppty id
            
            system.debug('accId : '+accId);
            
            objT2ALst =  [SELECT Id,ObjectId,Territory2Id FROM ObjectTerritory2Association WHERE ObjectId =: accId];//Get the list of territories related to the account
            for(ObjectTerritory2Association  accgrp : objT2ALst)//Loop through the list of territories
            {
                terrIdLst.add(accgrp.Territory2Id);
            }
            
            terrLst = [SELECT Id, Name,DeveloperName,ParentTerritory2Id,Description,Territory2TypeId FROM Territory2 where Id IN : terrIdLst];
            GIresults = terrLst;
            PULMresults = terrLst;
            if(GIresults.size() > 0){
                isShow = true;
            }
            if(PULMresults.size() > 0){
                isShow = true;
            }
            
        }
    }
 /*   
    public ENDOUpdateTerritoryOpptyController() {
        // get the current search string
        //searchString = System.currentPageReference().getParameters().get('lksrch');
        searchPString = System.currentPageReference().getParameters().get('lksrch');
        results=terrLst;
        resultsP=terrLst;
        runSearch();
        runSearchP();  
    }
*/    
    // performs the keyword search
    public PageReference GIsearch() {
        runGISearch();
        return null;
    }
    public PageReference PULMsearch() {
        runPULMSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runGISearch() {
        GIresults = performSearch(GIsearchString); 
        if(GIresults.size() > 0 ){
            isShow = true;
            isError = false;
        }
        else{
            isShow = false;
            isError = true;
        }
                      
    }
    // prepare the query and issue the search command
    private void runPULMSearch() {
        PULMresults = performSearch(PULMsearchString); 
        if(PULMresults.size() > 0 ){
            isShow = true;
            isError = false;
        }
        else{
            isShow = false;
            isError = true;
        }
                      
    } 
     
    
    // run the search and return the records found. 
    private List<Territory2> performSearch(string searchString) {
        String terr = 'SELECT Id, Name,DeveloperName,ParentTerritory2Id,Territory2TypeId,Description  FROM Territory2';
        if(searchString != '' && searchString != null)
            terr = terr+  ' WHERE Name LIKE \'%' + searchString +'%\'';
        terr = terr + ' limit 200'; 
        System.debug(terr+'terr');
        return database.query(terr);        
    }    
    
    
    public void dummyAction(){
        system.debug(selectedType+'selectedType');
        GIsearchString = '';
        PULMsearchString = '';
        retrieveQueryResults();
    }  
    
     public PageReference saveOppty(){
        boolean Excep;
        
        if(accId  != null && selectedType != null){
            if(selectedType == 'GI Territory'){
                oppty.Territory2Id = terrSelected;
            }
            else{
                string terrName = getTerritoryDetails(terrSelected);
                if(string.isNotEmpty(terrName)){
                    oppty.ENDO_Secondary_Territory__c = terrName;
                    Territory2 obj1 = [SELECT Id,Name,ParentTerritory2.Description,ParentTerritory2.ParentTerritory2.Description,Description  FROM Territory2 Where Name =: terrName];
                     oppty.SecondaryTerritoryDescription__c=obj1.Description;
                       oppty.Secondary_Territory_Area__c = obj1.ParentTerritory2.Description;
                       oppty.SecondaryTerritoryRegion__c = obj1.ParentTerritory2.ParentTerritory2.Description;
                }
            }
            
            
            
            try{
                update oppty; 
                Excep = false;
            }catch(Exception e){
                Excep = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You must select a territory from the active territory model that the parent account is assigned to.'));
            }
         }
         if(Excep){
             return null;
         }
         else{
            /*pagereference ref = new pagereference('/'+oppty.id);
            ref.setredirect(true);
            return ref;*/
            
            PageReference pageRef = this.CONTROL.save();
            Map<string,string> URLParameters = ApexPages.currentPage().getParameters();
            if(URLParameters.containsKey('retURL')){
                pageRef = new PageReference(URLParameters.get('retURL'));
                }       
            return pageRef;
         }
    }
    public PageReference redirectToPage(){
        pagereference ref = new pagereference('/'+oppty.id);
        ref.setredirect(true);
        return ref;
    }
    public string getTerritoryDetails(Id terrId){
        string name;
        if(terrId != null){
            name = [SELECT Name FROM Territory2 WHERE Id =: terrId].Name; 
        }
        return Name;
    }

}
public with sharing class CycleCountResponseAnalysisManager{
    
    List<Verification__c> ccraAssociatedVerificationRecordList  = new List<Verification__c>();
    Map<Id,List<Verification__c>> mapCCRAtoVerfication = new Map<Id,List<Verification__c>>();
    List<Verification__c> ccraAssociatedVerificationRemoveList  = new List<Verification__c>();
    
    public void fetchRelatedVerifications(List<Cycle_Count_Response_Analysis__c> CCRAs){
        if(!Trigger.IsDelete){
        ccraAssociatedVerificationRecordList = [SELECT Id, Name,Cycle_Count_Line_Item__c, Cycle_Count_Line_Item__r.Cycle_Count_Response_Analysis__c,Cycle_Count_Line_Item__r.Cycle_Count_Response_Analysis__r.Id FROM Verification__c WHERE Cycle_Count_Line_Item__r.Cycle_Count_Response_Analysis__r.Id IN :CCRAs];
        
        For(Cycle_Count_Response_Analysis__c c:CCRAs){
            for(Verification__c v: ccraAssociatedVerificationRecordList){
                List<Verification__c> vfs = mapCCRAtoVerfication.get(c.Id);
                    if(v.Cycle_Count_Line_Item__r.Cycle_Count_Response_Analysis__r.Id == c.id){
                        if(vfs!=null && vfs.size()>0){
                            mapCCRAtoVerfication.get(c.Id).add(v);
                        }else{
                            mapCCRAtoVerfication.put(c.id, new List<Verification__c>{v});
                    }
                        
                    }
            }
        }
      }
    }           
    
    
    public void processVerifications(Cycle_Count_Response_Analysis__c oldCcra, Cycle_Count_Response_Analysis__c newCcra){
    
        if(!Trigger.isDelete && oldCcra.Status__c!='Scanning' && (newCcra.Status__c=='Scanning' || newCcra.Status__c=='Ready')){
            If(mapCCRAtoVerfication.get(newCcra.Id)!=null && mapCCRAtoVerfication.get(newCcra.Id).size()>0)
                ccraAssociatedVerificationRemoveList.addAll(mapCCRAtoVerfication.get(newCcra.Id));
        }
    }
    
    public void commitVerificationChanges(){
        if(!ccraAssociatedVerificationRemoveList.isEmpty()){
            delete ccraAssociatedVerificationRemoveList;
        }
    }
    
}
/**
* Batch class, Change Object Owner Based on Territory Id 
*
* @author   Salesforce services
* @date     2014-04-03
*/
global class NMD_ContactOwnerByTerritorySchedule implements Schedulable{
    
    /**
    * immediately execute the batch
    * 
    * @param void
    */
    global void execute(SchedulableContext context) {
        NMD_ObjectOwnerByTerritoryBatch.runNowContacts();
    }

}
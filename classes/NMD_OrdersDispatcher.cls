/**
* Dispatcher class, to process incoming REST requests
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class NMD_OrdersDispatcher implements IRestHandler {
	
	// url keys/parts
	final static String KEY_ORDERID = 'orderId';
	final static String KEY_ACTION = 'action';

	// mapping, ie: /nmd/orders/{orderId}/{action}
	final static String URI_MAPPING = (String.format('/nmd/orders/\'{\'{0}\'}\'/\'{\'{1}\'}\'', new List<String> {
		KEY_ORDERID, KEY_ACTION
	}));

	// actions
	final static String ACTION_BILLING = 'billing';
	final static String ACTION_SUBMIT = 'submit';
	final static String ACTION_VALIDATE = 'validate';

	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		// retrieve the Order Ids
		Set<Id> orderIds = new Set<Id>{};
		try {
			// there may be several Ids
			String param = parameters.get(KEY_ORDERID);
			if (param.contains(',')) {
				for(String parm : param.split(',', -1)) {
					orderIds.add((Id)parm);
				}
			}
			else {
				orderIds.add((Id)param);
			}
		}
		catch (System.StringException te) {
			// return error
			caller.setResponse(400, 'Invalid Order Id');
			return;
		}
		String action = parameters.get(KEY_ACTION);
		Boolean success = false;
		String lastError;

		// call the action's internal method
		if (action == ACTION_BILLING) {
			//success = osm.generateAndEmailBillingForm();
			NMD_OrderBillingFormManager.generateAndEmailBillingForm(orderIds); // @future
			success = true;
		}
		else if ((new Set<String> { ACTION_SUBMIT, ACTION_VALIDATE }).contains(action)) {

			NMD_OrderSubmissionManager osm = new NMD_OrderSubmissionManager(orderIds);

			if (action == ACTION_SUBMIT) {
				success = osm.submitOrders();
			}
			else if (action == ACTION_VALIDATE) {
				success = osm.validateOrder();
			}

			if (!success) {
				lastError = osm.lastError;
			}

		}
		else {
			caller.setResponse(400, 'Invalid Action');
			return;
		}

		if (success) {
			caller.setResponse(200);
		}
		else {
			caller.setResponse(500, lastError);
		}	

	}

}
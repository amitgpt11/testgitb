/**
* Handler class for the Task trigger
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class TaskTriggerHandler extends TriggerHandler {
    
    private Boolean isDeletePermissible;
    
    
    final TaskManager manager = new TaskManager();
    
    public override void bulkBefore() {
        
        if(!trigger.IsDelete){
            
            List<Task> newTasks = (List<Task>)trigger.new;
            
            TaskManager.updateNMDLeadPatientEducationTasks( newTasks );
            //manager.createTasksOnPatients(newTasks);

            //if(trigger.IsUpdate){
            //    manager.fetchUserProfileName();
            //}
        }
        else{
            // The following profiles only must be able to delete the Task
            // In the Before Delete, prevent deletion, if the profile is not an admin
           /* Set<String> profilesExempt = new Set<String> {
                'EU Sys Admin', 
                    'BSci System Admin', 
                    'System Administrator'
                    };
                        String profileName = [
                            select Name
                            from Profile
                            where Id = :UserInfo.getProfileId()
                        ].Name;
            
            this.isDeletePermissible = profilesExempt.contains(profileName);

           
            Set<String> profilesExempt = new Set<String> {
                         'Shared Exclude from Validation Rules'
                    };
            */
                    
                     /*for(PermissionSetAssignment psa : [SELECT Assigneeid, PermissionSet.Label FROM PermissionSetAssignment ])
                     {
                         isDeletePermissible = false;
                         
                         if(psa.PermissionSet.Label =='Shared Exclude from Validation Rules'     &&  psa.AssigneeId == UserInfo.getUserId())
                         {
                             this.isDeletePermissible = true;
                         }
                         
                     }*/
                // Vikas: Since we are using Custom Permissions, this is the right way to validate
                isDeletePermissible = false;
                List<CustomPermission> customPermissions = [SELECT Id, MasterLabel FROM CustomPermission WHERE MasterLabel = 'Shared Exclude from Validation Rules'];
                if (null != customPermissions && customPermissions.size() > 0){
                    List<SetupEntityAccess> setupEntities = [SELECT SetupEntityId FROM SetupEntityAccess WHERE SetupEntityId = :customPermissions[0].Id AND ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment
                    WHERE AssigneeId = :UserInfo.getUserId())];
                    if (null != setupEntities && setupEntities.size() > 0){
                        isDeletePermissible = true;
                    }
                }
        }               
        
    } 

    public override void beforeInsert(sObject obj){
        Task newTask = (Task)obj;
         manager.taskDivisionDetailInsert(newTask);
         //manager.createTasksOnPatients(newTask);

        //default to false on new, only check on update
        //newTask.Make_Me_Owner__c = false;
    }

    /**
    * @desc before Update method
    */
    public override void beforeUpdate(sObject oldObj, sObject newObj){
        Task oldTask = (Task)oldObj;
        Task newTask = (Task)newObj;
       // TaskManager.makeUserTaskOwner(oldTask, newTask);
        manager.taskDivisionDetailUpdate(oldTask, newTask);

    }
    
    public override void beforeDelete(sObject obj){
        
        if(!this.isDeletePermissible){
            obj.addError(Label.Can_t_Delete_Activities);
        }
        
    } 
    
    public override void afterInsert(sObject obj){
            
        
    }
    
    public override void andFinally(){
      //  manager.commitTasksOnPatients();
    }
}
/*
 @CreatedDate     20JUNE2016                                  
 @ModifiedDate    20JUNE2016                                  
 @author          Mayuri-Accenture
 @Description     Q3 2016 - US2489 - Batch Apex class to update Territory Manager for all the Patient Cases associated to the Accounts whose territory has been changed/re aligned
 @Methods         batchAccountTerritoryUpdate 
 @Requirement Id  
 */
 
 global class BatchPatientCaseTMUpdate implements Database.Batchable<sObject> { 
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Shared_Patient_Case__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_WATCHMAN = RECTYPES.get('Watchman Patient Case').getRecordTypeId();
    global Database.QueryLocator start(Database.BatchableContext BC) {
    Date dt;
    if(Test.isRunningTest()){   
        dt = system.today();
    }
    else{
        dt = system.today()-Integer.valueOf(system.Label.ETM_Batch_Integer_Subtractor_For_Date);
    }
    String query =  'select ' +
                    'Id, '+
                    'ObjectId, '+
                    'Territory2Id, '+
                    'AssociationCause, '+
                    'SobjectType, '+
                    'LastModifiedDate '+
                    'FROM ObjectTerritory2Association ';
       if(Test.isRunningTest()){            
               query += 'WHERE LastModifiedDate >=: dt';
       }
       else{
             query += 'WHERE LastModifiedDate >: dt';
       }      
       system.debug('--query--'+query);      
       return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<ObjectTerritory2Association> scope) {
    
        set<Id> acctIdSet = new set<Id>();
        map<Id,List<Id>> acctTerritoryIdMap = new map<Id,List<Id>>();
        list<Shared_Patient_Case__c> patientCaseLst = new list<Shared_Patient_Case__c>();
        map<Id,list<Shared_Patient_Case__c>> acctIdPCMap = new map<Id,list<Shared_Patient_Case__c>>();
        map<Id,Id> acctUserMap = new map<Id,Id>(); //map to store Account Id and corresponding Territory Manager(User) Id
        UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
        list<Shared_Patient_Case__c> updatePCLst = new list<Shared_Patient_Case__c>();
        
        system.debug('--scope--'+scope); 
        for(ObjectTerritory2Association ob : scope)
        {
            if(ob.ObjectId != null){
                acctIdSet.add(ob.ObjectId);                     //set of account Ids
                if(ob.Territory2Id != null){
                    if (acctTerritoryIdMap.get(ob.ObjectId) == null){
                        acctTerritoryIdMap.put(ob.ObjectId,new List<Id>());
                    }
                    acctTerritoryIdMap.get(ob.ObjectId).add(ob.Territory2Id);   //map to store account Id and territory Id
                }
            }
        }
        system.debug('@acctIdSet-->'+acctIdSet);
        system.debug('@acctTerritoryIdMap-->'+acctTerritoryIdMap);
        if(acctIdSet.size() > 0){
            patientCaseLst = [SELECT Id,Name,Shared_Facility__c,LAAC_Territory_Manager__c,Shared_Start_Date_Time__c       //query on patient case records for accounts whose territory has been changed
                              FROM Shared_Patient_Case__c
                              WHERE Shared_Facility__c IN: acctIdSet AND RecordTypeId =: RECTYPE_WATCHMAN AND Shared_Start_Date_Time__c >=: system.today()];
            if(patientCaseLst.size() > 0){
                for(Shared_Patient_Case__c pc : patientCaseLst){
                    if(acctIdPCMap.containsKey(pc.Shared_Facility__c)){
                        acctIdPCMap.get(pc.Shared_Facility__c).add(pc);
                    }
                    else{
                        acctIdPCMap.put(pc.Shared_Facility__c,new list<Shared_Patient_Case__c>{pc});     //create a map of account Id and list of its patient cases
                    }
                }
                
            }
        }
        system.debug('@acctIdPCMap-->'+acctIdPCMap);
        
        if(acctTerritoryIdMap != null && acctTerritoryIdMap.size() > 0){
            acctUserMap = util.getAcctUserMap(acctTerritoryIdMap);          
        }
        
        if(acctIdPCMap != null && acctIdPCMap.size() > 0){
            for(Id acId : acctIdPCMap.keyset()){
                if(acctUserMap.keyset().contains(acId)){
                    for(Shared_Patient_Case__c pc : acctIdPCMap.get(acId)){
                        if(acctUserMap.get(acId) != null){
                            pc.LAAC_Territory_Manager__c = acctUserMap.get(acId);
                            updatePCLst.add(pc);
                        }
                    }
                }
            }
        }
        if(updatePCLst!= null && updatePCLst.size() > 0){
            Database.SaveResult[] objUpdateResult = Database.update(updatePCLst,TRUE);
                      
                // Error handling code Iterate through each returned result
                for (Database.SaveResult er : objUpdateResult) {
                    if (er.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully updated the Record. Record ID: ' + er.getId());                        
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : er.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Record fields that affected this error: ' + err.getFields());
                        }
                    }
                }     
        }
    }
    global void finish(Database.BatchableContext BC) {
    }
}
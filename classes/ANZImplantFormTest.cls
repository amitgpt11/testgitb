/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class ANZImplantFormTest {
  final static String LOT1 = 'LOT1';
  final static String MODEL123 = 'MODEL123';
  final static String SERIAL1 = '11111';
  final static String SERIAL2 = 'SERIAL2';
  final static String CCRANAME = 'CCRANAME';
  final static String SURGERYNAME = 'SURGERYNAME';
  final static String UPNMATERIALNUM = '1234567';
  final static String INVENLOCATION = 'LOC123';
    final static String ACCTNAME ='TestAccount';
  final static Date EXPIRATION = System.today().addDays(10);
  public static  Account acct = new account();
  public static     Product2 prod  = new Product2();
  public static     Inventory_Item__c invItem  = new Inventory_Item__c();
  public static  Inventory_Transaction__c invTrans   = new Inventory_Transaction__c();
     public static     Inventory_Location__c invLoc = new Inventory_Location__c ();
  
  //@TestSetup
  static Inventory_Location__c setupInventoryLocation(){
   

        // inventory tree
        Inventory_Data__c invData = new Inventory_Data__c(
            Account__c = acct.Id,
            Customer_Class__c = 'YY'
        );
        insert invData;

        invLoc = new Inventory_Location__c(
            Inventory_Data_ID__c = invData.Id,
            Name = INVENLOCATION
        );
        insert invLoc;
        return invLoc;
  }

  static Order setupOrder() {

        NMD_TestDataManager td = new NMD_TestDataManager();

      

        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
            user0, user1
        };

            Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
            insert hierarchy;

            //create two assignees
            Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
            assignee0.Rep_Designation__c = 'TM1';
            Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
            assignee1.Rep_Designation__c = 'TM2';
            insert new List < Assignee__c > {
                assignee0
            };


        
            acct= td.newAccount(ACCTNAME);  

            insert acct;

        
        
 //   insert acct;
    system.debug('___acct____'+acct);
            
      Account acc= [Select Id, Name From Account where Name =:ACCTNAME];
      
      

    // product
     prod = td.newProductANZ('Lead');
    prod.Model_Number__c = MODEL123;
    prod.EAN_UPN__c = MODEL123;
    prod.UPN_Material_Number__c = UPNMATERIALNUM;
    system.debug('prod details===>'+prod);
    insert prod;
    system.debug('after prod details===>'+prod);

    invLoc =setupInventoryLocation();

     invItem = new Inventory_Item__c(
      Inventory_Location__c = invLoc.Id,
      Model_Number__c = prod.Id,
      Serial_Number__c = SERIAL1,
      Lot_Number__c = LOT1,
      Expiration_Date__c = EXPIRATION,
      SAP_Quantity__c = 1
    );
    insert invItem;

     invTrans = new Inventory_Transaction__c(
      Inventory_Item__c = invItem.Id,
      Disposition__c = 'In Transit',
      Quantity__c = 1      
      );
    insert invTrans;

        
    Cycle_Count_Response_Analysis__c ccra = td.newCycleCountResponseAnalysis();
        ccra.OwnerId=user1.Id;
    insert ccra;
        system.debug('ccra'+ccra);
    
          system.debug('___acct____'+acc);
          system.debug('___prod____'+prod);
          system.debug('invLoc______'+invLoc);
            //create oppty
            Opportunity oppty = td.newOpportunityANZ(acc.Id);
            oppty.Territory_Id__c = hierarchy.Id;
        //    oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
            
            insert oppty;
             system.debug('___oppty____'+oppty);
   
        // create two products, pricebook, and entries
      Id pricebookId = Test.getStandardPricebookId();
//ANZ Implant Devices
      Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
     
     system.debug(' pb details===>'+pb);
        insert pb;
        
system.debug('after  pb details===>'+pb);


      PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
            //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
          PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
          system.debug(' standardPrice0 details===>'+standardPrice0);
          insert new List<PricebookEntry> { standardPrice0 };
system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        
            system.debug(' After pbe0 details===>'+pbe0);
            
      Order ord = new Order(
                AccountId = acc.id,
                Shipping_Location__c=invLoc.id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today(),
                Pricebook2Id = pb.Id,
                Status = 'Draft',
                RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
              //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
                Stage__c = 'Pending'
      );
      system.debug(' Order details===>'+ord);
        insert ord;
        
        system.debug('After Order details===>'+ord);
       OrderItem ordItem = new OrderItem(
          OrderId=ord.id,
            Material_Number_UPN__c=UPNMATERIALNUM,
            Serial_Number__c = SERIAL1,
            PricebookEntryId = pbe0.Id,
            Quantity=1,
            UnitPrice = 1.0
            //Product = p2.id
        );
        system.debug('ordItem details===>'+ordItem);
      insert ordItem; 
      system.debug('After ordItem details===>'+ordItem);
      return ord;
        //}
    
    }
  

//for ANZImplantFormExtension  

static TestMethod void TestANZImplantFormExtension(){ 
Test.StartTest();
    Order olist= setupOrder();    
    system.debug('____olist____'+olist);
    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
Test.StopTest();
}  

static TestMethod void TestANZImplantFormExtension1(){ 
   
Test.StartTest();

ANZ_TestDataManager anzobj = new  ANZ_TestDataManager();
    Order olist1= ANZ_TestDataManager.setupOrder1(); 
    system.debug('olist1+++'+olist1);
    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
Test.StopTest();
}  

static TestMethod void TestANZImplantFormExtension2(){ 
   
Test.StartTest();

ANZ_TestDataManager anzobj = new  ANZ_TestDataManager();
    Order olist1= ANZ_TestDataManager.setupOrder2(); 
    system.debug('olist1+++'+olist1);
    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
Test.StopTest();
}  

static TestMethod void TestANZImplantFormExtension3(){ 
   
Test.StartTest();

ANZ_TestDataManager anzobj = new  ANZ_TestDataManager();
    Order olist1= ANZ_TestDataManager.setupOrder4(); 
    system.debug('olist1+++'+olist1);
    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
    
    List <OrderItem> otlobj = [Select id,OrderId,Product_Name__c,PricebookEntryId,UnitPrice,Quantity,Serial_Number__c,Model_Number__c, ANZ_Batch_Number__c FROM OrderItem where OrderId = : olist1.id];
    system.debug('__v__otlobj__'+otlobj);
   // classInstanat.ShoppingCartContents = 2;
   // classInstanat.itemModelNumber=otlobj[0].Model_Number__c ;
   //classInstanat.itemSerialNumber=otlobj[0].Serial_Number__c ;
   //classInstanat.itemBatchNumber=otlobj[0].ANZ_Batch_Number__c ;
    classInstanat.ShoppingCartContents = otlobj;
    classInstanat.addToCart(otlobj);

    classInstanat.AccessoryItemListing =otlobj;
    classInstanat.accessoriesAddToCart();
    classInstanat.ExplantItemListing =otlobj;
    classInstanat.LAACItemListing =otlobj;
    classInstanat.getCartContents();
         classInstanat.shoppingCart=otlobj;
    classInstanat.devicesAddToCart();
    classInstanat.explantsAddToCart();
    classInstanat.LAACAddToCart();
        classInstanat.submitForm();
        classInstanat.PopulateOpptyInfo();


    classInstanat.doesExistInCart(otlobj[0].Serial_Number__c,otlobj[0].Model_Number__c);
    classInstanat.deleteCartItem();
        classInstanat.ClearCart();
Test.StopTest();
}  
static TestMethod void TestANZImplantFormExtension4(){ 
   
Test.StartTest();

ANZ_TestDataManager anzobj = new  ANZ_TestDataManager();
    Order olist1= ANZ_TestDataManager.setupOrder5(); 
    system.debug('olist1+++'+olist1);
    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
    
    List <OrderItem> otlobj = [Select id,OrderId,Product_Name__c,PricebookEntryId,UnitPrice,Quantity,Serial_Number__c, Model_Number__c, ANZ_Batch_Number__c FROM OrderItem where OrderId = : olist1.id];
    system.debug('__v__otlobj__'+otlobj);
    //classInstanat.itemModelNumber=otlobj[0].Model_Number__c ;
    //classInstanat.itemSerialNumber=otlobj[0].Serial_Number__c ;
    //classInstanat.itemBatchNumber=otlobj[0].ANZ_Batch_Number__c ;
   // classInstanat.ShoppingCartContents = 2;
    classInstanat.ShoppingCartContents = otlobj;
    classInstanat.addToCart(otlobj);
    classInstanat.AccessoryItemListing =otlobj;
      classInstanat.accessoriesAddToCart();
      classInstanat.shoppingCart=otlobj;
    classInstanat.ExplantItemListing =otlobj;
    classInstanat.LAACItemListing =otlobj;
    classInstanat.getCartContents();
    classInstanat.devicesAddToCart();
    classInstanat.explantsAddToCart();
    classInstanat.LAACAddToCart();
        classInstanat.submitForm();
        classInstanat.PopulateOpptyInfo();
    

    classInstanat.doesExistInCart(otlobj[0].Serial_Number__c,otlobj[0].Model_Number__c);
    classInstanat.deleteCartItem();
        classInstanat.ClearCart();

Test.StopTest();
}  




/*
static TestMethod void TestANZImplantFormExtension3(){ 
   
     
Test.StartTest();

  Order olist1= ANZ_TestDataManager.setupOrder4(); 
    system.debug('olist1+++'+olist1);
    
       OrderItem ordItemInst = ANZ_TestDataManager.setupOrderItem1(); 
    List<OrderItem> odlList = new List<OrderItem> ();
       odlList.add(ordItemInst);
       

    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
  classInstanat.itemSerialNumber='SERIAL1';
    classInstanat.ClearCart();
    classInstanat.doesExistInCart('SERIAL1');
    
    classInstanat.deleteCartItem();
    
 

    classInstanat.addToCart(odlList);
    classInstanat.devicesAddToCart();
    classInstanat.accessoriesAddToCart();
    classInstanat.explantsAddToCart();
    classInstanat.LAACAddToCart();
    classInstanat.clearCartAction();
   // classInstanat.checkRequired1();
    classInstanat.overrideRequired();
    classInstanat.getCartContents(); 
    //classInstanat.shoppingCart()
Test.StopTest();
}  */



/*
static TestMethod void TestANZImplantFormExtension3(){ 
   
Test.StartTest();

ANZ_TestDataManager anzobj = new  ANZ_TestDataManager();
    Order olist1= ANZ_TestDataManager.setupOrder3(); 
    system.debug('olist1+++'+olist1);
    PageReference pageRef = Page.ANZImplantForm;
    //pageRef.getParameters().put('id', olist.id);   // URL parameter "id"
    Test.setCurrentPage(pageRef);
    ANZImplantFormExtension classInstanat = new ANZImplantFormExtension();
    classInstanat.implantForm = olist1;
    classInstanat.submitForm();

    //classInstanat.PopulateOpptyInfo();
  //  insert     olist1;
Test.StopTest();
}  
*/

}
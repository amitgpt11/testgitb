/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/sendUserPincode/*')
global class RestSendUserPincode {
    global RestSendUserPincode() {

    }
    @HttpPost
    global static openq.OpenMSL.SEND_USER_PINCODE_RESPONSE_element sendUserPincode(openq.OpenMSL.SEND_USER_PINCODE_REQUEST_element request_x) {
        return null;
    }
}

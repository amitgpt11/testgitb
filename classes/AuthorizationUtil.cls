public abstract class AuthorizationUtil {   
                                        
    public Map<String,String> mapLocaleToLanguage{
        get {
            return new Map<String, String> {
                'fr_FR' => 'French',
                'fr_BE' => 'French',
                'fr_FR_EURO' => 'French',
                'fr_LU' => 'French',
                'fr_CH' => 'French', 
                'de_AT_EURO' => 'German',
                'de_AT' => 'German',
                'de_DE_EURO' => 'German',
                'de_DE' => 'German',
                'de_LU_EURO' => 'German',
                'de_LU' => 'German',
                'de_CH' => 'German',
                'sv_SE' => 'Swedish',
                'nl_NL' => 'Dutch',
                'nl_BE' => 'Dutch',
                'fr' => 'French', 
                'de' => 'German',
                'sv' => 'Swedish',
                'nl' => 'Dutch',
                'en_US' => 'English'

            };
        }
        set;
    }

    public PageReference init() {
    
        //if usertype is not GUEST and requested page is RequestAccount redirect it to ContactUs
        if(ApexPages.currentPage().getUrl().contains('RequestAccount') && UserInfo.getUserType() != 'Guest'){
           
                return new PageReference('/ContactUs');
        }
        
        // Redirect to UnAuthenticatedHome Page if the user is 'Guest' 
        if(UserInfo.getUserType() == 'Guest' && !ApexPages.currentPage().getUrl().contains('RequestAccount')){
            
            return new PageReference('/UnAuthenticatedHomePage');
        }
        
        fetchRequestedData();
        
        return null;
    }
    
    public abstract void fetchRequestedData();    
   
}
/**
* Test Class for the TriggerHandler methods for UserAssignmentTriggerHandler class.       
*
* @CreatedBy   Payal Ahuja
* @CreatedDate 28/09/2016
*/
@isTest(SeeAllData=false)
public class UserAssignmentTriggerHandlerTest {
   public static testmethod void createUserAssignmentTestMethod() {
     

    //User sysAdmin=UtilForUnitTestDataSetup.getUser();
    
    System.runAs((new User(Id = UserInfo.getUserId()))){
    
     //insert Account records
     Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
     Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
     list<Account> alist= new List<Account>{a1, a2};
     insert alist;
            
    
     //Prepare Users as an Owner to Account
     User u1 = UtilForUnitTestDataSetup.newUser();
     u1.Username = 'UserNameC@boston.com';
     u1.IsActive= True;
     User u2 = UtilForUnitTestDataSetup.newUser();
     u2.Username = 'UserNameR@boston.com';
     u2.IsActive= True;    
     List<User> ulist=new list<User>
     {u1,u2};
     insert ulist;
     
     //create user assignment records
     User_Assignment__c UserRec= new User_Assignment__c();
     UserRec.Account__c = a1.Id ;
     UserRec.User__c = u1.Id ;
     
     
Test.startTest();  
     try{
           if(Test.isRunningTest())
           insert UserRec;
        }  catch (Exception e){
                System.debug('The following exception has been caught by the UserAssignmentTriggerHandler.accessRecordstoUser ' + e);
         }

  
      //create Test Procedure Records
      Shared_Patient_Case__c primaryProcedure = new Shared_Patient_Case__c();
      primaryProcedure.Shared_Start_Date_Time__c = Datetime.now();
      primaryProcedure.Shared_End_Date_Time__c = Datetime.now() + (1/24) ;
      primaryProcedure.Shared_Facility__c = a1.Id ;
      insert primaryProcedure;
     
     //Create Procedure Share Table records
      Shared_Patient_Case__Share share = new Shared_Patient_Case__Share();
      share.UserOrGroupId = u1.Id;
      share.RowCause = 'Manual';
      share.AccessLevel = 'Read';
      share.ParentId = primaryProcedure.Id;
      insert share;
    
     //update user assignment records
     User_Assignment__c UserRec1= new User_Assignment__c();
     UserRec1.Account__c = a1.Id ;
     UserRec1.User__c = u2.Id ;
     update UserRec;
     
     //delete user assignment records
      User_Assignment__c UserRec2= new User_Assignment__c();
      UserRec1.Account__c = a1.Id ;
      UserRec1.User__c = u1.Id ;
      delete UserRec;
     
    Test.stopTest();
    }
    
   }
  
}
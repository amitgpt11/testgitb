/*
 @CreatedDate     4 Oct 2016                                  
 @author          Ashish-Accenture
 @Description     Create,update loaner kit relationship records based on Loaner kit Staging table object.
       
*/

public class batchLoanerKitRelUpdate implements Database.Batchable<sObject>, Schedulable, Database.stateful{
    
    // Schedulable method, executes the class instance
    
    Public void execute(SchedulableContext context) {
        Database.executeBatch(new batchLoanerKitRelUpdate(), 200);
    }
    
    Map<String,Uro_PH_Loaner_Kit_Staging_Table__c> mapLoanerKitStage;
    Map<String,Uro_PH_Loaner_Kit_Relationship__c > mapLoanerKitRel;
    Set<String> dupcheckSet;
    
    public batchLoanerKitRelUpdate(){
        mapLoanerKitStage = new Map<String,Uro_PH_Loaner_Kit_Staging_Table__c>();
        mapLoanerKitRel = new Map<String,Uro_PH_Loaner_Kit_Relationship__c >();
        dupcheckSet = new Set<String>();
    }
    //Start method, fetch user records from public group 
    public Database.QueryLocator start(Database.BatchableContext BC) {
       
       String query = 'Select id, Uro_PH_Loaner_Kit_Id__c,name, Uro_PH_Product_Id__c, Uro_PH_Quantity__c from Uro_PH_Loaner_Kit_Staging_Table__c order by Uro_PH_Loaner_Kit_Id__c';
        return database.getquerylocator(query);
    }
    public void execute(Database.BatchableContext BC, List<Uro_PH_Loaner_Kit_Staging_Table__c> scope) {
        Set<string> setLoanerKitId = new Set<string>();
        Set<string> setProductId = new Set<string>();
        list<Uro_PH_Loaner_Kit_Staging_Table__c> lstStageTableToDelete = new list<Uro_PH_Loaner_Kit_Staging_Table__c>();
        for(Uro_PH_Loaner_Kit_Staging_Table__c stageTable :scope){
            if(String.IsNotBlank(stageTable.Uro_PH_Loaner_Kit_Id__c) && String.IsNotBlank(stageTable.Uro_PH_Product_Id__c)){
                mapLoanerKitStage.put(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()+stageTable.Uro_PH_Product_Id__c.tolowerCase(),stageTable);
                setLoanerKitId.add(stageTable.Uro_PH_Loaner_Kit_Id__c);
                setProductId.add(stageTable.Uro_PH_Product_Id__c);
                lstStageTableToDelete.add(stageTable);
            }
        }
        
        list<Uro_PH_Loaner_Kit_Relationship__c> lstLoanerKitRel = [Select id,Uro_PH_Loaner_Kit__c,Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c , Uro_PH_Product__c,Uro_PH_Product__r.UPN_Material_Number__c,Uro_PH_Quantity__c from Uro_PH_Loaner_Kit_Relationship__c
                                                                            where Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c in: setLoanerKitId];
        
        Map<String,Product2> mapLKId = new Map<String,Product2>();
        Map<String,Product2> mapPrdId = new Map<String,Product2>();

        list<Product2> lstProducts = [Select id,UPN_Material_Number__c,Uro_Ph_Loaner_Kit_Id__c from Product2 where Uro_Ph_Loaner_Kit_Id__c in :setLoanerKitId OR UPN_Material_Number__c in :setProductId];
        for(Product2 prd: lstProducts ){
            if(String.IsNotBlank(prd.Uro_Ph_Loaner_Kit_Id__c)){
                mapLKId.put(prd.Uro_Ph_Loaner_Kit_Id__c.tolowercase(),prd);
            }
            if(String.IsNotBlank(prd.UPN_Material_Number__c)){
                mapPrdId.put(prd.UPN_Material_Number__c.tolowercase(),prd);
            }
        }
       
        list<Uro_PH_Loaner_Kit_Relationship__c> lstToUpsert = new list<Uro_PH_Loaner_Kit_Relationship__c>();
        
        Uro_PH_Loaner_Kit_Relationship__c objLKRel;
        
        
        for(Uro_PH_Loaner_Kit_Relationship__c LKrel : lstLoanerKitRel){
            mapLoanerKitRel.put(LKrel.Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c.tolowercase()+LKrel.Uro_PH_Product__r.UPN_Material_Number__c.tolowercase(),LKrel);
          
        } 
        
        
        if(mapLKId != null && mapPrdId != null){
            for(Uro_PH_Loaner_Kit_Staging_Table__c stageTable :scope){
                System.debug('==stageTable.Uro_PH_Loaner_Kit_Id__c====='+stageTable.Uro_PH_Loaner_Kit_Id__c+'===stageTable.Uro_PH_Product_Id__c==='+stageTable.Uro_PH_Product_Id__c);
                System.debug('====dupcheckSet==='+dupcheckSet.Contains(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()+stageTable.Uro_PH_Product_Id__c.tolowercase()));
                
                if(String.IsNotBlank(stageTable.Uro_PH_Loaner_Kit_Id__c) &&  String.IsNotBlank(stageTable.Uro_PH_Product_Id__c) && 
                    dupcheckSet.add(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()+stageTable.Uro_PH_Product_Id__c.tolowercase())){
                    
                  
                    objLKRel = new Uro_PH_Loaner_Kit_Relationship__c();
                    if(mapLoanerKitRel != null && mapLoanerKitRel.get(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()+stageTable.Uro_PH_Product_Id__c.tolowerCase())!= null){ 
                        objLKRel.Id = mapLoanerKitRel.get(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()+stageTable.Uro_PH_Product_Id__c.tolowerCase()).Id;     
                    }
                    objLKRel.Uro_PH_Quantity__c = stageTable.Uro_PH_Quantity__c;
                    if(mapLKId.get(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()) != null){
                        objLKRel.Uro_PH_Loaner_Kit__c =  mapLKId.get(stageTable.Uro_PH_Loaner_Kit_Id__c.tolowercase()).id; 
                    }
                    if(mapPrdId.get(stageTable.Uro_PH_Product_Id__c.tolowerCase()) != null){
                        objLKRel.Uro_PH_Product__c =  mapPrdId.get(stageTable.Uro_PH_Product_Id__c.tolowerCase()).id; 
                    }
                    lstToUpsert.add(objLKRel);
                }
            }
        }
        
        System.debug('====lstToUpsert===='+lstToUpsert);
        if(lstToUpsert.size() > 0){
            try{
                Database.upsertResult[] results = Database.upsert(lstToUpsert,false);
                
            }Catch(exception e){
                System.debug('####====e=='+e);
            }
            if(lstStageTableToDelete.size() > 0){
                try{    
                    Database.Delete(lstStageTableToDelete,false);
                }catch(exception e){
                    System.debug('###==e==='+e);
                }
            }
        }
                               
    }
    public void finish(Database.BatchableContext BC){
        System.debug('===mapLoanerKitStage==='+mapLoanerKitStage);
        Database.executeBatch(new batchLoanerKitRelDelete(mapLoanerKitStage), 200);
    }
}
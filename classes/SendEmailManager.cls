/**
* Utility Class for sending template-style emails
*
* @Author salesforce Services
* @Date 03/01/2015
*/
//without sharing is to ensure the recipientId/targetObjectId is available
public without sharing class SendEmailManager {

	private Id recipientId;
	private Id whatId;
	private EmailTemplate template;
	private String plainEmailAddress;
	private String plainSubject;
	private String plainTextBody;
	private List<String> ccAddresses;
	private List<Messaging.EmailFileAttachment> attachments;

	public SendEmailManager(EmailTemplate template, Id recipientId) {
		this();
		this.template = template;
		this.recipientId = recipientId;
	}

	public SendEmailManager(EmailTemplate template, Id recipientId, Id whatId) {
		this();
		this.template = template;
		this.recipientId = recipientId;
		this.whatId = whatId;
	}

	public SendEmailManager(String subject, String body, String emailAddress) {
		this();
		this.plainSubject = subject;
		this.plainTextBody = body;
		this.plainEmailAddress = emailAddress;
	}

	public SendEmailManager() {
		this.template = null;
		this.recipientId = null;
		this.whatId = null;
		this.plainEmailAddress = '';
		this.plainSubject = '';
		this.plainTextBody = '';
		this.ccAddresses = new List<String>{};
		this.attachments = new List<Messaging.EmailFileAttachment>{};
	}

	public void addAttachment(Attachment attch) {
		addAttachment(attch.Name, attch.Body);
	}

	public void addAttachment(String name, Blob body) {
		Messaging.EmailFileAttachment file = new Messaging.EmailFileAttachment();
        file.setFileName(name);
        file.setBody(body);
        this.attachments.add(file);
	}

	public void addCCAddress(String email) {
		this.ccAddresses.add(email);
	}

	public Messaging.Email generateEmail() {

		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setFileAttachments(this.attachments);
        message.setCCAddresses(this.ccAddresses);
        message.setSaveAsActivity(false);
        if (this.template != null) {
        	message.setTemplateId(this.template.Id);
        	message.setTargetObjectId(this.recipientId);
        	if (this.whatId != null && this.recipientId.getSObjectType() == Contact.SObjectType) {
        		message.setWhatId(this.whatId);
        	}
        }
        else {
        	message.setSubject(this.plainSubject);
        	message.setPlainTextBody(this.plainTextBody);
        	message.setToAddresses(new List<String> { this.plainEmailAddress });
        }

        return message;

	}

	public Messaging.SendEmailResult sendEmail() {

		Messaging.Email message = generateEmail();

        //send/verify email
        Messaging.SendEmailResult result = Messaging.sendEmail(new List<Messaging.Email> { message })[0];

        return result;

	}

}
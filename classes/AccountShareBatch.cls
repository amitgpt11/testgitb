/**
 * Class Name : AccountShareBatch
 * @Description : Batch to to update Opportunity, Contact and Case level access back to Private on Account Share 
 *                if a user manually adds an Account Team member with either Read or Read/Write access.
 * @Date : 9/19/2016
 **/
global class AccountShareBatch implements Database.Batchable<sObject>
{
   private static final String READ_ACCESS = 'Read';
   private static final String EDIT_ACCESS = 'Edit'; 
   private static final String TEAM_ROWCAUSE = 'Team';
   
   private static final String SYSTEM_ADMIN_PROFILE ='System Administrator';
   private static final String API_USER_PROFILE = 'API User Profile'; 
   private static final String SUPPORT_ADMIN_PROFILE = 'Support Admin';
   
   private Set<String> restrictedAccesses = new Set<String>{READ_ACCESS,EDIT_ACCESS};
   private Set<String> adminProfiles = new Set<String>{SYSTEM_ADMIN_PROFILE,SUPPORT_ADMIN_PROFILE,API_USER_PROFILE};
   /**
    *Method Name : Start
    *@param : BatchableContext
    *@Description : Collects AccountShare data having Rowcause equal to Team and read/edit opportunity/contact/case access level.    
    **/
   global Database.QueryLocator Start(Database.BatchableContext BC)
   {
      System.debug('in start');
      String query = 'select id,'+
                             'UserOrGroupId,'+
                             'AccountAccesslevel,'+
                             'OpportunityAccesslevel,'+
                             'ContactAccessLevel,'+
                             'CaseAccessLevel,'+
                             'Rowcause'+' '+
                             'From AccountShare'+' '+
                             'where Rowcause=:TEAM_ROWCAUSE '+
                             'AND (OpportunityAccesslevel In: restrictedAccesses '+
                             'OR ContactAccessLevel In: restrictedAccesses '+
                             'OR CaseAccessLevel In:restrictedAccesses) AND LastmodifiedBy.profile.Name NOT IN : adminProfiles ';
       System.debug('Query:'+query);
      return Database.getQueryLocator(query);
   }
   /**
    *Method Name : execute
    *@param : BatchableContext, List<sObject>
    *@Description : Procees data collected in start method to Updates opportunity/contact/case access level to private(None)   
    **/
   global void execute(Database.BatchableContext BC, List<sObject> scope)
   {
       System.debug('in execute:'+BC+':'+(List<AccountShare>)scope);
       List<AccountShare> accShareList = new List<AccountShare>();      
       System.debug('accessLevel :'+ restrictedAccesses );
       try{           
          if(!scope.isEmpty()){
                   for(AccountShare accShare : (List<AccountShare>)scope)
                   {
                       //Check for read/edit OpportunityAccessLevel
                       if(restrictedAccesses.contains(accShare.OpportunityAccessLevel)){
                         accShare.OpportunityAccessLevel = 'None';
                       }
                       //Check for read/edit ContactAccessLevel
                       if(restrictedAccesses.contains(accShare.ContactAccessLevel)){
                         accShare.ContactAccessLevel = 'None';
                       }
                       //Check for read/edit CaseAccessLevel
                       if(restrictedAccesses.contains(accShare.CaseAccessLevel)){
                         accShare.CaseAccessLevel = 'None';
                       }
                       
                       //accountshare objects to be updated
                       accShareList.add(accShare); 
                   }
                   if(!accShareList.isEmpty()){                   
                   //updates accountshare object
                   update accShareList;}
           }
       }
       catch(Exception e)
       {
          System.debug('Exception:'+e);
       }
   }
   /**
    *Method Name : finish
    *@param : BatchableContext
    *@Description :Finishes batch execution   
    **/
   global void finish(Database.BatchableContext BC)
   {
       AsyncApexJob a = [Select Id, 
                                Status, 
                                NumberOfErrors, 
                                JobItemsProcessed,
                                TotalJobItems, 
                                CreatedBy.Email, 
                                ExtendedStatus
                                from AsyncApexJob where id = :BC.getJobId()]; 
      System.debug('Status:'+a.status);

   }
}
/**
* Class for initializing and executing TriggerHandler methods.       
*
* @Author salesforce Services
* @Created 01/30/2014
* 
*/

public with sharing class TriggerHandlerManager {

    // track each update trigger's unique set of records.  this is used to determine if the same
    // trigger is running more than once per invocation per distinct set of records.
    static final Set<Set<Id>> before_update_keysets = new Set<Set<Id>>();
    static final Set<Set<Id>> after_update_keysets = new Set<Set<Id>>();

    // if set to false then the trigger will not attempt to defer/flush any application logs
    // generated while in the context of the trigger
    public static Boolean ALLOW_LOGGING = true;

    static Integer trigger_depth = -1;
    
    //Vikas: Variable to store the list of disallowed trigger hanlders
    private static Set<String> disallowedHandlers = new Set<String>();
    
    // Vikas: Adding a static block so that the number of queries is reduced for fetching Trigger Status custom metadata in the same context
    static{
        for (Trigger_Status__mdt   triggStatus : [SELECT DeveloperName,Status__c FROM Trigger_Status__mdt]){
            disallowedHandlers.add(triggStatus.DeveloperName);
        }
    }
    /**
    * This method clears out the hash sets to help out unit tests. 
    *
    */
    public static void clearTriggerHashes() {
        before_update_keysets.clear();
        after_update_keysets.clear();
    }

    /**
    * This method creates and executes a trigger handler. 
    * 
    * @param t - Type
    */
    public static void createAndExecuteHandler(Type t) {
        
        // Vikas 09/16 - Adding logic for Kill Switch. Test if this Trigger needs to be executed or not
        System.debug('----- t ---'+t.getName());
        
        if (isOkayToRun(t.getName())){
            System.debug('----- '+t.getName()+' Getting executed ---');
        // Get a handler appropriate to the object being processed
            ITriggerHandler handler = getHandler(t);
        // Execute the handler to fulfil the trigger
            execute(handler);
        }
    }
    
    
    public static Boolean isOkayToRun(String className){
        System.debug('----- disallowedHandlers ---'+disallowedHandlers);
        if (disallowedHandlers != null && !disallowedHandlers.isEmpty() && disallowedHandlers.contains(className)){
            return false;
        }else{
        return true;
        }
    }
 
   
    /**
    * This method invokes the TriggerHandler methods . 
    * 
    * @param handler - TriggerHandler
    */
    
    private static void execute(ITriggerHandler handler) {

        // prevent before/after update run-offs
        if (Trigger.isUpdate) {
            
            Set<Id> keyset = Trigger.newMap.keySet();
            if ((Trigger.isBefore && !before_update_keysets.add(keyset))
             || (Trigger.isAfter && !after_update_keysets.add(keyset))
            ) {
                return;
            }

        }

        // keep track of the current depth of the trigger.  since additional child triggers may
        // be invoked from the first trigger.
        ++trigger_depth;

        // only handle logging from within the trigger if the calling context has not already
        // deferred logging and _only_ if this is the root level trigger
        if (ALLOW_LOGGING && trigger_depth == 0) {
            DML.deferLogs();
        }

        // Before Trigger
        if (Trigger.isBefore) {
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkBefore();
 
            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete) {
                for (SObject so : Trigger.old) {
                    handler.beforeDelete(so);
                }
            } 
            else if (Trigger.isInsert) {
                
                if(Trigger.new != null && !Trigger.new.isEmpty()){
                    System.debug('Trigger Load Size:'+trigger.new.size());
                }
                
                for (SObject so : Trigger.new) {
                    handler.beforeInsert(so);
                }
            } 
            else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        } 
        else {
            // Call the bulk after to handle any caching of data and enable bulkification
            handler.bulkAfter();
 
            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete) {
                for (SObject so : Trigger.old) {
                    handler.afterDelete(so);
                }
            } 
            else if (Trigger.isInsert) {
                for (SObject so : Trigger.new) {
                    handler.afterInsert(so);
                }
            } 
            else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.afterUpdate(so, Trigger.newMap.get(so.Id));
                }
            } 
            else if(Trigger.isUndelete) {
                for (SObject so : Trigger.new) {
                    handler.afterUndelete(so);
                }
            }
        }
 
        // Perform any post processing
        handler.andFinally();

        // only handle logging from within the trigger if the calling context has not already
        // deferred logging and _only_ if this is the root level trigger
        if (ALLOW_LOGGING && trigger_depth == 0) {
            DML.flushLogs(false);
        }

        --trigger_depth;

    }
 
   
    /**
    * Get the named handler depending upon sObject Type . 
    * 
    * @param t - Type
    */
    
    private static ITriggerHandler getHandler(Type t) {
        
        // Instantiate the type
        Object o = t.newInstance();
 
        // if its not an instance of ITrigger return null
        if (!(o instanceOf ITriggerHandler)) {
            throw new TriggerHandlerException('No TriggerHandler named ' + t.getName());
        }
 
        return (ITriggerHandler)o;

    }
    
    public class TriggerHandlerException extends Exception{}

}
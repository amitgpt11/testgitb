/**
* NMD_TestDataManager.class
*
* @author   Salesforce services
* @date   02/06/2014
* @desc   Creates test data for the NMD code-base.
*/
@isTest
public class NMD_TestDataManager {

  static final String TEMPLATEACCOUNT = 'NMD_Quincy_Prospect_Report';
  static final String TEMPLATECONTACT = 'NMD_Quincy_Physician_Report';
  static final String NEUROMOD_ROLE_DEV_NAME = 'Neuromod_US_Test_Garbage123';//to ensure this user never exists in the system, ever.

  private Integer PersonnelIDCount = 0;

  public NMD_TestDataManager() { }

  public void setupQuincyContact() {

    insert new QuincyReportSettings__c(
      SetupOwnerId = UserInfo.getOrganizationId(),
      Account_EmailTemplate_DeveloperName__c = TEMPLATEACCOUNT,
      Contact_EmailTemplate_DeveloperName__c = TEMPLATECONTACT,
      Quincy_Email__c = 'quincy@email.test',
      BSN_Email__c = 'bsn@email.test'
    );
  }

  public void setupNuromodUserRoleSettings(){
     
    insert new Neuromod_UserRole__c(
      SetupOwnerId = UserInfo.getOrganizationId(),
      Developer_Name__c = NEUROMOD_ROLE_DEV_NAME
    );

    //commeting this code to SAve an SQOL - DIPIL 
/*
    try{
      UserRole root = [select Name, ParentRoleId from UserRole where DeveloperName = :NEUROMOD_ROLE_DEV_NAME];
    } catch (QueryException e){
      System.debug('***Query QueryException' + e);
        System.runAs(new User(Id = UserInfo.getUserId())) {
        insert new UserRole(
          Name = NEUROMOD_ROLE_DEV_NAME,
          DeveloperName = NEUROMOD_ROLE_DEV_NAME
        );  
      }
    }
    */
    System.runAs(new User(Id = UserInfo.getUserId())) {
        insert new UserRole(
          Name = NEUROMOD_ROLE_DEV_NAME,
          DeveloperName = NEUROMOD_ROLE_DEV_NAME
        );  
      }

  }

  public void setupLeadPreformSettings() {
    insert new NMD_Patient_Lead_Preform_Settings__c(
      SetupOwnerId = UserInfo.getOrganizationId(),
      NMD_Patient_RecordTypeId__c = ((String)LeadManager.RECTYPE_NMD_PATIENTS).substring(0, 15),
      Default_Company__c = 'Neuromod',
      Default_LastName__c = 'LName'
    );
  }

  public void createInventorySettings() {
    insert new NMD_Inventory_Settings__c(
      SetupOwnerId = UserInfo.getOrganizationId(),
      Return_Address__c = 'never never land',
      URI_Scheme__c = 'bscisynergy'
    );
  }

  public void setupCampaignMemberStatusSettings(){
    //Sort Order numbers need to start at 3, the system will create 1 and 2.
    //  If you try and use 1 and 2 they will not be inserted correctly
    NMD_DefaultCampaignStatus__c set1 = new NMD_DefaultCampaignStatus__c(
      Name = 'Test1',
      Default__c = true,
      Responded__c = false,
      Sort_Order__c = 3
    );
    NMD_DefaultCampaignStatus__c set2 = new NMD_DefaultCampaignStatus__c(
      Name = 'Test2',
      Default__c = false,
      Responded__c = true,
      Sort_Order__c = 4
    );
    NMD_DefaultCampaignStatus__c set3 = new NMD_DefaultCampaignStatus__c(
      Name = 'Test3',
      Default__c = false,
      Responded__c = true,
      Sort_Order__c = 5
    );

    insert new List<NMD_DefaultCampaignStatus__c>{ set1, set2, set3 };
  }

  public void createRestContext() {
    RestContext.response = new RestResponse();
    RestDispatcherV1 dispatcher = new RestDispatcherV1(null);
  }

  public UserRole setupUserRoleHierarchy() {
    return setupUserRoleHierarchy(NMD_UserRoleManager.NEUROMODUS);
  }

  public UserRole setupUserRoleHierarchy(String rootName) {

    List<UserRole> root = new List<UserRole>([select Id from UserRole where Name = :rootName or DeveloperName = :rootName]);

    UserRole rootRole = root.isEmpty()
      ? new UserRole(Name = rootName)
      : root[0];
    upsert rootRole;

    UserRole areaRole = new UserRole(
      Name = 'Area Role',
      ParentRoleId = rootRole.Id
    );
    insert areaRole;

    UserRole regionRole = new UserRole(
      Name = 'Region Role',
      ParentRoleId = areaRole.Id
    );
    insert regionRole;

    UserRole normalRole = new UserRole(
      Name = 'User Role',
      ParentRoleId = regionRole.Id
    );
    insert normalRole;

    return normalRole;

  }

  public Product2 newProduct() {
    return newProduct(randomString5());
  }

  public Product2 newProduct(String name) {
    return new Product2(
      Name = name,
      Description = name,
      RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('NMD').getRecordTypeId(),
      IsActive = true,
      EAN_UPN__c = randomString5(),
      ProductCode = randomString5(),
      UPN_Material_Number__c = randomString5()
    );
  }
  
  
  // create a product for ANZ 
    public Product2 newProductANZ() {
    return newProductANZ(randomString5());
  }
//Implant Form Device    ANZ_Implant_Form_Device
  public Product2 newProductANZ(String name) {
    return new Product2(
      Name = name,
      Description = name,
      RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Implant Form Device').getRecordTypeId(),
      IsActive = true,
      EAN_UPN__c = randomString5(),
      ProductCode = randomString5(),
      UPN_Material_Number__c = randomString5()
    );
  }

  public Pricebook2 newNMDPricebook() {
    Pricebook2 pb = newPricebook('NMD');
    pb.Description = 'NMD Products';
    return pb;
  }

  public Pricebook2 newPricebook() {
    return newPricebook(randomString5());
  }

  public Pricebook2 newPricebook(String name) {
    return new Pricebook2(
      Name = name,
      IsActive = true
    );
  }

  public PricebookEntry newPricebookEntry(Id productId) {
    return newPricebookEntry(Test.getStandardPricebookId(), productId);
  }

  public PricebookEntry newPricebookEntry(Id pricebookId, Id productId) {
    return new PricebookEntry(
      Pricebook2Id = pricebookId, 
      Product2Id = productId, 
      UnitPrice = 1, 
      IsActive = true, 
      UseStandardPrice = false
    );
  }

  public Account newAccount() {
    return newAccount('Account' + randomString5());
  }

  public Account newAccount(String name) {
    return new Account(
      Name = name,
      RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Insurance Provider').getRecordTypeId()
    );
  }
  public Account newAccountANZ() {
    return newAccountANZ('Account' + randomString5());
  }

  public Account newAccountANZ(String name) {
    return new Account(
      Name = name
    );
  }

  public Account createConsignmentAccount(){
    Personnel_ID__c pid = newPersonnelID();
    insert pid;
    Account acct = newAccount('Account' + randomString5());
    acct.RecordTypeId = AccountManager.RECTYPE_CONSIGNMENT;
    acct.Personnel_ID__c = pid.Id;
    return acct;
  }
    // test cycle
   
public Cycle_Count_Response_Analysis__c newCycleCountResponseAnalysis(){
    
    
    Cycle_Count_Response_Analysis__c resAna = new Cycle_Count_Response_Analysis__c();
    
    resAna.Status__c='Published';
    resAna.I_Acknowledge__c =true;
    resAna.Start_Date__c = System.today().addDays(1);
    resAna.End_Date__c = System.today().addDays(7);
    return resAna;  
}
  public Personnel_ID__c newPersonnelID() {
    return newPersonnelID(UserInfo.getUserId());
  }

  public Personnel_ID__c newPersonnelID(Id userId) {
    PersonnelIDCount++;
    return new Personnel_ID__c(
      Name = 'PID' + randomString5(), 
      User_for_account_team__c = userId,
      Personnel_ID__c = 'TESTDATA' + PersonnelIDCount
    );
  }

  public Opportunity newOpportunity(Id acctId) {
    return newOpportunity(acctId, 'Opportunity' + randomString5());
  }

  public Opportunity newOpportunity(Id acctId, String name) {
    return new Opportunity(
      AccountId = acctId,
      CloseDate = System.today().addDays(7),
      Name = name,
      StageName = 'New',
      Type = 'Standard',
      Opportunity_Type__c = 'Standard',
      Competitor_Name__c = 'Other',
    Product_System__c = 'Spectra'
    );
  }
  
  
    public Opportunity newOpportunityANZ(Id acctId) {
    return newOpportunityANZ(acctId, 'Opportunity' + randomString5());
  }

  public Opportunity newOpportunityANZ(Id acctId, String name) {
    return new Opportunity(
      AccountId = acctId,
      CloseDate = System.today().addDays(7),
      Name = name,
      StageName = 'New',
      Type = 'Standard',
      RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Explant').getRecordTypeId(),
      Opportunity_Type__c = 'Standard',
      Competitor_Name__c = 'Other',
    Product_System__c = 'Spectra'
    );
  }

  public Order newOrder(Id acctId, Id opptyId) {
    return new Order(
      AccountId = acctId,
      OpportunityId = opptyId,
      EffectiveDate = System.today(),
        Status = 'Draft',
        Order_Method__c = 'Approval'
    );
  }

  public Contact newContact(Id acctId) {
    return newContact(acctId, 'LastName' + randomString5());
  }

  public Contact newContact(Id acctId, String lname) {
    return new Contact(
      AccountId = acctId,
      LastName = lname
    );
  }

  public AccContactRelationship__c newAccContactRelationship(Id acctId, Id contactId) {
    return new AccContactRelationship__c(
      Facility__c = acctId,
      Contact__c = contactId
    );
  }

  public Lead newLead() {
    return newLead('LastName' + randomString5());  
  }

  public Lead newLead(String lname) {
    return new Lead(
      LastName = lname,
      Company = 'Company' + randomString5(),
      Status = 'Open',
      RecordTypeId = LeadManager.RECTYPE_NMD_PATIENTS
    );
  }

  public Patient__c newPatient() {
    return newPatient(randomString5(), randomString5());
  }
// AKG added Lead_Source__c on 13062016
  public Patient__c newPatient(String fname, String lname) {
    return new Patient__c(
      Patient_First_Name__c = fname,
      Patient_Last_Name__c = lname,
      Lead_Source__c = 'CARE Card'   
      
    );
  }
  
    public Patient__c newPatientANZ() {
    return newPatientANZ(randomString5(), randomString5());
  }
// AKG added Lead_Source__c on 13062016
  public Patient__c newPatientANZ(String fname, String lname) {
    return new Patient__c(
      Patient_First_Name__c = fname,
      Patient_Last_Name__c = lname,
      RecordTypeId = Schema.SObjectType.Patient__c.getRecordTypeInfosByName().get('ANZ Patient').getRecordTypeId(),
      Lead_Source__c = 'CARE Card'   
      
    );
  }
  

  public Task newTask(ID whoId, String subject, String status, Date activityDate) {
    return new Task(
      WhoId = whoId,
      Subject = subject,
      Status = status,
      ActivityDate = activityDate
    );
  }

  public Event newEvent(Id whatId) {
    return new Event(
      WhatId = whatId,
      ActivityDateTime = System.now(),
      DurationInMinutes = 60
    );
  }

  public Attachment newAttachment(Id parentId) {
    return newAttachment(parentId, randomString5());
  }

  public Attachment newAttachment(Id parentId, String name) {
    return newAttachment(parentId, name, randomString5());
  }

  public Attachment newAttachment(Id parentId, String name, String description) {
    return newAttachment(parentId, name, description, Blob.valueOf('X'));
  }

  public Attachment newAttachment(Id parentId, String name, String description, Blob body) {
    return new Attachment(
      ParentId = parentId,
      Name = name,
      Description = description,
      Body = body
    );
  }

  public PatientImageClassifications__c newPatientImageClassification(String name, Integer order) {
    return new PatientImageClassifications__c(
      Name = name,
      Order__c = order,
      Is_Active__c = true
    );
  }
  
  public Seller_Hierarchy__c newSellerHierarchy() {
    return newSellerHierarchy(randomString5());
  }

  public Seller_Hierarchy__c newSellerHierarchy(String name) {
    return new Seller_Hierarchy__c(
      Name = name
    );
  }
    public MC_Email__c newMCEmail(String name ){
        return new MC_Email__c(
        Name = name
        );
    }
  public Assignee__c newAssignee(Id userId, Id sellerId) {
    return newAssignee(randomString5(), userId, sellerId);
  }
  
  public Assignee__c newAssignee(String name, Id userId, Id sellerId) {
    return new Assignee__c(
      Name = name,
      Assignee__c = userId,
      Territory__c = sellerId    
    );
  }
  
  public User newUser(){
    return newUser('Standard User');
  }
  
  public User newUser(String userType){
    Profile p = [SELECT Id FROM Profile WHERE Name = :userType]; 
        User user = new User(
          Email = 'suser@boston.com', 
      LastName = 'LNAMETEST',  
      ProfileId = p.Id, 
      UserName = randomString5() + '@boston.com',
      Alias = 'standt', 
      EmailEncodingKey = 'UTF-8',  
      LanguageLocaleKey = 'en_US', 
      LocaleSidKey = 'en_US',  
      TimeZoneSidKey = 'America/Los_Angeles',
      Cost_Center_Code__c = randomString5() // special forNMD
    );
    return user;                  
  }  

  public String randomString5() {
    return randomString().substring(0, 5);
  }

  public String randomString() {
    //return random string of 32 chars
    return EncodingUtil.convertToHex(Crypto.generateAesKey(128));
  }
  
// run test class as admin user 

public Static User setupUser()
    {
        Profile prof = [select id from profile where name = 'System Administrator'];
        User user = new User(alias = 'standt', email = 'my123_123@gmail.com', emailencodingkey = 'UTF-8',
                         lastname = 'Test', languagelocalekey = 'en_US', localesidkey = 'en_US',
                         profileid = prof.Id, timezonesidkey = 'America/Los_Angeles',IsActive =true,
                         username = 'testuser_123@gmail.com');
        //insert user;
        return user;
    } 
    
    public Static User setupUserNMDRBM()
    {
        Profile prof = [select id from profile where name = 'NMD RBM'];
        User user = new User(alias = 'stRBM', email = 'my123_1234@gmail.com', emailencodingkey = 'UTF-8',
                         lastname = 'Test', languagelocalekey = 'en_US', localesidkey = 'en_US',
                         profileid = prof.Id, timezonesidkey = 'America/Los_Angeles',IsActive =true,
                         username = 'testuser_1234@gmail.com');
        //insert user;
        return user;
    }  
    
    public static void createRowCauseObjects(){
        list<RowCauseObjects__c> Rlist =  new list<RowCauseObjects__c >();
        RowCauseObjects__c r1 = new RowCauseObjects__c();
        r1.Name = 'Opportunity';
        r1.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,Team';
        Rlist.add(r1);
        RowCauseObjects__c r2 = new RowCauseObjects__c();
        r2.Name = 'Patient';
        r2.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,NMDTerritoryChange__c';
        Rlist.add(r2);
        RowCauseObjects__c r3 = new RowCauseObjects__c();
        r3.Name = 'Lead';
        r3.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
        Rlist.add(r3);
        RowCauseObjects__c r4 = new RowCauseObjects__c();
        r4.Name = 'Contact';
        r4.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
        Rlist.add(r4);
        insert Rlist;
    
    }
  
}
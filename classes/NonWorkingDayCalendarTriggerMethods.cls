/*
@modifiedDate    20Sept2016
 @author         Ashish 
 @Description    To Create child date records for Country Calender Year
 @Requirement ID SFDC-1488

*/

public class NonWorkingDayCalendarTriggerMethods{
     
     public static void CreateWeekendChildRecOnUpdate(Map<Id,Uro_PH_Non_Working_Day_Calendar__c> newCalenderMap,Map<Id,Uro_PH_Non_Working_Day_Calendar__c> oldCalenderMap){
         System.debug('==newCalenderMap===='+newCalenderMap);
         System.debug('==oldCalenderMap===='+oldCalenderMap);
         Map<Id,Uro_PH_Non_Working_Day_Calendar__c> calenderMap = new Map<Id,Uro_PH_Non_Working_Day_Calendar__c>();
         for(Id newRecId : newCalenderMap.KeySet()){
             if(newCalenderMap.get(newRecId).Uro_PH_Year__c != oldCalenderMap.get(newRecId).Uro_PH_Year__c && newCalenderMap.get(newRecId).Uro_PH_Year__c != null){
                 calenderMap.put(newRecId,newCalenderMap.get(newRecId));
             }
         }
         if(calenderMap.Size() > 0){
             list<Uro_PH_Non_Working_Day__c> lstNonWorkingDaysToDelete = [Select id from Uro_PH_Non_Working_Day__c where Uro_PH_Non_Working_Day_Calendar__c in : calenderMap.keyset() ];
             if(lstNonWorkingDaysToDelete.size() > 0){
                 try{
                     Delete lstNonWorkingDaysToDelete;
                 }Catch(Exception e){
                     System.debug('###==e==='+e);
                 }
             }
             
             CreateWeekendChildRec(calenderMap.Values());
         }
     
     }
     
     public static void CreateWeekendChildRec(List<Uro_PH_Non_Working_Day_Calendar__c> newCalenderLst){
         
         list<Uro_PH_Non_Working_Day__c> lstNonWorkingDaysToInsert = new list<Uro_PH_Non_Working_Day__c>();
         Uro_PH_Non_Working_Day__c objNWDToInsert;
         
         for(Uro_PH_Non_Working_Day_Calendar__c cl : newCalenderLst){
             Integer Year = Integer.ValueOf(cl.Uro_PH_Year__c);
             Date firstDay = date.newinstance(Year, 1, 1);
             Date LastDay = date.newInstance(Year, 12, 31);
             Date CurrentDate;
             Date tempDate = date.newInstance(1900, 1, 6);
             for(integer i=0; i < firstDay.daysBetween(LastDay) ; i++){
                CurrentDate = firstDay.addDays(i); 
                if(Math.MOD((tempDate.daysBetween(CurrentDate)) , 7) < 2 ){
                    objNWDToInsert = new Uro_PH_Non_Working_Day__c();
                    objNWDToInsert.Uro_PH_Non_Working_Day_Calendar__c = cl.Id;
                    objNWDToInsert.Uro_PH_Date__c = CurrentDate;
                    objNWDToInsert.Uro_PH_Description__c = (Math.MOD((tempDate.daysBetween(CurrentDate)) , 7) == 0) ? 'Saturday' : 'Sunday'; 
                    lstNonWorkingDaysToInsert.add(objNWDToInsert);
                }
             }
         }
         System.debug('=======lstNonWorkingDaysToInsert========'+lstNonWorkingDaysToInsert);
         if(lstNonWorkingDaysToInsert.size() > 0){
             try{
                 insert lstNonWorkingDaysToInsert;
             }catch(exception e){
                 System.debug('####===e===='+e);
             }
         }
     }       
}
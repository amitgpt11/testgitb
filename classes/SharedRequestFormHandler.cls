/*
* Handler class for the SharedRequestFormTrigger 
*
* @Author Accenture Services
* @Date 10OCT2016
*/

public class SharedRequestFormHandler  extends TriggerHandler {

final SharedRequestFormManager manager = new SharedRequestFormManager();
private list<Shared_Request_Form__c> updateLst = new list<Shared_Request_Form__c>();
static final Map<String,Schema.RecordTypeInfo> RECTYPES_RequestForm = Schema.SObjectType.Shared_Request_Form__c.getRecordTypeInfosByName();
public static final Id RECTYPE_Sample_Request = RECTYPES_RequestForm.get('PI Sample Request Form').getRecordTypeId();


    public override void bulkBefore() {        
        list<Shared_Request_Form__c> reqFrmlst = (list<Shared_Request_Form__c>)trigger.new;
        
       if(reqFrmlst != null && reqFrmlst.size()>0){
            manager.fetchSampleBudgetRecords(reqFrmlst);
       }
       if(Trigger.isInsert){
       system.debug('inside insert');
           if(reqFrmlst!= null && reqFrmlst.size() > 0){  
                manager.updatePriceFieldsOnSampleForm(reqFrmlst);              
                
            }
       }
        if (Trigger.isUpdate){
                      
        }  
    }
    
    public override void bulkAfter() { 
       list<Shared_Request_Form__c> reqFrmlst = (list<Shared_Request_Form__c>)trigger.new;
       list<Shared_Request_Form__c> OldreqFrmlst = (list<Shared_Request_Form__c>)trigger.old;
       
       map<Id,Shared_Request_Form__c> newReqFrmIdMap = new map<Id,Shared_Request_Form__c>();
       
       if (Trigger.isUpdate){
            if(reqFrmlst != null && reqFrmlst.size() > 0){
                for(Shared_Request_Form__c sr : reqFrmlst){
                    if(sr.RecordTypeId == RECTYPE_Sample_Request && sr.PI_Approval_Status__c == 'Approved'){
                        newReqFrmIdMap.put(sr.Id,sr);
                    }
                }  
            }   
            system.debug('newReqFrmIdMap -->'+newReqFrmIdMap );
            if(newReqFrmIdMap != null && newReqFrmIdMap.size() > 0){            
                manager.deductAmountFromBudget(newReqFrmIdMap);
            }       
        }          

    }
    
    
    public override void beforeInsert(SObject obj) {
        
    }

    public override void beforeUpdate(SObject oldObj, SObject obj) {
        
    }

    public override void afterUpdate(SObject oldObj, SObject obj) {
        
    }

    public override void andFinally() {
       manager.commitSampleFormRecords();
       manager.commitSampleBudgetRecords();      

    }

}
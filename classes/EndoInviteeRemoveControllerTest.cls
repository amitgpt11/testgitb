@isTest(SeeAllData=false)
public class EndoInviteeRemoveControllerTest {
    
    static testmethod void DeleteEventRelationInvitee(){
        
        Test.startTest();
        
        //ID ProfileID = [ Select id,UserType from Profile where name = 'System Administrator'].id;
        User u1 = new User( email='test-user111@fakeemail.com', profileid = UserInfo.getProfileId(), 
        UserName='test-user22222@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User');
        insert u1; 

        Event event = new Event(Subject = 'Test Event', StartDateTime=system.today(), EndDateTime=system.today(), Type = 'Call', BluePrint_Bypass_RelatedTo_and_Name__c = true);
        insert event;
        
        EventRelation er = new EventRelation(EventId =event.id, RelationId = u1.Id, Status='New',IsInvitee = true,RespondedDate=system.Today(),IsParent=false,IsWhat=false,Response='test');
        insert er;
                
        PageReference pg = Page.RemoveInviteesToENDOEvent;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('Id',event.Id);
        
        Event ent = new Event();
        ApexPages.StandardController st = new ApexPages.standardController(ent);
        EndoInviteeRemoveController e = new EndoInviteeRemoveController(st);

        for(EndoInviteeRemoveController.EventRelationwrapper ev : e.EventRelationWrapperList){
            ev.isSelected = true;
        }
        e.getSelected();
        e.DeleteEventRelation();
        
        EndoInviteeRemoveController.EventRelationwrapper ew= new EndoInviteeRemoveController.EventRelationwrapper();
                
        Test.stopTest();
    }
}
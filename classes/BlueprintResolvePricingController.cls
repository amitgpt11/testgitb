/*
 @CreatedDate     17Jun2016                                  
 @ModifiedDate    17Jun2016                                  
 @author          Mayuri-Accenture
 @Description     This class is used to fetch a tablue link for logged in user and display the link on Account detail page section "Resolved Pricing"
 @Methods         BlueprintResolvePricingController(),getTablueLinkInfo()
 @Requirement Id  Q3 2016-US2665
 */
public with sharing class BlueprintResolvePricingController {

     public Id profileId;
     public string SAPAccountId{get;set;}
     public string profileName{get;set;}
     public string link{get;set;}
     public string SF1SiteRoot{get;set;}
     public string linkSF1{get;set;}
     public string username{get;set;}
     map<string,string> profileLinkMap = new map<string,string>();
     map<string,string> profileUserMap = new map<string,string>();
     map<string,string> profileSiteRootMap = new map<string,string>();   
     map<string,string> profileLinkSF1Map = new map<string,string>();     
     list<string> excludedProfileNames = new list<string>();
     map<Id,Profile> profileMap{get;set;}
     map<string,BlueprintResolvedPricing__c> pricingValues{get;set;}
     public Account AC;
     public boolean isError{get;set;}   
      
     
     public BlueprintResolvePricingController(ApexPages.StandardController controller) {
         profileId = UserInfo.getProfileId();
         this.AC = (Account)controller.getRecord();
         getTablueLinkInfo();
         
     }
     
    /*
    @MethodName    getTablueLinkInfo
    @Return Type   void
    @Description   Method to extract tablue link from cutom setting 'BlueprintResolvedPricing__c' for the logged in user
    */
     public void getTablueLinkInfo(){
         SAPAccountId = [SELECT Id,name,Account_SAP_ID__c FROM Account WHERE Id =: AC.Id].Account_SAP_ID__c; //query sap account number of current account
         System.debug('SAP Account ID: ' + SAPAccountId);
         profileName = [SELECT Id,Name FROM Profile WHERE Id =:profileId].Name;         
         pricingValues = BlueprintResolvedPricing__c.getAll();
         for(string st : pricingValues.keyset()){
             string link = pricingValues.get(st).Tablue_Link__c;
             string siteRoot = pricingValues.get(st).TablueSF1SiteRoot__c;
             string linkSF1 = pricingValues.get(st).TablueLinkSF1__c;
             list<string> pfNames = pricingValues.get(st).Profile_Names__c.split(',');
             if(pfNames.size()>0){
                 for(string pf : pfNames){
                     profileLinkMap.put(pf,link);
                     profileUserMap.put(pf,st);
                     profileSiteRootMap.put(pf,siteRoot);
                     profileLinkSF1Map.put(pf,linkSF1);
                 }
             }             
         }
         
         if(profileLinkMap != null && profileLinkMap.size() > 0){
             if(profileName != null && profileLinkMap.keyset().contains(profileName)){
                 if(profileLinkMap.get(profileName) != null){
                     if(SAPAccountId!=null){
                         link = profileLinkMap.get(profileName) + SAPAccountId;
                         SF1SiteRoot = profileSiteRootMap.get(profileName);
                         linkSF1 = profileLinkSF1Map.get(profileName);
                         system.debug(SF1SiteRoot+'--SF1SiteRoot --');
                         isError = false;
                     }
                     else{
                         isError = true;
                     }
                 }
             }
         }         
         if(profileUserMap!= null && profileUserMap.size() > 0){
             if(profileName != null && profileUserMap.keyset().contains(profileName)){
                 if(profileUserMap.get(profileName) != null){
                     username = profileUserMap.get(profileName); //get logged in user profile name to display it in VF page 
                 }
             }
         }
         
     }
     
     private String myCustomFieldMapper() {
        // This customer written class does the "work" need to map and get a Tableau username
        // In our example we use a static string "sfUser" as the string to map.
        //public StringMyUserId = UserInfo.getUserId();
        List<User> u = [Select FederationIdentifier from user where id=:UserInfo.getUserId()];
        System.debug('FEderated Id is ---'+u[0].FederationIdentifier);
        return u[0].FederationIdentifier;
    }
    
    /**
    * Gets the signed identity; always want to generate this in a getter since the constructor
    * only gets called on original page load and timestamp will skew
    */
    public String getSignedIdentity() {
        String signedIdentity = TableauSparklerUtilities.generateSignedIdentity(this.myCustomFieldMapper());
        system.debug('Singned Entity========'+signedIdentity);
        return signedIdentity;
    }

}
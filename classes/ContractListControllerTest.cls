@isTest(SeeAllData=false)
public class ContractListControllerTest
{
   Private Static testmethod void TestContractListController(){    
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        
        Contract accContract = new Contract();
        accContract.AccountId = acc.id;
         accContract.Name = '1234';
         accContract.BSC_Contract_Number__c='12333';
        // accContract.ContractNumber='';
         accContract.Status='Draft';
         accContract.StartDate = date.today();
         accContract.EndDate = date.today().addDays(2);
         accContract.Contract_Type__c = 'All Play';
         accContract.Shared_Divisions__c = 'EP';
         accContract.Product_Group__c = 'Balloons';
         //accContract.CA_Days_Until_Expiration__c = '';
        insert accContract;
        
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(acc);
        ContractListController Cont  = new ContractListController(stdAccount );
       
    }
    Private Static testmethod void TestContractListController2(){    
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        
        Contract accContract = new Contract();
        accContract.AccountId = acc.id;
         accContract.Name = '1234';
         accContract.BSC_Contract_Number__c='12333';
        // accContract.ContractNumber='';
         accContract.Status='Draft';
         accContract.StartDate = date.today();
         accContract.EndDate = date.today().addDays(2);
         accContract.Contract_Type__c = 'All Play';
         accContract.Shared_Divisions__c = 'Endo';
         accContract.Product_Group__c = 'Balloons';
         //accContract.CA_Days_Until_Expiration__c = '';
        insert accContract;
        
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(acc);
        ContractListController Cont  = new ContractListController(stdAccount );
       
    }
}
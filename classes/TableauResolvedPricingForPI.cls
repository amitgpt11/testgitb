/*
Modified On : 26AUG2016
    Modified By : Mayuri 
    Modification comments : Added two new methods myCustomFieldMapper() and getSignedIdentity() to support tableau reports in SF1    
*/
public class TableauResolvedPricingForPI {

    public TableauResolvedPricingForPI(ApexPages.StandardController controller) {

          currentuserProfile=new Profile();
     currentuserProfile=[Select Id,Name from Profile where Id=:userinfo.getProfileId()];
     
    }


  public Profile currentuserProfile{get;set;}
  public TableauResolvedPricingForPI (){
     
     currentuserProfile=new Profile();
     currentuserProfile=[Select Id,Name from Profile where Id=:userinfo.getProfileId()];
}
 private String myCustomFieldMapper() {
        // This customer written class does the "work" need to map and get a Tableau username
        // In our example we use a static string "sfUser" as the string to map.
        //public StringMyUserId = UserInfo.getUserId();
        List<User> u = [Select FederationIdentifier from user where id=:UserInfo.getUserId()];
        System.debug('FEderated Id is ---'+u[0].FederationIdentifier);
        return u[0].FederationIdentifier;
    }
    
    /**
    * Gets the signed identity; always want to generate this in a getter since the constructor
    * only gets called on original page load and timestamp will skew
    */
    public String getSignedIdentity() {
        String signedIdentity = TableauSparklerUtilities.generateSignedIdentity(this.myCustomFieldMapper());
        system.debug('Singned Entity========'+signedIdentity);
        return signedIdentity;
    }
}
@isTest 
private class ANZImplantFormExtensionTest{

@testSetup 
static void setupData() {

Map <String,Schema.RecordTypeInfo> recordTypes_Account = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_Account = recordTypes_Account.get('Customer').getRecordTypeId();

Account acc = new Account();
acc.Name ='Test Account';
acc.RecordTypeId = recordTypeID_Account;
                                
insert acc;

Map <String,Schema.RecordTypeInfo> recordTypes_Contact = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_Contact  = recordTypes_Contact.get('General Contact').getRecordTypeId();
   Contact conc = new Contact();  
   conc.AccountId= acc.Id;  
   conc.LastName='Test';
   insert conc;
   
   
  // User userobj =  new User();

  /*Profile p = [SELECT Id FROM Profile WHERE Name ='System Administrator' Limit 1]; 
            User userobj = new User(
                Email = 'suser@boston.com', 
                LastName = 'LNAMETEST',  
                ProfileId = p.Id, 
                UserName = 'UserName12' + '@boston.com',
                Alias = 'standt', 
                EmailEncodingKey = 'UTF-8',  
                LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US',  
                TimeZoneSidKey = 'America/Los_Angeles',
                Cost_Center_Code__c = '1234', 
                IsActive = True);
                
                insert userobj;*/
   Map <String,Schema.RecordTypeInfo> recordTypes_SharedRequestForm = Shared_Request_Form__c.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_SharedRequestForm  = recordTypes_SharedRequestForm .get('UroPH HCP Disease State PES').getRecordTypeId();            
   Shared_Request_Form__c sharedreq = new  Shared_Request_Form__c();              
   sharedreq.Requesters_Name__c=Userinfo.getUserId();//userobj.Id;
  sharedreq.Physician_Name__c=conc.Id;
  sharedreq.Prior_Speaking_Training_Experience__c='Yes';
  sharedreq.Residency_Institution__c=acc.Id;
  sharedreq.Speaking_Experience_Examples__c='Faculty for training Programs';
  sharedreq.Years_of_Speaking_Training_Experience__c='< 3 years';
  sharedreq.Years_in_Practice__c= '< 3 years';
  sharedreq.Region__c = 'Europe';
  sharedreq.Business_Unit__c = 'BPH' ; 
  sharedreq.RecordTypeId = recordTypeID_SharedRequestForm  ;
  insert sharedreq;     
  

  Product2 prodobj= new Product2();
   prodobj.Name='Test';
   prodobj.EAN_UPN__c = 'Test EAN/UPN';
   prodobj.ProductCode = 'test code';
   prodobj.UPN_Material_Number__c = 'T1e2s3t4';
   insert  prodobj; 
   
    
/*
  Product2 prodobj2= new Product2();
   prodobj2.Name='TestABC2';
  prodobj2.EAN_UPN__c='1122345';
    
//    prodobj2.RecordTypeId=RECTYPE_LEVEL4;
   prodobj2.UPN_Material_Number__c='123421415'; 
    //prodobj2.Shared_Parent_Product__c=prodobj.Id ;
    
    insert prodobj2;
    */
    
 Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
            rItem.Full_UPN__c = '12345';
            rItem.Quantity__c = 1;
            rItem.Unit_Of__c = 'EA'; 
insert rItem;

Shared_Request_Form_Line_Item__c  rItem1 = new Shared_Request_Form_Line_Item__c(); 
            rItem1.Full_UPN__c = '12345';
            rItem1.Quantity__c = 1;
            rItem1.Unit_Of__c = 'EA'; 
insert rItem1;

 /*Shared_Parent_Product__c  objparentprod = new Shared_Parent_Product__c ();
 objparentprod.Name='Test';
 objparentprod.UPN_Material_Number__c='12345';
 objparentprod.EAN_UPN__c ='12345';
 insert objparentprod;*/
 
 
    

}
    static TestMethod void TestDemoRequestFormExtension(){ 
       // setupData();
        Account acc = [Select id,name from Account limit 1];
        Test.StartTest();
        DemoRequestFormExtension objdemo = new  DemoRequestFormExtension();
      
        objdemo.facilityID = acc.id;            
        objdemo.sendPdf();
        system.debug('*****'+ objdemo.requestForm);      
        objdemo.ValidateValues();
        //objdemo.changeProductLEVEL4(); //by AKG 06-oct-2016 as commented on the class
        objdemo.changeCostCenter();
        //objdemo.getLEVEL4Products(); //by AKG 06-oct-2016
        objdemo.save();
        
     list<Shared_Request_Form__c> lstObj = [Select Id,Name,Requesters_Name__c,Requesters_Name__r.Name from Shared_Request_Form__c limit 1];
    if(lstObj.size()>0){  
      Apexpages.currentpage().getparameters().put('id',lstObj.get(0).id);
        
      CallPDFController objcall= new CallPDFController();
      objcall.callPdfPage();
   }   
        Test.StopTest();
    }
}
global without sharing class RequestAccountController extends AuthorizationUtil{

    //Variables
    public Task objTask {get; set;}
    public List<Contact> objContact {get; set;}
    public BSC_VTM_Alignment__c objVTMAlignment {get; set;}
    public String country {get; set;}  
    @TestVisible private String taskSubject {get; set;}
    public String messageDescription {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String contactCompany {get; set;}
    public String contactEmail {get; set;}
    public String contactCity {get; set;}
    public String contactCountry {get; set;}
    public String contactDivision {get; set;}
    public String contactFunction {get; set;}
    public String contactLanguage {get;set;}
    public String taskRecordTypeId {get; set;}

    /**
      Description: Constructor of class
    */
    public RequestAccountController() {
    
        taskSubject = 'Account Request';
    } 
    
    public override void fetchRequestedData() {}
    
    /**
    *  Description: Get all Countries
    */
    public String selectedCountry {get;set;}

    public List<selectOption> countries {set;}
    public List<selectOption> getcountries(){
    
          List<selectOption> options = new List<selectOption>();
          options.add(new SelectOption('--Select--','--Select--'));

          contactDivision = (contactDivision == 'Cardiology')?'Cardio':contactDivision;
          
          for (BSC_Community_Countries__c country : [SELECT Name FROM BSC_Community_Countries__c WHERE Shared_Division__c =: contactDivision AND Shared_Division__c != null])
          options.add(new SelectOption(country.Name,country.Name));
          options.sort();
          return options;
    }
    
    /**
    *  Description: Get all Countries
    */
    

    public List<selectOption> Languages {set;}
    public List<selectOption> getLanguages(){
        
        List<selectOption> options = new List<selectOption>();

          options.add(new SelectOption('--Select--','--Select--'));
          
          contactDivision = (contactDivision == 'Cardiology')?'Cardio':contactDivision;
          
          for (Shared_BSC_Community_Languages__c Language : [SELECT Name FROM Shared_BSC_Community_Languages__c WHERE Division__c =: contactDivision OR Division__c = 'Default']){
              
            options.add(new SelectOption(Language.Name,Language.Name));
          }
          options.sort();
          return options;
    }
    
    public PageReference fetchLanguageAndCountryBasedOnSpeciality(){
        
        getLanguages();
        getcountries();
        return null;
    }
    
     /**
      Description: Get all Division(Area of interest)
    */
    public List<SelectOption> getDivisions() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--Select--','--Select--'));
       // options.add(new SelectOption('Cardiology','Cardiology'));
        options.add(new SelectOption('Endo','Gastroenterology'));
        
        return options;
    }
    public List<SelectOption> functions{
    
        get{
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Contact.Europe_Function__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                options.add(new SelectOption('--Select--','--Select--'));
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    /**
      Description: This method will call for Unauthenticated user's Contact Form send button to send an Email based on Country and Division selected by Guest User.
    */
    Public void contactVTMForAccess() {
    
        contactDivision = (contactDivision == 'Cardiology')?'Cardio':contactDivision;
        
        if(firstName != '' &&  lastName != '' && contactEmail != '' && contactCompany != '' && contactCity != '' && contactCountry != '--Select--' && contactDivision != '--Select--' && contactFunction != '--Select--' && contactLanguage != '--Select--' ) {
            
            //Check entered Email Address in Contact Object
            objContact = new List<Contact>();
            objContact = [SELECT Id, Name, Email, OwnerId 
                FROM Contact
                WHERE Email =: contactEmail 
                Limit 1];
            
            //If Contact found with same Email Address Create Task on That Contact. ELSE send an Email to VTM based on selection of Division and Country.
            if(objContact.size() > 0) {
                
                //Create Task if Found the Contact with Same Email Address.
                objTask = New Task();
                objTask.Subject = taskSubject;
                objTask.Message__c = 'Name: '+firstName+' '+lastName+'\r\n'+ 'Email: '+contactEmail+'\r\n'+ 'Company: '+contactCompany+'\r\n'+ 'City: '+ contactCity+'\r\n'+ 'Country: '+contactCountry+'\r\n'+ 'Division: '+contactDivision+'\r\n'+ 'Function: '+contactFunction+'\r\n'+ 'Language: '+contactLanguage+'\r\n'+ 'Subject: '+taskSubject+'\r\n' +'Message: '+messageDescription;
                objTask.Status = 'Not Started';
                objTask.OwnerId = objContact[0].OwnerId;
                objTask.WhoId = objContact[0].Id;
                objTask.RecordTypeId = [SELECT Id,SobjectType,Name 
                    FROM RecordType 
                    WHERE DeveloperName ='Shared_Community_Task' 
                    AND SobjectType ='Task' 
                    LIMIT 1].Id;
                    
                Insert objTask;
                
            }
            else {
                
                 objVTMAlignment = new BSC_VTM_Alignment__c();
                 
                 system.debug('contactDivision========='+contactDivision);
                 system.debug('contactCountry========='+contactCountry);
                 
                 objVTMAlignment = [SELECT Name, Shared_VTM_Name__c, Shared_Country__c, Shared_Division__c, Shared_Email__c
                        FROM BSC_VTM_Alignment__c
                        WHERE Shared_Division__c =: contactDivision
                        AND Shared_Country__c =: contactCountry
                        Limit 1];
                 
                 /*
                 //Query on Custom setting to get VTM based on the Selection od Division.
                 if(contactDivision == 'Cardiology') {
                    
                    objVTMAlignment = [SELECT Name, Shared_Country__c, Shared_Division__c, Shared_Email__c
                        FROM BSC_VTM_Alignment__c
                        WHERE Shared_Division__c =: contactDivision
                        AND Shared_Country__c =: contactCountry
                        Limit 1];
                }
                else if(contactDivision == 'Endo') {
                    
                    objVTMAlignment = [SELECT Name, Shared_Country__c, Shared_Division__c, Shared_Email__c
                        FROM BSC_VTM_Alignment__c
                        WHERE Shared_Division__c =: contactDivision];
                }*/
                
                system.debug('objVTMAlignment========='+objVTMAlignment);
                
                //if Found VTM send and Email for Access.
                if(objVTMAlignment != null) {
                
                    List<Messaging.SingleEmailMessage> objMailList = new List<Messaging.SingleEmailMessage>();
                //Approver email
                    Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
                    
                    //set To address to send an Email.
                    List<String> sendTo = new List<String>();
                    sendTo.add(objVTMAlignment.Shared_Email__c);
                    objMail.setToAddresses(sendTo);
                    
                    //Subject of an Email
                    objMail.setSubject('InTouch Account Request');
                    
                    //Create Email Body with format.
                    String Body = 'Hi '+objVTMAlignment.Shared_VTM_Name__c+',<br/><br/>';
                    body = body + 'This user is requesting for access in your community. Below are the details.<br/><br/>';
                    body = body + 'Name: '+firstName+' '+lastName+'<br/>';
                    body = body + 'Email: '+contactEmail+'<br/>';
                    body = body + 'Company: '+contactCompany+'<br/>';
                    body = body + 'City: '+ contactCity+'<br/>';
                    body = body + 'Country: '+contactCountry+'<br/>';
                    body = body + 'Division: '+contactDivision+'<br/>';
                    body = body + 'Function: '+contactFunction+'<br/>';
                    body = body + 'Language: '+contactLanguage+'<br/>';
                    body = body + 'Subject: New Account Request <br/>';
                    
                    if(String.isNotBlank(messageDescription)){
                        
                        body = body + 'Message: '+messageDescription;
                    }
                    
                    //Set Body of Email.
                    objMail.setHtmlBody(body);
                    
                    //add Mail Object into the List of SingleEmailMessage List Object
                    objMailList.add(objMail);
                    
                    
                    
                    
            //Requester email
                    Messaging.SingleEmailMessage objRequesterMail = new Messaging.SingleEmailMessage();
                    
                    //set To address to send an Email.
                    List<String> sendToRequester = new List<String>();
                    sendToRequester.add(contactEmail);
                    objRequesterMail.setToAddresses(sendToRequester);
                    
                    //Subject of an Email
                    objRequesterMail.setSubject('InTouch Account Request');
                    
                    //Create Email Body with format.
                    String BodyRequesterEmail = 'Hi '+ firstName +' '+ lastName +',<br/><br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Your request has been received for new account with following details.<br/><br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Name: '+firstName+''+lastName+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Email: '+contactEmail+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Company: '+contactCompany+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'City: '+ contactCity+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Country: '+contactCountry+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Division: '+contactDivision+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Function: '+contactFunction+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Language: '+contactLanguage+'<br/>';
                    BodyRequesterEmail = BodyRequesterEmail + 'Subject: Account Request Received <br/>';
                    
                    if(String.isNotBlank(messageDescription)){
                        
                        BodyRequesterEmail = BodyRequesterEmail + 'Message: '+messageDescription;
                    }
                    
                    //Set Body of Email.
                    objRequesterMail.setHtmlBody(BodyRequesterEmail);
                    
                    //add Mail Object into the List of SingleEmailMessage List Object
                    objMailList.add(objRequesterMail);
                    
                    
                    
                    //Send Email.
                    Messaging.sendEmail(objMailList);
                }
            }
            clearForm();
        }
        else {
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please fill all required* fields.'));
        }
        
    }
    
    /**
      Description: This method will Clear the form after clicking of Send button.
    */
    public void clearForm() {
        
        firstName = '';
        lastName = '';
        contactEmail = '';
        contactCompany = '';
        contactCity = '';
        contactCountry = '';
        contactDivision = '';
        contactFunction = '';
        contactLanguage = '';
        taskSubject = '';
        messageDescription = '';
    }
}
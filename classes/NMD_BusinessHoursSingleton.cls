public class NMD_BusinessHoursSingleton {
    public static final String NMD_BUSINESSHOURS_NAME = 'NMD_Hours';
    public BusinessHours NMD_BusinessHours = null;
    // private static variable referencing the class
	private static NMD_BusinessHoursSingleton instance = null;
    

    // The constructor is private and initializes the id of the NMD_BusinessHours type
    private NMD_BusinessHoursSingleton(){
         try{
            NMD_BusinessHours = [SELECT Id FROM BusinessHours WHERE Name = :NMD_BUSINESSHOURS_NAME];
        } catch (Exception e){
            System.debug('***OrderManager: No NMD Business Hours setup.');
            Application_Log__c log = new Application_Log__c(
                Source_Class__c = 'OrderManager',
                Source_Function__c = 'fetchRelatedRecords',
                Record_Object_Type__c = 'Order',
                Message__c = 'NMD Business Hours not setup, please create an entry called "NMD_Hours"',
                Timestamp__c = System.now()
            );
            DML.save('OrderManager', log);
        }
        
    }
    // a static method that returns the instance of the NMD_BusinessHours type
    public static NMD_BusinessHoursSingleton getInstance(){
        // lazy load the NMD_BusinessHours type - only initialize if it doesn't already exist
        if(instance == null) instance = new NMD_BusinessHoursSingleton();
        return instance;
    }
}
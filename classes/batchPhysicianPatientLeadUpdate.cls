global class batchPhysicianPatientLeadUpdate implements Database.Batchable<sObject>,Database.Stateful {


            List<contact> AllNewAccWiseContactList = new List<contact>();
            List<Contact> cFinalwithOwner = new List<Contact>();
            List<Patient__c> selectedPList = new List<Patient__c>();
            Set<Id> pOldTerr = new Set<Id>();
            List<Physician_Territory_Realignment__c> Allscope = new List<Physician_Territory_Realignment__c>();
            List<Patient__c> Allpatients = new List<Patient__c>();
            List<id> AlloldAccountIdList = new List<id>();
            set<id> AlloldContactTerriMulti = new set<id>();
            set<id> AlloldPatientTerriMulti = new set<id>();
            static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};  
            
List<Physician_Territory_Realignment__c>  ptrList = new List<Physician_Territory_Realignment__c>();
            
            
            
        global Database.QueryLocator start(Database.BatchableContext BC) {
           
            String query = 'SELECT Id,Name,Contact__c,New_Account_Id__c,Old_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c = \'readyforpatient\'' ;
            return Database.getQueryLocator(query);
        }
       
        global void execute(Database.BatchableContext BC, List<Physician_Territory_Realignment__c> scope) {
       
       system.debug('----Physician_Territory_Realignment__c  batchPhysicianPatientLeadUpdate scope--'+scope+'____ scope size ____'+scope.size());
            set<id> conIdList = new set<id> ();
        for(Physician_Territory_Realignment__c a : scope)
        {
            conIdList.add(a.Contact__c);        
        }

                      if(!conIdList.isEmpty())        
         MovePatientToPage3ConHelper.movePatientsOnly(conIdList);      


        }   
        
    global void finish(Database.BatchableContext BC) {

batchPhysicianOpportunityUpdate b = new batchPhysicianOpportunityUpdate();
database.executebatch(b,10);  
    }
}
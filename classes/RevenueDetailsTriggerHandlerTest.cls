/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RevenueDetailsTriggerHandlerTest {

    static final String ACCT_MASTER = 'MasterAccount';
    static final String OPPTY_NAME = '/-.-NMD SCS Trial-Implant';
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
    
    @testSetup
    static void setup() {
        NMD_TestDataManager td = new NMD_TestDataManager();
        
        Account acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

        insert new List<Account> {
            acctMaster
        };
        
        Opportunity oppty = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty.RecordTypeId = RECTYPES_OPPTY.get('EP Disposable').getRecordTypeId();
        oppty.Amount = 5000;
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        insert oppty;
    }
    
    
    static testMethod void testRevDetailInsert() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_MONTHLY;
        Test.startTest();
            insert revDetail;
        Test.stopTest();
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        Integer currMonth = System.today().month();
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 12-currMonth+1);
    }
    
    
    static testMethod void testRevDetailUpdate() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_MONTHLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        Integer currMonth = System.today().month();
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 12-currMonth+1);
        
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_QUARTERLY;
        Test.startTest();
            update revDetail;
        Test.stopTest();
        revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        Integer currentMnt = System.today().month();
        Integer currentQ =((currentMnt-1)/3) + 1;
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 4-currentQ+1);
    }
    
    
    static testMethod void testRevDetailAmountUpdate() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_MONTHLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        
        revDetail.Shared_Opportunity_Revenue__c = 6000;
        Test.startTest();
            update revDetail;
        Test.stopTest();
        oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        //System.assertEquals(6000,oppty.Amount);
    }
    
    
    static testMethod void testOnlyRevDetailPerOpportunity() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_MONTHLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        Opportunity_Revenue_Schedule__c revDetail2 = revDetail.clone();
        
        try {
            insert revDetail2;
            System.assertEquals(true, 1 != 1,'Multiple Rev Detail Allowed Per Opportunity');
        }
        catch (Exception e){
            System.assertEquals(true, e != null);
        }
    }
    
    
    /*static testMethod void testRevDetailUpdateonOppAmtUpdate() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_MONTHLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        
        oppty.Amount = 6000;
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        Test.startTest();
            update oppty;
        Test.stopTest();
        revDetail = [SELECT Id,Shared_Opportunity_Revenue__c FROM Opportunity_Revenue_Schedule__c WHERE Id = :revDetail.Id];
        //System.assertEquals(6000,revDetail.Shared_Opportunity_Revenue__c);
    }*/
    
    
    static testMethod void testRevDetailInsertYearly() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_YEARLY;
        Test.startTest();
            insert revDetail;
        Test.stopTest();
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 1);
    }
    
    
    static testMethod void testRevDetailInsertQuarterlyWithNoofInstallments() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_QUARTERLY;
        revDetail.Shared_Number_of_Installments__c = 12;
        Test.startTest();
            insert revDetail;
        Test.stopTest();
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 12);
    }
    
    
    static testMethod void testRevDetailNoUpdate() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_YEARLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 1);
        Test.startTest();
            update revDetail;
        Test.stopTest();
        List<Schedule__c> newrevScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        System.assertEquals(true,newrevScdls != null);
        System.assertEquals(true,newrevScdls.size() == 1);
        System.assertEquals(true,newrevScdls[0].Id == revScdls[0].Id);
    }
    
    
    static testMethod void testRevDetailUpdateWithOppProduct() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        PriceBook2 testPricebook = new PriceBook2();
        NMD_TestDataManager td = new NMD_TestDataManager();
        Id pricebookId = Test.getStandardPricebookId();
        
        testPricebook.Name = 'NMD';
        testPricebook.IsActive = true;
        testPricebook.Description='Neuromod';
        insert testPricebook;
        
        
        
        Product2 prod = td.newProduct('Endo');
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = testPricebook.id, Product2Id = prod.Id, UnitPrice = 1000, IsActive = true, UseStandardPrice = false);
        insert standardPrice1;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppty.Id,PricebookEntryId = standardPrice.Id,Quantity = 1, UnitPrice = oppty.Amount);
        insert oli;
        
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = 6000;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_YEARLY;
        Test.startTest();
            insert revDetail;
        Test.stopTest();
        
        oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        System.assertEquals(6000,oppty.Amount);
    }
    
    
    static testMethod void testRevScheduleUpdate() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_YEARLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 1);
        revScdls[0].Revenue__c = 6000;
        Test.startTest();
            RevenueDetailsManager.isRevDtlMgr = false;
            update revScdls[0];
        Test.stopTest();
        revDetail = [SELECT Id,Shared_Opportunity_Revenue__c FROM Opportunity_Revenue_Schedule__c WHERE Id = :revDetail.Id];
        System.assertEquals(6000,revDetail.Shared_Opportunity_Revenue__c);
        oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        //System.assertEquals(6000,oppty.Amount);
    }
    
    
    static testMethod void testRevScheduleDelete() {
        Opportunity oppty = [SELECT Id,Amount,CloseDate FROM Opportunity WHERE Name = :OPPTY_NAME];
        Opportunity_Revenue_Schedule__c revDetail = new Opportunity_Revenue_Schedule__c();
        revDetail.Opportunity__c = oppty.Id;
        revDetail.Shared_Opportunity_Revenue__c = oppty.Amount;
        revDetail.Start_Date__c = System.today();
        revDetail.Installment_Period__c = RevenueDetailsManager.SCHD_YEARLY;
        insert revDetail;
        System.assertEquals(true,revDetail.Id != null);
        List<Schedule__c> revScdls = [SELECT Id FROM Schedule__c WHERE Revenue_Schedule__c = :revDetail.Id];
        System.assertEquals(true,revScdls != null);
        System.assertEquals(true,revScdls.size() == 1);
        Test.startTest();
            RevenueDetailsManager.isRevDtlMgr = false;
            delete revScdls[0];
        Test.stopTest();
    }
}
/*
    Description: SFDC-715 : 1st Part
                            On a single click user wants to update the primary (GI) and secondary territory (PULM) from the associated account.
                            GI territory starts with (900) and its description , Parent description and parent to parent description along with
                            PULM territory starts with (500) and its description , Parent description and parent to parent description will get populated.
                            
                            2nd Part
                            Once territory assigned to secondary territory(PULM) then add the opportunity team with only the user assigned in secondary territory
                            and the parent territory of secondary territory, needs to be in opportunity team and again from those users only the users having 
                            territory role as “Regional Manager” and “Territory Manager” those will be in opportunity team.
                            
    Created By: Shashank Gupta
    Created Date: 21/09/2016
    Test Class: Endo_PopolautePrmryScndryTrrtryDtlsTest
*/
public class Endo_PopolautePrimarySecondaryTrrtryDtls{
    // 1st Part
    public Opportunity oppty;
    public Id CurrentPageId; 
    public List<ObjectTerritory2Association> accountAssociatedTerr{get; set;}
    public string AccountId {get; set;}
    public set<Id> TerritoryId = new set<Id>();
    public List<Territory2> TerritoryList = new List<Territory2>();
    public string primaryTerritoryId {get; set;}
    public string secondaryTerritoryId {get; set;}
    public string secondaryTerritoryParentId {get; set;}
    public string parentOfSecondaryTerritoryParentId {get; set;}
    public boolean primaryTerr {get; set;}
    public boolean secondaryTerr {get; set;}
    
    // 2nd Part
    public list <UserTerritory2Association> territoryUsers = new list <UserTerritory2Association>();
    public list<User> territoryManagerRole = new list <User>();
    public list<User> regionalManagerRole = new list <User>();
    //public list<User> areaDirectorRole = new list <User>();
    public list <OpportunityShare> allOpptyTeamRecords = new list <OpportunityShare>();
    public list <OpportunityShare> addOpptyTeamShare = new list <OpportunityShare>();
    public list <OpportunityShare> delOpptyTeam = new list <OpportunityShare>();
    Public List<OpportunityTeamMember> oppTeam = new List<OpportunityTeamMember>();
    public Set<Id> UserIdSet = new Set<Id>();
    
    // Constructor
    public Endo_PopolautePrimarySecondaryTrrtryDtls (ApexPages.StandardController controller){      
        CurrentPageId = ApexPages.currentPage().getParameters().get('Id');    
        this.oppty = (Opportunity)controller.getRecord(); // Getting Current record Data
        AccountId = [Select id, Name, AccountId, Account.Id from Opportunity Where id=:CurrentPageId].Account.Id;
        system.debug('***String***'+AccountId); 
        primaryTerr = false;
        secondaryTerr = false;
        
        if(AccountId != Null){
            accountAssociatedTerr = [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id,Territory2.Name FROM ObjectTerritory2Association WHERE ObjectId =:AccountId];
            system.debug('***accountAssociatedTerr***'+accountAssociatedTerr);
        }
        for(ObjectTerritory2Association at: accountAssociatedTerr){
        system.debug('***at.Territory2.Name***'+at.Territory2.Name);
            if(at.Territory2.Name.contains('900') ){
                primaryTerr = true;
            }
            if(at.Territory2.Name.contains('500')){
                secondaryTerr = true;
            }
        }
    }
 
 
/* 
Update Primary (GI) and Secondary (PULM) territory on opportunity with auto populate the related description fields of parent and parent to parent territory.
Also add the opportunity team with only the user assigned in secondary territory and the parent territory of secondary territory needs to be in opportunity team 
and again from those users only the users having territory role as “Regional Manager” and “Territory Manager” those will be in opportunity team. 
*/
    Public Pagereference updateGITerritoryBtn(){ 
    system.debug('Inside**** Method');         
        if(!accountAssociatedTerr.isEmpty()){
            for (ObjectTerritory2Association t: accountAssociatedTerr){
                TerritoryId.add(t.Territory2Id);
            }
            system.debug('***TerritoryId***'+TerritoryId);
        }
        
        if(!TerritoryId.isEmpty()){
            TerritoryList = [SELECT Id,Description,DeveloperName,Name,Endo_Territory__c,ParentTerritory2Id,ParentTerritory2.Description,ParentTerritory2.ParentTerritory2Id, 
                                    ParentTerritory2.ParentTerritory2.Description,ParentTerritory2.ParentTerritory2.ParentTerritory2.Description FROM Territory2 WHERE Id in: TerritoryId];            
            system.debug('***TerritoryList***'+TerritoryList);
        }
        
        if(!TerritoryList.isEmpty()){
            for(Territory2 trr:TerritoryList){
                if(trr.Name.contains('900') && trr.Endo_Territory__c!= null){
                system.debug('***Inside 900***');
                    oppty.Territory2Id = trr.Id;
                    oppty.GI_Region_Description__c = trr.ParentTerritory2.Description;
                    oppty.GI_Area_Description__c = trr.ParentTerritory2.ParentTerritory2.Description;
                    primaryTerritoryId = trr.Id;
                    oppty.GI_Parent__c = trr.ParentTerritory2Id;
                    oppty.GI_Parent_Parent__c = trr.ParentTerritory2.ParentTerritory2Id;
                }                 
            }
            
            /*if(oppty.Territory2Id != null){
                oppty.Territory_Assigned__c = true;
            }
            else{
                oppty.Territory_Assigned__c = false;
            }*/
        }
        
        try{
            Database.Update(oppty, False);
        }
        Catch (DMLException e) {
          ApexPages.addMessages(e);
          return null;
        }
        
        // 2nd Part
        // Populate the Opportunity Team Data Starts From here
        /*territoryUsers = [SELECT Id,RoleInTerritory2,Territory2Id,UserId, User.Name 
                                 FROM UserTerritory2Association 
                                 WHERE Territory2Id =: primaryTerritoryId ];
        system.debug('***territoryUsers ***'+territoryUsers);
        
        for(UserTerritory2Association tu: territoryUsers){
            if(tu.RoleInTerritory2=='Area Director' ){
                areaDirectorRole.add(tu.User);
            }
        }              

        system.debug('***areaDirectorRole***'+areaDirectorRole);
        
        
        //Add area director
        for(User uAdd : areaDirectorRole){
            OpportunityTeamMember otm1 = new OpportunityTeamMember(OpportunityId = CurrentPageId,
            UserId = uAdd.Id,
            TeamMemberRole = 'Area Director',
            OpportunityAccessLevel = 'Edit');
            oppTeam.add(otm1);
        }
        
        system.debug('***oppTeam***'+oppTeam);
        
        if (oppTeam.size() > 0) { 
            Database.Saveresult[] dbSave1 = Database.insert(oppTeam, false); 
        }
        */
        // Populate the Opportunity Team Data Ends here
        
        return null;
    }
    
    Public Pagereference updatePULMTerritoryBtn(){
    system.debug('Inside**** Method');         
        if(!accountAssociatedTerr.isEmpty()){
            for (ObjectTerritory2Association t: accountAssociatedTerr){
                TerritoryId.add(t.Territory2Id);
            }
            system.debug('***TerritoryId***'+TerritoryId);
        }
        
        if(!TerritoryId.isEmpty()){
            TerritoryList = [SELECT Id,Description,DeveloperName,Name,Endo_Territory__c,ParentTerritory2Id,ParentTerritory2.Description,ParentTerritory2.ParentTerritory2Id, 
                                    ParentTerritory2.ParentTerritory2.Description, ParentTerritory2.Name,ParentTerritory2.ParentTerritory2.Name FROM Territory2 WHERE Id in: TerritoryId];            
            system.debug('***TerritoryList***'+TerritoryList);
        }
                 
        if(!TerritoryList.isEmpty()){
            for(Territory2 trr:TerritoryList){
                if(trr.Name.contains('500') && trr.Endo_Territory__c!= null){
                system.debug('***Inside 500***');
                    oppty.ENDO_Secondary_Territory__c = trr.Name ;
                    oppty.SecondaryTerritoryDescription__c = trr.Description;
                    oppty.SecondaryTerritoryRegion__c = trr.ParentTerritory2.Description ;
                    oppty.Secondary_Territory_Area__c = trr.ParentTerritory2.ParentTerritory2.Description;
                    secondaryTerritoryId = trr.id;
                    secondaryTerritoryParentId = trr.ParentTerritory2Id;
                    oppty.Parent_Of_Secondary_Territory__c = trr.ParentTerritory2Id;
                    oppty.Remove_Secondary_Territory__c =  false;
                    oppty.PULM_Parent__c = trr.ParentTerritory2.Name;
                    oppty.PULM_Parent_Parent__c = trr.ParentTerritory2.ParentTerritory2.Name;
                }    
            }
            system.debug('***secondaryTerritoryId***'+secondaryTerritoryId);
            system.debug('***secondaryTerritoryParentId***'+secondaryTerritoryParentId);
            system.debug('***parentOfSecondaryTerritoryParentId***'+parentOfSecondaryTerritoryParentId);
        }
        
        try{
            Database.Update(oppty, False);
        }
        Catch (DMLException e) {
          ApexPages.addMessages(e);
          return null;
        }
        // Populate the territory and description fields Ends here
        
        // 2nd Part
        // Populate the Opportunity Team Data Starts From here
        territoryUsers = [SELECT Id,RoleInTerritory2,Territory2Id,UserId, User.Name 
                                 FROM UserTerritory2Association 
                                 WHERE Territory2Id =: secondaryTerritoryId OR Territory2Id =:secondaryTerritoryParentId ];
        system.debug('***territoryUsers ***'+territoryUsers);
        
        for(UserTerritory2Association tu: territoryUsers){
            //UserIdSet.add(tu.UserId);
            if(tu.RoleInTerritory2=='Territory Manager' ){
                territoryManagerRole.add(tu.User);                              
            }
            if(tu.RoleInTerritory2=='Regional Manager' ){
                regionalManagerRole.add(tu.User);
            }
            /*if(tu.RoleInTerritory2=='Area Director' ){
                areaDirectorRole.add(tu.User);
            }*/
        }              
        
        system.debug('***territoryManagerRole***'+territoryManagerRole);
        system.debug('***regionalManagerRole***'+regionalManagerRole);
        //system.debug('***areaDirectorRole***'+areaDirectorRole);
                
        //Add territory manager
        for(User uAdd : territoryManagerRole){
            OpportunityTeamMember otm = new OpportunityTeamMember(OpportunityId = CurrentPageId,
            UserId = uAdd.Id,
            TeamMemberRole = 'Territory Manager',
            OpportunityAccessLevel = 'Edit');
            oppTeam.add(otm);
        }
        //Add regional manager
        for(User uAdd : regionalManagerRole){
            OpportunityTeamMember otm1 = new OpportunityTeamMember(OpportunityId = CurrentPageId,
            UserId = uAdd.Id,
            TeamMemberRole = 'Regional Manager',
            OpportunityAccessLevel = 'Edit');
            oppTeam.add(otm1);
        }
        //Add area director
        /*for(User uAdd : areaDirectorRole){
            OpportunityTeamMember otm1 = new OpportunityTeamMember(OpportunityId = CurrentPageId,
            UserId = uAdd.Id,
            TeamMemberRole = 'Area Director',
            OpportunityAccessLevel = 'Edit');
            oppTeam.add(otm1);
        }*/
        
        system.debug('***oppTeam***'+oppTeam);
        
        if (oppTeam.size() > 0) { 
            Database.Saveresult[] dbSave1 = Database.insert(oppTeam, false); 
        }

        // Populate the Opportunity Team Data Ends here
               
        return null;
    } 
      
}
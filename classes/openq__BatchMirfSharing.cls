/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BatchMirfSharing implements Database.Batchable<SObject>, Database.Stateful, System.Schedulable {
    global void execute(System.SchedulableContext ctx) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global void scheduleNext() {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}

@IsTest
public class CustomerJsonRequest_Test {
    
    static testMethod void testParse() {
        String json = '{'+
        '                \"CustomerInfo\": {'+
        '                                \"partnerAddressDetail\": \"true\",'+
        '                                \"soldToAccountNumber\": \"11\",'+
        '                                \"salesOrganization\": \"US10\"'+
        '                }'+
        '}';
        CustomerJsonRequest obj = CustomerJsonRequest.parse(json);
        System.assert(obj != null);
    }
}
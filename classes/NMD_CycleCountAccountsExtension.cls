/**
* Controller class for the VF page CycleCountAccounts
*
* @Author salesforce Services
* @Date 2015-06-24
*/
public with sharing class NMD_CycleCountAccountsExtension extends NMD_ExtensionBase {

        @TestVisible
        final static String AREA = 'Area';
        @TestVisible
        final static String REGION = 'Region';
        @TestVisible
        final static String ACCOUNT = 'Account';

        final static Id RECTYPE_CYCLECOUNT = Schema.SObjectType.List__c.getRecordTypeInfosByName().get('Cycle Count Users').getRecordTypeId();
        final static Integer PAGESIZE = 20;
        final static String ACC_STATUS = 'Inactive';

        final ApexPages.StandardController sc;
        final Id neuromodRoleId;

        private Set<Id> savedAcctIds;
        private NMD_UserRoleManager roleManager;

        public Boolean canEdit { get; private set; }
        public String searchString { get; set; }
        public String searchType { get; set; }
        public String removeType { get; set; }
        public List<SelectOption> searchTypes { get; private set; }
        public List<SelectOption> removeTypes { get; private set; }
        public List<SelectOption> availableItems { get; private set; }
        public List<SelectOption> selectedItems { get; private set; }
        public List<ListItem> savedItems { get; private set; }
        
        public Integer currentPage { get; private set; }
        public Boolean isAccAdded;
        //**Added for Remove all by region
        public List<ListItem> savedItems1 = new List<ListItem>();
    public Integer maxPage { 
        get { return Math.ceil(this.savedAcctIds.size() / PAGESIZE).intValue(); } 
    }

        public Boolean orderAsc { get; set; }
    public String orderBy { 
        get; 
        set {
                if (orderBy != value) {
                        this.currentPage = 0;
                        this.orderAsc = true;
                }
                orderBy = value;
            } 
        }

        private String fuzzySearch {
                get {
                        return '%' + this.searchString.replaceAll('\\*', '') + '%';
                }
        }

        public NMD_CycleCountAccountsExtension(ApexPages.StandardController sc) {
                
                this.sc = sc;
                this.canEdit = ((Cycle_Count__c)sc.getRecord()).Status__c == 'Draft';
                this.currentPage = 0;
                this.searchString = '';
                this.searchType = AREA;
                this.searchTypes = new List<SelectOption> {
                        new SelectOption(AREA, AREA),
                        new SelectOption(REGION, REGION),
                        new SelectOption(ACCOUNT, ACCOUNT)
                };

                this.availableItems = new List<SelectOption>{};
                this.selectedItems = new List<SelectOption>{};
                this.removeTypes = new List<SelectOption>{};
                this.savedItems = new List<ListItem>{};
                this.savedAcctIds = new Set<Id>{};

                this.orderBy = 'Inventory_Account__r.Name';
                this.orderAsc = true;
                this.isAccAdded =true;
                // load role manager
                this.roleManager = new NMD_UserRoleManager();
                
                fetchSavedItems();

                if (this.canEdit) {
                        // load initial search state
                        doSearch();
                }

        }

        public void updateSearchType() {

                this.searchString = '';
                doSearch();

        }

        public void doSearch() {

                if (this.searchType == AREA) {
                        fetchAreas();
                }
                else if (this.searchType == REGION) {
                        fetchRegions();
                }
                else if (this.searchType == ACCOUNT) {
                        fetchAccounts();
                }

                if (this.availableItems.size() >= 100) {
                        newWarningMessage('Your search returned more than 100 rows. Only the first 100 are displayed. Please refine search criteria.');

                        while (this.availableItems.size() > 100) {
                                this.availableItems.remove(this.availableItems.size() - 1);
                        }

                }

        }       

        public void addSelectedItems() {

                Set<Id> acctIds = new Set<Id>{};
                Set<Id> roleIds = new Set<Id>{};
                List<Id> AreaIdList = new List<Id>();
                List<UserRole> CombinedAreaRegionList = new List<UserRole>();
                List<UserRole> AreaschildRegion = new List<userRole>();
                String SerchArea ='%'+'Area'+'%';
                String SerchRegion ='%'+'Region'+'%';
                //this.removeTypes = this.selectedItems;
                for (SelectOption so : this.selectedItems) {
                        Id recId = (Id)so.getValue();
                        if (recId.getSObjectType() == UserRole.SObjectType) {
                                roleIds.add(recId);
                                //*added by amitabh
                                system.debug('Selected values=='+so.getValue()+so.getLabel());
                                //this.removeTypes.add(so);
                                system.debug('Selected remove values=='+removeTypes);
                                
                                
                        }
                        else {
                                acctIds.add(recId);
                        }
                }
                //**below code added to convert selected Area to Region and add it to removeTypes picklist 
                /*if (!roleIds.isEmpty()) {
                    List<UserRole> AreaList =[select Id,Name from UserRole where Id in :roleIds and (Not Name Like : SerchRegion) ];
                    if(!AreaList.isEmpty()){
                        for(UserRole ur:AreaList){
                            AreaIdList.add(ur.id);
                        }
                         AreaschildRegion =[select Id,Name from UserRole where ParentRoleId in :AreaIdList];
                    }   
                    system.debug('AreaschildRegion'+AreaschildRegion);
                    List<UserRole> RegionList =[select Id,Name from UserRole where Id in :roleIds and Name Like : SerchRegion ];
                    system.debug('RegionList =='+RegionList );
                    
                    if(!AreaschildRegion.isEmpty()|| !RegionList.isEmpty()){
                        RegionList.addAll(AreaschildRegion);
                        system.debug('RegionList after =='+RegionList );
                        CombinedAreaRegionList = new List<UserRole>(RegionList);
                    }
                    for(UserRole u:CombinedAreaRegionList ){
                        this.removeTypes.add(new SelectOption(u.Id, REGION + ': ' + u.Name));
                        
                    }
                } */
                //** above changes ends here        
                if (!roleIds.isEmpty()) {

                        Set<Id> childIds = new Map<Id,UserRole>([select Id from UserRole where ParentRoleId in :roleIds]).keySet().clone();
                        while (roleIds.addAll(childIds)) {
                                childIds = new Map<Id,UserRole>([select Id from UserRole where ParentRoleId in :childIds]).keySet().clone();
                        }

                        acctIds.addAll(new Map<Id,Account>([
                                select Name 
                                from Account 
                                where Id not in :this.savedAcctIds
                                and Owner.UserRoleId in :roleIds 
                                and RecordTypeId = :AccountManager.RECTYPE_CONSIGNMENT  and (status__c != : ACC_STATUS and Type != 'DNU')
                        ]).keySet());

                }

                List<List__c> newLists = new List<List__c>{};

                if (!acctIds.isEmpty()) {
                        this.isAccAdded= true;
                        for (Id acctId : acctIds) {
                                if (!this.savedAcctIds.contains(acctId)) {
                                        newLists.add(new List__c(
                                                Cycle_Count__c = this.sc.getId(),
                                                Inventory_Account__c = acctId,
                                                RecordTypeId = RECTYPE_CYCLECOUNT
                                        ));
                                }
                        }

                        //insert newLists;
                        DML.save(this, newLists);

                }

                fetchSavedItems();

                this.selectedItems.clear();
                doSearch();

                newInfoMessage(String.format('{0} Account{1} added.', new List<String> { 
                        String.valueOf(newLists.size()),
                        newLists.size() == 1 ? '' : 's'
                }));

        }

        public void removeSelectedItems() {

                List<List__c> oldLists = new List<List__c>{};
                for (ListItem item : this.savedItems) {
                        if (item.selected) {
                                oldLists.add(item.record);
                        }
                }

                if (!oldLists.isEmpty()) {
                        //delete oldLists;
                        DML.remove(this, oldLists);
                }

                newInfoMessage(String.format('{0} Account{1} removed.', new List<String> { 
                        String.valueOf(oldLists.size()),
                        oldLists.size() == 1 ? '' : 's'
                }));

                firstPage();

        }
        //*below method added by amitabh 
        public void removeAllbyRegion(){
            
            List<List__c> oldLists1 = new List<List__c>{};
            system.debug('display selected region=>'+removeType);
            
            
            UserRole u;
             if (removeType != null) {
            u = [Select Name From UserRole Where Id = : removeType];
            List<String> removetypelist = new List<String>();
            Id cycleCountId = this.sc.getId();
            savedItems1.clear();
            
            //*removetypelist will hold the list of values in select option list
            /*for(SelectOption so:removeTypes){
                removetypelist.add(so.getValue());
                system.debug('removetypelist='+removetypelist);
            }*/
             
            system.debug('display selected region=>'+u.Name);
            
            //**code below to fetch all the account of sected region
            for (List__c item : Database.query(
                        'select ' +
                                'Inventory_Account__c, ' +
                                'Inventory_Account__r.ShippingCity, ' +
                                'Inventory_Account__r.ShippingState, ' +
                                'Inventory_Account__r.OwnerId, ' +
                                'Inventory_Account__r.Owner.UserRoleId ' +
                        'from List__c ' +
                        'where Cycle_Count__c = :cycleCountId ' +
                        'and RecordTypeId = :RECTYPE_CYCLECOUNT ' 
                        
                )) {

                        // have to query all so that all records Ids can be stored.  paginate progmatically
                        

                                UserRole areaRole;
                                UserRole regionRole = this.roleManager.getUserRegion(item.Inventory_Account__r.Owner.UserRoleId);
                                if (regionRole != null) {
                                        areaRole = this.roleManager.userRoles.get(regionRole.ParentRoleId);
                                }

                                String areaName = areaRole == null ? '' : areaRole.Name;
                                String regionName = regionRole == null ? '' : regionRole.Name;

                                this.savedItems1.add(new ListItem(item, areaName, regionName));

                                
                }
            
            
            for (ListItem item : this.savedItems1) {
                    system.debug('item rgn name='+item.regionName);
                        if (item.regionName == u.Name) {
                                oldLists1.add(item.record);
                                system.debug('OldList1='+oldLists1);
                        }
                }
    }
                if (!oldLists1.isEmpty()) {
                        //delete oldLists1;
                        DML.remove(this, oldLists1);
                }
                if(removeTypes.Size()>0){
                    for(integer i=0;i<removeTypes.Size();i++)
                        {
                               if(removeTypes[i].getValue() == u.Id){
                                    removeTypes.remove(i);
                                    //system.debug('removeTypes[i].getValue()==>'+removeTypes[i].getValue()+'u.Id==>'+u.Id);
                                 }   
                        }
                }   
                newInfoMessage(String.format('{0} Account{1} removed.', new List<String> { 
                        String.valueOf(oldLists1.size()),
                        oldLists1.size() == 1 ? '' : 's'
                }));

                firstPage();

        }       
        public void firstPage() {
        this.currentPage = 0;
        fetchSavedItems();
    }

    public void prevPage() {
        this.currentPage = this.currentPage == 0 ? 0 : this.currentPage - 1;
        fetchSavedItems();
    }

    public void nextPage() {
        this.currentPage = this.currentPage == this.maxPage ? this.currentPage : this.currentPage + 1;
        fetchSavedItems();
    }

    public void lastPage() {
        this.currentPage = this.maxPage;
        fetchSavedItems();
    }

        private void fetchAreas() {

                this.availableItems.clear();
                for (UserRole role : this.roleManager.areaRoles.values()) {
                        if (String.isBlank(this.searchString) || role.Name.containsIgnoreCase(this.searchString)) {
                                this.availableItems.add(new SelectOption(role.Id, AREA + ': ' + role.Name));
                        }
                }
                this.availableItems.sort();

        }

        private void fetchRegions() {

                this.availableItems.clear();
                for (UserRole role : this.roleManager.regionRoles.values()) {
                        if (String.isBlank(this.searchString) || role.Name.containsIgnoreCase(this.searchString)) {
                                this.availableItems.add(new SelectOption(role.Id, REGION + ': ' + role.Name));
                        }
                }
                this.availableItems.sort();

        }

        private void fetchAccounts() {
                
                Set<Id> roleIds = this.roleManager.userRoles.keySet();
                Id consignmentId = AccountManager.RECTYPE_CONSIGNMENT;
                String soql = String.format('select Name from Account where Owner.UserRoleId in :roleIds and RecordTypeId = :consignmentId {0} order by Name limit 100', new List<String> {
                        String.isBlank(this.searchString) ? '' : 'and Name like :fuzzySearch '
                });

                this.availableItems.clear();
                for (Account acct : Database.query(soql)) {
                        this.availableItems.add(new SelectOption(acct.Id, ACCOUNT + ': ' + acct.Name));
                }

        }
        //public set<UserRole> ur = new Set<UserRole>();
        public void fetchSavedItems() {

                this.savedItems = new List<ListItem>{};
                this.savedAcctIds = new Set<Id>{};
                set<UserRole> ur = new Set<UserRole>();
                set<UserRole> urfinal = new Set<UserRole>();
                Integer index = 0;
                //*clear the picklist of remove selected region
                //this.removeTypes.clear();
                Integer offset = this.currentPage * PAGESIZE;
                Id cycleCountId = this.sc.getId();

                for (List__c item : Database.query(
                        'select ' +
                                'Inventory_Account__c, ' +
                                'Inventory_Account__r.ShippingCity, ' +
                                'Inventory_Account__r.ShippingState, ' +
                                'Inventory_Account__r.OwnerId, ' +
                                'Inventory_Account__r.Owner.UserRoleId ' +
                        'from List__c ' +
                        'where Cycle_Count__c = :cycleCountId ' +
                        'and RecordTypeId = :RECTYPE_CYCLECOUNT ' +
                        'order by ' + this.orderBy + (this.orderAsc ? ' asc ' : ' desc ')
                )) {

                        // have to query all so that all records Ids can be stored.  paginate progmatically
                        if (index >= offset && index <= offset + PAGESIZE - 1) {

                                UserRole areaRole;
                                UserRole regionRole = this.roleManager.getUserRegion(item.Inventory_Account__r.Owner.UserRoleId);
                                if (regionRole != null) {
                                        areaRole = this.roleManager.userRoles.get(regionRole.ParentRoleId);
                                }

                                String areaName = areaRole == null ? '' : areaRole.Name;
                                String regionName = regionRole == null ? '' : regionRole.Name;

                                this.savedItems.add(new ListItem(item, areaName, regionName));
                                system.debug('Saved items to check area=='+savedItems);
                                //this.removeTypes.add(new SelectOption(regionRole.Id, REGION + ': ' + regionName));https://bsci--dev2--c.cs61.visual.force.com/s.gif
                                
                                 if (regionRole != null)
                                ur.add(regionRole);
                                system.debug('ur list =='+ur);
                        }
                        
                        this.savedAcctIds.add(item.Inventory_Account__c);
                        index++;
                       
                }
                system.debug('No of values in US=>'+ur+'Is acc added=>'+this.isAccAdded);
                if(!ur.isEmpty() && this.isAccAdded == true){
                        this.isAccAdded = false;
                        for(UserRole uniqueRegion:ur){
                            Set<SelectOption> uniRemoveTypes= new Set<SelectOption>();
                            
                            uniRemoveTypes = new Set<SelectOption>(this.removeTypes);
                            
                                SelectOption tempSo =new SelectOption(uniqueRegion.Id, REGION + ': ' + uniqueRegion.Name);
                                if(!uniRemoveTypes.contains(tempSo)){
                                    this.removeTypes.add(tempSo);
                                    //this.removeTypes.add(new SelectOption(uniqueRegion.Id, REGION + ': ' + uniqueRegion.Name));
                                }
                                
                        }                        
                } 
                

        }

        public class ListItem {

                public Boolean selected { get; set; }
                public List__c record { get; private set; }
                public String areaName { get; private set; }
                public String regionName { get; private set; }
                public String location { get; private set; }

                public ListItem(List__c item, String areaName, String regionName) {
                        this.selected = false;
                        this.record = item;
                        this.areaName = areaName;
                        this.regionName = regionName;

                        this.location = '';
                        if (String.isNotBlank(this.record.Inventory_Account__r.ShippingCity)
                         && String.isNotBlank(this.record.Inventory_Account__r.ShippingState)
                        ) {
                                this.location = String.format('{0}, {1}', new List<String> {
                                        this.record.Inventory_Account__r.ShippingCity,
                                        this.record.Inventory_Account__r.ShippingState
                                });
                        }
                        else if (String.isNotBlank(this.record.Inventory_Account__r.ShippingCity)
                         && String.isBlank(this.record.Inventory_Account__r.ShippingState)
                        ) {
                                this.location = this.record.Inventory_Account__r.ShippingCity;
                        }
                        else if (String.isBlank(this.record.Inventory_Account__r.ShippingCity)
                         && String.isNotBlank(this.record.Inventory_Account__r.ShippingState)
                        ) {
                                this.location = this.record.Inventory_Account__r.ShippingState;
                        }

                }

        }

}
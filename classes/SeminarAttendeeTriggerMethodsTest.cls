/**
* Test Class for the TriggerHandler methods for SeminarAttendeeTriggerMethods class.       
*
* @CreatedBy   Payal Ahuja
* @CreatedDate 06/10/2016
*/
@isTest(SeeAllData=false)
public class SeminarAttendeeTriggerMethodsTest {

   static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Patient__c.getRecordTypeInfosByName();
   public static final Id RECTYPE_UROMENSHEALTH = RECTYPES.get('Uro/PH Men\'s Health').getRecordTypeId();
   
   Public static testmethod void createSeminarAttendeeTestMethod(){   
       
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        
     //@TestSetup
      NMD_TestDataManager td = new NMD_TestDataManager();
        
     //create test patient records
    
       Patient__c patient = new Patient__c(
       RecordTypeId = RECTYPE_UROMENSHEALTH,
       //Patient_First_Name__c = 'Test First',
       //Patient_Last_Name__c = 'Test Last',
       Patient_Email_Address__c = 'testemail@gmail.com',
       Address_1__c = 'Test Address 1 ' ,
       Address_2__c = 'Test Address 2' ,
       City__c = 'Test City' ,
       State__c = 'Test State',
       Zip__c = 'Test 567',
       Country__c = 'CO',
       Patient_Mobile_Number__c = '9776001234' ,
       Shared_Daytime_Phone__c = '9776005678' ,
       Shared_Evening_Phone__c = '9776015678' ,
       Uro_Ph_ED_Treatment__c = 'Yes ' ,
       Source__c = 'AUS Medical ID ' ,
       Uro_Ph_ED_Duration__c  = '0 - 6 Months' ,
       Shared_Preferred_Contact_Method__c = 'Phone' ,
       Uro_Ph_ED_Oral_Medications__c  = 'Have Used',
       Uro_Ph_Penile_Injections__c = 'Have Used' ,
       Uro_Ph_Suppositories_MUSE__c = 'Have Used' ,
       Uro_Ph_Vacuum_Erection_Device_VED__c = 'Have Used',
       Uro_Ph_ED_Implant_Received__c  = TRUE,
       Uro_Ph_Opt_in_Date__c = Date.parse('12/15/16'),
       Patient_First_Name__c = 'Test First',
       Patient_Last_Name__c = 'Test Last'     
     );
      insert patient;
      
     //create test Seminar records
     Shared_Seminar__c seminar = new Shared_Seminar__c(
     Shared_Seminar_Type__c= ' Test Seminar',
     Name = 'test 123',
     Shared_Event_Topics__c = ' Test Event'  
     );
     insert seminar;
     
    //create test seminar attendee records
    Shared_Seminar_Attendee__c seminarAttendee = new Shared_Seminar_Attendee__c(
    Shared_Patient_Email_Address__c = 'testemail@gmail.com', 
   // Shared_Seminar__c = seminar.Id ,
    Shared_Seminar_ID__c = 'test 123' ,
    Shared_Address_1__c =  'Test Address 1 ' ,
    Shared_Address_2__c =  'Test Address 2' ,
    Shared_City__c =  'Test City', 
    Shared_State__c = 'AK' , 
    Shared_Zip__c = 'Test 567' ,
    Shared_Country__c = 'CO',
    Shared_Mobile_Number__c= '9776001234' ,
    Shared_Daytime_Phone__c = '9776005678' ,
    Shared_Evening_Phone__c = '9776015678' ,
    Shared_ED_Treatment__c = 'Yes ' ,
    Shared_Source__c = 'AUS Medical ID' ,
    Shared_ED_Duration__c = '0 - 6 Months',
    Shared_Preferred_Contact_Method__c = 'Phone' ,
    Shared_ED_Oral_Medications__c = 'Have Used',
    Shared_Penile_Injections__c = 'Have Used' ,
    Shared_Suppositories_MUSE__c = 'Have Used' ,
    Shared_Vacuum_Erection_Device_VED__c = 'Have Used',
    Shared_ED_Implant_Received__c = TRUE,
    Shared_Opt_in_Date__c = Date.parse('12/15/16'), 
    Shared_Patient_First_Name__c  = 'Test First',
    Shared_Patient_Last_Name__c = 'Test Last' 
    );
    
    Test.startTest();
    
    Shared_Seminar_Attendee__c seminarAttendee1 = new Shared_Seminar_Attendee__c(
    Shared_Patient_Email_Address__c = 'testemail11@gmail.com',
   // Shared_Seminar__c = seminar.Id,
    Shared_Seminar_ID__c = 'test 1234',
    Shared_Address_1__c =  'Test Address 21 ' ,
    Shared_Address_2__c =  'Test Address 21' ,
    Shared_City__c =  'Test City 1',
    Shared_State__c = 'AK' ,
    Shared_Zip__c = 'Test 5678' ,
    Shared_Country__c = 'C1',
    Shared_Mobile_Number__c= '9776001235' ,
    Shared_Daytime_Phone__c = '9776005679' ,
    Shared_Evening_Phone__c = '9776025678' ,
    Shared_ED_Treatment__c = 'Yes ' ,
    Shared_Source__c = 'AMSMensHealth.com' ,
    Shared_ED_Duration__c = '0 - 6 Months ',
    Shared_Preferred_Contact_Method__c = 'Phone ' ,
    Shared_ED_Oral_Medications__c = 'Have Used',
    Shared_Penile_Injections__c = 'Have Used ' ,
    Shared_Suppositories_MUSE__c = 'Have Used',
    Shared_Vacuum_Erection_Device_VED__c = 'Have Used',
    Shared_ED_Implant_Received__c = TRUE,
    Shared_Opt_in_Date__c = Date.parse('12/15/16'), 
    Shared_Patient_First_Name__c  = 'Test First',
    Shared_Patient_Last_Name__c = 'Test Last'  
    );
    
    Shared_Seminar_Attendee__c seminarAttendee2 = new Shared_Seminar_Attendee__c(
    Shared_Patient_Email_Address__c = 'testemail11@gmail.com',
   // Shared_Seminar__c = seminar.Id,
    Shared_Seminar_ID__c = '',
    Shared_Address_1__c =  'Test Address 21 ' ,
    Shared_Address_2__c =  'Test Address 21' ,
    Shared_City__c =  'Test City 1',
    Shared_State__c = 'AK' ,
    Shared_Zip__c = 'Test 5678' ,
    Shared_Country__c = 'C1',
    Shared_Mobile_Number__c= '9776001235' ,
    Shared_Daytime_Phone__c = '9776005679' ,
    Shared_Evening_Phone__c = '9776025678' ,
    Shared_ED_Treatment__c = 'Yes ' ,
    Shared_Source__c = 'AMSMensHealth.com' ,
    Shared_ED_Duration__c = '0 - 6 Months ',
    Shared_Preferred_Contact_Method__c = 'Phone ' ,
    Shared_ED_Oral_Medications__c = 'Have Used',
    Shared_Penile_Injections__c = 'Have Used ' ,
    Shared_Suppositories_MUSE__c = 'Have Used',
    Shared_Vacuum_Erection_Device_VED__c = 'Have Used',
    Shared_ED_Implant_Received__c = TRUE,
    Shared_Opt_in_Date__c = Date.parse('12/15/16'), 
    Shared_Patient_First_Name__c  = 'Test First',
    Shared_Patient_Last_Name__c = 'Test Last'  
    );
    
    insert new List<Shared_Seminar_Attendee__c > {
       seminarAttendee , seminarAttendee1, seminarAttendee2
    };
    
    List<Shared_Seminar_Attendee__c> lstSemAtt = [Select id, Name,Shared_Patient__c,Shared_Seminar_ID__c from Shared_Seminar_Attendee__c where id =:seminarAttendee.id];
    System.debug('** Seminar Attendee**' +lstSemAtt.get(0).Shared_Patient__c);
    System.assertEquals( lstSemAtt.get(0).Shared_Patient__c, patient.Id);
    
    Test.stopTest();
    
     }
   
   }
}
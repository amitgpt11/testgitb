/**
 *Description: This class is used as controller for Footer Component
*/
global class FooterController {
    
    public static String footerImageUrl    {get;set;}
    public static String footerCode        {get;set;}
    public static String pageName          {get;set;}    
    public static Boston_Scientific_Header_Footer_Config__c objHeaderFooterConfig {get;set;}
    
    
    global FooterController(){
        
        objHeaderFooterConfig = Boston_Scientific_Header_Footer_Config__c.getValues('Default');
        
        if(objHeaderFooterConfig.Footer_Logo_Id__c != null && Boston_Scientific_Config__c.getValues('Default').Boston_Document_Content_Base_Url__c != null){
                
            footerImageUrl = Boston_Scientific_Config__c.getValues('Default').Boston_Document_Content_Base_Url__c+'/servlet/servlet.ImageServer?id='+objHeaderFooterConfig.Footer_Logo_Id__c+
                '&oid='+userinfo.getOrganizationId(); 
        }
        
    }
    
    @RemoteAction
    global static String currentPageName(String strPageName) {
        
        //And on load of page, we call this method by passing {!$CurrentPage.Name}
        system.debug('strPageName=================='+strPageName);
        pageName = strPageName;
        
        List<Shared_Community_METIS_Code__c> lstMETISCode = new List<Shared_Community_METIS_Code__c>();
            
        User ObjUser = [Select Shared_Community_Division__c, Country  
            From User
            Where Id =: UserInfo.getUserId()];
        
//      String pageName = ApexPages.currentPage().getUrl().split('apex/')[1];
        
//      if(pageName.contains('?')) {
            
//         pageName = ApexPages.currentPage().getUrl().substringBetween('/apex/', '?');    
//      }
        
        system.debug('strPageName=====CONSTRUCTOR============='+PageName);
        lstMETISCode = [Select Id, Shared_METIS_Code__c, Shared_Community_Page__c, Shared_Community_Page__r.Shared_Page_Name__c
            From Shared_Community_METIS_Code__c 
            Where Shared_Community_Page__r.Shared_Page_Name__c =: pageName 
            AND Shared_Division__c =: ObjUser.Shared_Community_Division__c
            AND Shared_Country__c =: ObjUser.Country
            Order By Shared_Date_Assigned__c Desc];
            
        if(!lstMETISCode.isEmpty()) {
            
            footerCode = lstMETISCode[0].Shared_METIS_Code__c;
        }else {
            
            lstMETISCode = [Select Id, Shared_METIS_Code__c
                From Shared_Community_METIS_Code__c 
                where Shared_Community_Page__r.Shared_Page_Name__c =: pageName 
                AND Shared_Division__c =: ObjUser.Shared_Community_Division__c
                AND Shared_Country__c =: 'Default'
                Order By Shared_Date_Assigned__c Desc];
        
            if(!lstMETISCode.isEmpty()) 
                footerCode = lstMETISCode[0].Shared_METIS_Code__c;
        }   
        
        return footerCode;
    }
}
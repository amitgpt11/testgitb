@isTest
private class BSC_UtilTest {

	private static testMethod void test() {
        
        System.assertEquals(false,BSC_Util.isCommunityUser());
        User usr = Test_DataCreator.createCommunityUser();
        String AccountId = [SELECT contact.AccountId FROM User WHERE id=: usr.Id].contact.AccountId;
        Test_DataCreator.createUserSalesOrganisation(AccountId);
        
        Product2 objParentProd = Test_DataCreator.createProductEndo('test');
        List<Product2> objProducts = new List<Product2>([SELECT Id FROM Product2]);
        
        system.runAs(usr){
            
            System.assertEquals(true,BSC_Util.isCommunityUser());
            Set<String> setSalesOrg = BSC_Util.fetchSharedOrgForCommunityUser(AccountId);
            system.assertNotEquals(0, setSalesOrg.size());
            
            for(Product2 objProd : objProducts){
                
                Test_DataCreator.createSharedProductExtension(objProd.Id);
            }
            
            system.assertNotEquals(0, BSC_Util.fetchProductExtensionsForCommunityUser(setSalesOrg).size());
        }
	}
}
/*
 * User Story: SFDC-11, AC1: Need to update territory descriptions for all GI/PULM - All under ENUS, When Data come from HANA fro Territoy Model (Custom Object).
 * Created By: Shashank Gupta (Accenture Team)
 * Creatd Date: 12th Oct, 2016
 * Schedulable class : ScheduleEndo_UpdateTerritoryDescription
*/
global class Endo_UpdateTerritoryDescription implements Database.Batchable<sObject>
{
    public Map<string, Territory_Model__c> territoryModelMap = new Map<string, Territory_Model__c>();
    public String query = 'SELECT id, Area_Name__c,BU__c,Country__c, Delete_Indicator__c, Division__c, External_Id__c, Region_Name__c, Sales_Group__c, Sales_Office__c, Sales_Org__c, Status__c, Type__c, Name, Division_Description__c, Region_Description__c, Territory_Description__c, Territory_Name__c,LastModifiedDate FROM Territory_Model__c ORDER BY LastModifiedDate ASC';
    
    public List<Territory2> terrList = new List<Territory2>();    
    public Map<id, Territory2> territoryMapToCreate = new Map<id,Territory2>();
    public String UltimateParentTerritory = System.Label.ENUSTerritory;
    public List<Territory2> endoTerritories = new List<Territory2>();
    //public String str = UltimateParentTerritory;
    public List<Territory2> queryTerrList = [SELECT Id, Name, Description,Territory2TypeId, DeveloperName, ENDO_Territory_Description__c,No_Of_Parents_7_To_10__c,No_Of_Parents_1_To_6__c FROM Territory2 ];//where No_Of_Parents_1_To_6__c LIKE : str];
    
    Map<string,Territory_Model_Record_Pick__c> pickLastModifiedCustomSttngMap = Territory_Model_Record_Pick__c.getAll();  
    
    // Constructor
    global Endo_UpdateTerritoryDescription ()
    {} 
    /****************************************************************
*  start(Database.BatchableContext BC)
*****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('Inside Strat method');        
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
*  execute(Database.BatchableContext BC, List scope)
**********************************************************************/
    global void execute(Database.BatchableContext BC, List<Territory_Model__c> scope)
    {                
       System.debug('ISO Code: '+pickLastModifiedCustomSttngMap.get('Last Modified Today').Pick_Last_Modified_Today__c);
       system.debug('**InTrue**'+scope.size());
       
        for(Territory_Model__c tm: scope){
        dateTime last = tm.LastModifiedDate;
            if(pickLastModifiedCustomSttngMap.get('Last Modified Today').Pick_Last_Modified_Today__c != null){
                if(pickLastModifiedCustomSttngMap.get('Last Modified Today').Pick_Last_Modified_Today__c){
                    system.debug('**tm.LastModifiedDate**'+date.newinstance(last.year(), last.month(), last.day())+'*******Tody****'+date.today());
                    if(date.newinstance(last.year(), last.month(), last.day()) == date.Today()){
                        territoryModelMap.put(tm.Territory_Name__c, tm);
                    }
                }
                
                if(!pickLastModifiedCustomSttngMap.get('Last Modified Today').Pick_Last_Modified_Today__c){
                    territoryModelMap.put(tm.Territory_Name__c, tm);
                }
            }   
        }
        system.debug('territoryModelMap--> '+territoryModelMap.size());        
            
        for(Territory2 t: queryTerrList){
            if(t.No_Of_Parents_1_To_6__c!=null && t.No_Of_Parents_1_To_6__c.contains(UltimateParentTerritory)){
                endoTerritories.add(t);
            }
        }
        system.debug('*****************endoTerritories****************'+endoTerritories);
        
        for(Territory2 t: endoTerritories){
            if(territoryModelMap.containsKey(t.Name)){
                Territory_Model__c temp = territoryModelMap.get(t.Name);
                if(t.Description != temp.Territory_Description__c){
                    t.Description = temp.Territory_Description__c;
                    terrList.add(t);
                }
            }
        }        
        
        
        /*******************SFDC-934 Starts********************/
            //List of all existing territories in the system : queryTerrList
                 map<string,Territory2> territoryNameMap = new map<string,Territory2>();
                 string territoryModelId = [Select Id from Territory2Model where State = 'Active'].Id;
                for(Territory2 ter: queryTerrList){
                    territoryNameMap.put(ter.Name,ter); 
                }
                //get list of all those territories which needs to be created
                for(Territory_Model__c t1 : territoryModelMap.values()){
                    if(t1.Type__c == 'Territory'){
                        if((territoryNameMap.containsKey(t1.Region_Name__c) && territoryNameMap.containsKey(t1.Area_Name__c)) && !(territoryNameMap.containsKey(t1.Name))){
                            Territory2 newT = new Territory2();
                            newT.Name = t1.Territory_Name__c;
                            newT.DeveloperName = 'X'+t1.Territory_Name__c;
                            newT.ParentTerritory2Id = territoryNameMap.get(t1.Region_Name__c).Id;
                            newT.Description = t1.Territory_Description__c;
                            newT.Territory2ModelId = territoryModelId;
                            newT.AccountAccessLevel = 'Edit';
                            newT.CaseAccessLevel = 'None';
                            newT.ContactAccessLevel = 'None';
                            newT.OpportunityAccessLevel = 'None';
                            newT.Territory2TypeId = territoryNameMap.get(t1.Region_Name__c).Territory2TypeId;
                            terrList.add(newT);
                        }                   
                    }
                }
                     
                              
            
        /*******************SFDC-934 Ends********************/  
        
        system.debug('terrList'+terrList);
        DML.evaluateResults(this, Database.upsert(terrList, false));
        system.debug('terrList'+terrList);

    }
    
    /****************************************************
*  finish(Database.BatchableContext BC)
*****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Inside Finish method');
    }
}
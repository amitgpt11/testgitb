/**
* Utility class to generate a Billing Form PDF, send it as an attachment and finaly
* post it as a FeedItem to the related Patient
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class NMD_OrderBillingFormManager {

    @Future(callout=true)
    public static void generateAndEmailBillingFormFuture(Set<Id> orderIds) {
        generateAndEmailBillingForm(orderIds);
    }

    public static void generateAndEmailBillingForm(Set<Id> orderIds) {

        List<Order> updatedOrders = new List<Order>();
        List<Messaging.Email> emails = new List<Messaging.Email>();
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        for (Order ord : [
            select
                Commissionable_Rep__r.Email,
                CreatedBy.Email,
                Opportunity.Patient__c,
                Opportunity.Patient__r.Patient_First_Name__c,
                Opportunity.Patient__r.Patient_Last_Name__c,
                PONumber,
                Product_Billing_Form_Generated__c,
                Shipping_Location__r.Inventory_Data_ID__r.Owner.Email,
                Surgeon__r.Name,
                Surgery_Date__c
            from Order
            where Id in :orderIds
        ]) {

            // Boston Scientific Procedure - (Physician) - (Surgery Date) - Patient Initials
            String subject = String.format('Boston Scientific Procedure - {0} - {1} - {2}.{3}.', new List<String> {
                ord.Surgeon__r.Name,
                (safeDateFormat(ord.Surgery_Date__c)),
                (safeInitial(ord.Opportunity.Patient__r.Patient_First_Name__c)),
                (safeInitial(ord.Opportunity.Patient__r.Patient_Last_Name__c))
            });

            PageReference pr = Page.ProductBillingForm;
            pr.getParameters().put('id', ord.Id);

            Blob pdf = Test.isRunningTest() ? Blob.valueOf('foo') : pr.getContent();
            String filename = 'Product_Billing_Form.pdf';

            Set<String> emailAddresses = new Set<String> { ord.CreatedBy.Email };
            SendEmailManager manager = new SendEmailManager(subject, 'Form is attached.', ord.CreatedBy.Email);
            
            if (String.isNotBlank(ord.Shipping_Location__r.Inventory_Data_ID__r.Owner.Email)
             && !emailAddresses.contains(ord.Shipping_Location__r.Inventory_Data_ID__r.Owner.Email)
            ) {
                manager.addCCAddress(ord.Shipping_Location__r.Inventory_Data_ID__r.Owner.Email);
                emailAddresses.add(ord.Shipping_Location__r.Inventory_Data_ID__r.Owner.Email);
            }
            
            if (String.isNotBlank(ord.Commissionable_Rep__r.Email)
             && !emailAddresses.contains(ord.Commissionable_Rep__r.Email)
            ) {
                manager.addCCAddress(ord.Commissionable_Rep__r.Email);
            }
            manager.addAttachment(filename, pdf);

            //Messaging.SendEmailResult result = manager.sendEmail();
            emails.add(manager.generateEmail());

            if (!ord.Product_Billing_Form_Generated__c) {

                updatedOrders.add(new Order(
                    Id = ord.Id,
                    Product_Billing_Form_Generated__c = true
                ));

                //chatter post to patient feed
                ConnectApi.ContentCapabilityInput contentInput = new ConnectApi.ContentCapabilityInput();
                contentInput.title = ord.PONumber + ' Product Billing Form';

                ConnectApi.FeedElementCapabilitiesInput capabilities = new ConnectApi.FeedElementCapabilitiesInput();
                capabilities.content = contentInput;

                ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
                input.subjectId = ord.Opportunity.Patient__c;
                input.capabilities = capabilities;

                ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(pdf, 'application/pdf', filename);
                batchInputs.add(new ConnectApi.BatchInput(input, binInput));

            }

        }

        DML.deferLogs();

        if (!emails.isEmpty()) {
            Messaging.SendEmailResult result = Messaging.sendEmail(emails)[0];
            // log failure, if any
            if (!result.isSuccess()) {
                DML.log(new ApplicationLogWrapper(
                    'ERROR', 
                    'NMD_OrderBillingFormManager', 
                    'generateAndEmailBillingForm', 
                    (result.getErrors()[0]).getMessage()
                ));
            }
        }

        if (!updatedOrders.isEmpty()) {
            DML.save('NMD_OrderBillingFormManager', updatedOrders, false);
        }

        if (!batchInputs.isEmpty() && !Test.isRunningTest()) {
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        }

        DML.flushLogs(false);

    }

    private static String safeInitial(String s) {
        return String.isBlank(s) ? '' : s.substring(0, 1);
    }

    private static String safeDateFormat(Date d) {
        return d == null ? '' : d.format();
    }

}
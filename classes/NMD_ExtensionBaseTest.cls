/**
* Test class for the base extension
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_ExtensionBaseTest {

    static final String NAME = 'NAME';

    @testSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();
        test.startTest();
        Account acct = td.createConsignmentAccount();
        acct.Name = NAME;
        insert acct;
        
        Seller_Hierarchy__c sh = new Seller_Hierarchy__c();
        sh.Name = 'TestSH';
        //sh.ASP_Trial__c = 654.87;
        insert sh;        
        
        Opportunity oppty = td.newOpportunity(acct.Id);
        //oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        oppty.Territory_ID__c = sh.id;
        insert oppty;

        insert new Attachment(
            ParentId = oppty.Id,
            Name = 'foo',
            Body = Blob.valueOf('bar')
        );

        insert new FeedItem(
            Body = 'foo',
            ContentData = Blob.valueOf('bar'),
            ContentFileName = 'baz',
            ParentId = oppty.Id
        );
        test.stopTest();
    }

    static testMethod void test_Static() {
        test.startTest();
        Opportunity oppty = [select Id from Opportunity where Account.Name = :NAME];
        Attachment attach = [select Id from Attachment where ParentId = :oppty.Id];
        FeedItem item = [select Id from Feeditem where ParentId = :oppty.Id];

        NMD_ExtensionBase.fetchAttachment(attach.Id);
        NMD_ExtensionBase.fetchFeedItem(item.Id);
        test.stopTest();
    }

    static testMethod void test_Instantiated() {
        test.startTest();
        NMD_ExtensionBaseTestHelper helper = new NMD_ExtensionBaseTestHelper();
        test.stopTest();
    }

}
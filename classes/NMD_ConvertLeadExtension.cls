/**
* Extension class for the vf page ConvertLead
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class NMD_ConvertLeadExtension extends NMD_ExtensionBase {
    
    final ApexPages.StandardController sc;
    final Boolean isSalesforce1;
    //**Added by amitabh-27-Sept
    public boolean isSaveButtonDisabled = false;
    public String opptyOption { get; set; }
    public List<SelectOption> opptyOptions { get; private set; }
    public boolean readToConvert;
    public boolean showOption { get; set; }
    public String[] selProcedures {get;set;}
    public List<SelectOption> Procedures;
    
    public NMD_ConvertLeadExtension(ApexPages.StandardController sc) {

        this.sc = sc;
        this.isSalesforce1 = ApexPages.currentPage().getParameters().get('isdtp') == 'p1';
        this.opptyOptions = new List<SelectOption>();
        selProcedures = new List<String>();
        this.readToConvert = False;
        this.showOption = False;
        leadIsValid();
    }
    public List<SelectOption> getProcedures(){
        List<selectOption> options = new List<selectOption>();
        
        options.add(new selectOption('Lead Swap','Lead Swap'));
        options.add(new selectOption('Lead Addition','Lead Addition'));
        options.add(new selectOption('Lead Adjustment','Lead Adjustment'));
        options.add(new selectOption('BSC IPG Swap','BSC IPG Swap'));
        options.add(new selectOption('BSC IPG Addition','BSC IPG Addition'));
        options.add(new selectOption('BSC IPG Adjustment','BSC IPG Adjustment'));
        //options.sort();
        
        return options;
    }
    public void convert() {

        if (this.readToConvert) {
                this.showOption = False;
                system.debug('showOption ===>'+this.showOption);
            try {               
                    //[Select]
                    system.debug('this.sc.getId()==>'+this.sc.getId());
                    NMD_LeadConversionManager lcm = new NMD_LeadConversionManager(this.sc.getId());
                    Boolean isCreated = lcm.convertLead();
                
                if (isCreated) {
                    newInfoMessage(String.format('{0} <a id="continue_url" href="{1}">{2}</a>{3}.', new List<String> { 
                        Label.NMD_Lead_Converted,
                        buildViewUrl(lcm.newOpportunityId),
                        Label.NMD_Click_Here,
                        Label.NMD_To_Continue
                    }));
                }
                else {
                    newErrorMessage(lcm.lastErrorMessage);
                    newInfoMessage(String.format('<a id="continue_url" href="{0}">{1}</a> {2}.', new List<String> { 
                        buildViewUrl(this.sc.getId()),
                        Label.NMD_Click_Here,
                        Label.NMD_To_Continue
                    }));
                }

            }
            catch (NMD_LeadConversionManager.ConvertLeadException cle) {

                Lead lead = (Lead)this.sc.getRecord();

                newInfoMessage(String.format('{0} <a id="continue_url" href="{1}">{2}</a>{3}.', new List<String> { 
                    cle.getMessage(),
                    buildViewUrl(lead.ConvertedOpportunityId),
                    Label.NMD_Click_Here,
                    Label.NMD_To_Continue
                }));

            }

        }

    }

    private String buildViewUrl(Id recordId) {
        String url = '/';
        if (recordId != null) {
            url = this.isSalesforce1
                ? 'javascript:sforce.one.navigateToURL(\'/' + recordId + '\');'
                : new ApexPages.StandardController(recordId.getSObjectType().newSObject(recordId)).view().getUrl();
        }
        return url;
    }

    private void leadIsValid() {

        Lead lead = (Lead)this.sc.getRecord();
        Boolean pass = true;

        if (lead.Patient__c == null) {
            pass = false;
            newErrorMessage(Label.NMD_Patient_Required);
        }
    

        if (lead.Trialing_Physician__c == null && lead.Account_Facility__c == null) {
            pass = false;
            newErrorMessage(Label.NMD_Physician_Required);
        }
        
        //**Added by Amitabh in order to provide option to convert to trial or implant
        /*if (lead.ConvertTo__c == null || lead.ConvertTo__c == '') {
            pass = false;
            //**create new label NMD_ConvertTo_Required
            newErrorMessage(Label.NMD_ConvertTo_Required);
        }*/
        //confirmation
        if (!pass) {
            newInfoMessage(String.format(
                '<a id="continue_url" href="{0}">{1}</a> {2}.',
                new List<String> {
                    buildViewUrl(this.sc.getId()),
                    Label.NMD_Click_Here,
                    Label.NMD_To_Go_Back
                }
            ));
        }
        else if(pass){
            this.showOption = True;
        }
        //return pass;

    }
    public String formatForMultiPicklist(List<String> values) {
        if (values == null) return null;
        return String.join(values, ';');
    }
    public boolean getIsSaveButtonDisabled() {
        //isSaveButtonDisabled=true;
        return isSaveButtonDisabled;
    }
    public void clearselProcedure(){
        if(selProcedures.size()>0){
            selProcedures.clear();
            system.debug('selProcedures==>'+selProcedures);
        }
    }
    public void ConvertToOppty(){
        
        if (this.showOption) {
            Id leadId = this.sc.getId();
            Lead currentLead=(Lead)this.sc.getRecord();
            system.debug('Lead fields:=='+ currentLead.ConvertTo__c +currentLead.Procedure_Type__c );
            system.debug('Selected procedures==>'+selProcedures+'size==>'+selProcedures.size());
            if(currentLead.ConvertTo__c == 'Revision' && selProcedures.size() == 0){
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please enter Procedure Type'));
            }else{
                if(currentLead.ConvertTo__c == 'Revision'){
                    currentLead.Procedure_Type__c = formatForMultiPicklist(selProcedures);
                }
               // getIsSaveButtonDisabled();
                pagereference p =this.sc.save();
                this.readToConvert = True;
                this.isSaveButtonDisabled = true;
                convert();
            }            
        }
    }

}
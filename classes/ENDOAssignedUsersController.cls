public with sharing class ENDOAssignedUsersController{
  
    private List<User> AssignedUsers{get;set;}
    private List<ID> terrId{get;set;}
    private List<ID> UserIdSet{get;set;}
    private List<String> UserRoles{get;set;}
    private List<String> UserRolesinCustSett{get;set;}
    private map<Id,set<String>>UserandRoles{get;set;}
    private map<Id,set<String>>SingleUserRole{get;set;}
    private List<TerritoryUserRoles__c>TerrCussett{get;set;}
    private Set<String> tempRoles{get;set;}
    private List<ObjectTerritory2Association> objTerr2AsscList;
    private List<UserTerritory2Association> userTerr2AsscList;
    private List<UserTerritory2Association> userTerr2asscList1;
    private List<String>NewRoleslst{get;set;}
    private set<String>RolesinOrg{get;set;}
    private List<Territory2>TerritoryListonaccount;
    private list<Id> tempLst ;
    private set<Id> tempSet ;
    private Map<String,List<AssignedUser>> divisionUserMap = new Map<String,List<AssignedUser>>();
    private User_Division_Choice__c selDivChoice;
    public List<AssignedUser> displaybleUsers {get;set;}
    public List<SelectOption> Divisions;
    public String[] selDivisions {get;set;}
    Map<String,ENDOTerritoryTypes__c> TypesMap= ENDOTerritoryTypes__c.getAll();  
    public set<string> type = TypesMap.keyset();
    
    public ENDOAssignedUsersController(ApexPages.StandardController controller) {
        AssignedUsers= new List<User>();
        //holds all users with their all possible unique roles 
        map<Id,set<String>>UserandRoles = new map<Id,set<String>>();  
        selDivisions = new List<String>();
        set<Id>setofId=new set<Id>();
        NewRoleslst=new List<String>();
        SingleUserRole=new map<Id,set<String>>();
        terrId=new List<Id>();
        UserIdSet=new List<Id>();
        objTerr2AsscList = new List<ObjectTerritory2Association> ();
        userTerr2AsscList = new List<UserTerritory2Association>();
        userTerr2asscList1 = new List<UserTerritory2Association>();
        UserRoles= new List<String>();
        UserRolesinCustSett = new List<String>();
        TerrCussett= new List<TerritoryUserRoles__c>();
        TerritoryListonaccount= new List<Territory2>();
        tempLst = new list<Id>();
        tempSet = new set<Id>();
        
        String accid= ApexPages.currentPage().getParameters().get('id'); 
        If (accid!=null){
            objTerr2AsscList=  [SELECT Id,ObjectId,Territory2Id FROM ObjectTerritory2Association WHERE ObjectId =: accId];//Get the list of territories related to the account
            
            if(objTerr2AsscList!=null && objTerr2AsscList.size()>0){
                for(ObjectTerritory2Association  accrelatedterr:objTerr2AsscList){
                    terrId.add(accrelatedterr.Territory2Id);
                }
            }
            //==========get territories related to Account============
            TerritoryListonaccount=[SELECT Id, Name, Territory2TypeId, Territory2ModelId,ParentTerritory2Id,Description,No_Of_Parents_1_To_6__c,No_Of_Parents_7_To_10__c, DeveloperName
                                    FROM Territory2 where ID IN:terrId ]; 
            system.debug('*******'+TerritoryListonaccount);
            //=========================================================
        }
        
        //===========get Division of currently assigned territories ===============
        Map<Id,String> mapTerrIdParentId = new Map<Id,String>();
        for(Territory2 objterr :TerritoryListonaccount){
            if(objterr.No_Of_Parents_7_To_10__c==Null){
                if(string.IsNotEmpty(objterr.No_Of_Parents_1_To_6__c)){
                    if(objterr.No_Of_Parents_1_To_6__c.split(',').size() >= 2){
                        mapTerrIdParentId.put(objterr.id, objterr.No_Of_Parents_1_To_6__c.split(',')[1]);
                    }else{
                        mapTerrIdParentId.put(objterr.id,objterr.id);
                    }
                }
            }else {
                if(objterr.No_Of_Parents_7_To_10__c!=Null){
                    String ListofterrDiv = objterr.No_Of_Parents_7_To_10__c+ ','+objterr.No_Of_Parents_1_To_6__c;
                    system.debug('+++++'+ ListofterrDiv );
                    if(ListofterrDiv!=Null){
                        mapTerrIdParentId.put(objterr.id, ListofterrDiv.split(',')[1]);
                    }
                    
                }
                
                
            }
            
        }
        list<Territory2> listParentTerritory = [SELECT Id, Name, Territory2TypeId, Territory2ModelId,ParentTerritory2Id,Description,No_Of_Parents_1_To_6__c,No_Of_Parents_7_To_10__c, DeveloperName
                                                FROM Territory2 where ID IN: mapTerrIdParentId.Values()]; 
        Map<Id, String> mapParentIdDivision = new Map<Id, String>();
        Map<String,ETM_Divisions__c> MapofDivisionCodes= ETM_Divisions__c.getAll();  
        
        for(Territory2 prtr: listParentTerritory){          
            //mapParentIdDivision.put(prtr.id,prtr.Name.length() >= 2 ? prtr.name.substring(0,2): prtr.name);
            mapParentIdDivision.put(prtr.id,(MapofDivisionCodes!= null && MapofDivisionCodes.get(prtr.Name) != null) ? MapofDivisionCodes.get(prtr.Name).Division_Code__c :prtr.Name.length() >= 2 ? prtr.name.substring(0,2): prtr.name);
        }
        map<id,String> mapUserIdDivision = new map<id,String>();    
        //========================================================================  
        map<Id,list<UserTerritory2Association>> userTerrMap = new map<Id,list<UserTerritory2Association>>();
        userTerr2asscList=[SELECT Id, UserID, Territory2Id,Territory2.ParentTerritory2.Territory2Type.MasterLabel,Territory2.ParentTerritory2Id, Territory2.Territory2Type.MasterLabel,RoleInTerritory2, IsActive from UserTerritory2Association where Territory2Id IN : terrId];
        if(userTerr2asscList!=null && userTerr2asscList.size()>0){
            for(UserTerritory2Association utr:userTerr2asscList){
                
                UserIdSet.add(utr.UserId);
                UserRoles.add(utr.RoleInTerritory2);
                system.debug('<<<****>>>'+ UserIdSet);
                system.debug('*******'+ UserRoles);
                //system.debug('<<<<*>>>>>'+ UserandRoles);
                
                //==================assigned divisions with the Users=========================
                String Division = mapParentIdDivision.get(mapTerrIdParentId.get(utr.Territory2Id));
                if(mapUserIdDivision != null && mapUserIdDivision.get(utr.UserId) != null){
                    set<String> dupCheckList = new set<String>();
                    dupCheckList.addall(mapUserIdDivision.get(utr.UserId).split(','));
                    System.debug('==dupCheckList=='+dupCheckList);
                    System.debug('==Division=='+Division);
                    if(!dupCheckList.contains(Division)){
                        mapUserIdDivision.put(utr.UserId, mapUserIdDivision.get(utr.UserId) + ','+Division);
                    }
                }else{
                    mapUserIdDivision.put(utr.UserId, Division);    
                }
                //==========================================================================
                
            }
        }
        
       
        //public List<User>getAssignedUser() {
        //Final list of Users which is Used in the VF page to display 
        // AssignedUser= new List<User>();
        AssignedUsers=[select id, name, Division, Profile_Name__c, alias, username, UserRole.Name, isActive, 
                       Manager.Full_Name__c, profile.Name, LastLoginDate, Phone, MobilePhone, Title, Department
                       from user 
                       where id IN:UserIdSet Order By Division, UserRole.Name];
        
        if(AssignedUsers.size()<=0){
            System.debug('AssignedUser is empty!.');
        }                 
        
        
        userTerr2asscList1= [SELECT Id, UserID, Territory2Id, Territory2.ParentTerritory2.Territory2Type.MasterLabel,Territory2.ParentTerritory2Id ,Territory2.Territory2Type.MasterLabel,RoleInTerritory2, IsActive from UserTerritory2Association where UserID IN : AssignedUsers]; 
        system.debug('*******'+ userTerr2asscList1.size());
        
        Set<String>rolesPerUser;
        Set<Id>UserTerritoryIds = new  Set<Id>() ;
        for(User aUser : AssignedUSers){
            rolesPerUser = new Set<String>();
            for( UserTerritory2Association   u : userTerr2asscList1  ){ 
                if(UserandRoles.containskey(u.userId)){
                    Set<String> tempRoles = UserandRoles.get(u.userId);
                    tempRoles.add(u.RoleInTerritory2);
                    
                    UserandRoles.put(u.userId,tempRoles);
                    
                } else
                    UserandRoles.put(u.userId,new Set <String>{u.RoleInTerritory2});    
                
                if(aUser.Id==u.UserID)
                {
                    rolesPerUser.add(u.RoleInTerritory2);
                    
                    
                }
                //if(u.Territory2Id!=Null){
                // UserTerritoryIds.add(u.Territory2Id);
                //}
                if(userTerrMap.containsKey(u.userId)){
                    userTerrMap.get(u.userId).add(u);
                }else{
                    userTerrMap.put(u.userId,new list<UserTerritory2Association>{u});
                }
                
            }
            
            //    UserandRoles.put(aUser.Id,rolesPerUser);
            
        }
         system.debug('userTerrMap--'+userTerrMap);
        
        /* for( UserTerritory2Association   u : userTerr2asscList1  ){ 
if(UserandRoles.containskey(u.userId)){
if(u.RoleInTerritory2 != null){
UserandRoles.get(u.userId).add(u.RoleInTerritory2); 
system.debug('@@@@@@'+UserandRoles);
}        
} 
else 
{
if(u.RoleInTerritory2 == null){
UserandRoles.put(u.userId, new set<string>{u.RoleInTerritory2}); 
system.debug('######'+UserandRoles);
}
}
}*/ 
        
        /* if (UserTerritoryIds!=null){
for(Id userdiv:UserTerritoryIds){
for(Territory2 objterr: TerritoryListonaccount){            
if(string.IsNotEmpty(objterr.No_Of_Parents_1_To_6__c)){

// tempLst = objterr.No_Of_Parents_1_To_6__c.split(',');
// tempSet.addAll(tempLst);
system.debug('####'+tempSet);
}

}

}
}*/
        
        
        system.debug('******'+tempRoles); 
        system.debug('****Final Roles***'+UserandRoles);
        
        for (Id setofroles : UserandRoles.keyset()){
            if(setofroles!=null && UserandRoles.get(setofroles)!=null){
                RolesinOrg= new Set<String>();
                RolesinOrg.addAll(UserandRoles.get(setofroles));
                System.debug('*****Rolesin Org****'+RolesinOrg);
                if(RolesinOrg.size()==1){
                    SingleUserRole.put(setofroles,UserandRoles.get(setofroles));
                    System.debug('*****SingleUserRole****'+RolesinOrg);
                    
                }         
                
            }
        }        
        //Get the list of users with their most influential roles in all assigned territories. 
        
        map<id,String> userfinalrolesMap =  findUserInfluentialRole(UserandRoles);
        List<Id> userIDs= new List<Id>();
        userIDs.addAll(UserandRoles.keySet());
        
        displaybleUsers = new List<AssignedUser>();
        set<String> uniqueUserIdsToDisplay = new set<String>();
        
        for(User u: AssignedUsers)
            for(Id aUserId: userIds){
                if(u.Id==aUserId && userfinalrolesMap.get(aUserId) != ''){
                    uniqueUserIdsToDisplay.add(aUserId);
                }
            }
        
        
        for(User u: AssignedUsers)
            for(Id aUserId: userIds){
                AssignedUser anAssignedUser = new AssignedUser();
                anAssignedUser.userId = u.Id;
                anAssignedUser.Name = u.Name;
                //  anAssignedUser.userDivision = u.division;
                //============Assigned divisions===========
                anAssignedUser.userDivision = (mapUserIdDivision != null && mapUserIdDivision.get(u.Id) != null) ? mapUserIdDivision.get(u.Id) :'';
                
                //=========================================
                anAssignedUser.userProfile =u.Profile_Name__c;
                anAssignedUser.isUserActive =u.IsActive;
                anAssignedUser.userManager = u.Manager;
                anAssignedUser.MobilePhone=u.MobilePhone;
                //------------------------------
                    if(userTerrMap.containsKey(u.Id)){
                        system.debug('--261 ->'+userTerrMap.get(u.Id));
                        list<UserTerritory2Association> temp = userTerrMap.get(u.Id);
                        for(UserTerritory2Association t : temp){
                            system.debug('--265 ->'+t.Territory2.ParentTerritory2Id);
                             system.debug('--265 ->'+t.Territory2.Territory2Type.MasterLabel);
                            if(type.Contains(t.Territory2.Territory2Type.MasterLabel)){
                                    anAssignedUser.region=t.Territory2.Territory2Type.MasterLabel;
                                    break;
                            }
                            if(t.Territory2.ParentTerritory2Id != null){
                                system.debug('--267 ->'+t.Territory2.ParentTerritory2.Territory2Type.MasterLabel);
                                if(type.Contains(t.Territory2.ParentTerritory2.Territory2Type.MasterLabel)){
                                    anAssignedUser.region=t.Territory2.ParentTerritory2.Territory2Type.MasterLabel;
                                    break;
                                }
                                
                                else{
                                    anAssignedUser.region='';
                                }
                            }
                        }
                    }
                    else{
                        anAssignedUser.region='';
                    }
                //---------------------------------
                if(u.Id==aUserId){
                    anAssignedUser.userRole = userfinalrolesMap.get(aUserId);    
                    System.debug('%%%%%%'+anAssignedUser.userRole);            
                }else{
                    anAssignedUser.userRole = '';                                   
                }
                if(uniqueUserIdsToDisplay.contains(aUserId)){
                    if(anAssignedUser.userRole != ''){
                        fillDivisionUserMap (anAssignedUser);
                    }                   
                }else{
                    fillDivisionUserMap (anAssignedUser);                    
                }
                
                system.debug('anAssignedUser========'+anAssignedUser);
            }
            
        fetchSelectedDivision();
        fetchDisplayableUsersList();
    }
    
    
    private void fillDivisionUserMap (AssignedUser assUser){
      String[] userDivisions = assUser.userDivision.split(',');
      for (String usrdivision : userDivisions){
        if (!''.equals(usrdivision)){
           if (null != divisionUserMap.get(usrdivision)){
             divisionUserMap.get(usrdivision).add(assUser);
           }else{
             divisionUserMap.put(usrdivision,new List<AssignedUser>{assUser});
           }
        }
      }
      
    }
    
    
    private void fetchSelectedDivision(){
      selDivisions =  new List<String>();
      List<User_Division_Choice__c> udc = [SELECT Id,Name,Selected_Division__c,SessionId__c,UserId__c FROM User_Division_Choice__c WHERE UserId__c = :UserInfo.getUserId()];
      Boolean isChange = false;
      
      if (udc != null && udc.size() == 1){
        System.debug('udc[0].SessionId__c--'+udc[0].SessionId__c);
        System.debug('UserInfo.getSessionId()--'+UserInfo.getSessionId());
        System.debug('udc[0].Selected_Division__c--'+udc[0].Selected_Division__c);
        if (udc[0].SessionId__c == UserInfo.getSessionId()){
          selDivChoice = udc[0];
        }else{
          selDivChoice = udc[0];
          selDivChoice.SessionId__c = UserInfo.getSessionId();
          selDivChoice.Selected_Division__c = '';
        }
      }
      else{
        selDivChoice = new User_Division_Choice__c(SessionId__c = UserInfo.getSessionId(),UserId__c = UserInfo.getUserId(),Name = UserInfo.getUserName());
      }
      
      if (selDivChoice.Id != null && selDivChoice.Selected_Division__c != ''){
        List<String> divChoices = selDivChoice.Selected_Division__c.split(',');
        if (divChoices.size() == 1 && divChoices[0] == 'All' && divisionUserMap.keySet().size() > 1){
            selDivisions.add(divChoices[0]);
        }else{
            for (String selDiv : divChoices){
              if (divisionUserMap.keySet().contains(selDiv)){
                selDivisions.add(selDiv);
              }
            }
        }
        if (selDivisions.size() != 0 && selDivisions.size() != divChoices.size()){
          isChange = true;
          String saveDiv = '';
          for (String seldiv : selDivisions){
            saveDiv = saveDiv+seldiv+',';
          }
          selDivChoice.Selected_Division__c = saveDiv.removeEnd(',');
        }
      }
      System.debug('divisionUserMap'+divisionUserMap);
      if (selDivChoice.Id == null || selDivisions.size() == 0){
        isChange = true;
        if (divisionUserMap.keySet().contains('Endo')){
            selDivisions.add('Endo');
        }
        else if (divisionUserMap.keySet().size() > 1){
            selDivisions.add('All');
          }else{
             for (String divName: divisionUserMap.keySet()){
                selDivisions.add(divName);
        }
          }
          selDivChoice.Selected_Division__c = selDivisions.size() > 1 ? selDivisions[0]:'';
      }
      
    }
    
    
    private void fetchDisplayableUsersList(){
      List<AssignedUser> allUsers = new List<AssignedUser>();
      List<AssignedUser> singleDivUsers = new List<AssignedUser>();
        Set<AssignedUser> multDivUsers = new Set<AssignedUser>();
        Set<String> dispDivs = new Set<String>();
        for (String selDivision :selDivisions){
          if ('All'.equals(selDivision)){
            dispDivs.addAll(divisionUserMap.keySet());
            break;
          }else{
            dispDivs.add(selDivision);
          }
        }
        for (String divName : dispDivs){
          for(AssignedUser as1:divisionUserMap.get(divName)){
            if(!(as1.userDivision.contains(','))){
                singleDivUsers.add(as1);//users with Single division.
            }else{
                multDivUsers.add(as1);//users with Multiple divisions.
            }
            
          }
        }          
        singleDivUsers.sort(); //sorting single division users. 
        
        displaybleUsers.clear();
        displaybleUsers.addAll(multDivUsers);
        displaybleUsers.addAll(singleDivUsers);
        
    }
    
    
    public List<SelectOption> getDivisions(){
        List<selectOption> options = new List<selectOption>();

        for (String divName: divisionUserMap.keySet()){
            options.add(new selectOption(divName, divName));
    }
    options.sort();
    
    if (divisionUserMap.keySet().size() > 1){
          options.add(0,new selectOption('All', 'All'));
        }
        return options;
    }
    
    
    public void updateUserList(){
      String saveDiv = '';
    for (String seldiv : selDivisions){
      saveDiv = saveDiv+seldiv+',';
    }
    selDivChoice.Selected_Division__c = saveDiv.removeEnd(',');
    upsert selDivChoice;
      fetchDisplayableUsersList();
    }
    
    private map<id,String> findUserInfluentialRole(map<Id,set<String>> UserwithRoles){
        //make customsettings values map, map the user roles matching to custom settings - 
        //sort the map for their values lowest to highest. then take the first element. 
        //get values from custom settings
        Map<String, TerritoryUserRoles__c> UsrRoles = TerritoryUserRoles__c.getAll();
        
        //holds all users with their most influential roles
        map<id,String> userDisplayRoles =  new map<Id,String>(); 
        map<id,String> NewRolesMap =  new map<Id,String>();
        
        List<Id> userIDs= new List<Id>();
        userIDs.addAll(UserwithRoles.keySet());
        
        for(Id M1:SingleUserRole.Keyset()){
            for(string role : SingleUserRole.get(M1)){
                if(!UsrRoles.keyset().contains(role)){
                    NewRolesMap.put(M1,Role);                 
                    system.debug('*****NewRoleMap*****'+NewRolesMap);
                }
            }
        }
        
        
        for(Id userid:userIDs){
            if(UserwithRoles.get(userid)!=null){
                List<double> roleInfluence = new List<double>();
                for(String aRole: UserwithRoles.get(userid))
                    if(UsrRoles.get(aRole)!=null){
                        TerritoryUserRoles__c aTerRole = UsrRoles.get(aRole);
                        System.debug('<<<<<<<<>>>>>>>>>>>>Role/Priority added '+aTerRole.Name+' / '+aTerRole.Priority__c);
                        
                        roleInfluence.add(aTerRole.Priority__c);
                    }
                if(roleInfluence.size()>0){  
                    roleInfluence.sort();
                    
                    List<String> roles = new List<String>();
                    roles.addAll(usrRoles.keySet());
                    for(String role: roles){
                        TerritoryUserRoles__c aTerRole = UsrRoles.get(role);
                        if(aTerRole.Priority__c==roleInfluence.get(0)){
                            system.debug('Final User and Role >>>>>>>>>>'+userid+' : '+aTerRole.Name);
                            userDisplayRoles.put(userid, aTerRole.Name);//first element is the highest role.
                        }
                    }
                    
                }
                
            }
        }
        
        //adding users with single non-matching (from custom setting role values) to the final map       
        //  List<Id> user1IDs= new List<Id>();
        // user1IDs.addAll(NewRolesMap.keySet());
        
        for(Id M11:NewRolesMap.Keyset()){
            if(M11!=null && NewRolesMap.get(M11)!=null){
                userDisplayRoles.put(m11,NewRolesMap.get(M11));
            }
        } 
        System.debug('Single USER & ROLE LIST >>>>>'+NewRolesMap);
        System.debug('Final USER & ROLE LIST >>>>>'+userDisplayRoles);
        return userDisplayRoles;
        
        
        
    }
    
    public class AssignedUser implements Comparable  {
        public ID userId {get;set;}
        public String Name {get;set;}
        public String userRole {get;set;}
        public String userDivision {get;set;}
        public String userProfile {get;set;}
        public Boolean isUserActive {get;set;}
        public User userManager {get;set;}
        public String MobilePhone {get;set;}
        public string region{get;set;}
        // Added for SFDC-11&13 - DiamoJ1
        public String userTitle {get;set;} 
        public String userDepartment {get;set;}
        
        // Implement the compareTo() method for division based sorting
        public Integer compareTo(Object compareToAssignedUser) {
            AssignedUser compareToAsU = (AssignedUser)compareToAssignedUser;
            if (userDivision == compareToAsU.userDivision) return 0;
            if (userDivision > compareToAsU.userDivision) return 1;
            return -1;        
        }
        
        
        
    }
    
}
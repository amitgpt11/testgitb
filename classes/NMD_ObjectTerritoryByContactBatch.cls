/**
* Batch class, Change Opportunity/Patient Territory Based on Contact Physician Territory  
*
* @author   Salesforce services
* @date     2014-04-03
*/
global without sharing class NMD_ObjectTerritoryByContactBatch implements Database.Batchable<sObject>, Schedulable {
    static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};  
    //static final String queryField = 'Territory_ID__c';
      static final Set<String> queryField = new Set<String> {'Territory_ID__c'};
    //public String query = 'SELECT ContactId,CreatedById,CreatedDate,Field,Id,IsDeleted,NewValue,OldValue FROM ContactHistory where field =:queryField and CreatedDate = LAST_N_DAYS:30';
    //global String query = 'SELECT Id, ContactId FROM ContactHistory where field in : queryField and CreatedDate = LAST_N_DAYS:30'; 
    global String query = 'SELECT ' +
							'Id, ' +
							'ContactId ' +
						'FROM ContactHistory ' +
						'where field In: queryField '+ 
						'and CreatedDate = LAST_N_DAYS:30 ';
    
    //Contact Record Types  
    static final Set<Id> RECTYPES_NMD_CONTACTS = new Set<Id> {
        ContactManager.RECTYPE_CUSTOMER, ContactManager.RECTYPE_PROSPECT
    };  
    //Opportunity Record Types
    static final Set<Id> RECTYPES_NMD_OPPORTUNITY = new Set<Id> {
        OpportunityManager.RECTYPE_TRIAL, OpportunityManager.RECTYPE_EXPLANT, 
        OpportunityManager.RECTYPE_REVISION,OpportunityManager.RECTYPE_TRIAL_NEW, OpportunityManager.RECTYPE_IMPLANT
    };  
    //Patients Record Types
    static final Set<Id> RECTYPES_NMD_PATIENTS = new Set<Id> {
        PatientManager.RECTYPE_CUSTOMER, PatientManager.RECTYPE_PROSPECT
    };              
    global NMD_ObjectTerritoryByContactBatch() {
        if (Test.isRunningTest()) {
            this.query += 'order by CreatedDate desc limit 100';
        }
    }   
    /***  @desc Immediately executes an instance of this batch*/
    public static void runNow(){
        (new NMD_ObjectTerritoryByContactBatch()).execute(null);
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
                return Database.getQueryLocator(this.query);        
    }
    
    /***  @desc Database.Bachable method*/
    global void execute(Database.BatchableContext bc, List<ContactHistory> scope) {
        system.debug('-->'+scope);
        Set<String> contactIds = new Set<String>();
        //populate data
        for(ContactHistory ch : scope){
            contactIds.add(ch.ContactId);
        }       
        if(!contactIds.isEmpty()){
            objectTerritoryChange(contactIds);
        }                       
    }   
    public static void objectTerritoryChange(Set<String> contactIds){
        //init
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        List<Opportunity> oppourtunityList = new List<Opportunity>();
        List<Patient__c> patientList = new List<Patient__c>();                                  
        if(!contactIds.isEmpty()){
            contactMap = new Map<Id, Contact>([Select Id, Territory_ID__c from Contact where Id in :contactIds and RecordTypeId  =:RECTYPES_NMD_CONTACTS ]);
        }                                               
        //update opportunity            
        for (Opportunity oppo : [SELECT Id, Territory_ID__c, Patient__c, Patient__r.Physician_of_Record__c FROM Opportunity where Patient__r.Physician_of_Record__c in :contactMap.keySet() and RecordTypeId in :RECTYPES_NMD_OPPORTUNITY]) {
            if(contactMap.containsKey(oppo.Patient__r.Physician_of_Record__c)){
                oppo.Territory_ID__c = contactMap.get(oppo.Patient__r.Physician_of_Record__c).Territory_ID__c;
                oppourtunityList.add(oppo);
            }                       
        }           
        //update patient            
        for (Patient__c patient : [SELECT Id, name, Physician_of_Record__c, Territory_ID__c FROM Patient__c where Physician_of_Record__c in :contactMap.keySet() and RecordTypeId in :RECTYPES_NMD_PATIENTS]) {
            if(contactMap.containsKey(patient.Physician_of_Record__c)){
                patient.Territory_ID__c = contactMap.get(patient.Physician_of_Record__c).Territory_ID__c;
                patientList.add(patient);           
            }           
        }               
        //update patient Persist
        List<Database.SaveResult> patientSaveResults = Database.update(patientList, false);
        for (Database.SaveResult result : patientSaveResults) {
            if (!result.isSuccess()) {
                for (Database.Error err : result.getErrors()) {
                    logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Update of Patient Obj On NMD_ObjectTerritoryByContactBatch.execute'));
                }
            }
        }                   
        //update opportunity Persist
        List<Database.SaveResult> oppourtunitySaveResults = Database.update(oppourtunityList, false);
        for (Database.SaveResult result : oppourtunitySaveResults) {
            if (!result.isSuccess()) {
                for (Database.Error err : result.getErrors()) {
                    logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Update of opportunity Obj On NMD_ObjectTerritoryByContactBatch.execute'));
                }
            }
        }       
    }
    /***  @desc Schedulable method, executes the class instance*/
    global void execute(SchedulableContext context) {
        Database.executeBatch(this);
    }   
    global void finish(Database.BatchableContext bc) {
        if (!logs.isEmpty()) {
            Logger.logMessage(logs);
        }
    }   
}
public class LeadTriggerRecursivePrevention {
    // private static variable referencing the class
    private static LeadTriggerRecursivePrevention instance = null;
    public boolean isSingleRun {get; set;}

    // The constructor is private and initializes the id of the record type
    private LeadTriggerRecursivePrevention(){
        isSingleRun = false;
    }
    // a static method that returns the instance of the record type
    public static LeadTriggerRecursivePrevention getInstance(){
        // lazy load the boolean - only initialize if it doesn't already exist
        if(instance == null) instance = new LeadTriggerRecursivePrevention();
        return instance;
    }
}
/**
* Manager class for generic sobjects
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public class SObjectManager {
	
	/**
	* @Description  Methods for safely retrieving field values via relationships from generic SObjects
	*/
	public static String getFieldAsString(SObject obj, String field) {
    	
		Object res = getField(obj, field);
		return res == null ? '' : String.valueOf(res);

    }

	public static Object getField(SObject obj, String field) {

		Object fieldVal;

		// if the object or field is blank then don't evaluate, just pass back null
        if (obj != null && String.isNotBlank(field)) {

	        // if the field has dot notation then need to get the parent object
	        if (field.contains('.')) {

	            SObject subObj = obj.getSObject(field.substring(0, field.indexOf('.')));
	            String subField = field.substring(field.indexOf('.') + 1);
	            
	            // get the field from the parent
	            fieldVal = getField(subObj, subField);

	        }
	        else {
		        // no other conditions left, can grab the field
		        fieldVal = obj.get(field);
		    }

	    }

	    return fieldVal;

    }

}
/*****  
        Class:  ANZImplantFormExtensionPDF for pdf generation  (Controller for Page - ANZImplantForm)
        Created By: Aalokkumar Gupta
        Created Date: 14th-September-2016
        Last Modified By: Aalokkumar Gupta
        Last Modify Date: 14th-September-2016
        Description: Controller for the ANZImplantForm VF page that allows ANZ users to submit a form 
                                         and create an order, order products and a patient.
****/
public class ANZImplantFormExtensionPdf {
    public String loggedInUserName{get;set;}
    public String loggedInUserDivision{get;set;}    
    public User userRecord;
 
    
    //     my fields 
    String objectId= null;
    public Order implantFormDisplay{get;set;}
    public List <OrderItem> orderItemDisplay {get;set;}
    public ANZImplantFormExtensionPdf(ApexPages.StandardController sc){
    
     objectId= sc.getId();
     
     system.debug('objectId===='+objectId); 
     if(objectId != null){
            implantFormDisplay =[SELECT id,AccountId,ANZ_Related_Opportunity__r.name,ANZ_Implant_Date__c,PoNumber,Hospital_Patient_ID__c,Patient_Full_Name__c,ShippingAddress,ShippingStreet ,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,ANZ_Related_Opportunity__c,Opportunity_Stage__c,Opportunity_Record_Type__c,Status,Ownerid,RecordTypeid,RecordType.developername,OrderNumber,CreatedById,CreatedBy.Name,EffectiveDate,Telephone__c,ANZ_Case_Notes__c,ANZ_Implant_Funding__c,ANZ_Insurance_Provider__c,ANZ_Paid_by_Using_Technology_Credits__c,ANZ_Referring_Physician__c,ANZ_Implanting_Physician__c,ANZ_Follow_up_Physician__c,ANZ_Patient_Title__c,ANZ_Patient_First_Name__c,ANZ_Patient_Last_Name__c,ANZ_Gender__c,ANZ_DOB__c,ANZ_Email_Address__c,Shared_Division__c,ANZ_Special_Pricing__c,ANZ_SDF_Number_for_Pricing_Approval__c FROM order  where id =:objectId ];

system.debug('___order____'+implantFormDisplay);

            orderItemDisplay= [SELECT id,Orderid,Product_Name__c,order.ordernumber,Quantity,Model_Number__c,Serial_Number__c, ANZ_Batch_Number__c,Notes__c,ANZ_Temporary_Consignment_Item__c,ANZ_Manufacturer__c,ANZ_Billing_and_Inventory__c,ANZ_Device_Implanted_Used_Successfully__c,ANZ_Lead_Type__c,ANZ_Implant_Type__c,PricebookEntry.name  from  OrderItem where Orderid =: implantFormDisplay.id ];

system.debug('___OrderItem____'+orderItemDisplay); 
          
       userRecord = [Select ID,Name,Cost_Center_Code__C,Division from User Where Id =:UserInfo.getUserId() limit 1];
       loggedInUserName = UserInfo.getName();
       loggedInUserDivision = userRecord.Division;
    }
   

}


  
  }
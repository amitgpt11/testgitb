/**
*  Description: This class is used as controller for Header Component
*/

global class HeaderController {
    
    public Integer myListsize {get;set;}
    public Boston_Scientific_Header_Footer_Config__c objHeaderFooterConfig {get;set;}
    public Boston_Scientific_Config__c objBSConfig {get;set;} 
    public List<String> lstNavLabels {get;set;}
    public Map<String,String> mapNavLabelToValue {get;set;}
    public Boolean hasAgreementAccepted {get;set;}
    
    public HeaderController(){
        
        objHeaderFooterConfig = Boston_Scientific_Header_Footer_Config__c.getValues('Default');
        objBSConfig = Boston_Scientific_Config__c.getValues('Default');
        
        if(objHeaderFooterConfig == null || objBSConfig == null) return;
        
        lstNavLabels = new List<String>();
        mapNavLabelToValue = new map<String,String>();
        
        User objUser = [Select contactId, Agreement_Accepted__c
                       From User
                       Where Id =: Userinfo.getUserId()];
                       
        hasAgreementAccepted = objUser.Agreement_Accepted__c;
        
        myListsize = [Select count()
                      From Shared_Community_Order_Item__c 
                      Where Shared_Order__r.Contact__r.Id =: objUser.ContactId
                      AND Shared_Order__r.Status__c = 'Open'];
        
        if(Label.Boston_AuthHome_Header_Tab1_Title != null && objHeaderFooterConfig.Header_Nav1_Value__c != null) {
            
            lstNavLabels.add(Label.Boston_AuthHome_Header_Tab1_Title);
            mapNavLabelToValue.put(Label.Boston_AuthHome_Header_Tab1_Title,objHeaderFooterConfig.Header_Nav1_Value__c);    
        }
        
        if(Label.Boston_AuthHome_Header_Tab2_Title != null && objHeaderFooterConfig.Header_Nav2_Value__c != null) {
            
            lstNavLabels.add(Label.Boston_AuthHome_Header_Tab2_Title);
            mapNavLabelToValue.put(Label.Boston_AuthHome_Header_Tab2_Title,objHeaderFooterConfig.Header_Nav2_Value__c);    
        }
        if(Label.Boston_AuthHome_Header_Tab3_Title != null && objHeaderFooterConfig.Header_Nav3_Value__c != null) {
            
            lstNavLabels.add(Label.Boston_AuthHome_Header_Tab3_Title);
            mapNavLabelToValue.put(Label.Boston_AuthHome_Header_Tab3_Title,objHeaderFooterConfig.Header_Nav3_Value__c);    
        }
        if(Label.Boston_AuthHome_Header_Tab4_Title != null && objHeaderFooterConfig.Header_Nav4_Value__c != null) {
            
            lstNavLabels.add(Label.Boston_AuthHome_Header_Tab4_Title);
            mapNavLabelToValue.put(Label.Boston_AuthHome_Header_Tab4_Title,objHeaderFooterConfig.Header_Nav4_Value__c);    
        }
    }
    
    /*
        *Description: This Method updates the User(field: Agreement_Accepted__c) to avoid showing agreement popup  
    **/
    @RemoteAction
    global static void updateUser() {
        
        User obj = [Select Agreement_Accepted__c 
                    From User Where Id =: Userinfo.getUserId()];
        
        obj.Agreement_Accepted__c = true;
        Update obj;
    }
}
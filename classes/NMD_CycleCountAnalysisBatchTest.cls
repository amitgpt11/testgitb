/**
* Test class for NMD_CycleCountAnalysisBatchTest
*
* @author   Salesforce services
* @date     2014-07-23
*/
@isTest(SeeAllData=false) 
private class NMD_CycleCountAnalysisBatchTest {
    final static String BSCISYNERGY = 'bscisynergy';    

    static testMethod void test_NMD_CycleCountAnalysisBatchTest() {

        final Integer LENGTH = 1;

        NMD_TestDataManager testData = new NMD_TestDataManager();
                      
        //create account
        Account acc = testData.createConsignmentAccount();
        acc.Account_Number__c = testData.randomString5();
        acc.Customer_Class__c = 'YY';
        insert acc; 
        
        //Create User
        //User tester = testData.newUser();
        User tester = testData.newUser('API User Profile');
        insert tester;
        
        //add inv settings
        insert new NMD_Inventory_Settings__c(
            SetupOwnerId = tester.Id,
            URI_Scheme__c = BSCISYNERGY
        );
                        
        //create Cycle Count
        Cycle_Count__c cc = new Cycle_Count__c();
        cc.Start_Date__c= system.today()-7;
        cc.End_Date__c= System.today()+7;
        cc.Status__c= 'Published';
        cc.Description__c='TESTSOMETHING';
        cc.Product_Scope__c='TEST_PS';
        cc.name= 'TESTCC';
        insert cc;
        
        DateTime dateNow = System.now();
        Date dateTemp = date.newinstance(dateNow.year(), dateNow.month(), dateNow.day());
        
        //CCRA
        Cycle_Count_Response_Analysis__c ccra = new Cycle_Count_Response_Analysis__c();
        ccra.Cycle_Count__c = cc.Id;
        ccra.Start_Date__c = dateTemp;        
        insert ccra;        
        
        Product2 pro = testData.newProduct('A Product');
        insert pro;        
        
        //create list obj
        List__c listObj1 = new List__c();
        listObj1.Batch_Ran__c =false;
        listObj1.Inventory_Account__c =acc.Id;
        listObj1.Cycle_Count__c =cc.Id;
        listObj1.Model_Number__c=pro.Id;
        insert listObj1;            
        
        List__c listObj2 = new List__c();
        listObj2.Batch_Ran__c =false;
        listObj2.Inventory_Account__c =acc.Id;
        listObj2.Cycle_Count__c =cc.Id;
        listObj1.Model_Number__c=pro.Id;
        insert listObj2;  
        
        
        Inventory_Data__c data = new Inventory_Data__c(
            RecordTypeId = Schema.SObjectType.Inventory_Data__c.getRecordTypeInfosByName().get('Consignment Inventory').getRecordTypeId(),
            Name = 'Inventory Data',
            Account__c = acc.Id,
            OwnerId = tester.Id
        );
        insert data;

        // process builder creates this record
        //Inventory_Location__c location = [select Id from Inventory_Location__c where Inventory_Data_ID__c = :data.Id];
        
        Inventory_Location__c location = new Inventory_Location__c(
            Inventory_Data_ID__c = data.Id
        );
        insert location;

        //Create Inventory_Item__c
        Inventory_Item__c ii10 = new Inventory_Item__c();
        ii10.Model_Number__c=pro.Id;
        //ii10.Quantity__c = 10;
        ii10.Inventory_Location__c=location.Id;
        //insert ii10;                           
        
        Inventory_Item__c ii11 = new Inventory_Item__c();
        ii11.Model_Number__c=pro.Id;
        //ii11.Quantity__c = 10;
        ii11.Inventory_Location__c=location.Id;
        //insert ii11;
        
        Inventory_Item__c ii20 = new Inventory_Item__c();
        ii20.Model_Number__c=pro.Id;
        //ii20.Quantity__c = 10;
        ii20.Inventory_Location__c=location.Id;
        //insert ii20;                           
        
        Inventory_Item__c ii21 = new Inventory_Item__c();
        ii21.Model_Number__c=pro.Id;
        //ii21.Quantity__c = 10;
        ii21.Inventory_Location__c=location.Id;
        //insert ii21;      
        insert new List<Inventory_Item__c> { ii10, ii11, ii20, ii21 };

        // create transactions
        Inventory_Transaction__c invTrans0 = new Inventory_Transaction__c(
            Inventory_Item__c = ii10.Id,
            Disposition__c = 'In Transit',
            Quantity__c = 100
        );

        Inventory_Transaction__c invTrans1 = new Inventory_Transaction__c(
            Inventory_Item__c = ii11.Id,
            Disposition__c = 'In Transit',
            Quantity__c = 100
        );

        Inventory_Transaction__c invTrans2 = new Inventory_Transaction__c(
            Inventory_Item__c = ii20.Id,
            Disposition__c = 'In Transit',
            Quantity__c = 100
        );

        Inventory_Transaction__c invTrans3 = new Inventory_Transaction__c(
            Inventory_Item__c = ii21.Id,
            Disposition__c = 'In Transit',
            Quantity__c = 100
        );
        insert new List<Inventory_Transaction__c> { invTrans0, invTrans1, invTrans2, invTrans3 };


        //run the batch
        System.runAs(tester) {

            System.Test.startTest();
            {
                NMD_CycleCountAnalysisBatch.runNow();
            }
            System.Test.stopTest();

        }
    }
    
}
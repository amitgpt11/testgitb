/*
1/6/2016- Naveen Gondha Created PatientCaseTriggerMethods_Test class.  Added 

@ModifiedDate    2MAR2016
@author          Mayuri-Accenture
@Description     The class holds test class for PatientCaseTriggerMethods.
@UserStories     DRAFT US-2452 and DRAFT US-2453

@ModifiedDate    8APR2016
@author          Mike Lankfer
@Description     Updated to accomodate the addition of a URO Men's Health Patient Case Record Type and similar event creation functionality.
@UserStories     DRAFT US-2279 and DRAFT US-2281

@ModifiedDate    4AUG2016
@author          Ashish Master
@Description     Increased Code coverage to 97%

*/

@isTest

public class PatientCaseTriggerMethodsTest {
    public static list<Account> testAccLst = new list<Account>();
    public static list<User> WTmemberLst = new list<User>();
    public static list<Shared_Patient_Case__c> patientCaseLst = new list<Shared_Patient_Case__c>();
    public static list<Shared_Patient_Case__c> UMHPatientCaselst = new list<Shared_Patient_Case__c>();
    public static string profileName = 'Watchman Field User';
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Shared_Patient_Case__c.getRecordTypeInfosByName();    
    public static final Id RECTYPE_WATCHMAN = RECTYPES.get('Watchman Patient Case').getRecordTypeId();
    public static final Id RECTYPE_UROMENSHEALTH = RECTYPES.get('Urology Men\'s Health').getRecordTypeId();
    
    @testSetup
    static void setupTerritoryMockData(){
        BatchAccountTerritoryUpdateTest.setupTerritoryMockData();
    }
    static testMethod void createDataRecords(){  
    
        Territory2 trLst = [Select name From Territory2 where name Like : 'AA%' LIMIT 1 ]; 
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId(),Account_Number__c = 'Test123',shippingCountry='US'); 
        testAccLst.add(acct1);
      /*  Account acct2 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId(), Account_Number__c = 'Test123'); 
        testAccLst.add(acct2);*/
        insert testAccLst;   
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = testAccLst[0].Id;
        accountAssociation.Territory2Id = trLst.Id;
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        UtilForUnitTestDataSetup.createBusinessCloseTimeCountry();
         
        system.assertEquals(testAccLst[0].name,'ACCT1NAME');      
        Profile p = [SELECT Id,Name FROM Profile WHERE Name=:profileName];
        system.assertEquals(p.name,profileName);
        for(Integer i = 0; i < 7; i++){
            WTmemberLst.add(new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                     LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser('+i+')@test.com'));
        }
        insert WTmemberLst;
        
        for (Integer i = 0; i < 5; i++){
            patientCaseLst.add(new Shared_Patient_Case__c(Shared_Total_Number_of_Cases__c = '1', Shared_Facility__c = testAccLst[0].Id,Shared_Start_Date_Time__c = system.today()+1,Shared_end_Date_Time__c = system.today()+3,
                                                          LAAC_Lead_WCS__c = WTmemberLst[0].Id,LAAC_WCS_Trainee_1__c= WTmemberLst[1].Id,
                                                          LAAC_WCS_Trainee_2__c= WTmemberLst[2].Id,LAAC_WCS_Trainee_3__c= WTmemberLst[3].Id,LAAC_Territory_Manager__c = WTmemberLst[4].Id, RecordTypeId = RECTYPE_WATCHMAN));
        }
        
        insert patientCaseLst;
      //Added by Ashish -- Start --  
        Shared_Patient_Case__c UMHPatientCaseobj = new Shared_Patient_Case__c(Shared_Total_Number_of_Cases__c = '1', Shared_Facility__c = testAccLst[0].Id,Shared_Start_Date_Time__c = system.today()+6,Shared_end_Date_Time__c = system.today()+8,
                                                          LAAC_Lead_WCS__c = WTmemberLst[0].Id,LAAC_WCS_Trainee_1__c= WTmemberLst[1].Id,Shared_Status__c = 'Open',
                                                          LAAC_WCS_Trainee_2__c= WTmemberLst[2].Id,LAAC_WCS_Trainee_3__c= WTmemberLst[3].Id, RecordTypeId = RECTYPE_UROMENSHEALTH);
                                        
       UMHPatientCaselst.add(UMHPatientCaseobj);
       
       insert UMHPatientCaselst;
       
      //Added by Ashish -- End --    
        
    }
    static testmethod void testUpdateCalls1(){
        createDataRecords();
        Test.startTest();
        
        for(Integer i = 0; i < patientCaseLst.size(); i++){
            if(i==0 && i<3){
                patientCaseLst[i].Shared_Start_Date_Time__c = system.today()+2;
                patientCaseLst[i].Shared_End_Date_Time__c = system.today()+4;                
                patientCaseLst[i].LAAC_Lead_WCS__c = WTmemberLst[2].Id ;
                patientCaseLst[i].LAAC_WCS_Trainee_1__c = WTmemberLst[3].Id ;
                patientCaseLst[i].LAAC_WCS_Trainee_2__c = WTmemberLst[4].Id ;
                patientCaseLst[i].LAAC_WCS_Trainee_3__c = null ;   
                patientCaseLst[i].LAAC_Territory_Manager__c = WTmemberLst[5].Id;  //Added by Ashish          
            }
            if(i>=3 && i<5){
                patientCaseLst[i].LAAC_WCS_Trainee_1__c = null ;
                patientCaseLst[i].LAAC_WCS_Trainee_2__c = null ;
                patientCaseLst[i].LAAC_WCS_Trainee_3__c = WTmemberLst[5].Id ;
                
            }
        }
        update patientCaseLst;
        delete patientCaseLst;
        
        //Added by Ashish -- Start --
        for(Shared_Patient_Case__c urptc : UMHPatientCaselst){
            urptc.Shared_Start_Date_Time__c = System.today()-3;
            urptc.Shared_End_Date_Time__c = urptc.Shared_End_Date_Time__c + 1;
            urptc.ownerId = WTmemberLst.get(0).id;
            urptc.Uro_PH_Alternate_BSCI_Resource__c = WTmemberLst.get(0).id;
            
        }
        update UMHPatientCaselst;
        
        for(Shared_Patient_Case__c urptc : UMHPatientCaselst){
            urptc.Shared_Start_Date_Time__c = System.today();
            urptc.Shared_Status__c = 'Canceled';
            
        }
        try{
            update UMHPatientCaselst;
        }catch(exception e){
            System.debug('###======'+e);
            System.assert(e.getMessage().contains('You have passed the deadline to cancel this order in Salesforce.'));
        }
        
        //Added by Ashish -- End --
        
        
        Test.stopTest();
    }
    
    static testMethod void testDeleteCalls(){        
        undelete patientCaseLst;
    }
    
    static testMethod void testCreatePatientCaseClones(){
        
        Test.startTest();
        
        testAccLst = TestDataFactory.createTestAccounts(1);       
        Profile p = [SELECT Id,Name FROM Profile WHERE Name=:profileName];
        
        for(Integer i = 0; i < 4; i++){
            WTmemberLst.add(new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                     LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='standarduser('+i+')@test.com'));
        }
        insert WTmemberLst;
        
        patientCaseLst.add(new Shared_Patient_Case__c(Shared_Total_Number_of_Cases__c = '4', Shared_Facility__c = testAccLst[0].Id,Shared_Start_Date_Time__c = system.today()+1,Shared_end_Date_Time__c = system.today()+3,
                                                          LAAC_Lead_WCS__c = WTmemberLst[0].Id,LAAC_WCS_Trainee_1__c= WTmemberLst[1].Id,
                                                          LAAC_WCS_Trainee_2__c= WTmemberLst[2].Id,LAAC_WCS_Trainee_3__c= WTmemberLst[3].Id));
        
        insert patientCaseLst;
        
        List<Shared_Patient_Case__c> clonedPatientCases = new List<Shared_Patient_Case__c>([SELECT Shared_Facility__c, Shared_Start_Date_Time__c, Shared_end_Date_Time__c, LAAC_Lead_WCS__c, LAAC_WCS_Trainee_1__c,
                                                                                           LAAC_WCS_Trainee_2__c, LAAC_WCS_Trainee_3__c, Shared_Total_Number_of_Cases__c FROM Shared_Patient_Case__c
                                                                                           WHERE Shared_Total_Number_of_Cases__c = '1']);
        
        Test.stopTest();
        
        
        System.assertEquals(patientCaseLst.get(0).Shared_Facility__c, clonedPatientCases.get(0).Shared_Facility__c);
        System.assertEquals(patientCaseLst.get(0).Shared_end_Date_Time__c, clonedPatientCases.get(0).Shared_Start_Date_Time__c);
        System.assertEquals(patientCaseLst.get(0).LAAC_Lead_WCS__c, clonedPatientCases.get(0).LAAC_Lead_WCS__c);
        System.assertEquals(patientCaseLst.get(0).LAAC_WCS_Trainee_1__c, clonedPatientCases.get(0).LAAC_WCS_Trainee_1__c);
        System.assertEquals(patientCaseLst.get(0).LAAC_WCS_Trainee_2__c, clonedPatientCases.get(0).LAAC_WCS_Trainee_2__c);
        System.assertEquals(patientCaseLst.get(0).LAAC_WCS_Trainee_3__c, clonedPatientCases.get(0).LAAC_WCS_Trainee_3__c);
        
        
    }
    
}
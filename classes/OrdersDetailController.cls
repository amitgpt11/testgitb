public class OrdersDetailController extends AuthorizationUtil{
    
    public Shared_Community_Order__c objCommunityOrder    {get; set;}
    public list<Shared_Community_Order_Item__c> lstOrderItem  {get; set;}
    public String orderId {get; set;}
    public Date orderDate  {get; set;}
    public Boolean isEuropeanUser {get;set;}

    public List<ProductWrapper> productAttributeList {get;set;}
    
    public override void fetchRequestedData() {
        
       List<User> loggedInUser = new List<User>([SELECT TimeZoneSidKey 
                                           FROM User 
                                           WHERE TimeZoneSidKey like '%Europe%' 
                                           AND Id =: UserInfo.getUserID()]);
                                           
       isEuropeanUser = (!loggedInUser.isEmpty())?true:false;
        orderId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('#1'+orderId);
       
       objCommunityOrder = [Select Name, Shared_Date_Submitted__c, Status__c, Shared_Total_Quantity__c, 
            Contact__r.lastname, Contact__r.Shared_Ship_to_Number__c, Contact__r.Shared_Bill_to_Number__c, Contact__r.Account.BillingCity, 
            Contact__r.Account.BillingCountry, Contact__r.Account.BillingPostalCode, Contact__r.Account.BillingState, Contact__r.Account.BillingStreet, Contact__r.Account.ShippingStreet, 
            Contact__r.Account.ShippingState, Contact__r.Account.ShippingCity, Contact__r.Account.ShippingCountry, Contact__r.Account.ShippingPostalCode, Contact__r.Account.Account_Number__c, Contact__r.email, Contact__r.Phone,     
            (Select id, Product__r.Product_Label__c, Product__r.ProductCode From Community_List_Item__r)
            From Shared_Community_Order__c
            Where Id =: orderId];
        
        DateTime dT = objCommunityOrder.Shared_Date_Submitted__c;
        orderDate = Date.newinstance(dT.year(), dT.month(), dT.day());
        system.debug('#2'+objCommunityOrder);

        if(objCommunityOrder != null) {
        
            lstOrderItem = [Select Id, Shared_Order__c, Shared_Quantity__c, Shared_Order__r.Shared_Date_Submitted__c, Product__r.Shared_Product_Image_URL__c, 
                Product__r.Product_Label__c, Product__r.Id, Product__r.Shared_Community_Description__c, Product__r.ProductCode
                From Shared_Community_Order_Item__c 
                Where Shared_Order__c =: orderId];
        }   

        system.debug('#3'+lstOrderItem);

        set<Id> productIds = new set<Id>();
        Map<string,object> productMap = new Map<string,Object>();
        productAttributeList = new List<ProductWrapper>();

        for(Shared_Community_Order_Item__c orderItem : lstOrderItem){
            productIds.add(orderItem.Product__c);
        }

        //Create dynamic query to fetch products as per fields in field set named Product_Attribute_Fields
        String Query = 'Select ';
        
        for(Schema.FieldSetMember f : SObjectType.Product2.FieldSets.Product_Attribute_Fields.getFields()) {
            
            Query += f.getFieldPath() + ', ';
        }
        
        Query += 'Id From product2 where Id IN : productIds';
        
        //Iterate over products list of order line item
        for(product2 product : Database.query(Query)){
                    
            productMap = new Map<string,Object>();
            
            //Iterate over field set member of field set
            for(Schema.FieldSetMember f : SObjectType.Product2.FieldSets.Product_Attribute_Fields.getFields()) {
                
                //if value of a particular field is not null in product record then add to product map
                if(product.get(f.fieldPath) != null){
                    
                    productMap.put(f.fieldPath,product.get(f.fieldPath));
                }
            }
            
            //fill map of order item id and productMap to display in template
            ProductWrapper productWrapperObj = new ProductWrapper(product.Id,productMap);
            productAttributeList.add(productWrapperObj);
        }   
    }
    
    public pageReference reOrder() {
        
        String orderId = ApexPages.currentPage().getParameters().get('Id');
        
        Shared_Community_Order_Item__c objItem = new Shared_Community_Order_Item__c();
        List<Shared_Community_Order__c> lstOrderToClone = new List<Shared_Community_Order__c>();
        List<Shared_Community_Order__c> lstOrderInProgress = new List<Shared_Community_Order__c>();
        List<Shared_Community_Order_Item__c> lstItems = new List<Shared_Community_Order_Item__c>();
        
        Id contactId = [Select contactId 
            From User 
            Where Id =: UserInfo.getUserId()].contactId;
        
        if(orderId != null && orderId != '') {
            
            lstOrderInProgress = [Select Id 
                From Shared_Community_Order__c
                Where status__c = 'Open'
                AND contact__c =: contactId
                LIMIT 1];
                
            lstOrderToClone = [Select id, Contact__c, Shared_Total_Quantity__c, 
                (Select id, Product__c, Shared_Quantity__c From Community_List_Item__r)
                From Shared_Community_Order__c
                Where Id =: orderId];
            
            if(!lstOrderToClone.isEmpty()) {
                
                if(lstOrderInProgress.isEmpty()) {
                    
                    Shared_Community_Order__c objOrder = new Shared_Community_Order__c(Shared_Date_Submitted__c = DateTime.Now(), Status__c = 'Open', 
                        Contact__c = contactId);
                    lstOrderInProgress.add(objOrder);
                    
                    if(!Test.isRunningTest()) {
                        
                        Insert lstOrderInProgress;
                    }
                }                    
                for(Shared_Community_Order_Item__c objOrderItem : lstOrderToClone[0].Community_List_Item__r) {
                    
                    objItem = objOrderItem.clone();
                    objItem.Shared_Order__c = lstOrderInProgress[0].Id;
                    lstItems.add(objItem);
                }
            }
            
            if(!Test.isRunningTest()) {
                        
                Insert lstItems;
            }
        }
                
        PageReference redirect = Page.Orders; 
        return redirect;
    }

    public class ProductWrapper{
        public string productId{get;set;}
        public Map<string,object> productMap{get;set;}
        public ProductWrapper(string productId,Map<string,object> productMap){
            this.productId = productId;
            this.productMap = productMap;
        }
    }   
}
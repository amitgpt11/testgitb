/**
 * Name : ResetIndicatorController_MyObjectiveTest 
 * Author : Mayuri
 * Description : Test class used for testing the ResetIndicatorController_MyObjective
 * Date : 19th April 2016
 */
@isTest


private class ResetIndicatorController_MyObjectiveTest { 

/*@testSetup
static void setupMockData(){
        List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();       
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA6789',Territory2ModelIDActive, Ttype[0].Id);
        insert t1;
}
*/
    static testMethod void  testSave_Scenario1(){         
        
        My_Objectives__c MO = new My_Objectives__c();
        MO.Name = 'test1';
        insert MO;
                    
        Test.startTest();
        
        Test.setCurrentPage(Page.Reset_Indicator_Mobile_Page_MyObjective); 
        ApexPages.currentPage().getParameters().put('id', MO.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(MO);
        ResetIndicatorController_MyObjective controller =  new  ResetIndicatorController_MyObjective(ctr);  
        controller.MO=MO;
        controller.SaveMO();
          
        Test.stopTest();
           
    } 
    
   static testMethod void  testSave_Scenario2(){ 
        
        
        My_Objectives__c MO1 = new My_Objectives__c();
        MO1.Name = 'test2';
        MO1.Territory__c = 'T1';      
        MO1.Territory_Assigned__c=false;
        MO1.Territory__c = 'T1';   
        insert MO1;    
        MO1.Territory_Assigned__c=true;       
        Update MO1;
         
        Test.startTest();
        
        
        Test.setCurrentPage(Page.Reset_Indicator_Mobile_Page_MyObjective); 
        ApexPages.currentPage().getParameters().put('id', MO1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(MO1);
        ResetIndicatorController_MyObjective controller =  new  ResetIndicatorController_MyObjective(ctr);  
        controller.MO=MO1;         
        controller.SaveMO();   
        Test.stopTest();
        
           
    }
    
   static testMethod void  testSave_Scenario3(){ 
             
        My_Objectives__c MO2 = new My_Objectives__c();
        MO2.Name = 'test3';
        MO2.Territory__c ='Territory1';
        MO2.Territory_Assigned__c=true;
        insert MO2;
                    
        Test.startTest();
       
        Test.setCurrentPage(Page.Reset_Indicator_Mobile_Page_MyObjective); 
        ApexPages.currentPage().getParameters().put('id', MO2.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(MO2);
        ResetIndicatorController_MyObjective controller =  new  ResetIndicatorController_MyObjective(ctr);  
        controller.MO=MO2;
        controller.SaveMO();
          
        Test.stopTest();
        
        
           
    } 
    
  
}
public class RedirectToCommunityController{
    
    public pagereference redirectURL(){
        
        String networkId = [select id from Network limit 1].Id;
        PageReference pageRef = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/networks/switch?networkId='+networkId.substring(0,15));
        pageRef.setRedirect(true);
        return pageRef;
    }
}
/*
 @CreatedDate     1 July 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Apex class 'ProductGroupMappingTriggerMethods'
 
 */

@isTest
public class ProductGroupMappingTriggerMethodsTest{

    static TestMethod void TestProductGroupMappingTriggerMethods(){
        
        Account acc = new Account();
        acc.Name = 'Test Releted List';
        insert acc;
        
        Contract con = new Contract();
        con.Name = 'test';
        //con.Shared_divisions__c = 'IC';
        con.accountid = acc.id;
        Insert con;
        
        con.Status = 'Activated';
        update con;
        
        list<Shared_Product_Group_Mapping__c> lstGrp = new list<Shared_Product_Group_Mapping__c>(); 
        
        lstGrp.add(TestDataFactory.createProductGrpMapping('IC',con.Id));
        
        lstGrp.add(TestDataFactory.createProductGrpMapping('EP',con.Id));
        
        Test.StartTest();
        insert lstgrp;
        
        list<Contract> lstContr = [Select id, Shared_Divisions__c from contract];
        if(lstContr.size() > 0){
            System.assert(lstContr.get(0).Shared_Divisions__c.contains('IC') && lstContr.get(0).Shared_Divisions__c.contains('EP'));            
        }
        
        list<Shared_Product_Group_Mapping__c> lstGrp1 = [select id from Shared_Product_Group_Mapping__c where Shared_Division__c = 'EP'];
        if(lstGrp1.size() > 0){
            delete lstGrp1;
            
        }
        lstContr = [Select id, Shared_Divisions__c from contract];
        System.assert(!lstContr.get(0).Shared_Divisions__c.contains('EP'));
        Test.StopTest();
              
    }
}
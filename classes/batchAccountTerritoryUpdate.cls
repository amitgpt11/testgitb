/*
 @CreatedDate     30MAR2016                                  
 @ModifiedDate    04APR2016                                  
 @author          Mayuri-Accenture
 @Description     Batch Apex class to add an Account to its territory based on Account_Team_Members__c records. The Account_Team_Members__c records are updated to Salesforce
                  from a batch run by SAP. This batch class invokes another batch to update Territory details of Users in its finish() method.
 @Methods         batchAccountTerritoryUpdate 
 @Requirement Id  
 */
 
 global class batchAccountTerritoryUpdate implements Database.Batchable<sObject> { 
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    Date dt;
    if(Test.isRunningTest()){   
        dt = system.today();
    }
    else{
        dt = system.today()-Integer.valueOf(system.Label.ETM_Batch_Integer_Subtractor_For_Date);
    }
    String query =  'select ' +
                    'Account_Team_Role__c, ' +
                    'Personnel_ID__r.User_for_account_team__c, ' +
                    'Personnel_ID__r.User_for_account_team__r.IsActive, ' +
                    'Account_Team_Share__c, ' +
                    'Deleted__c, ' +
                    'Account_Deleted__c, ' + //added for SFDC-13 JD 10/5/16
                    'Territory_ID__c ' +
                    'from Account_Team_Member__c ' +
                    'where Account_Team_Share__c != null ' +
                    'and Territory_ID__c != null ';
        if(Test.isRunningTest()){            
               query += 'and LastModifiedDate =: dt';
       }
       else{
             query += 'and LastModifiedDate >: dt';
       }             
        system.debug('Value of tempVar :'+dt);
                    
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account_Team_Member__c> scope) {
    
        list<Account_Team_Member__c> remAcctTerritoryLst = new list<Account_Team_Member__c>();
        list<Account_Team_Member__c> nonRemAcctTerritoryLst = new list<Account_Team_Member__c>();
         for(Account_Team_Member__c a : scope)
         {   
            // SFDC-13 Added logic for Account Deleted indicator JD 10/5/16            
            if(a.Deleted__c == true || (a.Account_Deleted__c == true && a.Personnel_ID__c == null) ){                
                remAcctTerritoryLst.add(a);
            }
            if((a.Deleted__c == false && a.Account_Deleted__c == false) || (a.Deleted__c == false && a.Account_Deleted__c == true && a.Personnel_ID__c <> null) ){                
                nonRemAcctTerritoryLst.add(a);
            }
                      
         } 
         
         UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
         boolean result;
         boolean result1;
         system.debug('--remAcctTerritoryLst--'+remAcctTerritoryLst);
         result = util.userAccountTerritoryUpdate(scope,remAcctTerritoryLst,TRUE,TRUE);
         system.debug('--result--'+result);
         if(result == true){
             result1 = util.userAccountTerritoryUpdate(scope,nonRemAcctTerritoryLst,TRUE,FALSE);
         }       
        
    }   
    
    global void finish(Database.BatchableContext BC) {
        integer batchSize = Integer.valueOf(system.Label.ETM_UserTerritory_Update_Batch_Size);
        batchUserTerritoryUpdate b = new batchUserTerritoryUpdate();
        database.executebatch(b,batchSize);        
    }
}
/** 
Description: This class works as Controller for HomePageAuthenticated page
*/
global without sharing class HomePageAuthenticatedController extends AuthorizationUtil{
    
    public User objUser {get;set;}
    public Community_Event__c objFeaturedEvent {get;set;}
    public Product2 objFeaturedProduct {get;set;}
    public Boston_Scientific_Config__c objBSCustomSetting {get;set;}
    public Boolean showFeaturedVideo {get;set;}
    
    public override void fetchRequestedData() {
        
        objFeaturedEvent = new Community_Event__c();
        objFeaturedProduct = new Product2();
        
        String featuredEventFieldAPIName = '';
        String featuredProductFieldAPIName = '';
        
        List<Community_Event__c> lstCommunityEvents = new List<Community_Event__c>();
        List<Product2> lstProducts = new List<Product2>();
        
        if(Boston_Scientific_Config__c.getValues('Default') == null) return; 
        
        objUser = [Select Name, Contact.Owner.SmallPhotoUrl, Contact.Owner.FullPhotoUrl,Contact.Owner.Representative_Intro__c, Shared_Community_Division__c, contact.Account.BillingCountry     
                   From User 
                   Where Id =: UserInfo.getUserId()];
        
        featuredEventFieldAPIName = (objUser.Shared_Community_Division__c == 'Endo') ? 'Boston_AuthHome_Endo_FeaturedEvent_Id__c' : 'Boston_AuthHome_Cardio_FeaturedEvent_Id__c';
        //featuredProductFieldAPIName = (objUser.Shared_Community_Division__c == 'Endo') ? 'Shared_Boston_Endo_FeaturedPrduct_Id__c' : 'Shared_Boston_Cardio_FeaturedPrduct_Id__c';
        
        //updated code 21/10/2016 to display featured product based on country of contact account
        Shared_Featured_Product__c objFeaturedProductCustomSetting;
        for(Shared_Featured_Product__c featuredProductObj : [Select id,Shared_Country_Abbrevations__c,Shared_Product_Id__c,Shared_Community_Division__c
                                                            from  Shared_Featured_Product__c 
                                                            where Shared_Community_Division__c =: objUser.Shared_Community_Division__c]){
            
            if(featuredProductObj.Shared_Country_Abbrevations__c != null && objUser.contact.Account.BillingCountry != null){
                
                 if(featuredProductObj.Shared_Country_Abbrevations__c.contains(objUser.contact.Account.BillingCountry)){
                            
                    objFeaturedProductCustomSetting = featuredProductObj;
                }
            }

        }
        
        if(objFeaturedProductCustomSetting == null){
            objFeaturedProductCustomSetting = Shared_Featured_Product__c.getValues('Default');
            system.debug('22222'+objFeaturedProductCustomSetting);
        }
        
        objBSCustomSetting = Boston_Scientific_Config__c.getValues('Default');
        
        //showFeaturedVideo = (objUser.Shared_Community_Division__c == 'Endo') ? (objBSCustomSetting.get('Boston_AuthHome_Endo_Show_Video__c') == true ? true : false) : (objBSCustomSetting.get('Boston_AuthHome_Cardio_Show_Video__c') == true ? true : false);
        
        lstCommunityEvents = [Select Shared_Display_Name__c, Shared_End_Date__c, Shared_Start_Date__c, Shared_Description__c, Shared_Event_Hyperlink__c, Shared_Image_URL__c
                              From Community_Event__c 
                              Where Id =: String.valueOf(objBSCustomSetting.get(featuredEventFieldAPIName))
                              limit 1];
        
        if(!lstCommunityEvents.isEmpty()){
            
            objFeaturedEvent = lstCommunityEvents[0];
        }
        
        if(objFeaturedProductCustomSetting != null){
            lstProducts = [Select Id, Product_Label__c, Shared_Community_Description__c, Shared_Product_Image_URL__c, Shared_Video_URL__c
                           From Product2 
                           Where Id =: objFeaturedProductCustomSetting.Shared_Product_Id__c
                           Limit 1];
        }
        
        if(!lstProducts.isEmpty()){
            
            objFeaturedProduct = lstProducts[0];
            
            showFeaturedVideo = !String.isBlank(objFeaturedProduct.Shared_Video_URL__c);
        }
    }
}
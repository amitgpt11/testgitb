@isTest 
private class DeletePatientCaseExtensionTest{


@testSetup 
static void setupData() {

Map <String,Schema.RecordTypeInfo> recordTypes_Account = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_Account = recordTypes_Account.get('Customer').getRecordTypeId();

Account lk_Facility = new Account(
                                Name ='Test Account',
                               RecordTypeId = recordTypeID_Account
                                );
insert lk_Facility;

Map <String,Schema.RecordTypeInfo> recordTypes_Patient_Case = Shared_Patient_Case__c.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Patient_Case = recordTypes_Patient_Case.get('Watchman Patient Case').getRecordTypeId();

Shared_Patient_Case__c patientCase = new Shared_Patient_Case__c(
                                        Shared_Facility__c = lk_Facility.Id,
                                        Shared_Start_Date_Time__c = System.Now(),
                                        Shared_End_Date_Time__c = System.Now() + 3,
                                        RecordTypeId = recordTypeID_Patient_Case
                                     );
insert patientCase;

}

static testMethod void TestSave() {
    Test.startTest();   
    
   
    List<Shared_Patient_Case__c> patientCase = [Select Id,Shared_Facility__c from Shared_Patient_Case__c limit 1];
    /*
    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    DeletePatientCaseExtension deletePC = new DeletePatientCaseExtension(stc);
    patientCase[0].Shared_Facility__c  = null;
    deletePC.patientCase = patientCase[0];
    delete patientCase;
    deletePC.removePatientForWatchMan();
    patientCase = [Select Id,Shared_Facility__c from Shared_Patient_Case__c limit 1];
    
    System.assert(patientCase.Size() >  0);
    */
    Test.setCurrentPageReference(new PageReference('Page.DeletePatientCase'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);
    
    ApexPages.StandardController stc1 = new ApexPages.StandardController(patientCase[0]);
    DeletePatientCaseExtension deletePC1 = new DeletePatientCaseExtension(stc1);
    deletePC1.patientCase = patientCase[0];
    deletePC1.removePatientForWatchMan();
    patientCase = [Select Id,Shared_Facility__c from Shared_Patient_Case__c limit 1];
    
    System.assert(patientCase.Size() == 0);
    
    Test.stopTest();
}
}
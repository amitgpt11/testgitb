/**
* Manager class for custom Inventory_Transaction__c object
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public without sharing class InventoryTransactionManager {

  @TestVisible
  static final String ZTKA = 'ZTKA';
  @TestVisible
  static final String ZTKB = 'ZTKB';
  @TestVisible
  static final String REP_INITIATED = 'Rep Initiated';
  @TestVisible
  static final String DISPO_RECD = 'Received';
  @TestVisible
  static final String IN_TRANSIT = 'In Transit';
  static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Inventory_Transaction__c.getRecordTypeInfosByName();

  public static final Id RECTYPE_TRANSACTION = RECTYPES.get('NMD Inventory Transaction').getRecordTypeId();

  private Map<Id,OrderItem> parentOrderItems = new Map<Id,OrderItem>();
  private Map<Id,OrderItem> updatedOrderItems = new Map<Id,OrderItem>();
  private static boolean isRecursiveCall = false;
  private Map<Id,List<Inventory_Transaction__c>> siblings = new Map<Id,List<Inventory_Transaction__c>>();
  private Map<Id,Inventory_Item__c> parentInvItems = new Map<Id,Inventory_Item__c>();
  private Set<Id> updatedInvItemIds = new Set<Id>{};
  private Map<String,Inventory_Transaction__c> transZTKAs = new Map<String,Inventory_Transaction__c>();
  private Map<String,Inventory_Transaction__c> transZTKBs = new Map<String,Inventory_Transaction__c>();
  private Map<Id,Inventory_Transaction__c> updatedTrans = new Map<Id,Inventory_Transaction__c>();

  //Used for Checking if Orders should be activated
  private Map<Id, Order> parentOrders = new Map<Id, Order>();
  private Map<Id, List<Inventory_Transaction__c>> transByOrder = new Map<Id, List<Inventory_Transaction__c>>();
  private Map<Id, List<OrderItem>> orderItemByOrder = new Map<Id, List<OrderItem>>();
  private Map<Id, Order> updatedOrders = new Map<Id, Order>();
  private Map<Id,User> InvItemWithLocationOwners = new Map<Id,User>(); 

  //Removed Process builder Update of 'ZTKB before ZTKA' and adding it to trigger. this should be called in bulkBefore
  public void fetchLocationOwners(List<Inventory_Transaction__c> records){
      Set<Id> invTransLoc = new Set<Id>();
      for(Inventory_Transaction__c itc: records){
          invTransLoc.add(itc.inventory_item__r.id);
      }
      system.debug('*******invTransLoc************'+invTransLoc);
      List<inventory_item__c> locationowners = [select id, inventory_location__r.inventory_data_ID__r.Owner.id from inventory_item__c where id in :invTransLoc];
      
      for(inventory_item__c itc: locationowners){
          User u = new User();
          u.id = itc.inventory_location__r.inventory_data_ID__r.OwnerId;
          
          InvItemWithLocationOwners.put(itc.id, u);
      }
      
  }
  
  //Removed Process builder Update of 'ZTKB before ZTKA' and adding it to trigger. 
  //runs before insert or before update. 
  public void updateTransactionRecord(Inventory_Transaction__c newTrans){
      if(newTrans.Order__c==null && newTrans.order_type__C==ZTKB && (newTrans.Xfer_Type__c==null || newTrans.Xfer_Type__c=='' || newTrans.Xfer_Type__c==REP_INITIATED)){
          if(InvItemWithLocationOwners.get(newTrans.inventory_item__r.id)!=null && newTrans.Inventory_Location_Owner__c!=InvItemWithLocationOwners.get(newTrans.inventory_item__r.id).id){
              //updating Location Owner
              newTrans.Inventory_Location_Owner__c=InvItemWithLocationOwners.get(newTrans.inventory_Item__r.id).id;
          }
          
          if(newTrans.Quantity__c!=newTrans.Delivery_Quantity__c){
              //updating Quantity
              newTrans.Quantity__c=newTrans.Delivery_Quantity__c;
          }
          
          if(newTrans.Disposition__c!=DISPO_RECD){
              //updating Quantity
              newTrans.Disposition__c=DISPO_RECD;
          }
          
      }
      
  }
  
  public void fetchRelatedRecords(List<Inventory_Transaction__c> oldRecords, List<Inventory_Transaction__c> records) {

    Set<Id> orderIds = new Set<Id>();
    Set<Id> orderItemIds = new Set<Id>();
    Set<Id> invItemIds = new Set<Id>();
    if (records != null) {
      for (Inventory_Transaction__c trans : records) {
        
        if (trans.Order_Item__c != null) {
          orderItemIds.add(trans.Order_Item__c);
        }
        
        if (trans.Inventory_Item__c != null) {
          invItemIds.add(trans.Inventory_Item__c);
        }

        //get order ids
        if(trans.Order__c != null){
          orderIds.add(trans.Order__c);
        }

      }
    }

    if (oldRecords != null) {
      for (Inventory_Transaction__c trans : oldRecords) {
        
        if (trans.Order_Item__c != null) {
          orderItemIds.add(trans.Order_Item__c);
        }
        
        if (trans.Inventory_Item__c != null) {
          invItemIds.add(trans.Inventory_Item__c);
        }

      }
    }

    //query for return orders related to the Inventory_Transaction__c
    if(!orderIds.isEmpty()){
      this.parentOrders.putAll(new Map<Id, Order>([
          select
            Status,
            RecordTypeId
          from Order
          where Id in :orderIds
          and RecordTypeId = :OrderManager.RECTYPE_RETURN
          and Status != 'Activated'
        ]));
    }

    //If a return order is located, query for its chile OrderItems and Inventory_Transaction__c records
    if(!parentOrders.isEmpty()){
      //build list of Inventory Transactions by Order
      for(Inventory_Transaction__c tran : 
        [SELECT Id, 
          Order__c,
          Delivery_Quantity__c 
         FROM Inventory_Transaction__c 
         WHERE Order__c IN :parentOrders.keySet()
      ]){
        if(!transByOrder.containsKey(tran.Order__c)){
          transByOrder.put(tran.Order__c, new List<Inventory_Transaction__c>());
        }
        transByOrder.get(tran.Order__c).add(tran);
      }

      //build list of OrderItems by Order
      for(OrderItem item : 
        [SELECT Id,
          OrderId,
          Quantity
        FROM OrderItem
        WHERE OrderId IN :parentOrders.keySet()
      ]){
        if(!orderItemByOrder.containsKey(item.OrderId)){
          orderItemByOrder.put(item.OrderId, new List<OrderItem>());
        }
        orderItemByOrder.get(item.OrderId).add(item);
      }
    }


    if (!orderItemIds.isEmpty()) {
      this.parentOrderItems.putAll(new Map<Id,OrderItem>([
        select
          Order.RecordTypeId,
          Originating_Order_Item__c,
          Inventory_Source__c
        from OrderItem
        where Id in :orderItemIds
      ]));
    }

    if (!invItemIds.isEmpty()) {
      for (Inventory_Item__c item : [
        select 
          Available_Quantity__c,
          In_Transit__c,
          SAP_Calculated_Quantity__c,
          (
            select
              Delivery_Quantity__c,
              Disposition__c,
              Quantity__c
            from Inventory_Transactions__r
          )
        from Inventory_Item__c
        where Id in :invItemIds
      ]) {
        this.parentInvItems.put(item.Id, item);
        this.siblings.put(item.Id, item.Inventory_Transactions__r);
      }
    }

  }

  public void fetchMatchingZTKs(List<Inventory_Transaction__c> records) {

    // find matching ZTKA/ZTKB records
    Set<Id> itemIds = new Set<Id>();
    Set<Id> prodIds = new Set<Id>();
    Set<String> ztkaNos = new Set<String>();
    for (Inventory_Transaction__c trans : records) {
      if (trans.Model_Number__c != null
       && ((trans.Order_Type__c == ZTKA && String.isNotBlank(trans.Sales_Order_No__c))
        || (trans.Order_Type__c == ZTKB && String.isNotBlank(trans.ZTKA_No__c))
      )) {
        
        prodIds.add(trans.Model_Number__c);
        
        if (trans.Order_Item__c != null) {
          itemIds.add(trans.Order_Item__c);
        }

        if (trans.Order_Type__c == ZTKA) {
          ztkaNos.add(trans.Sales_Order_No__c);
        }
        else if (trans.Order_Type__c == ZTKB) {
          ztkaNos.add(trans.ZTKA_No__c);
        }

      }
    }
    
    system.debug('***In fetchMatchingZTKs  ztkaNos '+ ztkaNos);
    system.debug('***In fetchMatchingZTKs  prodIds '+ prodIds);

    if (!prodIds.isEmpty() && !ztkaNos.isEmpty()) {
      
      for (Inventory_Transaction__c trans : [
        select
          Order__c,
          Order_Item__c,
          Order_Type__c,
          Inventory_Item__c,
          Lot_Number__c,
          Model_Number__c,
          Sales_Order_No__c,
          Serial_Number__c,
          ZTKA_No__c,
          Order_Item__r.Order_Item_Id__c
        from Inventory_Transaction__c
        where Model_Number__c in :prodIds
        and ((Order_Type__c = :ZTKA and Sales_Order_No__c in :ztkaNos)
          or (Order_Type__c = :ZTKB and ZTKA_No__c in :ztkaNos))
      ]) {

        if (trans.Order_Item__c != null) {
          itemIds.add(trans.Order_Item__c);
        }

        if (trans.Order_Type__c == ZTKA) {
          String orderItemId = '';
          if(trans.Order_Item__c != null){
            orderItemId = '' + Integer.valueOf(trans.Order_Item__r.Order_Item_Id__c);
          }
          //this.transZTKAs.put(trans.Model_Number__c + trans.Sales_Order_No__c, trans);
          this.transZTKAs.put(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + trans.Order_Item__r.Order_Item_Id__c + trans.Sales_Order_No__c, trans);

        }
        else if (trans.Order_Type__c == ZTKB) {
          //this.transZTKBs.put(trans.Model_Number__c + trans.ZTKA_No__c, trans);    

          this.transZTKBs.put(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + trans.ZTKA_No__c, trans);

        }
      }

      system.debug('***In fetchMatchingZTKs  transZTKAs '+ transZTKAs);
      system.debug('***In fetchMatchingZTKs  transZTKBs '+ transZTKBs);
      system.debug('***In fetchMatchingZTKs  itemIds '+ itemIds);

      if (!itemIds.isEmpty()) {

        this.parentOrderItems.putAll(new Map<Id,OrderItem>([
          select Originating_Order_Item__c
          from OrderItem
          where Id in :itemIds
        ]));

      }

    }

  }

    /**
    *  For a Return Order, checks the total Quantity of Order Items and compares it to the 
    *  Absolute Value of the total Delivery_Quantity__c of each Inventory_Transaction__c
    *  If they are equal, then sets the Order.Status to 'Activated'
  *
    *  @param newTrans New Inventory_Transaction__c Object
    */
  public void updateOrderStatusToActivated(Inventory_Transaction__c newTrans){

    //Check if a return order was found
    if(newTrans.Order__c != null
      && parentOrders.containsKey(newTrans.Order__c)
    ){
      //remove the return order form the map so it is only checked once
      Order parent = parentOrders.remove(newTrans.Order__c);
      if(parent.RecordTypeId == OrderManager.RECTYPE_RETURN
        && parent.Status != 'Activated'
        && transByOrder.containsKey(parent.Id)
        && orderItemByOrder.containsKey(parent.Id)
      ){

        //Count the Quantity for each OrderItem
        //  and the Delivery_Quantity__c for each Inventory_Transaction__c
        Decimal itemCount = 0.0, transCount = 0.0;

        for(OrderItem item : orderItemByOrder.remove(parent.Id)){
          if(item.Quantity != null){
            itemCount += item.Quantity;
          }
        }

        for(Inventory_Transaction__c tran : transByOrder.remove(parent.Id)){
          if(tran.Delivery_Quantity__c != null){
            transCount += tran.Delivery_Quantity__c;
          }
        }

        //If the Quantity total and Delivery Quantity total (abs value) 
        //  match, then set the Order to Activated
        if(itemCount == Math.abs(transCount)){
          parent.Status = 'Activated';
          updatedOrders.put(parent.Id, parent);
        }

      }
    }
  }

  public void updateParentAvailableQuantity(Inventory_Transaction__c oldTrans, Inventory_Transaction__c newTrans) {
    system.debug('****Calling update**** '+ oldTrans);
    system.debug('****Calling update**** '+ newTrans);
    if (oldTrans.Inventory_Item__c != newTrans.Inventory_Item__c
     || oldTrans.Quantity__c != newTrans.Quantity__c) {

      if (oldTrans.Inventory_Item__c != null) {
        updateParentAvailableQuantity(oldTrans);
      }

      if (newTrans.Inventory_Item__c != null && oldTrans.Inventory_Item__c != newTrans.Inventory_Item__c) {
        updateParentAvailableQuantity(newTrans);  
      }

    }

  }

  public void updateParentAvailableQuantity(Inventory_Transaction__c trans) {
    system.debug('****Calling Insert****' + trans);
    if (trans.Inventory_Item__c == null) return;

    Inventory_Item__c item = this.parentInvItems.get(trans.Inventory_Item__c);

    Double total = 0;
    for (Inventory_Transaction__c child : this.siblings.get(item.Id)) {
    system.debug('****Calling Insert Total**** child: >>> '+child);
      total += child.Quantity__c != null ? child.Quantity__c : 0;
    }
    system.debug('****Calling Insert out total **** '+ total );
    if (total != item.Available_Quantity__c) {
      item.Available_Quantity__c = total;
      this.updatedInvItemIds.add(trans.Inventory_Item__c);
    }

  }

  public void updateParentInTransit(Inventory_Transaction__c oldTrans, Inventory_Transaction__c newTrans) {
    
    if (oldTrans.Inventory_Item__c != newTrans.Inventory_Item__c
     || oldTrans.Delivery_Quantity__c != newTrans.Delivery_Quantity__c
     || oldTrans.Disposition__c != newTrans.Disposition__c
    ) {

      if (oldTrans.Inventory_Item__c != null && oldTrans.Disposition__c == IN_TRANSIT) {
        updateParentInTransit(oldTrans);
      }

      if (newTrans.Inventory_Item__c != null && newTrans.Disposition__c == IN_TRANSIT) {
        updateParentInTransit(newTrans);
      }

    }

  }

  public void updateParentInTransit(Inventory_Transaction__c trans) {
    
    if (trans.Inventory_Item__c == null) return;

    Inventory_Item__c item = this.parentInvItems.get(trans.Inventory_Item__c);

    Double total = 0;
    for (Inventory_Transaction__c child : this.siblings.get(item.Id)) {
      if (child.Disposition__c == IN_TRANSIT) {
        total += child.Delivery_Quantity__c == null ? 0 : child.Delivery_Quantity__c;
      }
    }

    if (total != item.In_Transit__c) {
      item.In_Transit__c = total;
      this.updatedInvItemIds.add(trans.Inventory_Item__c);
    }

  }

  public void updateParentSAPCalculatedQuantity(Inventory_Transaction__c oldTrans, Inventory_Transaction__c newTrans) {
    
    if (oldTrans.Inventory_Item__c != newTrans.Inventory_Item__c
     || oldTrans.Delivery_Quantity__c != newTrans.Delivery_Quantity__c
    ) {

      if (oldTrans.Inventory_Item__c != null) {
        updateParentSAPCalculatedQuantity(oldTrans);
      }

      if (newTrans.Inventory_Item__c != null) {
        updateParentSAPCalculatedQuantity(newTrans);  
      }

    }

  }

  public void updateParentSAPCalculatedQuantity(Inventory_Transaction__c trans) {
    
    if (trans.Inventory_Item__c == null) return;

    Inventory_Item__c item = this.parentInvItems.get(trans.Inventory_Item__c);

    Double total = 0;
    for (Inventory_Transaction__c child : this.siblings.get(item.Id)) {
      total += child.Delivery_Quantity__c == null ? 0 : child.Delivery_Quantity__c;
    }

    if (total != item.SAP_Calculated_Quantity__c) {
      item.SAP_Calculated_Quantity__c = total;
      this.updatedInvItemIds.add(trans.Inventory_Item__c);
    }

  }

  //ZTKB coming in to match ZTKA
  public void matchZTKATransfer(Inventory_Transaction__c trans) {
      system.debug('****In matchZTKATransfer  '+trans);
      
    //Ger Order Item Id from external Id, if only 2 characters, remove the dash
    String orderItemId = '';
    if(String.isNotBlank(trans.External_Id__c)){
      orderItemId = trans.External_Id__c.right(3);
      orderItemId = orderItemId.replace('-', '');
    }
      system.debug('****orderItemId   '+orderItemId);
      system.debug('****transZTKAs   '+this.transZTKAs);
      
    if (trans.Order_Type__c == ZTKB
     && trans.Model_Number__c != null
     && String.isNotBlank(trans.ZTKA_No__c)
     && String.isNotBlank(trans.Lot_Number__c)
     && this.transZTKAs.containsKey(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + orderItemId + trans.ZTKA_No__c)
    ) {

      system.debug('****In Condition   '+trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + orderItemId + trans.ZTKA_No__c);
      
      Inventory_Transaction__c ztkaTrans = this.transZTKAs.get(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + orderItemId + trans.ZTKA_No__c);
      copyZTKAOrderInfo(ztkaTrans, trans, false);

    }

  }

  //ZTKA Coming in to match ZTKB
  public void matchZTKBTransfer(Inventory_Transaction__c trans) {
    system.debug('****In matchZTKBTransfer  '+trans);

      system.debug('****transZTKBs   '+this.transZTKBs);
  
    if (trans.Order_Type__c == ZTKA
     && trans.Model_Number__c != null
     && String.isNotBlank(trans.Sales_Order_No__c)
     && String.isNotBlank(trans.Lot_Number__c)
     && this.transZTKBs.containsKey(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + trans.Sales_Order_No__c)
    ) {
      system.debug('****In Condition   '+trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + trans.Sales_Order_No__c);

      Inventory_Transaction__c ztkbTrans = this.transZTKBs.get(trans.Model_Number__c + trans.Lot_Number__c + trans.Serial_Number__c + trans.Sales_Order_No__c);
      copyZTKAOrderInfo(trans, ztkbTrans, true);

    }

  }

  private void copyZTKAOrderInfo(Inventory_Transaction__c ztkaTrans, Inventory_Transaction__c ztkbTrans, Boolean allowUpdate) {


    system.debug('****In copyZTKAOrderInfo  '+ztkaTrans);
    system.debug('****In copyZTKAOrderInfo  '+ztkbTrans);
    system.debug('****In copyZTKAOrderInfo  '+allowUpdate);


    ztkbTrans.Order__c = ztkaTrans.Order__c;
    ztkbTrans.Order_Item__c = ztkaTrans.Order_Item__c;
    if (allowUpdate) {
      this.updatedTrans.put(ztkbTrans.Id, ztkbTrans);
    }

    if (ztkaTrans.Order_Item__c != null) {
      
      this.updatedOrderItems.put(ztkaTrans.Order_Item__c, new OrderItem(
        Id = ztkaTrans.Order_Item__c,
        Inventory_Source__c = ztkbTrans.Inventory_Item__c
      ));
      system.debug('****In copyZTKAOrderInfo parentOrderItems  '+this.parentOrderItems);

      OrderItem item = this.parentOrderItems.get(ztkaTrans.Order_Item__c);
      system.debug('****In copyZTKAOrderInfo item  '+item);
      
      
      if (item != null && item.Originating_Order_Item__c != null) {
        this.updatedOrderItems.put(item.Originating_Order_Item__c, new OrderItem(
          Id = item.Originating_Order_Item__c,
          Inventory_Source__c = ztkbTrans.Inventory_Item__c
        ));
      }

    }

  }

  public void commitRelatedRecords() {

    //List<ApplicationLogWrapper> errlogs = new List<ApplicationLogWrapper>();
    //List<Database.SaveResult> results;

    DML.deferLogs();
    
    if (!isRecursiveCall && !this.updatedOrderItems.isEmpty()) {
      //results = Database.update(this.updatedOrderItems.values(), false);
      //for (Database.SaveResult sr : results) {
    //          if (!sr.isSuccess()) {
    //              for (Database.Error err : sr.getErrors()) {
    //                  errlogs.add(new ApplicationLogWrapper(err, sr.getId(), err.getMessage()));
    //              }
    //          }
    //      }
    isRecursiveCall= true;
        DML.save(this, this.updatedOrderItems.values(), false);
      this.updatedOrderItems.clear();
    }

    if (!this.updatedTrans.isEmpty()) {
      //results = Database.update(this.updatedTrans.values(), false);
      //for (Database.SaveResult sr : results) {
    //          if (!sr.isSuccess()) {
    //              for (Database.Error err : sr.getErrors()) {
    //                  errlogs.add(new ApplicationLogWrapper(err, sr.getId(), err.getMessage()));
    //              }
    //          }
      //}  
      DML.save(this, this.updatedTrans.values(), false);
      this.updatedTrans.clear();
    }

    if (!this.updatedInvItemIds.isEmpty()) {

      List<Inventory_Item__c> parents = new List<Inventory_Item__c>();
      for (Id parentId : this.updatedInvItemIds) {
        parents.add(this.parentInvItems.get(parentId));
      }

      //results = Database.update(parents, false);
      //for (Database.SaveResult sr : results) {
    //          if (!sr.isSuccess()) {
    //              for (Database.Error err : sr.getErrors()) {
    //                  errlogs.add(new ApplicationLogWrapper(err, sr.getId(), err.getMessage()));
    //              }
    //          }
    //      }
      
        
        DML.save(this, parents, false);

    }

    if(!this.updatedOrders.isEmpty()){
      DML.save(this, this.updatedOrders.values(), false);
      this.updatedOrders.clear();
    }

    //if (!errlogs.isEmpty()) {
  //          Logger.logMessage(errlogs);
  //      }
      DML.flushLogs(false);

  }

}
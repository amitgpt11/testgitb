/**
* 
*
* @Author salesforce Services
* @Date 02/26/2016
*/
global class NMD_OpportunitySAPIdsBatch implements Database.Batchable<SObject> {

    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
    public static final Id RECTYPE_TRIAL = RECTYPES_OPPTY.get('NMD SCS Trial-Implant').getRecordTypeId();
    public static final Id RECTYPE_TRIAL_NEW = RECTYPES_OPPTY.get('NMD SCS Trial').getRecordTypeId();
    public static final Id RECTYPE_IMPLANT = RECTYPES_OPPTY.get('NMD SCS Implant').getRecordTypeId();    
    public static final Id RECTYPE_EXPLANT = RECTYPES_OPPTY.get('NMD SCS Explant').getRecordTypeId();
    public static final Id RECTYPE_REVISION = RECTYPES_OPPTY.get('NMD SCS Revision').getRecordTypeId();
    public static final Id RECTYPE_REPROGRAMMING = RECTYPES_OPPTY.get('NMD Reprogramming').getRecordTypeId();
    
    public static final set<Id> recordTypeIdSet = new set<Id>{RECTYPE_TRIAL,RECTYPE_TRIAL_NEW,RECTYPE_IMPLANT,RECTYPE_EXPLANT,RECTYPE_REVISION,RECTYPE_REPROGRAMMING};    
    
    
    // static entry point
    public static Id runNow() {
        return Database.executeBatch(new NMD_OpportunitySAPIdsBatch());
    }

    global String query =
        'select ' +
            'Patient__r.SAP_ID__c, ' +
            'Patient_SAP_ID__c, ' +
            'Procedure_Account__r.Account_Number__c, ' +
            'Procedure_Account_SAP_ID__c, ' +
            'Procedure_Physician__r.SAP_ID__c, ' +
            'Procedure_Physician_SAP_ID__c, ' +
            'Trialing_Account__r.Account_Number__c, ' +
            'Trialing_Account_SAP_ID__c, ' +
            'Trialing_Physician__r.SAP_ID__c, ' +
            'recordTypeId, '+
            'Trialing_Physician_SAP_ID__c ' +
        'from Opportunity ' +
        'where recordTypeId IN: recordTypeIdSet and ((Patient_SAP_ID__c = null and Patient__r.SAP_ID__c != null) ' +
        'or (Procedure_Account_SAP_ID__c = null and Procedure_Account__r.Account_Number__c != null) ' +
        'or (Procedure_Physician_SAP_ID__c = null and Procedure_Physician__r.SAP_ID__c != null) ' +
        'or (Trialing_Account_SAP_ID__c = null and Trialing_Account__r.Account_Number__c != null) ' +
        'or (Trialing_Physician_SAP_ID__c = null and Trialing_Physician__r.SAP_ID__c != null)) ';

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(this.query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> opptys) {

        for (Opportunity oppty : opptys) {

            // since any record being evaluated will need at least one update, and
            // since all the SAP IDs should always be aligned, there is no need to
            // check for a mach on each field.  Just grab the current value as-is.
            if(oppty.Patient__r.SAP_ID__c != null){
                oppty.Patient_SAP_ID__c = oppty.Patient__r.SAP_ID__c;
            }
            if(oppty.recordTypeId == RECTYPE_TRIAL){
                if(oppty.Trialing_Account__c != null && oppty.Trialing_Account__r.Account_Number__c != null){
                    oppty.Trialing_Account_SAP_ID__c = oppty.Trialing_Account__r.Account_Number__c;
                }
                if(oppty.Trialing_Physician__c != null && oppty.Trialing_Physician__r.SAP_ID__c != null){
                    oppty.Trialing_Physician_SAP_ID__c = oppty.Trialing_Physician__r.SAP_ID__c;
                }
            }else{
                if(oppty.Procedure_Account__c != null && oppty.Procedure_Account__r.Account_Number__c != null){
                    oppty.Procedure_Account_SAP_ID__c = oppty.Procedure_Account__r.Account_Number__c;
                }
                if(oppty.Procedure_Physician__c != null && oppty.Procedure_Physician__r.SAP_ID__c != null){
                    oppty.Procedure_Physician_SAP_ID__c = oppty.Procedure_Physician__r.SAP_ID__c;
                }
            }
            

        }

        DML.save(this, opptys, false);

    }

    global void finish(Database.BatchableContext bc) {

    }

}
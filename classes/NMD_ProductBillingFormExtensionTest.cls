@IsTest(SeeAllData=false)
private class NMD_ProductBillingFormExtensionTest {
    
    final static String ACCTNAME = 'Account Name';
    final static String SERIAL1 = '1234567890';
    final static String SERIAL2 = '0987654321';
    final static String SERIAL3 = '5555654321';
    
    @TestSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();

        User testUser = td.newUser('NMD Field Rep');
        insert testUser;

        System.runAs(new User(Id = UserInfo.getUserId())) {

            Seller_Hierarchy__c territory = td.newSellerHierarchy();
            insert territory;

            Patient__c patient = new Patient__c(
                RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
                Territory_ID__c = territory.Id,
                SAP_ID__c = td.randomString5()
            );
            insert patient;

            // create one consignment acct for context user, one for a test user
            Account acct1 = td.createConsignmentAccount();
            acct1.Name = ACCTNAME;

            Account acct2 = td.newAccount();
            acct2.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
            acct2.Status__c = 'Active';
            acct2.OwnerId = testUser.Id;

            Account acctTrialing = td.newAccount();
            acctTrialing.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
            acctTrialing.Type = 'Sold To';
            acctTrialing.Account_Number__c = td.randomString5();

            Account acctProdcedure = td.newAccount();
            acctProdcedure.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
            acctProdcedure.Account_Number__c = td.randomString5();

            insert new List<Account> { acct1, acct2, acctTrialing, acctProdcedure };

            Contact contTrialing = td.newContact(acctTrialing.Id);
            contTrialing.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            contTrialing.SAP_ID__c = td.randomString5();

            Contact contProcedure = td.newContact(acctProdcedure.Id);
            contProcedure.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            contProcedure.SAP_ID__c = td.randomString5();

            insert new List<Contact> { contTrialing, contProcedure };
            
            Opportunity oppty = td.newOpportunity(acct1.Id);
            oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            oppty.Primary_Insurance__c = acct2.Id;
            oppty.Territory_ID__c = territory.Id;
            oppty.StageName = 'Candidate (Trial implant)';
            oppty.Opportunity_Type__c = 'Standard';
            oppty.LeadSource = 'CARE Card';
            oppty.CloseDate = System.today().addDays(7);
            oppty.Patient__c = patient.Id;
            oppty.Patient_SAP_ID__c = patient.SAP_ID__c;
            oppty.Trialing_Account__c = acctTrialing.Id;
            oppty.Trialing_Account_SAP_ID__c = acctTrialing.Account_Number__c;
            oppty.Trialing_Physician__c = contTrialing.Id;
            oppty.Trialing_Physician_SAP_ID__c = contTrialing.SAP_ID__c;
            oppty.Procedure_Account__c = acctProdcedure.Id;
            oppty.Procedure_Physician__c = contProcedure.Id;
            oppty.Pain_Area__c = 'None';
            
            
            Opportunity implantOppty = td.newOpportunity(acct1.Id);
            implantOppty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
            implantOppty.Primary_Insurance__c = acct2.Id;
            implantOppty.Territory_ID__c = territory.Id;
            implantOppty.Opportunity_Type__c = 'Standard';
            implantOppty.StageName = 'Candidate (Trial implant)'; 
            implantOppty.LeadSource = 'CARE Card';
            implantOppty.CloseDate = System.today().addDays(1);
            implantOppty.Patient__c = patient.Id;
            implantOppty.Patient_SAP_ID__c = patient.SAP_ID__c;
            implantOppty.Procedure_Account__c = acctProdcedure.Id;
            implantOppty.Procedure_Account_SAP_ID__c = acctProdcedure.Account_Number__c;
            implantOppty.Procedure_Physician__c = contProcedure.Id;
            implantOppty.Procedure_Physician_SAP_ID__c = contProcedure.SAP_ID__c;
            implantOppty.Pain_Area__c = 'None';
            insert new List<Opportunity>{oppty, implantOppty};
            
            
           
            // create two products, pricebook, and entries
            Id pricebookId = Test.getStandardPricebookId();

            Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
            insert pb;

            Product2 prod0 = td.newProduct('Trial');
            Product2 prod1 = td.newProduct('Implant');
            Product2 prod2 = td.newProduct('Implant2');

            prod1.Xfer_Type__c = 'Auto';
            insert new List<Product2> { prod0, prod1, prod2 };

            PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod0.Id, UnitPrice = 0, IsActive = true, UseStandardPrice = false);
            PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod1.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
            PricebookEntry standardPrice2 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod2.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
            insert new List<PricebookEntry> { standardPrice0, standardPrice1, standardPrice2 };

            PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod0.Id, UnitPrice = 0, IsActive = true, UseStandardPrice = false);
            PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod1.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
            PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod2.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
            insert new List<PricebookEntry> { pbe0, pbe1, pbe2 };

            // create one inv data per acct
            Inventory_Data__c invData1 = new Inventory_Data__c(
                Account__c = acct1.Id,
                Customer_Class__c = 'YY'
            );
            Inventory_Data__c invData2 = new Inventory_Data__c(
                Account__c = acct2.Id,
                Customer_Class__c = 'YY',
                OwnerId = testUser.Id
            );
            insert new List<Inventory_Data__c> { invData1, invData2 };

            // create one inv location per inv data
            Inventory_Location__c invLoc1 = new Inventory_Location__c(
                Inventory_Data_ID__c = invData1.Id
            );
            Inventory_Location__c invLoc2 = new Inventory_Location__c(
                Inventory_Data_ID__c = invData2.Id
            );
            insert new List<Inventory_Location__c> { invLoc1, invLoc2 };

            // create one inventory item per product, alternative between inv locations
            Inventory_Item__c invItem1 = new Inventory_Item__c(
                Inventory_Location__c = invLoc1.Id,
                Model_Number__c = prod0.Id,
                Serial_Number__c = SERIAL1
            );
            Inventory_Item__c invItem2 = new Inventory_Item__c(
                Inventory_Location__c = invLoc2.Id,
                Model_Number__c = prod1.Id,
                Serial_Number__c = SERIAL2
            );
            Inventory_Item__c invItem3 = new Inventory_Item__c(
                Inventory_Location__c = invLoc2.Id,
                Model_Number__c = prod2.Id,
                Serial_Number__c = SERIAL3
            );
            insert new List<Inventory_Item__c> { invItem1, invItem2, InvItem3 };

            Inventory_Transaction__c invTrans1 = new Inventory_Transaction__c(
                Inventory_Item__c = invItem1.Id,
                Quantity__c = 10
            );
            Inventory_Transaction__c invTrans2 = new Inventory_Transaction__c(
                Inventory_Item__c = invItem2.Id,
                Quantity__c = 10
            );
            Inventory_Transaction__c invTrans3 = new Inventory_Transaction__c(
                Inventory_Item__c = invItem3.Id,
                Quantity__c = 10
            );
            insert new List<Inventory_Transaction__c> { invTrans1, invTrans2 };

            Order order1 = new Order(
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                AccountId = acct1.Id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today(),
                Pricebook2Id = pb.Id,
                PONumber = 'PO to Follow',
                Status = 'Draft'
            );
            
            Order order2 = new Order(
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                AccountId = acct1.Id,
                OpportunityId = implantOppty.Id,
                EffectiveDate = System.today(),
                Pricebook2Id = pb.Id,
                PONumber = 'PO to Follow',
                Status = 'Draft'
            );
            
            insert new List<Order>{order1,order2};

        }

    }
   static testMethod void test_ProductBillingForm() {

        List<PricebookEntry> pbes = [select Id from PricebookEntry where Pricebook2.Name = 'NMD' order by Product2Id];
        List<Inventory_Item__c> items = [select Inventory_Location__c from Inventory_Item__c order by Model_Number__c];

        Order parentOrder = [
            select Id 
            from Order 
            where Account.Name = :ACCTNAME
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT and Opportunity.RecordType.Name = 'NMD SCS Trial'
        ];

        List<OrderItem> ordItems = new List<OrderItem>();
        for (Integer i = 0; i < 2; i++) {
            ordItems.add(new OrderItem(
                OrderId = parentOrder.Id,
                PricebookEntryId = pbes[i].Id,
                Quantity = 1,
                UnitPrice = i,
                Is_Scanned__c = true,
               Inventory_Source__c = i == 0 ? null : items[i].Id,
                Inventory_Location__c = items[i].Inventory_Location__c,
                No_Charge_Reason__c = i == 0 ? 'Pre Purchase' : ''
            ));
        }

        Test.startTest();
        {
            insert ordItems;

            //Cover Virtual Kit sorting code
            ordItems[0].IsVirtualKit__c = true;
            ordItems[0].UnitPrice = 0;
            ordItems[1].IsVirtualKit__c = true;
            ordItems[1].Virtual_Kit__c = ordItems[0].Id;
            ordItems[1].UnitPrice = 0;
            update ordItems;

            PageReference pr = Page.ProductBillingForm;
            pr.getParameters().put('id', parentOrder.Id);
            Test.setCurrentPage(pr);

            NMD_ProductBillingFormExtension ext = new NMD_ProductBillingFormExtension(new ApexPages.StandardController(parentOrder));

            List<OrderItem> items1 = ext.firstItems;
            List<OrderItem> items2 = ext.secondItems;
        }
        Test.stopTest();

    }
 
    static testMethod void test_ProductBillingForm1() {

        List<PricebookEntry> pbes = [select Id from PricebookEntry where Pricebook2.Name = 'NMD' order by Product2Id];
        List<Inventory_Item__c> items = [select Inventory_Location__c from Inventory_Item__c order by Model_Number__c];
  
        Order parentOrder = [
            select Id 
            from Order 
            where Opportunity.RecordType.Name = 'NMD SCS Implant'
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT
        ];

        List<OrderItem> ordItems = new List<OrderItem>();
        for (Integer i = 0; i < 2; i++) {
            ordItems.add(new OrderItem(
                OrderId = parentOrder.Id,
                PricebookEntryId = pbes[i].Id,
                Quantity = 1,
                UnitPrice = i,
                Is_Scanned__c = true,
               Inventory_Source__c = i == 0 ? null : items[i].Id,
                Inventory_Location__c = items[i].Inventory_Location__c,
                No_Charge_Reason__c = i == 0 ? 'Pre Purchase' : ''
            ));
        }

        Test.startTest();
        {
            insert ordItems;

            //Cover Virtual Kit sorting code
            ordItems[0].IsVirtualKit__c = true;
            ordItems[0].UnitPrice = 0;
            ordItems[1].IsVirtualKit__c = true;
            ordItems[1].Virtual_Kit__c = ordItems[0].Id;
            ordItems[1].UnitPrice = 0;
            update ordItems;

            PageReference pr = Page.ProductBillingForm;
            pr.getParameters().put('id', parentOrder.Id);
            Test.setCurrentPage(pr);

            NMD_ProductBillingFormExtension ext = new NMD_ProductBillingFormExtension(new ApexPages.StandardController(parentOrder));

            List<OrderItem> items1 = ext.firstItems;
            List<OrderItem> items2 = ext.secondItems;
        }
        Test.stopTest();

    }

}
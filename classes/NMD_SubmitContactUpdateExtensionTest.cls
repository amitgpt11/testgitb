/**
* Test class for the page/class SubmitCustomerUpdate
*
* @author   Salesforce services
* @date     2015-02-12
*/
@isTest(SeeAllData=false)
private class NMD_SubmitContactUpdateExtensionTest {
	
	static final String PROSPECTACCOUNT = 'ProspectAccount';

	@testSetup
	static void setup() {
		//setup test data
		NMD_TestDataManager testData = new NMD_TestDataManager();
		Account acct = testData.newAccount(PROSPECTACCOUNT);
		acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		insert acct;

		List<Contact> contacts = new List<Contact>{
			testData.newContact(acct.Id)
		};
		insert contacts;

		List<AccContactRelationship__c> acres = new List<AccContactRelationship__c>{};
		for (Integer i = 0; i < contacts.size(); i++) {
			acres.add(testData.newAccContactRelationship(acct.Id, contacts[i].Id));
		}
		insert acres;

		//add quincy contact
		testData.setupQuincyContact();
	}

	static testMethod void test_SubmitCustomerUpdate() {

		//grab data, need to build soql string so fieldset gets added
		Id acctId = [select Id from Account where Name = :PROSPECTACCOUNT].Id;
		String contactSoql = 'select Id';
		Schema.FieldSet fieldset = Schema.SObjectType.Contact.fieldSets.getMap().get(NMD_SubmitContactUpdateExtension.QUINCY_FIELDSET);
		for (Schema.FieldSetMember field : fieldset.getFields()) {
			contactSoql += ',' + field.getFieldPath();
		}
		contactSoql += ' from Contact where AccountId = :acctId';
		Contact cont = (Contact)Database.query(contactSoql);

		//setup page
		PageReference pr = Page.SubmitContactUpdate;
		pr.getParameters().put('id', cont.Id);
		Test.setCurrentPage(pr);
		NMD_SubmitContactUpdateExtension extension = new NMD_SubmitContactUpdateExtension(
			new ApexPages.StandardController(cont)
		);

		//create a dummy attachment
		extension.attchbody = Blob.valueOf('filebody');
		extension.attchname = 'filename';

		extension.submit();

		//assert the email was submitted
		System.assert(extension.isSubmitted);

	}

	static testMethod void test_SubmitCustomerUpdate_NoAttachment() {

		//grab data, need to build soql string so fieldset gets added
		Id acctId = [select Id from Account where Name = :PROSPECTACCOUNT].Id;
		String contactSoql = 'select Id';
		Schema.FieldSet fieldset = Schema.SObjectType.Contact.fieldSets.getMap().get(NMD_SubmitContactUpdateExtension.QUINCY_FIELDSET);
		for (Schema.FieldSetMember field : fieldset.getFields()) {
			contactSoql += ',' + field.getFieldPath();
		}
		contactSoql += ' from Contact where AccountId = :acctId';
		Contact cont = (Contact)Database.query(contactSoql);

		//setup page
		PageReference pr = Page.SubmitContactUpdate;
		pr.getParameters().put('id', cont.Id);
		Test.setCurrentPage(pr);
		NMD_SubmitContactUpdateExtension extension = new NMD_SubmitContactUpdateExtension(
			new ApexPages.StandardController(cont)
		);

		//update a field
		cont.put((fieldset.getFields()[0]).getFieldPath(), 'update');

		extension.submit();

		//assert the correct error was thrown
		List<ApexPages.Message> msgs = ApexPages.getMessages();
		Boolean pass = false;
		for (ApexPages.Message msg : msgs) {
		    pass = pass || msg.getDetail().contains(Label.NMD_Attachment_Required);
		}
		System.assert(pass);

	}

	//static testMethod void test_SubmitCustomerUpdate_NoSend() {

	//	//grab data, need to build soql string so fieldset gets added
	//	//grab data, need to build soql string so fieldset gets added
	//	Id acctId = [select Id from Account where Name = :PROSPECTACCOUNT].Id;
	//	String contactSoql = 'select Id';
	//	Schema.FieldSet fieldset = Schema.SObjectType.Contact.fieldSets.getMap().get(NMD_SubmitContactUpdateExtension.QUINCY_FIELDSET);
	//	for (Schema.FieldSetMember field : fieldset.getFields()) {
	//		contactSoql += ',' + field.getFieldPath();
	//	}
	//	contactSoql += ' from Contact where AccountId = :acctId';
	//	Contact cont = (Contact)Database.query(contactSoql);

	//	//delete quincy contact
	//	delete new Contact(
	//		Id = QuincyReportSettings__c.getInstance().Contact_Id__c
	//	);

	//	//setup page
	//	PageReference pr = Page.SubmitContactUpdate;
	//	pr.getParameters().put('id', cont.Id);
	//	Test.setCurrentPage(pr);
	//	NMD_SubmitContactUpdateExtension extension = new NMD_SubmitContactUpdateExtension(
	//		new ApexPages.StandardController(cont)
	//	);

	//	//create a dummy attachment
	//	extension.attchbody = Blob.valueOf('filebody');
	//	extension.attchname = 'filename';

	//	extension.submit();

	//	//assert the correct error was thrown
	//	List<ApexPages.Message> msgs = ApexPages.getMessages();
	//	Boolean pass = false;
	//	for (ApexPages.Message msg : msgs) {
	//	    pass = pass || msg.getDetail().contains(Label.NMD_Please_Contact_Admin);
	//	}
	//	System.assert(pass);

	//}

}
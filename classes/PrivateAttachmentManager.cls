/**
* Manager class for Private Attachment Shared object
* @Author Accenture Services
* @Date 14SEPT2016
* Requirement Q4-2016 --> SFDC 404 & SFDC 405
*/
public class PrivateAttachmentManager{

    private Set<Id> accountIdSet = new Set<Id>();
    private list<AccountShare> accountShareLst = new list<AccountShare>();
    public UtilityICPIPrivateAttchShare utilPA = new UtilityICPIPrivateAttchShare();
    public set<Id> userGrpIds= new set<Id>();
    public set<string> PIProfiles = new set<string>();
    public set<string> ICProfiles = new set<string>();
    
    public List<String> strAccIdList = new List<String>();
    public List<Account> Acclist = new List <Account>();
    public List<Account> AcclistInsert = new List <Account>();
    public List<Account> AcclistToUpdate = new List <Account>();

    /**
    * Fetch the Account Ids for the List of Private Attachments
    * 
    * @param accountIds Private Attachments Set
    */  
   public set<Id> fecthAccountIds(Set<Id> PAIds){
        
        for(Shared_Private_Attachments__c paObj: [
                select
                    Id,
                    Account__c,
                    ICPI_Division__c
                FROM Shared_Private_Attachments__c
                WHERE Id IN: PAIds
          ]) {
              this.accountIdSet.add(paObj.Account__c);
        }
        return accountIdSet;
    
    } 

    /**
    * Fetch the Account Share Data for the List of Private Attachments
    * 
    * @param accountIds Private Attachments Set
    */  
    public void fetchAccountShareData(set<Id> AcctIdSet) {
        
        if(AcctIdSet!= null && AcctIdSet.size()>0){
            for (AccountShare idObj : [
                select 
                    AccountId,
                    RowCause,
                    UserOrGroup.name,
                    UserOrGroupId
                    FROM AccountShare 
                    WHERE AccountId in :AcctIdSet
            ]) {
                accountShareLst.add(idObj);
            }
        }
    }
    
    public void reCalculateShareMethod(list<Shared_Private_Attachments__c> PASLst){
       utilPA.reCalculateShareMethod(PASLst,accountShareLst);
    }   
     public void reCalculateShareAfterUpdate(map<Id,Shared_Private_Attachments__c> newPAMap){
        
        boolean isDeleted = false;
        if(newPAMap.keyset().size() > 0){
            isDeleted = utilPA.removePrivateAttchShares(newPAMap.keyset());
        }
        if(isDeleted == true){
         set<Id> acctIdSet = fecthAccountIds(newPAMap.keyset());
         fetchAccountShareData(acctIdSet);
            try{
                utilPA.reCalculateShareMethod(newPAMap.values(),accountShareLst);
            }catch(Exception e){
                system.debug('PrivateAttachmentManager Exception:after Update:->'+e);
            }
        }
        
    }
    
   /**** For Updating checkbox on Account*******/
   
   // For Delete
   /*
   public void fetchAccountfordeletedShareAttachement(List<Shared_Private_Attachments__c> newlst){
       for(Shared_Private_Attachments__c spa : newlst){
          strAccIdList.add(spa.Account__c); 
       }
       Acclist = [Select Id,IsDivisionalPresent__c From Account Where Id =:strAccIdList];
   }
   
   // For Insert
   public void fetchAccountforinsertedShareAttachement(List<Shared_Private_Attachments__c> newlst){
       for(Shared_Private_Attachments__c spa : newlst){
          strAccIdList.add(spa.Account__c); 
       }
       AcclistInsert = [Select Id,IsDivisionalPresent__c From Account Where Id =:strAccIdList];
   }
    
   public void updateCheckboxOnAccount(Shared_Private_Attachments__c newPAMap){
        system.debug('**Acclist**'+Acclist);
        
        // for deleted
        if(!Acclist.isEmpty()){
            for(Account ac: Acclist){
                ac.IsDivisionalPresent__c = false;
                AcclistToUpdate.add(ac);
            }
        }
        
        // for insert
        if(!AcclistInsert.isEmpty()){
            for(Account ac: AcclistInsert){
                ac.IsDivisionalPresent__c = true;
                AcclistToUpdate.add(ac);
            }
        }
        
    }
    public void updateAccount(){
        
        if(!AcclistToUpdate.isEmpty()){
           Update AcclistToUpdate;
        }
        
    } 
    */
       
}
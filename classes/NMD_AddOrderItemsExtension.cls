/**
* Extension class for the vf page AddOrderItems
*
* @Author salesforce Services
* @Date 2015-07-13
*/
public without sharing class NMD_AddOrderItemsExtension extends NMD_ExtensionBase {

    final static Integer DEFAULT_PAGESIZE = 25;
    final static String NMD = 'NMD';
    final static String ERR_FIELDREQUIRED = 'Field is Required.';
    final static String ERR_NOTWHOLENUMBER = 'Must be a whole number.';
    final static String ERR_EXCEEDSAVILABLE = 'Cannot exceed Available Quantity.';
    final static String ERR_CANNOTBEZERO = 'Must be greater than zero.';
    final static String ERR_NOSELECTIONS = 'Please select at least one row.';
    final static String ERR_ACTIVATEDORDER = 'Items cannot be added to activated orders.';

    final ApexPages.StandardController sc;
    //final Id pricebookId;
    // Product2.Id => Available Quantity
    final Set<Id> allItemIds;
    final Map<Id,Id> availProdIds;
    final Map<Id, Decimal> prodUnitPrice;

    public List<ProductItem> products { get; private set; }
    public Integer currentPage { get; private set; }
    public String searchStr { get; set; }
    public Integer maxPage { 
        get { return Math.ceil((this.allItemIds.size() - 1) / DEFAULT_PAGESIZE).intValue(); } 
    }
    public Integer totalProducts {
        get {
            return this.allItemIds != null ? this.allItemIds.size() : 0;
        }
    }

    public Boolean orderAsc { get; set; }
    public String orderBy { 
        get; 
        set {
            if (orderBy != value) {
                this.currentPage = 0;
                this.orderAsc = true;
            }
            orderBy = value;
        } 
    }

    public NMD_AddOrderItemsExtension(ApexPages.StandardController sc) {
        
        this.sc = sc;
        this.currentPage = 0;
        this.searchStr = '';
        this.prodUnitPrice = new Map<Id,Decimal>();
        this.availProdIds = new Map<Id,Id>();
        this.allItemIds = new Set<Id>();

        Order ord = (Order)sc.getRecord();

        //If order is activated prevent items from being added
        if(ord.Status == 'Activated') {
            newInfoMessage(ERR_ACTIVATEDORDER);
            return;
        }

        if (ord.RecordTypeId != OrderManager.RECTYPE_PATIENT
         && ord.RecordTypeId != OrderManager.RECTYPE_TRANSFER
         && ord.RecordTypeId != OrderManager.RECTYPE_RETURN
        ) {
            newErrorMessage('Only Patient, Return, and Transfer Order types are supported.');
            return;
        }        

        NMD_PricebookManager pbman = new NMD_PricebookManager(new Set<Id> { ord.AccountId });
        List<NMD_PricebookPair> pairs = pbman.getAccountPricebookEntries(ord.AccountId, ord.Surgery_Date__c);
        for (NMD_PricebookPair pair : pairs) {
            
            PricebookEntry pbe = pair.consolidate();

            this.prodUnitPrice.put(pbe.Product2Id, pbe.UnitPrice);
            this.availProdIds.put(pbe.Product2Id, pbe.Id);

        }

        Set<Id> prodIds = this.availProdIds.keySet().clone();
        Id acctId = ord.Shipping_Location__r.Inventory_Data_ID__r.Account__c;

        // retrieve set of all relevant products and their current available quantity
        for (Inventory_Item__c item : Database.query(
            'select ' +
                'Model_Number__c ' +
            'from Inventory_Item__c ' +
            'where Inventory_Location__r.Inventory_Data_ID__r.Account__c = :acctId ' +
            'and Model_Number__c in :prodIds ' +
            'and Available_Quantity__c > 0 ' +
            (ord.RecordTypeId == OrderManager.RECTYPE_TRANSFER
                ? 'and Is_QN__c = false and Is_Expired__c = false '
                : ''
            )
        )) {
            this.allItemIds.add(item.Id);
            prodIds.remove(item.Model_Number__c);
        }

        // clear out irrelevant pricebook entries
        for (Id prodId : prodIds) {
            this.availProdIds.remove(prodId);
        }

        this.orderBy = 'Model_Number__r.Name';
        this.orderAsc = true;

        updateProductList();

    }

    /**
    *  @desc    Saves the selected items and returns user back parent order detail
    */
    public PageReference saveSelected() {

        PageReference pr;

        if (createOrderItems()) {
            pr = this.sc.view();
        }

        return pr;

    }

    /**
    *  @desc    Saves the selected items and resets the page to add more
    */
    public PageReference saveSelectedMore() {

        PageReference pr;

        if (createOrderItems()) {
            pr = Page.AddOrderItems;
            pr.getParameters().put('id', this.sc.getId());
            pr.setRedirect(true);
        }

        return pr;

    }

    /**
    *  @desc    set to first page in results
    */
    public void firstPage() {
        this.currentPage = 0;
        updateProductList();
    }

    /**
    *  @desc    set to previous page in results
    */
    public void prevPage() {
        this.currentPage = this.currentPage == 0 ? 0 : this.currentPage - 1;
        updateProductList();
    }

    /**
    *  @desc    set to next page in results
    */
    public void nextPage() {
        this.currentPage = this.currentPage == this.maxPage ? this.currentPage : this.currentPage + 1;
        updateProductList();
    }

    /**
    *  @desc    set to last page in results
    */
    public void lastPage() {
        this.currentPage = this.maxPage;
        updateProductList();
    }

    /**
    *  @desc    retrieves the existing items
    */
    public void updateProductList() {
        
        this.products = new List<ProductItem>{};

        Integer offset = this.currentPage * DEFAULT_PAGESIZE;
        String fuzzedSearchStr = '';
        String soql =
            'select ' +
                'Inventory_Location__c, ' +
                'Available_Quantity__c, ' +
                'Lot_Number__c, ' +
                'Material_Number_UPN__c, ' +
                'Model_Number__c, ' +
                'Model_Number__r.Model_Number__c, ' +
                'Model_Number__r.Name, ' +
                'Model_Number__r.Family, ' +
                'Serial_Number__c ' +
            'from Inventory_Item__c ' +
            'where Id in :allItemIds ';

        if (String.isNotBlank(this.searchStr)) {

            fuzzedSearchStr = '%' + this.searchStr.replaceAll('\\*', '%') + '%';
            soql += 
                'and (' +
                    'Lot_Number__c like :fuzzedSearchStr or ' +
                    'Model_Number__r.Name like :fuzzedSearchStr or ' +
                    'Serial_Number__c like :fuzzedSearchStr' +
                ') ';
        }

        soql +=
            'order by ' + this.orderBy + (this.orderAsc ? ' asc ' : ' desc ') +
            'limit :DEFAULT_PAGESIZE ' +
            'offset :offset ';

        Id orderRecTypeId = ((Order)sc.getRecord()).RecordTypeId;
        for (Inventory_Item__c item : Database.query(soql)) {
            Decimal price;
            if(this.prodUnitPrice.containsKey(item.Model_Number__c)){
                price = this.prodUnitPrice.get(item.Model_Number__c);
            }

            this.products.add(new ProductItem(
                new Product2(
                    Name = item.Model_Number__r.Name,
                    Family = item.Model_Number__r.Family
                ), 
                new OrderItem(
                    OrderId = this.sc.getId(),
                    UnitPrice = price,
                    Lot_Number__c = item.Lot_Number__c,
                    Material_Number_UPN__c = item.Material_Number_UPN__c,
                    Model_Number__c = item.Model_Number__r.Model_Number__c,
                    No_Charge_Reason__c = '',
                    PricebookEntryId = this.availProdIds.get(item.Model_Number__c),
                    Quantity = null,
                    Serial_Number__c = item.Serial_Number__c,
                    Inventory_Source__c = item.Id,
                    Inventory_Location__c = item.Inventory_Location__c,
                    Item_Status__c = ''
                ),
                item.Available_Quantity__c != null ? item.Available_Quantity__c.intValue() : 0,
                price
            ));
        }

    }

    /**
    *  @desc    validate and create the new OrderItems
    */
    private Boolean createOrderItems() {
        
        List<OrderItem> newItems = new List<OrderItem>{};
        Integer selectedCount = 0;
        Id orderRecTypeId = ((Order)this.sc.getRecord()).RecordTypeId;
        for (ProductItem item : this.products) {
            if (item.selected) {

                selectedCount++;
                Boolean success = true;

                if (item.item.UnitPrice == null) {
                    item.item.UnitPrice = 0.0;
                }

                if (item.item.Quantity == null) {
                    item.item.Quantity.addError(ERR_FIELDREQUIRED);
                    success = false;
                }
                else if (item.item.Quantity != item.item.Quantity.intValue()) {
                    item.item.Quantity.addError(ERR_NOTWHOLENUMBER);
                    success = false;    
                }
                else if (item.item.Quantity > item.availQty) {
                    item.item.Quantity.addError(ERR_EXCEEDSAVILABLE);
                    success = false;
                }
                else if (item.item.Quantity < 1) {
                    item.item.Quantity.addError(ERR_CANNOTBEZERO);
                    success = false;
                }
                
                if (orderRecTypeId == OrderManager.RECTYPE_PATIENT) {
                    
                    if (String.isBlank(item.item.Item_Status__c)) {
                        item.item.Item_Status__c.addError(ERR_FIELDREQUIRED);
                        success = false;
                    }

                    if (String.isBlank(item.item.No_Charge_Reason__c) 
                        && item.item.UnitPrice == 0.0
                        && item.defaultPrice != 0.0
                    ) {
                        item.item.No_Charge_Reason__c.addError(ERR_FIELDREQUIRED);
                        success = false;
                    }

                }


                if (success) {
                    newItems.add(item.item);
                }

            }
        }

        if (selectedCount == 0) {
            newErrorMessage(ERR_NOSELECTIONS);
            return false;
        }
        else if (selectedCount != newItems.size()) {
            return false;
        }

        try {
            //insert newitems;
            DML.save(this, newItems);
        }
        catch (System.DmlException de) {
            
            for (Integer i = 0; i < de.getNumDml(); i++) {
                newItems.get(de.getDmlIndex(i)).addError(de.getDmlMessage(i));
            }

            return false;
        }

        return true;

    }

    public class ProductItem {

        public Product2 prod { get; private set; }
        public OrderItem item { get; private set; }
        public Integer availQty { get; private set; }
        public Boolean selected { get; set; }
        public Decimal defaultPrice {get; private set; }

        public ProductItem(Product2 prod, OrderItem item, Integer availQty, Decimal defPrice) {
            this.selected = false;
            this.prod = prod;
            this.item = item;
            this.availQty = availQty;
            this.defaultPrice = defPrice;
        }

    }

}
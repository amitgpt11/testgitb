/**
* Manager class for the Revenue Details Sobject
*
* @Author Vikas Malik
* @Date 2016-06-08 
*/
public with sharing class RevenueDetailsManager {
    
    // Singleton pattern
    private static RevenueDetailsManager instance;
    
    public static RevenueDetailsManager getInstance() {
        if (instance == null) {
            instance = new RevenueDetailsManager();
        }
        return instance;
    }
    
    private Set<Id> oppIds = new Set<Id>();
    private Map<Id,List<Schedule__c>> schedulesMap = new Map<Id,List<Schedule__c>>();
    private List<Schedule__c> revSchedules = new List<Schedule__c>();
    private List<Schedule__c> delSchedules = new List<Schedule__c>();
    private Map<Id,Double> oppAmtMap = new Map<Id,Double>();
    private List<Opportunity> updOppList =  new List<Opportunity>();
    private Map<Id,List<OpportunityLineItem>> oppProductMap = new Map<Id,List<OpportunityLineItem>>();
    private List<OpportunityLineItem> updOppLineItemList =  new List<OpportunityLineItem>();
    
    public static final String SCHD_MONTHLY = 'Monthly';
    public static final String SCHD_QUARTERLY = 'Quarterly';
    public static final String SCHD_YEARLY = 'Yearly';
    public static Boolean isRevDtlMgr = false;
    
    /**
	* This method fetches the Opportunity Details for the Revenue Detail object
	*
	*/
    public void fetchDetailsPerOpportunity(List<Opportunity_Revenue_Schedule__c> revDetails){
        Set<Id> currentOppIds = new Set<Id>();
        for (Opportunity_Revenue_Schedule__c revDtl: revDetails){
            currentOppIds.add(revDtl.Opportunity__c);
        }
        
        for(Opportunity_Revenue_Schedule__c revDtl :[SELECT Id,Opportunity__c FROM Opportunity_Revenue_Schedule__c WHERE Opportunity__c IN :currentOppIds]){
            this.oppIds.add(revDtl.Opportunity__c);
        }
        
        for(Opportunity opp :[SELECT Id,Amount FROM Opportunity WHERE Id IN :currentOppIds]){
            this.oppAmtMap.put(opp.Id,opp.Amount);
        }
        
        for(OpportunityLineItem oli :[SELECT Id,UnitPrice,Quantity,TotalPrice,OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN :currentOppIds]){
            if (null != this.oppProductMap.get(oli.OpportunityId)){
            	this.oppProductMap.get(oli.OpportunityId).add(oli);
            }else{
            	this.oppProductMap.put(oli.OpportunityId,new List<OpportunityLineItem>{oli});
            }
        }
    }
    
    
    /**
	* This method validates that only one Revenue Detail can be inserted per opportunity
	*
	*/
    public void validateInsert(Opportunity_Revenue_Schedule__c newRevDetail){
        if (!this.oppIds.isEmpty() && this.oppIds.contains(newRevDetail.Opportunity__c)){
            newRevDetail.addError(Label.One_Revenue_Detail_Per_Opportunity);
        }
    }
    
    
    /**
	* This method fetched Schedule Details for Revenue Detail  record
	*
	*/
    public void fetchScheduleDetails(List<Opportunity_Revenue_Schedule__c> revDetails){
        
        for(Schedule__c recSchdl :[SELECT Id,Month__c,Revenue_Schedule__c,Revenue__c FROM Schedule__c WHERE Revenue_Schedule__c IN :revDetails]){
            if (this.schedulesMap.get(recSchdl.Revenue_Schedule__c) == null)
                this.schedulesMap.put(recSchdl.Revenue_Schedule__c,new List<Schedule__c>{recSchdl});
            else
                this.schedulesMap.get(recSchdl.Revenue_Schedule__c).add(recSchdl);
        }
        
    }
    
    
    /**
	* This method creates the Schedule records based on the latest Revenue Detail record value
	*
	*/
    public void processScheduleDetails(Opportunity_Revenue_Schedule__c newrevDetails,Opportunity_Revenue_Schedule__c oldrevDetails){

        if (oldrevDetails == null || isScheduleChange(newrevDetails,oldrevDetails)){
        	isRevDtlMgr = true; 
            if (newrevDetails.Shared_Opportunity_Revenue__c != null && newrevDetails.Shared_Opportunity_Revenue__c != 0){
            	Integer schRecCount = null != newrevDetails.Shared_Number_of_Installments__c && newrevDetails.Shared_Number_of_Installments__c > 0 ? Integer.valueOf(newrevDetails.Shared_Number_of_Installments__c) : Integer.valueOf(newrevDetails.Remaining_Months__c);
        		Double schdlAmt = newrevDetails.Shared_Opportunity_Revenue__c / schRecCount;
        		Date currMonth = newrevDetails.Start_Date__c.addMonths(-1);
	            for (Integer i = 1; i< = schRecCount; i++){
	                currMonth = getScheduleDate(newrevDetails.Installment_Period__c,currMonth);
	                this.revSchedules.add(insertScheduleObject(schdlAmt,currMonth,newrevDetails.Id,newrevDetails.Opportunity__c));
	            }
            }
        }
    }
    
    
    /**
	* This method updates the opportunity amout with the Revenue Detail revenue amount field in case they do not match
	*
	*/
    public void updateOpportunityAmount(Opportunity_Revenue_Schedule__c newrevDetails,Opportunity_Revenue_Schedule__c oldrevDetails){
    	if (oldrevDetails == null || oldrevDetails.Shared_Opportunity_Revenue__c != newrevDetails.Shared_Opportunity_Revenue__c){
		    if (this.oppAmtMap.get(newrevDetails.Opportunity__c) != newrevDetails.Shared_Opportunity_Revenue__c){
			// Incase there are no Opportunity Products available then we can update the Amount directly on Opportunity Record
				if (this.oppProductMap.get(newrevDetails.Opportunity__c) == null){
        			this.updOppList.add(new Opportunity(Id = newrevDetails.Opportunity__c,	Amount = newrevDetails.Shared_Opportunity_Revenue__c));
				}else{
					Double amtPerOli = newrevDetails.Shared_Opportunity_Revenue__c / this.oppProductMap.get(newrevDetails.Opportunity__c).size();
					for (OpportunityLineItem oli : this.oppProductMap.get(newrevDetails.Opportunity__c)){
						Double oliAmt = amtPerOli/oli.Quantity;
						oli.UnitPrice = oliAmt;
						this.updOppLineItemList.add(oli);
					}
				}
        	}
    	}
    }
    
    
    
    /**
	* This method compares the old and new revenue detail record and return true if Date, Amount or Installment Period field changes
	*
	*/
    private Boolean isScheduleChange(Opportunity_Revenue_Schedule__c newrevDetails,Opportunity_Revenue_Schedule__c oldrevDetails){
        if (oldrevDetails.Start_Date__c != newrevDetails.Start_Date__c || oldrevDetails.Shared_Opportunity_Revenue__c != newrevDetails.Shared_Opportunity_Revenue__c || !oldrevDetails.Installment_Period__c.equals(newrevDetails.Installment_Period__c) || oldrevDetails.Shared_Number_of_Installments__c != newrevDetails.Shared_Number_of_Installments__c){
            if (null != this.schedulesMap.get(newrevDetails.Id) && !this.schedulesMap.get(newrevDetails.Id).isEmpty()){
                this.delSchedules.addAll(this.schedulesMap.get(newrevDetails.Id));
            }
            
            return true;
        }
        return false;
    }
    
    
    /**
	* This method returns the Schedule Date based on the current Scheule date and Revenue Details Installment Period field
	*
	*/
    private Date getScheduleDate(String schdPeriod, Date currDate){
        Date returnDate = currDate.addMonths(1);
        if (SCHD_MONTHLY.equals(schdPeriod)){
            Integer lastDay = Date.daysInMonth(returnDate.year(),returnDate.month());
            returnDate = Date.newInstance(returnDate.year(),returnDate.month(), lastDay);
        }else if (SCHD_QUARTERLY.equals(schdPeriod)){
            Integer currentMnt =returnDate.month();
            Integer currentQ =((currentMnt-1)/3) + 1;
            returnDate = date.newInstance(returnDate.year(),currentMnt + (4 - (currentMnt - ((currentQ -1)*3))) , 1).addDays(-1);
        }else if (SCHD_YEARLY.equals(schdPeriod)){
            returnDate = date.newInstance(returnDate.year(),12 , 31);
        }
        
        return returnDate;
    }
    
    
    /**
	* This method returns the Schedule record which has to be inserted
	*
	*/
    private Schedule__c insertScheduleObject(Double schdlAmt, Date schdlMonth,String revenueDetailId, String opportunityId){
        Schedule__c schobj = new Schedule__c();
        schobj.Month__c = schdlMonth;
        schobj.Revenue__c = schdlAmt;
        schobj.Revenue_Schedule__c = revenueDetailId;
        schobj.Opportunity__c = opportunityId;
        
        return schobj;
    }
    
    
    /**
	* This method deletes the old schedule records if the Revenue Detail object is changed
	*
	*/
    public void deleteSchedules(){
        if (this.delSchedules.isEmpty()){
            return;
        }
        
        try {
            DML.evaluateResults(this, Database.delete(this.delSchedules, false));
        }
        catch (System.DmlException de) {
            
        }
    }
    
    
    /**
	* This method inserts the new schedule records
	*
	*/
    public void commitSchedules(){
        if (this.revSchedules.isEmpty()){
            return;
        }
        
        try {
            DML.evaluateResults(this, Database.upsert(this.revSchedules, false));
        }
        catch (System.DmlException de) {
            
        }
    }
    
    
    /**
	* This method updates the opportunity records
	*
	*/
    public void commitOpportunities(){
        if (this.updOppList.isEmpty()){
            return;
        }
        
        try {
            DML.save(this, this.updOppList);
        }
        catch (System.DmlException de) {
            
        }
    }
    
    
    /**
	* This method updates the opportunity records
	*
	*/
    public void commitOpportunityLineItems(){
        if (this.updOppLineItemList.isEmpty()){
            return;
        }
        
        try {
            DML.save(this, this.updOppLineItemList);
        }
        catch (System.DmlException de) {
            
        }
    }
    
}
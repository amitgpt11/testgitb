/**
* Class Name : ContactListController
* Serves as controller for ContactList Page
* Provides contacts for Users division
*
* Only meant to be used for page where Standard Controller is Account
*
* Created : 17-3-2015
*
**/
public with sharing class ContactListController{

    /*
    * List of contacts for this account, of the same division as logged in User's
    */
    public List<Contact> contacts { get; private set; }
    
    /*
    * Constructor
    * Initializes the contact list
    */
    public ContactListController(ApexPages.StandardController sc) {
    
        this.contacts = new List<Contact>();

        if (sc.getId() != null) {
            
            String userDivision = [
                SELECT Division
                FROM User
                WHERE Id = :UserInfo.getUserId()
            ].Division;
            
            this.contacts = [
                SELECT 
                    Division__c, 
                    Email, 
                    Name,
                    Phone,
                    Title 
                FROM Contact
                WHERE AccountId = :sc.getId()
                AND Division__c = :userDivision
            ];
        }
        
    }

}
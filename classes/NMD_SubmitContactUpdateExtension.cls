/**
* Conroller class for the SubmitContactUpdate.page
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class NMD_SubmitContactUpdateExtension extends NMD_QuincyExtensionBase {

	@testVisible
	final static String QUINCY_FIELDSET = 'Quincy_Field_Updates';
	final Contact originalContact;

	public transient Blob attchbody { get; set; }
	public String attchname { get; set; }

	public NMD_SubmitContactUpdateExtension(ApexPages.StandardController sc) {
		super(sc);

		this.originalContact = (Contact)sc.getRecord().clone(true, true);
		this.attchbody = Blob.valueOf('');
		this.attchname = '';

		//retrieve available accounts
        for (AccContactRelationship__c junction : [
            select
            	Facility__c,
                Facility__r.Name
            from AccContactRelationship__c
            where Contact__c = :sc.getId()
            and Facility__c != null
        ]) {
            children.add(new NMD_ObjectWrapper(new Account(
                Id = junction.Facility__c, 
                Name = junction.Facility__r.Name
            )));
        }

        newInfoMessage(Label.NMD_Enter_Updated_Information);

	}

	public override void submit() {

		//collect overwrite fields
		Map<String,String> reportParams = getFieldValueDeltas(
			Schema.SObjectType.Contact.fieldSets.getMap().get(QUINCY_FIELDSET),
			this.originalContact,
			this.sc.getRecord()
		);

		if (!reportParams.isEmpty() && String.isBlank(this.attchname)) {
			//throw error if there's an update to the account but no upload
			newErrorMessage(Label.NMD_Attachment_Required);
		}
		else {

			NMD_SendQuincyEmailManager emailManager = new NMD_SendQuincyEmailManager(this.sc.getId(), getSelectedIds());
			emailManager.reportParameters.putAll(reportParams);
			
			//add attachment if present
			if (String.isNotBlank(this.attchname)) {
				emailManager.attachments.add(new Attachment(
					Name = this.attchname,
					Body = this.attchbody
				));
			}

            Boolean sentOK = emailManager.sendQuincyEmail();
            if (sentOK) {
                newInfoMessage(Label.NMD_Report_Emailed);
                isSubmitted = true;
            }
            else {
            	newErrorMessage(emailManager.lastErrorMessage + ' ' + Label.NMD_Please_Contact_Admin);
            	logError('NMD_SubmitCustomerUpdateExtension.submit', emailManager.lastErrorMessage);
            }
        }

	}

}
@IsTest(SeeAllData=false)
private class NMD_ConsignmentAccountDispatcherTest {
	
	final static NMD_TestDataManager td = new NMD_TestDataManager();
	final static RestDispatcherV1 dispatcher = new RestDispatcherV1(null);
	final static String ACCTNAME = 'ACCTNAME';

	static {
		RestContext.response = new RestResponse();
	}

	@TestSetup
	static void setup() {
		td.setupNuromodUserRoleSettings();
	}

	static testMethod void test_GetUriMapping() {

		NMD_ConsignmentAccountDispatcher agent = new NMD_ConsignmentAccountDispatcher();
		String uriMapping = agent.getURIMapping();

	}

	static testMethod void test_ConsignmentAccountDispatcher_NoSearch() {

		Map<String,String> parameters = new Map<String,String>();
		NMD_ConsignmentAccountDispatcher agent = new NMD_ConsignmentAccountDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_ConsignmentAccountDispatcher_Search() {

		Map<String,String> parameters = new Map<String,String> {
			NMD_ConsignmentAccountDispatcher.PARAM_QUERY => ACCTNAME
		};
		NMD_ConsignmentAccountDispatcher agent = new NMD_ConsignmentAccountDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

}
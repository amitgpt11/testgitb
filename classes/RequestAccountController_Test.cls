@isTest
private class RequestAccountController_Test {
        private static testMethod void test(){


            User objUser = Test_DataCreator.createCommunityUser();
            User guest = Test_DataCreator.createGuestUser();

            Account objAcc = Test_DataCreator.createAccount('Test Account');

           Contact objContact = Test_DataCreator.createContact('Test', objAcc.Id);
        objContact.Email = 'test@test.com';
        update objContact;

        Test_DataCreator.createCountriesCustomSetting('Test');
        Test_DataCreator.createLanguageCustomSetting('Test');
        Test_DataCreator.cerateBSC_VTM_AlignmentTestData();

        Test.startTest();

        System.RunAs(guest){
            
            PageReference pageRef = Page.RequestAccount;
            Test.setCurrentPage(pageRef);

            RequestAccountController objRequestAccountController = new RequestAccountController();

            objRequestAccountController.init();
            objRequestAccountController.firstName = 'test';
            objRequestAccountController.lastName = 'test';
            objRequestAccountController.contactEmail = 'test@test.com';
            objRequestAccountController.contactCompany = 'test';
            objRequestAccountController.contactCity = 'test';
            objRequestAccountController.contactCountry = 'test';
            objRequestAccountController.contactDivision = 'Endo';
            objRequestAccountController.taskSubject = 'test';
            objRequestAccountController.messageDescription = 'test';

            List<SelectOption> selDivs = objRequestAccountController.getDivisions();
            List<SelectOption> selOpts = objRequestAccountController.getcountries();
            List<SelectOption> selLanOpts = objRequestAccountController.getLanguages();
            List<SelectOption> selFunOpts = objRequestAccountController.functions;

            System.assertEquals(objRequestAccountController.contactEmail, 'test@test.com');
            System.assertEquals(objRequestAccountController.fetchLanguageAndCountryBasedOnSpeciality(), null);
            }
            Test.stopTest();    
        }
private static testMethod void test_Two() {
    
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        
        //Create custom Setting 
        BSC_VTM_Alignment__c objBSCVTMAlignment = Test_DataCreator.cerateBSC_VTM_AlignmentTestData();
        
        
        Test.StartTest();
        
        system.runAs(objUser){
            
            PageReference pageRef = Page.RequestAccount;
            Test.setCurrentPage(pageRef);
            
             RequestAccountController objRequestAccountController = new RequestAccountController();
            
            objRequestAccountController.firstName = 'test';
            objRequestAccountController.lastName = 'test';
            objRequestAccountController.contactEmail = 'test@test.com';
            objRequestAccountController.contactCompany = 'test';
            objRequestAccountController.contactCity = 'test';
            objRequestAccountController.contactCountry = 'test';
            objRequestAccountController.contactDivision = 'Endo';
            objRequestAccountController.taskSubject = 'test';
            objRequestAccountController.messageDescription = 'test';
            
            objRequestAccountController.contactVTMForAccess();
            
            objBSCVTMAlignment.Shared_Division__c = 'Cardio';
            update objBSCVTMAlignment;
            
            objRequestAccountController.firstName = 'test';
            objRequestAccountController.lastName = 'test';
            objRequestAccountController.contactEmail = 'test@test.com';
            objRequestAccountController.contactCompany = 'test';
            objRequestAccountController.contactCity = 'test';
            objRequestAccountController.contactCountry = 'test';
            objRequestAccountController.contactDivision = 'Cardiology';
            objRequestAccountController.taskSubject = 'test';
            objRequestAccountController.messageDescription = 'test';
            
            objRequestAccountController.contactVTMForAccess();
            
            system.assertEquals(objRequestAccountController.init(),null);
        }
        Test.StopTest();
        
        
    }
}
/*****  
        Class:  SampleProductRequestFormExtension  (Controller for Page - SampleRequestForm)
        @Author: Accenture Team
        @Requirement Id: PISFDC 12
        Description: This page is used to create Sample Product Request form.
****/
public class SampleProductRequestFormExtension {
    public String loggedInUserName{get;set;}
      
    public String SelectedItem3{get;set;}
    public String SelectedItem5{get;set;}
    public String SelectedItem6{get;set;}
    
    public String shippingAddress{get;set;}
    public String facilityID{get;set;}
    public Shared_Request_Form__c requestForm{get;set;}
    public List<Shared_Request_Form_Line_Item__c> requestFormItemList{get;set;}
    public List<Product2> products_LEVEL3{get;set;}
    public List<Product2> products_LEVEL4{get;set;}
    public List<Product2> products_LEVEL5{get;set;}
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Product2.getRecordTypeInfosByName();
    public static final Id RECTYPE_LEVEL3 = RECTYPES_OPPTY.get('Standard Product Hierarchy - Level 3').getRecordTypeId();
    public static final Id RECTYPE_LEVEL5 = RECTYPES_OPPTY.get('Standard Product Hierarchy - Level 5').getRecordTypeId();
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_RequestForm = Schema.SObjectType.Shared_Request_Form__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_Sample_Request = RECTYPES_RequestForm.get('PI Sample Request Form').getRecordTypeId();
    public User userRecord;
    Set<String> productLevel3Set = new Set<String>();
    List<String> productLevel3List = new List<String>();
    
    
    public transient list<Product2> allPI3ProductsLst{get;set;}
    public list<Product2> allPI5ProductsLst{get;set;}
    public list<Product2> allPI6ProductsLst{get;set;}
    map<Id,Product2> allCPG6productMap = new map<Id,Product2>();
    
    public string message{get;set;}
    public string errORsuccessMsg{get;set;}
    public set<Id> tempPrdIds{get;set;} 
    public Boolean desktopRedirect {get;set;}
    public String redirectUrl {get;set;}
    
    
    
    public Account account {get;set;} // new account to create
  public List<Account> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
    
    public SampleProductRequestFormExtension(){
        this.desktopRedirect = false;
        this.redirectUrl = '';
        this.requestForm = new Shared_Request_Form__c();
        requestFormItemList = new list<Shared_Request_Form_Line_Item__c>();
        tempPrdIds = new set<Id>();
        getAllPI3Products();
        
        if(allPI3ProductsLst != null && allPI3ProductsLst.size() > 0){
            for(Product2 prd : allPI3ProductsLst){
                if(prd.RecordTypeId == RECTYPE_LEVEL3 && prd.Shared_Custom_Product_Group_Level_3__c != ''){
                    productLevel3Set.add(prd.Shared_Custom_Product_Group_Level_3__c);
                }
            }
            
            productLevel3List.addAll(productLevel3Set); 
            productLevel3List.sort();
        }     
        
        
        loggedInUserName = UserInfo.getName();
        userRecord = [Select ID,Name,Cost_Center_Code__C from User Where Id =:UserInfo.getUserId() limit 1];
        loggedInUserName = UserInfo.getName();
        requestForm.Cost_Center__c = userRecord.Cost_Center_Code__C;
        
        
      
        
    }
    
    
   
    
     public PageReference ValidateValues(){
        facilityID = requestForm.Facility__c;
        system.debug(facilityID+'--facilityID');
        Account a;
        if(string.isNotBlank(facilityID) && string.IsNotEmpty(facilityID)){
            a = [SELECT Id,Name,Account_Number__c,ShippingCity,ShippingPostalCode,ShippingState,ShippingStreet,Sold_to_Attn__c,Sold_to_City__c,Sold_to_Country__c,Sold_to_Fax__c,Sold_to_Phone__c,Sold_to_State__c,Sold_to_Street__c,Sold_to_Zip_Postal_Code__c FROM Account where id =:requestForm.Facility__c ];
        }
        if(a!= null){
                requestForm.Shipping_Address__c = (a.ShippingStreet == null ? a.Sold_to_Street__c: a.ShippingStreet);
                requestForm.City__c = (a.ShippingCity == null ? a.Sold_to_City__c: a.ShippingCity);
                requestForm.State__c = (a.ShippingState == null ? a.Sold_to_State__c : a.ShippingState);
                requestForm.Zip_Code__c = (a.ShippingPostalCode == null ? a.Sold_to_Zip_Postal_Code__c : a.ShippingPostalCode);
                requestForm.SAP_Account_Number__c = a.Account_Number__c;
       }
       return null;
    }
     
     public List<SelectOption> getLEVEL3Products(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for(String prd : productLevel3List){
            options.add(new SelectOption(prd, prd));
        }
        system.debug('getLEVEL3Products--'+options);
        return options;
    }//End of Method
    
    public List<SelectOption> getLEVEL5Products(){
        Set<String> productLevel5Set = new Set<String>();
        list<String> productLevel5List = new list<String>();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        
        if(allPI5ProductsLst != null && allPI5ProductsLst.size() > 0){
            for(Product2 prd : allPI5ProductsLst){                  
                    productLevel5Set.add(prd.Shared_Custom_Product_Group_Level_5__c);                   
            }
            
            productLevel5List.addAll(productLevel5Set); 
            productLevel5List.sort();
            for(String prd : productLevel5List){
             options.add(new SelectOption(prd, prd));
            }
        }  
        system.debug('getLEVEL5Products--'+options);
        return options;
    }//End of Method
    
    public List<SelectOption> getCPG6Products(){
        Set<String> productLevel6Set = new Set<String>();
        list<String> productLevel6List = new list<String>();
        List<SelectOption> options = new List<SelectOption>();
        map<Id,string> prdIdNameMap = new map<Id,string>();
        options.add(new SelectOption('', '--None--'));
        if(allPI6ProductsLst != null && allPI6ProductsLst.size() > 0){
            for(Product2 prd : allPI6ProductsLst){   
                    prdIdNameMap.put(prd.Id,prd.UPN_Material_Number__c+'-'+prd.Description);              
                    //productLevel6Set.add(prd.Name+'-'+prd.Description);                   
            }           
            //productLevel6List.addAll(prdIdNameMap.values()); 
            //productLevel6List.sort();
            for(Id prd : prdIdNameMap.keyset()){
                options.add(new SelectOption(prd,prdIdNameMap.get(prd)));
            }
        }  
        system.debug('getCPG6Products--'+options);
        
        return options;
    }//End of Method
    
    public void getAllPI3Products(){
        allPI3ProductsLst = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,Cost__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_3__c,Shared_Custom_Product_Group_Level_6__c,Description  
                                FROM Product2 
                                WHERE Division__c='PI' AND RecordTypeId =:RECTYPE_LEVEL3 AND Shared_Custom_Product_Group_Level_3__c != '' AND Shared_Geography__c ='US'];
        system.debug(allPI3ProductsLst+'@@--this.allPI3ProductsLst');
    }
     
    public pagereference getAllPI5Products(){
        system.debug('selectedItem3-->'+selectedItem3);
        
        if(string.isNotEmpty(selectedItem3)){
            allPI5ProductsLst = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,Cost__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_3__c,Shared_Custom_Product_Group_Level_5__c,Description  
                                                FROM Product2 
                                                WHERE Division__c='PI' AND RecordTypeId =:RECTYPE_LEVEL5 AND Shared_Custom_Product_Group_Level_3__c =: SelectedItem3 AND Shared_Custom_Product_Group_Level_5__c != NULL AND Shared_Geography__c ='US'];
            system.debug(allPI5ProductsLst+'@@--this.allPI5ProductsLst');
            
        }
    return null;
    }
    
    public pagereference getAllCPG6Products(){
        system.debug('selectedItem5-->'+selectedItem5);
        
        if(string.isNotEmpty(selectedItem5)){
            allPI6ProductsLst = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,Cost__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_3__c,Shared_Custom_Product_Group_Level_6__c,Description  
                                                            FROM Product2 
                                                            WHERE Division__c='PI'  AND Shared_Custom_Product_Group_Level_6__c =: SelectedItem5  AND Shared_Geography__c ='US' order by Name];
            system.debug(allPI6ProductsLst+'@@--this.allPI6ProductsLst');
            for(Product2 p : allPI6ProductsLst){
                allCPG6productMap.put(p.Id,p);
            }
            
        }
        
    return null;
    }
    public void selectedItem6(){
        system.debug(selectedItem6+'-->selectedItem6');
    }
    public PageReference addProductToLst(){
        //RequestFrmUtemLst.clear();
        system.debug(selectedItem6+'-->selectedItem6');        
        
       // system.debug(tempPrdIds.contains(selectedItem6)+'-->tempPrdIds');
         message = '';
         if(string.isNotEmpty(selectedItem6)){
             if(tempPrdIds.contains(selectedItem6) == false){
                message = '';
                tempPrdIds.add(selectedItem6);
                system.debug(tempPrdIds+'-->tempPrdIds');
                Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
                rItem.Full_UPN__c = allCPG6productMap.get(selectedItem6).UPN_Material_Number__c;
                rItem.Description__c = allCPG6productMap.get(selectedItem6).Description;
                system.debug('@@@@-->'+allCPG6productMap.get(selectedItem6).cost__c);
                if(allCPG6productMap.get(selectedItem6).cost__c != null){
                    rItem.PI_Per_Price_Of_Line_Item__c = allCPG6productMap.get(selectedItem6).cost__c;
                }
                rItem.Quantity__c = 1;
                rItem.Unit_Of__c = 'EA';
                requestFormItemList.add(rItem);            
             }
             else{
                message = 'This Product has already been added to the list!!';
             }
         }
         else{
            message = 'Select at least one UPN to add to the list';             
         }
         system.debug(message+'-->message');  
        return null;
    } 
    public pageReference callSave(){
        list<Shared_Request_Form_Line_Item__c> newLineItemLst = new list<Shared_Request_Form_Line_Item__c>();
        //String fromdessktop = ApexPages.currentPage().getParameters().get('continue');
        decimal totalPriceOfAllLineItems;
          
        errORsuccessMsg ='';
        if(requestFormItemList.size() > 0){
            requestForm = this.requestForm;
            requestForm.Facility__C = facilityID;
            requestForm.RecordTypeId = RECTYPE_Sample_Request; 
            requestForm.Requesters_Name__c = UserInfo.getUserId();
            //requestForm.PI_Approval_Status__c = 'Submitted';  // The status becomes Submitted via approval process when the record is submitted to Manager for approval
            try{
                insert requestForm;
            }catch(Exception ex){
                errORsuccessMsg = 'Something went wrong, please contact your administartor.';
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,ex.getMessage());
                ApexPages.addMessage(myMsg);
                System.debug('Error '+ex.getMessage());
                return null;
            }
            
            if(requestForm.Id != null){
                system.debug(requestFormItemList+'<---@@');
                for(Shared_Request_Form_Line_Item__c r :requestFormItemList){
                    r.Request_Form__c = requestForm.Id;
                    system.debug('r.Quantity__c'+r.Quantity__c);
                    if(r.Quantity__c > 0){
                        newLineItemLst.add(r);
                        //if(r.PI_Per_Price_Of_Line_Item__c != null){
                        //totalPriceOfAllLineItems += r.Quantity__c*r.PI_Per_Price_Of_Line_Item__c;
                        //}
                    }
                }
            }
            system.debug('request form inserted');
            if(newLineItemLst.size()>0){
                try{
                    insert newLineItemLst;
                }catch(Exception e){
                    errORsuccessMsg = 'Something went wrong, please contact your administartor.';
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
                    ApexPages.addMessage(myMsg);
                    system.debug('Exception while inserting line items-'+e);
                    return null;
                }
            }
            
            if(errORsuccessMsg == ''){
               return new PageReference('/'+this.requestForm.Id);  
            }else{
                
                return null;
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning,'Add at least one prodcut before submitting.');
            ApexPages.addMessage(myMsg);
            //errORsuccessMsg = 'Add at least one prodcut before submitting.';
            system.debug('errORsuccessMsgs-'+errORsuccessMsg);
            return null;
        }
        
    }
    
    public PageReference changeCostCenter(){
       if(requestForm.Requesters_Name__c!=null){
        userRecord = [Select ID,Name,Cost_Center_Code__C from User Where Id =:requestForm.Requesters_Name__c limit 1];
        requestForm.Cost_Center__c = userRecord.Cost_Center_Code__C;        
        } 
        return null;
        }
   
  
    
    
    
}
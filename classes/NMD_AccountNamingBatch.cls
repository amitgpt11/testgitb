/**
* Batch class to format the naming of existing NMD Account records.  This batch will attempt
* to update the records but will fail silently.  if there are any that are not updated at the
* completion of the batch then it will restart itself.
*
* @Author salesforce Services
* @Date 01/28/2016
*/
global without sharing class NMD_AccountNamingBatch implements Database.Batchable<SObject> {

	public static Id runNow() {
		return Database.executeBatch(new NMD_AccountNamingBatch(), 2000);
	}

	global String query =
		'select ' +
	        'Account_Number__c, ' +
	    	'Mailing_Name__c, ' +
	    	'Name, ' +
	        'RecordTypeId, ' +
	    	'ShippingCity, ' +
	    	'ShippingState, ' +
	    	'Sold_To_City__c, ' +
	    	'Sold_To_State__c ' +
	   	'from Account ' +
	    'where RecordTypeId in :recordTypeIds ' +
	    'and Mailing_Name__c = null';

	final Set<Id> recordTypeIds = new Set<Id> { AccountManager.RECTYPE_CUSTOMER, AccountManager.RECTYPE_PROSPECT };

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(this.query);
	}

	global void execute(Database.BatchableContext BC, List<Account> accts) {

		AccountManager manager = new AccountManager();

		for (Account acct : accts) {
	        manager.formatNaming(acct);
	    }

	    Database.update(accts, false);

	}

	global void finish(Database.BatchableContext bc) {
		
		List<Account> remainingAccts = new List<Account>([
			select
		        Id
		   	from Account
		    where RecordTypeId in :recordTypeIds
		    and Mailing_Name__c = null
		    limit 1
		]);

		if (remainingAccts.size() > 0) {
			NMD_AccountNamingBatch.runNow();
		}

	}

}
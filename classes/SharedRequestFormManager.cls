/*
* Manager class for the SharedRequestFormHandler
*
* @Author Accenture Services
* @Date 10OCT2016
*/

public class SharedRequestFormManager {

static final Map<String,Schema.RecordTypeInfo> RECTYPES_RequestForm = Schema.SObjectType.Shared_Request_Form__c.getRecordTypeInfosByName();
public static final Id RECTYPE_Sample_Request = RECTYPES_RequestForm.get('PI Sample Request Form').getRecordTypeId();
public static list<PI_Sample_Budget_Tracker__c> updateBudgetLst = new list<PI_Sample_Budget_Tracker__c>();
public static list<Shared_Request_Form__c> updateSampleRequestLst = new list<Shared_Request_Form__c>();
public static map<string,list<PI_Sample_Budget_Tracker__c>> costCenterBudgetMap = new map<string,list<PI_Sample_Budget_Tracker__c>>();
public static boolean isRecursive = false;


/* This method updates the Total Budget and Total Remaining Budget of a particular
*  Sample Request Form as soon as it is created
*/
public void updatePriceFieldsOnSampleForm(list<Shared_Request_Form__c> reqFrmlst){

Date tDate = system.today();
    for(Shared_Request_Form__c pId : reqFrmlst){
        if(costCenterBudgetMap != null && costCenterBudgetMap.containsKey(pId.Cost_Center__c) ){
            for(PI_Sample_Budget_Tracker__c  pB : costCenterBudgetMap.get(pId.Cost_Center__c)){
                 if( (tDate  > pB.PI_Quarter_Start_Date__c && tDate  < pB.PI_Quarter_End_Date__c )
                        || (tDate  == pB.PI_Quarter_Start_Date__c)
                        || (tDate  == pB.PI_Quarter_End_Date__c ) ){
                        
                     if(pB.PI_Current_Quarter_Sample_Budget__c != null){
                         pId.PI_Total_Sample_Budget__c = pB.PI_Current_Quarter_Sample_Budget__c;
                     }
                     if(pB.PI_QTD_Sample_Budget_Remaining__c != null){
                        pId.PI_Sample_Budget_Remaining__c = pB.PI_QTD_Sample_Budget_Remaining__c;
                     }
                     
                 }
             
             }
        }
    }
}
 
/* This method deducts the total amount of all line items from Sample Request Form  
*  from corresponding PI Sample Budget record based on Cost center field
*/   
public void deductAmountFromBudget(map<Id,Shared_Request_Form__c> reqFrmsMap){
       
    for(Id pId: reqFrmsMap.keyset()){            
        if(costCenterBudgetMap != null && costCenterBudgetMap.containsKey(reqFrmsMap.get(pId).Cost_Center__c)){
            for(PI_Sample_Budget_Tracker__c  pB : costCenterBudgetMap.get(reqFrmsMap.get(pId).Cost_Center__c)){
                system.debug('reqFrmsMap.get(pId).PI_Approved_Rejected_Date__c-->'+reqFrmsMap.get(pId).PI_Approved_Rejected_Date__c);
                if( (reqFrmsMap.get(pId).PI_Approved_Rejected_Date__c > pB.PI_Quarter_Start_Date__c && reqFrmsMap.get(pId).PI_Approved_Rejected_Date__c < pB.PI_Quarter_End_Date__c )
                    || (reqFrmsMap.get(pId).PI_Approved_Rejected_Date__c == pB.PI_Quarter_Start_Date__c)
                    || (reqFrmsMap.get(pId).PI_Approved_Rejected_Date__c == pB.PI_Quarter_End_Date__c ) ){
                    system.debug('pB.PI_Total_Approved_Samples__c-->'+pB.PI_Total_Approved_Samples__c);
                    if(reqFrmsMap.get(pId).PI_Total_Price_Of_Line_Items__c != null){
                        pB.PI_Total_Approved_Samples__c += reqFrmsMap.get(pId).PI_Total_Price_Of_Line_Items__c ;
                        updateBudgetLst.add(pB);
                        break;
                    }
                    
                }
            }
        }
    }    
    system.debug(updateBudgetLst +'@@');
}

/* This method fectches all the sample Budget records based on 
*  cost center of the Form
*/
public void fetchSampleBudgetRecords(list<Shared_Request_Form__c> reqFrmsLst){
    set<string> allCostCenter = new set<string>();
    for(Shared_Request_Form__c sF: reqFrmsLst){
        if(sF.Cost_Center__c != null){
            allCostCenter.add(sF.Cost_Center__c);
        }
    }
    system.debug('allCostCenter-->'+allCostCenter);
    if(allCostCenter!= null && allCostCenter.size() > 0){
        for(PI_Sample_Budget_Tracker__c pBudget : [SELECT Id, Name, PI_Facility_Name__c, PI_Cost_Center__c, PI_Regional_Manager__c, Region_Name__c, PI_Current_Quarter_Sample_Budget__c, PI_Total_Approved_Samples__c, 
                                                    PI_Quarter_Start_Date__c, PI_Quarter_End_Date__c, PI_Current_Quarter__c, PI_QTD_Sample_Budget_Remaining__c 
                                                    FROM PI_Sample_Budget_Tracker__c 
                                                    WHERE PI_Cost_Center__c IN: allCostCenter]){
            if(costCenterBudgetMap.containskey(pBudget.PI_Cost_Center__c)){
                costCenterBudgetMap.get(pBudget.PI_Cost_Center__c).add(pBudget);
            }else{
                costCenterBudgetMap.put(pBudget.PI_Cost_Center__c,new list<PI_Sample_Budget_Tracker__c>{pBudget});
            }
        }
    }

}    

public void commitSampleBudgetRecords() {
     if(!updateBudgetLst.isEmpty()){
           try{               
               update updateBudgetLst;
           }catch(Exception e){
               system.debug('Exception caught in *Shared Request Form Handler* addFinally method-->'+e);
           }
       }
}

public void commitSampleFormRecords() {
     if(!updateSampleRequestLst.isEmpty()){
           try{               
               update updateSampleRequestLst;
           }catch(Exception e){
               system.debug('Exception caught in *Shared Request Form Handler* addFinally method-->'+e);
           }
       }
}

    

 
}
//MovePatientToPage3Con

@IsTest(SeeAllData=false)
private class MovePatientToPage3ConTest {
  
  
  static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME = '/-.-NMD SCS Trial-Implant';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static final String OPPORTUNITY_TYPE1 = 'Implant Only';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LEAD_SOURCE = 'Care Card';
    static final NMD_TestDataManager td = new NMD_TestDataManager();
  static public  List<Patient__c> ptList = new List<Patient__c>();
  static public  String SelectedItem4;
  static public  String SelectedItem5;
 static public  Id currentContactId;
 static public List<Physician_Territory_Realignment__c> ptrList = new  List<Physician_Territory_Realignment__c>();
 
  static public List<Contact> cList = new List<Contact>();
 
  static public set<id> oldPatientTerri = new   set<id>();  
    static public List<ID> oldAccIds = new List<ID>();
    static public set<id> conId = new set<id>();

  static public set<id> oldPatientTerriMulti = new set<id>();
   static public set<id> oldContactTerriMulti = new set<id>();
  static public Map<id,String> ContactwiseOldTerritory = new Map<id,String>();
  static public Map<id,String> ContactwiseOldAccount = new Map<id,String>();
  
  
  
  static public testmethod void testDataCreation()
  {
  
  List<RowCauseObjects__c> settingList  = new List<RowCauseObjects__c>();
RowCauseObjects__c setting1 = new RowCauseObjects__c();
setting1.Name = 'Opportunity';
setting1.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,Team';
settingList.add(setting1);


RowCauseObjects__c setting2 = new RowCauseObjects__c();
setting2.Name = 'Patient';
setting2.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild,NMDTerritoryChange__c';
settingList.add(setting2);

RowCauseObjects__c setting3 = new RowCauseObjects__c();
setting3.Name = 'Lead';
setting3.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
settingList.add(setting3);


RowCauseObjects__c setting4 = new RowCauseObjects__c();
setting4.Name = 'Contact';
setting4.RowCauses__c = 'Owner,Rule,Territory2Forecast,ImplicitChild';
settingList.add(setting4);


insert settingList;
  
  
        Account acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

        Account insurance = td.newAccount();
        insurance.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
        insurance.Status__c = 'Active';
        
        Account acctTrialing = td.newAccount(ACCT_TRIALING);
        acctTrialing.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing.AccountNumber = '1234';
        acctTrialing.Type = 'Sold to';

        Account acctProcedure = td.newAccount(ACCT_PROCEDURE);
        acctProcedure.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctProcedure.Type = 'Sold to';
        
        insert new List<Account> { acctMaster, acctTrialing, acctProcedure, insurance };

        Contact trialing = td.newContact(acctTrialing.Id);
        trialing.AccountId = acctTrialing.id;
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        Contact procedure = td.newContact(acctProcedure.Id);
        procedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { trialing, procedure };
        conId.add(trialing.id);
        conId.add(procedure.id);
        /*Opportunity oppty1 = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        
        Opportunity oppty2 = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty2.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        
        insert new List<Opportunity> { oppty1, oppty2};*/
        
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        insert territory;
        
        
        

        Patient__c patient = td.newPatient('Fname', 'Lname');
        patient.Territory_ID__c=territory.Id;
        patient.Primary_Insurance__c = insurance.id;
        patient.Secondary_Insurance__c = insurance.id;
        patient.Physician_of_Record__c = trialing.id;
        patient.Referring_Physician__c = trialing.id;
        patient.RecordTypeId = PatientManager.RECTYPE_PROSPECT;
        insert patient;




    
        for(integer i =1;i<4;i++){
        
            Patient__c p = td.newPatient('Fname'+i, 'Lname'+i);
        p.Territory_ID__c=territory.Id;
        p.Primary_Insurance__c = insurance.id;
        p.Secondary_Insurance__c = insurance.id;
        p.Physician_of_Record__c = trialing.id;
        p.Referring_Physician__c = trialing.id;
        p.RecordTypeId = PatientManager.RECTYPE_PROSPECT;
        oldPatientTerri.add(territory.Id);
        oldPatientTerriMulti.add(territory.Id);
       // insert patient;
        ptList.add(p);
        
        }
        
    


insert ptList;
        
       
        User usr = new User(
        Email = 'suser@boston.com', 
        LastName = 'LNAMETEST',  
        ProfileId = Label.NMD_RBM_Profile_Id, 
        UserName ='abc@boston.com',
        Alias = 'standt', 
        EmailEncodingKey = 'UTF-8',  
        LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US',  
        TimeZoneSidKey = 'America/Los_Angeles'
        /*Cost_Center_Code__c = randomString5()*/);
        insert usr;  
        
        Assignee__c assign = td.newAssignee(usr.id,territory.id); 
        insert assign;
        
        //assigning same master account to both opportunities.
        Opportunity trialOpty = td.newOpportunity(acctMaster.Id, OPPTY_NAME_TRIAL);
        trialOpty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        trialOpty.Trialing_Account__c = acctTrialing.Id;
        trialOpty.Trialing_Physician__c = trialing.Id;
        trialOpty.Procedure_Account__c = acctProcedure.Id;
        trialOpty.Patient__c = ptList[0].Id;
        trialOpty.Procedure_Physician__c = procedure.Id;
        trialOpty.Scheduled_Trial_Date_Time__c = system.today();
        trialOpty.Territory_ID__c = territory.Id;
        trialOpty.closeDate = System.today()+30;
        trialOpty.LeadSource = LEAD_SOURCE;
        trialOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
        trialOpty.CloseDate = system.today();
        //insert trialOpty;
        
        Opportunity implantOpty = td.newOpportunity(acctMaster.Id, OPPTY_NAME_IMPLANT);
        implantOpty.Territory_ID__c = territory.Id;
        implantOpty.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        implantOpty.Scheduled_Trial_Date_Time__c = system.today();
        implantOpty.closeDate = System.today()+30;
        implantOpty.LeadSource = LEAD_SOURCE;
        implantOpty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        implantOpty.Patient__c = ptList[0].Id;
        implantOpty.Procedure_Account__c = acctProcedure.Id;
        implantOpty.Procedure_Physician__c = procedure.Id;
        implantOpty.CloseDate = system.today();
        //insert implantOpty;
        
        Opportunity implantOpty1 = td.newOpportunity(acctMaster.Id, OPPTY_NAME_IMPLANT);
        implantOpty1.Territory_ID__c = territory.Id;
        implantOpty1.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        implantOpty1.Scheduled_Trial_Date_Time__c = null;
        implantOpty1.Scheduled_Procedure_Date_Time__c = null;
        implantOpty1.closeDate = null;
        implantOpty1.LeadSource = LEAD_SOURCE;
        implantOpty1.Patient__c = ptList[0].Id;
        implantOpty1.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        implantOpty1.Procedure_Account__c = null;
        implantOpty1.Procedure_Physician__c = null;
        implantOpty1.CloseDate = system.today();
        
        insert new List<Opportunity> { trialOpty, implantOpty, implantOpty1};
  
Lead ld = new Lead();  
ld.LastName = 'lname';
ld.Company = 'Company' ;
ld.Status = 'Open';
ld.Patient__c = ptList[0].Id;
ld.RecordTypeId = LeadManager.RECTYPE_NMD_PATIENTS;

insert ld;
//Patient__c

Contact ctemp = new Contact();
ctemp.Territory_ID__c = ptList[0].Territory_ID__c; 
if(insurance.id != null)
ctemp.AccountId = insurance.id;
ctemp.LastName = 'lname';
ctemp.recordtypeId = ContactManager.RECTYPE_CUSTOMER;//Schema.SObjectType.Contact.getRecordTypeInfosByName().get('General Contact').getRecordTypeId(); // Added by Ashish
insert ctemp;


  ContactwiseOldTerritory.put(ctemp.Id,ctemp.Territory_ID__c);
  ContactwiseOldAccount.put(ctemp.Id,ctemp.AccountId);


//conId.add(ctemp.id);
cList.add(ctemp);





 Date futureDate = date.today() +1;


        Physician_Territory_Realignment__c a = new Physician_Territory_Realignment__c();
        Physician_Territory_Realignment__c a2 = new Physician_Territory_Realignment__c();

        a.Contact__c = ctemp.id;        
        // a.Current_Territory__c = cMap.Territory_ID__c;  //old
        a.Current_Territory__c = ptList[0].Territory_ID__c;  //old
        a.Effective_Realignment_Date__c =futureDate;
        //a.New_Territory__c =cMap.Territory_ID__c;
        a.New_Territory__c = ptList[0].Territory_ID__c;
        a.Realignment_Status__c = 'Pending '; 
        ptrList.add(a);
             
        a2.Contact__c = ctemp.id;        
        // a.Current_Territory__c = cMap.Territory_ID__c;  //old
        a2.Current_Territory__c = ptList[0].Territory_ID__c;  //old
        a2.Effective_Realignment_Date__c =futureDate;
        //a.New_Territory__c =cMap.Territory_ID__c;
        a2.New_Territory__c = ptList[0].Territory_ID__c;
        a2.Realignment_Status__c = 'readyforpatient '; 
        ptrList.add(a2);
                
        //Realignment_Status__c = 'readyforpatient'        
   insert ptrList;    


    
oldAccIds.add(ctemp.AccountID);   
oldContactTerriMulti.add(ctemp.Territory_ID__c);

 SelectedItem4 =  territory.Id;
 SelectedItem5=insurance.id;
 currentContactId = ctemp.id;


  }
  
static testmethod void testFocusSingle (){
    User CCRUser =  NMD_TestDataManager.setupUser();



insert CCRUser;

testDataCreation();


system.debug('CCRUser===='+CCRUser);
        system.runAs(CCRUser) {
        
        system.debug('ptList===='+ptList);
        Test.startTest();
        MovePatientToPage3Con InsObj = new MovePatientToPage3Con();
        InsObj.focusPhysicianUpdate(ptList,SelectedItem4,SelectedItem5,currentContactId,oldPatientTerri);
       // InsObj.focusPhysicianUpdate(ptList,SelectedItem4,SelectedItem5,currentContactId,oldPatientTerri);
       //focusPhysicianUpdate(List<Patient__c>  patients,String SelectedItem4,String SelectedItem5,Id currentContactId,set<id> oldPatientTerri)
        
       
        // Added by Ashish --Start--
        // PhysicianRequestHandler.appLogs.clear();
         InsObj.sendToSAPForMultiple(new list<Physician_Territory_Realignment__c>());
         MovePatientToPage3Con.sendSelctedPatientToBatch();   
         MovePatientToPage3Con.sendPOldTerriToBatch();   
         InsObj.sendPatientinfoforBatchUse(null,null);       
        // Added by Ashish --end--
        }
       
    Test.stopTest();
    }    


static testmethod void testmultiplePhysicianUpdateVF (){
    User CCRUser =  NMD_TestDataManager.setupUser();
insert CCRUser;

testDataCreation();


system.debug('CCRUser===='+CCRUser);
        system.runAs(CCRUser) {
        
        system.debug('ptList===='+ptList);
        Test.startTest();
         NMD_TestDataManager.createRowCauseObjects();
        MovePatientToPage3Con InsObj = new MovePatientToPage3Con();

        //## InsObj.multiplePhysicianUpdateVF(cList,PtrList,ptList,oldAccIds,false,oldContactTerriMulti,oldPatientTerriMulti,ContactwiseOldTerritory,ContactwiseOldAccount);
        
        InsObj.currExeMultiplePhysicianUpdate(PtrList,ptList,false,oldPatientTerriMulti);
        InsObj.forBatchMultiplePhysicianUpdate(cList,PtrList,oldAccIds,true,oldContactTerriMulti);
        
        
        
//currExeMultiplePhysicianUpdate(List<Physician_Territory_Realignment__c> PtrList,List<Patient__c> SelectedPatientList,  Boolean conUpdt,set<id> oldPatientTerriMulti)      
   //   forBatchMultiplePhysicianUpdate(List<Contact>  SelectedPhysicianList, List<Physician_Territory_Realignment__c> PtrList, List<id> oldAccountIds        , Boolean conUpdt,set<id> oldContactTerriMulti) 

//multiplePhysicianUpdateVF(List<Contact>  SelectedPhysicianList, List<Physician_Territory_Realignment__c> PtrList,List<Patient__c> SelectedPatientList, List<id> oldAccountIds, Boolean conUpdt,set<id> oldContactTerriMulti,set<id> oldPatientTerriMulti, Map<id,String> ContactwiseOldTerritory,Map<id,String> ContactwiseOldAccount ) 

 
        

        }
   
    Test.stopTest();
    }   
    
    
    
static testmethod void testmultiplePhysicianUpdate (){
    User CCRUser =  NMD_TestDataManager.setupUser();
insert CCRUser;

testDataCreation();


system.debug('CCRUser===='+CCRUser);
        system.runAs(CCRUser) {
        
        system.debug('ptList===='+ptList);
        test.StartTest();
         NMD_TestDataManager.createRowCauseObjects();
        MovePatientToPage3Con InsObj = new MovePatientToPage3Con();
       
       //# InsObj.multiplePhysicianUpdate(cList,PtrList,ptList,oldAccIds,true,oldContactTerriMulti,oldPatientTerriMulti);
        
        // multiplePhysicianUpdate(List<Contact>  SelectedPhysicianList, List<Physician_Territory_Realignment__c> PtrList,List<Patient__c> SelectedPatientList, List<id> oldAccountIds, Boolean conUpdt,set<id> oldContactTerriMulti,set<id> oldPatientTerriMulti) 
      

        }
   
    Test.stopTest();
    }

//multiplePhysicianUpdateImmediate   movePatientsOpptyLead
static testmethod void testmultiplePhysicianUpdateImmediate (){
    User CCRUser =  NMD_TestDataManager.setupUser();
insert CCRUser;

testDataCreation();


system.debug('CCRUser===='+CCRUser);
        system.runAs(CCRUser) {
        
        system.debug('ptList===='+ptList);
        test.StartTest();
         NMD_TestDataManager.createRowCauseObjects();
        MovePatientToPage3Con InsObj = new MovePatientToPage3Con();
 
        InsObj.movePatientsOpptyLead(cList);    
        
        for(Contact cc :cList){
        
        conId.add(cc.id);
        }
        
     
        MovePatientToPage3ConHelper.movePatientsOnly(conId);
        //static public void movePatientsOnly(set<id>  contactIds1)
        //InsObj.multiplePhysicianUpdateImmediate(ptList,oldPatientTerriMulti);
        //public void multiplePhysicianUpdateImmediate(List<Patient__c> SelectedPatientList,set<id> oldPatientTerriMulti)
     
       

        }
   
    Test.stopTest();
    }   
    
    
    

}
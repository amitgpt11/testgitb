/**
* Extension class for the QuincyReport pages
*
* @author   Salesforce services
* @date     2015-02-06
*/ 
public with sharing class NMD_QuincyReportExtension extends NMD_QuincyExtensionBase {

  public Id painMapId { get; private set; }

  List<Clinical_Data_Summary__c> cdss = null;
  
  public Boolean isConsignmentAcct { get; private set; }

  public String userRBMName {
    get{
      String rbmName = '';

      User us = [SELECT Id, Manager.Name FROM User WHERE Id = :UserInfo.getUserId()];
      if(!String.IsBlank(us.Manager.Name)){
        rbmName = us.Manager.Name;
      }

      return rbmName;
    }

    set;
  }

  public String userASDName {
    get{
      String asdName = '';

      User us = [SELECT Id, ManagerId, Manager.Manager.Name FROM User WHERE Id = :UserInfo.getUserId()];
      if(us.ManagerId != null){
        if(!String.isBlank(us.Manager.Manager.Name)){
          asdName = us.Manager.Manager.Name;
        }
      }

      return asdName;
    }

    set;
  }


  public NMD_QuincyReportExtension(ApexPages.StandardController sc) {
    super(sc);

    this.isConsignmentAcct = (Id)String.valueOf(sc.getRecord().get('RecordTypeId')) == AccountManager.RECTYPE_CONSIGNMENT;

    String childIdsStr = ApexPages.currentPage().getParameters().get('cid');
    childIdsStr = String.IsBlank(childIdsStr) ? '' : childIdsStr;

    fetchChilds(new Set<String>(childIdsStr.split(',', -1)));

  }

  private void fetchChilds(Set<String> childIds) {

    Schema.SObjectType objectType = sc.getRecord().getSObjectType();

    if (objectType == Account.SObjectType) {

      fetchRelatedContacts(childIds);

    }
    else if (objectType == Contact.SObjectType) {
      
      fetchRelatedAccounts(childIds);

    }
    else if (objectType == Opportunity.SObjectType) {
      
      fetchRelatedAttachments(childIds);

    }

  }

  private void fetchRelatedContacts(Set<String> childIds) {
    Integer count = 0;
    for (Contact con : [
      select
        Account.Name,
        Account.BillingCity,
        Account.BillingPostalCode,
        Account.BillingState,
        Account.BillingStreet,
        CreatedBy.Name,
        Email,
        Fax,
        Name,
        Owner.Manager.Manager.Name,
        Owner.Manager.Name,
        Phone,
        Academic_Title__c,
        Date_of_First_Surgery__c,
        NPI_Number__c,
        SAP_ID__c
      from Contact
      where Id in :childIds
      order by LastName, FirstName
    ]) {
      children.add(new NMD_ObjectWrapper(con, ++count));
    }

    //User user = [
    //  select
    //    Manager.Name,
    //    Manager.Manager.Name
    //  from User
    //  where Id = :UserInfo.getUserId()
    //];

    //this.asdName = String.isBlank(user.Manager.Manager.Name) ? '' : user.Manager.Manager.Name;
    //this.rbmName = String.isBlank(user.Manager.Name) ? '' : user.Manager.Name;

  }

  private void fetchRelatedAccounts(Set<String> childIds) {
    Integer count = 0;
    for (Account acct : [
      select
        BillingCity,
        BillingPostalCode,
        BillingState,
        BillingStreet,
        Fax,
        Name,
        Phone,
        Account_Number__c,
        Billing_Attn__c,
        Classification__c,
        Sold_To_Attn__c,
        Sold_to_City__c,
        Sold_to_State__c,
        Sold_to_Street__c,
        Sold_to_Zip_Postal_Code__c,
        Tax_Exempt__c
      from Account
      where Id in :childIds
      order by Name
    ]) {
      children.add(new NMD_ObjectWrapper(acct, ++count));
    }
  }

  @TestVisible
  private void fetchRelatedAttachments(Set<String> childIds) {
    Integer count = 0;
    for(FeedItem item : [
            select 
              CreatedDate,
              RelatedRecordId,
              Title, 
              ContentDescription
            from FeedItem
            where Id in :childIds
            order by CreatedDate desc
        ]) {
      children.add(new NMD_ObjectWrapper(item, count++));
    }

    List<Opportunity> ops = [select id, Patient__c from Opportunity where id = :sc.getId() limit 1];
     if (!ops.isEmpty()) {     
      cdss = new List<Clinical_Data_Summary__c>([
      select Id  
      from Clinical_Data_Summary__c
      where RecordTypeId = :ClinicalDataSummaryManager.RECTYPE_PAINMAP
      and Patient__c= :ops[0].Patient__c
      and isActive__c= true
        ]);
     }
    if (cdss!=null && !cdss.isEmpty()) {

      List<Attachment> attachs = new List<Attachment>([
        select Id
        from Attachment
        where Name = 'painmap.png'
        and ParentId = :cdss[0].Id
        order by CreatedDate desc
        limit 1
      ]);

      this.painMapId = attachs.isEmpty() ? null : attachs[0].Id;

    }
  }

}
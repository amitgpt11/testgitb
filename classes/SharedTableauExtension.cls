/*
This class will be used to ideify user profiel to show right Tableau report based on user profile
Modified On : 24AUG2016
Modified By : Mayuri 
Modification comments : Added two new methods myCustomFieldMapper() and getSignedIdentity() to support tableau reports in SF1    
*/
/* 
Modified On : 10/10/16
Modified By : VM
Modification comments : added public group search for IC users for Tableau report US# 2699, 2647
*/
public class SharedTableauExtension{
  public Profile currentuserProfile{get;set;} 
   
  public Group currentuserGroup{get;set;}

  public SharedTableauExtension(){
     
        List<GroupMember> GrpMember= new List<GroupMember>();
        
        currentuserProfile=new Profile();
        currentuserProfile=[Select Id,Name from Profile where Id=:userinfo.getProfileId()];
        GrpMember =[select GroupId FROM GroupMember where UserOrGroupId=:userinfo.getUserId() AND (GroupId =: System.Label.IC_Tableau_Public_Group_Clinical_Specialists OR GroupId =: System.Label.IC_Tableau_Public_Group) LIMIT 1];
        if(GrpMember.size()>0)
        {
            currentuserGroup=[select name from group where id =:GrpMember[0].GroupId];
        }
    
  } 
  
  @TestVisible private String myCustomFieldMapper() {
        // This customer written class does the "work" need to map and get a Tableau username
        // In our example we use a static string "sfUser" as the string to map.
        //public StringMyUserId = UserInfo.getUserId();
        List<User> u = [Select FederationIdentifier from user where id=:UserInfo.getUserId()];
        System.debug('FEderated Id is ---'+u[0].FederationIdentifier);
        return u[0].FederationIdentifier;
    }
    
    /**
    * Gets the signed identity; always want to generate this in a getter since the constructor
    * only gets called on original page load and timestamp will skew
    */
    public String getSignedIdentity() {
        String signedIdentity = TableauSparklerUtilities.generateSignedIdentity(this.myCustomFieldMapper());
        system.debug('Singned Entity========'+signedIdentity);
        return signedIdentity;
    }
}
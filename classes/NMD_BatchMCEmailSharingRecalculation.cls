/**
NMD_BatchMCEmailSharingRecalculation : this class is created to recalcualte MC Email Sharing based on Patient Sharing i.e.
whoever has access to a patient record, should have access to associated (with patient) MC Email records. 
This batch class runs every night after Patient Sharing batch and takes all MC Email records in the system in account for 
sharing recalculation.
*/

global class NMD_BatchMCEmailSharingRecalculation implements Database.Batchable<sObject>, Schedulable
{
    public List<MC_Email__c> mcEmails = new List<MC_Email__c>();
    public List<Patient__c> patients = new List<Patient__c>();
    public List<Seller_Hierarchy__c> territories = new List<Seller_Hierarchy__c>();
    
    public Map<Id, List<Assignee__c>> terAssigneesMap = new Map<Id, List<Assignee__c>>();
    public Map<Id, Id> McEmailWithPatientTerritoryIdMap = new Map<Id, Id>();
    
    public Set<Id> terIDs = new Set<Id>();
    public Set<Id> patientIDs = new Set<Id>();
    public Set<Id> AssigneeIDs = new  Set<Id>();
    public Set<Id> mcEmailIDs = new  Set<Id>();
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Patient__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_CUSTOMER = RECTYPES.get('Patient Customer').getRecordTypeId();
    public static final Id RECTYPE_PROSPECT = RECTYPES.get('Patient Prospect').getRecordTypeId();
  
    
    Public Final String EDITACCESSLEVEL  = 'Read';
    
    public String query = 'SELECT Id, Name, PatientID__c, PatientID__r.Territory_ID__c, PatientID__r.RecordTypeId, PatientID__r.Territory_ID__r.id FROM MC_Email__c';
    
    
    public static final Set<Id> RECTYPES_NMD = new Set<Id> {
        PatientManager.RECTYPE_CUSTOMER, PatientManager.RECTYPE_PROSPECT
    };  
    
    // Constructor
    global NMD_BatchMCEmailSharingRecalculation ()  
    {} 
    /****************************************************************
    *  start(Database.BatchableContext BC)
    *****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('Inside Strat method');
        if(Test.isRunningTest()){
        this.query= this.query+' Limit 10';
        }
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
    *  execute(Database.BatchableContext BC, List scope)
    **********************************************************************/
    global void execute(Database.BatchableContext BC, List<MC_Email__c> scope)
    {
        System.debug('#########Scope####### :'+scope);
        
        List<MC_Email__Share> delMCEmailsShare =  new List<MC_Email__Share>();
        List<MC_Email__c> MCList = new List<MC_Email__c>();
        List<Assignee__c> assgneesForCurrentPatient = new List<Assignee__c>();
        
        MCList= scope;
        
        for(MC_Email__c aME:MCList){
         if (RECTYPES_NMD.contains(aME.PatientID__r.RecordTypeId)){
                terIDs.add(aME.PatientID__r.Territory_ID__r.id);
                patientIDs.add(aME.PatientID__c);
            }
            
        }
        
        patients= [SELECT Id, Territory_ID__c, RecordTypeId, Territory_ID__r.id FROM Patient__c where id in :patientIDs];
        
        territories = [select id, name, Current_TM1_Assignee__c, (select Id, Territory__c, Email__c, Assignee__c,Assignee__r.name, Personnel_ID__c, User_Role__c from Assignees__r where Assignee__r.isActive=true ) from Seller_Hierarchy__c where id in :terIDs];
        
        System.debug('#########territories####### :'+territories);
        for(Seller_Hierarchy__c aTer: territories){
            System.debug('####### List Of Assignees for TER:'+aTer.name+' is '+aTer.Assignees__r);
            terAssigneesMap.put(aTer.id, aTer.Assignees__r);
            for(Assignee__c a: aTer.Assignees__r){
                AssigneeIDs.add(a.Assignee__c);
            }
        }
        System.debug('=========terAssigneesMap '+terAssigneesMap);
        System.debug('=========Number of Assignee Records '+AssigneeIDs.size()+' for MC Email ===================');
        
        
        
        List<MC_Email__Share> CreateMCEmailShares = new List<MC_Email__Share>();
        
        
        for(Patient__c aPatient: patients){//for each patient
            if (RECTYPES_NMD.contains(aPatient.RecordTypeId)){//Filtering only NMD Patient Records 
                if(terAssigneesMap.containsKey(aPatient.Territory_ID__c)){
                    for(Assignee__c a:terAssigneesMap.get(aPatient.Territory_ID__c)){
                        assgneesForCurrentPatient.add(a);
                    }
                }
                //populate McEmailWithPatientTerritoryIdMap 
                
                for(MC_Email__c aME:MCList){
                    if(aME.PatientID__c==aPatient.id){
                         System.debug('#########MC_Email__c  :'+aME +' ___'+aPatient.Territory_ID__c);
                         McEmailWithPatientTerritoryIdMap.put(aME.id, aPatient.Territory_ID__c);
                    }
                        
                }
            }
        }
        System.debug('#########assgneesForCurrentPatient####### :'+assgneesForCurrentPatient);
        System.debug('#########MCList####### :'+MCList);
        System.debug('#########McEmailWithPatientTerritoryIdMap####### :'+McEmailWithPatientTerritoryIdMap);
        
        For(MC_Email__c m: MCList){
            mcEmailIDs.add(m.id);
        }
        
        System.debug('#########AssigneeIDs####### :'+AssigneeIDs);
        System.debug('#########mcEmailIDs####### :'+mcEmailIDs);
        System.debug('#########RowCause####### :'+Schema.MC_Email__Share.RowCause.Patient_Sharing__c);
        //delete MC_Emails__share
        
        //prepare the list of MC Email Share Objects record to delete (Each time)
        delMCEmailsShare = [select id from MC_Email__Share where RowCause = :Schema.MC_Email__Share.RowCause.Patient_Sharing__c];
        
        //Deleting the current Share records - with patient_Sharing__C reason only
        if(!delMCEmailsShare.isEmpty()){
            System.debug('=========Number of MC_EMail Share Records '+delMCEmailsShare.size()+' Deleted ===================');
          //  delete delMCEmailsShare;
        }
        
        System.debug('=========MC EMAIL SHARE CREATION LOOP BEGINS==========');
        //create the new share records based on patient share records : RowCause - Patient_Sharing
        if(!assgneesForCurrentPatient.isEmpty() && !MCList.isEmpty()){
            for(Assignee__c asn:assgneesForCurrentPatient ){//For Each Assignee record in the Patient's Territory - create MC_Email__Share Records.
                
                 for(MC_Email__c aME:MCList){//pull its related MC records and for Each MC Record
                    System.debug('========= McEmail:'+aME);
                    System.debug('========= Assignee__c:'+asn+'___Territory:'+McEmailWithPatientTerritoryIdMap.get(aME.id));
                    If(asn.Territory__c == McEmailWithPatientTerritoryIdMap.get(aME.id)) //condition to add only Patient's Relavent Territory Assignee records for that Particular MCEmail Share table. 
                    {
                    
                    MC_Email__Share CreateMCEmailShareRecord = new MC_Email__Share();
                    CreateMCEmailShareRecord.AccessLevel = EDITACCESSLEVEL;
                    CreateMCEmailShareRecord.ParentId= aME.id;
                    CreateMCEmailShareRecord.RowCause= Schema.MC_Email__Share.RowCause.Patient_Sharing__c;
                    CreateMCEmailShareRecord.UserOrGroupId = asn.Assignee__c;
                    CreateMCEmailShares.add(CreateMCEmailShareRecord);
                    }
                }
                
            }
        }
        
        system.debug('CreateMCEmailShares Size before adding Manually Shared Records is '+CreateMCEmailShares.size());
        
        //Taking care of Manually Shares Patients - MC Email access
        List<Patient__Share> ptShares = [select id, parentid , userOrGroupID , rowcause from Patient__Share where ParentId in :patientIDs and RowCause = 'Manual'];
        
        for(MC_Email__c aME:MCList){
            for(Patient__Share ps : ptShares){
                if(ps.ParentId == aME.PatientID__c){
                    MC_Email__Share CreateMCEmailShareRecord = new MC_Email__Share();
                    CreateMCEmailShareRecord.AccessLevel = EDITACCESSLEVEL;
                    CreateMCEmailShareRecord.ParentId= aME.id;
                    CreateMCEmailShareRecord.RowCause= Schema.MC_Email__Share.RowCause.Patient_Sharing__c;
                    CreateMCEmailShareRecord.UserOrGroupId = ps.userOrGroupID;
                    CreateMCEmailShares.add(CreateMCEmailShareRecord);
                }
            }
            
        }
        
        system.debug('CreateMCEmailShares Size after adding Manually Shared Records is '+CreateMCEmailShares.size());
        
        if(!CreateMCEmailShares.isEmpty()){
            while(CreateMCEmailShares.size()>0){
                if(CreateMCEmailShares.size()>5000){
                    //handling 10001 dml exception.
                    List<MC_Email__Share> CreateMCEmailShares_partial = new List<MC_Email__Share>(); //batch of 5000
                        for(integer i =0; i<5000;i++){
                            CreateMCEmailShares_partial.add(CreateMCEmailShares[0]);
                            CreateMCEmailShares.remove(0);//this will remove first 5000 records from the original list. 
                        }
                        System.debug('=========Inserting Partial '+CreateMCEmailShares.size()+' Share Records for MC Email ===================');
                        insert CreateMCEmailShares_partial;//inserting 5000 records at a time.
                    }else{
                    break;      
                    }
            }
            if(CreateMCEmailShares.size()<5000){
                System.debug('=========Inserting '+CreateMCEmailShares.size()+' Share Records for MC Email ===================');
                insert(CreateMCEmailShares);
            }
        }
    }
    
    /****************************************************
*  finish(Database.BatchableContext BC)
*****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Batch Execution finished.');
    }
        
    global void execute(SchedulableContext sc) {
      NMD_BatchMCEmailSharingRecalculation b = new NMD_BatchMCEmailSharingRecalculation(); 
      database.executebatch(b, 50);//Batch size 50.
   }
}
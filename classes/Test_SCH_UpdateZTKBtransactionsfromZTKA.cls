@isTest
private class Test_SCH_UpdateZTKBtransactionsfromZTKA {

   // CRON expression: every hour.
   // Because this is a test, job executes
   // immediately after Test.stopTest().
   public static String CRON_EXP = '0 0 0 ? * *';

   static testmethod void test() {
      Test.startTest();

      // Schedule the test job
      String jobId = System.schedule('Schedule_UpdateZTKBtransactionsfromZTKA',
                        CRON_EXP, 
                        new Schedule_UpdateZTKBtransactionsfromZTKA());
         
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);
    
      Test.stopTest();

   }
}
public class FormatDateTimezoneController{

    public DateTime date_time { get; set; } //property that reads the datetime value from component attribute tag
    public String defined_format { get; set;} //property that reads the string value from component attribute tag
    public String ownerId { get; set;} //property that reads the string value from component attribute tag
    
    public String getFormattedDatetime(){
        if(ownerId != null){
            String timeZoneString = [select id,TimeZoneSidKey from user where id =: ownerId].TimeZoneSidKey;
            
            if (date_time == null) {return ''; 
            }else { 
                if (defined_format == null) {
                    return date_time.format(); //return the full date/time in user's locale and time zone
                }else { 
                    // return date_time.format(defined_format,'PST');  //Specify Time zone like IST,CST
                    return date_time.format(defined_format,timeZoneString);
                }
            }
        }else{
            return '';
        }
    }
}
/**
* Manager class for the UserRole object, to help build the NMD Area/Region hierarchy
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class NMD_UserRoleManager {

	static NMD_UserRoleManager instance;
	public static NMD_UserRoleManager getInstance() {
		if (instance == null) {
			instance = new NMD_UserRoleManager();
		}
		return instance;
	}

	@TestVisible
	final static String NEUROMODUS = (Neuromod_UserRole__c.getInstance().Developer_Name__c);

	private Id neuromodId; // Id of the "root" Neuromod role
	public Map<Id,UserRole> userRoles { get; private set; } // map of all roles found
	public Map<Id,UserRole> areaRoles { get; private set; } // map of areas found
	public Map<Id,UserRole> regionRoles { get; private set; } // map of regions found

	public NMD_UserRoleManager() {

		this.userRoles = new Map<Id,UserRole>{};
		this.areaRoles = new Map<Id,UserRole>{};
		this.regionRoles = new Map<Id,UserRole>{};
	
		UserRole root = [select Name, ParentRoleId from UserRole where DeveloperName = :NEUROMODUS];
		this.userRoles = new Map<Id,UserRole>(new List<UserRole> { root });
		this.neuromodId = root.Id;

		Map<Id,UserRole> childRoles = fetchChildRoles(new Set<Id> { root.Id });
		// keep fetching new child roles until all have been added. since ParentRoleId is not 
		// a true lookup we have to iteratively fetch child roles until no more are returned
		while (!childRoles.isEmpty()) {

			// add the current children
			this.userRoles.putAll(childRoles);

			for (UserRole role : childRoles.values()) {
				// check for area
				if (role.ParentRoleId == this.neuromodId) {
					this.areaRoles.put(role.Id, role);
				}
				// check for region - since areas are fetched before regions it will be populated JIT
				else if (this.areaRoles.containsKey(role.ParentRoleId)) {
					this.regionRoles.put(role.Id, role);
				}
			}

			// fetch the childrens children.
			childRoles = fetchChildRoles(childRoles.keySet());

		}

	}

	private Map<Id,UserRole> fetchChildRoles(Set<Id> parentIds) {
		return new Map<Id,UserRole>([
			select 
				Name, 
				ParentRoleId 
			from UserRole 
			where ParentRoleId in :parentIds
		]);
	}

	public Boolean isUserRoleInNeuromod() {
		return isUserRoleInNeuromod(UserInfo.getUserRoleId());
	}

	public Boolean isUserRoleInNeuromod(Id roleId) {
		return isValidRole(roleId);
	}

	public UserRole getUserRegion() {
		return getUserRegion(UserInfo.getUserRoleId());
	}

	public UserRole getUserRegion(Id roleId) {

		UserRole region;

		if (isValidRole(roleId)) {

			if (this.regionRoles.containsKey(roleId)) {
				return this.regionRoles.get(roleId);
			}
			else {
				UserRole role = this.userRoles.get(roleId);
				return getUserRegion(role.ParentRoleId);
			}
 
		}

		return region;

	}

	public Set<Id> getRolesInRegion() {
		
		Set<Id> roleIds = new Set<Id>{};
		UserRole region = getUserRegion();
		
		if (region != null) {
			roleIds.addAll(getRolesInRegion(region.Id));
		}

		return roleIds;
	}

	public Set<Id> getRolesInRegion(Id regionId) {

		Set<Id> roleIds = new Set<Id>{};
		for (UserRole role : this.userRoles.values()) {
			UserRole region = getUserRegion(role.Id);
			if (region != null && region.Id == regionId) {
				roleIds.add(role.Id);
			}
		}
		return roleIds;

	}

	private Boolean isValidRole(Id roleId) {
		return roleId != null && this.userRoles.containsKey(roleId);
	}

}
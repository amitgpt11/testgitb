/*
 *Description: This class initilizes the files to be shared with community user on Documents Page
 */
public without sharing class DocumentsController {
    
    public List<FeedItemWrapper> lstFeedItemWrapper         {get;set;} 
    
    /*
    * Description: This wrapper class holds the FeetItem and its DownloadUrl String
    */
    private class FeedItemWrapper {
        
        public String downloadUrl              {get;set;} 
        public FeedItem objFeedItem            {get;set;}
        
        public FeedItemWrapper(){
            
            objFeedItem = new FeedItem();
        }
    }
    
    /*
    * Description: Controller Initilizes the lstFeedItemWrapper to be displayed on page
    */
    public DocumentsController() {
        
        String strDocumentDownloadUrlprefix;
        FeedItemWrapper objFeedItemWrapper;
        List<FeedItem> lstFeedItem = new List<FeedItem>();
        lstFeedItemWrapper = new List<FeedItemWrapper>();
        
        User objUser = [Select ContactId 
            From User 
            Where Id =: Userinfo.getUserId()];
        
        if(objUser.ContactId != null) {
            
            lstFeedItem = [Select RelatedRecordId, Id
                From FeedItem
                Where parentId =: objUser.ContactId
                AND Visibility = 'AllUsers'];  
        }    
        
        if(Boston_Scientific_Config__c.getValues('Default') != null) {
            
            strDocumentDownloadUrlprefix = Boston_Scientific_Config__c.getValues('Default').Boston_Document_Download_Url__c;
            for(FeedItem objFeedItem : lstFeedItem){
                
                objFeedItemWrapper = new FeedItemWrapper();
                objFeedItemWrapper.objFeedItem = objFeedItem;
                if(objFeedItem.RelatedRecordId != null) {
                    
                    objFeedItemWrapper.downloadUrl = strDocumentDownloadUrlprefix+objFeedItem.RelatedRecordId;    
                }
                lstFeedItemWrapper.add(objFeedItemWrapper);
            }
        }   
    }
}
/*
@CreatedDate     21JUNE2016
@author          Mike Lankfer
@Description     The trigger handler for the Implant custom object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : DRAFT US2484
@Modified Date   15SEP2016
@Modified By     Payal Ahuja
*/
public with Sharing class ImplantTriggerHandler extends TriggerHandler {
    
    
    private ImplantMatching matchImplant = new ImplantMatching();
    private PatientCaseManager caseManager = new PatientCaseManager();
    
    
/*
@CreatedDate     21JUNE2016
@author          Mike Lankfer
@Description     Executes bulk actions before Implant DML
@Requirement Id  User Story : DRAFT US2484
*/     
    public override void bulkBefore(){        
        if (Trigger.isInsert || Trigger.isUpdate){
            List<LAAC_Implant__c> implants = (List<LAAC_Implant__c>) Trigger.new;
            
            try{
            matchImplant.matchRelatedRecords(implants);
            //caseManager.DeleteImplants(implants);
            }
            catch (Exception e){
                System.debug('The following exception has been caught by the ImplantTriggerHandler.BulkBefore method: ' + e);
            }
        }    
    }
    
    
/*
@CreatedDate     21JUNE2016
@author          Mike Lankfer
@Description     Executes bulk actions after Implant DML
@Requirement Id  User Story : DRAFT US2484
*/     
    public override void bulkAfter(){
    List<LAAC_Implant__c> implants = (List<LAAC_Implant__c>) Trigger.new;
        if (Trigger.isInsert){
            try{
            caseManager.updateStatusOnInsert(implants);
            }
            catch (Exception e){
                System.debug('The following exception has been caught by the ImplantTriggerHandler.BulkAfter method: ' + e);
            }
        }
        if(Trigger.isUpdate){
         try{
           caseManager.DeleteImplants(implants);
           }
         catch (Exception e){
                System.debug('The following exception has been caught by the ImplantTriggerHandler.BulkAfter method: ' + e);
            } 
        }
    }
}
/**
* Model class, to pair a Products NMD pricebook entry with the relevant accounts pricebook entry
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public class NMD_PricebookPair {

	public PricebookEntry nmdEntry { get; private set; }
	public PricebookEntry acctEntry { get; private set; }
	public Boolean isRestricted { 
		get {
			return this.nmdEntry.Product2.Material_Group_1__c == '002';
		}
	}

	public NMD_PricebookPair(PricebookEntry nmdEntry, PricebookEntry acctEntry) {

		System.assertNotEquals(null, nmdEntry);
		if (acctEntry != null) {
			System.assertEquals(nmdEntry.Product2Id, acctEntry.Product2Id);
		}

		this.nmdEntry = nmdEntry;
		this.acctEntry = acctEntry;

	}

	public PricebookEntry consolidate() {
		PricebookEntry pbe = this.nmdEntry.clone(true, true);
		pbe.IsDisplayable_InMemory__c = pbe.Is_Displayable__c;
		if (this.acctEntry != null) {
			pbe.UnitPrice = this.acctEntry.UnitPrice;
			pbe.IsDisplayable_InMemory__c = this.acctEntry.Is_Displayable__c;
		}
		return pbe;
	}

}
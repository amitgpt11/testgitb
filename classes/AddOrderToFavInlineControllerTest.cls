/*
@CreatedDate     16SEPT2016
@author          Ashish
@Description     Tests Controller for AddOrderToFavInline VF Page
@Requirement Id  User Story : SFDC-1490
*/

@isTest

public class AddOrderToFavInlineControllerTest {
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Shared_Patient_Case__c.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROMENSHEALTH = RECTYPES.get('Urology Men\'s Health').getRecordTypeId();
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPESORD = Schema.SObjectType.Order.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROPHLOANERKIT = RECTYPESORD.get('Uro/PH Loaner Kit').getRecordTypeId();
    
    @testSetup
    static void setupMockData(){
        list<Account> testAccLst = new list<Account>();
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId(),Account_Number__c = 'Test123',shippingCountry='US'); 
        testAccLst.add(acct1);
        insert testAccLst;   
        
         Shared_Patient_Case__c UMHPatientCaseobj = new Shared_Patient_Case__c(Shared_Facility__c = testAccLst[0].Id,Shared_Start_Date_Time__c = system.today()+6,Shared_end_Date_Time__c = system.today()+8,
                                                          RecordTypeId = RECTYPE_UROMENSHEALTH);
         insert UMHPatientCaseobj;
         
         Order ordObj = new Order(AccountId = testAccLst.get(0).id,Uro_PH_Patient_Case__c = UMHPatientCaseobj.id, RecordTypeId = RECTYPE_UROPHLOANERKIT,
                                  EffectiveDate= date.Today(),Status='Draft'); 
        
         Insert ordObj ; 
    }
    
    static testmethod void addOrderToFavorites(){
        
        list<Order> ord = [select id from Order];
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord.get(0));
        
        AddOrderToFavInlineController controller = new AddOrderToFavInlineController(sc);
        
        controller.showOrderFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }  
    
    static testmethod void addAccountToFavorites2(){
        
        list<Order> ord = [select id from Order];
        
        Order_Favorite__c OrderFavorite = new Order_Favorite__c(Uro_PH_Order__c = ord.get(0).Id, Uro_PH_User__c = UserInfo.getUserId());
        insert OrderFavorite;
        
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(ord.get(0));
        
        AddOrderToFavInlineController controller = new AddOrderToFavInlineController(sc);
        
        controller.showOrderFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }
}
@IsTest(SeeAllData=false)
private class NMD_ProductsDispatcherTest {

	final static NMD_TestDataManager td = new NMD_TestDataManager();
	final static RestDispatcherV1 dispatcher = new RestDispatcherV1(null);
	final static String ACCTNAME = 'ACCTNAME';
	final static String PRODNAME = 'PRODNAME';

	static {
		RestContext.response = new RestResponse();
	}
	
	@testSetup
	static void setup() {

		td.setupNuromodUserRoleSettings();

		Pricebook2 pbNmd = td.newNMDPricebook();
		insert pbNmd;

		Product2 prod0 = td.newProduct(PRODNAME);
		prod0.Material_Group_1__c = '002'; // limited release
		prod0.Product_Hierarchy__c = '12345678901234567890';
		insert prod0;

		PricebookEntry pb0a = td.newPricebookEntry(prod0.Id);
		PricebookEntry pb0b = td.newPricebookEntry(pbNmd.Id, prod0.Id);
		insert new List<PricebookEntry> { pb0a, pb0b };

		Account acct = td.createConsignmentAccount();
		acct.Name = ACCTNAME;
		insert acct;

		Opportunity oppty = td.newOpportunity(acct.Id);
		insert oppty;

		Order order = td.newOrder(acct.Id, oppty.Id);
		insert order;

	}

	static testMethod void test_ProductsDispatcher_URI() {

		NMD_ProductsDispatcher agent = new NMD_ProductsDispatcher();
		String uri = agent.getURIMapping();

	}

	static testMethod void test_ProductsDispatcher_NoPricebook() {

		Map<String,String> parameters = new Map<String,String>();
		NMD_ProductsDispatcher agent = new NMD_ProductsDispatcher();

		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_ProductsDispatcher_NoOrder() {

		Map<String,String> parameters = new Map<String,String> {
			NMD_ProductsDispatcher.KEY_PRICEBOOKNAME => 'NMD'
		};

		NMD_ProductsDispatcher agent = new NMD_ProductsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_ProductsDispatcher_BadOrder() {

		Map<String,String> parameters = new Map<String,String> {
			NMD_ProductsDispatcher.KEY_PRICEBOOKNAME => 'NMD',
			NMD_ProductsDispatcher.PARAM_ORDERID => 'foo'
		};

		NMD_ProductsDispatcher agent = new NMD_ProductsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_ProductsDispatcher_Order() {

		Order ord = [select Id from Order where Account.Name = :ACCTNAME];

		Map<String,String> parameters = new Map<String,String> {
			NMD_ProductsDispatcher.KEY_PRICEBOOKNAME => 'NMD',
			NMD_ProductsDispatcher.PARAM_ORDERID => ord.Id
		};

		NMD_ProductsDispatcher agent = new NMD_ProductsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_ProductsDispatcher_OrderAndList() {

		String fuzzyName = '%' + PRODNAME + '%';

		Product2 prod = [select Id from Product2 where Name like :fuzzyName];
		Order ord = [select AccountId from Order where Account.Name = :ACCTNAME];

		// create list exemption
		insert new List__c(
			Inventory_Account__c = ord.AccountId,
			Model_Number__c = prod.Id,
			Material_Group_1__c = '002',
			Sales_Organization__c = 'US10'
		);

		Map<String,String> parameters = new Map<String,String> {
			NMD_ProductsDispatcher.KEY_PRICEBOOKNAME => 'NMD',
			NMD_ProductsDispatcher.PARAM_ORDERID => ord.Id
		};

		NMD_ProductsDispatcher agent = new NMD_ProductsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

}
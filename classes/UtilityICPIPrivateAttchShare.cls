/*
 @CreatedDate     18SEPT2016                                  
 @ModifiedDate    18SEPT2016                                  
 @author          Accenture
 @Description     Utility class used by batch classes 
 @Requirement Id  Q4 2016 - SFDC12
 */
public without sharing class UtilityICPIPrivateAttchShare{

    
    public set<string> PIProfiles = new set<string>();
    public set<string> ICProfiles = new set<string>();
    public UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
    public map<Id,set<Id>> getAccountIdUserMap(list<AccountShare> accountShareLst){
      
       map<Id,set<Id>> acctUserIdMap = new map<Id,set<Id>>();
       map<Id,set<Id>> acctTerritoryIdMap = new map<Id,set<Id>>();
       set<Id> acctIdSet = new set<Id>();
       list<UserTerritory2Association> usrTerritoryLst = new list<UserTerritory2Association>();
       UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
       String userType = Schema.SObjectType.User.getKeyPrefix(); 
       map<Id,set<Id>> territoryUsrIdMap = new map<Id,set<Id>>();      
       
       if(accountShareLst != null && accountShareLst.size() > 0){
           for(AccountShare m : accountShareLst){               
                acctUserIdMap.put(m.AccountId,new set<Id>{});
                // If the account is shared to a territory
                if(m.RowCause == 'TerritoryManual')                //skip the records shared to Public group/vis sharing Role manually 
                {
                    acctIdSet.add(m.AccountId);
                }
           }
           system.debug('!@#$'+acctUserIdMap);
           system.debug('%^&*'+acctIdSet);
           
       }
       
       if(acctIdSet.size() > 0){
           acctTerritoryIdMap  = util.getAcctnTeritryAssctnList(acctIdSet);
           if(acctTerritoryIdMap.size() > 0){
               set<Id> territoryIdSet = new set<Id>();
               for(Id actId : acctTerritoryIdMap.keyset()){
                   territoryIdSet.addAll(acctTerritoryIdMap.get(actId));
               }              
               usrTerritoryLst = util.getUserTeritryAssctnList(territoryIdSet,false);
           }
           if(usrTerritoryLst.size() > 0){
               for(UserTerritory2Association u : usrTerritoryLst){
                   if(territoryUsrIdMap.containsKey(u.Territory2Id)){
                       territoryUsrIdMap.get(u.Territory2Id).add(u.UserId);
                   }else{
                       territoryUsrIdMap.put(u.Territory2Id,new set<Id>{u.UserId});
                   }
               }
           
           }
       }
       for(AccountShare m : accountShareLst){
       
            if(((String)m.UserOrGroupId).startsWith(userType))
            {
                if(acctUserIdMap.containsKey(m.AccountId))
                    acctUserIdMap.get(m.AccountId).add(m.UserOrGroupId); 
            }
            else if(m.RowCause == 'TerritoryManual'){
                if(acctTerritoryIdMap.keyset().contains(m.AccountId)){
                    for(Id tId : acctTerritoryIdMap.get(m.AccountId)){
                        if(territoryUsrIdMap.keyset().contains(tId)){
                            if(acctUserIdMap.containsKey(m.AccountId)){
                                acctUserIdMap.get(m.AccountId).addAll(territoryUsrIdMap.get(tId));
                            }
                        }
                    }
                }
           }
      }
      
       system.debug('acctUserIdMap-->'+acctUserIdMap);
       return acctUserIdMap;
    }
    
    
    /*
    @MethodName    CreateNewShareRecords
    @Parameters    PASlst: List of Private_Attachments_Shared__Share records.
    @Return Type   void
    @Description   Method to create new  share records
    */
    public void CreateNewShareRecords(list<Shared_Private_Attachments__Share> PASlst){
         if(PASlst != NULL && PASlst.size()>0){
                Database.SaveResult[] objInsertResult = Database.insert(PASlst,TRUE);
                      
                // Error handling code Iterate through each returned result
                for (Database.SaveResult er : objInsertResult) {
                    if (er.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Share Record. Record ID: ' + er.getId());
                        
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : er.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Record fields that affected this error: ' + err.getFields());
                        }
                    }
                }                   
         } 
    
    
    }
    
    /*
    @MethodName    removePrivateAttchShares
    @Parameters    myObjIdLst: List of My_Objectives__c Id records.
    @Return Type   boolean
    @Description   Method to remove all My_Objectives__Share records for the list of My_Objectives__c Id list specified
    */
    public boolean removePrivateAttchShares(set<Id> PAIdLst){
        list<Shared_Private_Attachments__share> objectShareLst = new list<Shared_Private_Attachments__share>();
        integer successCount = 0;       
        boolean isSuccess;
        Database.DeleteResult[] drList;
        // Get a list of My_Objectives__Share records
        if(PAIdLst != null && PAIdLst.size()>0){
            objectShareLst = queryPAShareRecords(PAIdLst);
        }    
        
        if(objectShareLst != null && objectShareLst.size()>0){
            drList = Database.delete(objectShareLst,true);
                  
            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully deleted My_Objectives__share with ID: ' + dr.getId());
                    successCount++;
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : dr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('My_Objectives__share fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            if(successCount == objectShareLst.size()){
                isSuccess = true;
            }
            else{
                isSuccess = false;
            }
        }
        else{
            isSuccess = true;
        }
       
       
       return isSuccess;
    }
     
    public void reCalculateShareMethod(list<Shared_Private_Attachments__c> PASLst,list<AccountShare> accountShareLst){
        PIProfiles.addAll(PrivateAttachmentShared_ProfileData__c.getValues('PI').Profile_Names__c.split(';'));        //Set of all PI profile names
        ICProfiles.addAll(PrivateAttachmentShared_ProfileData__c.getValues('IC').Profile_Names__c.split(';'));        //set of all IC profile names
        map<Id,set<Id>> acctUserIdMap = new map<Id,set<Id>>();
        set<Id> userIdSet = new set<Id>();
        map<Id,User> userMap = new map<Id,User>();
        list<Shared_Private_Attachments__Share> newShareRecords = new list<Shared_Private_Attachments__Share>();
        if(accountShareLst!= null && accountShareLst.size() > 0){
           acctUserIdMap = getAccountIdUserMap(accountShareLst);
           if(acctUserIdMap.size() > 0){
               for(Id actId : acctUserIdMap.keyset()){
                   userIdSet.addAll(acctUserIdMap.get(actId));
               }
           }
        }
        if(userIdSet.size() > 0){
            userMap = util.getUserInfoMap(userIdSet);
        }
        for(Shared_Private_Attachments__c pa : PASLst){
           
            if(pa.Account__c != null && acctUserIdMap.containsKey(pa.Account__c)){
                for(Id uId : acctUserIdMap.get(pa.Account__c)){                                             
                    if(pa.ICPI_Division__c == 'PI' && PIProfiles.contains(userMap.get(uId).Profile.Name)){ 
                        Shared_Private_Attachments__Share newPAShare = new Shared_Private_Attachments__Share();
                        newPAShare.AccessLevel = 'Edit';                            // 'All' permission is forbidden when you share a record through Apex
                        newPAShare.ParentId = pa.Id;                       
                        newPAShare.RowCause = Schema.Shared_Private_Attachments__Share.rowCause.PIPrivateAttachmentShare__c;
                        newPAShare.UserOrGroupId = uId;
                        newShareRecords.add(newPAShare);
                    }                                                                                     
                    else if(pa.ICPI_Division__c == 'IC' && ICProfiles.contains(userMap.get(uId).Profile.Name)){
                        Shared_Private_Attachments__Share newPAShare = new Shared_Private_Attachments__Share();
                        newPAShare.AccessLevel = 'Edit';                            // 'All' permission is forbidden when you share a record through Apex
                        newPAShare.ParentId = pa.Id;
                        newPAShare.RowCause = Schema.Shared_Private_Attachments__Share.rowCause.ICPrivateAttachmentShare__c;
                        newPAShare.UserOrGroupId = uId;
                        newShareRecords.add(newPAShare);
                    }
                    //if(userMap != null && userMap.keyset().size()>0 && userMap.keyset().contains(uId) && userMap.get(uId).isActive == true){
                       
                   // }
                    
                }
            }
        }
        
        if(newShareRecords.size() > 0){
            CreateNewShareRecords(newShareRecords);
        }
    } 
    
     public list<Shared_Private_Attachments__Share> queryPAShareRecords(set<Id> PAIdSet){
         list<Shared_Private_Attachments__Share> PAShareLst = new list<Shared_Private_Attachments__Share>();
        if(PAIdSet != null && PAIdSet.size()>0){
                PAShareLst = new list<Shared_Private_Attachments__share>([SELECT Id,AccessLevel,ParentId,RowCause,UserOrGroupId,UserOrGroup.Profile.Name 
                                                                        FROM Shared_Private_Attachments__share
                                                                        WHERE ParentId IN : PAIdSet AND ( RowCause =: Schema.Shared_Private_Attachments__Share.rowCause.ICPrivateAttachmentShare__c 
                                                                        OR RowCause =: Schema.Shared_Private_Attachments__Share.rowCause.PIPrivateAttachmentShare__c)]);
         }  
         return PAShareLst;
      } 
    
}
/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ExportMirfCsvBatch implements Database.Batchable<SObject>, Database.Stateful {
    global List<openq__MIRF_Columns__c> columns;
    global List<SObject> columnsToUse;
    global openq.SearchMirfsController.ScopeCriteria criteria;
    global openq.ScopeCriteria criteriaToUse;
    global List<String> csvLines;
    global Integer line;
    global Map<Id,User> mapUser;
    global Id processId;
    global String Query;
    global ExportMirfCsvBatch(String query, List<openq__MIRF_Columns__c> columns, String csvName, Date startDate, Date endDate) {

    }
    global ExportMirfCsvBatch(String objectName, String query, List<SObject> columns, String csvName, Date startDate, Date endDate) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global List<openq__Process__c> getProcess() {
        return null;
    }
    global void sendDoc(Blob csvBlob) {

    }
    global void sendErrorEmail() {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
    global void startProcess() {

    }
}

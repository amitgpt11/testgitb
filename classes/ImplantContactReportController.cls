/*
@ModifiedDate    25SEP2016
@ModifiedBy      Payal Ahuja
@Description     Contains information about implants as an Implanting Physician and procedures as an Referring Physician on Contact object.
@Requirement Id  SFDC-1405 
*/

public class ImplantContactReportController {
    
    public Map<String,Integer> implantCountMap{get;set;}
    public List<String> displayOrderList{get;set;}
    
    public ImplantContactReportController(ApexPages.StandardController controller){
        Id contactId = ((Contact)controller.getRecord()).Id;
        
        displayOrderList = new List<String>{Label.Implanting,Label.Successful_Implants,Label.Referring};
        implantCountMap = new Map<String,Integer>{Label.Implanting => 0,Label.Successful_Implants => 0,Label.Referring => 0};
        
        for (LAAC_Implant__c implant :[SELECT Id, LAAC_Implanting_Physician__c, LAAC_Referring_Physician_LU__c, LAAC_Case_Aborted__c FROM LAAC_Implant__c WHERE LAAC_Implanting_Physician__c = :contactId OR LAAC_Referring_Physician_LU__c = :contactId]){
                  
             if (implant.LAAC_Implanting_Physician__c == contactId){
                Integer primaryCount = implantCountMap.get(Label.Implanting);
                implantCountMap.put(Label.Implanting,++primaryCount);
                if (!implant.LAAC_Case_Aborted__c){
                    Integer primarySuccess = implantCountMap.get(Label.Successful_Implants);
                    implantCountMap.put(Label.Successful_Implants,++primarySuccess);
                }
            }
          }  
                
         for (Shared_Patient_Case__c procedure :[SELECT Id,Shared_Referring_Physician__c FROM Shared_Patient_Case__c WHERE  Shared_Referring_Physician__c = :contactId]){
           
            if (procedure.Shared_Referring_Physician__c == contactId){
                Integer refCount = implantCountMap.get(Label.Referring);
                implantCountMap.put(Label.Referring,++refCount);
          
                }
            }
        }
}
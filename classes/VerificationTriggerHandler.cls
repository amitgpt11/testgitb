/**
* trigger handler class for the Verification__c object
*
* @Author Accenture Services
* @Date 2016/09/28
*/
public with sharing class VerificationTriggerHandler extends TriggerHandler {
    
    //final VerificationManager manager = new VerificationManager();
	final VerificationManager1 manager = new VerificationManager1();
    public override void bulkbefore(){
        /*manager.getCurrentUserProfileName();
        
        if(trigger.isUpdate){
        	manager.restrictReptoUpdateReconcileType(Trigger.newMap, Trigger.oldMap);
        }*/
		if(trigger.isInsert){
			manager.fetchVerificaitonofMissingItem(Trigger.new);
            manager.fetchOrderswithCriterion();
			manager.updateFileredVerificaitons(Trigger.new);
		}
    }
    public override void bulkAfter() {
        //trigger.isUpdate || 
       /*if(trigger.isInsert){
            system.debug('In VerificationTriggerHandler==>');
            manager.fetchVerificaitons((List<Verification__c>)trigger.new );
            manager.fetchRelatedFieldRep();
            manager.fetchOrderswithCriterion();
            manager.updateVeriTrackAndVeriType((Map<Id,Verification__c>)trigger.newMap);
        }*/
    }

    public override void beforeInsert(SObject obj) {

    }

    public override void beforeUpdate(SObject oldObj, SObject newObj) {
        Verification__c oldVerify = (Verification__c)oldObj;
        Verification__c newVerify = (Verification__c)newObj;
       // manager.restrictReptoUpdateReconcileType(oldVerify,newVerify);
    }

    private Verification__c cast(SObject obj) {
        return (Verification__c)obj;
    }

}
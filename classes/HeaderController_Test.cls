@isTest
private class HeaderController_Test {

    private static testMethod void test() {
        
        Test.startTest();
        
        User objUser = Test_DataCreator.createCommunityUser();
        
        System.runAs(objUser){
        
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            objOrder.Status__c = 'Open';
            update objOrder;
            
             //Create Products
            Test_DataCreator.createProducts();
            
            Id productId = [Select Id From Product2 Limit 1].Id;
            Shared_Community_Order_Item__c objOrderItem = Test_DataCreator.createOrderItem(objOrder.Id, productId);
            
            //Create Product
            Product2 objProduct = Test_DataCreator.createProduct('testProduct');
            
            //create Boston Scientific Header Footer Config test data using test_DataCreator class 
            Test_DataCreator.cerateBostonCSHeaderFooterTestData();
            
            //create Boston scientific cofig test data using Test_DataCreator class
            Community_Event__c objEvent = Test_DataCreator.createCommunityEvent(Date.valueOf(Date.today()));
            Test_DataCreator.cerateBostonCSConfigTestData(objEvent.Id);
            
            system.Test.setCurrentPage(Page.Events);
            
            //making an instace covers the constructor of the class HeaderController 
            HeaderController objHeaderController = new HeaderController();
            system.debug('objOrder.Contact__c========='+objOrder.Contact__c);
            system.debug('objUser.ContactId==============='+objUser.ContactId);
            system.debug('query===='+[Select count()
                      From Shared_Community_Order_Item__c 
                      Where Shared_Order__r.Contact__r.Id =: objUser.ContactId
                      AND Shared_Order__r.Status__c = 'Open']);
            
            system.assertNotEquals(objHeaderController.myListsize,1);
            
            HeaderController.updateUser();
            
            User obj = [Select Agreement_Accepted__c 
                From User Where Id =: Userinfo.getUserId()];
                
            system.assertEquals(true, obj.Agreement_Accepted__c);    
        }
        
        Test.stopTest();
    }
}
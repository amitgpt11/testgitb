@isTest 
private class DemoRequestFormExtensionTest{

@testSetup 
static void setupData() {

Map <String,Schema.RecordTypeInfo> recordTypes_Account = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_Account = recordTypes_Account.get('Customer').getRecordTypeId();

Account acc = new Account();
acc.Name ='Test Account';
acc.RecordTypeId = recordTypeID_Account;
acc.ShippingStreet = 'street';
acc.ShippingCity = 'city';
acc.ShippingState = 'state';
acc.ShippingPostalCode = '123445';
acc.Account_Number__c = '123456';                                 
insert acc;

Map <String,Schema.RecordTypeInfo> recordTypes_Contact = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_Contact  = recordTypes_Contact.get('General Contact').getRecordTypeId();
   Contact conc = new Contact();  
   conc.AccountId= acc.Id;  
    
   conc.LastName='Test';
   insert conc;
    
Map <String,Schema.RecordTypeInfo> recordTypes_Products = Product2.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id recordTypeID_Product3  = recordTypes_Products.get('Standard Product Hierarchy - Level 3').getRecordTypeId(); 
    Id recordTypeID_Product5  = recordTypes_Products.get('Standard Product Hierarchy - Level 5').getRecordTypeId();     
   
    
   Product2 p1= new Product2();
   p1.recordTypeId = recordTypeID_Product3;
   p1.Shared_Custom_Product_Group_Level_3__c = 'Level3';
   p1.Name='Test Level3';
   p1.EAN_UPN__c = 'Test EAN/UPN';
   p1.ProductCode = 'test code';
    p1.Shared_Geography__c='US';
   p1.UPN_Material_Number__c = 'T234567';
   p1.Division__c = 'PI';   
   insert  p1; 
   system.assertEquals(p1.recordTypeId,recordTypeID_Product3);
   Product2 p2= new Product2();
   p2.recordTypeId = recordTypeID_Product5;
   p2.Shared_Custom_Product_Group_Level_3__c = 'Level3';
    p2.Shared_Geography__c='US';
    
   p2.Shared_Custom_Product_Group_Level_5__c = 'Level5';
   p2.Name='Test Level5';
   p2.UPN_Material_Number__c = 'T1123456';   
   p2.Division__c = 'PI';
   insert  p2; 
   
    Product2 p3= new Product2();
   p3.recordTypeId = recordTypeID_Product5;
   p3.Shared_Custom_Product_Group_Level_6__c = 'Level5';
   p3.Name='Test CPG 6';
    p3.Shared_Geography__c='US';
   p3.UPN_Material_Number__c = 'T198754'; 
   p3.Division__c = 'PI';  
   insert  p3; 
   
 
   
   
  // User userobj =  new User();

  /*Profile p = [SELECT Id FROM Profile WHERE Name ='System Administrator' Limit 1]; 
            User userobj = new User(
                Email = 'suser@boston.com', 
                LastName = 'LNAMETEST',  
                ProfileId = p.Id, 
                UserName = 'UserName12' + '@boston.com',
                Alias = 'standt', 
                EmailEncodingKey = 'UTF-8',  
                LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US',  
                TimeZoneSidKey = 'America/Los_Angeles',
                Cost_Center_Code__c = '1234', 
                IsActive = True);
                
                insert userobj;*/
   Map <String,Schema.RecordTypeInfo> recordTypes_SharedRequestForm = Shared_Request_Form__c.sObjectType.getDescribe().getRecordTypeInfosByName();
  Id recordTypeID_SharedRequestForm  = recordTypes_SharedRequestForm .get('UroPH HCP Disease State PES').getRecordTypeId();            
   Shared_Request_Form__c sharedreq = new  Shared_Request_Form__c();              
   sharedreq.Requesters_Name__c=Userinfo.getUserId();//userobj.Id;
  sharedreq.Physician_Name__c=conc.Id;
  sharedreq.Prior_Speaking_Training_Experience__c='Yes';
  sharedreq.Residency_Institution__c=acc.Id;
  sharedreq.Speaking_Experience_Examples__c='Faculty for training Programs';
  sharedreq.Years_of_Speaking_Training_Experience__c='< 3 years';
  sharedreq.Years_in_Practice__c= '< 3 years';
  sharedreq.Region__c = 'Europe';
  sharedreq.Business_Unit__c = 'BPH' ; 
  sharedreq.RecordTypeId = recordTypeID_SharedRequestForm  ;
  insert sharedreq;     
  

  Product2 prodobj= new Product2();
   prodobj.Name='Test';
   prodobj.EAN_UPN__c = 'Test EAN/UPN';
   prodobj.ProductCode = 'test code';
   prodobj.UPN_Material_Number__c = 'T1e2s3t4';
   prodobj.Shared_Geography__c='US';
   insert  prodobj; 
   
    
/*
  Product2 prodobj2= new Product2();
   prodobj2.Name='TestABC2';
  prodobj2.EAN_UPN__c='1122345';
    
//    prodobj2.RecordTypeId=RECTYPE_LEVEL4;
   prodobj2.UPN_Material_Number__c='123421415'; 
    //prodobj2.Shared_Parent_Product__c=prodobj.Id ;
    
    insert prodobj2;
    */
    
 Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
            rItem.Full_UPN__c = '12345';
            rItem.Quantity__c = 1;
            rItem.Unit_Of__c = 'EA'; 
insert rItem;

Shared_Request_Form_Line_Item__c  rItem1 = new Shared_Request_Form_Line_Item__c(); 
            rItem1.Full_UPN__c = '12345';
            rItem1.Quantity__c = 1;
            rItem1.Unit_Of__c = 'EA'; 
insert rItem1;

 /*Shared_Parent_Product__c  objparentprod = new Shared_Parent_Product__c ();
 objparentprod.Name='Test';
 objparentprod.UPN_Material_Number__c='12345';
 objparentprod.EAN_UPN__c ='12345';
 insert objparentprod;*/
 
 
    

}
    static TestMethod void TestDemoRequestFormExtension(){ 
       // setupData();
        Account acc = [Select id,name from Account limit 1];
        Product2 p = [Select id,Name from Product2 where Shared_Custom_Product_Group_Level_6__c !=null limit 1];
        Test.StartTest();
        DemoRequestFormExtension objdemo = new  DemoRequestFormExtension();
      
        objdemo.requestForm.Facility__c = acc.id;
       // objdemo.requestForm.Onbehalf_Of__c = acc.id;
        objdemo.changeCostCenter();
        objdemo.SelectedItem3 = 'Level3';
        objdemo.selectedItem5 = 'Level5';
        objdemo.selectedItem6 = p.Id;
        objdemo.ValidateValues();
        objdemo.getAllPI5Products();
        objdemo.getAllCPG6Products();
        objdemo.selectedItem6();
        objdemo.getLEVEL3Products();
        objdemo.getLEVEL5Products();
        objdemo.getCPG6Products();
        objdemo.addProductToLst();
        objdemo.sendPdf();
        objdemo.save();
        
        
         
               
        system.debug('*****'+ objdemo.requestForm);      
        
       // objdemo.changeProductLEVEL4();
       
       // objdemo.getLEVEL4Products();
        
        
     list<Shared_Request_Form__c> lstObj = [Select Id,Name,Requesters_Name__c,Requesters_Name__r.Name from Shared_Request_Form__c limit 1];
    if(lstObj.size()>0){  
      Apexpages.currentpage().getparameters().put('id',lstObj.get(0).id);
        
      CallPDFController objcall= new CallPDFController();
      objcall.callPdfPage();
   }   
        Test.StopTest();
    }
     static TestMethod void TestDemoRequestFormExtension2(){ 
       // setupData();
        Account acc = [Select id,name from Account limit 1];
        Product2 p = [Select id,Name from Product2 where Shared_Custom_Product_Group_Level_6__c !=null limit 1];
        Test.StartTest();
        DemoRequestFormExtension objdemo = new  DemoRequestFormExtension();
      
        objdemo.facilityID = acc.id; 
        objdemo.selectedItem6 = '';
        objdemo.addProductToLst();
        objdemo.Save();   
        Test.StopTest();
    }
}
/**
 * Name : ResetIndicatorController_MyObjectiveTest 
 * Author : Sipra
 * Description : Test class used for testing the ResetIndicatorController_MyObjective
 * Date : 19th April 2016
 */
@isTest
private class ResetIndicatorController_OpportunityTest { 

@testSetup
static void setupMockData(){
        List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();       
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA6789',Territory2ModelIDActive, Ttype[0].Id);
        insert t1;
}

    static testMethod void  testSave_Scenario1(){    
    
         Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  //since territory and account records cannot be created in same method, created Account record in each method
        insert acct1;     
        
        Opportunity MO = new Opportunity();
         MO.Name = 'test';
         MO.AccountId = acct1.Id;
         MO.StageName='Qualification';
         MO.CloseDate=System.Today();
         //MO.CurrencyIsoCode='USD-U.S.Dollar';
         MO.Territory_Assigned__c = true;
        insert MO;
        PageReference pg = Page.Reset_Indicator_Mobile_Page_Opportunity;
        ApexPages.currentPage().getParameters().put('id', MO.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(MO);
        ResetIndicatorController_Opportunity controller =  new  ResetIndicatorController_Opportunity(ctr);        
        Test.setCurrentPage(Page.Reset_Indicator_Mobile_Page_Opportunity);        
        controller.SaveMO();
           
    }
    
    static testMethod void  testSave_Scenario2(){ 
        Territory2 tr = [Select Name,Id From Territory2 where name Like : 'AA%'  limit 1];
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        Opportunity MO1 = new Opportunity();
        MO1.Name = 'test';
        MO1.AccountId = acct1.Id;
         MO1.StageName='Qualification';
         MO1.CloseDate=System.Today();
         MO1.Territory2Id = tr.Id;    
         MO1.Territory_Assigned__c = false;      
        insert MO1;
        MO1.Territory2Id = tr.Id;    
         MO1.Territory_Assigned__c = TRUE;  
         update MO1;
        
        
        ApexPages.currentPage().getParameters().put('id', MO1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(MO1);
        ResetIndicatorController_Opportunity controller =  new  ResetIndicatorController_Opportunity(ctr);        
        Test.setCurrentPage(Page.Reset_Indicator_Mobile_Page_Opportunity);        
        controller.SaveMO();
       
        
           
    }
    static testMethod void  testSave_Scenario3(){ 
        Territory2 tr = [Select Name,Id From Territory2 where name Like : 'AA%'  limit 1];
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        Opportunity MO2 = new Opportunity();
        MO2.Name = 'test';
        MO2.AccountId = acct1.Id;
         MO2.StageName='Qualification';
         MO2.CloseDate=System.Today();
         MO2.Territory2Id = tr.Id;    
         MO2.Territory_Assigned__c = true;      
        insert MO2;
        
        
        ApexPages.currentPage().getParameters().put('id', MO2.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(MO2);
        ResetIndicatorController_Opportunity controller =  new  ResetIndicatorController_Opportunity(ctr);        
        Test.setCurrentPage(Page.Reset_Indicator_Mobile_Page_Opportunity);        
        controller.SaveMO();
                 
    }


}
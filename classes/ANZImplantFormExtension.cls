/*****  
        Class:  ANZImplantFormExtension  (Controller for Page - ANZImplantForm)
        Created By: Susannah St-Germain
        Created Date: 14th-September-2016
        Last Modified By: Susannah St-Germain
        Last Modify Date: 26th-October-2016
        Description: Controller for the ANZImplantForm VF page that allows ANZ users to submit a form 
                                         and create an order, order products and a patient.
****/
public class ANZImplantFormExtension {
    public String loggedInUserName{get;set;}
    public String loggedInUserDivision{get;set;}
    public String loggedInUserCountry{get;set;}
    public String ProductType{get;set;}
    public String shippingAddress{get;set;}
    public String facilityID{get;set;}
    public Order implantForm{get;set;}
    public OrderItem implantFormLineItem{get;set;}
    public List<PricebookEntry> implantFormProducts{get;set;}
    public List<PricebookEntry> implantFormAccessories{get;set;}
    public List<PricebookEntry> implantFormExplants{get;set;}
    public List<PricebookEntry> implantFormLAACDevices{get;set;}
    public List <OrderItem> OrderItemListing {get;set;}
    public List <OrderItem> AccessoryItemListing {get;set;}
    public List <OrderItem> ExplantItemListing {get;set;}
    public List <OrderItem> LAACItemListing {get;set;}
    public List <OrderItem> ShoppingCartContents {get;set;}
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Product2.getRecordTypeInfosByName();
    public static final Id RECTYPE_IMPLANTFORMDEVICE = RECTYPES_OPPTY.get('Implant Form Device').getRecordTypeId();
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_ORDER= Schema.SObjectType.Order.getRecordTypeInfosByName();
    public static final Id RECTYPE_ORDER = RECTYPES_ORDER.get('ANZ Implant Form').getRecordTypeId();
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_PATIENT= Schema.SObjectType.Patient__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_PATIENT = RECTYPES_PATIENT.get('ANZ Patient').getRecordTypeId();
    public User userRecord;
    Set<String> implantFormProductSet = new Set<String>();
    List<String> implantFormProductList = new List<String>();
    public List<SelectOption> ImplantFormProductPick;
    
    
    public ANZImplantFormExtension(){
        implantForm = new Order();
        implantFormLineItem = new OrderItem();
            
        //Query for devices to feature on implant form
        implantFormProducts = [SELECT Id,Name,Pricebook2Id FROM PricebookEntry WHERE PricebookEntry.Pricebook2.Name= 'ANZ Implant Devices' AND (NAME = 'Lead' OR Name = 'PG')];
        //Take values from product listing and add to list of OrderItems
        OrderItemListing = new List<OrderItem>();
        
        //Get and set Pricebook Id
         PricebookEntry e;
         ID PricebookID ;
        if(!implantformProducts.isEmpty())
        {
            e = implantformProducts[0];
            PricebookID = e.Pricebook2Id;
        }
        this.implantForm.Pricebook2Id = PricebookID;
        
        
        /*Query for pricebook entry ids
        Map<ID, PricebookEntry> m = new Map<ID,PricebookEntry>([Select Id, Name FROM PricebookEntry WHERE Product2id IN: implantFormProducts]);
        for (ID idKey : m.keySet()) {
            PricebookEntry p = m.get(idkey);
            
        }**/
        
        //Take values from device listing and add to list of OrderItems
        for(PricebookEntry p : implantFormProducts){
            OrderItem newItem = new OrderItem();
            newItem.OrderId = implantForm.Id;
            newItem.PricebookEntryId= p.id;
            newItem.Product_Name__c = p.Name;
            newItem.UnitPrice = 1;
            newItem.Quantity = 1;
            newItem.ANZ_Device_Implanted_Used_Successfully__c = true;
            newItem.ANZ_Billing_and_Inventory__c='Invoice & replace SRAI';
            OrderItemListing.add(newItem); 
        }
        
        //Query for accessories to feature on implant form
        implantFormAccessories = [SELECT Id,Name FROM PricebookEntry WHERE PricebookEntry.Pricebook2.Name = 'ANZ Implant Devices' AND (NAME = 'Accessory')];
        
        //Take values from accessory listing and add to list of OrderItems
        AccessoryItemListing = new List<OrderItem>();
            if(!implantFormAccessories.isEmpty()){
                for(PricebookEntry p : implantFormAccessories){
                    OrderItem newItem = new OrderItem();
                    newItem.OrderId = implantForm.Id;
                    newItem.PricebookEntryId= p.Id;
                    newItem.Product_Name__c = p.Name;
                    newItem.UnitPrice = 1;
                    newItem.Quantity = 1;
                    newItem.ANZ_Billing_and_Inventory__c='Invoice & replace SRAI';
                    AccessoryItemListing.add(newItem); 
                }
           }     
        //Query for explanted devices to feature on implant form
        implantFormExplants= [SELECT Id,Name FROM PricebookEntry WHERE PricebookEntry.Pricebook2.Name ='ANZ Implant Devices' AND (NAME = 'Explanted Lead' OR Name = 'Explanted PG')];
        //Take values from product listing and add to list of OrderItems
        ExplantItemListing = new List<OrderItem>();
          if(!implantFormExplants.isEmpty()){    
            for(PricebookEntry p : implantFormExplants){
                OrderItem newItem = new OrderItem();
                newItem.OrderId = implantForm.Id;
                newItem.PricebookEntryId= p.Id;
                newItem.Product_Name__c = p.Name;
                newItem.UnitPrice = 1;
                newItem.Quantity = 1;
                ExplantItemListing.add(newItem); 
            }
        }
        
        //Query for LAAC devices to feature on implant form
        implantFormLAACDevices= [SELECT Id,Name FROM PricebookEntry WHERE PricebookEntry.Pricebook2.Name = 'ANZ Implant Devices' AND (NAME = 'Access Sheath' OR Name = 'LAAC Device')];
        //Take values from product listing and add to list of OrderItems
        LAACItemListing = new List<OrderItem>();
          if(!implantFormLAACDevices.isEmpty()){      
            for(PricebookEntry p : implantFormLAACDevices){
                OrderItem newItem = new OrderItem();
                newItem.OrderId = implantForm.Id;
                newItem.PricebookEntryId= p.Id;
                newItem.Product_Name__c = p.Name;
                newItem.UnitPrice = 1;
                newItem.Quantity = 1;
                newItem.ANZ_Device_Implanted_Used_Successfully__c = true;
                newItem.ANZ_Billing_and_Inventory__c='Invoice & replace SRAI';
                LAACItemListing.add(newItem); 
            }
        }
        
       for(PricebookEntry prod : implantFormProducts){
            implantFormProductSet.add(prod.Name);   
       }
       implantFormProductList.addAll(implantFormProductSet);
       implantFormProductList.sort();
        
          
       userRecord = [Select ID,Name,Cost_Center_Code__C,Division, Country from User Where Id =:UserInfo.getUserId() limit 1];
       loggedInUserName = UserInfo.getName();
       loggedInUserDivision = userRecord.Division;
       loggedInUserCountry = userRecord.Country;
       
    }
   
    /*public List<SelectOption> getImplantFormProductPick(){
        List<SelectOption> options = new List<SelectOption>();
        for(String prod : implantFormProductList){
            options.add(new SelectOption(prod, prod));
        }
        return options;      
    }**/
      
   public List<OrderItem> shoppingCart = new List<OrderItem>();
    public void ClearCart() {
        shoppingCart.clear();
    }
    
    public Boolean doesExistInCart(String serialNumber, String modelNumber) {
        Boolean found = false;
        
        for (OrderItem o: shoppingCart) {
            if (o.Serial_Number__c != null && o.Serial_Number__c == serialNumber && o.Model_Number__c == modelNumber) {
                found = true;
                break;
            }
        }
        
        return found;
    }

    // Action method to handle add to cart process
    public void addToCart(List<OrderItem> items) {
        // this for loop causes only the number items in the form list to be added not multiples of the same.
        for(OrderItem o : items){
            system.debug('There are this many order items' +items.size());
            if((o.Serial_Number__c != null || o.ANZ_Batch_Number__c != null) && !doesExistInCart(o.Serial_Number__c,o.Model_Number__c) && o.Model_Number__c != null) {
                OrderItem cloney = o.clone();
                if(cloney != null){
                shoppingCart.add(cloney);
                ApexPages.Message itemAdded = new ApexPages.Message(ApexPages.Severity.CONFIRM,'You item was added to the cart');
            	ApexPages.addMessage(itemAdded);
                system.debug('This item should added to shopping cart' + o.Product_Name__c);
                }
                else if(1==1){
                ApexPages.Message itemnotAdded = new ApexPages.Message(ApexPages.Severity.WARNING,'Your item was not added to the cart. Please review your entries');
            	ApexPages.addMessage(itemnotAdded);
                }
            }

        }
        
    }
    
    //Add to cart button in NM & CRM Device Section
    public PageReference devicesAddToCart() {
        addToCart(OrderItemListing);
        return null;
    }
    
    //Add to cart button in Accessories Section
    public PageReference accessoriesAddToCart() {
        addToCart(AccessoryItemListing);
        return null;
    }
    
    //Add to cart button in Explanted Devices section
    public PageReference explantsAddToCart() {
        addToCart(ExplantItemListing);
        return null;
    }
    
    //Add to cart button in Devices section for LAAC user
    public PageReference LAACAddToCart() {
        addToCart(LAACItemListing);
        return null;
    }    
    
    //Clear Cart button
    public PageReference clearCartAction() {
        ClearCart();
        return null;
    }
    
    //Set Required Fields
   
    public boolean isRequired{get;set;}{isRequired=true;}   
    
    public PageReference overrideRequired(){
        if(this.implantForm.ANZ_Related_Opportunity__c!=null){
            isRequired = false;
        }
         
    return null;
    }
    
 
    
    //Submit Button
    
    
    public PageReference submitForm() {
       
  
        //Insert Patient     
        implantForm = this.implantForm;
        implantForm.AccountId = this.implantForm.AccountId;
        this.implantForm.OpportunityId = this.implantForm.ANZ_Related_Opportunity__c;
        
        
        Patient__c patient = new Patient__c();
        patient.RecordTypeId = RECTYPE_PATIENT;
        patient.Patient_First_Name__c = this.implantForm.ANZ_Patient_First_Name__c;
        patient.Patient_Last_Name__c = this.implantForm.ANZ_Patient_Last_Name__c;
        patient.Patient_Gender__c = this.implantForm.ANZ_Gender__c;
        patient.Patient_Date_of_Birth__c= this.implantForm.ANZ_DOB__c;
        patient.Address_1__c= this.implantForm.ShippingStreet;
        patient.City__c = this.implantForm.ShippingCity;
        patient.State__c = this.implantForm.ShippingState;
        patient.Zip__c = this.implantForm.ShippingPostalCode;
        patient.ANZ_Country__c = loggedinUserCountry;
        patient.Patient_Email_Address__c = this.implantForm.ANZ_Email_Address__c;
        patient.Hospital_Patient_ID__c = this.implantForm.Hospital_Patient_ID__c;
        patient.Physician_of_Record__c = this.implantForm.ANZ_Implanting_Physician__c;
        patient.Patient_Phone_Number__c = this.implantForm.ANZ_Phone__c;
        
        //if Special Pricing is checked, SDF number is required
        if(this.implantForm.ANZ_Special_Pricing__c == TRUE && this.implantForm.ANZ_SDF_Number_for_Pricing_Approval__c == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter an SDF Number for Pricing Approval when Special Pricing is checked.');
            ApexPages.addMessage(myMsg);
        }
            
            if(this.implantForm.ANZ_Implant_Funding__c == 'Private' && this.implantForm.ANZ_Insurance_Provider__c == null){
            ApexPages.Message myInsuranceMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter an Insurance Provider when Implant Funding is Private.');
            ApexPages.addMessage(myInsuranceMsg);
            
        }
        
        //Cart must have products to submit form
        else if(ShoppingCartContents.size() > 0){
        
        //First insert patient
        if(this.implantForm.ANZ_Related_Opportunity__c == null){
                system.debug('This is the Opportunity ID: ' +this.implantForm.OpportunityId);
                try{
                insert patient;
                
                }
        
        catch(Exception ex){
                        System.debug('Error inserting order record: '+ex.getMessage());         
        }}        
       
        System.debug('Implant Form:' +this.implantForm);
        implantForm = this.implantForm;
        implantForm.AccountId = this.implantForm.AccountId;
        implantForm.EffectiveDate = date.today();
        implantForm.Status = 'Draft';
        implantForm.RecordTypeId = RECTYPE_ORDER;
        implantForm.Shared_Division__c = loggedInUserDivision;
        implantForm.ShippingCountry = loggedInUserCountry;
        implantForm.CurrencyIsoCode = 'AUD';
        
       
        if(this.implantForm.ANZ_Related_Patient__c == null) {
            implantForm.ANZ_Related_Patient__c = patient.Id;
        }
        
        
        //Then insert order
        try{
            insert this.implantForm;
        }catch(Exception ex){
                        System.debug('Error inserting order record: '+ex.getMessage());         
        }
            
        //Then add order products
        if(this.implantForm.id != null){
            for(OrderItem o : ShoppingCartContents){
                o.OrderId = this.implantForm.id;
            }
        }
        
        try{
            System.debug('These are the OrderItems to be inserted :' +ShoppingCartContents);
            
            if(ShoppingCartContents.size() > 0){
                insert ShoppingCartContents;  
            }
        }
            
        catch(Exception ex){
                System.debug('Error inserting OrderItems '+ex.getMessage());
            }
        
        
         //Finally Update Oppty
        Opportunity oppty = new Opportunity();
        oppty.Id = this.implantForm.OpportunityId;
        oppty.PO__c = this.implantForm.PoNumber;
        oppty.StageName = 'Complete (Closed Won)';
        
        
        if(this.implantForm.OpportunityId != null){
                
                try{
                update oppty;
                }
            
            catch(Exception ex){
                System.debug('Error inserting order record: '+ex.getMessage()); 
            }}
        
        return new PageReference('/'+this.implantForm.id);
        
        }
        
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Add at least one item to your cart to submit your implant form.');
            ApexPages.addMessage(myMsg);
        }
        return null;
        }
    
    //In cart line item delete functionality
    public Integer itemIndex{get;set;}
    public PageReference deleteCartItem() {
        
        List<OrderItem> tempCart = new List<OrderItem>();
        Integer counter = -1;
        tempCart.addAll(shoppingCart);
		for (OrderItem o: shoppingCart) {
            counter = counter + 1;
            system.debug(itemIndex + '!=' + counter);
            if (itemIndex == counter)  {
                tempCart.remove(counter);  
                }
            }
        
        shoppingCart.clear();
        if (tempCart.size() > 0) {
            shoppingCart.addAll(tempCart);
        }
        return null;
    }
    
    public List <OrderItem>  getCartContents() {
        ShoppingCartContents = new List <OrderItem> ();
        if(0 == shoppingCart.size()) {
            return null;
        }
        for(OrderItem o : shoppingCart) {
            ShoppingCartContents.add(o);
            system.debug('this item is in my shopping cart contents' +o.Product_Name__c);
        }
        return ShoppingCartContents;
    }
    
    //Button to populate form from data on the opportunity
    
    
    
    public PageReference PopulateOpptyInfo(){
        
        List<Opportunity> opptylist = [SELECT ID,Name,AccountId,PO__c,Referral_MD__c,Procedure_Physician__c,
                                       Patient__r.Patient_First_Name__c, Patient__r.Patient_Last_Name__c,
                                       Patient__r.Patient_Phone_Number__c, Patient__r.Patient_Email_Address__c, 
                                       Patient__r.Patient_Date_of_Birth__c, Patient__r.Patient_Gender__c,
                                       Patient__r.Address_1__c, Patient__r.City__c, Patient__r.State__c,
                                       Patient__r.Zip__c, Patient__r.ANZ_Country__c, Patient__r.Hospital_Patient_ID__c,
                                       Patient__r.Referring_Physician__c, Patient__r.ID
                                       FROM Opportunity 
                                       WHERE ID=:implantForm.ANZ_Related_Opportunity__c];
        system.debug('This is the oppty name++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++' +opptylist);
          System.debug(LoggingLevel.Info, '___opptylist EXTENSION __'+opptylist );
        if(opptylist.size() > 0){
        implantForm.AccountId = opptylist[0].AccountId;
        implantForm.PoNumber = opptylist[0].PO__c;
        implantForm.ANZ_Referring_Physician__c = opptylist[0].Patient__r.Referring_Physician__c;
        implantForm.ANZ_Implanting_Physician__c = opptylist[0].Procedure_Physician__c;
        implantForm.ANZ_Patient_First_Name__c = opptylist[0].Patient__r.Patient_First_Name__c;   
        implantForm.ANZ_Patient_Last_Name__c = opptylist[0].Patient__r.Patient_Last_Name__c; 
        implantForm.ANZ_Phone__c = opptylist[0].Patient__r.Patient_Phone_Number__c;
        implantForm.ANZ_Email_Address__c = opptylist[0].Patient__r.Patient_Email_Address__c;
        implantForm.ANZ_DOB__c = opptylist[0].Patient__r.Patient_Date_of_Birth__c;
        implantForm.ANZ_Gender__c = opptylist[0].Patient__r.Patient_Gender__c;
        implantForm.ShippingStreet = opptylist[0].Patient__r.Address_1__c;
        implantForm.ShippingCity = opptylist[0].Patient__r.City__c;
        implantForm.ShippingState = opptylist[0].Patient__r.State__c;
        implantForm.ShippingPostalCode = opptylist[0].Patient__r.Zip__c;
        loggedinUserCountry = opptylist[0].Patient__r.ANZ_Country__c;
        implantForm.Hospital_Patient_ID__c = opptylist[0].Patient__r.Hospital_Patient_ID__c;
        implantForm.ANZ_Related_Patient__c = opptylist[0].Patient__r.ID;
            
        isRequired=true;}
        
        
        
    return null;        
    }
    
  }
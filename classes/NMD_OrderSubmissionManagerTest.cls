/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_OrderSubmissionManagerTest {

    final static NMD_TestDataManager td = new NMD_TestDataManager();
    final static String ACCTNAME = 'Account Name';
    final static String CONTNAME = 'Contant Name';
    final static String SERIAL1 = '1234567890';
    final static String SERIAL2 = '0987654321';
    static final String STAGE_NAME='Candidate (Trial implant)'; 
    static final String OPTY_TYPE='Standard'; 
    static final String LEADSOURCE = 'CARE Card';
    static final String PAIN_AREA = 'None';
    
    @TestSetup
    static void setup() {
    test.startTest();
        td.setupNuromodUserRoleSettings();
        User testUser = td.newUser('NMD Field Rep');
        insert testUser;

        System.runAs(new User(Id = UserInfo.getUserId())) {

            Seller_Hierarchy__c territory = td.newSellerHierarchy();
            territory.Current_TM1_Assignee__c = UserInfo.getUserId();
            territory.Current_TM2_Assignee__c = testUser.Id;
            insert territory;

            Patient__c patient = new Patient__c(
                RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
                Territory_ID__c = territory.Id,
                SAP_ID__c = td.randomString5()
            );
            insert patient;

            Pricebook2 pb = td.newNMDPricebook();
            insert pb;

            Product2 prod0 = td.newProduct('Trial');
            prod0.ProductCode = td.randomString5();
            Product2 prod1 = td.newProduct('Implant');
            prod1.ProductCode = td.randomString5();
            prod1.Xfer_Type__c = 'Auto';
            insert new List<Product2> { prod0, prod1 };

            PricebookEntry standardPrice0 = td.newPricebookEntry(prod0.Id);
            PricebookEntry standardPrice1 = td.newPricebookEntry(prod1.Id);
            insert new List<PricebookEntry> { standardPrice0, standardPrice1 };

            PricebookEntry pbe0 = td.newPricebookEntry(pb.Id, prod0.Id);
            PricebookEntry pbe1 = td.newPricebookEntry(pb.Id, prod1.Id);
            insert new List<PricebookEntry> { pbe0, pbe1 };

            // create one consignment acct for context user, one for a test user
            Account acct1 = td.newAccount();
            acct1.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
            acct1.Status__c = 'Active';
            acct1.Name = ACCTNAME;
            acct1.OwnerId = testUser.Id;

            Account acct2 = td.createConsignmentAccount();
            acct2.Account_Number__c = td.randomString5();
            acct2.Classification__c = 'NM Sales Rep';
            acct2.OwnerId = UserInfo.getUserId();

            Account acctTrialing = td.newAccount();
            acctTrialing.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
            acctTrialing.Account_Number__c = td.randomString5();
            acctTrialing.Type = 'Sold to';

            Account acctProcedure = td.newAccount();
            acctProcedure.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
            acctProcedure.Account_Number__c = td.randomString5();
            acctProcedure.Type = 'Sold to';

            insert new List<Account> { acct1, acct2, acctTrialing, acctProcedure };

            Contact contTrialing = td.newContact(acctTrialing.Id);
            contTrialing.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            contTrialing.SAP_ID__c = td.randomString5();

            Contact contProcedure = td.newContact(acctProcedure.Id, CONTNAME);
            contProcedure.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            contProcedure.SAP_ID__c = td.randomString5();

            insert new List<Contact> { contTrialing, contProcedure };
            
            Opportunity oppty = td.newOpportunity(acct1.Id);
            oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            oppty.Primary_Insurance__c = acct1.Id;
            oppty.Territory_ID__c = territory.Id;
            oppty.Contact__c=contTrialing.id;            
            oppty.StageName = STAGE_NAME;
            oppty.Opportunity_Type__c = OPTY_TYPE;
            oppty.LeadSource = LEADSOURCE;
            oppty.CloseDate = System.today().addDays(7);
            oppty.Patient__c = patient.Id;
            oppty.Patient_SAP_ID__c = patient.SAP_ID__c;
            oppty.Trialing_Account__c = acctTrialing.Id;
            oppty.Trialing_Account_SAP_ID__c = acctTrialing.Account_Number__c;
            oppty.Trialing_Physician__c = contTrialing.Id;
            oppty.Trialing_Physician_SAP_ID__c = contTrialing.SAP_ID__c;
            oppty.Procedure_Account__c = acctProcedure.Id;
            oppty.Procedure_Physician__c = contProcedure.Id;
            oppty.Pain_Area__c = PAIN_AREA;
            oppty.Trial_Revenue__c = 10;
            insert oppty;
        Test.StopTest();
            // create one inv data per acct
            Inventory_Data__c invData1 = new Inventory_Data__c(
                Account__c = acct1.Id,
                Customer_Class__c = 'YY'
            );
            Inventory_Data__c invData2 = new Inventory_Data__c(
                Account__c = acct2.Id,
                Customer_Class__c = 'YY',
                OwnerId = testUser.Id
            );
            insert new List<Inventory_Data__c> { invData1, invData2 };

            // create one inv location per inv data
            Inventory_Location__c invLoc1 = new Inventory_Location__c(
                Inventory_Data_ID__c = invData1.Id,
                Location_SAP_ID__c = '12345'
            );
            Inventory_Location__c invLoc2 = new Inventory_Location__c(
                Inventory_Data_ID__c = invData2.Id,
                Location_SAP_ID__c = '53321'
            );
            insert new List<Inventory_Location__c> { invLoc1, invLoc2 };

            // create one inventory item per product, alternative between inv locations
            Inventory_Item__c invItem1 = new Inventory_Item__c(
                Inventory_Location__c = invLoc1.Id,
                Model_Number__c = prod0.Id,
                Serial_Number__c = SERIAL1
            );
            Inventory_Item__c invItem2 = new Inventory_Item__c(
                Inventory_Location__c = invLoc2.Id,
                Model_Number__c = prod1.Id,
                Lot_Number__c = SERIAL2
            );
            insert new List<Inventory_Item__c> { invItem1, invItem2 };

            Inventory_Transaction__c invTrans1 = new Inventory_Transaction__c(
                Inventory_Item__c = invItem1.Id,
                Quantity__c = 50
            );
            Inventory_Transaction__c invTrans2 = new Inventory_Transaction__c(
                Inventory_Item__c = invItem2.Id,
                Quantity__c = 50
            );
            insert new List<Inventory_Transaction__c> { invTrans1, invTrans2 };

            Order order1 = new Order(
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                AccountId = acct1.Id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today().addDays(7),
                Pricebook2Id = pb.Id,
                Status = 'Draft',
                PONumber = 'PO to Follow',
                Shipping_Location__c = invLoc1.Id,
                Order_Method__c = 'Automated'
            );
            Order order2 = new Order(
                RecordTypeId = OrderManager.RECTYPE_TRANSFER,
                AccountId = acct1.Id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today().addDays(7),
                Pricebook2Id = pb.Id,
                Status = 'Draft',
                PONumber = 'PO to Follow',
                Shipping_Location__c = invLoc1.Id,
                Order_Reason__c = 'B07',
                PO_Type__c = 'ZFIM',
                Order_Method__c = 'Not Approved',
                PoDate = system.today(),
                Stage__c = 'Not Confirmed'
            );
            
            insert new List<Order> {order1, order2};
        }

    }
    

    static testMethod void test_OrderSubmission_Validate_FieldsMissing() {
        Test.startTest();
        Order order1 = [
            select Id 
            from Order 
            where Account.Name = :ACCTNAME
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT
        ];

        List<PricebookEntry> pbes = [select Id from PricebookEntry where Pricebook2.Name = 'NMD' order by Product2Id];
        List<Inventory_Item__c> items = [select Inventory_Location__c from Inventory_Item__c order by Model_Number__c];

        {
            List<OrderItem> ordItems = new List<OrderItem>();
            for (Integer i = 0; i < 1; i++) {
                ordItems.add(new OrderItem(
                    Billing_Block__c = 'ZB',
                    OrderId = order1.Id,
                    PricebookEntryId = pbes[i].Id,
                    Quantity = 1,
                    UnitPrice = 1,
                    Charge_Type__c = 'Center',
                    Is_Scanned__c = true,
                    Inventory_Source__c = items[i].Id,
                    Inventory_Location__c = items[i].Inventory_Location__c
                ));
            }
            insert ordItems;

            NMD_OrderSubmissionManager manager = new NMD_OrderSubmissionManager(new Set<Id> { order1.Id });
            manager.validateOrder();
        }
        Test.stopTest();

    }
    
    static testMethod void test_OrderSubmission_Validate_BadRecordType() {
      Test.startTest();
      Order order1 = [
          select Id, RecordTypeId
          from Order 
          where Account.Name = :ACCTNAME
          and RecordTypeId = :OrderManager.RECTYPE_TRANSFER
      ];
      
      Order order2 = [
          select Id, RecordTypeId
          from Order 
          where Account.Name = :ACCTNAME
          and RecordTypeId = :OrderManager.RECTYPE_PATIENT
      ];
      
        update new Order(
            Id = order1.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today().addDays(-1),
            Procedure_Type__c = 'Trial',
            PoDate = System.today(),
            Stage__c = 'Automated'
        );
      
      {
          NMD_OrderSubmissionManager manager = new NMD_OrderSubmissionManager(new Set<Id> { order1.Id });
          manager.validateFields(order2);
          manager.submitOrders();
      }
      Test.stopTest();

    }

     static testMethod void test_OrderSubmission_Validate_NoOrder() {
        Test.startTest();
        {
            NMD_OrderSubmissionManager manager = new NMD_OrderSubmissionManager(new Set<Id> {});
            manager.validateOrder();
        }
        Test.stopTest();
    } 

    static testMethod void test_OrderSubmission_Validate_Good() {
    
   
        Contact cont = [
            select Id
            from Contact
            where LastName = :CONTNAME
        ];  

        Order order1 = [
            select Id, Cost_Center__c
            from Order 
            where Account.Name = :ACCTNAME
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT
        ];

        List<PricebookEntry> pbes = [select Id from PricebookEntry where Pricebook2.Name = 'NMD' order by Product2Id];
        List<Inventory_Item__c> items = [select Id,Inventory_Location__c, Inventory_Location__r.Inventory_Data_ID__r.Account__r.Owner.Cost_Center_Code__c from Inventory_Item__c order by Model_Number__c];
        
            List<OrderItem> ordItems = new List<OrderItem>{};
            for (Integer i = 0; i < 1; i++) {
                ordItems.add(new OrderItem(
                    Billing_Block__c = 'ZB',
                    OrderId = order1.Id,
                    PricebookEntryId = pbes[i].Id,
                    Quantity = 1,
                    UnitPrice = 1,
                    Charge_Type__c = 'Center',
                    Is_Scanned__c = true,
                    Inventory_Source__c = items[i].Id,
                    Inventory_Location__c = items[i].Inventory_Location__c,
                    Material_Number_UPN__c = 'MatNum'+i,
                    Lot_Number__c = 'LotNum'+i,
                    Item_Status__c = 'Implanted',
                    No_Charge_Reason__c = 'None'
                ));
            }
            System.debug('**************OrdersItems*************'+ordItems);
/*            
            Virtual_Kit__c vik=new Virtual_Kit__c();
            insert vik;
            OrderItem oi=new OrderItem();
            oi.Virtual_Kit__c=vik.id;
            oi.Billing_Block__c = 'ZB';
                    oi.OrderId = order1.Id;
                    oi.PricebookEntryId = pbes[1].Id;
                    oi.Quantity = 1;
                    oi.UnitPrice = 1;
                    oi.Charge_Type__c = 'Center';
                    oi.Is_Scanned__c = true;
                    oi.Inventory_Source__c = items[1].Id;
                    oi.Inventory_Location__c = items[1].Inventory_Location__c;
                    oi.Material_Number_UPN__c = 'MatNum'+1;
                    oi.Lot_Number__c = 'LotNum'+1;
                    oi.Item_Status__c = 'Implanted';
                    oi.No_Charge_Reason__c = 'None';
            ordItems.add(oi);
*/  
           insert ordItems;
            Test.startTest();
            {         
            NMD_OrderSubmissionManager manager = new NMD_OrderSubmissionManager(new Set<Id> { order1.Id });
            manager.validateOrder();
            manager.fetchOrders(new Set<Id> { order1.Id });
            manager.consolidateOrdersItems();
            manager.reorderOrderItemIds(new Set<Id> { order1.Id });
            manager.safeInitial('Test');
            manager.safeDateFormat(system.today());
        }
        Test.stopTest();
       

    }


    static testMethod void test_OrderSubmission_Validate_Good_With_VirtualKit() {

        Contact cont = [
            select Id
            from Contact
            where LastName = :CONTNAME
        ];  

        Order order1 = [
            select Id 
            from Order 
            where Account.Name = :ACCTNAME
            and RecordTypeId = :OrderManager.RECTYPE_PATIENT
        ];

        List<PricebookEntry> pbes = [select Id from PricebookEntry where Pricebook2.Name = 'NMD' order by Product2Id];
        List<Inventory_Item__c> items = [select Id,Inventory_Location__c from Inventory_Item__c order by Model_Number__c];

        Test.startTest();
        {
            update new Order(
                Id = order1.Id,
                EffectiveDate = System.today(),
                Surgeon__c = cont.Id,
                Surgery_Date__c = System.today().addDays(-1),
                Procedure_Type__c = 'Trial',
                PoDate = System.today()
            );

            //Virtual Kit Item
            OrderItem virtualKit = new OrderItem(
                Billing_Block__c = 'ZB',
                OrderId = order1.Id,
                PricebookEntryId = pbes[0].Id,
                Quantity = 1,
                UnitPrice = 1,
                Charge_Type__c = 'Center',
                Is_Scanned__c = true,
                Inventory_Source__c = items[0].Id,
                Inventory_Location__c = items[0].Inventory_Location__c,
                Material_Number_UPN__c = 'MatNum',
                Lot_Number__c = 'LotNum',
                Item_Status__c = 'Implanted',
                No_Charge_Reason__c = 'None',
                Order_Item_Id__c = 10,
                IsVirtualKit__c = true
            );
            insert virtualKit;

            //insert Virtual Kit Items
            List<OrderItem> ordItems = new List<OrderItem>{};
            for (Integer i = 0; i < 1; i++) {
                ordItems.add(new OrderItem(
                    Billing_Block__c = 'ZB',
                    OrderId = order1.Id,
                    PricebookEntryId = pbes[i].Id,
                    Quantity = 1,
                    UnitPrice = 0,
                    Charge_Type__c = 'Center',
                    Is_Scanned__c = true,
                    Inventory_Source__c = items[i].Id,
                    Inventory_Location__c = items[i].Inventory_Location__c,
                    Material_Number_UPN__c = 'MatNum'+i,
                    Lot_Number__c = 'LotNum'+i,
                    Item_Status__c = 'Implanted',
                    No_Charge_Reason__c = 'None',
                    Order_Item_Id__c = i * 10,
                    Higher_Level_Item__c = 10,
                    Virtual_Kit__c = virtualKit.Id
                ));
            }
            insert ordItems;

            virtualKit.Lot_Number__c = 'VK555';
            virtualKit.Material_Number_UPN__c = 'VK555';
            //Rohit changed value from i<2 to i<1
            for (Integer i = 0; i < 1; i++) {
                ordItems[i].Lot_Number__c = 'LotNum' + i;
                ordItems[i].Material_Number_UPN__c = 'MatNum' + i;
            }
            //Commented by rohit and added new line below
            //update new List<OrderItem>{ virtualKit, ordItems[0], ordItems[1]};
            update new List<OrderItem>{ virtualKit, ordItems[0]};
            //commented by rohit and added line below
            //Set<Id> itemIds = new Set<Id>{ virtualKit.Id, ordItems[0].Id, ordItems[1].Id };
            Set<Id> itemIds = new Set<Id>{ virtualKit.Id, ordItems[0].Id};

            NMD_OrderSubmissionManager manager = new NMD_OrderSubmissionManager(new Set<Id> { order1.Id });
            manager.validateOrder();
            //manager.submitOrders();

        }
        Test.stopTest();
    }
    
}
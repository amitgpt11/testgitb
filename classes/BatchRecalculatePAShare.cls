/**
* @Requirement Id SFDC 405
* @Description Recalculate Private Attchment Shares based on Account Share table
* @author   Accenture services
* @date     19SEPT2016
*/
global class BatchRecalculatePAShare implements Database.Batchable<sObject>, System.Schedulable {

 
  
  String query = 
      'select '+
      'AccountId, '+
      'RowCause, '+
      'UserOrGroup.name, '+
      'UserOrGroupId '+
      'FROM AccountShare ';
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(this.query);
  }

  global void execute(Database.BatchableContext BC, List<AccountShare> scope) {
    system.debug('--scope--'+scope);
    system.debug('--scope--'+scope.size());
    set<Id> acctIdSet = new set<Id>();
    boolean isDeleted = false;
    UtilityICPIPrivateAttchShare util = new UtilityICPIPrivateAttchShare();
    PrivateAttachmentManager  PAManager = new PrivateAttachmentManager();
    map<Id,Shared_Private_Attachments__c> PAIdMap = new map<Id,Shared_Private_Attachments__c>();
    list<Shared_Private_Attachments__c> PALst = new list<Shared_Private_Attachments__c>();
    for(AccountShare a : scope){
        acctIdSet.add(a.AccountId);
    }
    for(Shared_Private_Attachments__c paObj: [
                select
                    Id,
                    Account__c,
                    ICPI_Division__c
                FROM Shared_Private_Attachments__c
                WHERE Account__c IN: acctIdSet
          ]) {
              PAIdMap.put(paObj.Id,paObj);
              PALst.add(paObj);
    }
    system.debug('--PALst--'+PALst);
    if(PAIdMap != null && PAIdMap.size()>0){
        isDeleted = util.removePrivateAttchShares(PAIdMap.keyset());
        system.debug('--isDeleted --'+isDeleted );
        if(isDeleted == true){
            util.reCalculateShareMethod(PALst,scope);
        }
    }
    
  }

  global void execute(System.SchedulableContext sc) {

    integer batchSize = Integer.valueOf(system.Label.Batch_Size_BatchRecalculatePAShare);
    
    try{
        BatchRecalculatePAShare b = new BatchRecalculatePAShare();
        database.executebatch(b,batchSize);
    }catch(Exception e){
        system.debug(e+'e');
    }

  }
  
  global void finish(Database.BatchableContext bc) {
    
  }
  
}
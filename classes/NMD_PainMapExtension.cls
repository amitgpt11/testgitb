/**
* Controller class for the vf page PainMap
*
* @Author salesforce Services
* @Date 03/25/2015
*/
global without sharing class NMD_PainMapExtension extends NMD_ExtensionBase {
  
   
  public Boolean isPatientMissing { get; private set; }
  public Patient__c Patient { get; private set; }
  String objectId= null;
  private List<Clinical_Data_Summary__c> clinicalDataSummaries = new List<Clinical_Data_Summary__c>{};
  List<Clinical_Data__c> existinClinicalDatas = new List<Clinical_Data__c> ();

  public String summaryId { get; private set; }
  public List<Map<String,String>> layers { get; private set; }
  public NMD_PainMapExtension(){
     // objectId=ApexPages.currentPage().getParameters().get('id');
  }

  public NMD_PainMapExtension(ApexPages.StandardController sc) {
    objectId= sc.getId();
    System.debug('OBJECT ID:'+objectId);
    objectId=ApexPages.currentPage().getParameters().get('id');
    System.debug('NEW OBJECT ID:'+objectId);
      System.debug('OBJECT ID:'+sc);
    isPatientMissing= false;
    this.layers = new List<Map<String,String>>{};
    List<Clinical_Data_Summary__c> cdss = null;
    //a0L - Patients
    //006 - Opportunities
    List<Opportunity> ops;
    if(objectId.startsWith('006')){
      //called from opportunity page
      //pull patient frst: 
       ops = [select id, Patient__c from Opportunity where id = :objectId limit 1];
      
      if(ops[0].patient__c!=null){
       
      cdss = new List<Clinical_Data_Summary__c>([
      select Id  
      from Clinical_Data_Summary__c
      where RecordTypeId = :ClinicalDataSummaryManager.RECTYPE_PAINMAP
      and Patient__c= :ops[0].Patient__c
      and  IsActive__c= true 
    ]);
    }else{
        isPatientMissing= true;
    }
    }else{  
      //called from patient page        
      cdss = new List<Clinical_Data_Summary__c>([
      select Id 
      from Clinical_Data_Summary__c
      where RecordTypeId = :ClinicalDataSummaryManager.RECTYPE_PAINMAP
      and Patient__c = :objectId
      and  IsActive__c= true 
    ]);
    }
    String patientId='';
    if(objectId.startsWith('006') && ops[0].patient__c!=null){
        patientId=ops[0].patient__c;
    }else{
        patientId= objectId;
    }
     List<Patient__c> patients = [select id, RecordTypeId, name from patient__c where id = :patientId];
        Patient = patients[0];
   /* 
    if(cdss.isEmpty()){
        //Creating a new Clinical Data Summary here in case they doesn't exists already on patient
        if (PatientManager.RECTYPES_NMD.contains(Patient.RecordTypeId))
        {
                this.clinicalDataSummaries.addAll(new List<Clinical_Data_Summary__c> {
                    new Clinical_Data_Summary__c(
                        Patient__c = Patient.Id,
                        isActive__C=True,
                        RecordTypeId = ClinicalDataSummaryManager.RECTYPE_PAINMAP 
                    ),
                    new Clinical_Data_Summary__c(
                        Patient__c = Patient.Id,
                        isActive__C=false,
                        RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS
                    )
                    
                });
                
                insert clinicalDataSummaries;
                for(Clinical_Data_Summary__c c: clinicalDataSummaries){
                    if(c.RecordTypeId==ClinicalDataSummaryManager.RECTYPE_PAINMAP){
                        cdss.add(c);
                    }
                }
        }
      
    }
        */
    if (!cdss.isEmpty()) {

      this.summaryId = cdss[0].Id;
      
      existinClinicalDatas = [select
          id,
          Name,
          Enabled__c,
          createddate,
          Index__c,
          Average_Pain_Intensity__c,
          Worst_Pain_Intensity__c,
          Continuous_Pain_Intensity__c,
          (select Id from Attachments)
        from Clinical_Data__c
        where Clinical_Data_Summary__c = :this.summaryId];
        DateTime dT = null;
      for (Clinical_Data__c cdata : existinClinicalDatas) {
        
        Map<String,Object> metadata = new Map<String,Object>{
          'id' => cdata.id,
          'name' => cdata.Name,
          'enabled' => cdata.Enabled__c,
          'index' => cdata.Index__c,
          'avg_pain' => cdata.Average_Pain_Intensity__c,
          'max_pain' => cdata.Worst_Pain_Intensity__c,
          'con_pain' => cdata.Continuous_Pain_Intensity__c,
          'cr_date' => cdata.createdDate.format('ddMMMyyyy')
        };
        system.debug('############ metadata'+metadata);
        this.layers.add(new Map<String,String>{
          'image_id' => cdata.Attachments[0].Id,
          
          'metadata' => JSON.serialize(metadata)
        });
      }

    }

  }

   public PageReference checkRedirect1() {
      PageReference pr;

    if (ApexPages.currentPage().getParameters().get('isdtp') != 'p1') {
     pr = Page.PainMap_Patient;
  
      pr.getParameters().put('id', objectId);  
    }
    
      return pr;
 
}
  public PageReference checkRedirect() {

    PageReference pr;
  
    if (ApexPages.currentPage().getParameters().get('isdtp') != 'p1') {
    
        system.debug('3333333333');
      pr = Page.PainMap;
    
      pr.getParameters().put('id', objectId); 
    }

    
    
    return pr;
  }
  
  
  @RemoteAction
  global static Map<String,Object> deletePainMap(Id summaryId, String painMapBase64, List<Map<String,Object>> layers) {
      Map<String,Object> result = new Map<String,Object>{
      'is_success' => false,
      'message' => 'unknown'
    };
    
    System.debug('######### Inside deletePainMap with '+layers.size()+' Layers');
    system.debug('######## layers :'+layers);
    
    
    Set<String> ids = new Set<String>();
    if(layers.size()>0){
        for (Map<String,Object> layer : layers) {
            system.debug('$$$$$$$$ keyset '+layer.keySet());
            for(String s: layer.keySet()){
                system.debug('layer.get(id) :'+layer.get(s));
                    ids.add((Id)layer.get(s));
            }
            
         }
        
        system.debug('######## PainMapIds :'+Ids);
        System.Savepoint sp = Database.setSavepoint();
        
        //remove old painmap
    List<Attachment> oldPainMap = new List<Attachment>([select Id from Attachment where ParentId = :summaryId]);
    if (!oldPainMap.isEmpty()) {
      delete oldPainMap;
    }

    //attach pain map
    List<Attachment> attachmentPMap = new List<Attachment>{
      new Attachment(
        ParentId = summaryId,
        Name = 'painmap.png',
        Body = EncodingUtil.base64Decode(painMapBase64)
      )
    };
    insert attachmentPMap;
    painMapBase64 = null;
        
    
    Set<Id> pids = new Set<Id>();   
        List<attachment> attachments = new List<attachment>([select Id, parentId from attachment where Id in :ids]);
        for(attachment a :attachments){
            pids.add(a.parentId);
        }
        List<Clinical_Data__c> oldLayers = new List<Clinical_Data__c>([select Id from Clinical_Data__c where Id in :pids]);
        if (!oldLayers.isEmpty()) {
          delete oldLayers;
        }

    result = new Map<String,Object>{
      'is_success' => true
    };      
    }


    return result;
    
      
  }
  
  
  @RemoteAction
  global static Map<String,Object> savePainMap(Id summaryId, String painMapBase64, List<Map<String,Object>> layers) {
      
    Map<String,Object> result = new Map<String,Object>{
      'is_success' => false,
      'message' => 'unknown'
    };

    //System.debug(summaryId);
    //System.debug(painMapBase64.length());
    System.debug(layers[0].get('name'));

    System.Savepoint sp = Database.setSavepoint();

    //remove old painmap
    List<Attachment> oldPainMap = new List<Attachment>([select Id from Attachment where ParentId = :summaryId]);
    if (!oldPainMap.isEmpty()) {
      delete oldPainMap;
    }

    //attach pain map
    List<Attachment> attachments = new List<Attachment>{
      new Attachment(
        ParentId = summaryId,
        Name = 'painmap.png',
        Body = EncodingUtil.base64Decode(painMapBase64)
      )
    };
    painMapBase64 = null;

    /*
    //remove old child records
    List<Clinical_Data__c> oldLayers = new List<Clinical_Data__c>([select Id from Clinical_Data__c where Clinical_Data_Summary__c = :summaryId]);
    if (!oldLayers.isEmpty()) {
      delete oldLayers;
    }
    */


    //create child records - this will always give only new pain maps
    List<Clinical_Data__c> newLayers = new List<Clinical_Data__c>{};
    for (Map<String,Object> layer : layers) {
    
        newLayers.add(new Clinical_Data__c(
        Clinical_Data_Summary__c = summaryId,
        Name = String.valueOf(layer.get('name')),
        Enabled__c = Boolean.valueOf(layer.get('enabled')),
        Index__c = Decimal.valueOf(String.valueOf(layer.get('index'))),
        Average_Pain_Intensity__c = Decimal.valueOf(String.valueOf(layer.get('avg_pain'))),
        Worst_Pain_Intensity__c = Decimal.valueOf(String.valueOf(layer.get('max_pain'))),
        Continuous_Pain_Intensity__c = Decimal.valueOf(String.valueOf(layer.get('con_pain')))
      ));
      
    }
    if(!newLayers.isEmpty())
        insert newLayers;

    //attach images
    for (Integer i = 0; i < layers.size(); i++) {
        Map<String,Object> layer =  layers[i];
        
          
          Id cdataId = newLayers[i].Id;

          attachments.add(new Attachment(
            ParentId = cdataId,
            Name = 'layer.png',
            Description = String.valueOf(layer.get('name')),
            Body = EncodingUtil.base64Decode(String.valueOf(layer.get('image')))
          ));
        
    }
    
    if(!attachments.isEmpty())
    insert attachments;

    result = new Map<String,Object>{
      'is_success' => true
    };

    return result;

  }


}
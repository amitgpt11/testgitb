/**
* Manager class for Account object
*
* 03/07/2016 - Mike Lankfer - Updated per US2290.  Refactored formatNaming (Account acct) method to remove NULL values from standarized account names
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public class AccountManager {
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Account.getRecordTypeInfosByName();
    static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();

    public static final Id RECTYPE_CONSIGNMENT = RECTYPES.get('Consignment Inventory').getRecordTypeId();
    public static final Id RECTYPE_CUSTOMER = RECTYPES.get('Customer').getRecordTypeId();
    public static final Id RECTYPE_INSPROVIDER = RECTYPES.get('Insurance Provider').getRecordTypeId();
    public static final Id RECTYPE_PROSPECT = RECTYPES.get('Prospect').getRecordTypeId();
    
    private List<Inventory_Data__c> idObjectList = new List<Inventory_Data__c>();
    private Map<Id, Inventory_Data__c> currentIdObjectMap = new Map<Id, Inventory_Data__c>();
    private Map<Id, Personnel_ID__c> personnelObjectMap = new Map<Id, Personnel_ID__c>();
    private Set<Id> updatedSAPIDs = new Set<Id>();

    private NMD_AutoSubmitManager submitManager = null;

    /**
    * Initialize the NMD_AutoSubmitManager if there are records that need to be validated and submitted
    * 
    * @param void
    */  
    public void initSubmissionManager(){
        List<SObject> accountProspects = new List<SObject>();
        for(SObject obj : trigger.new){
            Account acct = (Account)obj;
            if(acct.RecordTypeId == RECTYPE_PROSPECT
                && acct.Sent_to_Quincy__c == false
            ){
                accountProspects.add(obj);
            }
        }

        if(!accountProspects.isEmpty()){
            submitManager = new NMD_AutoSubmitManager();
            submitManager.init(Account.getSObjectType(), accountProspects);
        }
    }

    /**
    * If the NMD_AutoSubmitManager is initialized, check the provided record to see if it is valid
    * 
    * @param SObject obj
    */  
    public void checkForAutoSubmit(SObject oldObj, SObject obj){
        Account oldAccount = (Account) oldObj;
        if(submitManager != null
            && oldAccount.Sent_to_Quincy__c != true
        ){
            submitManager.validateRecord(obj);
        }
    }

    /**
    * If the NMD_AutoSubmitManager is initialized, submit all valid records to quincy
    * 
    * @param void
    */  
    public void autoSubmitRecords(){
        if(submitManager != null){
            submitManager.queueQuincyEmails();
        }
    }


    /**
    * Fetch the Inventory Data Objects for the List of Accounts
    * 
    * @param accountIds Account Id Set
    */    
    public void fetchInventoryDataForAccounts(Set<Id> accountIds) {
        
        for (Inventory_Data__c idObj : [
            select 
                Name,
                OwnerId, 
                Account__c
            from Inventory_Data__c 
            where Account__c in :accountIds
        ]) {
            currentIdObjectMap.put(idObj.Account__c, idObj);
        }
    }

    /**
    * Fetch the Inventory Data Objects for the List of Accounts
    * 
    * @param accounts Account Object List
    */    
    public void fetchPersonnelIdRecords(List<Account> accounts){
        Set<Id> pidSet = new Set<Id>();
        for(Account acc : accounts){
            if(acc.RecordTypeId == RECTYPE_CONSIGNMENT && acc.Personnel_ID__c != null){
                pidSet.add(acc.Personnel_ID__c);
            }
        }

        if(!pidSet.isEmpty()){
            for(Personnel_ID__c pid : [
                SELECT
                    Name,
                    Personnel_ID__c,
                    User_for_account_team__c
                FROM Personnel_ID__c
                WHERE Id IN :pidSet
                AND User_for_account_team__c != null
            ]){
                personnelObjectMap.put(pid.Id, pid);
            }
        }
    }

    /**
    * Fetch the Inventory Data Objects for the List of Accounts
    * 
    * @param a Account Being Operated on
    */    
    public void updateConsignmentAccountOwnerWithPersonnelID(Account a){
        If(a.RecordTypeId == RECTYPE_CONSIGNMENT 
            && a.Personnel_ID__c != null
            && personnelObjectMap.containsKey(a.Personnel_ID__c)){

            a.OwnerId = personnelObjectMap.get(a.Personnel_ID__c).User_for_account_team__c;
        }
    }

    /**
    * Create Inventoy Data for accounts who have changed record type to Consignment Inventory
    * 
    * @param oldAcct Old Account Object
    * @param newAcct Current Account Object
    */    
    public void createInventoryDataForAccounts(Account oldAcct, Account newAcct) {

        if (oldAcct.RecordTypeId != newAcct.RecordTypeId) {
            createInventoryDataForAccounts(newAcct);
        }

    }
    
    /**
    * Create new Inventory Data when an Consignment Inventory Account is created
    *
    * @param a Account Being Operated on    
    */
    public void createInventoryDataForAccounts(Account a) {

        if (a.RecordTypeId == RECTYPE_CONSIGNMENT && !this.currentIdObjectMap.containsKey(a.Id)) {
            //create custom obj

            idObjectList.add(new Inventory_Data__c(
                OwnerId = a.OwnerId,
                Account__c = a.Id,
                Name = a.Name,
                Customer_Class__c = a.Classification__c
            ));
        }

    }

    /**
    * Update new Inventory Data when an Consignment Inventory Account is updates
    *
    * @param oldAccount Old Account Object
    * @param account    Current Account Object      
    */
    public void updateInventoryDataForAccounts(Account oldAccount, Account account) {

        if (account.RecordTypeId == RECTYPE_CONSIGNMENT && currentIdObjectMap.containsKey(account.Id)) {
            //update custom obj
            Inventory_Data__c invData = currentIdObjectMap.get(account.Id);
            invData.OwnerId = account.OwnerId;
            invData.Customer_Class__c = account.Classification__c;

            idObjectList.add(invData);
        }

    }

    public void checkSAPID(Account oldAcct, Account newAcct) {
        if (oldAcct.Account_Number__c != newAcct.Account_Number__c) {
            this.updatedSAPIDs.add(newAcct.Id);
        }
    }

    public void formatNaming(Account oldAcct, Account newAcct) {

        // if the name has changed, or the recordtype has changed to customer, or the account
        // is a propect and Sold to address changed, or is a Customer and Shipping address has changed
        if (oldAcct.Name != newAcct.Name
         || (newAcct.RecordTypeId == RECTYPE_PROSPECT && newAcct.Account_Number__c != null)
         //|| (oldAcct.RecordTypeId != newAcct.RecordTypeId && newAcct.RecordTypeId == RECTYPE_CUSTOMER)
         || (newAcct.RecordTypeId == RECTYPE_PROSPECT
          && (oldAcct.Sold_to_City__c != newAcct.Sold_to_City__c
           || oldAcct.Sold_to_State__c != newAcct.Sold_to_State__c))
         || (newAcct.RecordTypeId == RECTYPE_CUSTOMER
          && (oldAcct.ShippingCity != newAcct.ShippingCity
           || oldAcct.ShippingState != newAcct.ShippingState
           || oldAcct.Account_Number__c != newAcct.Account_Number__c))
        ) {
            formatNaming(newAcct);
        }

    }

    public void formatNaming(Account acct) {
        
        if (acct.RecordTypeId != RECTYPE_CUSTOMER && acct.RecordTypeId != RECTYPE_PROSPECT) return;

        //Integer loopCondition = acct.RecordTypeId == RECTYPE_CUSTOMER ? 3 : 2;
        Integer loopCondition = acct.RecordTypeId == RECTYPE_CUSTOMER ? 1 : 0;

        String sep = ' - ';
        List<String> parts = new List<String> { acct.Name };
        if (acct.Id != null) {
            parts = acct.Name.split(sep);
            /*while (parts.size() > loopCondition) {
                parts[0] += sep + parts.remove(1);
            }*/
            
            if(parts.size() > 1){
                for(Integer i = 1; i < parts.size(); i++){
                    if(i < parts.size()){
                        if(parts[i].contains(', ') && (i == (parts.size() - 1 - loopCondition))){
                            break;
                        }
                        parts[0] += sep + parts[i];
                    }
                }
            }
        }

        //Prospect: Test - Test - Test - city, state

        //Customer: Test - Test - Test - city, state - 12345

        acct.Name = parts[0];
        acct.Mailing_Name__c = parts[0];

        if (acct.RecordTypeId == RECTYPE_CUSTOMER
            || (acct.RecordTypeId == RECTYPE_PROSPECT && acct.Account_Number__c != null)
        ) {
            
            //  3/7/2016 - Mike Lankfer - Added logic to identify and remove NULL values from the standard naming format
            if ((acct.ShippingCity != null) && (acct.ShippingState != null) && (acct.Account_Number__c != null)){
                acct.Name = String.format('{0}{1}{2}, {3}{1}{4}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity != null) && (acct.ShippingState != null) && (acct.Account_Number__c == null)){
                acct.Name = String.format('{0}{1}{2}, {3}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity != null) && (acct.ShippingState == null) && (acct.Account_Number__c == null)){
                acct.Name = String.format('{0}{1}{2}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity == null) && (acct.ShippingState == null) && (acct.Account_Number__c == null)){
                acct.Name = String.format('{0}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity == null) && (acct.ShippingState != null) && (acct.Account_Number__c != null)){
                acct.Name = String.format('{0}{1}{3}{1}{4}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity == null) && (acct.ShippingState == null) && (acct.Account_Number__c != null)){
                acct.Name = String.format('{0}{1}{4}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity == null) && (acct.ShippingState != null) && (acct.Account_Number__c == null)){
                acct.Name = String.format('{0}{1}{3}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }
            
            else if ((acct.ShippingCity != null) && (acct.ShippingState == null) && (acct.Account_Number__c != null)){
                acct.Name = String.format('{0}{1}{2}{1}{4}', new List<String> {
                    parts[0], sep, acct.ShippingCity, acct.ShippingState, acct.Account_Number__c
                        });
            }            
            
        }
        else if (acct.RecordTypeId == RECTYPE_PROSPECT
         && String.isNotBlank(acct.Sold_to_City__c)
         && String.isNotBlank(acct.Sold_to_State__c)
        ) {
            acct.Name = String.format('{0}{1}{2}, {3}', new List<String> {
                parts[0], sep, acct.Sold_to_City__c, acct.Sold_to_State__c
            });
        }

    }

    public void cascadeSAPIDInfo() {
        if (this.updatedSAPIDs.isEmpty()) return;

        List<Opportunity> opptys = new List<Opportunity>();
        Set<Id> orderIds = new Set<Id>();
        for (Opportunity oppty : [
            select 
                Trialing_Account__c,
                Trialing_Account__r.Account_Number__c,
                Trialing_Account_SAP_ID__c,
                Procedure_Account__c,
                Procedure_Account__r.Account_Number__c,
                Procedure_Account_SAP_ID__c,
                (
                    select Id
                    from Orders
                    where Stage__c = 'Pending SAP ID'
                )
            from Opportunity
            where Trialing_Account__c in :this.updatedSAPIDs
            or Procedure_Account__c in :this.updatedSAPIDs
        ]) {

            Boolean hasUpdate = false;

            if (this.updatedSAPIDs.contains(oppty.Trialing_Account__c)
             && oppty.Trialing_Account_SAP_ID__c != oppty.Trialing_Account__r.Account_Number__c
            ) {
                oppty.Trialing_Account_SAP_ID__c = oppty.Trialing_Account__r.Account_Number__c;
                hasUpdate = true;
            }

            if (this.updatedSAPIDs.contains(oppty.Procedure_Account__c)
             && oppty.Procedure_Account_SAP_ID__c != oppty.Procedure_Account__r.Account_Number__c
            ) {
                oppty.Procedure_Account_SAP_ID__c = oppty.Procedure_Account__r.Account_Number__c;
                hasUpdate = true;
            }

            if (hasUpdate) {
                opptys.add(oppty);

            }

            if (!oppty.Orders.isEmpty()) {
                orderIds.addAll(new Map<Id,Order>(oppty.Orders).keySet());
            }

        }

        if (!opptys.isEmpty()) {
            //update opptys;
            DML.save(this, opptys, false);
        }
        
        // Uncommented for SFDC 4305
        if (!orderIds.isEmpty()) {
            OrderManager.confirmPatientOrders(orderIds); // @future
        }

    }
    
    /**
    * Upserts in bulk for InventoryDataObjects
    * 
    * @param void
    */
    public void commitInventoryDataObjects() {

        if (this.idObjectList.isEmpty()) {
            return;
        }
        if(1==1)
        {
        }
        ////Insert new obj
        //upsert this.idObjectList;
        //List<Database.UpsertResult> inventoryDataObjectShareSaveResults = Database.upsert(this.idObjectList, false);
        //for (Database.UpsertResult result: inventoryDataObjectShareSaveResults) {
        //    if (!result.isSuccess()) {
        //        for (Database.Error err: result.getErrors()) {
        //            logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Upsert of inventoryDataObjectShareSaveResults in AccountManager.commitInventoryDataObjects'));
        //        }
        //    }
        //}
        DML.save(this, this.idObjectList, false);

    }   

}
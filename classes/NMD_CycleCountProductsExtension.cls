/**
* Extension class for the CycleCountProducts vf page
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class NMD_CycleCountProductsExtension extends NMD_ExtensionBase {

    @TestVisible
    final static Map<String,String> LEVELS = new Map<String,String> {
        'L1' => 'Level_1__c',
        'L2' => 'Level_2__c',
        'L3' => 'Level_3__c',
        'L4' => 'Level_4__c',
        'L5' => 'Level_5__c'
    };
    final static Integer PAGESIZE = 20;
    final static Id RECTYPE_CYCLECOUNT = Schema.SObjectType.List__c.getRecordTypeInfosByName().get('Cycle Count Product').getRecordTypeId();

    final ApexPages.StandardController sc;

    private Set<Id> savedProdIds;

    public Boolean canEdit { get; private set; }
    public String searchString { get; set; }
    public String searchType { get; set; }
    public List<SelectOption> searchTypes { get; private set; }
    public List<SelectOption> availableItems { get; private set; }
    public List<SelectOption> selectedItems { get; private set; }
    public List<ListItem> savedItems { get; private set; }
    public Integer currentPage { get; private set; }
    public Integer maxPage { 
        get { return Math.ceil(this.savedProdIds.size() / PAGESIZE).intValue(); } 
    }

    public Boolean orderAsc { get; set; }
    public String orderBy { 
        get; 
        set {
            if (orderBy != value) {
                this.currentPage = 0;
                this.orderAsc = true;
            }
            orderBy = value;
        } 
    }

    public NMD_CycleCountProductsExtension(ApexPages.StandardController sc) {
        this.sc = sc;
        this.canEdit = ((Cycle_Count__c)sc.getRecord()).Status__c == 'Draft';
        this.availableItems = new List<SelectOption>{};
        this.selectedItems = new List<SelectOption>{};
        this.searchString = '12';
        this.searchType = 'L3';
        this.currentPage = 0;
        this.savedProdIds = new Set<Id>{};
        this.savedItems = new List<ListItem>{};
        
        this.searchTypes = new List<SelectOption>{};
        for (String key : LEVELS.keySet()) {
            this.searchTypes.add(new SelectOption(key, key));
        }
        this.searchTypes.sort();

        this.orderBy = 'Model_Number__r.Name';
        this.orderAsc = true;

        fetchSavedItems();

        if (this.canEdit) {
            // load initial search state
            doSearch();
        }

    }

    public void doSearch() {

        String searchField = LEVELS.get(this.searchType);
        String whereClause = '';

        for (String str : this.searchString.split(',')) {
            whereClause += String.format('{0} {1} like {2} ', new List<String> {
                (whereClause.length() == 0 ? 'where ' : 'or '),
                searchField,
                ('\'' + str.trim().replaceAll('\\*', '') + '%\' ')
            });
        }

        Set<String> values = new Set<String>{};
        this.availableItems = new List<SelectOption>{};
        for (Product2 prod : Database.query(
            'select ' +
                searchField + ' ' +
            'from Product2 ' +
            whereClause + 
            'order by ' + searchField
        )) {
            String val = this.searchType + ': ' + String.valueOf(prod.get(searchField));
            if (values.add(val)) {
                
                if (this.availableItems.size() == 100) {
                    newWarningMessage('Your search returned more than 100 rows. Only the first 100 are displayed. Please refine search criteria.');
                    break;
                }

                this.availableItems.add(new SelectOption(val, val));

            }

        }

    }

    public void firstPage() {
        this.currentPage = 0;
        fetchSavedItems();
    }

    public void prevPage() {
        this.currentPage = this.currentPage == 0 ? 0 : this.currentPage - 1;
        fetchSavedItems();
    }

    public void nextPage() {
        this.currentPage = this.currentPage == this.maxPage ? this.currentPage : this.currentPage + 1;
        fetchSavedItems();
    }

    public void lastPage() {
        this.currentPage = this.maxPage;
        fetchSavedItems();
    }

    public void addSelectedItems() {

        if (this.selectedItems.isEmpty()) {
            newErrorMessage('Please select at least one Available item.');
            return;
        }

        // take the possible distinct levels and store each in their own array
        Map<String,List<String>> types = new Map<String,List<String>>{};
        for (SelectOption so : this.selectedItems) {

            List<String> values = so.getValue().split(':');
            String level = values[0].trim();
            String value = values[1].trim();

            if (types.containsKey(level)) {
                types.get(level).add(value);
            }
            else {
                types.put(level, new List<String> { value });
            }

        }

        // now for each distinct level, turn its valid values into a string like "Level_N__c in ('x', 'y', 'z')"
        List<String> criteria = new List<String>{};
        for (String key : types.keySet()) {

            String arr = '';
            for (String criterion : types.get(key)) {
                arr += (arr.length() == 0 ? '\'' : ',\'') + criterion + '\'';
            }
            arr = '(' + arr + ')';

            criteria.add(String.format('{0} in {1} ', new List<String> {
                LEVELS.get(key),
                arr
            }));

        }

        // group all the value clauses together like "(A or B or C)"
        String levelsClause = '';
        for (String clause : criteria) {
            levelsClause += (levelsClause.length() == 0 ? '' : 'or ') + clause;
        }
        levelsClause = '(' + levelsClause + ')';

        // find products that match the possible selected values, and are not already present
        List<List__c> newLists = new List<List__c>{};
        for (Product2 prod : Database.query(
            'select Id ' +
            'from Product2 ' +
            'where Id not in :savedProdIds ' +
            'and ' + levelsClause
        )) {
            newLists.add(new List__c(
                Cycle_Count__c = this.sc.getId(),
                Model_Number__c = prod.Id,
                RecordTypeId = RECTYPE_CYCLECOUNT
            ));
        }

        if (!newLists.isEmpty()) {
            //insert newLists;
            DML.save(this, newLists);
        }

        fetchSavedItems();

        this.selectedItems.clear();
        doSearch();

        newInfoMessage(String.format('{0} Product{1} added.', new List<String> { 
            String.valueOf(newLists.size()),
            newLists.size() == 1 ? '' : 's'
        }));

    }

    public void removeSelectedItems() {

        List<List__c> oldLists = new List<List__c>{};
        for (ListItem item : this.savedItems) {
            if (item.selected) {
                oldLists.add(item.record);
            }
        }

        if (!oldLists.isEmpty()) {
            //delete oldLists;
            DML.remove(this, oldLists);
        }

        newInfoMessage(String.format('{0} Product{1} removed.', new List<String> { 
            String.valueOf(oldLists.size()),
            oldLists.size() == 1 ? '' : 's'
        }));

        firstPage();

    }

    public void fetchSavedItems() {

        this.savedItems = new List<ListItem>{};
        this.savedProdIds = new Set<Id>{};

        Integer index = 0;
        Integer offset = this.currentPage * PAGESIZE;
        Id cycleCountId = this.sc.getId();

        for (List__c item : Database.query(
            'select ' +
                'Model_Number__c, ' +
                'Model_Number__r.Name, ' +
                'Model_Number__r.ProductCode, ' +
                'Model_Number__r.Description, ' +
                'Model_Number__r.Family, ' +
                'Model_Number__r.Level_5__c ' +
            'from List__c ' +
            'where Cycle_Count__c = :cycleCountId ' +
            'and RecordTypeId = :RECTYPE_CYCLECOUNT ' +
            'order by ' + this.orderBy + (this.orderAsc ? ' asc ' : ' desc ')
        )) {

            // have to query all so that all records Ids can be stored.  paginate progmatically
            if (index >= offset && index <= offset + PAGESIZE - 1) {
                this.savedItems.add(new ListItem(item));
            }
            this.savedProdIds.add(item.Model_Number__c);

        }

    }

    public class ListItem {

        public List__c record { get; private set; }
        public Boolean selected { get; set; }

        public ListItem(List__c item) {
            this.record = item;
        }

    }
    
}
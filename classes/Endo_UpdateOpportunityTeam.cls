global class Endo_UpdateOpportunityTeam implements Database.Batchable<sObject>
{
    public string EndoCapitalBusinessRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Endo Capital Business').getRecordTypeId();
    public string EndoNewDisposableBusinessRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Endo New Disposable Business').getRecordTypeId();
        
    public Set<Opportunity> oppySet = new Set<Opportunity>();
    public List<Opportunity> oppyList = new List<Opportunity>();
    
    public Set<String> secondaryTerrOnOpptyList =  new Set<String>();
    public Set<String> parentSecondaryTerrOnOpptyList =  new Set<String>();
    
    public Set<String> primaryTerrOnOpptyList =  new Set<String>();
    public Set<String> parentprimaryTerrOnOpptyList =  new Set<String>();
    public Set<String> parentToParentprimaryTerrOnOpptyList =  new Set<String>();
    
    public List<UserTerritory2Association> territoryUsersList = new List<UserTerritory2Association>();
    
    /*public List<OpportunityShare> allOpptyTeamRecordsList = new List<OpportunityShare>();
    public List <OpportunityShare> delOpptyTeamList = new List<OpportunityShare>();
    public Set<OpportunityShare> delOpptyTeamSet = new Set<OpportunityShare>(); */   
    
    public List<OpportunityTeamMember> allOpptyTeamRecordsList = new List<OpportunityTeamMember>();
    public List <OpportunityTeamMember> delOpptyTeamList = new List<OpportunityTeamMember>();
    public Set<OpportunityTeamMember> delOpptyTeamSet = new Set<OpportunityTeamMember>();    
    
    public List<OpportunityTeamMember> oppTeam = new List<OpportunityTeamMember>();
    
    /** Added 11/22/16 JD - Manual Shares **/
    public List<OpportunityShare> allOpptyShareRecordsList = new List<OpportunityShare>();
    public List <OpportunityShare> delOpptyShareList = new List<OpportunityShare>();
    public Set<OpportunityShare> delOpptyShareSet = new Set<OpportunityShare>();    
    
    public List<OpportunityShare> oppShare = new List<OpportunityShare>();
        
    public String query = 'SELECT id, RecordTypeId , ENDO_Secondary_Territory__c , Parent_Of_Secondary_Territory__c , Territory2Id , GI_Parent__c , GI_Parent_Parent__c, PULM_Parent__c, PULM_Parent_Parent__c , LastModifiedDate FROM Opportunity Where RecordTypeId =\''+EndoCapitalBusinessRecordTypeId+'\' OR RecordTypeId =\''+EndoNewDisposableBusinessRecordTypeId+'\' ORDER BY LastModifiedDate ASC';
    
    // Constructor
    global Endo_UpdateOpportunityTeam ()
    {} 
    /****************************************************************
*  start(Database.BatchableContext BC)
*****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('Inside Strat method');
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
*  execute(Database.BatchableContext BC, List scope)
**********************************************************************/
    global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {   
        system.debug('**scope**'+scope.Size());
        if(scope != null){
            for(Opportunity oppy : scope){
                //dateTime last = oppy.LastModifiedDate;
                //if(date.newinstance(last.year(), last.month(), last.day()) == date.Today()){
                    if(oppy.RecordTypeId == EndoCapitalBusinessRecordTypeId || oppy.RecordTypeId == EndoNewDisposableBusinessRecordTypeId){
                        primaryTerrOnOpptyList.add(oppy.Territory2Id);
                        parentprimaryTerrOnOpptyList.add(oppy.GI_Parent__c);
                        parentToParentprimaryTerrOnOpptyList.add(oppy.GI_Parent_Parent__c);
                        
                        secondaryTerrOnOpptyList.add(oppy.ENDO_Secondary_Territory__c);
                        parentSecondaryTerrOnOpptyList.add(oppy.PULM_Parent__c);
                        
                        oppySet.add(oppy);
                    }
                //}
            }
            if(oppySet != null){
                oppyList.addAll(oppySet);
            }
            system.debug('****Opportunity Today***'+oppySet);
            
            if(!primaryTerrOnOpptyList.isEmpty() || !parentprimaryTerrOnOpptyList.isEmpty() || !parentToParentprimaryTerrOnOpptyList.isEmpty() || !secondaryTerrOnOpptyList.isEmpty() || !parentSecondaryTerrOnOpptyList.isEmpty())          
            {
                territoryUsersList = [SELECT Id,RoleInTerritory2,Territory2Id,UserId, User.Name, Territory2.Name
                                     FROM UserTerritory2Association 
                                     WHERE Territory2.Name =: secondaryTerrOnOpptyList OR Territory2.Name =: parentSecondaryTerrOnOpptyList 
                                           OR Territory2Id =: primaryTerrOnOpptyList OR Territory2Id =: parentprimaryTerrOnOpptyList OR Territory2Id =: parentToParentprimaryTerrOnOpptyList];
                system.debug('***territoryUsersList ***'+territoryUsersList);
            }
            
            if(!oppyList.isEmpty()){
                allOpptyTeamRecordsList = [Select OpportunityId,UserId,TeamMemberRole,OpportunityAccessLevel From OpportunityTeamMember Where TeamMemberRole='Territory Manager' OR TeamMemberRole='Regional Manager'];
                allOpptyShareRecordsList = [SELECT Id,OpportunityAccessLevel,OpportunityId,RowCause,UserOrGroupId FROM OpportunityShare WHERE OpportunityId =: oppyList AND RowCause = 'Manual' AND OpportunityAccessLevel='Edit'];
                system.debug('***allOpptyTeamRecordsList***'+allOpptyTeamRecordsList);
            }
            
        }
        
        // Delete Opportunity Team Code Starts
        if(!allOpptyTeamRecordsList.isEmpty() && !territoryUsersList.isEmpty()){
            //for(OpportunityShare delOpshare: allOpptyTeamRecordsList){
            for(OpportunityTeamMember delOpshare: allOpptyTeamRecordsList){
                //for(UserTerritory2Association tu: territoryUsersList){
                    //if(delOpshare.UserOrGroupId == tu.UserId){
                        delOpptyTeamSet.add(delOpshare);
                    //}
                //}
            }
            if(delOpptyTeamSet != null){
                delOpptyTeamList.addAll(delOpptyTeamSet);
            }
            
            system.debug('terrList'+delOpptyTeamList);
            if(!delOpptyTeamList.isEmpty()){
                DML.evaluateResults(this, Database.Delete(delOpptyTeamList, false));
            }
        }
        // Delete Opportunity Team Code Ends

        // Delete Opportunity Share Code Starts **Added 11-22-16 JD
        if(!allOpptyShareRecordsList.isEmpty() && !territoryUsersList.isEmpty()){
            //for(OpportunityShare delOpshare: allOpptyShareRecordsList){
            for(OpportunityShare delOpshare: allOpptyShareRecordsList){
                //for(UserTerritory2Association tu: territoryUsersList){
                    //if(delOpshare.UserOrGroupId == tu.UserId){
                        delOpptyShareSet.add(delOpshare);
                    //}
                //}
            }
            if(delOpptyShareSet != null){
                delOpptyShareList.addAll(delOpptyShareSet);
            }
            
            system.debug('terrList'+delOpptyShareList);
            if(!delOpptyShareList.isEmpty()){
                DML.evaluateResults(this, Database.Delete(delOpptyShareList, false));
            }
        }
        // Delete Opportunity Share Code Ends
                
        // Insert Opportunity Team Code Starts 
        if(!territoryUsersList.isEmpty()){ 
            for(Opportunity op: oppyList){
                for(UserTerritory2Association tu: territoryUsersList){
                    if(op.Territory2Id == tu.Territory2Id || op.GI_Parent__c == tu.Territory2Id || op.GI_Parent_Parent__c == tu.Territory2Id || op.ENDO_Secondary_Territory__c == tu.Territory2.Name || op.PULM_Parent__c == tu.Territory2.Name){
                    if(tu.RoleInTerritory2=='Territory Manager'){
                        OpportunityTeamMember otm = new OpportunityTeamMember(OpportunityId = op.Id,
                        UserId = tu.UserId,
                        TeamMemberRole = 'Territory Manager',
                        OpportunityAccessLevel = 'Edit');
                        oppTeam.add(otm);
                    }
                    if(tu.RoleInTerritory2=='Regional Manager'){
                        OpportunityTeamMember otm1 = new OpportunityTeamMember(OpportunityId = op.Id,
                        UserId = tu.UserId,
                        TeamMemberRole = 'Regional Manager',
                        OpportunityAccessLevel = 'Edit');
                        oppTeam.add(otm1);
                    }
                    }
                }

            }
        }

        if (oppTeam.size() > 0) { 
            DML.evaluateResults(this, Database.insert(oppTeam, false));
            oppTeam.clear(); 
        }
        
    }
    
    /****************************************************
*  finish(Database.BatchableContext BC)
*****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Inside Finish method');
    }
}
/**
* Trigger handler for the Patient__c object
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class PatientTriggerHandler extends TriggerHandler {
    
    final PatientManager manager = new PatientManager();

    public override void bulkBefore() {

        manager.findTerritories();

        if(trigger.isUpdate){
            manager.initSubmissionManager();
        }

    }
    
    public override void bulkAfter() {
        if (trigger.isInsert) {
            manager.patientSharing(trigger.new);
        }

    }
    
    public override void beforeInsert(SObject so) {
        
        Patient__c patient = (Patient__c)so;

        manager.assignTerritory(patient);
        manager.formatPhone(patient);

    }

    public override void afterInsert(SObject so) {

        Patient__c patient = (Patient__c)so;

        manager.createNmdLead(patient);
        manager.createClinicalDataSummary(patient);

    }
    
    public override void beforeUpdate(SObject oldObj, SObject obj) {

        Patient__c oldPatient = (Patient__c)oldObj;
        Patient__c patient = (Patient__c)obj;
        manager.updatePatientOwner(oldPatient, patient);
        manager.formatPhone(patient);
        manager.checkForAutoSubmit(oldObj, obj);

    }

    public override void afterUpdate(SObject oldObj, SObject newObj) {

        Patient__c oldPatient = (Patient__c)oldObj;
        Patient__c newPatient = (Patient__c)newObj;
        manager.checkSAPID(oldPatient, newPatient);

    }

    public override void andFinally() {

        manager.commitNmdLeads();
        manager.cascadeSAPIDInfo();
        manager.commitClinicalDataSummaries();

        if(trigger.isBefore && trigger.isUpdate){
            manager.autoSubmitRecords();
        }

    }


}
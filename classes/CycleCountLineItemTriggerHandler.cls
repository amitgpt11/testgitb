/**
* trigger handler class for the Cycle_Count_Line_Item object
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class CycleCountLineItemTriggerHandler extends TriggerHandler {
	
	final CycleCountLineItemManager manager = new CycleCountLineItemManager();

	public override void bulkBefore() {

		manager.fetchMatchingRecords( (List<Cycle_Count_Line_Item__c>)trigger.new );

	}

	public override void beforeInsert(SObject obj) {

		manager.matchRelatedRecords( cast(obj) );

	}

	public override void beforeUpdate(SObject oldObj, SObject newObj) {

		manager.matchRelatedRecords( cast(newObj) );

	}

	private Cycle_Count_Line_Item__c cast(SObject obj) {
		return (Cycle_Count_Line_Item__c)obj;
	}

}
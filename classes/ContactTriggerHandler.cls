/**
* Handler class for the Contact trigger
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class ContactTriggerHandler extends TriggerHandler {

    final ContactManager manager = new ContactManager();

    public override void bulkBefore() {
        List<Contact> contacts = (List<Contact>)trigger.new;
        if(trigger.isUpdate){
        manager.fetchPatientCounts(contacts);
        }
        manager.fetchCurrentProfileName();
        manager.fetchEUContact(contacts); // SFDC-1317: Shashank Gupta
        manager.assignDefaultRecordTypeFrRIVA(contacts); //SFDC 4780 - Q4 2016
    }
    
    public override void bulkAfter() {

        List<Contact> contacts = (List<Contact>)trigger.new;

        manager.fetchPatientCounts(contacts);
        manager.fetchContactsOwnerManager(contacts);
        manager.fetchContactsAccounts(contacts);

        if (trigger.isInsert) {
            manager.contactSharing(contacts);
        }

        if(trigger.isUpdate){
            manager.initSubmissionManager();
        }

    }
    
    // SFDC-1317: Shashank Gupta
    public override void beforeInsert(SObject obj) {
        Contact contact = (Contact)obj;
        manager.contactDivisionDetailInsert(contact);
    }

    public override void beforeUpdate(SObject oldObj, SObject obj) {
        Contact contact = (Contact)obj;
        Contact oldContact = (Contact)oldObj;
        manager.checkContact(oldContact, contact);
        manager.contactDivisionDetailUpdate(oldContact, contact);// SFDC-1317: Shashank Gupta
    }

    public override void afterUpdate(SObject oldObj, SObject obj) {
        Contact contact = (Contact)obj;
        Contact oldContact = (Contact)oldObj;
        manager.checkContactAndSendEmail(oldContact, contact);
        manager.checkSAPID(oldContact, contact);
        manager.checkForAutoSubmit(oldObj, obj);
    }

    public override void andFinally() {
        //manager.sendEmailListFuture();
        manager.sendEmailList();
        manager.cascadeSAPIDInfo();

        if(trigger.isAfter && trigger.isUpdate){
            manager.autoSubmitRecords();
        }

    }
}
/*
This class will be used to identify user profile to show right Tableau report based on user profile and account number
Modified On : 02NOV2016
Modified By : Susannah St-Germain
Modification comments : Added new link finalLinkforanzfilterSF1 support tableau reports in SF1   
*/

public class EndoTableauReportExtension{
    public Profile currentuserProfile{get;set;}
    public string SAPAccountId{get;set;}
    public string UppercaseMailingName{get;set;}
    public boolean isError{get;set;}    
    Public string finalLink {get; set;}
    Public string finalLinkforanzusers {get; set;}
    Public string finalLinkforanzfilterSF1 {get; set;}
    list<Account_Performance__c> ApRecords = new list<Account_Performance__c>();
    Id CurrentRecordId ;
    public string str {get; set;}
     
  public EndoTableauReportExtension(ApexPages.StandardController controller){
     CurrentRecordId = Apexpages.currentPage().getParameters().get('Id');
     currentuserProfile=new Profile();
     currentuserProfile=[Select Id,Name from Profile where Id=:userinfo.getProfileId()];
     getTablueLinkInfo();
  } 
  
    public void getTablueLinkInfo(){
        ApRecords = [SELECT Id,name,Facility_Name__c,Facility_Name__r.Account_Number__c,Account_SAP_ID__c, Shared_Uppercase_Mailing_Name__c FROM Account_Performance__c WHERE Id =: CurrentRecordId and Facility_Name__r.Account_Number__c != null]; //query sap account number of current account        
        If(!ApRecords.isEmpty()){
            SAPAccountId = ApRecords[0].Account_SAP_ID__c;
            UppercaseMailingName = ApRecords[0].Shared_Uppercase_Mailing_Name__c;    
                //finalLink = Label.EndoTableauReport+SAPAccountId+Label.EndoTableauReportEnd;
                finalLink = Label.EndoTableauReport+SAPAccountId;
                finalLinkforanzusers=Label.ANZ_Sales_Transactional_History_Report+UppercaseMailingName+'  - '+SAPAccountId;
                finalLinkforanzfilterSF1 =UppercaseMailingName+'  - '+SAPAccountId;
                isError = false;
                system.debug('finalLink ****'+finalLink);
                system.debug('finalLinkforanzusers ****'+finalLinkforanzusers);
                system.debug('finalLinkforanzfilterSF1  ****'+finalLinkforanzfilterSF1 );
         }
         else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Sales Data For Account'));
            isError = true;
         }
    }
    
      private String myCustomFieldMapper() {
        // This customer written class does the "work" need to map and get a Tableau username
        // In our example we use a static string "sfUser" as the string to map.
        //public StringMyUserId = UserInfo.getUserId();
        List<User> u = [Select FederationIdentifier from user where id=:UserInfo.getUserId()];
        System.debug('FEderated Id is ---'+u[0].FederationIdentifier);
        return u[0].FederationIdentifier;
    }
    
    /**
    * Gets the signed identity; always want to generate this in a getter since the constructor
    * only gets called on original page load and timestamp will skew
    */
    public String getSignedIdentity() {
        String signedIdentity = TableauSparklerUtilities.generateSignedIdentity(this.myCustomFieldMapper());
        system.debug('Singned Entity========'+signedIdentity);
        return signedIdentity;
    }
}

// :embed=y&:showShareOptions=true&:display_count=no&:showVizHome=no
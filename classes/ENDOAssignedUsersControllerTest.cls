/*
 @CreatedDate     28JUL2016                                  
 @author          Mayuri-Accenture
 @Description     Test class for 'AssignedUsersExt'. -This class Displays list of Users  assigned to the territories which is Used in the VF page to display.
 
 */



@isTest()
private class ENDOAssignedUsersControllerTest{

     public static User userC;
    public static List<Territory2> trList=new List<Territory2>(); 
    public static List<Territory2Type> Ttype; 
    public static Id Territory2ModelIDActive;  
    
    @testSetup
    static void setupTerritoryMockData(){
        userC = UtilForUnitTestDataSetup.newUser('Standard User');
        userC.Username = 'UserNameC1@xyz.com';
        insert userC;
        
        
        Ttype = UtilForUnitTestDataSetup.newTerritory2Type(); 
        Ttype  = [SELECT id, DeveloperName from Territory2Type Where MasterLabel LIKE : 'US Endo%' limit 2]; //SF does not allow to create territory type in test class, hence querying
        system.debug(Ttype+'Ttype@@');       
        Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA1234',Territory2ModelIDActive,Ttype[1].Id);   
        insert t1;              
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('ENUS1',Territory2ModelIDActive,Ttype[1].Id); 
        t2.ParentTerritory2Id = t1.Id;
        insert t2;
        Territory2 t3 = UtilForUnitTestDataSetup.newTerritory2('AA1245',Territory2ModelIDActive,Ttype[0].Id); 
        t3.ParentTerritory2Id = t2.Id;
        insert t3;
        
        System.assertEquals(t2.Territory2ModelId,Territory2ModelIDActive);        
        
        
        UserTerritory2Association nAssc = new UserTerritory2Association();
        nAssc.RoleInTerritory2 = 'Territory Manager';
        nAssc.Territory2Id = t3.Id;
        nAssc.UserId = userC.Id;
        insert nAssc;
        
        UserTerritory2Association nAssc1 = new UserTerritory2Association();
        nAssc1.RoleInTerritory2 = 'Area Manager';
        nAssc1.Territory2Id = t2.Id;
        nAssc1.UserId = userC.Id;
        insert nAssc1;
        
    } 
    static testMethod void ENDOUser_method1()
    {
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ]; 
        list<TerritoryUserRoles__c> csLst = new list<TerritoryUserRoles__c>();
        User u = [select Id from User where Username like : 'UserNameC1@xyz%' limit 1];
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst[1].Id; // assigns t3
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        TerritoryUserRoles__c cs1 = new TerritoryUserRoles__c();
        cs1.Name='Territory Manager';
        cs1.Priority__c = 1;
        csLst.add(cs1);
        TerritoryUserRoles__c cs2 = new TerritoryUserRoles__c();
        cs2.Name='Area Manager';
        cs2.Priority__c = 2;
        csLst.add(cs2);
        
        insert csLst;
        
        ENDOTerritoryTypes__c etm = new ENDOTerritoryTypes__c();
        etm.Name = 'US Endo Central Area';
        insert etm;
        
        ETM_Divisions__c eDiv = new ETM_Divisions__c();
        eDiv.Division_Code__c = 'Endo';
        eDiv.Name = 'ENUS1';
        insert eDiv;
        
        User_Division_Choice__c udc = new User_Division_Choice__c();
        udc.name = 'UserNameC1@xyz.com';
        udc.SessionId__c = '00D1100000Bv37i!AQQAQC8xB026kYOz.XwiLyN18In.Q2qUsOX_3Oh7o5i5jB54hRKomplLnVduRMuS.43J.tDFK3YlGJlMo1N1O4cl4997OEjv';
        //udc.SessionId__c = UserInfo.getSessionId();
        //udc.Selected_Division__c = 'EN';
        //udc.UserId__c = UserInfo.getUserId();
        udc.Selected_Division__c = 'ENUS1';
        udc.UserId__c = u.Id;
        insert udc;
        
        ApexPages.currentPage().getParameters().put('id', acct1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
        ENDOAssignedUsersController acctControlleer  =  new  ENDOAssignedUsersController(ctr);
        acctControlleer.getDivisions();
        acctControlleer.updateUserList();
        system.runAs(u){
        Test.startTest();
        Test.setCurrentPage(Page.ENDOAssignedTerritoryUsers);
        Test.stopTest();
        }
    
    
    }
    static testMethod void ENDOUser_method2()
    {
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'EN%'  ]; 
        list<TerritoryUserRoles__c> csLst = new list<TerritoryUserRoles__c>();
        User u = [select Id from User where Username like : 'UserNameC1@xyz%' limit 1];
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst[0].Id; // assigns t3
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        TerritoryUserRoles__c cs1 = new TerritoryUserRoles__c();
        cs1.Name='Territory Manager';
        cs1.Priority__c = 1;
        csLst.add(cs1);
        TerritoryUserRoles__c cs2 = new TerritoryUserRoles__c();
        cs2.Name='Area Manager';
        cs2.Priority__c = 2;
        csLst.add(cs2);
        
        insert csLst;
        
        ENDOTerritoryTypes__c etm = new ENDOTerritoryTypes__c();
        etm.Name = 'US Endo';
        insert etm;
        
        ETM_Divisions__c eDiv = new ETM_Divisions__c();
        eDiv.Division_Code__c = 'Endo';
        eDiv.Name = 'EN';
        insert eDiv;
        
        User_Division_Choice__c udc = new User_Division_Choice__c();
        udc.name = 'UserNameC1@xyz.com';
        udc.SessionId__c = UserInfo.getSessionId();
        udc.Selected_Division__c = 'EN';
        udc.UserId__c = UserInfo.getUserId();
        
        insert udc;
        
        ApexPages.currentPage().getParameters().put('id', acct1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
        ENDOAssignedUsersController acctControlleer  =  new  ENDOAssignedUsersController(ctr);
        acctControlleer.getDivisions();
        acctControlleer.updateUserList();
        
        Test.startTest();
        Test.setCurrentPage(Page.ENDOAssignedTerritoryUsers);
        Test.stopTest();
           
    
    }
    
    
    static testMethod void AssignedUser_withoutUDC()
    {
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ]; 
        list<TerritoryUserRoles__c> csLst = new list<TerritoryUserRoles__c>();
        User u = [select Id from User where Username like : 'UserNameC1@xyz%' limit 1];
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst[1].Id; // assigns t3
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        TerritoryUserRoles__c cs1 = new TerritoryUserRoles__c();
        cs1.Name='Territory Manager';
        cs1.Priority__c = 1;
        csLst.add(cs1);
        TerritoryUserRoles__c cs2 = new TerritoryUserRoles__c();
        cs2.Name='Area Manager';
        cs2.Priority__c = 2;
        csLst.add(cs2);
        
        insert csLst;
        
        ENDOTerritoryTypes__c etm = new ENDOTerritoryTypes__c();
        etm.Name = 'US Endo Central Area';
        insert etm;
        
        ETM_Divisions__c eDiv = new ETM_Divisions__c();
        eDiv.Division_Code__c = 'Endo';
        eDiv.Name = 'ENUS1';
        insert eDiv;
        
        User_Division_Choice__c udc = new User_Division_Choice__c();
        udc.name = 'UserNameC1@xyz.com';
        udc.SessionId__c = '00D1100000Bv37i!AQQAQC8xB026kYOz.XwiLyN18In.Q2qUsOX_3Oh7o5i5jB54hRKomplLnVduRMuS.43J.tDFK3YlGJlMo1N1O4cl4997OEjv';
        //udc.SessionId__c = UserInfo.getSessionId();
        //udc.Selected_Division__c = 'EN';
        //udc.UserId__c = UserInfo.getUserId();
        udc.Selected_Division__c = 'ENUS1';
        udc.UserId__c = u.Id;
        insert udc;
        
        ApexPages.currentPage().getParameters().put('id', acct1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
        AssignedUsersExt acctControlleer  =  new  AssignedUsersExt(ctr);
        acctControlleer.getDivisions();
        acctControlleer.updateUserList();
        system.runAs(u){
        Test.startTest();
        Test.setCurrentPage(Page.Assigned_Users);
        Test.stopTest();
        }
    
    
    }
    static testMethod void AssignedUser_withUDC()
    {
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'EN%'  ]; 
        list<TerritoryUserRoles__c> csLst = new list<TerritoryUserRoles__c>();
        User u = [select Id from User where Username like : 'UserNameC1@xyz%' limit 1];
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst[0].Id; // assigns t3
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        TerritoryUserRoles__c cs1 = new TerritoryUserRoles__c();
        cs1.Name='Territory Manager';
        cs1.Priority__c = 1;
        csLst.add(cs1);
        TerritoryUserRoles__c cs2 = new TerritoryUserRoles__c();
        cs2.Name='Area Manager';
        cs2.Priority__c = 2;
        csLst.add(cs2);
        
        insert csLst;
        
        ENDOTerritoryTypes__c etm = new ENDOTerritoryTypes__c();
        etm.Name = 'US Endo';
        insert etm;
        
        ETM_Divisions__c eDiv = new ETM_Divisions__c();
        eDiv.Division_Code__c = 'Endo';
        eDiv.Name = 'EN';
        insert eDiv;
        
        User_Division_Choice__c udc = new User_Division_Choice__c();
        udc.name = 'UserNameC1@xyz.com';
        udc.SessionId__c = UserInfo.getSessionId();
        udc.Selected_Division__c = 'EN';
        udc.UserId__c = UserInfo.getUserId();
        
        insert udc;
        
        ApexPages.currentPage().getParameters().put('id', acct1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
        AssignedUsersExt acctControlleer  =  new  AssignedUsersExt(ctr);
        acctControlleer.getDivisions();
        acctControlleer.updateUserList();
        
        Test.startTest();
        Test.setCurrentPage(Page.Assigned_Users);
        Test.stopTest();
           
    
    }
    

}
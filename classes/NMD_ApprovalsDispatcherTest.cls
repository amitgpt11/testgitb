/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_ApprovalsDispatcherTest {

    static final String ACCTNAME = 'ACCTNAME';
    static final RestDispatcherV1 dispatcher = new RestDispatcherV1(null);

    static {
        RestContext.response = new RestResponse();
    }
    
    @testSetup 
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();

        Account acct = td.newAccount(ACCTNAME);
        insert acct;

        Opportunity oppty = td.newOpportunity(acct.Id);
        insert oppty;

        // create one inv data per acct
        Inventory_Data__c invData1 = new Inventory_Data__c(
            Account__c = acct.Id,
            Customer_Class__c = 'YY'
        );
        insert invData1;

        // create one inv location per inv data
        Inventory_Location__c invLoc1 = new Inventory_Location__c(
            Inventory_Data_ID__c = invData1.Id
        );
        insert invLoc1;

        Order order = new Order(
            RecordTypeId = OrderManager.RECTYPE_TRANSFER,
            AccountId = acct.Id,
            OpportunityId = oppty.Id,
            EffectiveDate = System.today(),
            Status = 'Draft',
            Order_Method__c = 'Approval',
            Shipping_Location__c = invLoc1.Id,
            Stage__c = 'Transfer Requested'
        );
        insert order;

    }

    static testMethod void test_URI() {

        NMD_ApprovalsDispatcher handler = new NMD_ApprovalsDispatcher();
        String uri = handler.getURIMapping();

    }

    static testMethod void test_Order_RecallAction() {

        Opportunity opp = [select Id, AccountId from Opportunity where Account.Name = :ACCTNAME];
        Inventory_Location__c location = [select Id from Inventory_Location__c LIMIT 1];

        Order order = new Order(
            RecordTypeId = OrderManager.RECTYPE_TRANSFER,
            AccountId = opp.AccountId,
            OpportunityId = opp.Id,
            EffectiveDate = System.today(),
            Status = 'Draft',
            Order_Method__c = 'Approval',
            Shipping_Location__c = location.Id,
            Stage__c = 'Transfer Requested'
        );
        insert order;

        NMD_ApprovalsDispatcher handler = new NMD_ApprovalsDispatcher();
        handler.execute(dispatcher, new Map<String,String>{
            'recordId' => order.Id,
            'action' => 'recall'
        }, null);

    }

    static testMethod void test_Order_RejectAction() {

        Opportunity opp = [select Id, AccountId from Opportunity where Account.Name = :ACCTNAME];
        Inventory_Location__c location = [select Id from Inventory_Location__c LIMIT 1];

        Order order = new Order(
            RecordTypeId = OrderManager.RECTYPE_TRANSFER,
            AccountId = opp.AccountId,
            OpportunityId = opp.Id,
            EffectiveDate = System.today(),
            Status = 'Draft',
            Order_Method__c = 'Approval',
            Shipping_Location__c = location.Id,
            Stage__c = 'Transfer Requested'
        );
        insert order;

        NMD_ApprovalsDispatcher handler = new NMD_ApprovalsDispatcher();
        handler.execute(dispatcher, new Map<String,String>{
            'recordId' => order.Id,
            'action' => 'reject'
        }, null);

    }

    static testMethod void test_Order_ApproveAction() {

        Opportunity opp = [select Id, AccountId from Opportunity where Account.Name = :ACCTNAME];
        Inventory_Location__c location = [select Id from Inventory_Location__c LIMIT 1];

        Order order = new Order(
            RecordTypeId = OrderManager.RECTYPE_TRANSFER,
            AccountId = opp.AccountId,
            OpportunityId = opp.Id,
            EffectiveDate = System.today(),
            Status = 'Draft',
            Order_Method__c = 'Approval',
            Shipping_Location__c = location.Id,
            Stage__c = 'Transfer Requested'
        );
        insert order;

        NMD_ApprovalsDispatcher handler = new NMD_ApprovalsDispatcher();
        handler.execute(dispatcher, new Map<String,String>{
            'recordId' => order.Id,
            'action' => 'approve'
        }, null);

    }

    static testMethod void test_Order_InvalidAction() {

        Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

        NMD_ApprovalsDispatcher handler = new NMD_ApprovalsDispatcher();
        handler.execute(dispatcher, new Map<String,String>{
            'recordId' => order.Id,
            'action' => 'foo'
        }, null);

    }

    static testMethod void test_Order_InvalidOrder() {

        Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

        NMD_ApprovalsDispatcher handler = new NMD_ApprovalsDispatcher();
        handler.execute(dispatcher, new Map<String,String>{
            'recordId' => 'foo'
        }, null);

    }

}
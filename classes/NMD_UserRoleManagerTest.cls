/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_UserRoleManagerTest {
    
    @TestSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();

        System.runAs(new User(Id = UserInfo.getUserId())) {

            UserRole normalRole = td.setupUserRoleHierarchy(NMD_UserRoleManager.NEUROMODUS);

            update new User(
                Id = UserInfo.getUserId(),
                UserRoleId = normalRole.Id
            );

        }

    }

    static testMethod void test_UserRoleManager() {

        NMD_UserRoleManager roleManager = NMD_UserRoleManager.getInstance();
        Set<Id> rolesInRegion = roleManager.getRolesInRegion();
        Boolean isNMDRole = roleManager.isUserRoleInNeuromod();

        System.assertNotEquals(null, rolesInRegion);
        System.assertEquals(2, rolesInRegion.size());
        System.assert(isNMDRole);

    }

}
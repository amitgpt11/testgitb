/**
* Handler class for the Private Attachment Shared trigger
* Requirement Q4-2016 --> SFDC 404 & SFDC 405*
* @Author Accenture Services
* @Date 14SEPT2016
*/
public with sharing class PrivateAttachmentTriggerHandler extends TriggerHandler {

    final PrivateAttachmentManager manager = new PrivateAttachmentManager();
   
    /**
    * method to 
    * 
    * @param void
    */
    public override void bulkBefore() {
        List<Shared_Private_Attachments__c> pAttachments = (List<Shared_Private_Attachments__c>)trigger.new;
    }

    /**
    * method to 
    * 
    * @param void
    */
    public override void bulkAfter() {
        List<Shared_Private_Attachments__c> pAttachments = (List<Shared_Private_Attachments__c>)trigger.new;
        if(trigger.newMap != null){
        Set<Id> PAIds = trigger.newMap.keySet();
        set<Id> NewAcctIdsSet = new set<Id>();
        set<Id> OldAcctIdsSet = new set<Id>();
        set<Id> acctIdSet = manager.fecthAccountIds(PAIds);
        manager.fetchAccountShareData(acctIdSet);
        manager.reCalculateShareMethod(pAttachments);
       // manager.fetchAccountforinsertedShareAttachement(pAttachments);/**** For update checkbox on Account ***/
        }
        if(trigger.isUpdate){
            map<Id,Shared_Private_Attachments__c> newPAMap = new map<Id,Shared_Private_Attachments__c>();
            map<Id,Shared_Private_Attachments__c> oldPAMap = new map<Id,Shared_Private_Attachments__c>();
            for(Shared_Private_Attachments__c pa : (List<Shared_Private_Attachments__c>)trigger.new){ 
                if(pa.Account__c != ((Shared_Private_Attachments__c)Trigger.oldMap.get(pa.Id)).Account__c                        
                    || pa.ICPI_Division__c != ((Shared_Private_Attachments__c)Trigger.oldMap.get(pa.Id)).ICPI_Division__c){
                    newPAMap.put(pa.Id,(Shared_Private_Attachments__c)Trigger.NewMap.get(pa.Id));
                    oldPAMap.put(pa.Id,(Shared_Private_Attachments__c)Trigger.oldMap.get(pa.Id));
                }
            }
            manager.reCalculateShareAfterUpdate(newPAMap);
        }
        

    }
    /**
    * Called once per object
    * 
    * @param void
    */
    public override void beforeUpdate(SObject oldObj, SObject obj) {
        Shared_Private_Attachments__c pAttachment = (Shared_Private_Attachments__c)obj;
        Shared_Private_Attachments__c oldPAttachment = (Shared_Private_Attachments__c)oldObj;        

    }

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void afterUpdate(SObject oldObj, SObject obj) {
        Shared_Private_Attachments__c pAttachment = (Shared_Private_Attachments__c)obj;
        Shared_Private_Attachments__c oldpAttachment = (Shared_Private_Attachments__c)oldObj;       

    }

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void beforeInsert(SObject obj) {
        Shared_Private_Attachments__c pAttachment = (Shared_Private_Attachments__c)obj;

    }     

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void afterInsert(SObject obj) {
        Shared_Private_Attachments__c pAttachment = (Shared_Private_Attachments__c)obj;
        //manager.updateCheckboxOnAccount(pAttachment);/**** For update checkbox on Account ***/
    } 
    
     /**
    * Called once per object
    * 
    * @param void
    */
    public override void afterDelete(SObject obj) {
        Shared_Private_Attachments__c pAttachment = (Shared_Private_Attachments__c)obj;
        //manager.updateCheckboxOnAccount(pAttachment);/**** For update checkbox on Account ***/
    }
    
    /**
    * Called once per object
    * 
    * @param void
    */
    public override void beforeDelete(SObject obj) {
        Shared_Private_Attachments__c pAttachment = (Shared_Private_Attachments__c)obj;
        //manager.updateCheckboxOnAccount(pAttachment);
    }      

    /**
    * Called at end of trigger
    * 
    * @param void
    */  
    public override void andFinally() {
        
        //manager.updateAccount();
    }   
}
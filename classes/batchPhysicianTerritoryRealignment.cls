global class batchPhysicianTerritoryRealignment implements Database.Batchable<sObject>,Database.Stateful {


            List<contact> AllNewAccWiseContactList = new List<contact>();
            List<Contact> cFinalwithOwner = new List<Contact>();
            List<Patient__c> selectedPList = new List<Patient__c>();
            Set<Id> pOldTerr = new Set<Id>();
            List<Physician_Territory_Realignment__c> Allscope = new List<Physician_Territory_Realignment__c>();
            List<Patient__c> Allpatients = new List<Patient__c>();
            List<id> AlloldAccountIdList = new List<id>();
            set<id> AlloldContactTerriMulti = new set<id>();
            set<id> AlloldPatientTerriMulti = new set<id>();
            
        global Database.QueryLocator start(Database.BatchableContext BC) {
            DateTime dT = System.now();
            Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
            String query = 'SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c = \'Pending\' and Effective_Realignment_Date__c =: myDate';
            return Database.getQueryLocator(query);
        }
       
        global void execute(Database.BatchableContext BC, List<Physician_Territory_Realignment__c> scope) {
            
            //  DateTime dT = System.now();
            //Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
            set<id> conIdList = new set<id> ();
            system.debug('----scope--'+scope);
            
            List<id> oldAccountIdList = new List<id>();
            set<id> oldAccountId = new set<id>();
            set<id> oldContactTerriMulti = new set<id>();
            set<id> oldPatientTerriMulti = new set<id>();
     
            if(!scope.isEmpty())
                Allscope.addall(scope);
            for(Physician_Territory_Realignment__c a : scope)
            {
                // if(a.Effective_Realignment_Date__c == myDate )
                 {
                 
               //  conId.add(a.New_Account_Id__c);
                     conIdList.add(a.Contact__c);
                     
                 }
                 //a.Name = a.Name;            
            }
             
               Map<Id, Contact> contactMap = new Map<Id, Contact>();
               List<Contact> contactList = new List<Contact>() ;
               
             if(!conIdList.isEmpty())
                contactList = new List<Contact>([Select Id, Territory_ID__c,AccountID from Contact where Id in :conIdList ]);
             
       if(!contactList.isEmpty())   
       {
            for(contact c: contactList){
                oldContactTerriMulti.add(c.Territory_ID__c);
                oldAccountId.add(c.Accountid);
            }
       } 
     if(!oldAccountId.isEmpty())   
       oldAccountIdList.addAll(oldAccountId);
        if(!oldAccountIdList.isEmpty())
       AlloldAccountIdList.addAll(oldAccountIdList);
       
        if(!oldContactTerriMulti.isEmpty())
       AlloldContactTerriMulti.addAll(oldContactTerriMulti);
       
                system.debug('AlloldAccountIdList------'+AlloldAccountIdList);
                system.debug('oldContactTerriMulti------'+oldContactTerriMulti);
       
       
       
    ///////// to update new account in contact 
    List<contact> conListNewAcc = new List<contact>();
            for(Physician_Territory_Realignment__c a : scope)
            {
                // if(a.Effective_Realignment_Date__c == myDate )
                 {
                     contact ct = new contact();
                     if(a.Contact__c != null ){
                        ct.id= a.Contact__c;
                    if(a.New_Account_Id__c != null)
                        ct.accountid= a.New_Account_Id__c;
                 
                 ct.Territory_ID__c= a.New_Territory__c;
               //  conId.add(a.New_Account_Id__c);
                     conListNewAcc.add(ct);
                 }
                     
                 }
                 //a.Name = a.Name;            
            }   
             
           update conListNewAcc;
           Map<Id,Id> contactsNewTerritoryMap = new Map<Id,Id>();
           
           for(Contact c:conListNewAcc){
               contactsNewTerritoryMap.put(c.id,c.Territory_ID__c);
           }
           for(Contact c1:[Select Id,Territory_ID__c,Territory_ID__r.Current_TM1_Assignee__c from Contact where Id in: contactsNewTerritoryMap.keyset()]){
                if(c1.Territory_ID__r.Current_TM1_Assignee__c != null)
                    c1.OwnerId= c1.Territory_ID__r.Current_TM1_Assignee__c;
                    cFinalwithOwner.add(c1);
            }
            if(!cFinalwithOwner.isEmpty()){
                update cFinalwithOwner;
            }
             
             system.debug('conListNewAcc------'+conListNewAcc);
            List<contact> NewAccWiseContactList = new List<contact>();

             if(!conIdList.isEmpty())
                contactMap = new Map<Id, Contact>([Select Id, Territory_ID__c,AccountID from Contact where Id in :conIdList ]);
                 NewAccWiseContactList = new List<Contact>([Select Id, Territory_ID__c,AccountID from Contact where Id in :conIdList ]);
        //Below Line commented by AMitabh to fetch selected patients instead of related patients of Physician *********
        //List<Patient__c> patients = [SELECT Id, name, Physician_of_Record__c,Territory_ID__c,Physician_of_Record__r.Territory_ID__c,Physician_of_Record__r.AccountId FROM Patient__c where Physician_of_Record__c in :contactMap.keySet()];
        
        //****Below two lists selectedPIdList and pOldTerr are need to send to method-Amitabh
        
        List<Id> selectedPIdList = new List<Id>();
        For(Patient__c p:selectedPList){
            selectedPIdList.add(p.Id);
        }
        selectedPList = MovePatientToPage3Con.sendSelctedPatientToBatch();
        
        pOldTerr = MovePatientToPage3Con.sendPOldTerriToBatch();
        
        system.debug('selectedPList------'+selectedPList+'_____pOldTerr______'+pOldTerr);
        
        List<Patient__c> patients = [SELECT Id, name, Physician_of_Record__c,Territory_ID__c,Physician_of_Record__r.Territory_ID__c,Physician_of_Record__r.AccountId FROM Patient__c where Id in: selectedPIdList];
        
         if(!NewAccWiseContactList.isEmpty())  
        AllNewAccWiseContactList.addAll(NewAccWiseContactList);
        
         if(!patients.isEmpty())  
        Allpatients.addAll(patients);
        system.debug('AllNewAccWiseContactList------'+AllNewAccWiseContactList+'----Allpatients---'+Allpatients);
        system.debug('contactMap------'+contactMap+'----patients---'+patients);
        

    //multiplePhysicianUpdate(List<Contact>  SelectedPhysicianList, List<Physician_Territory_Realignment__c> PtrList,List<Patient__c> SelectedPatientList, List<id> oldAccountIds, Boolean conUpdt) 

        
        for(Patient__c pt : patients){
        if(contactMap.containsKey(pt.Physician_of_Record__c)){
            List<Patient__c> pList = new  List<Patient__c> ();
            oldPatientTerriMulti.add(pt.Territory_ID__c);
            pList.add(pt);
            string a;
            id i;
         
            
        }
        }
        if(!oldPatientTerriMulti.isEmpty())
        AlloldPatientTerriMulti.addAll(oldPatientTerriMulti);
        
    //MovePatientToPage3Con InsObj = new MovePatientToPage3Con();
    //InsObj.multiplePhysicianUpdate(NewAccWiseContactList,scope,patients,oldAccountIdList,true,oldContactTerriMulti,oldPatientTerriMulti);
                 /*
                 Physician_of_Record__c 
                 Contact.Territory_ID__c    
                 Contact.AccountId           
                 Physician_of_Record__c
                 */
                 
            // update scope;
              //SelectedItem4 ==> physician Territory   
       //SelectedItem5 ==> physician Account 
            //focusPhysicianUpdate(List<Patient__c>  patients,String SelectedItem4,String SelectedItem5,Id currentContactId)
            MovePatientToPage3Con InsObj = new MovePatientToPage3Con();
            InsObj.forBatchMultiplePhysicianUpdate(cFinalwithOwner,Allscope,AlloldAccountIdList,true,AlloldContactTerriMulti);
        }   
        
    global void finish(Database.BatchableContext BC) {
            
            
            system.debug('AllNewAccWiseContactList------------'+AllNewAccWiseContactList+'     Allscope   '+Allscope+'____Allpatients___'+Allpatients+'`````````````AlloldAccountIdList```'+AlloldAccountIdList+'========AlloldContactTerriMulti==='+AlloldContactTerriMulti+'----AlloldPatientTerriMulti----'+AlloldPatientTerriMulti);
            
            //Below line commented by amitabh as need to send selected patients 
            //InsObj.multiplePhysicianUpdate(AllNewAccWiseContactList,Allscope,Allpatients,AlloldAccountIdList,true,AlloldContactTerriMulti,AlloldPatientTerriMulti);
        /*   on 12-09-2016
            InsObj.multiplePhysicianUpdate(cFinalwithOwner,Allscope,selectedPList,AlloldAccountIdList,true,AlloldContactTerriMulti,pOldTerr);
*/
              
//forBatchMultiplePhysicianUpdate(List<Contact>  SelectedPhysicianList, List<Physician_Territory_Realignment__c> PtrList, List<id> oldAccountIds   , Boolean conUpdt,set<id> oldContactTerriMulti) 
         
batchPhysicianPatientLeadUpdate b = new batchPhysicianPatientLeadUpdate();
database.executebatch(b,20);        

            
            /*
            batchPhysicianTerritoryRealignment b = new batchPhysicianTerritoryRealignment();
            database.executebatch(b);
            */
    }
}
/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PostInstallClass implements System.InstallHandler {
    global static List<openq__Attribute__c> ats;
    global static List<openq__Category_Group__c> cG;
    global static List<openq__Country__c> countries;
    global static List<openq__MIRF_Attribute__c> mirfAttributes;
    global static List<openq__MIRF_Section__c> mirfSections;
    global static List<openq__Related_List__c> relatedLists;
    global static List<openq__Related_List_Attribute__c> rlAt;
    global PostInstallClass() {

    }
    global static void addApiNames() {

    }
    global static void addMobileQEventPreferenceField() {

    }
    global static void addNewEventSettingFields(openq__Event_Settings__c eventSetting) {

    }
    global static openq__Attribute__c cA(String name, Integer displayOrder, String label, openq__Page_Layout__c pageLayout) {
        return null;
    }
    global static openq__Related_List__c cRL(String name, openq__Category_Group__c categoryGroup, String contactRelationship, Integer displayOrder, String keyPrefix, String label, openq__Page_Layout__c pageLayout) {
        return null;
    }
    global static openq__Category_Group__c ccg(String name, Integer displayOrder) {
        return null;
    }
    global static void clearFilingDateField() {

    }
    global static void clearUpStaleParticipants() {

    }
    global static void copyDueDateDatetimeToNewFieldPlanning() {

    }
    global static void copyOrderIntoOrderNew() {

    }
    global static void copyPlanNamesIntoCustomNameField() {

    }
    global static openq__Category__c createCategory(String name, String apiName) {
        return null;
    }
    global static openq__Country__c createCountry(String name, Id regionId) {
        return null;
    }
    global static void createEngagementPlanningPageLayouts() {

    }
    global static void createEventCategoriesAndColors() {

    }
    global static void createEventPageLayouts() {

    }
    global static void createMIRFPageLayouts() {

    }
    global static openq__MIRF_Attribute__c createMirfAttribute(openq__MIRF_Section__c mirfSection, String label, String apiName, Integer displayOrder) {
        return null;
    }
    global static openq__MIRF_Section__c createMirfSection(openq__MIRF_Layout__c mirfLayout, String name) {
        return null;
    }
    global static openq__Page_Layout__c createPageLayout(String name, openq__Category__c category, String recordType) {
        return null;
    }
    global static openq__Region__c createRegion(String regionName) {
        return null;
    }
    global static void createRegions() {

    }
    global static void createScopeGroups() {

    }
    global static openq__Related_List_Attribute__c crla(String name, Integer displayOrder, String label, openq__Related_List__c relatedList) {
        return null;
    }
    global static void emulateInstall() {

    }
    global static void insertContactLinksDefaultSettings() {

    }
    global static void insertContactSettings() {

    }
    global static void insertDefaultEngagementPlanColumns() {

    }
    global static void insertDefaultEventObjectIdentifiers() {

    }
    global static void insertDefaultInteractionColumns() {

    }
    global static void insertDefaultMirfColumns() {

    }
    global static void insertEventCalendarPopupFieldsDefaultSettings() {

    }
    global static void insertEventSpeakerPageLayout() {

    }
    global static void insertEventSpeakerSelectColumns() {

    }
    global static void insertGlobalCustomSettingsDefaultsAdminConsoleSettings() {

    }
    global static void insertGlobalCustomSettingsDefaultsBudgetTotalCostObjects() {

    }
    global static void insertGlobalCustomSettingsDefaultsCalendarSettings() {

    }
    global static void insertGlobalCustomSettingsDefaultsEmpowerSettings() {

    }
    global static void insertGlobalCustomSettingsDefaultsEngagementPlanPreferences() {

    }
    global static void insertGlobalCustomSettingsDefaultsEngagementPlanSettings() {

    }
    global static void insertGlobalCustomSettingsDefaultsEventListViewFields() {

    }
    global static void insertGlobalCustomSettingsDefaultsEventSettings() {

    }
    global static void insertGlobalCustomSettingsDefaultsMappingSettings() {

    }
    global static void insertGlobalCustomSettingsDefaultsSegmentationColumns() {

    }
    global static void insertGlobalCustomSettingsDefaultsSegmentationObjects() {

    }
    global static void insertGlobalCustomSettingsDefaultsSegmentationSettings() {

    }
    global static void insertInteractionSettings() {

    }
    global static void insertMirfSettings() {

    }
    global static void insertSegmentationLensDefaults() {

    }
    global static void migratePreferredInfoToContactHeaderField() {

    }
    global static void migrateUserToScopeGroups() {

    }
    global void onInstall(System.InstallContext context) {

    }
    global static void removeApprovalTabFromEventLayouts() {

    }
    global static void removePreferredLinkWithDrp() {

    }
    global static void scheduleRegionCountryCalculation() {

    }
    global static void updateAttendeeRelatedListAttribute() {

    }
    global static void updateContactPageLayoutLabels() {

    }
    global static void updateDrpMappingFieldLengths() {

    }
    global static void updateDrpMappingObject() {

    }
    global static void updateEngagementPlanningPageLayoutFields() {

    }
    global static void updateEventPageLayoutsWithRelatedInteractions() {

    }
    global static void updateEventSpeakerAttributes() {

    }
    global static void updateInteractionAttendeeCustomSetting() {

    }
    global static void updateInteractions() {

    }
    global static void updatePageLayoutLabels() {

    }
}

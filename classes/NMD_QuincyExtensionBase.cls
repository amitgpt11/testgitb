/**
* Virtual Class containing base methods for the Quincy vf pages
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public virtual class NMD_QuincyExtensionBase extends NMD_ExtensionBase {
	
	protected final ApexPages.StandardController sc;

    public List<NMD_ObjectWrapper> children { get; private set; }
	public Boolean isSalesforce1 { get; private set; }
    public Boolean isSubmitted { get; protected set; }

	public NMD_QuincyExtensionBase(ApexPages.StandardController sc) {
		this.sc = sc;
		this.children = new List<NMD_ObjectWrapper>{};
		this.isSalesforce1 = ApexPages.currentPage().getParameters().get('isdtp') == 'p1';
        this.isSubmitted = false;
	}

	public virtual void submit() {}

	protected void logError(String classNameDotMethodName, String errorMessage) {

        logError(
        	classNameDotMethodName, 
        	errorMessage, 
        	this.sc.getId(), 
        	this.sc.getRecord().getSObjectType().getDescribe().getName()
        );
        
	}

	protected Set<Id> getSelectedIds() {
        Set<Id> selectedIds = new Set<Id>{};
        for (NMD_ObjectWrapper item : this.children) {
            if (item.selected) {
                selectedIds.add(item.data.Id);
            }
        }
        return selectedIds;
    }

    protected Map<String,String> getFieldValueDeltas(Schema.FieldSet fieldset, SObject obj0, SObject obj1) {

        Map<String,String> fieldValues = new Map<String,String>{};
        for (Schema.FieldSetMember field : fieldset.getFields()) {
            String fieldApi = field.getFieldPath();
            String fieldVal = String.valueOf(obj1.get(fieldApi));
            String orignVal = String.valueOf(obj0.get(fieldApi));
            //only add deltas
            if (fieldVal != orignVal) {
                fieldValues.put(fieldApi, fieldVal);
            }
        }

        return fieldValues;

    }

}
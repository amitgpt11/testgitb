/*
@CreatedDate     21JUNE2016
@author          Mike Lankfer
@Description     Contains all the automation logic to populate the lookup values of all related records (Facility, Implanting Physician, Secondary Implanting Physician, Referring Physician, Lead WCS, and Parent Patient Case (Procedure)) on the Watchman Implant object
@Requirement Id  User Story : DRAFT US2484
*/
public with Sharing class ImplantMatching {
    
    private static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Contact.getRecordTypeInfosByName();    
    private static final Id RECTYPE_WATCHMAN_CONTACT = RECTYPES.get('Watchman General').getRecordTypeId();
    private static final Integer QUERY_LIMIT = 39999;
    
    private Set<String> accountNumberSet = new Set<String>();
    private Set<Id> affectedAccountsSet = new Set<Id>();
    private Set<Date> patientCaseDateSet = new Set<Date>();
    private Set<Id> watchmanReferringPhyscianAccountSet = new Set<Id>();
    private Set<String> wmuIdSet = new Set<String>();
    private List<Shared_Patient_Case__c> affectedPatientCaseList = new List<Shared_Patient_Case__c>();
    private List<Contact> fullReferringContactsList = new List<Contact>();
    private List<Contact> possibleContactMatches = new List<Contact>();
    private List<User> possibleUserMatches = new List<User>();
    private List<Contact> wmuContactList = new List<Contact>();
    private List<Account> watchmanReferringPhyscianAccountList = new List<Account>();
    private List<User> watchmanUserList = new List<User>();
    private List<List<String>> implantReferringPhyscian = new List<List<String>>();
    private List<List<String>> implantWatchmanUser = new List<List<String>>();
    private Map<String, Id> affectedAccountsMap = new Map<String, Id>();
    private Map<String, Id> wmuContactMap = new Map<String, Id>();
    private Shared_Patient_Case__c fosterParentPatientCase = new Shared_Patient_Case__c();
    private Datetime fosterParentStartDateTime = Datetime.newInstance(2001, 01, 01, 00, 00, 00);       
    private String watchmanReferralAccounts = '%Watchman Referring Physicians%';
    private String watchmanProfileName = '%Watchman%';
    private Date reformattedProcedureDate = Date.newInstance(2000, 01, 01);
    
    
    
    /*
@CreatedDate     21JUNE2016
@author          Mike Lankfer
@Description     Contains all the automation logic to populate the lookup values of all related records (Facility, Implanting Physician, Secondary Implanting Physician, Referring Physician, Lead WCS, and Parent Patient Case (Procedure)) on the Watchman Implant object
@Requirement Id  User Story : DRAFT US2484
*/    
    public void matchRelatedRecords(List<LAAC_Implant__c> implants){
        
        
        if (Trigger.isInsert){
            /*------------------------------Match Account--------------------------------------*/        
            //build Set of SAP Account Numbers from list of implants
            for(LAAC_Implant__c implant:implants){
                if (implant.LAAC_Account_Number__c != NULL){
                    accountNumberSet.add(implant.LAAC_Account_Number__c);
                }
            }
            
            //build map of SAP Account Numbers and Account Record IDs
            for(Account a:[SELECT Id, Account_Number__c, Name FROM Account WHERE Account_Number__c IN :accountNumberSet LIMIT :QUERY_LIMIT]){
                affectedAccountsMap.put(a.Account_Number__c, a.Id); 
            }
            
            //loop through implant records and set facility value
            for(LAAC_Implant__c implant:implants){
                if ((affectedAccountsMap != NULL) && (affectedAccountsMap.size() > 0)){
                    implant.LAAC_Facility__c = affectedAccountsMap.get(implant.LAAC_Account_Number__c);
                }
            }
            
            
            /*------------------------------Match Patient Case-----------------------------------*/
            
            fosterParentPatientCase = [SELECT Id FROM Shared_Patient_Case__c WHERE Shared_Start_Date_Time__c < :fosterParentStartDateTime LIMIT :QUERY_LIMIT];
            
            //build Set of affected Account Ids
            for(Account a:[SELECT Id, Account_Number__c, Name FROM Account WHERE Account_Number__c IN :accountNumberSet LIMIT :QUERY_LIMIT]){
                affectedAccountsSet.add(a.Id);
            }
            
            //build list of Patient Cases filtered by facility and procedure date
            affectedPatientCaseList = [SELECT Id, Shared_Facility__r.Id, Shared_Start_Date_Time__c FROM Shared_Patient_Case__c WHERE ((Shared_Facility__r.Id IN :affectedAccountsSet) AND (LAAC_Count_of_Watchman_Implants__C = 0))
                                       ORDER BY Shared_Start_Date_Time__c ASC LIMIT :QUERY_LIMIT];
            
            //loop through implant list and set parent patient case value to first available patient case on the same day that does not already have an implant associated to it.  If no match is found, assign to foster parent patient case
            for(LAAC_Implant__c implant:implants){
                for(Integer i = 0; i < affectedPatientCaseList.size(); i++){
                    if ((affectedPatientCaseList.get(i).Shared_Facility__c == implant.LAAC_Facility__c) && (implant.LAAC_Date_of_Procedure__c  == affectedPatientCaseList.get(i).Shared_Start_Date_Time__c.Date())){
                        implant.LAAC_Implant_Patient_Case__c = affectedPatientCaseList.get(i).Id;
                        affectedPatientCaseList.remove(i);
                        break;
                    }
                }
                if (implant.LAAC_Implant_Patient_Case__c == NULL){
                    implant.LAAC_Implant_Patient_Case__c = fosterParentPatientCase.Id;
                }
            }
        }
        
        /*----------------------------------------------Match Lead WCS----------------------------------------------------------*/
        
        //build list of all watchman users
        watchmanUserList = [SELECT Id, FirstName, LastName FROM User WHERE Profile_Name__c LIKE :watchmanProfileName LIMIT :QUERY_LIMIT];
        
        for(LAAC_Implant__c implant:implants){
            if (implant.LAAC_WATCHMAN_User__c != NULL){
                implantWatchmanUser.add(implant.LAAC_WATCHMAN_User__c.split(' ', 0));
                if(implantWatchmanUser.get(0).size() == 2){
                    for(User user:watchmanUserList){
                        if ((implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 1) == user.LastName) && (implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 2) == user.FirstName)){
                            possibleUserMatches.add(user);
                        }
                    }                                      
                }
                else if(implantWatchmanUser.get(0).size() == 3){
                    for(User user:watchmanUserList){
                        if ((((implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 2)) + ' ' + (implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 1))) == user.LastName) && 
                            (implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 3) == user.FirstName)){
                                possibleUserMatches.add(user);
                            }
                    }
                    
                }
                else if(implantWatchmanUser.get(0).size() == 4){
                    for(User user:watchmanUserList){
                        if ((((implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 3)) + ' ' + (implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 2)) + ' ' + (implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 1))) == user.LastName) && 
                            (implantWatchmanUser.get(0).get(implantWatchmanUser.get(0).size() - 4) == user.FirstName)){
                                possibleUserMatches.add(user);
                            }
                    }
                    
                }
                if (possibleUserMatches.size() == 1){
                    implant.LAAC_Lead_WCS__c = possibleUserMatches.get(0).Id;
                } 
                possibleUserMatches.clear();
                implantWatchmanUser.clear();                  
            }
        }
        
        
        /*------------------------------Match Implanting & Secondary Implanting Physicians--------------------------------------*/
        
        //build a set of all contact WMU ids associated to new implants
        for(LAAC_Implant__c implant:implants){
            wmuIdSet.add(implant.LAAC_Implanting_Physician_User_ID__c);
            wmuIdSet.add(implant.LAAC_Secondary_Implanting_Physician_Id__c);
        }
        
        //build a list of Contacts related to new implants
        wmuContactList = [SELECT Id, LAAC_WMU_User_Id__c FROM Contact WHERE LAAC_WMU_User_Id__c IN :wmuIdSet AND RecordTypeId = :RECTYPE_WATCHMAN_CONTACT LIMIT :QUERY_LIMIT];
        
        //build map of contact wmu id to contact id
        for(Contact contact:wmuContactList){
            wmuContactMap.put(contact.LAAC_WMU_User_Id__c, Contact.Id);
        }
        
        //loop through implants and populate the implanting physcian and secondary implanting physicians
        for(LAAC_Implant__c implant:implants){
            if (implant.LAAC_Implanting_Physician_User_ID__c != NULL){
                implant.LAAC_Implanting_Physician__c = wmuContactMap.get(implant.LAAC_Implanting_Physician_User_ID__c);
            }
            if (implant.LAAC_Secondary_Implanting_Physician_Id__c != NULL){
                implant.LAAC_Secondary_Implanting_Physician_LU__c = wmuContactMap.get(implant.LAAC_Secondary_Implanting_Physician_Id__c);
            }
        }      
        
        
        /*--------------------------------Match Referring Physician------------------------------------*/
        
        //build list of Watchman referral account ids
        watchmanReferringPhyscianAccountList = [SELECT Id FROM Account WHERE Name LIKE :watchmanReferralAccounts];
        
        //build set of watchman referral account ids
        for(Account account:watchmanReferringPhyscianAccountList){
            watchmanReferringPhyscianAccountSet.add(account.Id);
        }
        
        //build list of all Watchman Referring Physician Contacts
        fullReferringContactsList = [SELECT Id, FirstName, LastName FROM Contact WHERE AccountId IN :watchmanReferringPhyscianAccountSet AND RecordTypeId = :RECTYPE_WATCHMAN_CONTACT LIMIT :QUERY_LIMIT];
        
        //loop through the list of implants and compare to all watchman referring contacts. if exactly 1 exact match (first name, last name) is found, then the referring physician lookup field is populated with that value
        for(LAAC_Implant__c implant:implants){
            if (implant.LAAC_Referring_Physician__c != NULL){
                implantReferringPhyscian.add(implant.LAAC_Referring_Physician__c.split(' ', 0));
                if((implantReferringPhyscian.get(0).size() > 1) && (implantReferringPhyscian.get(0).size() < 4)){
                    for(Contact contact:fullReferringContactsList){
                        if ((implantReferringPhyscian.get(0).get(implantReferringPhyscian.get(0).size() - 1) == contact.LastName) && (implantReferringPhyscian.get(0).get(implantReferringPhyscian.get(0).size() - 2) == contact.FirstName)){
                            possibleContactMatches.add(contact);
                        }
                    }
                    if (possibleContactMatches.size() == 1){
                        implant.LAAC_Referring_Physician_LU__c = possibleContactMatches.get(0).Id;
                    }
                }
                possibleContactMatches.clear();
                implantReferringPhyscian.clear();
            }
        }      
    }     
}
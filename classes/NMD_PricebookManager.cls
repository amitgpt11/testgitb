/**
* Manager class to help control Customer Specific Pricing for NMD customers
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public without sharing class NMD_PricebookManager {

    final static Id limitedReleaseId = Schema.SObjectType.List__c.getRecordTypeInfosByName().get('NMD Limited Release').getRecordTypeId();
    
    public final static Set<String> NEUROMOD = new Set<String> { 'NM', 'NMD', 'Neuromod', 'Neuromodulation' };
    public final static String NMD = 'NMD';

    // hold entries by 'NMD' then by PricebookEntryId, or the account Id and then by the Product Id
    final Map<String,Map<Id,PricebookEntry>> entries;
    final Id userId; // the user context
    private Set<Id> userExemptions; // set of Products that the user is exempt from restriction, lazily loaded

    public NMD_PricebookManager(Set<Id> acctIds) {
        this(acctIds, new Set<Id>());
    }

    public NMD_PricebookManager(Set<Id> acctIds, Set<Id> nmdEntryIds) {
        this(acctIds, nmdEntryIds, '', '');
    }

    public NMD_PricebookManager(Set<Id> acctIds, String prodSearch, String prodFamily) {
        this(acctIds, new Set<Id>(), prodSearch, prodFamily);
    }

    public NMD_PricebookManager(Set<Id> acctIds, Set<Id> nmdEntryIds, String prodSearch, String prodFamily) {
        this(null, acctIds, new Set<Id>(), prodSearch, prodFamily); 
    }

    public NMD_PricebookManager(Id userId, Set<Id> acctIds, Set<Id> nmdEntryIds, String prodSearch, String prodFamily) {

        this.userId = (userId != null && userId.getSObjectType() == User.getSObjectType()) ? userId : UserInfo.getUserId();

        this.entries = new Map<String,Map<Id,PricebookEntry>> { 
            NMD => new Map<Id,PricebookEntry>() 
        };

        // get entries
        Set<Id> nmdProdIds = new Set<Id>();
        for (PricebookEntry pbe : Database.query(
            'select ' +
                'UnitPrice, ' +
                'Is_Sellable__c, ' +
                'Start_Date__c, ' +
                'End_Date__c, ' +
                'Product2Id, ' +
                'Product2.Name, ' +
                'Product2.Description, ' +
                'Product2.Family, ' +
                'Product2.ProductCode, ' +
                'Product2.Material_Group_1__c, ' +
                'Product2.UPN_Material_Number__c, ' +
                'Product2.EAN_UPN__c, ' +
                'Product2.QN__c, ' +
                'Product2.Xfer_Type__c, ' +
                'Pricebook2.Name, ' +
                'Pricebook2.Account__c, ' +
                'IsVirtualKit__c, ' + 
                'Is_Displayable__c, ' + 
                'IsDisplayable_InMemory__c ' + 
            'from PricebookEntry ' +
            'where (' + 
                '(Pricebook2.Name = :NMD' + 
                    (nmdEntryIds.isEmpty() ? '' : ' and Id in :nmdEntryIds') + 
                ')' + 
                (acctIds.isEmpty() ? '' : ' or Pricebook2.Account__c in :acctIds') + 
            ') ' +
            'and IsActive = true ' +
            (String.isBlank(prodSearch) ? '' : 
                'and (Product2.Name like :prodSearch or Product2.Model_Number__c like :prodSearch or Product2.Description like :prodSearch) '
            ) +
            (String.isBlank(prodFamily) ? '' :
                'and Product2.Family = :prodFamily '
            ) +
            'order by Pricebook2.Account__c nulls first, Name ' // this is so that the NMD entries are loaded before the acct entries
        )) {
            if (pbe.Pricebook2.Name == NMD) {
                this.entries.get(NMD).put(pbe.Id, pbe);
                nmdProdIds.add(pbe.Product2Id);
            }
            else if (nmdProdIds.contains(pbe.Product2Id)) {
                if (this.entries.containsKey(pbe.Pricebook2.Account__c)) {
                    this.entries.get(pbe.Pricebook2.Account__c).put(pbe.Product2Id, pbe);
                }
                else {
                    this.entries.put(pbe.Pricebook2.Account__c, new Map<Id,PricebookEntry> {
                        pbe.Product2Id => pbe
                    });
                }
            }
        }

    }

    public Boolean hasExemption(Id prodId) {

        if (this.userExemptions == null) {

            // find user exemptions
            this.userExemptions = new Set<Id>();

            if (this.userId != null) {
                for (List__c exemption : [
                    select
                        Model_Number__c
                    from List__c
                    where RecordTypeId = :limitedReleaseId
                    and Inventory_Account__c in (
                        select Id
                        from Account
                        where RecordTypeId = :AccountManager.RECTYPE_CONSIGNMENT
                        and OwnerId = :this.userId
                    )
                ]) {
                    this.userExemptions.add(exemption.Model_Number__c);
                }
            }

        }

        return this.userExemptions.contains(prodId);

    }

    public List<NMD_PricebookPair> getDefaultPricebookEntries() {

        return getAccountPricebookEntries(null, null);

    }

    public List<NMD_PricebookPair> getAccountPricebookEntries(Id acctId) {

        return getAccountPricebookEntries(acctId, System.today());

    }

    public List<NMD_PricebookPair> getAccountPricebookEntries(Id acctId, Date d) {

        List<NMD_PricebookPair> pairs = new List<NMD_PricebookPair>();
        for (PricebookEntry pbe : this.entries.get(NMD).values()) {
            NMD_PricebookPair pair = getPricebookEntry(acctId, pbe.Id, d);
            if (pair != null) {
                pairs.add(pair);
            }
        }

        return pairs;

    }

    public NMD_PricebookPair getPricebookEntry(Id acctId, Id entryId) {
        
        return getPricebookEntry(acctId, entryId, System.today());

    }

    public NMD_PricebookPair getPricebookEntry(Id acctId, Id entryId, Date d) {
        
        // start with default NMD entry
        PricebookEntry nmdPbe = this.entries.get(NMD).get(entryId);
        PricebookEntry cusPbe;
        NMD_PricebookPair pair;

        // if there is an acct specific mapping
        if (nmdPbe != null && acctId != null && this.entries.containsKey(acctId)) {

            if (d == null) {
                d = System.today();
            }

            // and if it contains special pricing for this product
            Map<Id,PricebookEntry> cpbes = this.entries.get(acctId);
            if (cpbes.containsKey(nmdPbe.Product2Id)) {

                // and if the dates are valid
                PricebookEntry cpbe = cpbes.get(nmdPbe.Product2Id);
                if ((cpbe.Start_Date__c == null || cpbe.Start_Date__c <= d)
                 && (cpbe.End_Date__c == null || cpbe.End_Date__c >= d)
                ) {
                    cusPbe = cpbe;
                }

            }

        }

        if (nmdPbe != null) {
            pair = new NMD_PricebookPair(nmdPbe, cusPbe);
        }
        return pair;

    }

}
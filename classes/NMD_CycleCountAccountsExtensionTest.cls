/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_CycleCountAccountsExtensionTest {

	static final String CYCLECOUNTNAME = 'Cycle Count';
	
	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		td.setupNuromodUserRoleSettings();

		System.runAs(new User(Id = UserInfo.getUserId())) {

			UserRole normalRole = td.setupUserRoleHierarchy(NMD_UserRoleManager.NEUROMODUS);

			update new User(
				Id = UserInfo.getUserId(),
				UserRoleId = normalRole.Id
			);

			Account acct = td.createConsignmentAccount();
			acct.ShippingCity = 'City';
			acct.ShippingState = 'State';
			acct.OwnerId = UserInfo.getUserId();
			insert acct;

			insert new Cycle_Count__c(
				Name = CYCLECOUNTNAME,
				Status__c = 'Draft'
			);

		}

	}

	static testMethod void test_CycleCountAccounts() {

		Cycle_Count__c cycleCount = [select Status__c from Cycle_Count__c where Name = :CYCLECOUNTNAME];
		
		PageReference pr = Page.CycleCountAccounts;
		pr.getParameters().put('id', cycleCount.Id);
		Test.setCurrentPage(pr);

		NMD_CycleCountAccountsExtension ext = new NMD_CycleCountAccountsExtension(new ApexPages.StandardController(cycleCount));

		ext.searchType = NMD_CycleCountAccountsExtension.ACCOUNT;
		ext.doSearch();

		ext.searchType = NMD_CycleCountAccountsExtension.AREA;
		ext.doSearch();

		ext.searchType = NMD_CycleCountAccountsExtension.REGION;
		ext.doSearch();

		ext.selectedItems.add(ext.availableItems[0]);
		ext.addSelectedItems();
        
        ext.removeType =ext.removeTypes[0].getValue();
		ext.removeAllbyRegion();
		ext.firstPage();
		ext.nextPage();
		ext.prevPage();
		ext.lastPage();

		//ext.savedItems[0].selected = true;
		ext.removeSelectedItems();

	}

}
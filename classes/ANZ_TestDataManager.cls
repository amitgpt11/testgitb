/**
* ANZ_TestDataManager.class
*
* @author   Salesforce services
* @date   21/09/2016
* @desc   Creates test data for the ANZ code-base.
*/
@isTest
public class ANZ_TestDataManager {
static final String NEUROMOD_ROLE_DEV_NAME = 'Neuromod_US_Test';
public static  account     acct;


  public ANZ_TestDataManager() { } 

//------------ aalok code starts---------




  public void setupNuromodUserRoleSettings(){
    insert new Neuromod_UserRole__c(
      SetupOwnerId = UserInfo.getOrganizationId(),
      Developer_Name__c = NEUROMOD_ROLE_DEV_NAME
    );

    try{
      UserRole root = [select Name, ParentRoleId from UserRole where DeveloperName = :NEUROMOD_ROLE_DEV_NAME];
    } catch (QueryException e){
      System.debug('***Query QueryException' + e);

      System.runAs(new User(Id = UserInfo.getUserId())) {
        insert new UserRole(
          Name = NEUROMOD_ROLE_DEV_NAME,
          DeveloperName = NEUROMOD_ROLE_DEV_NAME
        );  
      }    

    }

  }

  public Account newAccount() {
    return newAccount('Account' + randomString5());
  }

  public Account newAccount(String name) {
    return new Account(
      Name = name
    );
  }





  public Opportunity newOpportunity2(Id acctId) {
    return newOpportunity2(acctId, 'Opportunity' + randomString5());
  }
  public Opportunity newOpportunity2(Id acctId, String name) {
    return new Opportunity(
      AccountId = acctId,
      CloseDate = System.today().addDays(7),
      Name = name,
      StageName = 'New',
      Type = 'Standard',
      Opportunity_Type__c = 'Standard',
      Competitor_Name__c = 'Other',
    Product_System__c = 'Spectra'
    );
  }
  
  public order newOrder2(Id acctId, id opptyId,id pb2ID){
        order newOrd = new order();
        newOrd.AccountId = acctId;
        newOrd.OpportunityId = opptyId;
        newOrd.EffectiveDate = System.today();
        newOrd.Status = 'Draft';
        newOrd.Order_Method__c = 'Approval';
        newOrd.Pricebook2Id  = pb2ID;
        return newOrd;
        
        }
  
  
  Id pricebookId = Test.getStandardPricebookId();
  
  public Product2 newProduct2() {
  Product2 p = new Product2();
  p.Name = 'ANZPro1';
  p.Description = 'ANZ Products';
  p.IsActive = true;
  
  return p;
  
  }
  
  
  public Pricebook2 newPricebook2(){
  
   Pricebook2 p = new Pricebook2();
  p.Name = 'ANZPro2';
  p.Description = 'ANZ Products2';
  p.IsActive = true;
  return p;
  
  } 
  
  public PricebookEntry newPricebookEntry( id prodNew0,id pb2ID)
  {
  PricebookEntry pbe = new PricebookEntry();
pbe.Pricebook2Id = pb2ID; //pricebookId; //pricebookIdNew;
pbe.Product2Id = prodNew0;
pbe.UnitPrice = 1;
pbe.IsActive = true;
pbe.UseStandardPrice = false;
pbe.Start_Date__c = System.today().addDays(-1);
pbe.End_Date__c = System.today().addDays(1);
 return pbe;
  }
  
  
  

        
  public OrderItem newOrderItem(id orderId,Id PricebookEntryId, id InventoryItemId ){
  
  OrderItem ot = new OrderItem();
  
ot.OrderId = orderId;
ot.PriceBookEntryId = PricebookEntryId;
ot.Inventory_Source__c = InventoryItemId;// iItem.Id;
ot.Quantity = 1;
ot.UnitPrice = 1;

  return ot;
  }
  

  
  
//------------ aalok code Ends---------  

  public Opportunity newOpportunity(Id acctId, String name) {
    return new Opportunity(
      AccountId = acctId,
      CloseDate = System.today().addDays(7),
      Name = name,
      StageName = 'New',
      Type = 'Standard',
      Opportunity_Type__c = 'Standard',
      Competitor_Name__c = 'Other',
    Product_System__c = 'Spectra'
    );
  }

  public Order newOrder(Id acctId, Id opptyId) {
    return new Order(
      AccountId = acctId,
      OpportunityId = opptyId,
      EffectiveDate = System.today(),
        Status = 'Draft',
        Order_Method__c = 'Approval'
    );
  }

  public Contact newContact(Id acctId) {
    return newContact(acctId, 'LastName' + randomString5());
  }

  public Contact newContact(Id acctId, String lname) {
    return new Contact(
      AccountId = acctId,
      LastName = lname
    );
  }



  public Lead newLead() {
    return newLead('LastName' + randomString5());  
  }

  public Lead newLead(String lname) {
    return new Lead(
      LastName = lname,
      Company = 'Company' + randomString5(),
      Status = 'Open',
      RecordTypeId = LeadManager.RECTYPE_NMD_PATIENTS
    );
  }

  public Patient__c newPatient() {
    return newPatient(randomString5(), randomString5());
  }
// AKG added Lead_Source__c on 13062016
  public Patient__c newPatient(String fname, String lname) {
    return new Patient__c(
      Patient_First_Name__c = fname,
      Patient_Last_Name__c = lname,
      Lead_Source__c = 'CARE Card'   
      
    );
  }



  public Attachment newAttachment(Id parentId) {
    return newAttachment(parentId, randomString5());
  }

  public Attachment newAttachment(Id parentId, String name) {
    return newAttachment(parentId, name, randomString5());
  }

  public Attachment newAttachment(Id parentId, String name, String description) {
    return newAttachment(parentId, name, description, Blob.valueOf('X'));
  }

  public Attachment newAttachment(Id parentId, String name, String description, Blob body) {
    return new Attachment(
      ParentId = parentId,
      Name = name,
      Description = description,
      Body = body
    );
  }

  public PatientImageClassifications__c newPatientImageClassification(String name, Integer order) {
    return new PatientImageClassifications__c(
      Name = name,
      Order__c = order,
      Is_Active__c = true
    );
  }
  


  
  public User newUser(){
    return newUser('Standard User');
  }
  
  public User newUser(String userType){
    Profile p = [SELECT Id FROM Profile WHERE Name = :userType]; 
        User user = new User(
          Email = 'suser@boston.com', 
      LastName = 'LNAMETEST',  
      ProfileId = p.Id, 
      UserName = randomString5() + '@boston.com',
      Alias = 'standt', 
      EmailEncodingKey = 'UTF-8',  
      LanguageLocaleKey = 'en_US', 
      LocaleSidKey = 'en_US',  
      TimeZoneSidKey = 'America/Los_Angeles',
      Cost_Center_Code__c = randomString5() // special forNMD
    );
    return user;                  
  }  

  public String randomString5() {
    return randomString().substring(0, 5);
  }

  public String randomString() {
    //return random string of 32 chars
    return EncodingUtil.convertToHex(Crypto.generateAesKey(128));
  }
  
// run test class as admin user 

public Static User setupUser()
    {
        Profile prof = [select id from profile where name = 'System Administrator'];
        User user = new User(alias = 'standt', email = 'my123_123@gmail.com', emailencodingkey = 'UTF-8',
                         lastname = 'Test', languagelocalekey = 'en_US', localesidkey = 'en_US',
                         profileid = prof.Id, timezonesidkey = 'America/Los_Angeles',IsActive =true,
                         username = 'testuser_123@gmail.com');
        //insert user;
        return user;
    } 
    
    public Static User setupUserNMDRBM()
    {
        Profile prof = [select id from profile where name = 'NMD RBM'];
        User user = new User(alias = 'stRBM', email = 'my123_1234@gmail.com', emailencodingkey = 'UTF-8',
                         lastname = 'Test', languagelocalekey = 'en_US', localesidkey = 'en_US',
                         profileid = prof.Id, timezonesidkey = 'America/Los_Angeles',IsActive =true,
                         username = 'testuser_1234@gmail.com');
        //insert user;
        return user;
    }


/// setup order 
public Static Order setupOrder1() {

        NMD_TestDataManager td = new NMD_TestDataManager();



        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
        user0, user1
        };

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create two assignees
        Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
        assignee0.Rep_Designation__c = 'TM1';
        Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
        assignee1.Rep_Designation__c = 'TM2';
        insert new List < Assignee__c > {
        assignee0
        };
    
    //   insert acct;
        acct= td.newAccountANZ('ACCTNAME');  
        insert acct;    
        system.debug('___acct____'+acct);

        // product
        Product2  prod = td.newProductANZ('Accessory');
        prod.Model_Number__c = 'MODEL123';
        prod.EAN_UPN__c = 'MODEL123';
        prod.UPN_Material_Number__c = 'UPNMATERIALNUM';
        system.debug('prod details===>'+prod);
        insert prod;
        system.debug('after prod details===>'+prod);
        Inventory_Location__c invLoc =setupInventoryLocation();
        system.debug('___acct____'+acct);
        system.debug('___prod____'+prod);
        system.debug('invLoc______'+invLoc);
        
        //create oppty
        Opportunity oppty = td.newOpportunityANZ(acct.Id);
        oppty.Territory_Id__c = hierarchy.Id;            
        insert oppty;
        system.debug('___oppty____'+oppty);

        // create two products, pricebook, and entries
        Id pricebookId = Test.getStandardPricebookId();
        //ANZ Implant Devices
        Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
        system.debug(' pb details===>'+pb);
        insert pb;
        system.debug('after  pb details===>'+pb);
        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' standardPrice0 details===>'+standardPrice0);
        insert new List<PricebookEntry> { standardPrice0 };
        system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        system.debug(' After pbe0 details===>'+pbe0);
        Order ord = new Order(
        AccountId = acct.id,
        Shipping_Location__c=invLoc.id,
        OpportunityId = oppty.Id,
        EffectiveDate = System.today(),
        Pricebook2Id = pb.Id,
        Status = 'Draft',
        RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
        //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
        Stage__c = 'Pending'
        );
        system.debug(' Order details===>'+ord);
        insert ord;

        system.debug('After Order details===>'+ord);
        OrderItem ordItem = new OrderItem(
        OrderId=ord.id,
        Material_Number_UPN__c='UPNMATERIALNUM',
        Serial_Number__c = 'SERIAL1',
        ANZ_Batch_Number__c='123',
        PricebookEntryId = pbe0.Id,
        Quantity=1,
        UnitPrice = 1.0
        //Product = p2.id
        );
        system.debug('ordItem details===>'+ordItem);
        insert ordItem; 
        system.debug('After ordItem details===>'+ordItem);
        return ord;    
    }   
  public Static Order setupOrder2() {

        NMD_TestDataManager td = new NMD_TestDataManager();



        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
        user0, user1
        };

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create two assignees
        Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
        assignee0.Rep_Designation__c = 'TM1';
        Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
        assignee1.Rep_Designation__c = 'TM2';
        insert new List < Assignee__c > {
        assignee0
        };
    
    //   insert acct;
        acct= td.newAccountANZ('ACCTNAME');  
        insert acct;    
        system.debug('___acct____'+acct);

        // product
        Product2  prod = td.newProductANZ('Explanted Lead');
        prod.Model_Number__c = 'MODEL123';
        prod.EAN_UPN__c = 'MODEL123';
        prod.UPN_Material_Number__c = 'UPNMATERIALNUM';
        system.debug('prod details===>'+prod);
        insert prod;
        system.debug('after prod details===>'+prod);
        Inventory_Location__c invLoc =setupInventoryLocation();
        system.debug('___acct____'+acct);
        system.debug('___prod____'+prod);
        system.debug('invLoc______'+invLoc);
        
        //create oppty
        Opportunity oppty = td.newOpportunityANZ(acct.Id);
        oppty.Territory_Id__c = hierarchy.Id;            
        insert oppty;
        system.debug('___oppty____'+oppty);

        // create two products, pricebook, and entries
        Id pricebookId = Test.getStandardPricebookId();
        //ANZ Implant Devices
        Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
        system.debug(' pb details===>'+pb);
        insert pb;
        system.debug('after  pb details===>'+pb);
        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' standardPrice0 details===>'+standardPrice0);
        insert new List<PricebookEntry> { standardPrice0 };
        system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        system.debug(' After pbe0 details===>'+pbe0);
        Order ord = new Order(
        AccountId = acct.id,
        Shipping_Location__c=invLoc.id,
        OpportunityId = oppty.Id,
        EffectiveDate = System.today(),
        Pricebook2Id = pb.Id,
        Status = 'Draft',
        RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
        //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
        Stage__c = 'Pending'
        );
        system.debug(' Order details===>'+ord);
        insert ord;

        system.debug('After Order details===>'+ord);
        OrderItem ordItem = new OrderItem(
        OrderId=ord.id,
        Material_Number_UPN__c='UPNMATERIALNUM',
        Serial_Number__c = 'SERIAL1',
        ANZ_Batch_Number__c='123',
        PricebookEntryId = pbe0.Id,
        Quantity=1,
        UnitPrice = 1.0
        //Product = p2.id
        );
        system.debug('ordItem details===>'+ordItem);
        insert ordItem; 
        system.debug('After ordItem details===>'+ordItem);
        return ord;    
    }   

     public Static Order setupOrder3() {

        NMD_TestDataManager td = new NMD_TestDataManager();



        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
        user0, user1
        };

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create two assignees
        Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
        assignee0.Rep_Designation__c = 'TM1';
        Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
        assignee1.Rep_Designation__c = 'TM2';
        insert new List < Assignee__c > {
        assignee0
        };
    
     Patient__C  patObj = td.newPatientANZ();
     insert patObj;
     system.debug('___patObj__'+patObj);
    
    //   insert acct;
        acct= td.newAccountANZ('ACCTNAME');  
        insert acct;    
        system.debug('___acct____'+acct);

        // product
        Product2  prod = td.newProductANZ('Access Sheath');
        prod.Model_Number__c = 'MODEL123';
        prod.EAN_UPN__c = 'MODEL123';
        prod.UPN_Material_Number__c = 'UPNMATERIALNUM';
        system.debug('prod details===>'+prod);
        insert prod;
        system.debug('after prod details===>'+prod);
        Inventory_Location__c invLoc =setupInventoryLocation();
        system.debug('___acct____'+acct);
        system.debug('___prod____'+prod);
        system.debug('invLoc______'+invLoc);
        
        //create oppty
        Opportunity oppty = td.newOpportunityANZ(acct.Id);
        oppty.Territory_Id__c = hierarchy.Id;      
oppty.Patient__c = patObj.id;       
        insert oppty;
        system.debug('___oppty____'+oppty);
        //ANZ Implant RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Explant').getRecordTypeId(),
        
        oppty.RecordTypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Implant').getRecordTypeId();
update oppty;

  System.debug(LoggingLevel.Info, '___Update oppty__'+oppty);
        // create two products, pricebook, and entries
        Id pricebookId = Test.getStandardPricebookId();
        //ANZ Implant Devices
        Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
        system.debug(' pb details===>'+pb);
        insert pb;
        system.debug('after  pb details===>'+pb);
        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' standardPrice0 details===>'+standardPrice0);
        insert new List<PricebookEntry> { standardPrice0 };
        system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        system.debug(' After pbe0 details===>'+pbe0);
        Order ord = new Order(
        AccountId = acct.id,
        Shipping_Location__c=invLoc.id,
        OpportunityId = oppty.Id,
        ANZ_Related_Opportunity__c = oppty.Id,
        EffectiveDate = System.today(),
        Pricebook2Id = pb.Id,
        Status = 'Draft',
        RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
        //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
        Stage__c = 'Pending'
        );
        system.debug(' Order details===>'+ord);
       /* insert ord;

        system.debug('After Order details===>'+ord);
        OrderItem ordItem = new OrderItem(
        OrderId=ord.id,

        Material_Number_UPN__c='UPNMATERIALNUM',
        Serial_Number__c = 'SERIAL1',
        PricebookEntryId = pbe0.Id,
        Quantity=1,
        UnitPrice = 1.0
        //Product = p2.id
        );
        system.debug('ordItem details===>'+ordItem);
        insert ordItem; 
        system.debug('After ordItem details===>'+ordItem); */
        return ord;    
    }
    
    
     public Static Order setupOrder4() {

        NMD_TestDataManager td = new NMD_TestDataManager();



        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
        user0, user1
        };

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create two assignees
        Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
        assignee0.Rep_Designation__c = 'TM1';
        Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
        assignee1.Rep_Designation__c = 'TM2';
        insert new List < Assignee__c > {
        assignee0
        };
    
            acct= td.newAccountANZ('ACCTNAME');  
        insert acct;    
        
    Contact cont = new Contact();
    cont.AccountId = acct.id;
    cont.LastName = 'lname';
    cont.RecordTypeId =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('ANZ General Contact').getRecordTypeId();
insert cont;

        
        /**/
        
        
     Patient__C  patObj = td.newPatientANZ();
     patObj.Physician_of_Record__c = cont.id;
     insert patObj;
     system.debug('___patObj__'+patObj);
    
    //   insert acct; Insurance Provider

        system.debug('___acct____'+acct);

        // product
        Product2  prod = td.newProductANZ('Access Sheath');        
        system.debug('prod details===>'+prod);
        insert prod;
        system.debug('after prod details===>'+prod);
        Inventory_Location__c invLoc =setupInventoryLocation();
        system.debug('___acct____'+acct);
        system.debug('___prod____'+prod);
        system.debug('invLoc______'+invLoc);
        
        //create oppty
        Opportunity oppty = td.newOpportunityANZ(acct.Id);
        oppty.Territory_Id__c = hierarchy.Id;      
oppty.Patient__c = patObj.id;       
        insert oppty;
        system.debug('___oppty____'+oppty);
        //ANZ Implant RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Explant').getRecordTypeId(),
        
        oppty.RecordTypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Implant').getRecordTypeId();
update oppty;

  System.debug(LoggingLevel.Info, '___Update oppty__'+oppty);
        // create two products, pricebook, and entries
        Id pricebookId = Test.getStandardPricebookId();
        //ANZ Implant Devices
        Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
        system.debug(' pb details===>'+pb);
        insert pb;
        system.debug('after  pb details===>'+pb);
        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' standardPrice0 details===>'+standardPrice0);
        insert new List<PricebookEntry> { standardPrice0 };
        system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        system.debug(' After pbe0 details===>'+pbe0);
        Order ord = new Order(
        AccountId = acct.id,
        Shipping_Location__c=invLoc.id,
        OpportunityId = oppty.Id,
        ANZ_Related_Opportunity__c = oppty.Id,
        ANZ_Implant_Funding__c ='Public',
        EffectiveDate = System.today(),
        Pricebook2Id = pb.Id,
        ANZ_Special_Pricing__c = true,
        Status = 'Draft',
        RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
        //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
        Stage__c = 'Pending'
        );
        system.debug(' Order details===>'+ord);
       insert ord;

        system.debug('After Order details===>'+ord);
        OrderItem ordItem = new OrderItem(
        OrderId=ord.id,

        Material_Number_UPN__c='UPNMATERIALNUM',
        Serial_Number__c = 'SERIAL1',
        ANZ_Batch_Number__c='123',
        PricebookEntryId = pbe0.Id,
        Quantity=1,
        UnitPrice = 1.0
        //Product = p2.id
        );
        system.debug('ordItem details===>'+ordItem);
        insert ordItem; 
        system.debug('After ordItem details===>'+ordItem); 
        return ord;    
    }   

    
    
    // 5 without oppy in order 
    
     public Static Order setupOrder5() {

        NMD_TestDataManager td = new NMD_TestDataManager();



        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
        user0, user1
        };

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create two assignees
        Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
        assignee0.Rep_Designation__c = 'TM1';
        Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
        assignee1.Rep_Designation__c = 'TM2';
        insert new List < Assignee__c > {
        assignee0
        };
    
            acct= td.newAccountANZ('ACCTNAME');  
        insert acct;    
        
    Contact cont = new Contact();
    cont.AccountId = acct.id;
    cont.LastName = 'lname';
    cont.RecordTypeId =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('ANZ General Contact').getRecordTypeId();
insert cont;

        
        /**/
        
        
     Patient__C  patObj = td.newPatientANZ();
     patObj.Physician_of_Record__c = cont.id;
     insert patObj;
     system.debug('___patObj__'+patObj);
    
    //   insert acct; Insurance Provider

        system.debug('___acct____'+acct);

        // product
        Product2  prod = td.newProductANZ('Access Sheath');        
        system.debug('prod details===>'+prod);
        insert prod;
        system.debug('after prod details===>'+prod);
        Inventory_Location__c invLoc =setupInventoryLocation();
        system.debug('___acct____'+acct);
        system.debug('___prod____'+prod);
        system.debug('invLoc______'+invLoc);
        
        //create oppty
       /* Opportunity oppty = td.newOpportunityANZ(acct.Id);
        oppty.Territory_Id__c = hierarchy.Id;      
oppty.Patient__c = patObj.id;       
        insert oppty;
        system.debug('___oppty____'+oppty);
        //ANZ Implant RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Explant').getRecordTypeId(),
        
        oppty.RecordTypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Implant').getRecordTypeId();
update oppty;

  System.debug(LoggingLevel.Info, '___Update oppty__'+oppty); */
        // create two products, pricebook, and entries
        Id pricebookId = Test.getStandardPricebookId();
        //ANZ Implant Devices
        Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
        system.debug(' pb details===>'+pb);
        insert pb;
        system.debug('after  pb details===>'+pb);
        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' standardPrice0 details===>'+standardPrice0);
        insert new List<PricebookEntry> { standardPrice0 };
        system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        system.debug(' After pbe0 details===>'+pbe0);
        Order ord = new Order(
        AccountId = acct.id,
        Shipping_Location__c=invLoc.id,
       // OpportunityId = oppty.Id,
       // ANZ_Related_Opportunity__c = oppty.Id,
        ANZ_Implant_Funding__c ='Public',
        EffectiveDate = System.today(),
        Pricebook2Id = pb.Id,
        ANZ_Special_Pricing__c = true,
        Status = 'Draft',
        RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
        //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
        Stage__c = 'Pending'
        );
        system.debug(' Order details===>'+ord);
       insert ord;

        system.debug('After Order details===>'+ord);
        //Serial_Number__c  ANZ_Batch_Number__c
        OrderItem ordItem = new OrderItem(
        OrderId=ord.id,

        Material_Number_UPN__c='UPNMATERIALNUM',
        ANZ_Batch_Number__c='123',
        Serial_Number__c = 'SERIAL1',
        PricebookEntryId = pbe0.Id,
        Quantity=1,
        UnitPrice = 1.0
        //Product = p2.id   
        );
        system.debug('ordItem details===>'+ordItem);
        insert ordItem; 
        system.debug('After ordItem details===>'+ordItem); 
        return ord;    
    }   

    
    
     public Static OrderItem setupOrderItem1() {

        NMD_TestDataManager td = new NMD_TestDataManager();



        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
        user0, user1
        };

        Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
        insert hierarchy;

        //create two assignees
        Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
        assignee0.Rep_Designation__c = 'TM1';
        Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
        assignee1.Rep_Designation__c = 'TM2';
        insert new List < Assignee__c > {
        assignee0
        };
    
     Patient__C  patObj = td.newPatientANZ();
     insert patObj;
     system.debug('___patObj__'+patObj);
    
    //   insert acct;
        acct= td.newAccountANZ('ACCTNAME');  
        insert acct;    
        system.debug('___acct____'+acct);

        // product
        Product2  prod = td.newProductANZ('LAAC Device');
        
        system.debug('prod details===>'+prod);
        insert prod;
        system.debug('after prod details===>'+prod);
        Inventory_Location__c invLoc =setupInventoryLocation();
        system.debug('___acct____'+acct);
        system.debug('___prod____'+prod);
        system.debug('invLoc______'+invLoc);
        
        //create oppty
        Opportunity oppty = td.newOpportunityANZ(acct.Id);
        oppty.Territory_Id__c = hierarchy.Id;      
oppty.Patient__c = patObj.id;       
        insert oppty;
        system.debug('___oppty____'+oppty);
        //ANZ Implant RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Explant').getRecordTypeId(),
        
        oppty.RecordTypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ANZ Implant').getRecordTypeId();
update oppty;

  System.debug(LoggingLevel.Info, '___Update oppty__'+oppty);
        // create two products, pricebook, and entries
        Id pricebookId = Test.getStandardPricebookId();
        //ANZ Implant Devices
        Pricebook2 pb = new Pricebook2(Name = 'ANZ Implant Devices', Description = 'NMD Products', IsActive = true);
        system.debug(' pb details===>'+pb);
        insert pb;
        system.debug('after  pb details===>'+pb);
        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        //PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pb.id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false); 
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' standardPrice0 details===>'+standardPrice0);
        insert new List<PricebookEntry> { standardPrice0 };
        system.debug('after  standardPrice0 details===>'+standardPrice0);
        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        system.debug(' pbe0 details===>'+pbe0);
        insert new List<PricebookEntry> { pbe0};
        system.debug(' After pbe0 details===>'+pbe0);
        Order ord = new Order(
        AccountId = acct.id,
        Shipping_Location__c=invLoc.id,
        OpportunityId = oppty.Id,
        ANZ_Related_Opportunity__c = oppty.Id,
        EffectiveDate = System.today(),
        Pricebook2Id = pb.Id,
        Status = 'Draft',
        RecordTypeId =Schema.SObjectType.order.getRecordTypeInfosByName().get('ANZ Implant Form').getRecordTypeId(),
        //  RecordTypeId = OrderManager.RECTYPE_PATIENT,
        Stage__c = 'Pending'
        );
        system.debug(' Order details===>'+ord);
       insert ord;

        system.debug('After Order details===>'+ord);
        OrderItem ordItem = new OrderItem(
        OrderId=ord.id,

        Material_Number_UPN__c='UPNMATERIALNUM',
        Serial_Number__c = 'SERIAL1',
        PricebookEntryId = pbe0.Id,
        Quantity=1,
        UnitPrice = 1.0
        //Product = p2.id
        );
        system.debug('ordItem details===>'+ordItem);
        insert ordItem; 
        system.debug('After ordItem details===>'+ordItem); 
        return ordItem;    
    }   

    
    
  static Inventory_Location__c setupInventoryLocation(){
   

        // inventory tree
        Inventory_Data__c invData = new Inventory_Data__c(
            Account__c = acct.Id,
            Customer_Class__c = 'YY'
        );
        insert invData;

      Inventory_Location__c  invLoc = new Inventory_Location__c(
            Inventory_Data_ID__c = invData.Id,
            Name = 'INVENLOCATION'
        );
        insert invLoc;
        return invLoc;
  }

// Account_Performance__c

  public Account newAccountANZ() {
    return newAccountANZ('Account' + randomString5());
  }

  public Account newAccountANZ(String name) {
    return new Account(
      Name = name,
      ShippingCountry ='AU'
    );
  }

  public Account_Performance__c newAccountPerformanceANZ(Id acctId) {
    return newAccountPerformanceANZ1(acctId);
  }

  public Account_Performance__c newAccountPerformanceANZ1(Id acctId) {
    return new Account_Performance__c (
      CY__c = '2016',
      Facility_Name__c =acctId
    );
  }  
  //Potential__c
  public Potential__c newPotentialANZ(Id acctPerfId) {
    return newPotentialANZ1(acctPerfId);
  }

  public Potential__c newPotentialANZ1(Id acctPerfId) {
    return new Potential__c (
  
      Account_Performance__c =acctPerfId,
      Potential_External_Id__c ='420'
    );
  }  
  
  }
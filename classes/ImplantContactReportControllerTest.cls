/**
* Test Class for ImplantContactReport controller and ImplantReportsExtensionContact page.       
*
* @CreatedBy   Payal Ahuja
* @CreatedDate 25/09/2016
*/
@isTest(SeeAllData=false)
public with sharing class ImplantContactReportControllerTest {
    
    public static testmethod void createContactTestMethod(){   
       
Test.startTest();
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        
        //@TestSetup
        NMD_TestDataManager td = new NMD_TestDataManager();
        
        Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
        Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
        Account a3 = UtilForUnitTestDataSetup.newAccount('ACCTNAME3');
        list<Account> alist= new List<Account>{a1, a2,a3};
        insert alist; 
        
      //create an implanting physician contact records 
        
        Contact trialing = td.newContact(a1.Id, 'TestP');
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        
        Contact procedure = td.newContact(a2.Id, 'Testp2');
        procedure.AccountId = a1.Id;
        procedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        procedure.MailingStreet = '123 Main St';
        procedure.MailingCity = 'test';
        procedure.MailingState = 'test';
        procedure.MailingPostalCode = 'test';
        procedure.MailingCountry = 'test';
        procedure.Fax = 'test';       
        procedure.Phone = '5556667788';
        
        Contact implant = td.newContact(a3.Id, 'TestP3');
        implant.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { 
            trialing, procedure , implant
        };
        System.debug('My debug statement');
        system.assertEquals(1,1);
        
        //create test procedure records
        Shared_Patient_Case__c primaryProcedure = new Shared_Patient_Case__c();
        primaryProcedure.Shared_Start_Date_Time__c = Datetime.now();
        primaryProcedure.Shared_End_Date_Time__c = Datetime.now() + (1/24) ;
        primaryProcedure.Shared_Facility__c = a1.Id ;
        insert primaryProcedure;
        
         //create test implant records
        LAAC_Implant__c primaryImplant = new LAAC_Implant__c();
        primaryImplant.LAAC_Implant_Patient_Case__c = primaryProcedure.Id;
        //primaryImplant.LAAC_Implanting_Physician__c = trialing.Id;
        primaryImplant.LAAC_Case_Aborted__c=false;
        insert primaryImplant;
        system.debug('***insideImplants***');
        
        //build page
        PageReference pg = Page.ImplantReportsExtensionContact;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('Id',trialing.Id);
        
        Contact ent = new Contact();
        ApexPages.StandardController st = new ApexPages.standardController(ent);
        ImplantContactReportController e = new ImplantContactReportController(st);                           

Test.stopTest();
        
     }
    }
}
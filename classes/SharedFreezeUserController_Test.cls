@isTest (SeeAllData=true)
public class SharedFreezeUserController_Test {

    public static testMethod void validateUser(){
        Profile p = [SELECT id FROM Profile WHERE Name='System Administrator'];

        User u = new User(
            FirstName = 'test', 
            LastName= 'last_test', 
            Email='test323232@test.com', 
            Phone='111-111-1111', 
            alias = 'test', 
            EmailEncodingKey='UTF-8',
            ProfileId = p.Id,
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', 
            TimeZoneSidKey='America/Los_Angeles',
            UserName = 'test323232@test.com',
            Highest_Level_Role__c='CEO'
        );

        insert u;

        System.assertEquals('test', u.FirstName);
    }

    public static testMethod void testRedirect(){

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

        User u = new User(
            FirstName = 'test', 
            LastName= 'last_test', 
            Email='test323232@test.com', 
            Phone='111-111-1111', 
            alias = 'test', 
            EmailEncodingKey='UTF-8',
            ProfileId = p.Id,
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', 
            TimeZoneSidKey='America/Los_Angeles',
            UserName = 'test323232@test.com',
            Highest_Level_Role__c='CEO'
        );

        try{
            insert u;
        }catch(DMLException e){
            System.debug('Error inserting new user: ' + e.getMessage());
        }
         Test.StartTest();
        System.runAs(u){
            Test.setCurrentPage(Page.freezeUser);
            ApexPages.currentPage().getParameters().put('Id', u.Id);
            SharedFreezeUserController obj = new SharedFreezeUserController();
            PageReference pageRef1 = obj.freezeUserButton();
            PageReference pageRef = obj.redirect();

            System.assertNotEquals(null, pageRef);
            //System.assertEquals('/apex/freezeUser', pageRef.getUrl());
            System.assertEquals('/' + u.Id, pageRef.getURL());
            Test.StopTest();
        }

    }

}
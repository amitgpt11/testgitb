/**
* Test class for the Convert Lead extension class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_ConvertLeadExtensionTest {
  
  static final String FNAME = 'FNOM';
  static final String LNAME = 'LNOM';
  static final String FACIL = 'FACIL';
  static final String TRIAL = 'TRIAL';
  static final String IMPLANTTYPE = 'Implant';
  static final String TRIALTYPE = 'Trial';  
    
  @testSetup
  static void setup() {

    NMD_TestDataManager td = new NMD_TestDataManager();

    Account facilityAcct = td.createConsignmentAccount();
    facilityAcct.Name = FACIL;
    Account trialingAcct = td.newAccount();
    trialingAcct.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
    trialingAcct.Type = 'Ship To';
    trialingAcct.Name = TRIAL;
    insert new List<Account> { facilityAcct, trialingAcct };

    Contact cont = td.newContact(trialingAcct.Id, LNAME);
    cont.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
    insert cont;

    Lead lead = td.newLead(LNAME);
    insert lead;

    Patient__c patient = td.newPatient(FNAME, LNAME);
    insert patient;

  }

  static List<SObject> getTestData() {

    List<SObject> result = new List<SObject>{};

    Set<String> names = new Set<String> { FACIL, TRIAL };
    for (Account acct : [select Name from Account order by Name]) {
      result.add(acct);
    }

    result.add([select Id from Contact limit 1]);
    result.add([select Id from Lead limit 1]);
    result.add([select Id from Patient__c limit 1]);

    return result;

  }

  static testMethod void test_ConvertLead() {

    NMD_TestDataManager td = new NMD_TestDataManager();
    User testuser = td.newUser('System Administrator');
    insert testuser;
   // ApexPages.StandardController st = new ApexPages.standardController(lead);
   // NMD_ConvertLeadExtension ncle =new NMD_ConvertLeadExtension(st);
        
    System.runAs(testuser) {
    
      List<SObject> result = getTestData();
      Id trialId = result[0].Id;
      Id facilId = result[1].Id;
      Id physId = result[2].Id;
      Id leadId = result[3].Id;
      Id patientId = result[4].Id;

      Lead lead = td.newLead();
      lead.Id = leadId;
      lead.Account_Facility__c = facilId;
      lead.Trialing_Physician__c = physId;
      lead.Patient__c = patientId;
      lead.ConvertTo__c = TRIALTYPE;   
      update lead;

      PageReference pr = Page.ConvertLead;
      pr.getParameters().put('id', leadId);
      Test.setCurrentPage(pr);

      NMD_ConvertLeadExtension ext = new NMD_ConvertLeadExtension(new ApexPages.StandardController(lead));
      //ext.convert();
       
		ext.ConvertToOppty();
      //assert the correct messages were given
      Boolean pass = false;
      for (ApexPages.Message msg : ApexPages.getMessages()) {
        System.debug(msg.getDetail());
        pass = pass || msg.getDetail().contains(Label.NMD_Lead_Converted);
      }
      System.assert(pass);

    }

  }

  static testMethod void test_ConvertLead_MissingFields() {
    
    List<SObject> result = getTestData();
    Id leadId = result[3].Id;

    PageReference pr = Page.ConvertLead;
    pr.getParameters().put('id', leadId);
    Test.setCurrentPage(pr);

    NMD_ConvertLeadExtension ext = new NMD_ConvertLeadExtension(new ApexPages.StandardController(new Lead(Id = leadId)));
    ext.convert();
	ext.clearselProcedure();
    ext.getProcedures();
    //assert the correct messages were given
    Boolean pass0 = false;
    Boolean pass1 = false;
    for (ApexPages.Message msg : ApexPages.getMessages()) {
        pass0 = pass0 || msg.getDetail().contains(Label.NMD_Patient_Required);
        pass1 = pass1 || msg.getDetail().contains(Label.NMD_Physician_Required);
    }
    System.assert(pass0);
    System.assert(pass1);

  }
    
  static testMethod void test_ConvertLead_AlreadyConverted() {

    NMD_TestDataManager td = new NMD_TestDataManager();
    User testuser = td.newUser('System Administrator');
    insert testuser;

    System.runAs(testuser) {
      
      List<SObject> result = getTestData();
      Id facilId = result[1].Id;
      Id physId = result[2].Id;
      Id leadId = result[3].Id;
      Id patientId = result[4].Id;

      Lead lead = td.newLead();
      lead.Id = leadId;
      lead.Account_Facility__c = facilId;
      lead.Trialing_Physician__c = physId;
      lead.Patient__c = patientId;
      lead.ConvertTo__c = TRIALTYPE;  
      update lead;

      NMD_LeadConversionManager lcm = new NMD_LeadConversionManager(leadId);
      lcm.convertLead();

      PageReference pr = Page.ConvertLead;
      pr.getParameters().put('id', leadId);
      Test.setCurrentPage(pr);

      NMD_ConvertLeadExtension ext = new NMD_ConvertLeadExtension(new ApexPages.StandardController(lead));
      //ext.convert();
		ext.ConvertToOppty();	
      //assertion
      Boolean pass = false;
      for (ApexPages.Message msg : ApexPages.getMessages()) {
          pass = pass || msg.getDetail().contains(Label.NMD_Lead_Already_Converted);
      }
      System.assert(pass);

    }

  }

}
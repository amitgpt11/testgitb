public with sharing class OrderAPIService {
    
    
    public static Order submitOrderRequest(String orderId,String operationType){
        //TODO: Query here for all the relevant fields and use it for assignment to Order Request object
        Order ord = [Select o.Sales_Order_No__c,o.OrderNumber, o.Id,o.Order_Reason__c,o.Type,o.PoNumber,o.PO_Type__c,o.Surgery_Date__c,o.Shared_Ship_To_Account_Number__c,o.Shared_Ship_To_Partner_Name__c,o.Account.Account_Number__c,o.Uro_PH_Mode_of_Transport__c,o.Shared_Billing_Block__c,o.shippingStreet,o.shippingCity,o.shippingPostalCode,o.shippingState,o.shippingCountry,o.Shared_Attention_line__c, o.ANZ_Implanting_Physician__r.Name, o.Ship_Date__c, o.Uro_PH_Delivery_Date__c, o.Uro_PH_Kit_Name__c, (Select Id,Order_Item_Id__c,Item_Number__c,Quantity,Material_Number_UPN__c,Shared_Line_Item_Stage__c,PricebookEntry.Product2.UPN_Material_Number__c,Uro_Ph_Loaner_Kit_Id__c From OrderItems) From Order o WHERE o.Id =:orderId];
        OrderRequest ordreq = createOrderRequest(ord,operationType);
        String reqBody = JSON.serialize(ordreq);
        System.debug('reqBody ---'+reqBody);
        CustomerServiceIntegrationDetail__c CSI = CustomerServiceIntegrationDetail__c.getValues('Insert Update Orders');
        String integrationUsername = CSI.Username__c; 
        String integrationPassword = CSI.Password__c;
        HttpRequest req = new HttpRequest();
        //req.setEndpoint('https://testgateway.bsci.com:4080/DEV/int/services/REST/Order/v1');
        req.setEndpoint(CSI.URL__c);
        req.setMethod('POST');
        Blob headerValue = Blob.valueOf(integrationUsername+ ':' + integrationPassword);
        String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('content-type', 'application/json');
        req.setHeader('Accept', 'application/json'); 
        req.setTimeout(120000);
        req.setClientCertificateName(CSI.Certificate__c);
        req.setBody(reqBody);
        
        if(!Test.isRunningTest()){
            Http http = new Http();
            
            try{
                HTTPResponse res = http.send(req);
                System.debug('==res body==='+res.getBody());
                
                
                if (res.getStatusCode() == 200){
                    OrderResponse ordRes = (OrderResponse)JSON.deserialize(res.getBody(),OrderResponse.class);
                    System.debug('===ordRes==='+ordRes);
                    if (null != ordRes.OrderResponse.orderNumber && !''.equals(ordRes.OrderResponse.orderNumber)){
                        ord.Shared_SAP_Order_Number__c = ordRes.OrderResponse.orderNumber;
                        
                        //A update ord;
                        return ord;
                    }else{
                        Application_Log__c error = new Application_Log__c();
                        error.Integration_Payload__c = req.getBody();
                        error.Order__c = (Id)orderId;
                        error.Log_Code__c = 'SAP Response Error';
                       // error.Message__c = res.getBody();
                        error.Integration_Resource__c = res.getBody();
                       
                        insert error;

                        System.debug('Error---'+ordRes.OrderResponse.item[0].responseDescription);
                    }
                }
                else {
                    Application_Log__c error = new Application_Log__c();
                        error.Integration_Payload__c = req.getBody();
                        error.Order__c = (Id)orderId;
                        error.Log_Code__c = 'SAP Response Error';
                        //error.Message__c = res.getBody();
                        error.Integration_Resource__c = res.getBody();
                       
                        insert error;
                }
            }
            Catch (Exception e){
                System.debug('####===e==='+e);
                System.debug('####===e.getMessage()==='+e.getMessage());
                Application_Log__c error = new Application_Log__c();
                error.Integration_Payload__c = req.getBody();
                error.Stack_Trace__c = e.getStackTraceString();
                error.Order__c = (Id)orderId;
                error.Log_Code__c = e.getTypeName();
                
                error.Message__c = e.getMessage().length() > 255 ? e.getMessage().substring(0,254) : e.getMessage();
                
                insert error;
                
                throw e;         
            }
        }else{
            ord.Shared_SAP_Order_Number__c = 'Test';
            return Ord;
        }
        return null;
    }
    
    public static OrderRequest createOrderRequest(Order ord,String oprType){
        
        OrderRequest ordreq = new OrderRequest();
        OrderRequest.cls_Order_v1 cls_Order_v1 = new OrderRequest.cls_Order_v1();
        OrderRequest.cls_OrderBasic cls_OrderBasic = new OrderRequest.cls_OrderBasic();
        OrderRequest.cls_OrderDelivery cls_OrderDelivery = new OrderRequest.cls_OrderDelivery();
        OrderRequest.cls_OrderBilling cls_OrderBilling = new OrderRequest.cls_OrderBilling();
        OrderRequest.cls_OrderSalesInfo cls_OrderSalesInfo = new OrderRequest.cls_OrderSalesInfo();
        OrderRequest.cls_CustPOInfo cls_CustPOInfo = new OrderRequest.cls_CustPOInfo();
        OrderRequest.cls_Price cls_Price = new OrderRequest.cls_Price();
        OrderRequest.cls_orderPrice cls_orderPrice = new OrderRequest.cls_orderPrice();
        OrderRequest.cls_Inventory cls_Inventory = new OrderRequest.cls_Inventory();
        OrderRequest.cls_SurgeryInfo cls_SurgeryInfo = new OrderRequest.cls_SurgeryInfo();
        OrderRequest.cls_OrderPartners cls_OrderPartners = new OrderRequest.cls_OrderPartners();
        OrderRequest.cls_SoldToPartner cls_SoldToPartner = new OrderRequest.cls_SoldToPartner();
        OrderRequest.cls_SoldTopartnerID cls_SoldTopartnerID = new OrderRequest.cls_SoldTopartnerID();
        OrderRequest.cls_ShipToPartner cls_ShipToPartner = new OrderRequest.cls_ShipToPartner();
        OrderRequest.cls_partnerID cls_partnerID = new OrderRequest.cls_partnerID();
        OrderRequest.cls_partnerAddress cls_partnerAddress = new OrderRequest.cls_partnerAddress();
        OrderRequest.cls_partnerName cls_partnerName = new OrderRequest.cls_partnerName();
        OrderRequest.cls_text cls_text = new OrderRequest.cls_text();
        list<Product2> loanerKit = null;
        
        
        //cls_OrderBasic.orderNumber = ord.OrderNumber;
        if(oprType.equalsignoreCase('Insert'))
        {
            cls_OrderBasic.orderNumber = ord.Id;
        }
        else
        {
            cls_OrderBasic.orderNumber = ord.Sales_Order_No__c;
        }
        
        cls_OrderBasic.orderReason = ord.Order_Reason__c != null ? ord.Order_Reason__c : '';//'G35';
        cls_OrderBasic.orderType = ord.Type != null ? ord.Type : '';//'ZKB';
        // cls_OrderBasic.orderReason = 'G35';
        // cls_OrderBasic.orderType = 'ZKB';
        
        cls_OrderBasic.operationType = oprType.equalsignoreCase('Insert') ? 'I' :'U';
        cls_Order_v1.OrderBasic = cls_OrderBasic;
        
        Map<String,MOTShippingConditionMapping__c> MSCM = MOTShippingConditionMapping__c.getall();
        
        cls_OrderDelivery.shippingCondition = (MSCM != null && MSCM.get(ord.Uro_PH_Mode_of_Transport__c) != null) ? MSCM.get(ord.Uro_PH_Mode_of_Transport__c).Shipping_Condition__c : '';
        cls_OrderDelivery.deliveryBlock = 'true';
        cls_OrderDelivery.requestedDeliveryDate = (ord.Uro_PH_Delivery_Date__c != NULL) ? String.ValueOf(ord.Uro_PH_Delivery_Date__c): '';//(ord.Ship_Date__c != NULL) ? String.ValueOf(ord.Ship_Date__c): '';
        cls_Order_v1.OrderDelivery = cls_OrderDelivery;
        
        cls_OrderBilling.billingBlock = ord.Shared_Billing_Block__c != null ? ord.Shared_Billing_Block__c : '';// 56 or 57   // 'false';
        cls_Order_v1.OrderBilling = cls_OrderBilling;
        
        loanerKit = [SELECT Uro_PH_Sales_Org__c FROM Product2 WHERE Name = :ord.Uro_PH_Kit_Name__c LIMIT 1];
        //cls_OrderSalesInfo.salesOrganization = 'US10';
        cls_OrderSalesInfo.salesOrganization = '';
        if(loanerKit.size() > 0){
            cls_OrderSalesInfo.salesOrganization = (ord.Uro_PH_Kit_Name__c != NULL) ? loanerKit.get(0).Uro_PH_Sales_Org__c : '';
        }
        cls_Order_v1.OrderSalesInfo = cls_OrderSalesInfo;
        
        cls_CustPOInfo.customerPurchaseOrderNumber = ord.PoNumber != null ? ord.PoNumber : '';//'Pending PO';
        //cls_CustPOInfo.customerPurchaseOrderType = ord.PO_Type__c != null ? ord.PO_Type__c : '';//'ConsignmentOrder';
        cls_CustPOInfo.customerPurchaseOrderType = 'ConsignmentOrder';
        cls_Order_v1.CustPOInfo = cls_CustPOInfo;
        
        cls_Text.textLine = ord.ANZ_Implanting_Physician__r.Name;
        
        List<OrderRequest.cls_ItemInfo> itemList = new List<OrderRequest.cls_ItemInfo>();
        //TODO: Commenting this loop and hardcoding values. Once the query is finalized. This should be uncommented
        for (OrderItem item : ord.OrderItems){
            System.debug('===item=='+item);
            OrderRequest.cls_ItemInfo cls_ItemInfo = new OrderRequest.cls_ItemInfo();
            cls_ItemInfo.itemNumber = item.Order_Item_Id__c != null ? String.valueOf(item.Order_Item_Id__c) : '';//'10';
            cls_ItemInfo.itemProposalNumber = String.IsNotBlank(item.Uro_Ph_Loaner_Kit_Id__c) ? item.Uro_Ph_Loaner_Kit_Id__c : '';//item.Item_Number__c != null ? item.Item_Number__c : '';//'50000819';
            cls_ItemInfo.orderQuantity = item.Quantity != null ? String.valueOf(Integer.ValueOf(item.Quantity)) : '';//'1';
            cls_ItemInfo.lineItemStatus = (String.IsNotBlank(item.Shared_Line_Item_Stage__c) && item.Shared_Line_Item_Stage__c.equalsIgnoreCase('Cancelled')) ? item.Shared_Line_Item_Stage__c : '';
            OrderRequest.cls_MaterialInfo cls_MaterialInfo = new OrderRequest.cls_MaterialInfo();
            cls_MaterialInfo.materialNumber = item.PricebookEntry.Product2.UPN_Material_Number__c != null ? item.PricebookEntry.Product2.UPN_Material_Number__c : '';//'M000100003';
            cls_ItemInfo.MaterialInfo = cls_MaterialInfo;
            itemList.add(cls_ItemInfo);
            System.debug('===cls_ItemInfo=='+cls_ItemInfo);
        }
       
        cls_Order_v1.ItemInfo = itemList;
        
        cls_Price.orderPrice = cls_orderPrice;
        cls_Order_v1.Price = cls_Price;
        
        cls_Order_v1.Inventory = cls_Inventory;
        
        cls_SurgeryInfo.surgeryDate = ord.Surgery_Date__c != null ? String.ValueOf(ord.Surgery_Date__c):'';//'2016-09-30';
        cls_Order_v1.SurgeryInfo = cls_SurgeryInfo;
        
        cls_SoldTopartnerID.ns4 = 'http://bsci.com/CIM/Base/v2';
        cls_SoldTopartnerID.ID = ord.Account.Account_Number__c;//'11899';
        cls_SoldToPartner.SoldTopartnerID = cls_SoldTopartnerID;
        cls_OrderPartners.SoldToPartner = cls_SoldToPartner;
        cls_partnerID.ns4 = 'http://bsci.com/CIM/Base/v2';
        cls_partnerID.ID = ord.Shared_Ship_To_Account_Number__c;
        
        cls_partnerAddress.attentionLine1 = String.IsNotBlank(ord.Shared_Attention_line__c) ? ord.Shared_Attention_line__c : '' ;
        cls_partnerAddress.attentionLine2 = '';
        cls_partnerAddress.cityName = String.IsNotBlank(ord.shippingCity) ? ord.shippingCity : '' ;
        cls_partnerAddress.stateCode = String.IsNotBlank(ord.shippingState) ? ord.shippingState : '' ;
        cls_partnerAddress.postalCode = String.IsNotBlank(ord.shippingPostalCode) ? ord.shippingPostalCode : '' ;
        cls_partnerAddress.countryCode = String.IsNotBlank(ord.shippingCountry) ? ord.shippingCountry : '' ;
        cls_partnerAddress.addressLine1 = String.IsNotBlank(ord.shippingStreet) ? ord.shippingStreet : '' ;
        
        cls_partnerName.Name = String.IsNotBlank(ord.Shared_Ship_To_Partner_Name__c) ? ord.Shared_Ship_To_Partner_Name__c : '' ;
        
        cls_ShipToPartner.partnerID = cls_partnerID;
        cls_ShipToPartner.partnerAddress = cls_partnerAddress;
        cls_ShipToPartner.partnerName = cls_partnerName;
        
        cls_OrderPartners.ShipToPartner = cls_ShipToPartner;
        cls_Order_v1.OrderPartners = cls_OrderPartners;
        
        ordreq.Order_v1 = cls_Order_v1;
        
        return ordreq;
    }
    
}
/**
* Batch Schedule class for 'BatchPatientCaseTMUpdate'
* @author   Mayuri - Accenture
* @date     20JUNE2016
*/
global class BatchPatientCaseTMUpdateSchedule implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
    integer batchSize = Integer.valueOf(system.Label.WatchmanPatientCaseTMUpdateBatchSize);
    
    try{
        BatchPatientCaseTMUpdate b = new BatchPatientCaseTMUpdate();
        database.executebatch(b,batchSize);
    }catch(Exception e){
        system.debug(e+'e');
    }

    }

}
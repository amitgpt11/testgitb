/**
 * Name : BlueprintResolvePricingControllerTest
 * Author : Accenture - Payal
 * Description : Test class used for testing the EndoTableauReportExtension
 * Date : 22June2016
 */
@isTest
private class EndoTableauReportExtensionTest{ 
    static testMethod void testMethod1(){      
      test.startTest();
      string str = 'test';
          Account acct1 = new Account(Name = 'ACCT1NAME',Account_Number__c='897623454',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  
          insert acct1;
          system.assertEquals(acct1.name,'ACCT1NAME');
          
          Account_Performance__c ap = new Account_Performance__c();
          ap.Division__c = 'Endo';
          ap.CY__c = '2015';
          ap.Facility_Name__c = acct1.id;
          insert ap;
          
          ApexPages.currentPage().getParameters().put('id', ap.Id);
          ApexPages.StandardController ctr = new ApexPages.StandardController(ap);
          EndoTableauReportExtension controller =  new  EndoTableauReportExtension(ctr);        
          Test.setCurrentPage(Page.TableauAccountPerformance);        
          controller.getTablueLinkInfo();
      test.stopTest();
    }   
    
    
    static testMethod void testMethod2(){      
      test.startTest();
      string str = 'test';
          Account acct2 = new Account(Name = 'ACCT1NAME1',Account_Number__c='',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  
          insert acct2; 
          system.assertEquals(acct2.name,'ACCT1NAME1');
          
          Account_Performance__c ap1 = new Account_Performance__c();
          ap1.Division__c = 'Endo';
          ap1.CY__c = '2015';
          ap1.Facility_Name__c = acct2.id;
          insert ap1;
          
          ApexPages.currentPage().getParameters().put('id', ap1.Id);
          ApexPages.StandardController ctr = new ApexPages.StandardController(ap1);
          EndoTableauReportExtension controller =  new  EndoTableauReportExtension(ctr);        
          Test.setCurrentPage(Page.TableauAccountPerformance);        
          controller.getTablueLinkInfo();
      test.stopTest();
    }     

}
/**
* Manager class for the Task object
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class TaskManager {
    
    final static SObjectType OBJECT_TYPE = Lead.SObjectType;
    Map<Id,Lead> lds = new Map<Id,Lead>{};
    Map<Id,Patient__c> pts = new Map<Id,Patient__c>{};
    Boolean isRecursive = LeadTriggerRecursivePrevention.getInstance().isSingleRun;

    private static String userProfileName;
    static List<Task> patientsTasksToCreate = new List<Task>();
    

    @testVisible
    final static String RAW_PATIENT_EDUCATION_SUBJECT = 'Patient Education+Physician+Patient Initials';

    

    public void createTasksOnPatients(List<Task> newTasks){
        for (Task task : newTasks) {
        if (task.WhoId != null
             && task.WhoId.getSObjectType() == OBJECT_TYPE && !isRecursive ){
             lds.put(task.WhoId, null);
            }
         }
        system.debug('############ :'+lds.KeySet());
        for (Lead lead : [
                select
                Id,
                Patient__c,
                Patient__r.id,
                Trialing_Physician__c
            from Lead
            where Id in :lds.keySet()
        ]) {
            lds.put(lead.Id, lead);
           // patients.put(lead.Patient__c, null);
        }
        system.debug('############ AFTER :'+lds);
        //system.debug('############ patients :'+patients);
        for (Task task : newTasks) {
            if (task.WhoId != null
             && task.WhoId.getSObjectType() == OBJECT_TYPE && !isRecursive){
                Lead parentLead = lds.get(task.WhoId);
                    if (parentLead == null) continue;
                system.debug('############ parentLead :'+parentLead);
                //Patient__c patient = patients.get(parentLead.Patient__c);
              //  Task t1= task.clone(false, true, true, false);
            //  system.debug('############ patient:'+patient);
              //  system.debug('############ t1.WhatId'+t1.WhatId);
               
                task.whoId=null;
                task.WhatId = parentLead.patient__c;
                system.debug('############ AFTER t1.WhatId'+task.WhatId);
               // patientsTasksToCreate.add(t1);
                system.debug('############ patientsTasksToCreate'+patientsTasksToCreate);
             }
         }
    
    }
    
    public void commitTasksOnPatients(){
        System.debug('############# isRecursive:'+isRecursive);
        if(patientsTasksToCreate!=null && !patientsTasksToCreate.isEmpty() && !isRecursive){
            LeadTriggerRecursivePrevention.getInstance().isSingleRun=true;
            System.debug('############# isRecursive AFTER:'+isRecursive);
            system.debug('########## '+patientsTasksToCreate);
            
            insert patientsTasksToCreate;
            patientsTasksToCreate= null;
            
        }
    }

    public static void updateNMDLeadPatientEducationTasks(List<Task> newTasks) {

        List<Task> patientTasks = new List<Task>{};
        Map<Id,Lead> nmdLeads = new Map<Id,Lead>{};
        Map<Id,Patient__c> patients = new Map<Id,Patient__c>{};
        Map<Id,Contact> physicians = new Map<Id,Contact>{};
        
        for (Task task : newTasks) {
            if (task.WhoId != null
             && task.WhoId.getSObjectType() == OBJECT_TYPE
             && task.Subject == RAW_PATIENT_EDUCATION_SUBJECT
            ) {

                patientTasks.add(task);
                nmdLeads.put(task.WhoId, null);

            }
        }

        if (patientTasks.isEmpty()) {
            
            return;
        }

        for (Lead lead : [
            select
                Id,
                Patient__c,
                Patient__r.id,
                Trialing_Physician__c
            from Lead
            where Id in :nmdLeads.keySet()
        ]) {
            nmdLeads.put(lead.Id, lead);
            //lds.put(lead.Id, lead);
            physicians.put(lead.Trialing_Physician__c, null);
            patients.put(lead.Patient__c, null);
        }
        

        physicians.putAll(new Map<Id,Contact>([
            select Id, Name
            from Contact
            where Id in :physicians.keySet()
        ]));

        patients.putAll(new Map<Id,Patient__c>([
            select Id, Patient_First_Name__c, Patient_Last_Name__c
            from Patient__c
            where Id in :patients.keySet()
        ]));

        for (Task task : patientTasks) {

            Lead parentLead = nmdLeads.get(task.WhoId);
            if (parentLead == null) continue;

            Patient__c patient = patients.get(parentLead.Patient__c);
            Contact physician = physicians.get(parentLead.Trialing_Physician__c);
            //if (patient == null || physician == null) continue;

            task.Subject = formatSubject(
                physician == null ? 'Physician' : physician.Name, 
                patient.Patient_First_Name__c, 
                patient.Patient_Last_Name__c
            );
        
            
            
            
        }

    }

    @testVisible
    private static String formatSubject(String physicianName, String patientFirstName, String patientLastName) {
        return String.format('Patient Education - {0} - {1}', new List<String> {
            physicianName,
            String.format('{0}.{1}.', new List<String> {
                patientFirstName.left(1).toUpperCase(),
                patientLastName.left(1).toUpperCase()
            })
        });
    }
    
    /* SFDC-1317 : Update Task division based on owner division if division is blank.
        Created By : Shashank Gupta (Accenture Team)
        Created date :  29/9/2016
    */
    // Code Starts
    
    Public void taskDivisionDetailInsert(Task newTask){
    Id TaskEuropeRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Europe').getRecordTypeId(); 
        if(newTask.Division__c == null && newTask.RecordTypeId == TaskEuropeRecordTypeId){
            newTask.Division__c = newTask.User_Division_Multi__c;
        }
    }
    
    Public void taskDivisionDetailUpdate(Task oldTask, Task newTask){
    Id TaskEuropeRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Europe').getRecordTypeId(); 
        if((oldTask.Division__c  != newTask.Division__c) && newTask.Division__c == null && newTask.RecordTypeId == TaskEuropeRecordTypeId){
            newTask.Division__c = newTask.User_Division_Multi__c;
        }
    }
    
    // Code Ends
    
    //public void fetchUserProfileName(){
    //  Id profileId = userInfo.getProfileId();
    //  userProfileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name; 
    //}

    //public static void makeUserTaskOwner(Task oldTask, Task newTask) {

    //  TaskManagerNoSharing.makeUserTaskOwner(oldTask, newTask, userProfileName);

        //if(oldTask.Make_Me_Owner__c != newTask.Make_Me_Owner__c
  //          && newTask.Make_Me_Owner__c == true
  //      ){
  //        if(profileNames.contains(userProfileName)){
     //         if(newTask.Status != 'Completed'){
  //                newTask.OwnerId = UserInfo.getUserId();
  //                } else {
  //                    newTask.addError('Completed tasks cannot be reassigned.');
        //        }
  //        } else {
  //            newTask.addError('Only users in the NMD IBU-PC user profile can make themselves the owner.');
  //        }
  //      }
  //        newTask.Make_Me_Owner__c = false;

    //}

}
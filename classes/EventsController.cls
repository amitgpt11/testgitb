/**
* Description: This class is used as controller of Events page
*/
public class EventsController extends AuthorizationUtil{
    
    public Community_Event__c objFeaturedEvent {get;set;}
    public List<String> lstMonths    {get;set;}
    public Map<String, List<Community_Event__c>> mapMonthToEvents {get;set;}
    public String selectedMonth {get;set;}
    public List<SelectOption> MonthsOfYear {get;set;}
    
    /**
        * Description: This method initlizes the list of Community Evnets to display on the Events page
    */
    public override void fetchRequestedData() {
        
        Boston_Scientific_Config__c objBSCustomSetting = Boston_Scientific_Config__c.getValues('Default');
        objFeaturedEvent = new Community_Event__c();
        
        if(objBSCustomSetting != null) {
            
            User objUser = [Select Shared_Community_Division__c 
                   From User 
                   Where Id =: UserInfo.getUserId()];
                   
            String featuredEventFieldAPIName = (objUser.Shared_Community_Division__c == null || objUser.Shared_Community_Division__c == 'Endo') ? 'Boston_AuthHome_Endo_FeaturedEvent_Id__c' : 'Boston_AuthHome_Cardio_FeaturedEvent_Id__c';
            
            if(objBSCustomSetting.get(featuredEventFieldAPIName) != null) {
                
                List<Community_Event__c> lstCommunityEvents = new List<Community_Event__c>([Select Shared_Display_Name__c, Shared_End_Date__c, Shared_Start_Date__c, Shared_Description__c,
                                                                                            Shared_Event_Hyperlink__c, Shared_Image_URL__c, Shared_Country__c
                                                                                            From Community_Event__c 
                                                                                            Where Id =: (Id)objBSCustomSetting.get(featuredEventFieldAPIName)
                                                                                            limit 1]);
                
                if(!lstCommunityEvents.isEmpty()){
                    
                    objFeaturedEvent = lstCommunityEvents[0];
                }
            }
        }
        
        objFeaturedEvent.Shared_Country__c = null;
        selectedMonth=null;
        applyFilters();
    }
    
    public void applyFilters(){
    
        system.debug('selectedCountry========='+objFeaturedEvent.Shared_Country__c);
        system.debug('selectedMonth========='+selectedMonth);
        String selectedCountry = objFeaturedEvent.Shared_Country__c;
        
        //This list holds the value of months from current month to previous month of next year
        lstMonths = new List<string>();
        MonthsOfYear = new List<SelectOption>();
        
        Map<Integer, String> mapIntToMonth = new Map<Integer, String>{
            1 => 'Boston_Month_January',
            2 => 'Boston_Month_February',
            3 => 'Boston_Month_March',
            4 => 'Boston_Month_April',
            5 => 'Boston_Month_May',
            6 => 'Boston_Month_June',
            7 => 'Boston_Month_July',
            8 => 'Boston_Month_August',
            9 => 'Boston_Month_September',
            10 => 'Boston_Month_October',
            11 => 'Boston_Month_November',
            12 => 'Boston_Month_December'};
                
        MonthsOfYear.add(new SelectOption('',Label.Boston_None));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_January,Label.Boston_Month_January));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_February,Label.Boston_Month_February));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_March,Label.Boston_Month_March));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_April,Label.Boston_Month_April));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_May,Label.Boston_Month_May));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_June,Label.Boston_Month_June));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_July,Label.Boston_Month_July));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_August,Label.Boston_Month_August));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_September,Label.Boston_Month_September));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_October,Label.Boston_Month_October));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_November,Label.Boston_Month_November));
        MonthsOfYear.add(new SelectOption(Label.Boston_Month_December,Label.Boston_Month_December));
        
        Integer intcurrentMonth = Date.Today().month();
        
        for(Integer intCM = intcurrentMonth; intCM <=12; intCM++) {
            
            lstMonths.add(mapIntToMonth.get(intCM));
        }
        
        for(Integer intFmToCm = 1; intFmToCm < intcurrentMonth; intFmToCm++) {
            
            lstMonths.add(mapIntToMonth.get(intFmToCm));
        }
        
       mapMonthToEvents  = new Map<String, List<Community_Event__c>>{
            'Boston_Month_January' => new List<Community_Event__c>(),
            'Boston_Month_February' => new List<Community_Event__c>(),
            'Boston_Month_March' => new List<Community_Event__c>(),
            'Boston_Month_April' => new List<Community_Event__c>(),
            'Boston_Month_May' => new List<Community_Event__c>(),
            'Boston_Month_June' => new List<Community_Event__c>(),
            'Boston_Month_July' => new List<Community_Event__c>(),
            'Boston_Month_August' => new List<Community_Event__c>(),
            'Boston_Month_September' => new List<Community_Event__c>(),
            'Boston_Month_October' => new List<Community_Event__c>(),
            'Boston_Month_November' => new List<Community_Event__c>(),
            'Boston_Month_December' => new List<Community_Event__c>()
            };
            
        User objUser = [Select Shared_Community_Division__c 
            From User 
            Where Id =: UserInfo.getUserId()];
                
        String query = 'SELECT Shared_Event_Quarter__c, Shared_Display_Name__c, Shared_City__c,  Shared_Start_Date__c,Shared_End_Date__c, Shared_Event_Hyperlink__c, Shared_Image_URL__c,'+
            'Shared_Description__c, Shared_Event_Month__c, Shared_Division1__c'+
            ' FROM Community_Event__c'+
            ' Where Shared_End_Date__c >= Today'+
            ' AND Shared_End_Date__c <= NEXT_N_MONTHS:12'+
            ' AND Shared_Division1__c = \''+ objUser.Shared_Community_Division__c +'\'';
                
        if(String.isNotBlank(selectedCountry)){
            
            query += ' AND Shared_Country__c =\''+ selectedCountry+ '\'';
        }
        
        if(String.isNotBlank(selectedMonth)){
            
            lstMonths.clear();
            lstMonths.add('Boston_Month_'+selectedMonth);
            mapMonthToEvents  = new Map<String, List<Community_Event__c>>{
            'Boston_Month_'+selectedMonth => new List<Community_Event__c>()};
            
            query += ' AND Shared_Event_Month__c =\''+ selectedMonth + '\'';
        }
        
        query += ' ORDER BY Shared_Start_Date__c ASC';
        
        for(Community_Event__c objCommunityEvent : Database.query(query)) {
                                                      
            system.debug('objCommunityEvent==='+objCommunityEvent.Shared_Event_Month__c);
            mapMonthToEvents.get('Boston_Month_'+objCommunityEvent.Shared_Event_Month__c).add(objCommunityEvent);
        }
    }
    
    public void clearFilters(){
        
        objFeaturedEvent.Shared_Country__c = null;
        selectedMonth=null;
        applyFilters();
    }
}
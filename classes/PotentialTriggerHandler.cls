/*
-------Created by Susannah St-Germain on 9/26/16 - Created PotentialsTriggerMethods class to hold methods that will be called from the Potentials trigger.
*/

public class PotentialTriggerHandler {
    //Handler Methods
    //BEFORE INSERT METHOD: Before records are inserted group them together in a list called newPotentials
    public void OnBeforeInsert (List<Potential__c> newPotentials){
     
        
        system.debug('These are the potentials Im trying to insert' +newPotentials);
        
        //Get the account performance IDs related to the new potentials
        List<ID> accountperf = new List <ID>();
        for(Potential__c n :newPotentials){
            accountperf.add(n.Account_Performance__c);
        }
        system.debug('These are the account performance IDs related to the new potential Im trying to insert' +accountperf);
            
        //Then get a list of all the existing Potentials currently in database related to the same account performance record as the newPotential
        List <Potential__c> currentPotentials = new List <Potential__c>([SELECT Product__c, Account_Performance__c, ID FROM Potential__c  
                      WHERE Account_Performance__r.ID in:accountperf]);
        system.debug(currentPotentials);
        
        //Get the string value of the names of products related to the current potentials
        set<string> currentPotentialName = new set <string>(); 
        for(Potential__c p :currentPotentials){
            currentPotentialName.add(p.Product__c);
            system.debug('These are the names of the products related to current potentials' +currentPotentialName);
        }
        
        //Get logged in user info
        
        User userRecord = [Select ID,Name,Profile.Name from User Where Id =:UserInfo.getUserId() limit 1];
       	String loggedInUserProfile = userRecord.Profile.Name;
        system.debug('The logged in user Profile is : '+loggedInUserProfile);
        
        //Create a list to hold dupes
        List <Potential__c> dupes= new List <Potential__c>();
        //Now compare the string values and add any duplicates to the new list
        for (Potential__c i: newPotentials) {
            if(currentPotentialName.contains(i.Product__c)){
                if(loggedInUserProfile.startsWith('ANZ')){
                dupes.add(i);
                system.debug('These are the dupes' +dupes);
                }}
                
        }
        
        //Now throw an error for duplicate new items and do not insert
            for (Potential__c dupe: dupes) {
         //   throw new BSCIException('Duplicate Item! There is already a potential for this product');         
         
         dupe.addError('Duplicate Item! There is already a potential for this product');

            }
        }        
    }
/*
This class will be used to identify user profile to show right Tableau report based on user profile and account number
Modified On : 24AUG2016
Modified By : Mayuri 
Modification comments : Added two new methods myCustomFieldMapper() and getSignedIdentity() to support tableau reports in SF1   
*/
public class EndoTableauReportExtensionForAccount{
    public Profile currentuserProfile{get;set;}
    public string SAPAccountIdonAccount{get;set;}
    public string UppercaseMailingNameonAccount{get;set;}
    public boolean isError{get;set;}    
    Public string finallinkforaccount{get;set;}
    Public string tableaulinkforpiusers{get;set;}
    Public string tableaulinkforcorpsusers{get;set;}
    Public string tableaulinkforurophusers{get;set;}
    Public string tableaulinkforwatchmanusers{get;set;}
    list<Account> AccRecords = new list<Account>();
   
    Id CurrentRecordId ;
    public string str {get; set;}
     
  public EndoTableauReportExtensionForAccount(ApexPages.StandardController controller){
     CurrentRecordId = Apexpages.currentPage().getParameters().get('Id');
     currentuserProfile=new Profile();
     currentuserProfile=[Select Id,Name from Profile where Id=:userinfo.getProfileId()];
     getTablueLinkInfoforaccount();
     
  } 
    
    public void getTablueLinkInfoforaccount(){
        AccRecords = [SELECT Id,name,Account_Number__c,Account_SAP_ID__c,Shared_Uppercase_Mailing_Name__c,Classification__c FROM Account WHERE Id =: CurrentRecordId and  Account_Number__c != null]; //query sap account number of current account        
        system.debug('AccRecords ****'+AccRecords);
        If(!AccRecords.isEmpty()){
            SAPAccountIdonAccount = AccRecords[0].Account_SAP_ID__c;
            UppercaseMailingNameonAccount = AccRecords[0].Shared_Uppercase_Mailing_Name__c;  
                finallinkforaccount = Label.EndoTableauReportforAccount+SAPAccountIdonAccount;
                tableaulinkforpiusers = Label.PI_Tableau_Sales_Report+SAPAccountIdonAccount;
                tableaulinkforurophusers = Label.UroPH_Tableau_Sales_Report+UppercaseMailingNameonAccount+'  - '+SAPAccountIdonAccount; 
                tableaulinkforwatchmanusers = Label.Watchman_Tableau_Sales_Report+SAPAccountIdonAccount; 
                If(AccRecords[0].Classification__c=='GPO')
                {                               
                    tableaulinkforcorpsusers = Label.Corps_Tableau_GPO_Account_Report+UppercaseMailingNameonAccount+'  - '+SAPAccountIdonAccount;
                    system.debug('****GPO***'+tableaulinkforcorpsusers);     
                }
                else {               
                    tableaulinkforcorpsusers = Label.Corps_Tableau_Sales_Report+UppercaseMailingNameonAccount+'  - '+SAPAccountIdonAccount;
                    system.debug('****Other***'+tableaulinkforcorpsusers);
                     }      
                isError = false;
                system.debug('finalLink ****'+finalLinkforaccount);
                system.debug('tableaulinkforpiusers ****'+tableaulinkforpiusers);
                system.debug('tableaulinkforcorpsusers ****'+tableaulinkforcorpsusers);
                system.debug('tableaulinkforurophusers ****'+tableaulinkforurophusers);  
                system.debug('tableaulinkforwatchmanusers ****' +tableaulinkforwatchmanusers);
                                     
         }
         else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Sales Data For Account'));
            isError = true;
         }
    }
     private String myCustomFieldMapper() {
        // This customer written class does the "work" need to map and get a Tableau username
        // In our example we use a static string "sfUser" as the string to map.
        //public StringMyUserId = UserInfo.getUserId();
        List<User> u = [Select FederationIdentifier from user where id=:UserInfo.getUserId()];
        System.debug('FEderated Id is ---'+u[0].FederationIdentifier);
        return u[0].FederationIdentifier;
    }
    
    /**
    * Gets the signed identity; always want to generate this in a getter since the constructor
    * only gets called on original page load and timestamp will skew
    */
    public String getSignedIdentity() {
        String signedIdentity = TableauSparklerUtilities.generateSignedIdentity(this.myCustomFieldMapper());
        system.debug('Singned Entity========'+signedIdentity);
        return signedIdentity;
    }
    
}

// :embed=y&:showShareOptions=true&:display_count=no&:showVizHome=no
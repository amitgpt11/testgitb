/*
 @CreatedDate     5 Oct 2016                                  
 @author          Ashish-Accenture
 @Description     Delete loaner kit relationship records based on Loaner kit Staging table object.
       
*/

public class batchLoanerKitRelDelete implements Database.Batchable<sObject>, Database.stateful{
     
     Map<String,Uro_PH_Loaner_Kit_Staging_Table__c> mapLoanerKitStage;
     public batchLoanerKitRelDelete(Map<String,Uro_PH_Loaner_Kit_Staging_Table__c> mapStage){
         mapLoanerKitStage = new Map<String,Uro_PH_Loaner_Kit_Staging_Table__c>();
         mapLoanerKitStage = mapStage;
     }
     
     public Database.QueryLocator start(Database.BatchableContext BC) {
         String query = 'Select id,Uro_PH_Loaner_Kit__c,Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c , Uro_PH_Product__c,Uro_PH_Product__r.UPN_Material_Number__c,Uro_PH_Quantity__c from Uro_PH_Loaner_Kit_Relationship__c order by Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c';
        return database.getquerylocator(query);
     }
     
     public void execute(Database.BatchableContext BC, List<Uro_PH_Loaner_Kit_Relationship__c> scope) {
         System.debug('===mapLoanerKitStage==='+mapLoanerKitStage);
         if(mapLoanerKitStage != null && mapLoanerKitStage.size() > 0){
             list<Uro_PH_Loaner_Kit_Relationship__c> lstToDelete = new list<Uro_PH_Loaner_Kit_Relationship__c>();
             for(Uro_PH_Loaner_Kit_Relationship__c Lkrel : scope){
                 System.debug('====Lkrel===='+Lkrel);
                 if(Lkrel.Uro_PH_Loaner_Kit__c == null || Lkrel.Uro_PH_Product__c == null ||
                     Lkrel.Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c == null || Lkrel.Uro_PH_Product__r.UPN_Material_Number__c == null ||
                      !mapLoanerKitStage.containsKey(Lkrel.Uro_PH_Loaner_Kit__r.Uro_Ph_Loaner_Kit_Id__c.tolowercase()+Lkrel.Uro_PH_Product__r.UPN_Material_Number__c.tolowercase())){
                     
                     lstToDelete.add(Lkrel);
                 }
             }
             
             System.debug('===lstToDelete===='+lstToDelete);
             if(lstToDelete.size() > 0){
                 try{
                     Database.Delete(lstToDelete,false);
                 }catch(Exception e){
                     System.debug('####==e==='+e);
                 }
             }
         }
         
     }
     
      public void finish(Database.BatchableContext BC){
      }
}
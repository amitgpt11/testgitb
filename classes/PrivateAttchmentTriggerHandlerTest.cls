/**
* Trigger for the Private Attchment to create Custom Object
*
* @Author Accenture Services
* @Date 21SEPT2016
*/
@isTest(SeeAllData=false)
private class PrivateAttchmentTriggerHandlerTest {
    public static list<User> usrLst = new list<User>();
    public static List<Territory2Type> Ttype;  
    public static Id Territory2ModelIDActive;
    public static List<Territory2> trList=new List<Territory2>();   
    @testSetup
    static void setUpdata(){
        User user1 = UtilForUnitTestDataSetup.newUser('US IC User');
        user1.Username = 'UserNameIC1@xyz.com';
        usrLst.add(user1);
        User user2 = UtilForUnitTestDataSetup.newUser('US IC User');
        user2.Username = 'UserNameIC2@xyz.com';
        usrLst.add(user2);
        User user3 = UtilForUnitTestDataSetup.newUser('US PI User');
        user3.Username = 'UserNamePI1@abc.com';
        usrLst.add(user3);
        User user4 = UtilForUnitTestDataSetup.newUser('US PI User');
        user4.Username = 'UserNamePI2@abc.com';
        usrLst.add(user4);   
        User user5 = UtilForUnitTestDataSetup.newUser('US PI User');
        user5.Username = 'UserNamePI3@abc.com';
        usrLst.add(user5);       
         
        insert usrLst;
        
        Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA1234',Territory2ModelIDActive,Ttype[0].Id);   
        trList.add(t1);              
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('AA1245',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t2); 
        insert trList; 
        System.assertEquals(trList[0].Territory2ModelId,Territory2ModelIDActive);        
        
        list<UserTerritory2Association> userTerrLst = new list<UserTerritory2Association>();
        for(integer i=0;i<2;i++){
            UserTerritory2Association nAssc1 = new UserTerritory2Association();
            nAssc1.RoleInTerritory2 = 'Territory Manager';
            nAssc1.Territory2Id = trList[0].Id;
            nAssc1.UserId = usrLst[i].Id;
            userTerrLst.add(nAssc1);
        }
        for(integer i=2;i<4;i++){
            UserTerritory2Association nAssc1 = new UserTerritory2Association();
            nAssc1.RoleInTerritory2 = 'Territory Manager';
            nAssc1.Territory2Id = trList[1].Id;
            nAssc1.UserId = usrLst[i].Id;
            userTerrLst.add(nAssc1);
        }
        
        insert userTerrLst;
    }

    
    static testMethod void test_PAShareInsert(){
        List<Territory2> trLst = new list<Territory2>([Select Id,name From Territory2 where name Like : 'AA%'  ]);  
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%']);
        Account acc1 = new Account();
        acc1.Name ='Test Account1';
        acc1.ShippingStreet = 'street';
        acc1.ShippingCity = 'city';
        acc1.ShippingState = 'state';
        acc1.ShippingPostalCode = '123445';
        acc1.Account_Number__c = '123456';                                
        insert acc1;
        system.assertEquals(acc1.Account_Number__c,'123456');
        
        Account acc2 = new Account();
        acc2.Name ='Test Account2';
        acc1.Account_Number__c = '679087';                                
        insert acc2;
        
        ObjectTerritory2Association accountAssociation1 = new ObjectTerritory2Association();
        accountAssociation1.ObjectId = acc1.Id;
        accountAssociation1.Territory2Id = trLst[0].Id;
        accountAssociation1.AssociationCause = 'Territory2Manual';
        insert accountAssociation1;
        
        ObjectTerritory2Association accountAssociation2 = new ObjectTerritory2Association();
        accountAssociation2.ObjectId = acc2.Id;
        accountAssociation2.Territory2Id = trLst[1].Id;
        accountAssociation2.AssociationCause = 'Territory2Manual';
        insert accountAssociation2;
        
        AccountTeamMember Teammemberad=new AccountTeamMember();
        Teammemberad.AccountId=acc1.Id;
        Teammemberad.UserId=urLst[4].Id;
        Teammemberad.TeamMemberRole = 'CA P&C Analyst';
        insert Teammemberad;
        
        
        list<Shared_Private_Attachments__c> FinalLst = new list<Shared_Private_Attachments__c>();
        
        for(integer i=0;i<50;i++){
            Shared_Private_Attachments__c pa = new Shared_Private_Attachments__c();
            pa.Account__c = acc1.Id;
            //pa.division__c = 'PI';
            FinalLst.add(pa);
        }
        for(integer i=0;i<50;i++){
            Shared_Private_Attachments__c pa = new Shared_Private_Attachments__c();
            pa.Account__c = acc2.Id;
           // pa.division__c = 'IC';
            FinalLst.add(pa);
        }
        PrivateAttachmentShared_ProfileData__c profile = new PrivateAttachmentShared_ProfileData__c();
        profile.Name = 'IC';
        profile.Profile_Names__c = 'US IC User';
        insert profile;
        PrivateAttachmentShared_ProfileData__c profile1 = new PrivateAttachmentShared_ProfileData__c();
        profile1.Name = 'PI';
        profile1.Profile_Names__c = 'US PI User';
        insert profile1;
        
        test.startTest();
            insert FinalLst;
        
        list<Shared_Private_Attachments__c> UpdateLst = new list<Shared_Private_Attachments__c>();
        for(Shared_Private_Attachments__c p : FinalLst){
            p.Account__c = acc2.Id;
            UpdateLst.add(p);
        }
        
            update UpdateLst;
        test.stoptest();
    }
    

}
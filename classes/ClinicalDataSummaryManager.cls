/**
* Manager class for the Clinical Data Summary object trigger
*
* @Author salesforce Services
* @Date 03/01/2015
*/
public with sharing class ClinicalDataSummaryManager {
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Clinical_Data_Summary__c.getRecordTypeInfosByName();

    public static final Id RECTYPE_PAINMAP = RECTYPES.get('Pain Map').getRecordTypeId();
    public static final Id RECTYPE_ANNOTATIONS = RECTYPES.get('Annotations').getRecordTypeId();
    public static final Id RECTYPE_ANZPAINMAP = RECTYPES.get('ANZ Pain Map').getRecordTypeId();

}
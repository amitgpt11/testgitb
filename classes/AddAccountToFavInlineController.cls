/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Controller for AddAccountToFavInline VF Page: Determines whether a user had added the selected record as a favorite, for inline display of the favorites notification
@Requirement Id  User Story : US2674
*/

public class AddAccountToFavInlineController {
    List<Account_Favorite__c> lstFavAccount;
    public boolean showAddButton{get;set;}
    public String messageText{get;set;}
    Id accId;
    Id accountFavId;
    public AddAccountToFavInlineController (ApexPages.StandardController controller){
        accId = controller.getId();
        showAccountFavorites();
        
        System.debug('==URL===='+ApexPages.currentPage().getParameters());
        System.debug('==URL1===='+ApexPages.currentPage());
        System.debug('==URL2===='+ApexPages.currentPage().getURL());
        
    }
    
    public void showAccountFavorites(){
        lstFavAccount = [Select id, Name, Account__c, User__c from Account_Favorite__c where Account__c = :AccId and User__c = :UserInfo.getUserId() ];
        if(lstFavAccount.size() > 0){
            accountFavId = lstFavAccount.get(0).id;
            showAddButton = false;
          
            messageText = 'This record is a favorite.';
        }else{
            showAddButton = true;
        }
    }
      
}
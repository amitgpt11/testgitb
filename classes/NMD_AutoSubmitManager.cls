/**
* Class for automatically submitting Patient, Account, and Contact records
*
* @Author Salesforce Services
* @Date 2016/03/18
*/
public with sharing class NMD_AutoSubmitManager {

    private Boolean emailsQueued = false;
    public SObjectType objType;

    private List<SObject> validRecords = new List<SObject>();
    
    
//###S  
static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
public static final Id RECTYPE_IMPLANT = RECTYPES_OPPTY.get('NMD SCS Implant').getRecordTypeId();
public static final Id RECTYPE_TRIAL = RECTYPES_OPPTY.get('NMD SCS Trial').getRecordTypeId();
    
    
    /*public static final Id RECTYPE_TRIAL = RECTYPES.get('NMD SCS Trial').getRecordTypeId();
    public static final Id RECTYPE_IMPLANT = RECTYPES.get('NMD SCS Implant').getRecordTypeId();*/
    public static final Set<Id> RECTYPES_NMD_OPPTY = new Set<Id> {
        RECTYPE_TRIAL, RECTYPE_IMPLANT
    };
    
//###E
    private Map<Id, Opportunity> oppWithTrialDate = new Map<Id, Opportunity>();
    private Map<Id, Opportunity> oppWithProcedureDate = new Map<Id, Opportunity>();

    public NMD_AutoSubmitManager() {

    }

    /**
    * Sets SObject type, and queries for related Opportunity records
    * 
    * @param SObjectType objType
    * @param List<SObject> objList
    */  
    public void init(SObjectType objType, List<SObject> objList){
        this.objType = objType;

        Set<Id> recordIds = new Set<Id>();

        for(SObject obj : objList){
            recordIds.add(obj.Id);
        }

        fetchRelatedOpportunities(recordIds);
    }

    /**
    * Queries for related Opportunity records
    * 
    * @param Set<Id> recordIds
    */  
    public void fetchRelatedOpportunities(Set<Id> recordIds){
   // system.debug('========RECTYPE_IMPLANT==='+RECTYPE_IMPLANT+'========RECTYPE_TRIAL==='+RECTYPE_TRIAL+'====RECTYPES_NMD_OPPTY==='+RECTYPES_NMD_OPPTY);    
        if (!recordIds.isEmpty()) {

            String queryField = this.objType.getDescribe().getName();

            String whereClause = 'WHERE ' + queryField + ' IN :recordIds';
            
//###S      //added by Amitabh as per US2471......
            String RTCheck = ' AND RecordTypeId IN: RECTYPES_NMD_OPPTY';
//###E
            if(queryField == 'Account'){ 
                whereClause = 'WHERE (Trialing_Account__c IN :recordIds ';
                whereClause += 'OR Procedure_Account__c IN :recordIds)';
            }

            if(queryField == 'Contact'){ 
                whereClause = 'WHERE (Trialing_Physician__c IN :recordIds ';
                whereClause += 'OR Procedure_Physician__c IN :recordIds)';
            }

            String oppQuery = 'SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId, ';
            oppQuery += 'Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c, ';
            oppQuery += 'Trialing_Account__c, Procedure_Account__c, ';
            oppQuery += 'Trialing_Physician__c, Procedure_Physician__c ';
            oppQuery += 'FROM Opportunity ' + whereClause + RTCheck; //###

            //For Account and Contact, if the Scheduled Trialing Date is populated and that record is set in the
            //  Opportunity Trialing field, put it in the Trialing Map.  Same for Procedures.
            //  Patients__c only have 1 related field so put them in both.
            //
            //  These maps are checked later during validation.  We will submit a Trialing Account only if
            //      there is an opportunity with that record in the Trialing field.  Same for Procedures.
           // system.debug('1oppQuery==='+oppQuery);
            if(this.objType == Patient__c.getSObjectType()){
                for (Opportunity oppty : Database.query(oppQuery)){
            system.debug('oppQuery==='+oppQuery);               
                    if(oppty.Scheduled_Trial_Date_Time__c != null && oppty.RecordTypeId == RECTYPE_TRIAL){ //###
                        oppWithTrialDate.put(oppty.Patient__c, oppty);
                    }
                    if(oppty.Scheduled_Procedure_Date_Time__c != null && oppty.RecordTypeId == RECTYPE_IMPLANT){ //###
                        oppWithProcedureDate.put(oppty.Patient__c, oppty);
                    }
                }
            } else if(this.objType == Account.getSObjectType()){
                for (Opportunity oppty : Database.query(oppQuery)){
                    if(oppty.Scheduled_Trial_Date_Time__c != null
                        && oppty.Trialing_Account__c != null && oppty.RecordTypeId == RECTYPE_TRIAL ////###
                    ){
                        oppWithTrialDate.put(oppty.Trialing_Account__c, oppty);
                    }
                    if(oppty.Scheduled_Procedure_Date_Time__c != null
                        && oppty.Procedure_Account__c != null && oppty.RecordTypeId == RECTYPE_IMPLANT //###
                    ){
                        oppWithProcedureDate.put(oppty.Procedure_Account__c, oppty);
                    }
                }
            } else if(this.objType == Contact.getSObjectType()){
                for (Opportunity oppty : Database.query(oppQuery)){
                    if(oppty.Scheduled_Trial_Date_Time__c != null
                        && oppty.Trialing_Physician__c != null && oppty.RecordTypeId == RECTYPE_TRIAL   //###
                    ){
                        oppWithTrialDate.put(oppty.Trialing_Physician__c, oppty);
                    }
                    if(oppty.Scheduled_Procedure_Date_Time__c != null
                        && oppty.Procedure_Physician__c != null && oppty.RecordTypeId == RECTYPE_IMPLANT  //###
                    ){
                        oppWithProcedureDate.put(oppty.Procedure_Physician__c, oppty);
                    }
                }
            }

        }

    }


    /**
    * If there are any records that pass through the validation:
    *       Patient: Update the record which will trigger a workflow to send the update
    *       Account and Contact: get the record Ids and pass them to a future method to send emails to Quincy
    *
    * @param    void
    */
    public void queueQuincyEmails() {

        if (this.emailsQueued) {
            return;
        }

        this.emailsQueued = true;

        
        if(!validRecords.isEmpty()){
            Set<Id> sendUpdateIds = new Set<Id>();

            for(SObject obj : validRecords){
                sendUpdateIds.add(obj.Id);
            }

            if(!sendUpdateIds.isEmpty()){

                if (this.objType == Patient__c.getSobjectType()) {
                    List<Patient__c> patients = new List<Patient__c>{};
                    for (Patient__c patient : (List<Patient__c>) validRecords) {
                        patient.BSN_Update__c = 'Request SAP ID';
                    }
                } else if (this.objType == Account.getSObjectType()
                    || this.objType == Contact.getSObjectType()
                ){
                    sendQuincyEmails(sendUpdateIds);
                    system.debug('======='+ 'In queueQuincyEmails()');
                } 

            }

            validRecords.clear();

        }

    }

    /**
    * Pass each record to the appropriate validation method
    *
    * @param    void
    */
    public void validateRecord(SObject obj){

        if(this.objType == Patient__c.getSObjectType()){
            if(isReadyToSubmit((Patient__c) obj)){
                validRecords.add(obj);
            }
        } else if(this.objType == Account.getSObjectType()){
            if(isReadyToSubmit((Account) obj)){
                validRecords.add(obj);
            }
        } else if(this.objType == Contact.getSObjectType()){
            if(isReadyToSubmit((Contact) obj)){
                validRecords.add(obj);
            }
        } 

    }

    /**
    * Returns False if any of the required fields (for SAP submission) are missing.
    *
    * @param    Contact contact
    */
    private Boolean isReadyToSubmit(Contact contact) {

        // check the required fields for submission
        // 6.23 addedd Academic_Title__c,Specialty__c to required fields as per US2471 by Amitabh
        if ((!oppWithTrialDate.containsKey(contact.Id)
                && !oppWithProcedureDate.containsKey(contact.Id)
            )
         || (String.isBlank(contact.FirstName)
                && String.isBlank(contact.LastName)
            )
         || contact.RecordTypeId != ContactManager.RECTYPE_PROSPECT
         || contact.AccountId == null
         || String.isBlank(contact.MailingStreet)
         || String.isBlank(contact.MailingCity)
         || String.isBlank(contact.MailingState)
         || String.isBlank(contact.MailingPostalCode)
         || String.isBlank(contact.MailingCountry)
         || contact.Territory_ID__c == null
         || String.isBlank(contact.Phone)
         || String.isBlank(contact.Fax)
         || String.isBlank(contact.Academic_Title__c)
         || String.isBlank(contact.Specialty__c) 
        ) {
            return false;
        }

        return true;

    }

    /**
    * Returns False if any of the required fields (for SAP submission) are missing.
    *
    * @param    Account acct
    */
    private Boolean isReadyToSubmit(Account acct) {

        // check the required fields for submission
        // 6.23 addedd Type,Classification to required fields as per US2471 by Amitabh
        if ((!oppWithTrialDate.containsKey(acct.Id)
                && !oppWithProcedureDate.containsKey(acct.Id)
            )
         || acct.RecordTypeId != AccountManager.RECTYPE_PROSPECT
         || String.isBlank(acct.Name)
         || String.isBlank(acct.BillingStreet)
         || String.isBlank(acct.BillingCity)
         || String.isBlank(acct.BillingState)
         || String.isBlank(acct.BillingPostalCode)
         || String.isBlank(acct.BillingCountry)
         //|| String.isBlank(acct.Sold_to_Attn__c)
         || String.isBlank(acct.Sold_to_Street__c)
         || String.isBlank(acct.Sold_to_City__c)
         || String.isBlank(acct.Sold_to_State__c)
         || String.isBlank(acct.Sold_to_Zip_Postal_Code__c)
         || String.isBlank(acct.Sold_to_Country__c)
         || String.isBlank(acct.Sold_to_Phone__c)
         || String.isBlank(acct.Sold_to_Fax__c)
         || acct.NMD_Territory__c == null
         || String.isBlank(acct.Type)
         || String.isBlank(acct.Classification__c)  
        ) { 
            return false;
        }

        return true;

    }

    /**
    * Returns False if any of the required fields (for SAP submission) are missing.
    *
    * @param    Patient__c patient
    */
    private Boolean isReadyToSubmit(Patient__c patient) {
system.debug('=====patient====='+patient);
        // check the required fields for submission
        if ((!oppWithTrialDate.containsKey(patient.Id)
                && !oppWithProcedureDate.containsKey(patient.Id)
            )
         || patient.RecordTypeId != PatientManager.RECTYPE_PROSPECT
         || String.isBlank(patient.Patient_First_Name__c)
         || String.isBlank(patient.Patient_Last_Name__c)
         || String.isBlank(patient.Address_1__c)
         || String.isBlank(patient.City__c)
         || String.isBlank(patient.State__c)
         || String.isBlank(patient.Country__c)
         || String.isBlank(patient.County__c)
         || String.isBlank(patient.Zip__c)
         || (String.isBlank(patient.Patient_Phone_Number__c)
                && String.isBlank(patient.Patient_Mobile_Number__c)
            )
         || patient.Patient_Date_of_Birth__c == null
         || String.isBlank(patient.Patient_Gender__c)
         || String.isNotBlank(patient.SAP_ID__c)
        ) {
            return false;
        }

        return true;
    }

    /**
    * for any map of recordIds tht have a need for their associated Accounts or Contact to be automatically submitted to Quincy
    *
    * @param    Map     recordIds
    */
    @Future
    public static void sendQuincyReminder(Map<Id,Id> recordIds) {

        final String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

        final String baseMessage = new List<Id>(recordIds.keySet())[0].getSobjectType() == Contact.SObjectType
            ? 'Your scheduled surgery requires this physician to have a SAP ID.  Please submit all required information to Quincy as soon as possible.\n\nPhysician Record: {0}'
            : 'Your scheduled surgery requires this Account to have a SAP ID.  Please submit all required information to Quincy as soon as possible.\n\nAccount Record: {0}';

        Map<Id,String> userEmails = new Map<Id,String>{};
        for (User u : [
            select Email
            from User
            where Id in :recordIds.values()
        ]) {
            userEmails.put(u.Id, u.Email);
        }

        List<Messaging.Email> messages = new List<Messaging.Email>{};

        for (Id recordId : recordIds.keySet()) {
            String body = String.format(baseMessage, new List<String> { baseUrl + '/' + recordId });
            
            SendEmailManager emailManager = new SendEmailManager(
                'Required Fields Missing',
                body, userEmails.get(recordIds.get(recordId))
            );

            messages.add(emailManager.generateEmail());

        }

        Messaging.sendEmail(messages);

    }

    /**
    * For the Set of Ids, send out emails to quincy with the record information
    *
    * @param    Set<Id> recordIds
    */
    @Future
    public static void sendQuincyEmails(Set<Id> recordIds) {

        // get template names
        QuincyReportSettings__c settings = QuincyReportSettings__c.getInstance();
        Set<String> templateNames = new Set<String> {
            settings.Account_EmailTemplate_DeveloperName__c, //NMD_Quincy_Prospect_Report  //###
            settings.Contact_EmailTemplate_Developername__c  //NMD_Quincy_Physician_Report //###
        };

        // get/map templates
        Map<String,EmailTemplate> templates = new Map<String,EmailTemplate>();
        for (EmailTemplate template : [
            select DeveloperName, Subject
            from EmailTemplate 
            where DeveloperName in :templateNames
        ]) {
            templates.put(template.DeveloperName, template);
        }

        // must have all the necessary templates
        System.assertEquals(templateNames.size(), templates.size(), 'Expected to find all the relevant Email Templates.');

        List<Messaging.Email> messages = new List<Messaging.Email>();
        List<Attachment> attachments = new List<Attachment>();
        List<SObject> updates = new List<SObject>();

        DML.deferLogs();

        // create temporary contact to send the emails
        Contact quincy = new Contact(
            LastName = 'Quincy Contact',
            Email = settings.Quincy_Email__c
        );
        DML.save('', quincy);
        String quincyId = quincy.Id;

        // create individualized messages/attachments
        for (Id recordId : recordIds) {

            PageReference reportReference;
            String templateName;

            if (recordId.getSobjectType() == Account.SObjectType) {
                reportReference = Page.QuincyProspectReport;
                templateName = settings.Account_EmailTemplate_DeveloperName__c;
            }
            else if (recordId.getSobjectType() == Contact.SObjectType) {
                reportReference = Page.QuincyPhysicianReport;
                templateName = settings.Contact_EmailTemplate_DeveloperName__c;
            }

            EmailTemplate template = templates.get(templateName);

            // generate the PDF attachment
            reportReference.getParameters().putAll(new Map<String,String> { 'id' => recordId });
            Blob content = Test.isRunningTest() ? Blob.valueOf('X') : reportReference.getContent();
            Attachment pdf = new Attachment(
                Name = 'report.pdf', 
                Body = content
            );

            SendEmailManager emailManager = recordId.getSObjectType() == Account.SObjectType
                ? new SendEmailManager(template, quincyId, recordId)
                : new SendEmailManager(template, quincyId);

            emailManager.addAttachment(pdf);
            messages.add(emailManager.generateEmail());

            // mark record as sent
            SObject record = recordId.getSObjectType().newSObject(recordId);
            record.put('Sent_to_Quincy__c', true);
            updates.add(record);
            

        }

        // send the emails
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(messages);

        // make sure they all were queued successfully
        for (Messaging.SendEmailResult result : results) {
            if (!result.isSuccess()) {
                DML.log(new ApplicationLogWrapper(
                    'ERROR', 
                    'NMD_OrderBillingFormManager', 
                    'generateAndEmailBillingForm', 
                    (result.getErrors()[0]).getMessage()
                ));
            }
        }

        DML.save('', updates, false);
        System.debug('=========='+updates);
        DML.remove('', quincy);

        DML.flushLogs();

    }


}
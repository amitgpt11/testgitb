@isTest(SeeAllData=false)
private class AccountTeamMembersBatchTest {

	static final String ACCTNAME = 'AcctName';
	static final String EXTERNALID1 = 'EXTERNALID1';
	static final String EXTERNALID2 = 'EXTERNALID2';
	static final String USERNAMEA = 'user.a@bsci.com.test';
	static final String USERNAMEB = 'user.b@bsci.com.test';

	static final List<String> roles;

	static {

		roles = new List<String>{};
		for(Schema.PicklistEntry ple : Schema.SObjectType.Account_Team_Member__c.fields.Account_Team_Role__c.getPicklistValues()) {
			roles.add(ple.getValue());
		}

	}
	
	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		// create an admin and two users
		User admin = td.newUser('System Administrator');
		
		User userA = td.newUser('Standard User');
		userA.Username = USERNAMEA;
		
		User userB = td.newUser('Standard User');
		userB.Username = USERNAMEB;
		
		insert new List<User> { 
			admin, userA, userB 
		};

		// switch user context
		System.runAs(admin) {

			// create account
			Account acct = new Account(
				Name = ACCTNAME,
				RecordTypeId = AccountManager.RECTYPE_CUSTOMER
			);
			insert acct;	

			// add userA to the account
			insert new AccountTeamMember(
				AccountId = acct.Id,
				UserId = userA.Id,
				TeamMemberRole = roles[roles.size()-1]
			);

			// manually create a share for userA
			insert new AccountShare(
                AccountId = acct.Id,
                UserOrGroupId = userA.Id,
                AccountAccessLevel = 'Edit',
                OpportunityAccessLevel = 'None'
            );

			// create personnel Id records for each user
			Personnel_ID__c personA = new Personnel_ID__c(
				Personnel_ID__c = EXTERNALID1,
				User_for_account_team__c = userA.Id
			);
			Personnel_ID__c personB = new Personnel_ID__c(
				Personnel_ID__c = EXTERNALID2,
				User_for_account_team__c = userB.Id
			);
			insert new List<Personnel_ID__c> { personA, personB };

		}

	}

	static testMethod void test_AccountTeamMembersBatch_Add() {

		// create records to add userB to account
		Account acct = [select Id from Account where Name like :(ACCTNAME + '%')];
		User userB = [select Id from User where Username = :USERNAMEB];
		Personnel_ID__c personB = [select Id from Personnel_ID__c where User_for_account_team__c = :userB.Id];
		String role = roles[0];

		Account_Team_Member__c member = new Account_Team_Member__c(
			Account_Team_Role__c = role,
			Account_Team_Share__c = acct.Id,
			Deleted__c = false,
			Personnel_ID__c = personB.Id
		);
		insert member;
		
		List<AccountTeamMember> atms1 = new List<AccountTeamMember>([
			select Id
			from AccountTeamMember
			where AccountId = :acct.Id
			and UserId = :userB.Id
			and TeamMemberRole = :role
		]);
		system.debug('>>>>>>>>>>>>> : '+atms1);

		Test.startTest();

		//execute the batch
		AccountTeamMembersBatch.runNow();

		Test.stopTest();

		// assert that the account team member was created
		List<AccountTeamMember> atms = new List<AccountTeamMember>([
			select Id
			from AccountTeamMember
			where AccountId = :acct.Id
			and UserId = :userB.Id
			and TeamMemberRole = :role
		]);
		system.debug('>>>>>>>>>>>>> : '+atms);
		System.assertEquals(1, atms.size());

		// assert that the account share was created
		List<AccountShare> shares = new List<AccountShare>([
			select AccountAccessLevel
			from AccountShare
			where AccountId = :acct.Id
			and UserOrGroupId = :userB.Id
		]);
		//System.assertEquals(1, shares.size());
		//System.assertEquals('Edit', shares[0].AccountAccessLevel);

	}

	static testMethod void test_AccountTeamMembersBatch_Update() {

		// update the account team member to a new role
		Account acct = [select Id from Account where Name like :(ACCTNAME + '%')];
		User userA = [select Id from User where Username = :USERNAMEA];
		Personnel_ID__c personA = [select Id from Personnel_ID__c where User_for_account_team__c = :userA.Id];
		String role = roles[0];

		Account_Team_Member__c member = new Account_Team_Member__c(
			Account_Team_Role__c = role,
			Account_Team_Share__c = acct.Id,
			Deleted__c = false,
			Personnel_ID__c = personA.Id
		);
		insert member;

		Test.startTest();

		//execute the batch
		AccountTeamMembersBatch.runNow();

		Test.stopTest();

		// assert that the account team member was updated
		List<AccountTeamMember> atms = new List<AccountTeamMember>([
			select Id
			from AccountTeamMember
			where AccountId = :acct.Id
			and UserId = :userA.Id
			and TeamMemberRole = :role
		]);
		System.assertEquals(1, atms.size());

	}

	static testMethod void test_AccountTeamMembersBatch_Remove() {

		// remove the existing team member role
		Account acct = [select Id from Account where Name like :(ACCTNAME + '%')];
		User userA = [select Id from User where Username = :USERNAMEA];
		Personnel_ID__c personA = [select Id from Personnel_ID__c where User_for_account_team__c = :userA.Id];
		String role = roles[0];

		Account_Team_Member__c member = new Account_Team_Member__c(
			Account_Team_Role__c = role,
			Account_Team_Share__c = acct.Id,
			Deleted__c = true,
			Personnel_ID__c = personA.Id
		);
		insert member;

		Test.startTest();

		//execute the batch
		AccountTeamMembersBatch.runNow();

		Test.stopTest();

		// assert that the account team member was removed
		List<AccountTeamMember> atms = new List<AccountTeamMember>([
			select Id
			from AccountTeamMember
			where AccountId = :acct.Id
			and UserId = :userA.Id
			and TeamMemberRole = :role
		]);
		System.assertEquals(0, atms.size());

	}

}
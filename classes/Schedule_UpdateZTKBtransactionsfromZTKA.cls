/*
 * Cron Job: 
 * Schedule_UpdateZTKBtransactionsfromZTKA  m = new Schedule_UpdateZTKBtransactionsfromZTKA ();
String sch = '0 15 * ? * *';
String jobID = system.schedule('Update ZTKBs', sch, m);
*/
global class Schedule_UpdateZTKBtransactionsfromZTKA implements Schedulable {
    global void execute(SchedulableContext sc) {
        UpdateZTKBtransactionsfromZTKA b = new UpdateZTKBtransactionsfromZTKA(); 
        database.executebatch(b,50);
    }
}
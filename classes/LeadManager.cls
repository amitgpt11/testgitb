/**
* Manager class for the Lead object
*
* @Author salesforce Services
* @Date 03/23/2015
*/
public with sharing class LeadManager {
    
    static final String profileName = 'API User Profile';
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_LEAD = Schema.SObjectType.Lead.getRecordTypeInfosByName();  
    
    public static final Id RECTYPE_NMD_PATIENTS = RECTYPES_LEAD.get('NMD Patient').getRecordTypeId();
    public static final Set<String> LEAD_STATUS_TYPES = new Set<String> {
        'Disqualified (Closed)', 
        'Converted'
    };
    public static final Set<Id> RECTYPES_NMD = new Set<Id> {
        LeadManager.RECTYPE_NMD_PATIENTS
    };      
    static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};
    private Map<Id,Seller_Hierarchy__c> territoriesById = new Map<Id,Seller_Hierarchy__c>{};
    private Map<String,Seller_Hierarchy__c> territoriesByRepCode = new Map<String,Seller_Hierarchy__c>{};
    private Map<Id,Patient__c> patientMap = new Map<Id, Patient__c>{};
    private List<Patient__c> patientUpdateList = new List<Patient__c>{};
    private Map<Id,Lead> leadUpdateMap = new Map<Id,Lead>{};    
    
    //Create Territory Map with Current TM1 Assignee and Create Territory Map with Current TM1 Assignee and Rep Code  
    public void fetchLeadTerritories(List<Lead> leads) {
        
        Set<Id> leadTerritoryIds = new Set<Id>{};
        Set<String> repCodes = new Set<String>{};
        for (Lead lead : leads) {
            if (lead.Territory_ID__c != null) {
                leadTerritoryIds.add(lead.Territory_ID__c);
            }
            if (String.isNotBlank(lead.Rep_Code__c)) {
                repCodes.add(lead.Rep_Code__c);
            }
        }

        if (!leadTerritoryIds.isEmpty() && trigger.isUpdate) {
            this.territoriesById.putAll([
                select Current_TM1_Assignee__c
                from Seller_Hierarchy__c
                where Id in :leadTerritoryIds
                and Current_TM1_Assignee__c != null
            ]);
        }

        if (!repCodes.isEmpty() && trigger.isInsert) {
            for (Seller_Hierarchy__c tty : [
                select Current_TM1_Assignee__c, Rep_Code__c
                from Seller_Hierarchy__c
                where Rep_Code__c in :repCodes
                and Current_TM1_Assignee__c != null
            ]) {
                this.territoriesByRepCode.put(tty.Rep_Code__c, tty);
            }
        }

    }   

    //Create PatientMap from Patient ID's on Leads 
    public void fetchLeadPatients(List<Lead> leads) {

        Set<Id> leadPatientIds = new Set<Id>();
        for (Lead lead : leads) {
            if (lead.Patient__c != null) {
                leadPatientIds.add(lead.Patient__c);
            }
        }

        if (!leadPatientIds.isEmpty()) {
            this.patientMap = new Map<Id, Patient__c>([
                select 
                    Physician_of_Record__c,
                    Source__c
                from Patient__c 
                where Id in :leadPatientIds
            ]);
        }

    }

    public void setLeadSource(Lead lead) {

        Patient__c patient = this.patientMap.get(lead.Patient__c);
        if (patient != null
            && String.isNotBlank(patient.Source__c)
        ) {
            lead.LeadSource = patient.Source__c;
        }

    }

	//insert Lead Owner and Territory ID
    public void insertLeadOwner(Lead lead) {
        if (lead.RecordTypeId == RECTYPE_NMD_PATIENTS
         && lead.Territory_ID__c == null
         && String.isNotBlank(lead.Rep_Code__c)
         && territoriesByRepCode.containsKey(lead.Rep_Code__c)
        ) {
            Seller_Hierarchy__c territory = territoriesByRepCode.get(lead.Rep_Code__c);
            lead.Territory_ID__c = territory.Id;
            lead.OwnerId = territory.Current_TM1_Assignee__c;
        }
    }

    //Update Lead Owner
    public void updateLeadOwner(Lead oldLead, Lead lead){
        if ((lead.RecordTypeId == RECTYPE_NMD_PATIENTS &&
           !LEAD_STATUS_TYPES.contains(lead.Status) &&
           lead.Territory_ID__c != oldLead.Territory_ID__c
        ) && (lead.Territory_ID__c != null && territoriesById.containsKey(lead.Territory_ID__c))) {
            lead.OwnerId = territoriesById.get(lead.Territory_ID__c).Current_TM1_Assignee__c;
            leadUpdateMap.put(lead.Id,lead);
        }
    }

    //Update Patient's Physician of Record
    public void updatePatientPhysician(Lead oldLead, Lead lead){
        if ((lead.RecordTypeId == RECTYPE_NMD_PATIENTS && lead.Trialing_Physician__c != oldLead.Trialing_Physician__c)
         && (this.patientMap.containsKey(lead.Patient__c))) {
            Patient__c patient = patientMap.get(lead.Patient__c);
            if (patient.Physician_of_Record__c != lead.Trialing_Physician__c) {
                patient.Physician_of_Record__c = lead.Trialing_Physician__c;
                this.patientUpdateList.add(patient);
            }
        }       
    }
    
    public void commitPatientUpdateList() {
        if (!this.patientUpdateList.isEmpty()) {
            try {
                //update this.patientUpdateList;
                DML.save(this, this.patientUpdateList);
            }
            catch (System.DmlException de) {

            }
        }
    }
    
    public void createFeedForLead() {
    	if (!this.leadUpdateMap.isEmpty()) {
    		for (Id leadId : leadUpdateMap.keyset()) {

        		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
				ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
				ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
				ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
				messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
				
				mentionSegmentInput.id = leadUpdateMap.get(leadId).OwnerId;
				messageBodyInput.messageSegments.add(mentionSegmentInput);
				
				String messageString = '\n A new Lead has been assigned to you \n';
				
				textSegmentInput.text = messageString;
				messageBodyInput.messageSegments.add(textSegmentInput);
				
				feedItemInput.body = messageBodyInput;
				feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
				feedItemInput.subjectId = leadId;
				ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);

    		}
    	}
    }
    
    //recalculate sharing table
    public void leadSharing(List<Lead> leads) {

        // just build the sets of expected users for each patient
        Map<Id,Set<Id>> userIdMap = new Map<Id,Set<Id>>(); // Lead.Id => | User.Id |
        Map<Id,List<Lead>> leadsByTtyId = new Map<Id,List<Lead>>(); // Lead.Territory_ID__c => [ Lead ]

        // map each patient by their territory
        for (Lead lead : leads) {
            if (!RECTYPES_NMD.contains(lead.RecordTypeId)) continue;
            if (lead.Territory_ID__c == null) continue;

            userIdMap.put(lead.Id, new Set<Id>());

            if (leadsByTtyId.containsKey(lead.Territory_ID__c)) {
                leadsByTtyId.get(lead.Territory_ID__c).add(lead);
            }
            else {
                leadsByTtyId.put(lead.Territory_ID__c, new List<Lead> { lead });
            }
            
        }

        if (userIdMap.isEmpty()) return;

        // queue a share for each active user (assignee) in the territory
        for (Assignee__c assignee : [
            select
                Assignee__c,
                Territory__c
            from Assignee__c
            where Territory__c in :leadsByTtyId.keySet()
            and Assignee__r.IsActive = true
        ]) {

            for (Lead lead : leadsByTtyId.get(assignee.Territory__c)) {
                // add assignee to lead mapping
                userIdMap.get(lead.Id).add(assignee.Assignee__c);
            }

        }

        // apply delta shares.  this will add/remove shares as needed
        new SObjectSharingManager(LeadShare.getSObjectType(), userIdMap.keySet()).applyDeltaShares(userIdMap);

    }   
            
}
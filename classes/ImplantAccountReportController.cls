/*
@CreatedDate     21SEP2016
@author          Payal Ahuja
@Description     Contains information about implants and procedures scheduled on Account object.
@Requirement Id  SFDC- 1404
*/

public class ImplantAccountReportController {
  
  public Map<String,Integer> implantCountMap{get;set;}
  public List<String> displayOrderList{get;set;}
  
  public ImplantAccountReportController(ApexPages.StandardController controller){
    Id accountId = ((Account)controller.getRecord()).Id;
  
    displayOrderList = new List<String>{Label.Implants,Label.Successful_Implants,Label.Procedures_for_Week,Label.Procedures_for_Month};
    implantCountMap = new Map<String,Integer>{Label.Implants => 0,Label.Successful_Implants => 0,Label.Procedures_for_Week => 0,Label.Procedures_for_Month => 0};
    
    for (LAAC_Implant__c implant :[SELECT Id, LAAC_Implanting_Physician__c, LAAC_Referring_Physician_LU__c, LAAC_Secondary_Implanting_Physician_LU__c, LAAC_Case_Aborted__c,LAAC_Facility__c FROM LAAC_Implant__c WHERE LAAC_Facility__c = :accountId]) {   
        if (implant != null){            
            Integer primaryCount = implantCountMap.get(Label.Implants);
            implantCountMap.put(Label.Implants,++primaryCount);
                        
            if (!implant.LAAC_Case_Aborted__c){
                Integer primarySuccess = implantCountMap.get(Label.Successful_Implants);
                implantCountMap.put(Label.Successful_Implants,++primarySuccess);
            }
        } 
     }
     for (Shared_Patient_Case__c procedure :[Select Id,Shared_Start_Date_Time__c,Shared_End_Date_Time__c,Shared_Facility__c FROM Shared_Patient_Case__c WHERE Shared_Facility__c =:accountId]){
 
          if (procedure != null) {
              if((procedure.Shared_Start_Date_Time__c >= Date.Today()-1) && (procedure.Shared_End_Date_Time__c <= Date.Today()+6)){
                  Integer primaryCountForWeek = implantCountMap.get(Label.Procedures_for_Week);
                  implantCountMap.put(Label.Procedures_for_Week,++primaryCountForWeek);
              }
              if(((procedure.Shared_End_Date_Time__c).Month() == Date.Today().Month())){
                  Integer primarySuccessForMonth = implantCountMap.get(Label.Procedures_for_Month);
                  implantCountMap.put(Label.Procedures_for_Month,++primarySuccessForMonth);
              }
          }
     }
    
   }
}
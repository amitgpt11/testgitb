/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OpenQInboundHandler {
    global OpenQInboundHandler() {

    }
    webService static openq.OpenQInboundHandler.Response createKOLShellProfile(List<openq.OpenQInboundHandler.ContactInfo> contactInfo) {
        return null;
    }
global class ContactInfo {
    @WebService
    webService String firstName;
    @WebService
    webService String lastName;
    @WebService
    webService String middleName;
    @WebService
    webService Integer profileRequestId;
    @WebService
    webService String sfUserName;
    global ContactInfo() {

    }
}
global class ErrorKOL {
    @WebService
    webService String code;
    @WebService
    webService String description;
    @WebService
    webService Integer profileRequestId;
    global ErrorKOL() {

    }
}
global class Response {
    @WebService
    webService List<openq.OpenQInboundHandler.ErrorKOL> errorResp;
    @WebService
    webService List<openq.OpenQInboundHandler.SuccessKOL> successResp;
    global Response() {

    }
}
global class SuccessKOL {
    @WebService
    webService String code;
    @WebService
    webService String description;
    @WebService
    webService Integer profileRequestId;
    @WebService
    webService String SFDCRecordId;
    global SuccessKOL() {

    }
}
}

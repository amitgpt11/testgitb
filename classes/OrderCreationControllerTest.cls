@isTest 
private class OrderCreationControllerTest {
 
 static final Map<String,Schema.RecordTypeInfo> RECTYPESORD = Schema.SObjectType.Order.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROPHLOANERKIT = RECTYPESORD.get('Uro/PH Loaner Kit').getRecordTypeId();
    
   static final Map <String,Schema.RecordTypeInfo> recordTypes_Product2_LK_Kit = Product2.sObjectType.getDescribe().getRecordTypeInfosByName();
   public static Id recordTypeID_Product2_LK_Kit = recordTypes_Product2_LK_Kit.get('Uro/PH Loaner Kit').getRecordTypeId();   
 
@testSetup 
static void setupData() {

Map <String,Schema.RecordTypeInfo> recordTypes_Account = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id recordTypeID_Account = recordTypes_Account.get('Customer').getRecordTypeId();

Account lk_Facility = new Account(
                                Name ='Test Account',
                                RecordTypeId = recordTypeID_Account,
                                Account_Number__c = 'test123',
                                shippingCountry = 'US'
                                );
insert lk_Facility;

List<Contact> uroPh_LK_Contact_List = new List<Contact>(); 

Map <String,Schema.RecordTypeInfo> recordTypes_Contact = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Contact = recordTypes_Contact.get('UroPH General').getRecordTypeId();

for(Integer i=1; i<4;i++){

Contact uroPh_LK_Contact = new Contact(
                                FirstName = 'Test',
                                LastName = 'Leaner Kit'+i,
                                MailingStreet = '100 Boston Scientific Way',
                                MailingCity = 'Marlborough', 
                                MailingPostalCode = '01752',
                                MailingState = 'MA',
                                MailingCountry = 'United States',
                                AccountId = lk_Facility.Id,
                                RecordTypeId = recordTypeID_Contact
                                );  
uroPh_LK_Contact_List.add(uroPh_LK_Contact);                                
}//End of Loop

insert uroPh_LK_Contact_List;

Map <String,Schema.RecordTypeInfo> recordTypes_Patient_Case = Shared_Patient_Case__c.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Patient_Case = recordTypes_Patient_Case.get('Urology Men\'s Health').getRecordTypeId();



Shared_Patient_Case__c patientCase = new Shared_Patient_Case__c(
                                        Shared_Implanting_Physician__c = uroPh_LK_Contact_List[0].Id,
                                        Shared_Primary_Case_Contact__c = uroPh_LK_Contact_List[1].Id,
                                        Shared_Facility__c = lk_Facility.Id,
                                        Shared_Start_Date_Time__c = System.Now(),
                                        Shared_End_Date_Time__c = System.Now() + 3,
                                        RecordTypeId = recordTypeID_Patient_Case
                                     );
insert patientCase;


    
Map <String,Schema.RecordTypeInfo> recordTypes_Product2_UroPH = Product2.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Product2_UroPH = recordTypes_Product2_UroPH.get('UroPH').getRecordTypeId(); 


 
List<Product2> product_List = new List<Product2>();

for(Integer i=1;i<=20;i++){
 
Product2 prod = new Product2(
                    Name = 'Loanker Kit Product '+i, 
                    Family = 'Medical',
                    currencyIsoCode = 'USD',
                    Uro_PH_Sales_Org__c = 'Test',
                    RecordTypeId = (i < 11 ? recordTypeID_Product2_UroPH : recordTypeID_Product2_LK_Kit)
                    );
product_List.add(prod);
} 
      
insert product_List;

Pricebook2 priceBook_UroPH = new Pricebook2(
                                Name = 'UroPH', 
                                isActive = true,
                                CurrencyIsoCode = 'USD'
                                );
insert priceBook_UroPH;

List<PricebookEntry> prod_Entry_List1 = new List<PricebookEntry>();


for(Product2 prod : product_List){

PricebookEntry prod_Entry = new PricebookEntry(
                                Pricebook2Id = Test.getStandardPricebookId(), 
                                Product2Id = prod.Id,
                                UnitPrice = 12000,
                                UseStandardPrice=false,
                                IsActive = true
                                );
prod_Entry_List1.add(prod_Entry);
}

    insert prod_Entry_List1;
    
List<PricebookEntry> prod_Entry_List = new List<PricebookEntry>();
   //prod_Entry_List.clear();
    
for(Product2 prod : product_List){

PricebookEntry prod_Entry = new PricebookEntry(
                                Pricebook2Id = priceBook_UroPH.Id, 
                                Product2Id = prod.Id,
                                UnitPrice = 12000,
                                UseStandardPrice=false,
                                IsActive = true,
                                CurrencyIsoCode ='USD'
                                );
prod_Entry_List.add(prod_Entry);    


}   

insert prod_Entry_List;     



List<Uro_PH_Loaner_Kit_Relationship__c> lk_Relationship_List = new List<Uro_PH_Loaner_Kit_Relationship__c>();
for(Product2 prod : product_List){
if(prod.RecordTypeId == recordTypeID_Product2_UroPH){
Uro_PH_Loaner_Kit_Relationship__c lk_Relationship = new Uro_PH_Loaner_Kit_Relationship__c(
                                                        Uro_PH_Product__c = prod.Id,
                                                        Uro_PH_Quantity__c = 2.0
                                                        );

    lk_Relationship_List.add(lk_Relationship);
    }
}

integer count = 0;
for(Product2 prod : product_List){
    if(prod.RecordTypeId == recordTypeID_Product2_LK_Kit){
        lk_Relationship_List[count].Uro_PH_Loaner_Kit__c = prod.Id;
        count++;
    }
}

insert lk_Relationship_List;

UtilForUnitTestDataSetup.createBusinessCloseTimeCountry(); // Insert Custom setting Business Close Time 
UtilForUnitTestDataSetup.createCustomerServiceIntegrationDetail(); // Insert Custom setting Customer Service Integration

}//End of SetupData 


 
static testMethod void TestSave() {
    Test.startTest();   
    
   
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    List<Contact> uroPh_LK_Contact_List = [SELECT Id, MailingAddress,MailingCity,MailingCountry,MailingGeocodeAccuracy,MailingPostalCode,MailingState,MailingStreet FROM Contact];
  //A  List<Uro_PH_Loaner_Kit_Assignment__c > lk_Assignment_List =  [SELECT Id,Name,Uro_PH_Facility_Name__c,Uro_PH_Loaner_Kit__c,Uro_PH_Loaner_Kit__r.Name FROM Uro_PH_Loaner_Kit_Assignment__c ];
    List<Product2> lk_Assignment_List = [SELECT Id,Name,Uro_PH_Sales_Org__c FROM Product2 Where recordtypeId =:recordTypeID_Product2_LK_Kit];
     Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

   
    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    String shippingAddrsId = uroPh_LK_Contact_List[0].Id;
    orderCreationRd.ShippingAddressID = shippingAddrsId ;   
    //A orderCreationRd.SelectedItem = lk_Assignment_List[0].Uro_PH_Loaner_Kit__c;
    orderCreationRd.SelectedItem = lk_Assignment_List[0].Id; 
    orderCreationRd.poNumber = 'PO12354';
    orderCreationRd.Alternate_BSCI_Resouce = Userinfo.getUserId();
   
    orderCreationRd.LoanerKitAssignments();
    System.assert(orderCreationRd.SKUList.Size() > 0);
    orderCreationRd.getSAPShippingAddress();
    
    orderCreationRd.save();
    orderCreationRd.callOrderRESTService();
    Test.stopTest();
    }    
     
 
static testMethod void TestGetLoanerKitItems() {
    Test.startTest();   
    
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    
    List<SelectOption> returnLoanerKitProducts = orderCreationRd.getLoanerKitItems();
    System.assert(returnLoanerKitProducts.Size() > 0);
    
    Test.stopTest();
    }//End of getItems method    

 
static testMethod void TestGetSAPShippingAddress() {
    Test.startTest();   
    
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    
    List<SelectOption> returnShippingAddress = orderCreationRd.getSAPShippingAddress();
    System.assert(returnShippingAddress .Size() > 0);
        
    Test.stopTest();
    }//End of GetSAPShippingAddress method  
 
 
static testMethod void TestGetShippingAddress() {
    Uro_PH_Non_Working_Day_Calendar__c NWDC = new Uro_PH_Non_Working_Day_Calendar__c();
     NWDC.Uro_PH_Year__c = String.ValueOf(date.Today().Year());
     NWDC.Name = 'US';
     Insert NWDC;
     
    Test.startTest();   
    
    List<Shared_Patient_Case__c> patientCase = [Select id,Shared_Start_Date_Time__c,Shared_End_Date_Time__c,shared_facility__c from Shared_Patient_Case__c limit 1];
    Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    
    orderCreationRd.ShippingAddressID = 'Test';
    orderCreationRd.getShippingAddress();
    
    orderCreationRd.getSAPShippingAddress();
    System.debug('===Test===');
    orderCreationRd.validateOrderCreation();
   // orderCreationRd.calcModeOfTransport('US',System.now(),Date.Today(),true);
    
    patientCase.get(0).Shared_Start_Date_Time__c = System.Now()+10;
    patientCase.get(0).Shared_End_Date_Time__c = System.Now() + 20;
    update patientCase;
    orderCreationRd = new OrderCreationController(stc);
    orderCreationRd.ShippingAddressID = 'Test';
    orderCreationRd.getShippingAddress();
    
    orderCreationRd.getSAPShippingAddress();
    System.debug('===Test===');
    orderCreationRd.validateOrderCreation();
    System.assert(orderCreationRd.estShippingDate != null);
    
    String MOT = OrderCreationController.calcModeOfTransport('US',Datetime.newInstance(2016,10,3),Date.newInstance(2016,9,30),true,OrderCreationController.fetchLstNonWorkingDays('US',Datetime.newInstance(2016,10,3)));
    System.assert(MOT.equalsignorecase('FedEx Saturday'));
    
    MOT = OrderCreationController.calcModeOfTransport('US',Datetime.newInstance(2016,10,3),Date.newInstance(2016,9,30),false,OrderCreationController.fetchLstNonWorkingDays('US',Datetime.newInstance(2016,10,3)));
    System.assert(MOT.equalsignorecase('FedEx Next Day Early AM'));
    
    Date Estdate = OrderCreationController.calculateShippingDate('US',System.now().adddays(1),OrderCreationController.fetchLstNonWorkingDays('US',System.now().adddays(1)));
    System.assert(Estdate == date.today() || Estdate == date.today().adddays(-1) || Estdate == date.today().adddays(-2));
    
    Date Estdate1 = OrderCreationController.calculateShippingDate('US',System.now().adddays(14 + (Math.MOD(Date.today().daysBetween(Date.newInstance(2016,11,2)),7))),OrderCreationController.fetchLstNonWorkingDays('US',System.now().adddays(14 + (Math.MOD(Date.today().daysBetween(Date.newInstance(2016,11,2)),7))))); // to cover code to shift ship date from saturday to friday
    System.debug('===Estdate1===='+Estdate1);
    
    Date Estdate2 = OrderCreationController.calculateShippingDate('US',System.now().adddays(2),OrderCreationController.fetchLstNonWorkingDays('US',System.now().adddays(2)));
    System.debug('===Estdate2===='+Estdate2);
    
    Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
    insert customPB;
    Order ordObj = new Order(AccountId = patientCase.get(0).shared_facility__c,Uro_PH_Patient_Case__c = patientCase.get(0).id, RecordTypeId = RECTYPE_UROPHLOANERKIT,
                                  EffectiveDate= date.Today(),Status='Draft', PriceBook2Id = customPB.Id ); 
    Insert ordObj;
    
    orderCreationRd = new OrderCreationController(stc);
    
    
    //System.assert(selectedAddress != null);
        
    Test.stopTest();
    }//End of GetSAPShippingAddress method  
    
    
    
}
/**
* Extension class for the Physician Practice Summary Dashboard vf page
*
* @Author salesforce Services
* @Date 04/07/2015
*/
public with sharing class NMD_PhysicianDashboardExtension {
	
	final ApexPages.StandardController sc;

	public List<String> reportIds { get; private set; }

	public NMD_PhysicianDashboardExtension(ApexPages.StandardController sc) {
		this.sc = sc;

		this.reportIds = new List<String>{};
		for (PhysicianDashboard__c dashboard : [
			select Report_Id__c
			from PhysicianDashboard__c
			where Active__c = true
			order by Order__c
			limit 50
		]) {
			reportIds.add(dashboard.Report_Id__c);
		}

	}

}
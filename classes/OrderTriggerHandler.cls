/**
* Trigger Handler class for the Order Sobject
*
* @Author salesforce Services
* @Date 2015-06-08
*/
public with sharing class OrderTriggerHandler extends TriggerHandler {
    
    //final OrderManager manager = new OrderManager();
    final OrderManager manager = OrderManager.getInstance();

    /**
    *  @desc    Bulk before method
    */
    public override void bulkBefore() {
        if (!Trigger.IsDelete) {
            manager.fetchRelatedRecords(trigger.new);
        }
    }

    /**
    *  @desc    before Insert method
    */
    public override void beforeInsert(SObject obj) {

        Order newOrder = (Order)obj;

        manager.updateCommissionInfo(newOrder);
        manager.setNMDPricebook(newOrder);
        manager.beforeInsertForUroPHOrder(newOrder); // Added by Ashish to update Stage and Ship date field===sfdc-1489==
        
        
       //  manager.OpportunityRevenueUpdatesFromOrder(newOrder);
       
       // manager.checkForOpportunityUpdate(newOrder);

    }
    
  /**
    * @desc after Insert method
    */    
    
    public override void afterInsert(SObject newObj) {

         Order newOrder = (Order)newObj;
         manager.checkForOpportunityUpdate(newOrder); // sum revenue amount 
         //manager.OpportunityRevenueUpdatesFromOrder(newOrder);
        // manager.UpdateOptyCloseDateOnOrderInsert(newOrder);
         manager.UpdateOptyOrderNumberOnOrderInsert(newOrder);
         // for record type Uro/PH Loaner Kit
        manager.createProcedure(newOrder);   //===Added by Ashish =to Create Procedure on Order insert===sfdc-1489==Start==

    }/**/   
    
    /**
    *  @desc    before Delete method
    */
    public override void beforeDelete(SObject obj) {

        manager.checkDeleteValidity( (Order)obj );
        
    }   
    
    /**
    *  @desc    after Delete method
    */
    public override void afterDelete(SObject obj) {

        
        
    }   
    
    /**
    *  @desc    after undelete method
    */
    public override void afterUndelete(SObject obj) {

        manager.checkDeleteValidity( (Order)obj );
        
    }   
    
   
    

    /**
    * @desc before Update method
    */
    public override void beforeUpdate(SObject oldObj, SObject newObj) {

        Order oldOrder = (Order)oldObj;
        Order newOrder = (Order)newObj;

        manager.updateCommissionInfo(newOrder);
        manager.updateProductInfo(oldOrder, newOrder);
        manager.calculateFSRHoldTimes(oldOrder, newOrder);
        
        manager.beforeupdateForUroPHOrder(oldOrder, newOrder); // Added by Ashish== to update Stage and Ship date field===sfdc-1489==   

        //manager.verifySubmit(oldOrder, newOrder);
        //manager.updateStageToBillingBlock(oldOrder, newOrder);
     //   manager.checkForOpportunityUpdate(newOrder);

    }

    public override void bulkAfter() {
        if (trigger.isInsert) {
            manager.orderSharing(trigger.new);
        }
        if (trigger.isUpdate) {
            manager.fetchRelatedOpptys(Trigger.new);
            //manager.updateLineItems(Trigger.oldMap,Trigger.newMap);
            //***Below method added by Amitabh for ZTKA issue fix
            manager.createInventoryTransactionRecordforMultiOrder(Trigger.oldMap,Trigger.newMap);
            manager.updateOrderItemsStage(Trigger.oldMap,Trigger.newMap); //===Added by Ashish =to update Stage value on Order products===sfdc-1489==Start==
            manager.updateProcedureOwner(Trigger.oldMap,Trigger.newMap); //===Added by Ashish ==to update owner on Procedure===sfdc-1489==Start==
        }
    }
    
    /**
    * @desc after Update method
    */
    public override void afterUpdate(SObject oldObj, SObject newObj) {

        Order oldOrder = (Order)oldObj;
        Order newOrder = (Order)newObj;
        // below method commented by amitabh (for ZTKA issue) to call identical method from Bulk after update with name createInventoryTransactionRecordforMultiOrder
        //manager.createInventoryTransactionRecord(oldOrder, newOrder);
        manager.checkCancelledTransferOrder(oldOrder, newOrder);
        manager.confirmParentPatientStage(oldOrder, newOrder);
        manager.checkSpecialProcessingCompleted(oldOrder, newOrder);
        manager.checkForUpdatedAccountOrContact(oldOrder, newOrder);
        manager.sendProductBillingForm(oldOrder, newOrder);
        manager.checkForOpportunityUpdate(newOrder);
         manager.UpdateOptyCloseDate(oldOrder, newOrder);
         manager.UpdateOptyOrderNumber(oldOrder, newOrder);
      //  manager.checkForOpportunityUpdate(oldOrder, newOrder); // sum revenue amount 

    }   
    
    /**
    * Called at end of trigger
    * 
    * @param void
    */
    public override void andFinally() {
        manager.commitInventoryTransaction();
        manager.commitInventoryItems();
        manager.commitParentOrders();
        manager.commitOpportunities();
        manager.commitOpportunityLineItems();
        manager.sendBillingForms();
        manager.commitLineItems();
        //manager.commitLineItemsOrder();
        //===Added by Ashish =to Create Procedure on Order insert===sfdc-1489==Start==
       if(Trigger.IsAfter && Trigger.Isinsert){ 
           manager.commitCreateProcedure();
       }
       //===Added by Ashish =to Create Procedure on Order insert===sfdc-1489==end==
       
       //===Added by Ashish =to update Stage value on Order products===sfdc-1489==Start==
       if(Trigger.IsAfter && Trigger.IsUpdate){
           manager.commitOrderItemUpdate();
           manager.commitProcedureOwner();
       }
       //===Added by Ashish =to update Stage value on Order products===sfdc-1489==End==
       
        //manager.removeDuplicateOrderItems();
    }

}
@isTest
private class SearchController_Test {

    private static testMethod void test() {
        
        //set the current page to 'SearchResults' page
        PageReference pageRef = Page.SearchResults;
        Test.setCurrentPage(pageRef);
        
        //set the 'query' parameters of the page 
        pageRef.getParameters().put('query', 'Test');
        List<Id> fixedSearchResults = new List<Id>();
        
        //create Community user 
        User objUser = Test_DataCreator.createCommunityUser();
        
        //initilizes the class
        SearchController objSearchController = new SearchController();
        //objSearchController.init();
        
        system.runAs(objUser){
            
            //Create Products
            Test_DataCreator.createProducts();
            List<Product2> lstproducts = [Select Id From Product2];
            
            for(Product2 objProduct : lstproducts) {
            
                fixedSearchResults.add(objProduct.Id);
            }
            
            //SOSL does not return results in test code so using this method to bypass find clause of sosl
            system.debug('fixedSearchResults=================='+fixedSearchResults);
            Test.setFixedSearchResults(fixedSearchResults);
            SearchController objSearchController1 = new SearchController();
            objSearchController1.init();
            
            //check the size of list of products 
            //system.assertNotEquals(0, objSearchController1.lstProducts.size());
            system.assertEquals(true, objSearchController1.isEnglishUser);
        }

        User objLocaleUser = Test_DataCreator.createCommunityUserWithLocale();
        
        system.runAs(objLocaleUser){
            
            Product2 objProduct = Test_DataCreator.createProductEndo('test');
            //objProduct.Shared_Top_Level_Parent__c = False;
            update objProduct;
            
            Shared_Community_Product_Translation__c ObjProductTranslationRecord = Test_DataCreator.createProductTranslationRecord(objProduct.Id,objLocaleUser.Shared_Community_Division__c);
                        
            pageRef.getParameters().put('query', objLocaleUser.Shared_Community_Division__c);
            objSearchController = new SearchController(); 
            
            List<Shared_Community_Product_Translation__c> lstproducts = [Select Id From Shared_Community_Product_Translation__c];
            
            for(Shared_Community_Product_Translation__c objTranslationProduct : lstproducts) {
            
                fixedSearchResults.add(objTranslationProduct .Id);
            }
            
            Test.setFixedSearchResults(fixedSearchResults);
            
            objSearchController.fetchRequestedData();
            
            //check the size of list of products 
            //system.assert(objSearchController.lstProductTranslation.isEmpty(),'asbc======'+objSearchController.lstProductTranslation);
            system.assertEquals(objSearchController.lstProductTranslation.isEmpty(),true);
            system.assertEquals(true, objSearchController.isOtherUser);
         }
        
    }   
}
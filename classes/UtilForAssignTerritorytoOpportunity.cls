public with sharing class UtilForAssignTerritorytoOpportunity {
        public List<Territory2Model>   models =null;
        
        public Id activeModelId = null;
        UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
        private static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName(); 
        private static final Id RECTYPE_ENDO_DISPOSABLE_OPTY = RECTYPES.get('Endo New Disposable Business').getRecordTypeId();
        private static final Id RECTYPE_ENDO_CAPITAL_OPTY = RECTYPES.get('Endo Capital Business').getRecordTypeId();
        
        public Set<Id> accountOnOpportunity = new Set<Id>();          
        public Set<Id> ownerOnOpportunity = new Set<Id>();    
        public List<Id> territoryOnOpportunity = new List<Id>();
        public List<Id> matchedTerritories = new List<Id>();
        public boolean isAccountChangeonOpty =false;
        public List<TerritoriesForGI__c> GIterr = new  List<TerritoriesForGI__c>();
        public List<TerritoriesForPlum__c> Plumterr = new  List<TerritoriesForPlum__c>();
        //public list<String> lstOfPLUMterr {get;set;}
        //public list<String> lstOfGIterr {get;set;}
                 
        
        map<id,set<Id>> acctTerritorymap = new map<id,set<Id>>();
        map<id,set<Id>> userTerritorymap = new map<id,set<Id>>();
        //This  method is used in AssignTerritoryForOpportunityHandler and in Batch class for updating existing old Opportunities one time batch
        // For batch class we need to pass fisrt parameter 'isInsert' = true; so it will skip to check the new and old opty camparison  
        
        public void utilassignTerritoryForOpportunity(boolean isInsert, List<Opportunity> newOpttys) {
             List<Opportunity> lstnewopptys =  newOpttys;
            models =  [Select Id from Territory2Model where State = 'Active'];
        
               //Checking for activeModelId otherwise skip 
            if(models.size() == 1){
                activeModelId = models.get(0).Id;
            }
            
                   
            for(Opportunity o : lstnewopptys){
                if(o.AccountId != null){
                    system.debug('Account Found '+o.AccountId +', '+o);
                    accountOnOpportunity.add(o.AccountId);   
                }//finding account on opty
                //checking if account has been changed on the opportunity
                
                if(!isInsert)
                    if( ((Opportunity)trigger.oldMap.get(o.Id)).AccountId != ((Opportunity)trigger.newMap.get(o.Id)).AccountId)
                         isAccountChangeonOpty= true;
                         system.debug('Entered in update value is==>'+isAccountChangeonOpty);

                if(o.OwnerId!=null){
                    system.debug('Owner Found '+o.OwnerId);
                    ownerOnOpportunity.add(o.OwnerId);
                    system.debug('Opty Owner '+o.OwnerId);
                }//finding user on opty  
                if(o.territory2Id!=null){
                    system.debug('Territory Found '+o.territory2Id);    
                    territoryOnOpportunity.add(o.territory2Id);   
                }
            }//End of Loop
            
            List<ObjectTerritory2Association> territoriesOnNewAcount = [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id FROM ObjectTerritory2Association Where ObjectId IN :accountOnOpportunity AND  Territory2.Territory2ModelId = :activeModelId];
            List<UserTerritory2Association> territoriesOnNewOwner = [SELECT Id,RoleInTerritory2,Territory2Id,UserId FROM UserTerritory2Association Where UserId =:ownerOnOpportunity AND  Territory2.Territory2ModelId = :activeModelId];
            system.debug('territoriesOnNewOwner ==> '+territoriesOnNewOwner +'AccountTerritory==>'+territoriesOnNewAcount.size() );
            //for the ease of calculation, this logic will always consider territories on the 
            //new account and new owner on the opportunity, in below cases
            //1. account change
            //2. owner change
            //3. account and owner change together
            
            //Below code will create For each User/Account list of Territories
            if(territoriesOnNewAcount!=null && territoriesOnNewAcount.size()>0){

                //UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                //territoriesOnNewAcount = util.getAcctnTeritryAssctnList(accountIdLst);                                 
                if(territoriesOnNewAcount != null && territoriesOnNewAcount.size() > 0){
                    for(ObjectTerritory2Association at1 : territoriesOnNewAcount){
                        if(acctTerritorymap.containsKey(at1.ObjectId)){
                            acctTerritorymap.get(at1.ObjectId).add(at1.Territory2Id);
                        }
                        else{
                            acctTerritorymap.put(at1.ObjectId,new set<Id>{at1.Territory2Id});
                        }
                    }
                    
                }                     
            }
            if(territoriesOnNewOwner!=null && territoriesOnNewOwner.size()>0){

                //UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                //territoriesOnNewOwner = util.getUserTeritryAssctnList(userIdLst,true);                                 
                if(territoriesOnNewOwner != null && territoriesOnNewOwner.size() > 0){
                    for(UserTerritory2Association ut : territoriesOnNewOwner){
                        if(userTerritorymap.containsKey(ut.UserId)){
                            userTerritorymap.get(ut.UserId).add(ut.Territory2Id);
                        }
                        else{
                            userTerritorymap.put(ut.UserId,new set<Id>{ut.Territory2Id});
                        }
                    }
                    
                }       
            }
            
            //Preapared maps
            
            system.debug('--userTerritorymap--'+userTerritorymap);
            system.debug('--acctTerritorymap--'+acctTerritorymap);
            map<Id,Id> opptyTerAssignmap = new map<Id,Id>();
            map<Id,Id>  tempOpttyLst;       
            list<Id> opptyIdLstWthFalseCheckbx = new list<Id>();
            for(Opportunity opt : lstnewopptys){
                if(userTerritorymap.keyset().Contains(opt.OwnerId) && acctTerritorymap.keyset().Contains(opt.AccountId)){       //If User to Territories map doesn't contain Oppty's owner and Account to Territories map doesn't contain Oppty's Account
                    if(userTerritorymap.get(opt.OwnerId).size() > 0 && acctTerritorymap.get(opt.AccountId).size() > 0){         //If User to Territories map doesn't have territories for Oppty's owner and Account to Territories map doesn't have territories for Oppty's Account
                          system.debug('**userTerritorymap.get(opt.OwnerId)***'+userTerritorymap.get(opt.OwnerId));
                        //Find a match for Territory                
                        integer cnt = 0;
                        for(Id tId : userTerritorymap.get(opt.OwnerId)){
                            if(acctTerritorymap.get(opt.AccountId).contains(tId)){
                               // Integer matchcount =acctTerritorymap.get(opt.AccountId);
                                
                                cnt++;                                                                              //Increment the count when multiple matches are found
                                if(cnt == 1){
                                    tempOpttyLst = new map<Id,Id>();
                                    tempOpttyLst.put(opt.Id,tId);
                                    system.debug('**tempOpttyLst****'+tempOpttyLst);
                                }
                            }                           
                        }
                        if(cnt == 1){
                        system.debug('**tempOpttyLst111****'+tempOpttyLst);                                                                           //If only one match is found put it in a map of <Oppty Id-->Territory id>
                            opptyTerAssignmap.putAll(tempOpttyLst);
                            if(Trigger.isExecuting){
                            
                            system.debug('**opptyTerAssignmap.get(opt.Id)****'+opptyTerAssignmap.get(opt.Id));
                            
                                Id trrId=opptyTerAssignmap.get(opt.Id);
                                system.debug('**trrId****'+trrId);
                                opt.territory2Id = trrId;
                                opt.territory_assigned__C = true;
                            }   
                        }
                        if(cnt>1 || cnt ==0){
                            //OpptyIdLstWthFalseCheckbx.add(opt.id);                        //If multiple matches are found put Oppty id in another map
                            if(Trigger.isExecuting){
                                if(isAccountChangeonOpty){
                                    opt.Territory2Id=null;//clearing the existing territory since new account does not have any matching territory.
                                }  
                                            
                                opt.territory_assigned__C = false;
                            }else{
                                OpptyIdLstWthFalseCheckbx.add(opt.id);
                            }   
                        }                       
                    }
                    
                }
                else{
                          
                        opt.territory_assigned__C = false;                           //If both UserTerritoryMap and AccountTerritoryMap have no Oppty owner and no Oppty Account in it, mark the checkbox in Oppty to false
                }  
            }

            
        }//end if trigger if
        
         /*
        @MethodName    assignENDOSecondaryTerritory
        @Parameters    newOpttys: List of opportunities
        @Return Type   void
        @Description   Method to assign seconday territory on endo opportunities based on owner.
        */        
        public void assignENDOSTerritory(List<Opportunity> newOpttys,boolean isUpdate){
        
        set<Id> acctIdSet = new set<Id>();
        set<String> territoryId = new set<String>();
        string PULM = system.label.ENDO_PULM_parent_territories;
        string GI = system.label.ENDO_GI_Parent_territories;
        list<string> PULMlist = PULM.Split(';');
        set<string> PULMset = new set<string>();
        PULMset.addAll(PULMlist);
        list<string> GIlist = GI.Split(';');
        set<string> GIset = new set<string>();
        GIset.addAll(GIlist);
        string PULMRolesString = system.label.ENDO_PULM_Roles;
        list<string> PULMroles = PULMRolesString.Split(',');
        set<string> PULMRoleSet = new set<string>();
        PULMRoleSet.addAll(PULMroles);
        
        Plumterr = TerritoriesForPlum__c.getall().values();
        string PlumValue = Plumterr[0].Plum__c;
        
        GIterr = TerritoriesForGI__c.getall().values();
        string GIValue = GIterr[0].GI__c;
        
        set<Id> ownerIdSet = new set<Id>();
        set<Id> accountIdSet = new set<Id>();
        map<Id,string> userRoleMap = new map<Id,string>();
        map<Id,List<Opportunity>> acctOpptyMap = new map<Id,List<Opportunity>>();
        map<Id,list<ObjectTerritory2Association>> acctTerrMap = new map<Id,list<ObjectTerritory2Association>>();
        
        map<Id,set<Id>> accTerrrIdMap = new  map<Id,set<Id>>();
        map<Id,set<string>> accTerrrNameMap = new  map<Id,set<string>>(); 
        
        set<Id> opptyownerIdset = new set<Id>();
        list<UserTerritory2Association> opptyOwnerTerrLst = new list<UserTerritory2Association>();
        map<Id,list<UserTerritory2Association>> opptyOwnerTerrmap = new map<Id,list<UserTerritory2Association>>();
        
        for(Opportunity op : newOpttys){
            if(op.ownerId != null){
                opptyownerIdset.add(op.ownerId);
            }
            if(op.AccountId != null){
                accountIdSet.add(op.AccountId);
            }
        }
        map<Id,set<Id>> ownerTerrrIdMap = new  map<Id,set<Id>>();
        map<Id,set<string>> ownerTerrrNameMap = new  map<Id,set<string>>(); 
        if(opptyownerIdset != null && opptyownerIdset.size() > 0){
            opptyOwnerTerrLst = util.getUserTeritryAssctnList(opptyownerIdset,true);
            if(opptyOwnerTerrLst != null && opptyOwnerTerrLst.size() > 0){
                for(UserTerritory2Association u : opptyOwnerTerrLst ){
                    if(ownerTerrrIdMap.containsKey(u.UserId)){
                        ownerTerrrIdMap.get(u.UserId).add(u.Territory2Id);
                    }else{
                        ownerTerrrIdMap.put(u.UserId,new set<Id>{u.Territory2Id});
                    }
                    if(ownerTerrrNameMap.containsKey(u.UserId)){
                        ownerTerrrNameMap.get(u.UserId).add(u.Territory2.Name);
                    }else{
                        ownerTerrrNameMap.put(u.UserId,new set<string>{u.Territory2.Name});
                    }
                }
            }
        }
        list<ObjectTerritory2Association> opptyAccountTerrLst = new list<ObjectTerritory2Association>();
        if(accountIdSet != null && accountIdSet.size() > 0){
            opptyAccountTerrLst = util.getObject2AssctnList(accountIdSet);
            if(opptyAccountTerrLst != null && opptyAccountTerrLst.size() > 0){
                for(ObjectTerritory2Association a : opptyAccountTerrLst ){
                    if(accTerrrIdMap.containsKey(a.ObjectId)){
                        accTerrrIdMap.get(a.ObjectId).add(a.Territory2Id);
                    }else{
                        accTerrrIdMap.put(a.ObjectId,new set<Id>{a.Territory2Id});
                    }
                    if(accTerrrNameMap.containsKey(a.ObjectId)){
                        accTerrrNameMap.get(a.ObjectId).add(a.Territory2.Name);
                    }else{
                        accTerrrNameMap.put(a.ObjectId,new set<string>{a.Territory2.Name});
                    }
                }
            }
        }
        for(Opportunity op :newOpttys){
               if(op.AccountId != null){
                   if(acctOpptyMap.containsKey(op.AccountId)){
                       acctOpptyMap.get(op.AccountId).add(op);
                   }else{
                       acctOpptyMap.put(op.AccountId,new list<Opportunity>{op});
                   }                   
               }
               ownerIdSet.add(op.OwnerId);
         }
         if(ownerIdSet.size() >0){
             userRoleMap = getUserRole(ownerIdSet);
         }
         system.debug('userRoleMap -->'+userRoleMap );
         
         if(acctOpptyMap!=null){
             for(ObjectTerritory2Association  obj : [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id,Territory2.Name,Territory2.No_Of_Parents_1_To_6__c,Territory2.ParentTerritory2.Name,Territory2.ParentTerritory2.Description,
                                                     Territory2.ParentTerritory2.ParentTerritory2.Name,Territory2.ParentTerritory2.ParentTerritory2.Description,Territory2.Description, Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2.Name
                                                     FROM ObjectTerritory2Association Where ObjectId IN : acctOpptyMap.keyset() Order BY Territory2.Name ASC]){
                  if(acctTerrMap.containsKey(obj.ObjectId)){
                       acctTerrMap.get(obj.ObjectId).add(obj);
                   }else{
                       acctTerrMap.put(obj.ObjectId,new list<ObjectTerritory2Association>{obj});
                   }                                   
                
             }
             
         }    
        
         for(Opportunity op :newOpttys){
             if(acctTerrMap.containsKey(op.AccountId)){
                 for(ObjectTerritory2Association  obj: acctTerrMap.get(op.AccountId)){
                     system.debug(obj.Territory2.Name+'123');
                     system.debug(obj.Territory2.ParentTerritory2.Name+'456');
                     system.debug(obj.Territory2.ParentTerritory2.ParentTerritory2.Name+'789');
                     if(GIset.contains(obj.Territory2.Name) && (obj.Territory2.Name).Contains(GIValue) ){
                         op.territory2Id = obj.Territory2Id;
                         op.GI_Region_Description__c = obj.Territory2.ParentTerritory2.Description;
                         op.GI_Area_Description__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                         op.GI_Parent__c = obj.Territory2.ParentTerritory2Id;
                         op.GI_Parent_Parent__c = obj.Territory2.ParentTerritory2.ParentTerritory2Id;
                         break;
                     }
                     else if(obj.Territory2.ParentTerritory2Id != null && GIset.contains(obj.Territory2.ParentTerritory2.Name) && (obj.Territory2.Name).Contains(GIValue)){
                         op.territory2Id = obj.Territory2Id;
                         op.GI_Region_Description__c = obj.Territory2.ParentTerritory2.Description;
                         op.GI_Area_Description__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                         op.GI_Parent__c = obj.Territory2.ParentTerritory2Id;
                         op.GI_Parent_Parent__c = obj.Territory2.ParentTerritory2.ParentTerritory2Id;
                         break;
                     }
                     else if(obj.Territory2.ParentTerritory2.ParentTerritory2Id != null && GIset.contains(obj.Territory2.ParentTerritory2.ParentTerritory2.Name) && (obj.Territory2.Name).Contains(GIValue)){
                         op.territory2Id = obj.Territory2Id;
                         op.GI_Region_Description__c = obj.Territory2.ParentTerritory2.Description;
                         op.GI_Area_Description__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                         op.GI_Parent__c = obj.Territory2.ParentTerritory2Id;
                         op.GI_Parent_Parent__c = obj.Territory2.ParentTerritory2.ParentTerritory2Id;
                         break;
                     }
                     else{
                         if(isUpdate == false){
                             op.territory2Id = null;
                             op.GI_Region_Description__c = '';
                             op.GI_Area_Description__c = '';
                             op.GI_Parent__c = '';
                             op.GI_Parent_Parent__c = '';
                         }
                         
                     }
                     
                 }
                                  
                 if(userRoleMap.containsKey(op.OwnerId)){
                    if(PULMroleSet.contains(userRoleMap.get(op.OwnerId))){                  
                     for(ObjectTerritory2Association  obj: acctTerrMap.get(op.AccountId)){
                         
                         if(PULMset.contains(obj.Territory2.Name) && (obj.Territory2.Name).Contains(PlumValue)){
                             
                             op.ENDO_Secondary_Territory__c = obj.Territory2.name;
                             op.SecondaryTerritoryDescription__c=obj.Territory2.Description;
                             op.SecondaryTerritoryRegion__c = obj.Territory2.ParentTerritory2.Description;
                             op.Secondary_Territory_Area__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                             op.PULM_Parent__c = obj.Territory2.ParentTerritory2.Name;
                             op.PULM_Parent_Parent__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Name;
                             break;
                         }
                         else if(obj.Territory2.ParentTerritory2Id != null && PULMset.contains(obj.Territory2.ParentTerritory2.Name) && (obj.Territory2.Name).Contains(PlumValue)){
                             
                             op.ENDO_Secondary_Territory__c = obj.Territory2.name;
                             op.SecondaryTerritoryDescription__c=obj.Territory2.Description;
                             op.SecondaryTerritoryRegion__c = obj.Territory2.ParentTerritory2.Description;
                             op.Secondary_Territory_Area__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                             op.PULM_Parent__c = obj.Territory2.ParentTerritory2.Name;
                             op.PULM_Parent_Parent__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Name;
                             break;
                         }
                         else if(obj.Territory2.ParentTerritory2.ParentTerritory2Id != null && PULMset.contains(obj.Territory2.ParentTerritory2.ParentTerritory2.Name) && (obj.Territory2.Name).Contains(PlumValue)){
                             
                             op.ENDO_Secondary_Territory__c = obj.Territory2.name;
                             op.SecondaryTerritoryDescription__c=obj.Territory2.Description;
                             op.SecondaryTerritoryRegion__c = obj.Territory2.ParentTerritory2.Description;
                             op.Secondary_Territory_Area__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                             op.PULM_Parent__c = obj.Territory2.ParentTerritory2.Name;
                             op.PULM_Parent_Parent__c = obj.Territory2.ParentTerritory2.ParentTerritory2.Name;
                             break;
                         }
                         else{
                             if(isUpdate == false){
                                 op.ENDO_Secondary_Territory__c = '';
                                 op.SecondaryTerritoryDescription__c='';
                                 op.Secondary_Territory_Area__c ='';
                                 op.SecondaryTerritoryRegion__c='';
                                 op.PULM_Parent__c = '';
                                 op.PULM_Parent_Parent__c = '';
                             }
                         }
                     }
                     }
                 }
             }
             // Added for update territory indicator if the oppy owner is in primary territory or not in that.
             // Start
             // Owner
             
             if((op.Territory2Id != null || op.ENDO_Secondary_Territory__c != null) && (ownerTerrrIdMap.containsKey(op.OwnerId) || ownerTerrrNameMap.containsKey(op.OwnerId))){
                if((ownerTerrrIdMap.get(op.OwnerId).contains(op.Territory2Id) || ownerTerrrNameMap.get(op.OwnerId).contains(op.ENDO_Secondary_Territory__c) || ownerTerrrIdMap.get(op.OwnerId).contains(op.GI_Parent__c) || ownerTerrrIdMap.get(op.OwnerId).contains(op.GI_Parent_Parent__c) || ownerTerrrNameMap.get(op.OwnerId).contains(op.PULM_Parent__c) || ownerTerrrNameMap.get(op.OwnerId).contains(op.PULM_Parent_Parent__c))){
                    if(accTerrrIdMap.containsKey(op.AccountId)){
                        if(accTerrrIdMap.get(op.AccountId).contains(op.Territory2Id) || accTerrrNameMap.get(op.AccountId).contains(op.ENDO_Secondary_Territory__c)){
                       
                            op.territory_assigned__C = true;
                            break;
                        }
                    }
                      
                     op.territory_assigned__C = false;
                }
                else{
                  
                    op.territory_assigned__C = false;
                }
            }
            else{
              
                op.territory_assigned__C = false;
            }
            
            // End
         } 
        }
        
        public map<Id,string> getUserRole(set<Id> Idset){
            map<Id,string> usrMap = new map<Id,string>();
            for(User u :[Select Id,name,UserRole.Name, Profile.Name from User
                    WHERE Id IN: Idset]){
                if(u.UserRole.name != null){
                    usrMap.put(u.Id,u.UserRole.name);
                }
                
            }
            return usrMap;
        }
        
        /*@MethodName  batchOpptyTerritoryUpdateENDO
        @Parameters    newOpttys: List of opportunities
        @Return Type   void
        @Description   Q3-2016, change request for ENDO
        */        
        public list<Opportunity> batchOpptyTerritoryUpdateENDO(List<Opportunity> newOpttys){
        
            list<Opportunity> endoOpptyLst = new list<Opportunity>();
            list<Opportunity> updateOpptyLst = new list<Opportunity>();
            set<Id> opptyAcctIdset = new set<Id>();
            set<Id> opptyownerIdset = new set<Id>();
            string GIValueSet = TerritoriesForGI__c.getall().values().GI__c;
            string PULMValueSet = TerritoriesForPlum__c.getall().values().PLUM__c;
            string PULM = system.label.ENDO_PULM_parent_territories;
            string GI = system.label.ENDO_GI_Parent_territories;
            list<string> PULMlist = PULM.Split(';');
            set<string> PULMset = new set<string>();
            PULMset.addAll(PULMlist);
            list<string> GIlist = GI.Split(';');
            set<string> GIset = new set<string>();
            GIset.addAll(GIlist);
            list<UserTerritory2Association> opptyOwnerTerrLst = new list<UserTerritory2Association>();
            map<Id,list<UserTerritory2Association>> opptyOwnerTerrmap = new map<Id,list<UserTerritory2Association>>();
            
            for(Opportunity op : newOpttys){
                if(op.AccountId != null){
                    opptyAcctIdset.add(op.AccountId);
                }
                if(op.ownerId != null){
                    opptyownerIdset.add(op.ownerId);
                }
            }
            map<Id,set<Id>> ownerTerrrIdMap = new  map<Id,set<Id>>();
            map<Id,set<string>> ownerTerrrNameMap = new  map<Id,set<string>>(); 
            if(opptyownerIdset != null && opptyownerIdset.size() > 0){
                opptyOwnerTerrLst = util.getUserTeritryAssctnList(opptyownerIdset,true);
                if(opptyOwnerTerrLst != null && opptyOwnerTerrLst.size() > 0){
                    for(UserTerritory2Association u : opptyOwnerTerrLst ){
                        if(ownerTerrrIdMap.containsKey(u.UserId)){
                            ownerTerrrIdMap.get(u.UserId).add(u.Territory2Id);
                        }else{
                            ownerTerrrIdMap.put(u.UserId,new set<Id>{u.Territory2Id});
                        }
                        if(ownerTerrrNameMap.containsKey(u.UserId)){
                            ownerTerrrNameMap.get(u.UserId).add(u.Territory2.Name);
                        }else{
                            ownerTerrrNameMap.put(u.UserId,new set<string>{u.Territory2.Name});
                        }
                    }
                }
            }
            map<Id,list<ObjectTerritory2Association>> acctTerritorymap1 = new map<Id,list<ObjectTerritory2Association>>();
            if(opptyAcctIdset != null && opptyAcctIdset.size() > 0){
                for(ObjectTerritory2Association  obj : [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id,Territory2.Name,Territory2.No_Of_Parents_1_To_6__c,Territory2.ParentTerritory2.Name,Territory2.ParentTerritory2.Description,
                                                         Territory2.ParentTerritory2.ParentTerritory2.Name,Territory2.ParentTerritory2.ParentTerritory2.Description,Territory2.Description, Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2.Name
                                                         FROM ObjectTerritory2Association Where ObjectId IN : opptyAcctIdset Order BY Territory2.Name ASC]){
                      if(acctTerritorymap1.containsKey(obj.ObjectId)){
                           acctTerritorymap1.get(obj.ObjectId).add(obj);
                       }else{
                           acctTerritorymap1.put(obj.ObjectId,new list<ObjectTerritory2Association>{obj});
                       }                                   
                 
                 }
            }
            
            string newGITerritory;
            string newPULMTerritory;
            for(Opportunity o : newOpttys){
                integer GICount = 0;
                string GITerritory;
                string GITerrRegionDescription;
                string GITerrAreaDescription;
                string GIParentId;
                string GIParentParentId;
                integer PULMCount = 0;
                string PULMTerritory;
                string PULMTerritoryDescription;
                string PULMTerritoryArea;
                string PULMTerritoryRegion;
                string PULMParent;
                string PULMParentParent;
                string PULMParentId;
                if(o.territory2Id != null){
                  if(acctTerritorymap1.keyset().Contains(o.AccountId)){
                     for(ObjectTerritory2Association obj: acctTerritorymap1.get(o.AccountId)){
                        if((obj.Territory2.Name).Contains(GIValueSet)){                  //check lowest territory
                            if(obj.Territory2.ParentTerritory2.ParentTerritory2Id != null && GIset.contains(obj.Territory2.ParentTerritory2.ParentTerritory2.Name)){    //check for GI territory
                                GICount++;
                                GITerritory = obj.Territory2Id;
                                GITerrRegionDescription = obj.Territory2.ParentTerritory2.Description;
                                GITerrAreaDescription = obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                                GIParentId = obj.Territory2.ParentTerritory2Id;
                                GIParentParentId = obj.Territory2.ParentTerritory2.ParentTerritory2Id;
                            }
                        }
                     }
                     if(GICount == 1){
                        if(o.territory2Id != GITerritory){                  //If previous primary territory is not avaialabe and new sinlge primary territory is avaialabe
                            o.territory2Id = GITerritory;
                            o.GI_Region_Description__c = GITerrRegionDescription;
                            o.GI_Area_Description__c = GITerrAreaDescription;
                            o.GI_Parent__c = GIParentId;
                            o.GI_Parent_Parent__c = GIParentParentId;
                            //newGITerritory = GITerritory;
                            if(ownerTerrrIdMap.containskey(o.OwnerId)){
                               if(ownerTerrrIdMap.get(o.OwnerId).contains(GITerritory)){
                                   o.territory_assigned__C = true;
                               } 
                            }
                        }
                        else{
                           // o.territory2Id = GITerritory; ---------> ????? null or as is
                           if(ownerTerrrIdMap.containskey(o.OwnerId)){
                               if(ownerTerrrIdMap.get(o.OwnerId).contains(GITerritory)){
                                   o.territory_assigned__C = true;
                               } 
                               else{
                                  
                               o.territory_assigned__C = false;
                               }
                           }
                           else{
                            
                                o.territory_assigned__C = false;
                           }
                            
                        }
                     }
                     else{
                        o.territory_assigned__C = false;
                     }
                     if(o.ENDO_Secondary_Territory__c != null && string.isNotEmpty(o.ENDO_Secondary_Territory__c)){
                            for(ObjectTerritory2Association obj: acctTerritorymap1.get(o.AccountId)){
                                if((obj.Territory2.Name).Contains(PULMValueSet)){                    //check lowest territory
                                    if(obj.Territory2.ParentTerritory2Id != null && PULMset.contains(obj.Territory2.ParentTerritory2.Name) ){    //check for PULM territory
                                        PULMCount++;
                                        PULMTerritory = obj.Territory2.name;
                                        PULMTerritoryDescription = obj.Territory2.Description;                                        
                                        PULMTerritoryRegion= obj.Territory2.ParentTerritory2.Description;
                                        PULMTerritoryArea =obj.Territory2.ParentTerritory2.ParentTerritory2.Description;
                                        PULMParent = obj.Territory2.ParentTerritory2.Name;
                                        PULMParentParent = obj.Territory2.ParentTerritory2.ParentTerritory2.Name; 
                                        PULMParentId = obj.Territory2.ParentTerritory2Id;                                    
                                    }
                                }
                         }
                         if(PULMCount == 1){                            //If previous primary territory is not avaialabe and new sinlge primary territory is avaialabe
                            if(o.ENDO_Secondary_Territory__c != PULMTerritory){             
                                 o.ENDO_Secondary_Territory__c = PULMTerritory;
                                 newPULMTerritory = PULMTerritory;
                                 o.SecondaryTerritoryDescription__c= PULMTerritoryDescription;
                                 o.Secondary_Territory_Area__c = PULMTerritoryArea;
                                 o.SecondaryTerritoryRegion__c = PULMTerritoryRegion;
                                 o.PULM_Parent__c = PULMParent;
                                 o.PULM_Parent_Parent__c = PULMParentParent;
                                 o.Parent_Of_Secondary_Territory__c = PULMParentId;
                                 
                                 if(ownerTerrrNameMap.containskey(o.OwnerId)){
                                   if(ownerTerrrNameMap.get(o.OwnerId).contains(PULMTerritory)){
                                       o.territory_assigned__C = true;
                                   } 
                                }
                                
                            
                            }
                            else{
                               if(ownerTerrrNameMap.containskey(o.OwnerId)){
                                   if(ownerTerrrNameMap.get(o.OwnerId).contains(PULMTerritory)){
                                       o.territory_assigned__C = true;
                                   } 
                                   else{
                                  
                                  o.territory_assigned__C = false;
                                   }
                               }
                               else{
                                  
                                    o.territory_assigned__C = false;
                               }
                                
                            }
                        }
                        else{
                        
                            o.territory_assigned__C = false;
                        }
                     }
                }
                else{
                
                    o.territory_assigned__C = false;
                }
            }
            else{
            
                o.territory_assigned__C = false;
            }
            updateOpptyLst.add(o);
                
            } 
            
            list<Opportunity> tempOpptyLst = new list<Opportunity>();
            set<Id> opptyIdSet = new set<Id>();
           /* for(Opportunity o : updateOpptyLst){
            system.debug(o.Territory2Id+'--o.Territory2Id--'+newGITerritory);
            system.debug(o.ENDO_Secondary_Territory__c +'--o.ENDO_Secondary_Territory__c --'+newPULMTerritory);
                if(o.Territory2Id != null && opptyOwnerTerrmap.containsKey(o.OwnerId)){
                    for(UserTerritory2Association ut : opptyOwnerTerrmap.get(o.OwnerId)){
                        if(o.Territory2Id == ut.Territory2Id){
                            o.territory_assigned__C = true;
                            break;
                        }
                        else{
                            o.territory_assigned__C = false;
                        }
                        if(!opptyIdSet.contains(o.Id)){
                            opptyIdSet.add(o.Id);
                            tempOpptyLst.add(o);
                        }
                        
                    }
                }
                else{
                    o.territory_assigned__C = false;
                }
                
                if(o.ENDO_Secondary_Territory__c != null && o.ENDO_Secondary_Territory__c != '' && opptyOwnerTerrmap.containsKey(o.OwnerId)){
                    for(UserTerritory2Association ut : opptyOwnerTerrmap.get(o.OwnerId)){
                        if(o.ENDO_Secondary_Territory__c == ut.Territory2.Name){
                            o.territory_assigned__C = true;
                            break;
                        }
                        if(!opptyIdSet.contains(o.Id)){
                            opptyIdSet.add(o.Id);
                            tempOpptyLst.add(o);
                        }
                    }
                }
            }*/
            if(updateOpptyLst!= null && updateOpptyLst.size() > 0){
                return updateOpptyLst;
            }
            else{
                return null;
            }
        }
    
}
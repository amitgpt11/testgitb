/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class VerificationTriggerHandlerTest {
	final static String LOT1 = 'LOT1';
	final static String MODEL123 = 'MODEL123';
	final static String SERIAL1 = '11111';
	final static String SERIAL2 = 'SERIAL2';
	final static String CCRANAME = 'CCRANAME';
	final static String SURGERYNAME = 'SURGERYNAME';
	final static String UPNMATERIALNUM = '1234567';
	final static String INVENLOCATION = 'LOC123';
    final static String ACCTNAME ='TestAccount';
	final static Date EXPIRATION = System.today().addDays(10);
    public static final String MISSING_SERIAL = 'Missing (Serial)';

	//@TestSetup
    static void getUser(){
        NMD_TestDataManager td = new NMD_TestDataManager();
        User user1 = td.newUser('System Administrator');
        user1.Personnel_ID__c ='TESTDATA1';
        insert user1;
        //return user1;
    }
	static Inventory_Location__c setupInventoryLocation(){
		NMD_TestDataManager td = new NMD_TestDataManager();
		// account
		Account acct = td.createConsignmentAccount();
        acct.Name = ACCTNAME;
		insert acct;
		user u = [Select id,Personnel_ID__c From User where Personnel_ID__c ='TESTDATA1' ];
		// inventory tree
		Inventory_Data__c invData = new Inventory_Data__c(
			Account__c = acct.Id,
			Customer_Class__c = 'YY',
            OwnerId = u.Id
		);
		insert invData;
		system.debug('Inventory Location=='+invData);
		Inventory_Location__c invLoc = new Inventory_Location__c(
			Inventory_Data_ID__c = invData.Id,
			Name = INVENLOCATION
		);
		insert invLoc;
        return invLoc;
	}
	static Cycle_Count_Line_Item__c setupCycleCountLineItem() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		// product
		Product2 prod = td.newProduct('Trial');
		prod.Model_Number__c = MODEL123;
		prod.EAN_UPN__c = MODEL123;
		prod.UPN_Material_Number__c = UPNMATERIALNUM;
        prod.Serial_Indicator__c = true;
		insert prod;
		system.debug('####'+prod);
        
       /* User user1 = td.newUser('System Administrator');
        user1.Personnel_ID__c ='TESTDATA1';
        insert user1;*/
        User user1 =[Select id,Personnel_ID__c From User where Personnel_ID__c ='TESTDATA1'];
		Inventory_Location__c invLoc =setupInventoryLocation();
		
        
		Inventory_Item__c invItem = new Inventory_Item__c(
			Inventory_Location__c = invLoc.Id,
			Model_Number__c = prod.Id,
			Serial_Number__c = SERIAL1,
			Lot_Number__c = LOT1,
			Expiration_Date__c = EXPIRATION,
			SAP_Quantity__c = 1
		);
		insert invItem;
		system.debug('####'+invItem);
		Inventory_Transaction__c invTrans = new Inventory_Transaction__c(
			Inventory_Item__c = invItem.Id,
			Disposition__c = 'In Transit',
			Quantity__c = 1			
			);
		insert invTrans;
		
        
		Cycle_Count_Response_Analysis__c ccra = td.newCycleCountResponseAnalysis();
        ccra.OwnerId=user1.Id;
        //invLoc.Inventory_Data_ID__r.OwnerId = user1.Id;
        //update invLoc;
		insert ccra;
        system.debug('ccra'+ccra);
		
		Cycle_Count_Line_Item__c item = new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id,
			Inventory_Item__c = invItem.Id,
			Other_inventory_Item__c = invItem.Id,
			//Scanned_Lot_Number__c = LOT1,
			//Scanned_Model_Number__c = MODEL123,
			///Scanned_Serial_Number__c = SERIAL1, 
			Exception_Reason__c = MISSING_SERIAL	 
		);
		insert item;
		
		return item;
		
	}
   
	static List<Order> setupOrder() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        td.setupNuromodUserRoleSettings();

        //create two users
        Id contextUserId = UserInfo.getUserId();
        User user0 = td.newUser('System Administrator');
        User user1 = td.newUser('System Administrator');
        insert new List < User > {
            user0
        };

        //System.runAs(new User(Id = UserInfo.getUserId())) {

            //Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
        //insert pb;

            Seller_Hierarchy__c hierarchy = td.newSellerHierarchy();
            insert hierarchy;

            //create two assignees
            Assignee__c assignee0 = td.newAssignee(user0.Id, hierarchy.Id);
            assignee0.Rep_Designation__c = 'TM1';
           // Assignee__c assignee1 = td.newAssignee(user1.Id, hierarchy.Id);
            //assignee1.Rep_Designation__c = 'TM2';
            insert new List < Assignee__c > {
                assignee0
            };

            hierarchy.Current_TM1_Assignee__c = user0.Id;
           // hierarchy.Current_TM2_Assignee__c = user1.Id;
            update hierarchy;

            //create account
           /* Account acct = td.createConsignmentAccount();
            acct.Name = ACCTNAME;
       		 insert acct;
            
            Account consignment0 = td.createConsignmentAccount();
            consignment0.OwnerId = user0.Id;
            consignment0.Account_Number__c = 'SAP-1234';
            consignment0.Customer_Class__c = 'YY';
            
            Account consignment1 = td.createConsignmentAccount();
            consignment1.OwnerId = contextUserId;
            consignment1.Account_Number__c = 'SAP-0987';
            consignment1.Customer_Class__c = 'YY';
            
            insert new List < Account > {
                acct, consignment0, consignment1
            };*/
			Account acc= [Select Id, Name From Account where Name =:ACCTNAME];
            //create oppty
            Opportunity oppty = td.newOpportunity(acc.Id);
            oppty.Territory_Id__c = hierarchy.Id;
            oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
            
            insert oppty;
			// product
            Product2 prod = [Select Id,Model_Number__c,EAN_UPN__c,UPN_Material_Number__c from Product2 where Model_Number__c=:MODEL123 AND UPN_Material_Number__c =: UPNMATERIALNUM ] ;
            prod.Model_Number__c = MODEL123;
            prod.EAN_UPN__c = MODEL123;
            prod.UPN_Material_Number__c = UPNMATERIALNUM;
            Update prod;
        	
        // create two products, pricebook, and entries
      Id pricebookId = Test.getStandardPricebookId();

      Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
        insert pb;
        
			Inventory_Location__c il =[Select Id,Name,Inventory_Data_ID__r.OwnerId,Inventory_Data_ID__c From Inventory_Location__c where name =:INVENLOCATION ];
       // il.Inventory_Data_ID__r.OwnerId 
            PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
          PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
          insert new List<PricebookEntry> { standardPrice0 };

        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        insert new List<PricebookEntry> { pbe0};
         List<Order> orders=new List<Order>();   
			Order ord = new Order(
                AccountId = acc.id,
                Shipping_Location__c=il.id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today(),
                Pricebook2Id = pb.Id,
                Status = 'Draft',
                RecordTypeId = OrderManager.RECTYPE_PATIENT,
                Stage__c = 'Pending'
			);
        insert ord;
        orders.add(ord);
        OrderItem ordItem = new OrderItem(
        	OrderId=ord.id,
            Material_Number_UPN__c=UPNMATERIALNUM,
            Serial_Number__c = SERIAL1,
            PricebookEntryId = pbe0.Id,
            Quantity=1,
            UnitPrice = 0
            //Product = p2.id
        );
        system.debug('Order details===>'+ord);
			insert ordItem;
       /* Order ord1 = new Order(
                AccountId = acc.id,
                Shipping_Location__c=il.id,
                OpportunityId = oppty.Id,
                EffectiveDate = System.today(),
                Pricebook2Id = pb.Id,
                Status = 'Draft',
                RecordTypeId = OrderManager.RECTYPE_TRANSFER,
                Stage__c = 'Pending'
			);
        
        insert ord1;
        orders.add(ord1);
        OrderItem ordItem1 = new OrderItem(
        	OrderId=ord1.id,
            Material_Number_UPN__c=UPNMATERIALNUM,
            Serial_Number__c = SERIAL1,
            PricebookEntryId = pbe0.Id,
            Quantity=1,
            UnitPrice = 0
            //Product = p2.id
        );
        system.debug('Order details===>'+ord);
        insert ordItem1;*/
			system.debug('Order details===>'+ordItem);
			
			return orders;
        //}
		
    }
	
	static testMethod void test_Verificaiton_fetchVerificaitons() {
       test.startTest();
        getUser();
			Cycle_Count_Line_Item__c item =setupCycleCountLineItem();
			List<Order> olist= setupOrder();
			system.debug('Order inserted=='+olist);
            Map<id,Verification__c> VerifyById = new Map<id,Verification__c>();            
            
			// insert item;
            //Cycle_Count_Line_Item__c item1 =setupCycleCountLineItem1();
        	system.debug('######' +item);
        	//VerificationManager manager = new VerificationManager();
        	//manager.ItemsbyId.put(item.id,item);
        
            Verification__c v1 = new Verification__c();        
            v1.Cycle_Count_Line_Item__c = item.id; 
        	system.debug('Verification##**' +v1);
            
            Verification__c v2 = new Verification__c();
            v2.Cycle_Count_Line_Item__c = item.id;
            system.debug('Verification##**' +v2);
		
            List<Verification__c> vlist = new List<Verification__c>{v1,v2};
            insert vlist;
        	
            for(Verification__c v: vlist){
                VerifyById.put(v.id,v);
            }
            //VerificationManager manager = new VerificationManager();
            
            //manager.fetchVerificaitons(vlist);
            //manager.fetchRelatedFieldRep();
            //manager.fetchOrderswithCriterion();
		
		test.stopTest();
	}

}
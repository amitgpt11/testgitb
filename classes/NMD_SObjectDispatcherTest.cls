@IsTest(SeeAllData=false)
private class NMD_SObjectDispatcherTest {
	
	final static String ACCTNAME = 'Account Name';
	final static RestDispatcherV1 dispatcher = new RestDispatcherV1(null);

	static {

		RestContext.response = new RestResponse();

	}
	
	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		// create one consignment acct for context user, one for a test user
		Account acct = td.createConsignmentAccount();
		acct.Name = ACCTNAME;
		insert acct;

	}

	static testMethod void test_OrderItemsDispatcher_noAction() {

		List<Account> accts = new List<Account>([
			select
				Name
			from Account
			where Name = :ACCTNAME
		]);

		Map<String,String> parameters = new Map<String,String>();

		NMD_SObjectDispatcher handler = new NMD_SObjectDispatcher();
		String uri = handler.getURIMapping();
		handler.execute(dispatcher, parameters, null);

	}

	static testMethod void test_OrderItemsDispatcher_noBody() {

		Map<String,String> parameters = new Map<String,String> {
			'action' => 'update'
		};

		NMD_SObjectDispatcher handler = new NMD_SObjectDispatcher();
		handler.execute(dispatcher, parameters, null);

	}

	static testMethod void test_OrderItemsDispatcher_badBody() {

		Map<String,String> parameters = new Map<String,String> {
			'action' => 'update'
		};

		NMD_SObjectDispatcher handler = new NMD_SObjectDispatcher();
		handler.execute(dispatcher, parameters, Blob.valueOf('foo'));

	}

	static testMethod void test_OrderItemsDispatcher_badSave() {

		List<Account> accts = new List<Account>([
			select
				RecordTypeId
			from Account
			where Name = :ACCTNAME
		]);

		accts[0].RecordTypeId = OpportunityManager.RECTYPE_EXPLANT; // illegal record type!

		Map<String,String> parameters = new Map<String,String> {
			'action' => 'update'
		};

		NMD_SObjectDispatcher handler = new NMD_SObjectDispatcher();
		handler.execute(dispatcher, parameters, Blob.valueOf(JSON.serialize(accts)));

		Account acct = [select RecordTypeId from Account where Id = :accts[0].Id];
		System.assertNotEquals(OpportunityManager.RECTYPE_EXPLANT, acct.RecordTypeId);

	}

	static testMethod void test_OrderItemsDispatcher_update() {

		String newName = 'NEW Name';

		List<Account> accts = new List<Account>([
			select
				Name
			from Account
			where Name = :ACCTNAME
		]);

		accts[0].Name = newName;

		Map<String,String> parameters = new Map<String,String> {
			'action' => 'update'
		};

		NMD_SObjectDispatcher handler = new NMD_SObjectDispatcher();
		handler.execute(dispatcher, parameters, Blob.valueOf(JSON.serialize(accts)));

		Account acct = [select Name from Account where Id = :accts[0].Id];
		System.assertEquals(newName, acct.Name);

	}

}
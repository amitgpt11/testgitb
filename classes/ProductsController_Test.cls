@isTest
private class ProductsController_Test {
 
    private static testMethod void testmethod2() {
        
        User usr = Test_DataCreator.createCommunityUser();
        
        system.Test.setCurrentPage(Page.Products);
        Test_DataCreator.createUserSalesOrganisation([SELECT contact.AccountId FROM User WHERE id=: usr.Id].contact.AccountId);
        
        system.runAs(usr){
            
            ProductsController objProductsController = new ProductsController();
            objProductsController.init();
        }
         
        Product2 objParentProd = Test_DataCreator.createProductEndo('test');
        system.runAs(usr){
            
            ApexPages.currentPage().getParameters().put('productFamily','test');
            ProductsController objProductsController = new ProductsController();
            objProductsController.init();
            
            System.assertEquals('No active product found under product family - test',objProductsController.errorMessage);
        }
        
        Test_DataCreator.createProductsWithParent(objParentProd.Id);
        List<Product2> objProducts = new List<Product2>([SELECT Id FROM Product2]);
            
        system.runAs(usr){
            
            for(Product2 objProd : objProducts){
                
                Test_DataCreator.createSharedProductExtension(objProd.Id);
            }
            
            ApexPages.currentPage().getParameters().put('productFamily','test');
            ProductsController objProductsController = new ProductsController();
            objProductsController.init();
            System.assertNotEquals(0,objProductsController.lstProductWrapper.size());
        }
        
    }
    
    private static testMethod void testmethodLocale() {
        
        User usr = Test_DataCreator.createCommunityUserWithLocale();
        
        system.Test.setCurrentPage(Page.Products);
        Test_DataCreator.createUserSalesOrganisation([SELECT contact.AccountId FROM User WHERE id=: usr.Id].contact.AccountId);
        
        system.runAs(usr){
            
            ProductsController objProductsController = new ProductsController();
            objProductsController.init();
        }
         
        Product2 objParentProd = Test_DataCreator.createProductEndo('test');
        system.runAs(usr){
            
            ApexPages.currentPage().getParameters().put('productFamily','test');
            ProductsController objProductsController = new ProductsController();
            objProductsController.init();
            
            System.assertEquals('No active product found under product family - test',objProductsController.errorMessage);
        }
        
        Test_DataCreator.createProductsWithParent(objParentProd.Id);
        List<Product2> objProducts = new List<Product2>([SELECT Id FROM Product2]);
            
        system.runAs(usr){
            
            for(Product2 objProd : objProducts){
                
                Test_DataCreator.createSharedProductExtension(objProd.Id);
            }
            
            ApexPages.currentPage().getParameters().put('productFamily','test');
            ProductsController objProductsController = new ProductsController();
            
            Shared_Community_Product_Translation__c objSharedTranslation = Test_DataCreator.createProductTranslationRecord(objParentProd.Id,usr.Shared_Community_Division__c);
            objSharedTranslation.Shared_Language__c = objProductsController.mapLocaleToLanguage.get(usr.LanguageLocaleKey);
            update objSharedTranslation;
            
            // objProductsController.fetchRequestedData();
            
            objProductsController.init();
            System.assertNotEquals(0,objProductsController.lstProductWrapper.size());
            
        }
        
    }
}
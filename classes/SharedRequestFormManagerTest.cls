/**
* Test class for SharedRequestFormManager 
*
* @Author Accenture Services
* @Date 13OCT2016
*/
@isTest(SeeAllData=false)
private class SharedRequestFormManagerTest {

public static Schema.DescribeFieldResult fieldResult1 = Shared_Request_Form__c.PI_Reason_Code__c.getDescribe();
public static List<Schema.PicklistEntry> piReasonCode  = fieldResult1.getPicklistValues();

public static Schema.DescribeFieldResult fieldResult2 = Shared_Request_Form__c.PI_Ship_Via__c.getDescribe();
public static List<Schema.PicklistEntry> piShipVia  = fieldResult2.getPicklistValues();

public static Map <String,Schema.RecordTypeInfo> recordTypes_SharedRequestForm = Shared_Request_Form__c.sObjectType.getDescribe().getRecordTypeInfosByName();
public static Id recordTypeID_SharedRequestForm  = recordTypes_SharedRequestForm.get('PI Sample Request Form').getRecordTypeId(); 

public static list<Shared_Request_Form__c> tempLst = new list<Shared_Request_Form__c>();  
public static list<Shared_Request_Form__c> tempUpdateLst = new list<Shared_Request_Form__c>(); 

@testSetup 
static void setupData() {
   
    User user4 = UtilForUnitTestDataSetup.newUser('US PI User');
    user4.Username = 'RequestFormUser1@abc.com';
    insert user4;

}


static TestMethod void TestSharedRequestFormManager1(){ 

    Account acc = new Account();
    acc.Name ='Test Account';
    acc.ShippingStreet = 'street';
    acc.ShippingCity = 'city';
    acc.ShippingState = 'state';
    acc.ShippingPostalCode = '123445';
    acc.Account_Number__c = '123456';                                
    insert acc;
    system.assertEquals(acc.Account_Number__c,'123456');
    list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%']);
    
    PI_Sample_Budget_Tracker__c Budget1 = new PI_Sample_Budget_Tracker__c();
    Budget1.PI_Cost_Center__c = '1110099';
    Budget1.PI_Quarter_End_Date__c = system.today()+10;
    Budget1.PI_Quarter_Start_Date__c = system.today()-10;
    Budget1.PI_Current_Quarter_Sample_Budget__c = 100000;
    insert Budget1;
    
    
        for(integer i=0;i<100;i++){
            Shared_Request_Form__c srf1 = new Shared_Request_Form__c();
            srf1.RecordTypeId = recordTypeID_SharedRequestForm;
            srf1.Facility__c = acc.Id;
            srf1.PI_ATTN__c = '12334'+i;
            srf1.Shipping_Address__c = acc.ShippingStreet;
            srf1.City__c = acc.ShippingCity;
            srf1.State__c = acc.ShippingState;
            srf1.Zip_Code__c = acc.ShippingPostalCode;
            srf1.PI_Reason_Code__c = piReasonCode[0].Value;
            srf1.PI_Ship_Via__c = piShipVia[0].Value;
            srf1.PI_N_C_PO__c = 'test'+'-'+i;
            srf1.SAP_Account_Number__c = acc.Account_Number__c;
            srf1.Requesters_Name__c = urLst[0].Id;
            srf1.Cost_Center__c = '1110099';
            tempLst.add(srf1);
        }
        
        
        test.startTest();
        insert tempLst;
        system.assertEquals([SELECT PI_Total_Sample_Budget__c from Shared_Request_Form__c limit 1].PI_Total_Sample_Budget__c ,Budget1.PI_Current_Quarter_Sample_Budget__c);
    
        for(Shared_Request_Form__c srf1 :tempLst){
            srf1.PI_Approval_Status__c = 'Approved';
            srf1.PI_Approved_Rejected_Date__c = system.today();
            tempUpdateLst.add(srf1);
        }
        update tempUpdateLst;
        test.stopTest();
        
        
}



}
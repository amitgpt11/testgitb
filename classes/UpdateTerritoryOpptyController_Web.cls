/**
 * Name : UpdateTerritoryOpptyController_Web
 * Author : Accenture - Mayuri
 * Description : This class is invoked by Visual Force page ETM_Oppty_Territory_Update_VF_SF1 
 * Date : 27APR2016
 */
public with sharing class UpdateTerritoryOpptyController_Web {
    public Opportunity oppty{get;set;}
    public Id accId{get;set;}
    public list<Territory2> results{get;set;} // search results
    public string searchString{get;set;} // search keyword 
    public list<Id> terrIdLst{get;set;}
    public list<Territory2> terrLst{get;set;}
    public list<ObjectTerritory2Association> objT2ALst{get;set;}
    public String terrSelected{get;set;}
    private final ApexPages.StandardController CONTROL;
    public boolean isShow{get;set;}
    public boolean isError{get;set;}

    public UpdateTerritoryOpptyController_Web(ApexPages.StandardController controller) {
        terrIdLst = new list<Id>();
        terrLst = new list<Territory2>();
        objT2ALst = new list<ObjectTerritory2Association>();
        oppty = (Opportunity)controller.getRecord();  
        
        this.CONTROL = controller;  
        isShow = false;
        isError = false;   
        retrieveQueryResults();
    }
    
    public void retrieveQueryResults(){
    
    if(oppty.Id !=null){
    
            accId = [SELECT Id,AccountId FROM Opportunity WHERE Id =: oppty.Id].AccountId ;//Get the accountId related to Oppty id
            
            system.debug('accId : '+accId);
            
            objT2ALst =  [SELECT Id,ObjectId,Territory2Id FROM ObjectTerritory2Association WHERE ObjectId =: accId];//Get the list of territories related to the account
            for(ObjectTerritory2Association  accgrp : objT2ALst)//Loop through the list of territories
            {
                terrIdLst.add(accgrp.Territory2Id);
            }
            
            terrLst = [SELECT Id, Name,DeveloperName,ParentTerritory2Id,Description,Territory2TypeId FROM Territory2 where Id IN : terrIdLst];
            results = terrLst;
            if(results.size() > 0){
                isShow = true;
            }
            System.debug('******'+results );
            
        }
    }
    public UpdateTerritoryOpptyController_Web() {
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        results=terrLst;
        runSearch();  
    }
    
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        results = performSearch(searchString); 
        if(results.size() > 0 ){
            isShow = true;
            isError = false;
        }
        else{
            isShow = false;
            isError = true;
        }
                      
    } 
    
    // run the search and return the records found. 
    private List<Territory2> performSearch(string searchString) {
        String terr = 'SELECT Id, Name,DeveloperName,ParentTerritory2Id,Territory2TypeId,Description  FROM Territory2';
        if(searchString != '' && searchString != null)
            terr = terr+  ' WHERE Name LIKE \'%' + searchString +'%\'';
        terr = terr + ' limit 200'; 
        System.debug(terr+'terr');
        return database.query(terr);        
    }    
    
    
    public void dummyAction(){
        
    }  
    
     public PageReference saveOppty(){
        boolean Excep;
        if(accId  != null){
            oppty.Territory2Id = terrSelected;
            try{
                update oppty; 
                Excep = false;
            }catch(Exception e){
                Excep = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You must select a territory from the active territory model that the parent account is assigned to.'));
            }
         }
         if(Excep){
             return null;
         }
         else{
            pagereference ref = new pagereference('/'+oppty.id);
            ref.setredirect(true);
            return ref;
         }
    }
    public PageReference redirectToPage(){
        pagereference ref = new pagereference('/'+oppty.id);
        ref.setredirect(true);
        return ref;
    }
    

}
@isTest
private class ProductCategoriesController_Test {
    
    private static testMethod void test() {
     
     User usr = Test_DataCreator.createCommunityUser();
        
        system.Test.setCurrentPage(Page.Products);
        Test_DataCreator.createUserSalesOrganisation([SELECT contact.AccountId FROM User WHERE id=: usr.Id].contact.AccountId);
        
        Product2 objParentProd = Test_DataCreator.createProductEndo('test');
        Test_DataCreator.createProductCategoryImageMapping(objParentProd.Family,'test_doc');
        Test_DataCreator.createProductsWithParent(objParentProd.Id);
        List<Product2> objProducts = new List<Product2>([SELECT Id FROM Product2]);
        
        //folder and doc for prod family
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'test_doc';
        document.IsPublic = true;
        document.Name = 'My Document';
        document.FolderId = [select id from folder where DeveloperName = 'Product_Category_Images'].id;
        insert document;
        
        system.runAs(usr){
            
            for(Product2 objProd : objProducts){
                
                Test_DataCreator.createSharedProductExtension(objProd.Id);
            }
            
            ApexPages.currentPage().getParameters().put('productFamily','test');
            ProductCategoriesController objProductCategoriesController = new ProductCategoriesController();
            objProductCategoriesController.init();
            system.assertNotEquals(0,objProductCategoriesController.lstProductCategories.size());
        }
        
    }
}
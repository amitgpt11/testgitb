/**
* Trigger Handler class for the custom Inventory_Transaction__c sobject
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class InventoryTransactionTriggerHandler extends TriggerHandler {

  final InventoryTransactionManager manager = new InventoryTransactionManager();

  public override void bulkBefore() {

    if (trigger.isInsert) {
      manager.fetchMatchingZTKs(trigger.new);
    }
    //added by dipil to replicate process builders's work
    manager.fetchLocationOwners(trigger.new);

  }

  public override void beforeInsert(SObject obj) {

    Inventory_Transaction__c trans = (Inventory_Transaction__c)obj;
    manager.matchZTKATransfer(trans);
    manager.matchZTKBTransfer(trans);
    manager.updateTransactionRecord(trans);

  }
  
  public override void bulkAfter() {

    manager.fetchRelatedRecords(trigger.old, trigger.new);

  }
  
  public override void beforeUpdate(SObject oldObj, SObject newObj) {

    Inventory_Transaction__c trans = (Inventory_Transaction__c)newObj;
    manager.updateTransactionRecord(trans);

  }

  public override void afterInsert(SObject obj) {

    Inventory_Transaction__c trans = (Inventory_Transaction__c)obj;
    //manager.updateMatchingPatientOrderItem(trans);
    manager.updateParentAvailableQuantity(trans);
    manager.updateParentInTransit(trans);
    manager.updateParentSAPCalculatedQuantity(trans);
    manager.updateOrderStatusToActivated(trans);

  }

  public override void afterUpdate(SObject oldObj, SObject newObj) {

    Inventory_Transaction__c oldTrans = (Inventory_Transaction__c)oldObj;
    Inventory_Transaction__c newTrans = (Inventory_Transaction__c)newObj;
    manager.updateParentAvailableQuantity(oldTrans, newTrans);
    manager.updateParentInTransit(oldTrans, newTrans);
    manager.updateParentSAPCalculatedQuantity(oldTrans, newTrans);
    manager.updateOrderStatusToActivated(newTrans);

  }

  public override void afterDelete(SObject obj) {

    Inventory_Transaction__c trans = (Inventory_Transaction__c)obj;
    manager.updateParentAvailableQuantity(trans);
    manager.updateParentInTransit(trans);
    manager.updateParentSAPCalculatedQuantity(trans);

  }

  public override void afterUndelete(SObject obj) {

    Inventory_Transaction__c trans = (Inventory_Transaction__c)obj;
    manager.updateParentAvailableQuantity(trans);
    manager.updateParentInTransit(trans);
    manager.updateParentSAPCalculatedQuantity(trans);

  }

  public override void andFinally() {

    manager.commitRelatedRecords();

  }

}
/*
    Description: Controller Class for RemoveInviteesToENDOEvent VF page, This Controller is for deleting the invitees from the event.
    Developed By: Shashank Gupta
    Development Date: 8-9-2016
*/
public class EndoInviteeRemoveController{
    public List<EventRelationwrapper> EventRelationWrapperList {get;set;}
    public String EId {get;set;}
    public List<EventRelation> EventRelationList {get;set;}
    Set<EventRelation> ERToDelete = new Set<EventRelation>();
    List<EventRelation> ERToDeleteFinal = new List<EventRelation>();
    List<EventRelationwrapper> listUnSelectedRecords  = new List<EventRelationwrapper>();
    Public String EventName {get; set;}
    public boolean Isselected {get; set;}
    
    public EndoInviteeRemoveController(ApexPages.StandardController controller){

    //public EndoInviteeRemoveController(){
    system.debug('***st Hellor**');
        EventRelationList = new List<EventRelation>();
        EventRelationWrapperList = new List<EventRelationwrapper>();
        EId =  ApexPages.currentPage().getParameters().get('Id');
        EventRelationList = [SELECT AccountId,EventId,Event.Subject,Id,IsInvitee,IsParent,IsWhat,RelationId,Relation.Name,RespondedDate,Response,Status FROM EventRelation WHERE EventId = :EId AND IsInvitee = true];
        string str = 'SELECT AccountId,EventId,Event.Subject,Id,IsInvitee,IsParent,IsWhat,RelationId,Relation.Name,RespondedDate,Response,Status FROM EventRelation WHERE EventId ='+EId+' AND IsInvitee ='+'true';
        
        system.debug('***str**'+str);
        system.debug('***EId **'+EId );
        system.debug('***EventRelationList ***'+EventRelationList );
        if(!EventRelationList.isEmpty()){
            for(EventRelation er: EventRelationList){
                 EventRelationwrapper erw = new EventRelationwrapper();
                 erw.isSelected =  false;
                 erw.ERObj = er;
                 EventRelationWrapperList.add(erw);
            } //end of for loop.
        } //end of if condition.
        if(!EventRelationList.isEmpty()){
            EventName = EventRelationList[0].Event.Subject;
        }
        Isselected = false;
    }
    
    /*
   Select EventRelation(Invitees) for current event record.
  */
    public PageReference getSelected(){ 
        if(EventRelationWrapperList !=null && EventRelationWrapperList.size()>0) {
            for(EventRelationwrapper wrapObj :  EventRelationWrapperList){
                if(wrapObj.isSelected == true){
                    ERToDelete.add(wrapObj.ERObj);
                }else{
                    listUnSelectedRecords.add(wrapObj);
                }           
            }//end of for.
            if(ERToDelete !=null && ERToDelete.size()>0){
                Isselected = true;
            }    
        }// end of if
        return null;   
    }
  
  /*
   Delete EventRelation(Invitees) for current event record.
  */
    public PageReference DeleteEventRelation(){
        if(EventRelationWrapperList !=null && EventRelationWrapperList.size()>0) {
        /*
        checking the delete list size and assign the unselected values to 
        original wrapper list.
        */
            if(ERToDelete !=null && ERToDelete.size()>0){
            for(EventRelation e:ERToDelete){
                ERToDeleteFinal.add(e);
            }
            try{
                delete ERToDeleteFinal;
            }
            catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please Contact Your System Administrator'));
            }
                EventRelationWrapperList.clear();
                EventRelationWrapperList.addAll(listUnSelectedRecords);
            }           
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info, 'Records were not there to delete.');
            ApexPages.addMessage(myMsg);
        }
        
        return null;
    }
    
 /* Wrapper class with checkbox and EventRelation object. 
  this is also  called as inner class 
  */

    @TestVisible public class EventRelationwrapper{
        public boolean isSelected {get;set;}
        public EventRelation ERObj {get;set;}    
    }
}
/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_QNProductsBatchTest {
	
	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Account acct = td.createConsignmentAccount();
		acct.Account_Number__c = td.randomString5();
		acct.Classification__c = 'NM Sales Rep';
		insert acct;

		// sets of matching data
		List<String> types = new List<String> {
			'QN Add', 'QN Delete'
		};

		List<String> lots = new List<String> {};
		List<String> serials = new List<String> {};
		for (Integer i = 0; i < 10; i++) {
			lots.add('Lot ' + i);
			serials.add('Serial ' + i);
		}

		// create 10 products
		List<Product2> prods = new List<Product2>{};
		for (Integer i = 0; i < 10; i++) {
			prods.add(td.newProduct('Product ' + i));
		}
		insert prods;

		List<Id> models = new List<Id>{};
		for(Product2 prod : prods) {
			models.add(prod.Id);
		}

		Inventory_Data__c data = new Inventory_Data__c(
			Name = 'Inventory Data',
			Account__c = acct.Id
		);
		insert data;

		Inventory_Location__c location = new Inventory_Location__c(
			Name = 'Inventory Location',
			Inventory_Data_ID__c = data.Id
		);
		insert location;

		// create 200 inventory items
		List<Inventory_Item__c> items = new List<Inventory_Item__c>();
		for (Integer i = 0; i < 10; i++) {
			items.add(new Inventory_Item__c(
				RecordTypeId = InventoryItemManager.RECTYPE_NMD,
				Inventory_Location__c = location.Id,
				Lot_Number__c = lots[i],
				Serial_Number__c = serials[i],
				Model_Number__c = models[i],
				Disposition__c = 'Available'
			));
		}
		insert items;

		List<Inventory_Transaction__c> trans = new List<Inventory_Transaction__c>();
		for (Integer i = 0; i < 10; i++) {
			trans.add(new Inventory_Transaction__c(
				Inventory_Item__c = items[i].Id,
				Quantity__c = 1
			));
		}
		insert trans;

		// create several lists
		List<List__c> lists = new List<List__c>{};
		for (Integer i = 0; i < 10; i++) {
			lists.add(new List__c(
				RecordTypeId = NMD_QNProductsBatch.RECTYPE_QN,
				Lot_Number__c = lots[i],
				Serial_Number__c = serials[i],
				Model_Number__c = models[i],
				Type__c = types[Math.mod(i, types.size())]
			));
		}

		// create special one for product update
		lists.add(new List__c(
			RecordTypeId = NMD_QNProductsBatch.RECTYPE_QN,
			Model_Number__c = models[0],
			Type__c = types[0]
		));

		insert lists;

	}

	static testMethod void test_CreateSchedule() {

		NMD_QNProductsBatch.createHourlySchedule();

	}

	static testMethod void test_QNProductsBatch() {

		System.debug([
			select
				Disposition__c,
                Is_QN_Lot__c,
                Lot_Number__c,
                Model_Number__c,
                Serial_Number__c
            from Inventory_Item__c
            where RecordTypeId = :InventoryItemManager.RECTYPE_NMD
        ]);

		Test.startTest();
		{
			new NMD_QNProductsBatch().execute(null);
		}
		Test.stopTest();

	}
	
}
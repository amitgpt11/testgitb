@isTest
public class Test_DataCreator {

    public static Account createAccount(String strName){
        
        ID owner = [Select Id FROM User WHERE UserRoleId != null AND IsActive = true LIMIT 1].Id;
        //create Account  
        Account objAccount = new Account(Name = strName, OwnerId = UserInfo.getUserId(), BillingStreet = '123 Main', BillingCity='Seattle',BillingState='WA',BillingPostalCode='98211',BillingCOuntry='United States');
        Insert objAccount;

        return objAccount;
    }
    
    public static Contact createContact(String strlastName, Id accountId){
        
        //create Account  
        ID owner = [Select Id FROM User WHERE UserRoleId != null AND IsActive = true LIMIT 1].Id;

        Contact objContact = new Contact(lastname = strlastName, AccountId = accountId, OwnerId = UserInfo.getUserId());
        insert objContact;
        return objContact;
    }
    
    public static Meeting__c createMeeting(String strSubject){
        
        Meeting__c objMeeting = new Meeting__c(Name = strSubject);
        insert objMeeting;
        return objMeeting;
    }
    
    public static Event createEvent(Id IdWhat, Id IdWho){
        
        Event objEvent = new Event(ActivityDateTime = datetime.now(), DurationInMinutes = 10, Subject='creating test event',
            WhatId = IdWhat, WhoId= IdWho, Type='Visit');
        
        insert objEvent;
        return objEvent;
    }
    
    public static Product2 createProduct(String strName){
        
        Product2 p2 = new Product2( Name = strName, IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Cardio',Shared_Video_URL__c = 'test.com',Shared_Video_URL_2__c = 'test.com',
            Shared_Product_Image_URL__c = 'product.com', Shared_Community_Description__c = 'test description', ProductCode = 'test code', Family = 'test',
            Product_Label__c = 'product label');
          
        Insert p2;
        return p2;
    }
    
    public static Product2 createProductEndo(String strName){
        
        Product2 p2 = new Product2( Name = strName, IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo',Shared_Video_URL__c = 'test.com',Shared_Video_URL_2__c = 'test.com',
            Shared_Product_Image_URL__c = 'product.com', Shared_Community_Description__c = 'test description', ProductCode = 'test code', Family = 'test',
            Product_Label__c = 'product label', Shared_Top_Level_Parent__c = true);
          
        Insert p2;
        return p2;
    }
    
    public static void createProducts(){
        
        List<Product2> lstproducts = new List<product2>();
        
        Id recType = [SELECT ID FROM RecordType WHERE Name = 'InTouch Product'].Id;
        
        Product2 p1 = new Product2( Name = 'test', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Shared_Product_Image_URL__c = 'test.com', RecordTypeId = recType);
        lstproducts.add(p1);        
        Product2 p2 = new Product2( Name = 'testName2', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Shared_Product_Image_URL__c = 'test.com', RecordTypeId = recType);
        lstproducts.add(p2);        
        Product2 p3 = new Product2( Name = 'testName3', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Cardio', Shared_Community_Description__c = 'test description', RecordTypeId = recType);
        lstproducts.add(p3);
        Product2 p4 = new Product2( Name = 'testName4', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Cardio', ProductCode = 'test code', RecordTypeId = recType);
        lstproducts.add(p4);
        Product2 p5 = new Product2( Name = 'testName5', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Cardio', Family = 'test', RecordTypeId = recType);
        lstproducts.add(p5);
        Product2 p6 = new Product2( Name = 'testName6', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Cardio', Product_Label__c = 'product test', RecordTypeId = recType);
        lstproducts.add(p6);
        Product2 p7 = new Product2( Name = 'testName7', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', RecordTypeId = recType);
        lstproducts.add(p7);
        
        Insert lstproducts;          
        
       //// lstproducts[1].Shared_Parent_Product__c = lstproducts[0].Id;
        //update lstproducts[1];
    }
    
     public static void createProductsWithParent(Id parentProductId){
        
        List<Product2> lstproducts = new List<product2>();
        
        Id recType = [SELECT ID FROM RecordType WHERE Name = 'InTouch Product'].Id;
        
        Product2 p1 = new Product2( Name = 'test', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test' , Shared_Product_Image_URL__c = 'test.com', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p1);        
        Product2 p2 = new Product2( Name = 'testName2', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', Shared_Product_Image_URL__c = 'test.com', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p2);        
        Product2 p3 = new Product2( Name = 'testName3', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', Shared_Community_Description__c = 'test description', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p3);
        Product2 p4 = new Product2( Name = 'testName4', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', ProductCode = 'test code', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p4);
        Product2 p5 = new Product2( Name = 'testName5', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p5);
        Product2 p6 = new Product2( Name = 'testName6', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', Product_Label__c = 'product test', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p6);
        Product2 p7 = new Product2( Name = 'testName7', IsActive = true, CurrencyIsoCode = 'GBP', Division__c = 'Endo', Family = 'test', RecordTypeId = recType, Shared_Parent_Product__c=parentProductId);
        lstproducts.add(p7);
        
        Insert lstproducts;          
        
       
    }
    
    public static Shared_Community_Page__c createCommunityPage(){
        
        Shared_Community_Page__c objSharedPage = new Shared_Community_Page__c(name = 'testname', Shared_Page_Name__c = 'BSC_SiteTemplate', 
            Shared_Page_URL__c = 'https://www.test.com');
        insert objSharedPage;
        return objSharedPage;
        
    }
    
    public static Shared_Community_METIS_Code__c createCommunityMETISCode(Id masterId){
        
        Shared_Community_METIS_Code__c objShared_Community_METIS_Code = new Shared_Community_METIS_Code__c(Shared_Division__c = 'Endo', 
            Shared_Date_Assigned__c = Date.Today(), Shared_Country__c = 'France', Shared_Community_Page__c = masterId, Shared_METIS_Code__c = 'test');
        
        // Shared_Community_METIS_Code__c objShared_Community_METIS_Code1 = new Shared_Community_METIS_Code__c(name = 'testname1', Shared_Division__c = 'Cardio', 
        //     Shared_Date_Assigned__c = Date.Today(), Shared_Country__c = 'Default', Shared_Community_Page__c = masterId);
        
        Insert objShared_Community_METIS_Code;
        return objShared_Community_METIS_Code;
    }
    
    public static void cerateBostonCSHeaderFooterTestData(){
        
        Boston_Scientific_Header_Footer_Config__c objHeaderFooter = new Boston_Scientific_Header_Footer_Config__c(Header_Logo_Id__c = '015R0000000GsnF',
            Header_Nav1_Label__c = 'Products', Header_Nav1_Value__c = '/ProductsPage', Header_Nav2_Label__c = 'Education', Header_Nav2_Value__c = '/EducationPageName', 
            Header_Nav3_Label__c = 'Events', Header_Nav3_Value__c = '/EventsPageName', Header_Nav4_Label__c = 'ContactUs', Header_Nav4_Value__c = 'ContactUsPageName',
            Header_Option1_Label__c = 'My Account', Header_Option1_Value__c='/AccountPageNAme',Header_Option2_Label__c = 'My Cart',Header_Option2_Value__c = '/MyCartPageNAme',
            Header_Option3_Label__c = 'Logout',Header_Option3_Value__c = '/LogoutPageName',Header_Title__c = '', Name = 'Default',
            Footer_Logo_Id__c = '015R0000000GsnF'); 
            
        insert objHeaderFooter;
    }
    
    public static Boston_Scientific_Config__c cerateBostonCSConfigTestData(Id Idevent){
    
        Product2 objProduct = createProduct('testProduct');
        
        Boston_Scientific_Config__c objBostonScientificConfig = new Boston_Scientific_Config__c(Name = 'Default', 
            Boston_AuthHome_Cardio_FeaturedEvent_Id__c = Idevent,Boston_AuthHome_Cardio_FeaturedPrduct_Id__c = objProduct.Id,
            Boston_AuthHome_Endo_FeaturedPrduct_Id__c = '' ,Boston_EndoFeaturedEvent_logoId__c = Idevent,
            Boston_EndoFeaturedProduct_logoId__c = Idevent,Boston_UnAuthenticatedpage_BannerImageId__c = Idevent,
            Boston_Events_Till_Date__c = system.Today(), Boston_Email_Template_Right_Logo_Id__c = Idevent,
            Boston_AuthHome_Endo_FeaturedEvent_Id__c = Idevent, Boston_Document_Content_Base_Url__c = 'https://bsci--dev2.cs2.my.salesforce.com', 
            Boston_Availability_Color_code__c = '#4bed84',
            Boston_Non_Availability_Color_Code__c = '#FF5C5F',  Boston_PastDate_Color_Code__c = '#C8C8C8',
            Boston_No_of_Month_to_Fetch_Meetings__c = 6, Boston_Document_Download_Url__c = 'http://sfc/servlet.shepherd/version/download/',
            Boston_AuthHome_InTouch_Tile1_Label__c = 'tile1Label', Boston_AuthHome_InTouch_Tile1_Nav__c = 'tile1Nav',
            Boston_AuthHome_InTouch_Tile2_Label__c = 'tile2Label', Boston_AuthHome_InTouch_Tile2_Nav__c = 'tile2Nav',
            Boston_AuthHome_InTouch_Tile3_Label__c = 'tile3Label', Boston_AuthHome_InTouch_Tile3_Nav__c = 'tile3Nav',
            Boston_AuthHome_InTouch_Tile4_Label__c = 'tile4Label', Boston_AuthHome_InTouch_Tile4_Nav__c = 'tile4Nav',
            Boston_AuthHome_InTouch_Tile5_Label__c = 'tile5Label', Boston_AuthHome_InTouch_Tile5_Nav__c = 'tile5Nav',
            Boston_AuthHome_InTouch_Tile6_Label__c = 'tile6Label', Boston_AuthHome_InTouch_Tile6_Nav__c = 'tile6Nav'
        );
        
        insert objBostonScientificConfig;
        return objBostonScientificConfig;
    }
    
    public static BSC_VTM_Alignment__c cerateBSC_VTM_AlignmentTestData(){
    
        BSC_VTM_Alignment__c objBSCVTMAlignment = new BSC_VTM_Alignment__c(Name ='Test',
            Shared_Country__c = 'test', Shared_Division__c = 'Endo', Shared_Email__c = 'test@test.com');
        
        insert objBSCVTMAlignment;
        return objBSCVTMAlignment;
    }
    
    public static Community_Event__c createCommunityEvent(String strDisplayName){
        
        Community_Event__c obj = new Community_Event__c(Shared_City__c = 'Boston', Shared_Start_Date__c = Date.Today(), Shared_Display_Name__c = 'Dummy Event',
            Shared_Description__c = 'test description', Shared_Division1__c = 'Endo', Shared_Country__c = 'Germany');
        insert obj;
        return obj;
        
    }
    
    public static Community_Event__c createCommunityEvent(Date startDate){
        
        Community_Event__c obj = new Community_Event__c(Shared_City__c = 'Boston', Shared_Start_Date__c = startDate, Shared_Display_Name__c = 'Dummy Event',
            Shared_Description__c = 'test description', Shared_Division1__c = 'Endo');
        insert obj;
        return obj;
    }
    public static User createGuestUser(){
        
        Id ProfileId = [Select Id 
            From Profile 
            Where Name = 'InTouch Profile' 
            Limit 1].Id;
        
        Account objAccount = createAccount('test Account');
        Contact objContact = createContact('testLastname', objAccount.Id);
        User objUser = new User(Alias = 'standt', Email='staard123@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = ProfileId, 
            TimeZoneSidKey='America/Los_Angeles', UserName='st121anda123*@testorg.com',
            Shared_Community_Division__c = 'Endo');
        
        Insert objUser;
        return objUser;
    }
    
    public static User createCommunityUser(){
        
        Id profileId = [Select Id 
            From Profile 
            Where Name = 'InTouch Community User'
            Limit 1].Id;  

        Account objAccount = createAccount('test Account');
        Contact objContact = createContact('testLastname', objAccount.Id);
        User objUser = new User(
        Alias = 'standt', 
        Email='staard123@testorg.com', 
        ContactId = objContact.Id,
        EmailEncodingKey='UTF-8',
        FirstName = 'first',
        LastName='Testing', 
        CommunityNickname = 'testUser123',
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', 
        ProfileId = ProfileId, 
        // IsActive = true,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName='standa123*@testorg.com',
        Country = 'France', Shared_Community_Division__c = 'Endo');

        Insert objUser;
        return objUser;
    }
    
    public static User createCommunityUserWithLocale(){
        //create user with diff locale
        Id ProfileId = [Select Id 
            From Profile 
            Where Name = 'InTouch Community User' 
            Limit 1].Id;
        
        Account objAccount = Test_DataCreator.createAccount('test Account1');
        Contact objContact = Test_DataCreator.createContact('testLastname1', objAccount.Id);
        User objUser = new User(Alias = 'standtt', Email='staard1234@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
            LocaleSidKey='fr_CA', ProfileId = ProfileId, ContactId = objContact.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='st121anda123*@testorg.com',
            Shared_Community_Division__c = 'Endo');
        
        Insert objUser;
        return objUser;
    }
    
    public static Meeting__c createMeetingForCalendar(String strSubject,DateTime Start_Time, DateTime End_Time,String Meeting_Request_Status,String AssignedTo){
        
        Meeting__c objMeeting = new Meeting__c(Name = strSubject, Start_Time__c = Start_Time, End_Time__c = End_Time, Meeting_Request_Status__c = Meeting_Request_Status);
        //, AssignedTo__c = AssignedTo);
        insert objMeeting;
        return objMeeting;
    }
    
    public static Shared_Community_Order__c createOrder() {
        
        Id contactId;
        List<User> lstUser = new List<User>();//new List<User>([Select Id, ContactId From User Where Id =: UserInfo.getUserId() AND ContactId != null]);
        




        if(lstUser.isEmpty()) {
           
            Account objAccount = createAccount('test Account');
            Contact objContact = createContact('testLastname', objAccount.Id);
            system.debug('## objContact : '+objContact);
            system.debug('## Contact : '+[Select OwnerId From Contact Where Id =: contactId]);
            system.debug('## User : '+UserInfo.getUserId());
            contactId = objContact.Id;
        }else{
           
           contactId = lstUser[0].ContactId;
        }
        




        Shared_Community_Order__c objOrder = new Shared_Community_Order__c(Contact__c = contactId, Status__c = 'Open', 
            Shared_Date_Submitted__c = DateTime.now(), OwnerId = UserInfo.getUserId());
        Insert objOrder;
        return objOrder;
    }
    
    public static Shared_Community_Order_Item__c createOrderItem(Id orderId, Id productId) {
        
        Shared_Community_Order_Item__c objOrderItem = new Shared_Community_Order_Item__c(Shared_Order__c = OrderId, Product__c = productId,
            Shared_Quantity__c = 10);
        Insert objOrderItem;
        return objOrderItem;
    }
    
    public static BSC_Community_Countries__c createCountriesCustomSetting(String name){
        
        BSC_Community_Countries__c objCountryCustomSetting = new BSC_Community_Countries__c(Name = name);
        Insert objCountryCustomSetting;
        return objCountryCustomSetting;
    }
    
    public static Shared_BSC_Community_Languages__c createLanguageCustomSetting(String name){
        
        Shared_BSC_Community_Languages__c objLanguageCustomSetting = new Shared_BSC_Community_Languages__c(Name = name);
        Insert objLanguageCustomSetting;
        return objLanguageCustomSetting;
    }
    
    public static Shared_Community_Product_Translation__c createProductTranslationRecord(String sharedProductId, String userDivision){
     
        Shared_Community_Product_Translation__c objProductTranslationRecord = new Shared_Community_Product_Translation__c(Shared_Product__c = sharedProductId,
                                                                                                                            Shared_Division__c = userDivision,
                                                                                                                            Shared_Product_Label__c = 'Test',
                                                                                                                            Shared_Language__c = 'French');
        Insert objProductTranslationRecord;
        return objProductTranslationRecord;
    }
    
    public static Product_Category_Image_Mapping__c createProductCategoryImageMapping(String Name, String DocumentUniqueName){
     
        Product_Category_Image_Mapping__c objProductDocumentUniqueName = new Product_Category_Image_Mapping__c(Name = name, Document_Unique_Name__c = DocumentUniqueName);
        Insert objProductDocumentUniqueName;
        return objProductDocumentUniqueName;
    }
    
    public static Shared_Sales_Organization__c createUserSalesOrganisation(Id accountId){
        
        Shared_Sales_Organization__c objSharedSalesOrganization = new Shared_Sales_Organization__c(Shared_Account__c=accountId,Shared_Sales_Organization__c='AL10');
        insert objSharedSalesOrganization;
        return objSharedSalesOrganization;
    }
    
    public static Shared_Product_Extension__c createSharedProductExtension(Id productId){
        
        Shared_Product_Extension__c objSharedProductExtension = new Shared_Product_Extension__c(Shared_Product__c=productId,Shared_Sales_Org__c='AL10');
        insert objSharedProductExtension;
        return objSharedProductExtension;
    }
}
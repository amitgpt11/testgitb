public class ProductsController extends AuthorizationUtil{
    
    
    public List<ProductsWrapper> lstProductWrapper{get;set;}
    public String productFamily {get; set;}
    public String errorMessage {get; set;}
    
    
    
    /**
* Description: method to query product information
*/
    public override void fetchRequestedData() {
        
        Set<String> setUserSharedSalesOrganization = new Set<String>();
        Set<String> setParentProducts = new Set<String>();
        lstProductWrapper = new List<ProductsWrapper>();
        
        productFamily = ApexPages.currentPage().getParameters().get('productFamily');
        
        if(String.isBlank(productFamily)) {
            
            errorMessage = 'No product family specified.';
        }
        
        //Get the Community User's Division, Language and AccountId
        List<User> lstCurrentUser = new List<User>([SELECT Shared_Community_Division__c, LanguageLocaleKey, Contact.AccountId
                                                    FROM User
                                                    WHERE Id =: UserInfo.getUserId()
                                                   ]);
        
        String strProductsQuery = 'SELECT Id, Name, Product_Label__c, toLabel(Family), Shared_Product_Image_URL__c, Shared_Product_Image_Align_Type__c'+
                                  ' FROM Product2'+
                                  ' WHERE toLabel(Family) =: productFamily'+
                                  ' AND Division__c =\''+ lstCurrentUser[0].Shared_Community_Division__c +'\''+
                                  ' AND Shared_Parent_Product__c = null'+
                                  ' AND IsActive = TRUE';

        system.debug('lstCurrentUser[0].Contact.AccountId============'+lstCurrentUser[0].Contact.AccountId);
        if(BSC_Util.isCommunityUser()){
            
            setUserSharedSalesOrganization = BSC_Util.fetchSharedOrgForCommunityUser(lstCurrentUser[0].Contact.AccountId);  
            system.debug('setUserSharedSalesOrganization========='+setUserSharedSalesOrganization);
            
            for (Product2 availableProducts : [SELECT Id, toLabel(Family), Shared_Parent_Product__c
                                               FROM Product2
                                               WHERE toLabel(Family) =: productFamily
                                               AND Division__c =: lstCurrentUser[0].Shared_Community_Division__c
                                               AND IsActive = TRUE
                                               AND Id IN :BSC_Util.fetchProductExtensionsForCommunityUser(setUserSharedSalesOrganization)]){

                setParentProducts.add(availableProducts.Shared_Parent_Product__c);
            }
            
            strProductsQuery += ' AND Id IN : setParentProducts';
        }
        strProductsQuery += ' order by Name';
      
       List<Product2> lstProducts = new List<Product2>();
       system.debug('strProductsQuery============='+strProductsQuery);
        lstProducts = Database.query(strProductsQuery);
        
        
        
        if(lstProducts.isEmpty()) {
            
            errorMessage = 'No active product found under product family - ' + productFamily;
        } else{ //BSMI-253
            
            Set<Id> setProductIds = new Set<Id>();
            Map<Id,String> MapProductIdsToLabel = new Map<Id,String>();
            for(Product2 objProduct: lstProducts){
                
                setProductIds.add(objProduct.Id);                
            }
            
            // 
            if(lstCurrentUser[0].LanguageLocaleKey != 'en_US'){
            
                for(Shared_Community_Product_Translation__c objProductTranslation : [SELECT Id, Name, Shared_Product_Label__c, Shared_Product_Family__c , Shared_Document_URL__c, Shared_Product__c
                           FROM Shared_Community_Product_Translation__c
                           WHERE Shared_Product__c =: setProductIds
                           AND Shared_Language__c =: mapLocaleToLanguage.get(lstCurrentUser[0].LanguageLocaleKey)
                           ORDER BY Name]){
                           
                      MapProductIdsToLabel.put(objProductTranslation.Shared_Product__c, objProductTranslation.Shared_Product_Label__c); 
               }
           }
                         
           for(Product2 objProduct: lstProducts){
                
                ProductsWrapper objProductWrapper;
                if(MapProductIdsToLabel.containsKey(objProduct.Id)){                                       

                  objProductWrapper = new ProductsWrapper(
                                                    MapProductIdsToLabel.get(objProduct.Id),
                                                    objProduct.Id,
                                                    objProduct.Shared_Product_Image_URL__c,
                                                    objProduct.Shared_Product_Image_Align_Type__c); 
                } else{
                
                   objProductWrapper = new ProductsWrapper(objProduct.Product_Label__c,
                                                           objProduct.Id,
                                                           objProduct.Shared_Product_Image_URL__c,
                                                           objProduct.Shared_Product_Image_Align_Type__c);                
                }
               
               lstProductWrapper.add(objProductWrapper);
           }//BSMI-253
        }
    }
    
    private class ProductsWrapper{
        
        public String strProdName{get;set;}
        public String strProdImageURL{get;set;}
        public String strProdId{get;set;}
        public String strAlignType{get;set;}
        
        public ProductsWrapper(String name, String Id, String imageURL, String alignType){
            
            this.strProdName = name;
            this.strProdImageURL = imageURL;
            this.strProdId = Id;
            this.strAlignType = alignType;
        }
    }
}
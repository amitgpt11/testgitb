/*
* Handler class for the SharedRequestFormLineItemTrigger  
*
* @Author Accenture Services
* @Date 10OCT2016
*/

public class SharedRequestFormLineItemHandler  extends TriggerHandler {

  SharedRequestFormLineItemManager manager = new SharedRequestFormLineItemManager();

    public override void bulkBefore() {
    system.debug('inside bulkBefore');
       List<Shared_Request_Form_Line_Item__c> newRecords = (List<Shared_Request_Form_Line_Item__c>)trigger.new;
       List<Shared_Request_Form_Line_Item__c> oldRecords = (List<Shared_Request_Form_Line_Item__c>)trigger.old;
       
       if(Trigger.isDelete){
           if(oldRecords != null && oldRecords.size() > 0){          
               manager.fetchParentIdStatusMap(oldRecords);
           }
       }
       else{
           if(newRecords != null && newRecords.size() > 0){          
               manager.fetchParentIdStatusMap(newRecords);
           }
       }
    }
    
    public override void bulkAfter() { 
       system.debug('inside bulkAfter');
       List<Shared_Request_Form_Line_Item__c> newRecords = (List<Shared_Request_Form_Line_Item__c>)trigger.new;
       system.debug(newRecords+'$$');
       if(Trigger.isInsert){
           if(newRecords != null && newRecords.size() > 0){
               manager.updateParentTotalSamplePrice(newRecords);
           }
       }

    }   
    
    public override void beforeInsert(SObject obj) {
        
    }

    public override void beforeUpdate(SObject oldObj, SObject obj) {
        
    }

    public override void afterUpdate(SObject oldObj, SObject obj) {
        
    }
    
    public override void beforeDelete(SObject oldObj) {
        system.debug('inside delete');
        Shared_Request_Form_Line_Item__c oldRecord = (Shared_Request_Form_Line_Item__c)oldObj;
         manager.preventChildDeletion(oldRecord);
    }

    public override void andFinally() {
       
      manager.commitRecordDeletion();
      manager.commitParentRecords();       

    }

}
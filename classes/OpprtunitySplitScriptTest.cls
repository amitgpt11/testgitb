@isTest(SeeAllData=true)
private class OpprtunitySplitScriptTest {

    final static String ACCTNAME = 'Account Name';

    static testMethod void test_OpprtunitySplitScript() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        Account acct = td.createConsignmentAccount();
        acct.Name = ACCTNAME;
        insert acct;

        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.Opportunity_Type__c = 'Standard';
        oppty.StageName ='Candidate (Trial implant)';
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty);

        Opportunity oppty1 = td.newOpportunity(acct.Id);
        oppty1.Opportunity_Type__c = 'Competitive Swap';
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty1);

        Opportunity oppty2 = td.newOpportunity(acct.Id);
        oppty2.Opportunity_Type__c = 'Standard';
        oppty2.StageName ='Current Trial';
        oppty2.Trial_status__c='Failed';
        oppty2.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty2);

        Opportunity oppty3 = td.newOpportunity(acct.Id);
        oppty3.Opportunity_Type__c = 'Standard';
        oppty3.StageName ='Current Trial';
        oppty3.Trial_status__c='Inconclusive';
        oppty3.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty3);

        Opportunity oppty4 = td.newOpportunity(acct.Id);
        oppty4.Opportunity_Type__c = 'Standard';
        oppty4.StageName ='Current Trial';
        oppty4.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty4);

        Opportunity oppty5 = td.newOpportunity(acct.Id);
        oppty5.Opportunity_Type__c = 'Standard';
        oppty5.StageName ='Current Trial';
        oppty5.Trial_PO__c='Not null';
        oppty5.Trial_status__c='Successful';
        oppty5.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty5);


     
       Opportunity oppty6 = td.newOpportunity(acct.Id);
        oppty6.Opportunity_Type__c = 'Standard';
        oppty6.StageName ='Current Trial'; 
        oppty6.Trial_status__c='Successful';
        oppty6.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty6);


        Opportunity oppty7 = td.newOpportunity(acct.Id);
        oppty7.Opportunity_Type__c = 'Standard';
        oppty7.StageName ='Current Trial';
        oppty7.Trial_PO__c='Not null';
        oppty7.Trial_status__c='OMG';
        oppty7.Scheduled_Procedure_Date_Time__c=System.Now()+50;
        oppty7.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty7);

        Opportunity oppty8 = td.newOpportunity(acct.Id);
        oppty8.Opportunity_Type__c = 'Standard';
        oppty8.StageName ='Current Trial'; 
        oppty8.Trial_status__c='OMG';
        oppty8.Scheduled_Procedure_Date_Time__c=System.Now()+50;
        oppty8.PO__c='Not Null';
        oppty8.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(oppty8);

        Opportunity oppty9 = td.newOpportunity(acct.Id);
        oppty9.Opportunity_Type__c = 'Standard';
        oppty9.StageName ='Scheduled Implant'; 
        oppty9.Trial_status__c='OMG';
        oppty9.Scheduled_Procedure_Date_Time__c=System.Now()+50;
        oppty9.Trial_PO__c='Not Null';
        oppty9.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(oppty9);

        
        Opportunity oppty10 = td.newOpportunity(acct.Id);
        oppty10.Opportunity_Type__c = 'Standard';
        oppty10.StageName ='Scheduled Implant'; 
        oppty10.Trial_status__c='OMG';
        oppty10.Scheduled_Procedure_Date_Time__c=System.Now()+50;
        oppty10.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(oppty10);
        
        Opportunity oppty11 = td.newOpportunity(acct.Id);
        oppty11.Opportunity_Type__c = 'Standard';
        oppty11.StageName ='Scheduled Implant'; 
        oppty11.Trial_status__c='OMG';
        oppty11.PO__c='Not null';
        oppty11.Scheduled_Procedure_Date_Time__c=System.Now()+50;
        oppty11.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(oppty11);

        
        Opportunity oppty12 = td.newOpportunity(acct.Id);
        oppty12.Opportunity_Type__c = 'Standard';
        oppty12.CreatedDate=Date.parse('01/01/2016');
        oppty12.StageName='Cancelled';
        oppty12.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(oppty12);
    
        insert oppList;

        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=oppList[1].id;
        insert attach;



        Order order = new Order(
        AccountId = acct.Id,
        OpportunityId = oppList[0].Id,
        EffectiveDate = System.today(),
        Status = 'Draft',
        Order_Method__c = 'Approval'
        );
        insert order; 


    /*      
        
   //---------DetermineStage---------when creation date is more//     
        Opportunity dtstg = td.newOpportunity(acct.Id);
        dtstg.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg.createdDate= date.newInstance(2016, 8, 12);
        //date.parse('2016-08-12');
        oppList.add(dtstg);

        Opportunity dtstg1 = td.newOpportunity(acct.Id);
        dtstg1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg1.createdDate= date.newInstance(2016, 8, 12);
        dtstg1.Scheduled_Trial_Date__c=System.today();
        //date.parse('2016-08-12');
        oppList.add(dtstg1);

        Opportunity dtstg2 = td.newOpportunity(acct.Id);
        dtstg2.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg2.createdDate= date.newInstance(2016, 8, 12);
        dtstg2.Scheduled_Trial_Date__c=System.today();
        dtstg2.Trial_Order_Number__c='1234';
        //date.parse('2016-08-12');
        oppList.add(dtstg2);

        
        
        Opportunity dtstg3 = td.newOpportunity(acct.Id);
        dtstg3.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg3.createdDate= date.newInstance(2016, 8, 12);
        dtstg3.Scheduled_Trial_Date__c=System.today();
        dtstg3.Trial_Order_Number__c='1234';
        dtstg3.Trial_Status__c = 'Failed';
        //date.parse('2016-08-12');
        oppList.add(dtstg3);

        
        Opportunity dtstg4 = td.newOpportunity(acct.Id);
        dtstg4.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg4.createdDate= date.newInstance(2016, 8, 12);
        dtstg4.Scheduled_Trial_Date__c=System.today();
        dtstg4.Trial_Order_Number__c='1234';
        dtstg4.Trial_Status__c = 'Failed';
        dtstg4.Trial_PO__c='Notnull';
        //date.parse('2016-08-12');
        oppList.add(dtstg4);

         
        Opportunity dtstg5 = td.newOpportunity(acct.Id);
        dtstg5.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg5.createdDate= date.newInstance(2016, 8, 12);
        dtstg5.Scheduled_Trial_Date__c=System.today();
        dtstg5.Trial_Order_Number__c='1234';
        dtstg5.Trial_Status__c = 'Successful';
        dtstg5.Trial_PO__c='Notnull';
        //date.parse('2016-08-12');
        oppList.add(dtstg4);
        
        
        
        //Dtstg-determine stage for implant oppties
        
        Opportunity dtstgimplant7 = td.newOpportunity(acct.Id);
        
        dtstgimplant7.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        dtstgimplant7.createdDate= date.newInstance(2016, 8, 12);
        
        oppList.add(dtstgimplant7);

        
        
        Opportunity dtstgimplant8 = td.newOpportunity(acct.Id);
        dtstgimplant8.Opportunity_Type__c = 'Standard';
        dtstgimplant8.createdDate= date.newInstance(2016, 8, 12);
        
      dtstgimplant8.Scheduled_Procedure_Date__c=System.Today()+50;
        oppList.add(dtstgimplant8);

        Opportunity dtstgimplant9 = td.newOpportunity(acct.Id);
        dtstgimplant9.Opportunity_Type__c = 'Standard';
        dtstgimplant9.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant9.StageName ='Scheduled Implant'; 
        dtstgimplant9.Procedure_Order_Number__c='111';        
        dtstgimplant9.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstgimplant9.Trial_PO__c='Not Null';
        dtstgimplant9.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant9);

        
        Opportunity dtstgimplant10 = td.newOpportunity(acct.Id);
        dtstgimplant10.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant10.Opportunity_Type__c = 'Standard';
        dtstgimplant10.StageName ='Scheduled Implant'; 
        dtstgimplant10.Trial_status__c='OMG';
        
        dtstgimplant10.Procedure_Order_Number__c='111';
        dtstgimplant10.PO__C='Not null';
        dtstgimplant10.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstgimplant10.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant10);
        
        Opportunity dtstgimplant11 = td.newOpportunity(acct.Id);
        dtstgimplant11.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant11.Opportunity_Type__c = 'Standard';
        dtstgimplant11.StageName ='Scheduled Implant'; 
        dtstgimplant11.Trial_status__c='OMG';
        dtstgimplant11.PO__c='Not null';
        dtstgimplant11.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstgimplant11.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant11);

        
        Opportunity dtstgimplant12 = td.newOpportunity(acct.Id);
        dtstgimplant12.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant12.Opportunity_Type__c = 'Standard';
        dtstgimplant12.CreatedDate=Date.parse('01/01/2016');
        dtstgimplant12.StageName='Cancelled';
        dtstgimplant12.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant12);
        
        
        
    
            //---------DetermineStage---------when creation date is less//     
       Opportunity dtstg_postdate = td.newOpportunity(acct.Id);
        dtstg_postdate.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg_postdate.createdDate= date.newInstance(2016, 1, 1);
        //date.parse('2016-08-12');
        oppList.add(dtstg_postdate);

        Opportunity dtstg_postdate1 = td.newOpportunity(acct.Id);
        dtstg_postdate1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg_postdate1.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdate1.Scheduled_Trial_Date__c=System.today();
        //date.parse('2016-08-12');
        oppList.add(dtstg_postdate1);

        Opportunity dtstg_postdate2 = td.newOpportunity(acct.Id);
        dtstg_postdate2.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg_postdate2.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdate2.Scheduled_Trial_Date__c=System.today();
        dtstg_postdate2.Trial_Order_Number__c='1234';
        //date.parse('2016-08-12');
        oppList.add(dtstg_postdate2);

        
        Opportunity dtstg_postdate3 = td.newOpportunity(acct.Id);
        dtstg_postdate3.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg_postdate3.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdate3.Scheduled_Trial_Date__c=System.today();
        dtstg_postdate3.Trial_Order_Number__c='1234';
        dtstg_postdate3.Trial_Status__c = 'Failed';
        //date.parse('2016-08-12');
        oppList.add(dtstg_postdate3);

        
           Opportunity dtstg_postdate4 = td.newOpportunity(acct.Id);
        dtstg_postdate4.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg_postdate4.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdate4.Scheduled_Trial_Date__c=System.today();
        dtstg_postdate4.Trial_Order_Number__c='1234';
        dtstg_postdate4.Trial_Status__c = 'Failed';
        dtstg_postdate4.Trial_PO__c='Notnull';
        //date.parse('2016-08-12');
        oppList.add(dtstg_postdate4);

         
           Opportunity dtstg_postdate5 = td.newOpportunity(acct.Id);
        dtstg_postdate5.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg_postdate5.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdate5.Scheduled_Trial_Date__c=System.today();
        dtstg_postdate5.Trial_Order_Number__c='1234';
        dtstg_postdate5.Trial_Status__c = 'Successful';
        dtstg_postdate5.Trial_PO__c='Notnull';
        //date.parse('2016-08-12');
        oppList.add(dtstg_postdate4);
        

        
        //dtstg_postdate-determine stage for implant oppties
        
        Opportunity dtstg_postdateimplant7 = td.newOpportunity(acct.Id);
        
        dtstg_postdateimplant7.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        dtstg_postdateimplant7.createdDate= date.newInstance(2016, 1, 1);
        
        oppList.add(dtstg_postdateimplant7);

        
        
        Opportunity dtstg_postdateimplant8 = td.newOpportunity(acct.Id);
        dtstg_postdateimplant8.Opportunity_Type__c = 'Standard';
        dtstg_postdateimplant8.createdDate= date.newInstance(2016, 1, 1);
        
      dtstg_postdateimplant8.Scheduled_Procedure_Date__c=System.Today()+50;
        oppList.add(dtstg_postdateimplant8);

        Opportunity dtstg_postdateimplant9 = td.newOpportunity(acct.Id);
        dtstg_postdateimplant9.Opportunity_Type__c = 'Standard';
        dtstg_postdateimplant9.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdateimplant9.StageName ='Scheduled Implant'; 
        dtstg_postdateimplant9.Procedure_Order_Number__c='111';        
        dtstg_postdateimplant9.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstg_postdateimplant9.Trial_PO__c='Not Null';
        dtstg_postdateimplant9.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstg_postdateimplant9);

        
        Opportunity dtstg_postdateimplant10 = td.newOpportunity(acct.Id);
        dtstg_postdateimplant10.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdateimplant10.Opportunity_Type__c = 'Standard';
        dtstg_postdateimplant10.StageName ='Scheduled Implant'; 
        dtstg_postdateimplant10.Trial_status__c='OMG';
        
        dtstg_postdateimplant10.Procedure_Order_Number__c='111';
        dtstg_postdateimplant10.PO__C='Not null';
        dtstg_postdateimplant10.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstg_postdateimplant10.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstg_postdateimplant10);
        
        Opportunity dtstg_postdateimplant11 = td.newOpportunity(acct.Id);
        dtstg_postdateimplant11.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdateimplant11.Opportunity_Type__c = 'Standard';
        dtstg_postdateimplant11.StageName ='Scheduled Implant'; 
        dtstg_postdateimplant11.Trial_status__c='OMG';
        dtstg_postdateimplant11.PO__c='Not null';
        dtstg_postdateimplant11.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstg_postdateimplant11.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstg_postdateimplant11);

        
        Opportunity dtstg_postdateimplant12 = td.newOpportunity(acct.Id);
        dtstg_postdateimplant12.createdDate= date.newInstance(2016, 1, 1);
        dtstg_postdateimplant12.Opportunity_Type__c = 'Standard';
        dtstg_postdateimplant12.CreatedDate=Date.parse('01/01/2016');
        dtstg_postdateimplant12.StageName='Cancelled';
        dtstg_postdateimplant12.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstg_postdateimplant12);
         
        system.debug(' Number of Optys : '+oppList.size());

        
        
        
   //---------DetermineStage End---------// 
        Test.startTest();

        */
        if(getOpportunityTriggerStatus().EqualsIgnoreCase('active')){  
        OpprtunitySplitScript obj =new OpprtunitySplitScript(oppList); }
        /* 
        obj.determineStage(dtstg, 'Trial');
        obj.determineStage(dtstg1, 'Trial');
        obj.determineStage(dtstg2, 'Trial');
        obj.determineStage(dtstg3, 'Trial');
        obj.determineStage(dtstg4, 'Trial');
        obj.determineStage(dtstg5, 'Trial');
        obj.determineStage(dtstgimplant7, 'Implant');
        obj.determineStage(dtstgimplant8, 'Implant');
        obj.determineStage(dtstgimplant9, 'Implant');
        obj.determineStage(dtstgimplant10, 'Implant');
        obj.determineStage(dtstgimplant11, 'Implant');
        obj.determineStage(dtstgimplant12, 'Implant');
    /*    //when date is more
        obj.determineStage(dtstg_postdate, 'Trial');
        obj.determineStage(dtstg_postdate1, 'Trial');
        obj.determineStage(dtstg_postdate2, 'Trial');
        obj.determineStage(dtstg_postdate3, 'Trial');
        obj.determineStage(dtstg_postdate4, 'Trial');
        obj.determineStage(dtstg_postdate5, 'Trial');
        obj.determineStage(dtstg_postdateimplant7, 'Implant');
        obj.determineStage(dtstg_postdateimplant8, 'Implant');
        obj.determineStage(dtstg_postdateimplant9, 'Implant');
        obj.determineStage(dtstg_postdateimplant10, 'Implant');
        obj.determineStage(dtstg_postdateimplant11, 'Implant');
        obj.determineStage(dtstg_postdateimplant12, 'Implant');
        }
        
        Test.stopTest();*/
    }

    static testMethod void test_determinestage() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        Account acct = td.createConsignmentAccount();
        acct.Name = ACCTNAME;
        insert acct;
        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.Opportunity_Type__c = 'Standard';
        oppty.StageName ='Candidate (Trial implant)';
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        oppList.add(oppty);
        //---------DetermineStage---------when creation date is more//     
         Opportunity dtstg = td.newOpportunity(acct.Id);
        dtstg.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg.createdDate= date.newInstance(2016, 8, 12);
        //date.parse('2016-08-12');
        oppList.add(dtstg);

        Opportunity dtstg1 = td.newOpportunity(acct.Id);
        dtstg1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg1.createdDate= date.newInstance(2016, 8, 12);
        dtstg1.Scheduled_Trial_Date__c=System.today();
        //date.parse('2016-08-12');
        oppList.add(dtstg1);

        Opportunity dtstg2 = td.newOpportunity(acct.Id);
        dtstg2.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg2.createdDate= date.newInstance(2016, 8, 12);
        dtstg2.Scheduled_Trial_Date__c=System.today();
        dtstg2.Trial_Order_Number__c='1234';
        //date.parse('2016-08-12');
        oppList.add(dtstg2);

        
        
        Opportunity dtstg3 = td.newOpportunity(acct.Id);
        dtstg3.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg3.createdDate= date.newInstance(2016, 8, 12);
        dtstg3.Scheduled_Trial_Date__c=System.today();
        dtstg3.Trial_Order_Number__c='1234';
        dtstg3.Trial_Status__c = 'Failed';
        //date.parse('2016-08-12');
        oppList.add(dtstg3);

        
        Opportunity dtstg4 = td.newOpportunity(acct.Id);
        dtstg4.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg4.createdDate= date.newInstance(2016, 8, 12);
        dtstg4.Scheduled_Trial_Date__c=System.today();
        dtstg4.Trial_Order_Number__c='1234';
        dtstg4.Trial_Status__c = 'Failed';
        dtstg4.Trial_PO__c='Notnull';
        //date.parse('2016-08-12');
        oppList.add(dtstg4);

         
        Opportunity dtstg5 = td.newOpportunity(acct.Id);
        dtstg5.RecordTypeId = OpportunityManager.RECTYPE_TRIAL ;
        dtstg5.createdDate= date.newInstance(2016, 8, 12);
        dtstg5.Scheduled_Trial_Date__c=System.today();
        dtstg5.Trial_Order_Number__c='1234';
        dtstg5.Trial_Status__c = 'Successful';
        dtstg5.Trial_PO__c='Notnull';
        //date.parse('2016-08-12');
        oppList.add(dtstg5);
        
         //Dtstg-determine stage for implant oppties
        
        Opportunity dtstgimplant7 = td.newOpportunity(acct.Id);
        
        dtstgimplant7.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        dtstgimplant7.createdDate= date.newInstance(2016, 8, 12);
        
        oppList.add(dtstgimplant7);

        
        
        Opportunity dtstgimplant8 = td.newOpportunity(acct.Id);
        dtstgimplant8.Opportunity_Type__c = 'Standard';
        dtstgimplant8.createdDate= date.newInstance(2016, 8, 12);
        
      dtstgimplant8.Scheduled_Procedure_Date__c=System.Today()+50;
        oppList.add(dtstgimplant8);

        Opportunity dtstgimplant9 = td.newOpportunity(acct.Id);
        dtstgimplant9.Opportunity_Type__c = 'Standard';
        dtstgimplant9.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant9.StageName ='Scheduled Implant'; 
        dtstgimplant9.Procedure_Order_Number__c='111';        
        dtstgimplant9.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstgimplant9.Trial_PO__c='Not Null';
        dtstgimplant9.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant9);

        
        Opportunity dtstgimplant10 = td.newOpportunity(acct.Id);
        dtstgimplant10.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant10.Opportunity_Type__c = 'Standard';
        dtstgimplant10.StageName ='Scheduled Implant'; 
        dtstgimplant10.Trial_status__c='OMG';
        
        dtstgimplant10.Procedure_Order_Number__c='111';
        dtstgimplant10.PO__C='Not null';
        dtstgimplant10.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstgimplant10.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant10);
        
        Opportunity dtstgimplant11 = td.newOpportunity(acct.Id);
        dtstgimplant11.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant11.Opportunity_Type__c = 'Standard';
        dtstgimplant11.StageName ='Scheduled Implant'; 
        dtstgimplant11.Trial_status__c='OMG';
        dtstgimplant11.PO__c='Not null';
        dtstgimplant11.Scheduled_Procedure_Date__c=System.Today()+50;
        dtstgimplant11.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant11);
        
        
        insert oppList;

        
        /*Opportunity dtstgimplant12 = td.newOpportunity(acct.Id);
        dtstgimplant12.createdDate= date.newInstance(2016, 8, 12);
        dtstgimplant12.Opportunity_Type__c = 'Standard';
        dtstgimplant12.CreatedDate=Date.parse('01/01/2016');
        dtstgimplant12.StageName='Cancelled';
        dtstgimplant12.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT ;
        oppList.add(dtstgimplant12);*/
        

        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=oppList[0].id;
        insert attach;



        Order order = new Order(
        AccountId = acct.Id,
        OpportunityId = oppList[0].Id,
        EffectiveDate = System.today(),
        Status = 'Draft',
        Order_Method__c = 'Approval'
        );
        insert order; 


        Test.startTest();
        OpprtunitySplitScript obj =new OpprtunitySplitScript(oppList);    
        
       // obj.determineStage(oppty, OpportunityManager.RECTYPE_TRIAL);
        obj.determineStage(dtstg, 'Trial');
        obj.determineStage(dtstg1, 'Trial');
        obj.determineStage(dtstg2, 'Trial');
        obj.determineStage(dtstg3, 'Trial');
        obj.determineStage(dtstg4, 'Trial');
        obj.determineStage(dtstg5, 'Trial');
        obj.determineStage(dtstgimplant7, 'Implant');
        obj.determineStage(dtstgimplant8, 'Implant');
        obj.determineStage(dtstgimplant9, 'Implant');
        obj.determineStage(dtstgimplant10, 'Implant');
        obj.determineStage(dtstgimplant11, 'Implant');
        
        Test.stopTest();
    
        }


    public static String getOpportunityTriggerStatus()
    {
      ApexTrigger apx = [Select id,Status,Name From ApexTrigger where Name = 'OpportunityTrigger'];
      return apx.status;
    }

}
@isTest
public class McEmailTriggerTest {
  /*   static final String NAME = 'NAME';
    @testSetup
    static void setup() {
        NMD_TestDataManager td = new NMD_TestDataManager();

        Account acct = td.newAccount(NAME);
        insert acct;

        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
        insert oppty; 
        
        Patient__c pat = td.newPatient(NAME,NAME);
        insert pat;
 
 } */
 
    static testMethod void test_Static() {

      //  Opportunity oppty = [select Id from Opportunity where Account.Name = :NAME];
     //   Patient__c pat = [select Id from Patient__c where Patient_First_Name__c = :NAME and Patient_Last_Name__c = :NAME];
        
        Account acc = new Account(Name='test Account');
        insert acc;
        
        Opportunity opp = new Opportunity(AccountId = acc.id,
                                          RecordTypeId = OpportunityManager.RECTYPE_TRIAL, 
                                          CloseDate = System.today().addDays(7),
                                          Name = 'opp name',
                                          StageName = 'New',
                                          Type = 'Standard',
                                           Opportunity_Type__c = 'Standard',
                                           Competitor_Name__c = 'Other',
                                          Product_System__c = 'Spectra'
                                          );
        insert opp;
        
        Patient__c pat = new Patient__c(Patient_First_Name__c = 'fname',
            Patient_Last_Name__c = 'lname');
            
         insert pat;   
        
        MC_Email__c mcemail = new MC_Email__c();
        mcemail.Name = 'Name';
         //mcemail.PatientID__c = pat.Id;
        insert mcemail;
         mcemail.Related_Opportunity__c = opp.Id;
        update mcemail;

    } 
}
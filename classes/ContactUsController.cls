global without sharing class ContactUsController  extends AuthorizationUtil {
    
    public User objUser {get;set;}
    public String taskSubject {get;set;}
    public String messageDescription {get;set;}

    /**
      Description: This method initilizes the VTM's Image and Representative Intro
    */
    public override void fetchRequestedData() {
        
        // Get the VTM's Image 
        objUser = [Select Name, ContactId, Contact.Owner.SmallPhotoUrl, Contact.Owner.FullPhotoUrl,Contact.Owner.Representative_Intro__c, Contact.Owner.Phone
            From User 
            Where Id =: UserInfo.getUserId()];
    }
    
    /**
      Description: This method will call for authenticated user's Contact Form send button to create Task for VTM.
    */
    Public void createTaskForVTM() {
        
        system.debug('## taskSubject '+taskSubject);
        system.debug('## messageDescription '+messageDescription);
        
        if(!String.isBlank(taskSubject) && !String.isBlank(messageDescription)) {
        
            Task objTask = new Task(Subject = taskSubject, Message__c = messageDescription, Status = 'Not Started', ActivityDate = Date.today(), OwnerId = objUser.Contact.OwnerId, WhoId = objUser.ContactId);
            objTask.RecordTypeId = [SELECT Id,SobjectType,Name 
                FROM RecordType 
                WHERE DeveloperName ='Shared_Community_Task' 
                AND SobjectType ='Task' 
                LIMIT 1].Id;
                
            Insert objTask;
            
            taskSubject = '';
            messageDescription = '';
        } else {
        
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.Boston_ContactUs_Error));
        }
    }
}
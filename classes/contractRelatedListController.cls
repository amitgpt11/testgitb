/*
 @CreatedDate     24 June 2016                                  
 @author          Ashish-Accenture (Updated by Susannah St-Germain on 10/13/2016 to include Contract Type as filter)
 @Description     This class is used display active contracts as related list in Page layout
       
*/

public with sharing class contractRelatedListController{
    Public list<Contract> lstContractRec {get;set;}
    public list<String> lstOfFields {get;set;}
    public Map<String,string> mapLabelAPI{get;set;}
    public List<Contract> EP_ContractList{get;set;}
    public id accId;
    // To fetch Active and visible contracts for Account
    public contractRelatedListController(ApexPages.StandardController controller) {
        try{
            accId = controller.getId();
           
            ContractConfiguration__c CS;
            if(ContractConfiguration__c.getValues(UserInfo.getUserId()) != null){
                CS = ContractConfiguration__c.getValues(UserInfo.getUserId());
            }else if(ContractConfiguration__c.getValues(UserInfo.getProfileId()) != null){
                CS = ContractConfiguration__c.getValues(UserInfo.getProfileId());
            }else{
                CS = ContractConfiguration__c.getOrgDefaults();
            }
            mapLabelAPI = new Map<String,string>();
            
            
        Schema.Sobjecttype objType = Contract.getSObjectType();
        Schema.Describesobjectresult sobjRes = objType.getDescribe();
        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
        List<String> sortSelectOption = new List<String>(fieldMap.keyset());
        sortSelectOption.sort();
        String fieldType;
        String fieldName;
        String fieldLabel;
        for(String a : sortSelectOption) {
            Schema.Sobjectfield field = fieldMap.get(a);
            fieldName = field.getDescribe().getName().toLowerCase();
            fieldType = field.getDescribe().getType().name().toLowerCase();
            fieldLabel = field.getDescribe().getlabel();
            
            mapLabelAPI.put(fieldName,fieldLabel);
        } 
         System.debug('mapLabelAPI:'+mapLabelAPI); 
            lstOfFields = new list<String>();
            if(CS != null && String.isNotBlank(cs.Visible_Fields__c)){
                
                for(String field : cs.Visible_Fields__c.Split(',')){
                    if(String.isNotBlank(field)){
                        lstOfFields.add(field.tolowercase().trim());
                    }
                }
                 
            }
            
            //Set Divisions
            Set<String> setDivisions = new Set<String>();
            String divisionString= '';
            if(CS != null && String.isNotBlank(cs.Contract_Divisions__c)){
                for(String div : cs.Contract_Divisions__c.split(',')){
                    if(String.isNotBlank(div)){
                        setDivisions.add(div.trim());
                        divisionString = divisionString + '\''+div.trim() + '\',';
                    }
                }
            }
            divisionString = divisionString.removeEnd(',');
            
            if(lstOfFields.size() == 0){
                lstOfFields = new list<String>{'Name'};
            }
            System.debug('===divisionString==='+divisionString);
            
            //Set Contract Type added by Susannah St-Germain during Q4-2016
           Set<String> setType  = new Set<String>();
           if(CS != null && String.isNotBlank(cs.Contract_Type__c)){
                
                for(String type: cs.Contract_Type__c.Split(',')){
                    if(String.isNotBlank(type)){
                        setType.add(type.tolowercase().trim());
                        system.debug('These are the contract types to be included in the related list : '+setType);
                    }
                }
                 
            }
            
           //Set<String> setStatus  = new Set<String>{'Active','Activated'};
           Set<String> setStatus  = new Set<String>();
           if(CS != null && String.isNotBlank(cs.Contract_Status__c)){
                
                for(String status: cs.Contract_Status__c.Split(',')){
                    if(String.isNotBlank(status)){
                        setStatus.add(status.tolowercase().trim());
                    }
                }
                 
            }
            
            //Query to determine which records to display. Updated to include Contract Type on 10/13/16 by Susannah St-Germain
            String query = 'Select ' + String.join(lstOfFields, ',')+ ' from Contract where AccountId =: accId AND status in : setStatus AND Contract_Type__c in : setType AND Shared_Divisions__c includes ('+divisionString+')';
           
            lstContractRec = database.query(query);
            
        }Catch(exception e){
            System.debug('###==e===='+e);
        }
      
    }
   
}
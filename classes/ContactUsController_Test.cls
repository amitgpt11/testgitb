@isTest
private class ContactUsController_Test {

    private static testMethod void test() {
    
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        
        Test.StartTest();
        
        system.runAs(objUser){
            
            PageReference pageRef = Page.ContactUs;
            Test.setCurrentPage(pageRef);
            
            ContactUsController objContactUsController = new ContactUsController();
            
            objContactUsController.init();
	        objContactUsController.createTaskForVTM();
	        objContactUsController.createTaskForVTM();
	        
	        system.assertEquals(null, objContactUsController.init());
        }
        Test.StopTest();
	    
    }
}
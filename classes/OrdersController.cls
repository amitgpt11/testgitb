/*
* Description: This controller initilizes the List of Shared Order Item to display on Orders Page.
*/
public class OrdersController extends AuthorizationUtil{
    
    public Integer index {get;set;}
    public User objUser {get;set;}
    public list<Shared_Community_Order_Item__c> lstOrderItem {get;set;}
    public List<Shared_Community_Order_Item__c> lstOrderItemToDelete{get;set;}
    public Boolean hasPurchasingPermission  {get; set;}
    
    public List<ProductWrapper> productAttributeList {get;set;}
    
    /*
        * Description: This method initilizes the list to be displayed and called after initial page load.
    */
    public override void fetchRequestedData() {
        
        hasPurchasingPermission = false;
        lstOrderItem = new List<Shared_Community_Order_Item__c>();
        lstOrderItemToDelete = new List<Shared_Community_Order_Item__c>();
        productAttributeList = new List<ProductWrapper>();
        
        Map<string,Shared_Community_Order_Item__c> orderItemMap = new Map<string,Shared_Community_Order_Item__c>(); 
        Map<string,object> productMap = new Map<string,Object>();
        set<Id> productIds = new set<Id>();
        
        objUser = [Select contactId, Shared_Community_Division__c
                   From User
                   Where Id =: Userinfo.getUserId()];
        
        List<Contact> lstContacts = new List<Contact>([SELECT Purchasing_Permission__c FROM Contact WHERE Id =: objUser.contactId]);
        
        if(!lstContacts.isEmpty()) {
            
            hasPurchasingPermission = lstContacts[0].Purchasing_Permission__c;
        }
        
        String queryOrderItems = 'Select Id, Shared_Order__c,Product__c,Shared_Quantity__c, Shared_Order__r.Shared_Date_Submitted__c, ' +
            					'Product__r.Shared_Product_Image_URL__c, Product__r.Product_Label__c, Product__r.Id, ' +
            					'Product__r.Shared_Community_Description__c, Product__r.ProductCode, ' +
                        		'Product__r.Shared_Parent_Product__r.Shared_Product_Image_URL__c ' +
                        		'From Shared_Community_Order_Item__c ' + 
                        		'Where Shared_Order__r.Status__c = \'Open\' ';
        
        if(!Test.isRunningTest()) {
            
            queryOrderItems += 'AND Shared_Order__r.Contact__r.Id = \'' + objUser.contactId + '\'';
        }
        
        for(Shared_Community_Order_Item__c orderItem : Database.query(queryOrderItems)){
            orderItemMap.put(orderItem.Id,orderItem);
            productIds.add(orderItem.Product__c);
        }
        
        //Create dynamic query to fetch products as per fields in field set named Product_Attribute_Fields
        String Query = 'Select ';
        
        for(Schema.FieldSetMember f : SObjectType.Product2.FieldSets.Product_Attribute_Fields.getFields()) {
            
            Query += f.getFieldPath() + ', ';
        }
        
        Query += 'Id From product2 where Id IN : productIds';
        
        //Iterate over products list of order line item
        for(product2 product : Database.query(Query)){
            
            productMap = new Map<string,Object>();
            
            //Iterate over field set member of field set
            for(Schema.FieldSetMember f : SObjectType.Product2.FieldSets.Product_Attribute_Fields.getFields()) {
                
                //if value of a particular field is not null in product record then add to product map
                if(product.get(f.fieldPath) != null){
                    
                    productMap.put(f.fieldPath,product.get(f.fieldPath));
                }
            }
            
            //fill map of order item id and productMap to display in template
            ProductWrapper productWrapperObj = new ProductWrapper(product.Id,productMap);
            productAttributeList.add(productWrapperObj);
        }
        
        lstOrderItem = Database.query(queryOrderItems);
        
    }
    
    public class ProductWrapper{
        public string productId{get;set;}
        public Map<string,object> productMap{get;set;}
        public ProductWrapper(string productId,Map<string,object> productMap){
            this.productId = productId;
            this.productMap = productMap;
        }
    }
    
    public void updateOrderItem(){
        
        Update lstOrderItem;
        Delete lstOrderItemToDelete;
        fetchRequestedData();
    }
    
    public void deleteOrderItem() {
        
        lstOrderItemToDelete.add(lstOrderItem[index]);
        lstOrderItem.remove(index);
    }
    
    public PageReference goToCheckout() {
        
        String strCheckoutURL = ((String.isBlank(Site.getName())) ? Site.getPathPrefix() : '/apex') + '/Checkout?orderId=' + lstOrderItem[0].Shared_Order__c;
        return new PageReference(strCheckoutURL);
    }
    
}
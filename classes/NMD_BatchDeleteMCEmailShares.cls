/**
NMD_BatchDeleteMCEmailShares : this class is created to delete MC Email Sharing since the count of the records is more than 10000, and if not done in a batch, we will get a too many DML 10001 error. 
Author: Dipil Jain
*/

global class NMD_BatchDeleteMCEmailShares implements Database.Batchable<SObject>,Schedulable {
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([select id from MC_Email__Share where RowCause = :Schema.MC_Email__Share.RowCause.Patient_Sharing__c]);
    }
    public void execute(Database.BatchableContext context, MC_EMail__Share[] scope) {
        if(scope.size()>0) {
            delete scope;
        }
    }
    public void finish(Database.BatchableContext context) {
	System.debug('Delete of MC_EMAIL__SHARE records is completed.');
        NMD_BatchMCEmailSharingRecalculation b = new NMD_BatchMCEmailSharingRecalculation();
		database.executebatch(b, 20);//Batch size 200
    }
	
	global void execute(SchedulableContext sc) {
      NMD_BatchDeleteMCEmailShares b1 = new NMD_BatchDeleteMCEmailShares(); 
      database.executebatch(b1, 200);//Batch size 100.
   }
	
}
global class NMD_BatchPainMapReparent implements  Database.Batchable<sObject>, Schedulable{

    Map<Id, List<Clinical_Data_Summary__c>> OptyToCDSMap = new Map<Id, List<Clinical_Data_Summary__c>> ();
    Map<Id, List<Opportunity>> PatientToOptyMap = new Map<Id, List<Opportunity>> ();
                List<Patient__c> ptsToUpdate = new List<Patient__c>();
    Set<Id> PatientIds = new Set<Id>();
    Set<Id> AllOpptyIds = new Set<Id>();
    Set<Id> OpptyWithCDSIds = new Set<Id>();
                Integer queryLimit=0;
    List<Opportunity> OpptyWithCDS= new List<Opportunity>();
    List<Clinical_Data_Summary__c> ActivePainMaps = new List<Clinical_Data_Summary__c>();

    public String query = 'select id,name,RecordTypeId,PainMapReparented__c from patient__c where recordtype.name in (\'Patient Prospect\',\'Patient Customer\') and  PainMapReparented__c= false ' ;
    
    // Constructor
    global NMD_BatchPainMapReparent ()  
    {} 
                global NMD_BatchPainMapReparent (Integer queryLim)  
    {
                                this.queryLimit=queryLim;
                } 
    /****************************************************************
    *  start(Database.BatchableContext BC)
    *****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('Inside Start method');
        if(Test.isRunningTest()){
        this.query= this.query+' Limit 10';
        }
        if(queryLimit!=0){
            this.query=this.query+' Limit '+queryLimit;
        }
                                
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
    *  execute(Database.BatchableContext BC, List scope)
    **********************************************************************/
    global void execute(Database.BatchableContext BC, List<patient__c> scope)
    {
        System.debug('#########Scope####### :'+scope);
    
        for(patient__c p: scope){
            PatientIds.add(p.id);
                                                
        }        
        
        List<Clinical_Data_Summary__c> cds = [select id, Opportunity__c, RecordTypeId, RecordType.name, Opportunity__r.patient__C from Clinical_Data_Summary__c where RecordType.name in ('Pain Map', 'Annotations') and Opportunity__c !=null and Opportunity__r.patient__c!=null and Opportunity__r.patient__c in :PatientIds];
        
        system.debug('###### cds size : '+cds.size());
        
        //there are some opportunities which may not have any CDS associated with them.. so create the list of only those Optys which has CDS. 
        For(Clinical_Data_Summary__c c: cds){
                OpptyWithCDSIds.add(c.Opportunity__c); // this will give only those optys whic have CDS associated with them.
        }
        
        system.debug('#### OpptyWithCDSIds :'+OpptyWithCDSIds);
                
        List<Opportunity> optys = [select id, createdDate, patient__c,patient__r.name, recordtype.name from Opportunity where id in :OpptyWithCDSIds ]; 
                
        for(patient__c p : scope){

            List<Opportunity> opps = new List<Opportunity>();
            for(Opportunity o1: optys){
                if(p.id == o1.patient__c){
                    opps.add(o1);
                }
            }
            if(!opps.isEmpty())
                PatientToOptyMap.put(p.id, opps);
        }
        
        Set<Id> finalOptyIds = new Set<Id>();
        for(patient__c p : scope){
                p.PainMapReparented__c = true;
                ptsToUpdate.add(p);
            List<Opportunity> opps = PatientToOptyMap.get(p.id);
            if(opps!=null && !opps.isEmpty()){
                System.debug('######Number of Child optys is '+p.Opportunities__r +'on patient :'+p.Name );
                Id tempId = getFinalOpty(opps).recordId;
                System.debug('****************** FINAL OpportunityID :'+tempId);
                if(tempId!=null)
                    finalOptyIds.add(tempId);
            }else{
                system.debug('***************Patient '+p.id+' Name '+p.name+' does not have any opportunities.*******');
            }
        }
        system.debug('###### finalOptyIds size : '+finalOptyIds.size());
        List<Clinical_Data_Summary__c> cds1 = new List<Clinical_Data_Summary__c> ();
        List<PainMapReparentTemp__c> painmapTemps = new List<PainMapReparentTemp__c>();
        Set<Id>patientsWithCDs = new Set<Id>();
        if(!finalOptyIds.isEmpty()){
        //code to move all clinical data summaries from Opportunities to patients
            for(Clinical_Data_Summary__c c: cds){
                Id oldOpp = c.Opportunity__c;
                if(finalOptyIds.contains(c.Opportunity__c) && c.recordType.name=='Pain Map'){
                    c.isActive__c=true;
                }else{
                    c.isActive__c=false;
                }
                
                c.Patient__c = c.Opportunity__r.patient__C;
                c.Opportunity__c = null;
                cds1.add(c);
                
                //PainMapReparent Audit Record
                    PainMapReparentTemp__c temp = new PainMapReparentTemp__c();
                    
                    temp.OldOpportunity__c=oldOpp;
                    temp.Clinical_Data_Summary__c=c.id;
                    temp.patient__c = c.patient__c;
                    
                    temp.isActive__c= c.isActive__c;
                    painmapTemps.add(temp);
                    patientsWithCDs.add(c.patient__c);
            }
        }
        system.debug('###### cds1 to be updated size : '+cds1.size());
        if(!cds1.isEmpty())
            update cds1;
        
        system.debug('###### painmapTemps to be updated size : '+painmapTemps.size());
        
        if(!painmapTemps.isEmpty())
            insert painmapTemps;
        
        System.debug('Updated '+cds1.size()+' PainMaps & Created '+painmapTemps.size()+' Audit Records');
            
        List<Clinical_Data_Summary__c> clinicalDataSummaries = new List<Clinical_Data_Summary__c>();
        //To handle such patients which don't have any opty, thereby they will not have any PainMaps post migration. 
        for(Patient__c p1: scope){
            if(!patientsWithCDs.contains(p1.id)&& PatientManager.RECTYPES_NMD.contains(p1.RecordTypeId)){
                    clinicalDataSummaries.addAll(new List<Clinical_Data_Summary__c> {
                    new Clinical_Data_Summary__c(
                        Patient__c = p1.Id,
                        isActive__C=True,
                        RecordTypeId = ClinicalDataSummaryManager.RECTYPE_PAINMAP 
                    ),
                    new Clinical_Data_Summary__c(
                        Patient__c = p1.Id,
                        isActive__C=false,
                        RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS
                    )
                    
                });
                
            }
        }
        
        if(!clinicalDataSummaries.isEmpty()){
            
            insert clinicalDataSummaries;
            system.debug('Created New CDSs :'+ clinicalDataSummaries.size());
        }
        
          //marking patients as painmapmigration completed
        if(!ptsToUpdate.isEmpty()){
             update ptsToUpdate;
        }
        
    }
    
    //method to get that one opty whose CDS will be active. 
    private OpportunityRecord getFinalOpty(List<Opportunity> opptys){
    //this mthod should provide that final opportunity whose Clinical_Data_Summary__c should be made active based on certain conditions. 
    List<OpportunityRecord> optyRecords = new List<OpportunityRecord>();
    String patientName = null;
    for(Opportunity o: opptys){
        OpportunityRecord tempOR = new  OpportunityRecord(o.id, o.createdDate, o.recordType.name);
        patientName= o.Patient__r.name;
        optyRecords.add(tempOR);
    }
    
    system.debug('########### OptyRecords: '+optyRecords);
        
    OpportunityRecord finalOpty = new OpportunityRecord();
    //temp lists. 
    List<OpportunityRecord> trialImplants = new List<OpportunityRecord>();
    List<OpportunityRecord> trials = new List<OpportunityRecord>();
    List<OpportunityRecord> Implants = new List<OpportunityRecord>();
    List<OpportunityRecord> explants = new List<OpportunityRecord>();
    List<OpportunityRecord> revisions = new List<OpportunityRecord>();
    List<OpportunityRecord> reprogramming = new List<OpportunityRecord>();
    
    
        for(OpportunityRecord o : optyRecords){
            if(o.RecordTypeName == 'NMD SCS Trial-Implant' ){
                trialImplants.add(o);
            }else if(o.RecordTypeName == 'NMD SCS Trial' ){
                trials.add(o);
            }else if(o.RecordTypeName == 'NMD SCS Implant' ){
                Implants.add(o);
            }else if(o.RecordTypeName == 'NMD SCS Revision' ){
                revisions.add(o);
            }else if(o.RecordTypeName == 'NMD Reprogramming' ){
                reprogramming.add(o);
            }else if(o.RecordTypeName == 'NMD SCS Explant' ){
                explants.add(o);
            }
        
        }
        
        System.debug('========================STATUS===============================================');
        system.debug(trialImplants.size()+' TrialImplants for Patient :'+patientName);
        system.debug(trials.size()+' trials for Patient :'+patientName);
        system.debug(Implants.size()+' Implants for Patient :'+patientName);
        system.debug(revisions.size()+' revisions for Patient :'+patientName);
        system.debug(explants.size()+' explants for Patient :'+patientName);
        System.debug('========================######===============================================');
    
        if(trialImplants.size()>0){
            trialImplants.sort();//this will sort the opptys on the basis of their created date. 
            finalOpty = trialImplants[0];
        }else if(trials.size()>0){
            trials.sort();
            finalOpty = trials[0];
        }else if(Implants.size()>0){
            Implants.sort();
            finalOpty = Implants[0];
        }else if(revisions.size()>0){
            revisions.sort();
            finalOpty = revisions[0];
        }else if(explants.size()>0){
            explants.sort();
            finalOpty = explants[0];
        }
        
    system.debug('##################Retuning FinalOpty :'+finalOpty);
    return finalOpty;
    }
    
     /****************************************************
    *  finish(Database.BatchableContext BC)
    *****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Batch Execution finished.');
    }
        
    global void execute(SchedulableContext sc) {
      NMD_BatchPainMapReparent b = new NMD_BatchPainMapReparent(); 
      Database.executebatch(b, 20);//Batch size 200.
   }
    
    
    public  class OpportunityRecord implements Comparable{
        public Id recordId;
        public DateTime createdDate;
        String recordTypeName;
    
        public OpportunityRecord(){
            recordId= null;
            createdDate=null;
            recordTypeName='';
        }
    
        //Constructor
        public OpportunityRecord(Id recordId, DateTime createdDate, String recordTypeName){
            this.recordId = recordId;
            this.createdDate= createdDate;
            this.recordTypeName=recordTypeName;
        }
        
        public Integer compareTo(Object compareTo) {
            OpportunityRecord compareToOpty = (OpportunityRecord)compareTo;
            if (createddate == compareToOpty.createddate) return 0;
            if (createddate > compareToOpty.createddate) return -1;
            return 1;
        }
    
    }
    
}
/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/KOLCreation/*')
global class RestCreateKOLContact {
    global RestCreateKOLContact() {

    }
    @HttpPost
    global static openq.OpenMSL.CREATE_KOL_RESPONSE_element createKOLContact(openq.OpenMSL.CREATE_KOL_REQUEST_element request_x) {
        return null;
    }
}

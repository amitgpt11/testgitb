/**
* Utility class, to process submission and validation for Orders from the custom app
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class NMD_OrderSubmissionManager {

    final static String ERR_INVALID = 'Invalid/Missing Order';
    final static Set<String> patientStages = new Set<String> {
        'Pending', 'Pending SAP ID', 'Submitted', 'Awaiting Transfers', 'Updating Surgery'
    };
    final static Map<String,String> transferStages = new Map<String,String> {
        'Approval' => 'Transfer Requested',
        'Automated' => 'Submitted'
    };

    final static Map<Id,List<String>> requiredOrderFields = new Map<Id,List<String>> {
        OrderManager.RECTYPE_PATIENT => new List<String> {
            'Attending_Rep_Sold_to__c',
            'Commissionable_Rep_Sold_to__c',
            'Cost_Center__c',
            'Order_Reason__c',
            'PO_Type__c',
            'PoDate',
            'Surgery_Date__c',
            'Telephone__c'
        },
        OrderManager.RECTYPE_RETURN => new List<String> {
            'EffectiveDate',
            'Order_Reason__c',
            'Shipping_Location__r.Inventory_Data_ID__r.Account__r.Account_Number__c',
            'Type'
        },
        OrderManager.RECTYPE_TRANSFER => new List<String> {
            'Order_Reason__c',
            'PO_Type__c',
            'PoDate',
            'Receiving_Location__r.Inventory_Data_ID__r.Account__r.Account_Number__c',
            'Shipping_Location__r.Inventory_Data_ID__r.Account__r.Account_Number__c',
            'Type'
        }
    };

    final static Set<String> virtualKitIgnoreFields = new Set<String>{
        'Lot_Number__c',
        'Item_Status__c',
        'Inventory_Source__c'
    };

    final static Map<Id,List<String>> requiredOrderItemFields = new Map<Id,List<String>> {
        OrderManager.RECTYPE_PATIENT => new List<String> {
            'Item_Status__c',
            'Lot_Number__c',
            'Material_Number_UPN__c',
            'Order_Item_ID__c',
            'Quantity',
            'UnitPrice'
        },
        OrderManager.RECTYPE_RETURN => new List<String> {
            'Lot_Number__c',
            'Material_Number_UPN__c',
            'Order_Item_ID__c',
            'Quantity',
            'UnitPrice'
        },
        OrderManager.RECTYPE_TRANSFER => new List<String> {
            'Lot_Number__c',
            'Material_Number_UPN__c',
            'Order_Item_ID__c',
            'Quantity',
            'UnitPrice'
        }
    };

    @TestVisible private Order order;
    @TestVisible private Map<Id,Order> orders;
    @TestVisible private Map<Id,Inventory_Item__c> invItems;
    @TestVisible private Map<Id,List<Order>> childOrders;
    @TestVisible private Map<Id,List<OrderItem>> items;

    public String lastError { get; private set; }

    public NMD_OrderSubmissionManager(Set<Id> orderIds) {
        this.lastError = '';
        fetchOrders(orderIds);
    }

    @TestVisible private void fetchOrders(Set<Id> orderIds) {
        // fetch record
        this.orders = new Map<Id,Order>([
            select 
                Account.Account_Number__c,
                Attending_Rep_Sold_to__c,
                Commissionable_Rep__r.Email,
                Commissionable_Rep_Sold_to__c,
                CreatedBy.Email,
                Count_of_Valid_Order_Items__c,
                Cost_Center__c,
                EffectiveDate,
                Is_Exception__c,
                No_Charge_Evaluation__c,
                Opportunity.Patient__c,
                Opportunity.Patient__r.Patient_First_Name__c,
                Opportunity.Patient__r.Patient_Last_Name__c,
                Opportunity.Primary_Insurance__c,
                Order_Method__c,
                Order_Reason__c,
                Patient_SAP_ID__c,
                PODate,
                PONumber,
                PO_Type__c,
                Procedure_Type__c,
                Product_Billing_Form_Generated__c,
                RecordTypeId,
                Receiving_Location__r.Inventory_Data_ID__r.Account__r.Account_Number__c,
                Ship_Method__c,
                Shipping_Location__c,
                Shipping_Location__r.Inventory_Data_ID__r.Account__r.Account_Number__c,
                Shipping_Location__r.Inventory_Data_ID__r.Owner.Email,
                Special_Processing__c,
                Stage__c,
                Status,
                Stock_Location_Partner__c,
                Surgeon__c,
                Surgeon__r.Name,
                Surgeon__r.SAP_ID__c,
                Surgery_Date__c,
                Surgical_Center__c,
                Telephone__c,
                Type,
                Your_Reference__c
            from Order
            where Id in :orderIds
        ]);

        if (!this.orders.isEmpty()) {

            if (this.orders.size() == 1) {
                this.order = this.orders.values()[0];
                //System.debug(this.order);
            }

            // fetch child transfer orders
            Set<Id> childIds = new Set<Id>{};
            this.childOrders = new Map<Id,List<Order>>{};
            for (Order child : [
                select 
                    Originating_Order__c,
                    Stage__c
                from Order
                where Originating_Order__c in :this.orders.keySet()
                and RecordTypeId = :OrderManager.RECTYPE_TRANSFER
            ]) {
                childIds.add(child.Id);
                if (this.childOrders.containsKey(child.Originating_Order__c)) {
                    this.childOrders.get(child.Originating_Order__c).add(child);
                }
                else {
                    this.childOrders.put(child.Originating_Order__c, new List<Order> { child });
                }
            }

            // fetch child order items, map by parent Order.id
            this.items = new Map<Id,List<OrderItem>>{};
            Set<Id> sourceIds = new Set<Id>();
            for(OrderItem item : [
                select
                    Billing_Block__c,
                    OrderId,
                    Charge_Type__c,
                    Item_Status__c,
                    Inventory_Source__c,
                    //Inventory_Source__r.Inventory_Location__c,
                    //Inventory_Source__r.Available_Quantity__c,
                    Inventory_Location__c,
                    Lot_Number__c,
                    Material_Number_UPN__c,
                    Model_Number__c,
                    No_Charge_Reason__c,
                    PricebookEntry.ProductCode,
                    Quantity,
                    Serial_Number__c,
                    Serial_Lot__c,
                    UnitPrice,
                    Order_Item_ID__c,
                    Price_Verified__c,
                    Type__c,
                    Virtual_Kit__c,
                    IsVirtualKit__c
                from OrderItem
                where OrderId in :this.orders.keySet()
                or OrderId in :childIds
                Order by
                    Order_Item_Id__c ASC
            ]) {
                sourceIds.add(item.Inventory_Source__c);

                if (this.items.containsKey(item.OrderId)) {
                    this.items.get(item.OrderId).add(item);
                }
                else {
                    this.items.put(item.OrderId, new List<OrderItem> { item });
                }
            }

            this.invItems = new Map<Id,Inventory_Item__c>([
                select
                    Available_Quantity__c,
                    Inventory_Location__c
                from Inventory_Item__c
                where Id in :sourceIds
            ]);

        }
        
    }

    public Boolean submitOrders() {

        if (this.orders.isEmpty()) {
            return error(ERR_INVALID);
        }

        List<Map<String,Object>> errors = new List<Map<String,Object>>{};

        for (Order ord : this.orders.values()) {
            if (ord.RecordTypeId == OrderManager.RECTYPE_TRANSFER) {
                if (!transferStages.containsKey(ord.Order_Method__c)) {
                    errors.add(new Map<String,Object> {
                        'id' => ord.Id,
                        'error' => 'Order_Method__c: Invalid Value'
                    });
                }
            }

            List<Map<String,Object>> fieldErrors = validateFields(ord);

            if (fieldErrors.isEmpty()) {
                if (ord.RecordTypeId == OrderManager.RECTYPE_TRANSFER && ord.Stage__c != 'Confirmed') {
                    ord.Stage__c = transferStages.get(ord.Order_Method__c);
                }
                //else if (ord.RecordTypeId == OrderManager.RECTYPE_PATIENT) {
                    
                //    if (String.isBlank(this.order.Surgical_Center__c)
                //     || String.isBlank(this.order.Patient_SAP_ID__c) 
                //     || String.isBlank(this.order.Surgeon__r.SAP_ID__c)
                //    ) {
                //        this.order.Stage__c = 'Pending SAP ID';
                //    }
                //    else if (this.childOrders.containsKey(ord.Id)) {

                //        Boolean allConfirmed = true;
                //        for (Order child : this.childOrders.get(ord.Id)) {

                //            if (child.Stage__c != 'Confirmed') {
                //                allConfirmed = false;
                //                break;
                //            }

                //        }

                //        if (!allConfirmed) {
                //            ord.Stage__c = 'Awaiting Transfers';
                //        }
                //        else {
                //            ord.Stage__c = 'Updating Surgery';
                //        }

                //    }
                //    // Is_Exception__c == true if any OrderItem CustomerPrice != UnitPrice
                //    else if (ord.Is_Exception__c || (new Set<String> { 'B11', 'NM4', 'NM7', 'NM8' }).contains(ord.Order_Reason__c)) {
                //        ord.Stage__c = 'Pending';
                //        ord.Special_Processing__c = 'Processing Required';
                //    }
                //    else if ((new Set<String> { 'NM2', 'NM3' }).contains(ord.Order_Reason__c)) {
                //        ord.Stage__c = 'Submitted';
                //    }
                //    else if (ord.No_Charge_Evaluation__c > 0) {
                //        ord.Stage__c = 'Pending';
                //    }
                //    else {
                //        ord.Stage__c = 'Updating Surgery';
                //    }

                //}
                else if (ord.RecordTypeId == OrderManager.RECTYPE_RETURN) {
                    if ((new Set<String> { 'B11', 'NM4', 'NM7', 'NM8' }).contains(ord.Order_Reason__c)) {
                        ord.Stage__c = 'Pending';
                        ord.Special_Processing__c = 'Processing Required';
                    }
                    else if ((new Set<String> { 'NM2', 'NM3' }).contains(ord.Order_Reason__c)) {
                        ord.Stage__c = 'Submitted';
                    }
                }
            }
            else {
                errors.addAll(fieldErrors);
            }
        }

        if (errors.isEmpty()) {
            try {

                Boolean canSave = true;
                if (this.order.RecordTypeId == OrderManager.RECTYPE_PATIENT
                 && patientStages.contains(this.order.Stage__c)
                ) {
                    canSave = consolidateOrdersItems();
                }

                if (canSave) {
                    //update this.orders.values();
                    DML.save(this, this.orders.values());
                }
                else {
                    errors.add(new Map<String,Object> {
                        'id' => '',
                        'errors' => this.lastError
                    });
                }

            }
            catch (System.DmlException de) {

                return error(de.getDmlMessage(0));

            }
        }
        else {
            return error(JSON.serialize(errors));
        }

        if (!errors.isEmpty()) {
            //Database.rollback(sp);
            return error(JSON.serialize(errors));
        }

        return true;

    }

    public Boolean validateOrder() {

        if (this.order == null) {
            return error(ERR_INVALID);
        }

        List<Map<String,Object>> errors = validateFields(this.order);
        
        if (!errors.isEmpty()) {
            return error(JSON.serialize(errors));
        }
        
        
        if (String.isBlank(this.order.Surgical_Center__c)
         || String.isBlank(this.order.Patient_SAP_ID__c) 
         || String.isBlank(this.order.Surgeon__r.SAP_ID__c)
        ) {
            this.order.Stage__c = 'Pending SAP ID';
        }
        else if (this.childOrders.containsKey(this.order.Id)) {

            Boolean allConfirmed = true;
            for (Order child : this.childOrders.get(this.order.Id)) {

                if (child.Stage__c != 'Confirmed') {
                    allConfirmed = false;
                    break;
                }

            }

            if (!allConfirmed) {
                this.order.Stage__c = 'Awaiting Transfers';
            }
            else {
                this.order.Stage__c = 'Updating Surgery';
            }

        }
        // Is_Exception__c == true if any OrderItem CustomerPrice != UnitPrice
        else if (this.order.Is_Exception__c || (new Set<String> { 'B11', 'NM4', 'NM7', 'NM8' }).contains(this.order.Order_Reason__c)) {
            this.order.Stage__c = 'Pending';
            this.order.Special_Processing__c = 'Processing Required';
        }
        else if ((new Set<String> { 'NM2', 'NM3' }).contains(this.order.Order_Reason__c)) {
            this.order.Stage__c = 'Submitted';
        }
        else if (this.order.No_Charge_Evaluation__c > 0) {
            this.order.Stage__c = 'Pending';
        }
        else {
            this.order.Stage__c = 'Updating Surgery';
        }

        System.Savepoint sp = Database.setSavepoint();

        try {

            Boolean canSave = true;
            if (this.order.RecordTypeId == OrderManager.RECTYPE_PATIENT
             && patientStages.contains(this.order.Stage__c)
            ) {
                canSave = consolidateOrdersItems();
            }

            if (canSave) {
                //update this.order;
                DML.save(this, this.order);
            }
            else {
                //errors.add(this.order.Id => this.lastError);
                errors.add(new Map<String,Object> {
                    'id' => this.order.Id,
                    'errors' => this.lastError
                });
            }
            
        }
        catch (System.DmlException de) {
            errors.add(new Map<String,Object> {
                this.order.Id => (de.getDmlMessage(0))
            });

        }
        catch (System.Exception ex) {
            errors.add(new Map<String,Object> {
                this.order.Id => (ex.getMessage())
            });
        }

        if (!errors.isEmpty()) {
            Database.rollback(sp);
            return error(JSON.serialize(errors));
        }

        return true;

    }

    public Boolean verifyOrder() {

        //if (this.order == null) {
        //    return error(ERR_INVALID);
        //}

        //try {

        //    this.order.Stage__c = 'Verify';
        //    update this.order;

        //}
        //catch (System.DmlException de) {

        //    return error(de.getDmlMessage(0));

        //}

        return true;

    }

    @TestVisible private Boolean consolidateOrdersItems() {

        Map<String,OrderItem> consolidatedItems = new Map<String,OrderItem>{};
        List<OrderItem> dupeItems = new List<OrderItem>{};

        Set<Id> orderIds = new Set<Id>();

        for (Order order : this.orders.values()) {
            if (!this.items.containsKey(order.Id)) continue;

            orderIds.add(order.Id);//used for reordering later

            for (OrderItem item : this.items.get(order.Id)) {
                if (String.isNotBlank(item.Serial_Number__c)) continue;
                if (item.IsVirtualKit__c) continue; //do not consolidate the Virtual Kit parent records

                //Similar records can only be consolidated if they are not in a virtual kit, 
                //  or have the same parent
                String key = String.format('{0}{1}{2}{3}{4}{5}{6}', new List<String> {
                    item.Model_Number__c,
                    item.Lot_Number__c,
                    item.Item_Status__c,
                    (String.valueOf(item.UnitPrice)),
                    item.No_Charge_Reason__c,
                    item.OrderId,
                    item.Virtual_Kit__c
                });

                if (consolidatedItems.containsKey(key)) {
                    consolidatedItems.get(key).Quantity += item.Quantity;
                    dupeItems.add(item);
                }
                else {
                    consolidatedItems.put(key, item);
                }

            }

        }

        System.Savepoint sp = Database.setSavepoint();

        try {

            DML.save(this, consolidatedItems.values());

            OrderItemManager.consolidating = true;
            DML.remove(this, dupeItems);
            OrderItemManager.consolidating = false;

            // need to reset some flags so that transactions will still create
            TriggerHandlerManager.clearTriggerHashes();
            OrderManager.createTransactions = true;
        }
        catch (System.DmlException de) {
            Database.rollback(sp);
            return error(de.getDmlMessage(0));
        }

        return true;

    }


    /**
    * Recalculates the order of the Order items and makes sure that the Virtual Kit children
    *   are sequentially after the parent record.  
    * 
    * @param Set<Id> orderIds ids of the submitted orders
    */   
    @TestVisible private Boolean reorderOrderItemIds(Set<Id> orderIds){

        if(!orderIds.isEmpty()){
            List<OrderItem> updatedItems = new List<OrderItem>();

            Map<Id, List<OrderItem>> childItemMap = new Map<Id, List<OrderItem>>();

            Map<Id, List<OrderItem>> kitItemMap = new Map<Id, List<OrderItem>>();

            for(OrderItem item : [
                select
                    Id,
                    OrderId,
                    Order_Item_Id__c,
                    Higher_Level_Item__c,
                    IsVirtualKit__c,
                    Virtual_Kit__c
                from OrderItem
                where OrderId IN :orderIds
                Order by
                    OrderId,
                    Order_Item_Id__c,
                    Higher_Level_Item__c
            ]){

                //Build map of virtual kit children and a separate map for order child records
                if(item.Virtual_Kit__c != null){
                    if(!kitItemMap.containsKey(item.Virtual_Kit__c)){
                        kitItemMap.put(item.Virtual_Kit__c, new List<OrderItem>());
                    }
                    kitItemMap.get(item.Virtual_Kit__c).add(item);
                } else {

                    if(!childItemMap.containsKey(item.OrderId)){
                        childItemMap.put(item.OrderId, new List<OrderItem>());
                    }
                    childItemMap.get(item.OrderId).add(item);
                }

            }

            if(!childItemMap.isEmpty()){

                //Loop through each order
                for(Id orderId : childItemMap.keySet()){
                    List<OrderItem> items = childItemMap.get(orderId);
                    Integer count = 10;

                    //loop through Orderitem records for each order
                    for(OrderItem item : items){
                        if(item.Order_Item_Id__c != count){
                            item.Order_Item_Id__c = count;
                            updatedItems.add(item);
                        }

                        //If the Orderitem is a virtual kit, then reorder children sequentially
                        if(kitItemMap.containsKey(item.Id)){
                            for(OrderItem kitItem : kitItemMap.get(item.Id)){
                                count += 10;
                                if(kitItem.Order_Item_Id__c != count
                                    || kitItem.Higher_Level_Item__c != item.Order_Item_Id__c
                                ){
                                    kitItem.Order_Item_Id__c = count;
                                    kitItem.Higher_Level_Item__c = item.Order_Item_Id__c;
                                    updatedItems.add(kitItem);
                                }
                            }
                        }

                        count += 10;
                    }
                }

            }

            /*
            Test to try and reduce the loops to 2 instead of 3

            List<OrderItem> updatedItems = new List<OrderItem>();

            Map<Id, List<OrderItem>> childItemMap = new Map<Id, List<OrderItem>>();

            Map<Id, List<OrderItem>> kitItemMap = new Map<Id, List<OrderItem>>();

            for(OrderItem item : [
                select
                    Id,
                    OrderId,
                    Order_Item_Id__c,
                    Higher_Level_Item__c,
                    IsVirtualKit__c,
                    Virtual_Kit__c
                from OrderItem
                where OrderId IN :orderIds
                Order by
                    OrderId,
                    Order_Item_Id__c,
                    Higher_Level_Item__c
            ]){

                if(!childItemMap.containsKey(item.OrderId)){
                    childItemMap.put(item.OrderId, new List<OrderItem>());
                }
                childItemMap.get(item.OrderId).add(item);

            }

            if(!childItemMap.isEmpty()){

                Map<Decimal, Decimal> kitCountMap = new Map<Decimal, Decimal>();

                Map<Id, OrderItem> updatedMap = new Map<Id, OrderItem>();

                for(Id orderId : childItemMap.keySet()){
                    List<OrderItem> items = childItemMap.get(orderId);
                    Integer count = 10;
                    for(OrderItem item : items){
                        Decimal oldCount = item.Order_Item_Id__c;
                        if(item.Order_Item_Id__c != count){
                            item.Order_Item_Id__c = count;
                            updatedMap.put(item.Id, item);
                        }

                        if(item.IsVirtualKit__c){
                            if(!kitCountMap.containsKey(oldCount)
                                && oldCount != null
                            ){
                                kitCountMap.put(oldCount, count);
                            }
                        }

                        if(item.Higher_Level_Item__c != null){

                            if(kitCountMap.containsKey(item.Higher_Level_Item__c)){
                                Decimal highCount = kitCountMap.get(item.Higher_Level_Item__c);
                                if(item.Higher_Level_Item__c != highCount){
                                    item.Higher_Level_Item__c = highCount;
                                    updatedMap.put(item.Id, item);
                                }
                            }

                        }

                        count += 10;
                    }
                }

                updatedItems.addAll(updatedMap.values());

            }*/

            System.Savepoint sp = Database.setSavepoint();

            try{

                if(!updatedItems.isEmpty()){
                    DML.save(this, updatedItems);
                }

                //reset flag
                TriggerHandlerManager.clearTriggerHashes();

            } catch(System.DmlException de){
                Database.rollback(sp);
                return error(de.getDmlMessage(0));
            }
        }

        return true;

    }

    @TestVisible private List<Map<String,Object>> validateFields(Order ord) {

        List<Map<String,Object>> errors = new List<Map<String,Object>>{};
        List<String> fieldErrors = new List<String>{};

        if (requiredOrderFields.containsKey(ord.RecordTypeId)) {

            if (this.items.containsKey(ord.Id)) {

                Set<Id> sources = new Set<Id>();

                for (OrderItem item : this.items.get(ord.Id)) {

                    for (String field : requiredOrderItemFields.get(ord.RecordTypeId)) {
                        if(item.IsVirtualKit__c && virtualKitIgnoreFields.contains(field)){
                            continue;
                        }
                        if (fails(SObjectManager.getField(item, field))) {
                            fieldErrors.add(field);
                        }
                    }

                    Inventory_Item__c invItem = this.invItems.get(item.Inventory_Source__c);

                    if (ord.RecordTypeId == OrderManager.RECTYPE_PATIENT) {
                        if (item.Item_Status__c == 'Explanted' || item.No_Charge_Reason__c == 'Pre Purchase') {
                            sources.add(item.Inventory_Location__c);
                        }
                        else if (item.IsVirtualKit__c == false
                            && item.Inventory_Source__c == null
                        ) {
                            fieldErrors.add('Inventory_Source__c');
                        }
                    }

                    if (item.Item_Status__c != 'Explanted'
                     && invItem != null
                     && (ord.RecordTypeId == OrderManager.RECTYPE_PATIENT 
                            || ord.RecordTypeId == OrderManager.RECTYPE_TRANSFER
                            || ord.RecordTypeId == OrderManager.RECTYPE_RETURN
                        )
                    ) {

                        if (invItem.Inventory_Location__c == ord.Shipping_Location__c
                         && item.Quantity > invItem.Available_Quantity__c
                        ) {
                            fieldErrors.add('Insufficient Quantity');
                        }
                        else {
                            invItem.Available_Quantity__c -= item.Quantity;
                        }

                    }

                    if (!fieldErrors.isEmpty()) {
                        errors.add(new Map<String,Object> {
                            'id' => item.Id,
                            'errors' => fieldErrors.clone()
                        });
                    }

                    fieldErrors.clear();

                }

                for (String field : requiredOrderFields.get(ord.RecordTypeId)) {
                    if (fails(SObjectManager.getField(ord, field))) {
                        fieldErrors.add(field);
                    }
                }

                if (!fieldErrors.isEmpty()) {
                    errors.add(new Map<String,Object> {
                        'id' => ord.Id,
                        'errors' => fieldErrors.clone()
                    });
                }

            }
            else {
                errors.add(new Map<String,Object> {
                    'id' => ord.Id,
                    'errors' => new List<String> { 'No valid items found.' }
                });
            }
        }

        return errors;

    }

    @TestVisible private Boolean fails(Object val) {
        return String.isBlank(String.valueOf(val)) || val == 'null';
    }

    @TestVisible private Boolean error(String message) {
        this.lastError = message;
        return false;
    }

    @TestVisible private String safeInitial(String s) {
        return String.isBlank(s) ? '' : s.substring(0, 1);
    }

    @TestVisible private String safeDateFormat(Date d) {
        return d == null ? '' : d.format();
    }

}
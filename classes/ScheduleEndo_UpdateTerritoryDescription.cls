/*
 * User Story: SFDC-11, AC1: Need to update territory descriptions for all GI/PULM - All under ENUS, When Data come from HANA fro Territoy Model (Custom Object).
 * Created By: Shashank Gupta (Accenture Team)
 * Creatd Date: 12th Oct, 2016
 * Schedulable class for : Endo_UpdateTerritoryDescription batch Class
 * 
 * Cron Job: 
 * ScheduleEndo_UpdateTerritoryDescription m = new ScheduleEndo_UpdateTerritoryDescription();
   String sch = '0 30 23 ? * MON-FRI'; ------> Class runs every day at 11.30 PM IST MON-FRI.
   String jobID = system.schedule('Update GI/PULM Territory Description', sch, m);
*/
global class ScheduleEndo_UpdateTerritoryDescription implements Schedulable {
    global void execute(SchedulableContext sc) {
        Endo_UpdateTerritoryDescription b = new Endo_UpdateTerritoryDescription(); 
        database.executebatch(b,200);
    }
}
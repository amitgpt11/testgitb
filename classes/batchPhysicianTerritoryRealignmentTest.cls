/**
* Test Class for the batchPhysicianTerritoryRealignment.       
*
* @Author Accenture Services
* @Date 07/29/2016
*/
@isTest(SeeAllData=false)
@testVisible
public class batchPhysicianTerritoryRealignmentTest {

    static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LNAME = 'LName';
    static final String LEAD_SOURCE = 'Care Card';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static Opportunity trialOpty = null;
    static Opportunity implantOpty = null;
    static DateTime dT = System.now();
    static Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
    
   // @testSetup
    static void setup() {
    
        NMD_TestDataManager td = new NMD_TestDataManager();
        
        
        Seller_Hierarchy__c seller =td.newSellerHierarchy();
        insert seller;
        
        Account acc = td.newAccount();
        insert acc;
        
        Contact cont = td.newContact(acc.id);
        cont.Territory_ID__c = seller.id;
        cont.RecordTypeId = ContactManager.RECTYPE_PROSPECT; 
        insert cont;
        
        Physician_Territory_Realignment__c physTer = new Physician_Territory_Realignment__c ( Contact__c=cont.id,
        New_Account_Id__c=acc.id,
        Current_Territory__c=seller.id,
        Effective_Realignment_Date__c=myDate,
        New_Territory__c=seller.id,
        Realignment_Status__c = 'Pending');  
        insert physTer;
       
        Patient__c patients = new Patient__c(
        Physician_of_Record__c=cont.id,
        Territory_ID__c=seller.id);
        insert patients;
    
    }
    
    static testMethod void test_method_1() {
       setup();
   List<Physician_Territory_Realignment__c> phys = [SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c = 'Pending' and Effective_Realignment_Date__c =: myDate];
  Test.StartTest();
    NMD_TestDataManager.createRowCauseObjects();
   //batchPhysicianTerritoryRealignment bptr=new batchPhysicianTerritoryRealignment();
     //Database.executeBatch(bptr);
     ID batchprocessid = Database.executeBatch(new batchPhysicianTerritoryRealignment());
     Test.StopTest();   
    }
    
}
/*
-------11/2/2015 - Mike Lankfer - Created AccountPerformanceTriggerMethods_Test class.  Added 
*/

@isTest
public class AccountPerformanceTriggerMethodsTest {
    
    static testmethod void createAPRecords(){
        
        Test.startTest();
        
        //create list of 1 test account
        List<Account> accounts = TestDataFactory.createTestAccounts(1);
        
        //create list of 250 ap records on 1 test account
        List<Account_Performance__c> accountPerformanceRecords = TestDataFactory.createTestAPRecords(accounts[0], 250);
        
        Test.stopTest();
        
        //check parent Account fields for NULL values after insert
        for (Account_Performance__c ap : accountPerformanceRecords){
            System.assertEquals(Null, ap.Facility_Name__r.National_Tiering_CRM__c);
            System.assertEquals(Null, ap.Facility_Name__r.National_Tiering_EP__c);
            System.assertEquals(Null, ap.Facility_Name__r.National_Tiering_IC__c);
            System.assertEquals(Null, ap.Facility_Name__r.National_Tiering_PI__c);
            System.assertEquals(Null, ap.Facility_Name__r.National_Tiering_U_WH__c);
        }        
        
    }
    
    
    static testmethod void updateAPRecords(){
        
        Test.startTest();
        
        //create list of 1 test account
        List<Account> accounts = TestDataFactory.createTestAccounts(1);
        
        //create list of 250 ap records on 1 test account
        List<Account_Performance__c> accountPerformanceRecords = TestDataFactory.createTestAPRecords(accounts[0], 250);
        
        //update AP records with Division, QBR, Rev. Amount, 
        for (Integer i = 0; i < accountPerformanceRecords.size(); i++){
            if ((i >= 0) && (i < 50)){
                accountPerformanceRecords[i].Division__c = 'CRM';
                accountPerformanceRecords[i].National_Tiering__c = '1' ;
            }
            else if ((i >= 51) && (i < 100)){
                accountPerformanceRecords[i].Division__c = 'EP';
                accountPerformanceRecords[i].National_Tiering__c = '2' ;
            }
            else if ((i >= 101) && (i < 150)){
                accountPerformanceRecords[i].Division__c = 'IC';
                accountPerformanceRecords[i].National_Tiering__c = '3' ;
            }
            else if ((i >= 151) && (i < 200)){
                accountPerformanceRecords[i].Division__c = 'PI';
                accountPerformanceRecords[i].National_Tiering__c = '4' ;
            }
            else {
                accountPerformanceRecords[i].Division__c = 'Uro/WH';
                accountPerformanceRecords[i].National_Tiering__c = '5' ;
            }
            
        }
        
        update accountPerformanceRecords;
        
        accountPerformanceRecords = [SELECT Id, Division__c, National_Tiering__c, Facility_Name__r.National_Tiering_CRM__c, Facility_Name__r.National_Tiering_EP__c, Facility_Name__r.National_Tiering_IC__c,
                                     Facility_Name__r.National_Tiering_PI__c, Facility_Name__r.National_Tiering_U_WH__c FROM Account_Performance__c WHERE Id IN :accountPerformanceRecords];
        
        Test.stopTest();
        
        //compare ap values to corresponding divisional values on parent account
        for (Account_Performance__c ap : accountPerformanceRecords){
            
            if (ap.Division__c == 'CRM'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_CRM__c);
            }
            else if (ap.Division__c == 'EP'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_EP__c);
            }
            else if (ap.Division__c == 'IC'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_IC__c);
            }
            else if (ap.Division__c == 'PI'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_PI__c);
            }
            else if (ap.Division__c == 'Uro/WH'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_U_WH__c);
            }            
        } 
    }
    
    
    
    static testmethod void deleteAPRecords(){
        
        Test.startTest();
        
        //create list of 1 test account
        List<Account> accounts = TestDataFactory.createTestAccounts(1);
        
        //create list of 250 ap records on 1 test account
        List<Account_Performance__c> accountPerformanceRecords = TestDataFactory.createTestAPRecords(accounts[0], 250);
        
        //update AP records with Division, QBR, Rev. Amount, 
        for (Integer i = 0; i < accountPerformanceRecords.size(); i++){
            if ((i >= 0) && (i < 50)){
                accountPerformanceRecords[i].Division__c = 'CRM';
                accountPerformanceRecords[i].National_Tiering__c = '1' ;
            }
            else if ((i >= 51) && (i < 100)){
                accountPerformanceRecords[i].Division__c = 'EP';
                accountPerformanceRecords[i].National_Tiering__c = '2' ;
            }
            else if ((i >= 101) && (i < 150)){
                accountPerformanceRecords[i].Division__c = 'IC';
                accountPerformanceRecords[i].National_Tiering__c = '3' ;
            }
            else if ((i >= 151) && (i < 200)){
                accountPerformanceRecords[i].Division__c = 'PI';
                accountPerformanceRecords[i].National_Tiering__c = '4' ;
            }
            else {
                accountPerformanceRecords[i].Division__c = 'Uro/WH';
                accountPerformanceRecords[i].National_Tiering__c = '5' ;
            }
            
        }
        
        update accountPerformanceRecords;
        
        delete accountPerformanceRecords;    
        
        accounts = [SELECT Id, National_Tiering_CRM__c, National_Tiering_EP__c, National_Tiering_IC__c, National_Tiering_PI__c, National_Tiering_U_WH__c FROM Account WHERE Id IN :accounts];
        
        
        Test.stopTest();
        
        System.assertEquals(Null, accounts[0].National_Tiering_CRM__c);
        System.assertEquals(Null, accounts[0].National_Tiering_EP__c);
        System.assertEquals(Null, accounts[0].National_Tiering_IC__c);
        System.assertEquals(Null, accounts[0].National_Tiering_PI__c);
        System.assertEquals(Null, accounts[0].National_Tiering_U_WH__c);
    }
    
    
    
    
    static testmethod void undeleteAPRecords(){
        
        Test.startTest();
        
        //create list of 1 test account
        List<Account> accounts = TestDataFactory.createTestAccounts(1);
        
        //create list of 250 ap records on 1 test account
        List<Account_Performance__c> accountPerformanceRecords = TestDataFactory.createTestAPRecords(accounts[0], 250);
        
        //update AP records with Division, QBR, Rev. Amount, 
        for (Integer i = 0; i < accountPerformanceRecords.size(); i++){
            if ((i >= 0) && (i < 50)){
                accountPerformanceRecords[i].Division__c = 'CRM';
                accountPerformanceRecords[i].National_Tiering__c = '1' ;
            }
            else if ((i >= 51) && (i < 100)){
                accountPerformanceRecords[i].Division__c = 'EP';
                accountPerformanceRecords[i].National_Tiering__c = '2' ;
            }
            else if ((i >= 101) && (i < 150)){
                accountPerformanceRecords[i].Division__c = 'IC';
                accountPerformanceRecords[i].National_Tiering__c = '3' ;
            }
            else if ((i >= 151) && (i < 200)){
                accountPerformanceRecords[i].Division__c = 'PI';
                accountPerformanceRecords[i].National_Tiering__c = '4' ;
            }
            else {
                accountPerformanceRecords[i].Division__c = 'Uro/WH';
                accountPerformanceRecords[i].National_Tiering__c = '5' ;
            }
            
        }
        
        update accountPerformanceRecords;        
        delete accountPerformanceRecords;
        undelete accountPerformanceRecords;
        
        accountPerformanceRecords = [SELECT Id, Division__c, National_Tiering__c,Facility_Name__r.National_Tiering_CRM__c, Facility_Name__r.National_Tiering_EP__c, Facility_Name__r.National_Tiering_IC__c,Facility_Name__r.National_Tiering_PI__c,
                                     Facility_Name__r.National_Tiering_U_WH__c FROM Account_Performance__c WHERE Id IN :accountPerformanceRecords];
        
        Test.stopTest();
        
        //compare ap values to corresponding divisional values on parent account
        for (Account_Performance__c ap : accountPerformanceRecords){
            
            if (ap.Division__c == 'CRM'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_CRM__c);
            }
            else if (ap.Division__c == 'EP'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_EP__c);
            }
            else if (ap.Division__c == 'IC'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_IC__c);
            }
            else if (ap.Division__c == 'PI'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_PI__c);
            }
            else if (ap.Division__c == 'Uro/WH'){
                System.assertEquals(ap.National_Tiering__c, ap.Facility_Name__r.National_Tiering_U_WH__c);
            }            
        }
    }
}
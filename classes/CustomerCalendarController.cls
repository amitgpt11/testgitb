global with sharing class CustomerCalendarController extends AuthorizationUtil{
    
    
    public Boston_Scientific_Config__c objBostonConfig {get;set;}
    public List<String> lstHolidays {get;set;}
    public BusinessHourWrapper objBusinessHours {get; set;}
    public Meeting__c objMeeting {get;set;}
    
    /**
        Description: Inner Class to hold the values for Business hours to bind with Javascript.
    */
    private class BusinessHourWrapper{
    
        public String mondayBusinessHourStartTime {get;set;}
        public String mondayBusinessHourEndTime {get;set;}
        public String mondowBH {get;set;}
        public String tuesdayBusinessHourStartTime {get;set;}
        public String tuesdayBusinessHourEndTime {get;set;}
        public String tuedowBH {get;set;}
        public String wednesdayBusinessHourStartTime {get;set;}
        public String wednesdayBusinessHourEndTime {get;set;}
        public String weddowBH {get;set;}
        public String thursdayBusinessHourStartTime {get;set;}
        public String thursdayBusinessHourEndTime {get;set;}
        public String thudowBH {get;set;}
        public String fridayBusinessHourStartTime {get;set;}
        public String fridayBusinessHourEndTime {get;set;}
        public String fridowBH {get;set;}
    }
    
    /**
        Description: Inner Class to hold the values came from the Javascript to Save in Meeting Object.
    */
    private class MeetingRequest {
        
        public string AppointmentTopic{get; set;}
        public String Start_TimeSelected{get; set;}
        public string EventId{get; set;}
        public string Start_DateSelected{get; set;}
        public string Shared_Phone_Number{get; set;}
    }
    
    
    /**
        Description: Class to hold calendar event data
    */
    private class MeetingWrapper{
        
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String start {get;set;}
        public String endString {get;set;}
        public String className {get;set;}
    }
    
    /**
        Description: Constructor functionality placed here
    */
   public override void fetchRequestedData() {
        
        //init
        lstHolidays = new List<String>();
        objBostonConfig = Boston_Scientific_Config__c.getOrgDefaults();
        objMeeting = new Meeting__c();
        objBusinessHours = new BusinessHourWrapper();
        User loggedInUser = new user();
        String defaultStartTime = '09:00';
        String defaultEndTime = '06:00';
        String timeFormatString = 'hh:mm a';
        
        //BSMI-129
        String strUserDivision = [  SELECT Shared_Community_Division__c
                                    FROM User
                                    WHERE Id =: UserInfo.getUserId()].Shared_Community_Division__c;
                                        
        strUserDivision = String.isBlank(strUserDivision) ? 'Endo':strUserDivision;
        
        // to load Appointment Topic picklist on page
        objMeeting.recordTypeId = Schema.getGlobalDescribe().get('Meeting__c').getDescribe().getRecordTypeInfosByName().get(strUserDivision+' Meetings').getRecordTypeId();
        
        //Get Business Hours for the Organization
        BusinessHours objBusinessHour = [   SELECT  TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime,
                                                    SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime
                                            FROM BusinessHours 
                                            WHERE Name = 'VTM_Availability' limit 1];
        
        //get user detail to get VTM's time zone and logged in user time zone to display time slots on calendar page
        loggedInUser =  [   SELECT Id, TimeZoneSidKey,Shared_Community_Division__c, Contact.OwnerId,Contact.owner.TimeZoneSidKey
                            FROM User
                            WHERE Id =: UserInfo.getUserId() LIMIT 1];
            
        Date todayDate = Date.today();
        Time currentTime = Time.newInstance(0, 0, 0, 0);
        DateTime gmtDate = DateTime.newInstanceGMT(todayDate, currentTime);
        Timezone vtmTimeZone;
        if(Test.isRunningTest()){
            vtmTimeZone = Timezone.getTimeZone('America/Chicago'); //CST VTM time's zone
        }else{
            vtmTimeZone = Timezone.getTimeZone(loggedInUser.Contact.owner.TimeZoneSidKey); //CST VTM time's zone
        }
        Integer vtmOffset = vtmTimeZone.getOffset(gmtDate);
        Timezone loggedInUserTimeZone = Timezone.getTimeZone(loggedInUser.TimeZoneSidKey); //America/New_York/Los_Angeles
        Integer loggedInUserOffset = loggedInUserTimeZone.getOffset(gmtDate);
        Integer offsetDifference = (vtmOffset - loggedInUserOffset)/(1000*60*60);
        
        //business hours slots value sent to VF
        objBusinessHours.mondayBusinessHourStartTime = objBusinessHour.MondayStartTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.MondayStartTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultStartTime;
        objBusinessHours.mondayBusinessHourEndTime = objBusinessHour.MondayEndTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.MondayEndTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultEndTime;
        objBusinessHours.mondowBH = '1';
        objBusinessHours.tuesdayBusinessHourStartTime = objBusinessHour.TuesdayStartTime != Null ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.TuesdayStartTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultStartTime;
        objBusinessHours.tuesdayBusinessHourEndTime = objBusinessHour.TuesdayEndTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.TuesdayEndTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultEndTime;
        objBusinessHours.tuedowBH = '2';
        objBusinessHours.wednesdayBusinessHourStartTime = objBusinessHour.WednesdayStartTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.WednesdayStartTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultStartTime;
        objBusinessHours.wednesdayBusinessHourEndTime = objBusinessHour.WednesdayEndTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.WednesdayEndTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultEndTime;
        objBusinessHours.weddowBH = '3';
        objBusinessHours.thursdayBusinessHourStartTime = objBusinessHour.ThursdayStartTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.ThursdayStartTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultStartTime;
        objBusinessHours.thursdayBusinessHourEndTime = objBusinessHour.ThursdayEndTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.ThursdayEndTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultEndTime;
        objBusinessHours.thudowBH = '4';
        objBusinessHours.fridayBusinessHourStartTime = objBusinessHour.FridayStartTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.FridayStartTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultStartTime;
        objBusinessHours.fridayBusinessHourEndTime = objBusinessHour.FridayEndTime != NULL ? String.valueOf(getDateTimeForLoggedInUser(objBusinessHour.FridayEndTime,offsetDifference).format(timeFormatString,loggedInUser.TimeZoneSidKey)).substring(0,5) : defaultEndTime;
        objBusinessHours.fridowBH = '5';
        
        //get the array List from holidayList
        for(Holiday objHoliday: [   SELECT ActivityDate
                                    FROM Holiday
                                    WHERE ActivityDate >= Today
                                ]) {
            
            DateTime holidayDateTime = objHoliday.ActivityDate;
            String holidayDate = holidayDateTime.addDays(1).format('YYYY-MM-dd');
            lstHolidays.add(holidayDate);
        }
    }
    
    private Datetime getDateTimeForLoggedInUser(Time objTime, Integer offsetDifference) {
        
        return Datetime.newInstance(system.today().year(), system.today().month(), system.today().day(), Integer.valueOf(String.valueOf(objTime).split(':')[0]) - offsetDifference, Integer.valueOf(String.valueOf(objTime).split(':')[1]), 0);
    }
    
    @RemoteAction
    public static user getCurrentUserdetail(){
        
        return  [   SELECT Id, Shared_Community_Division__c, Contact.OwnerId, Contact.owner.Name, Contact.Name, Contact.Email, Contact.Phone, ContactId
                    FROM User
                    WHERE Id =: UserInfo.getUserId() LIMIT 1
                ];
    }
    
    private static Integer getBusinessHoursPerDay(Time endTime,Time startTime) {
        
        return Integer.valueof(String.valueOf(endTime).substring(0,2)) - Integer.valueof(String.valueOf(startTime).substring(0,2));
    }

    /**
        Description: Called from the PageLoad method to get corresponding Meetings
    */
    @RemoteAction
    public static map<String, Object> getMeetings() {
        
        //Variables
        map<String, Integer> mapObjHoursAgainstDayOfWeek = new map<String, Integer>();
        list<MeetingWrapper> meetings = New List<MeetingWrapper>();
        map<DateTime, Integer> mapObjHoursAgainstDate = new map<DateTime, Integer>();
        map<DateTime, Boolean> mapObjAvailabilityAgainstDate = new map<DateTime, Boolean>();
        List<String> nonAvailableDates = new List<String>();
        Boston_Scientific_Config__c objBostonConfig = Boston_Scientific_Config__c.getOrgDefaults();
        map<String, Object> mapDayToMeetings = new map<String, Object>();
        
        String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
        DateTime fetchMeetingsTillDate = DateTime.now();
        
        //Get Business Hours for the Organization
        BusinessHours objBusinessHour = [SELECT SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime
            FROM BusinessHours 
            WHERE Name = 'VTM_Availability'];
        
        //get the Schema of the BusinessHours Object to get fieldsAPI names and Fill the Map mapObjHoursAgainstDayOfWeek
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.BusinessHours.fields.getMap();
        for(String fieldName: schemaFieldMap.keySet()) {
            
            if(fieldName == 'mondaystarttime') {
                
                mapObjHoursAgainstDayOfWeek.put('Monday',getBusinessHoursPerDay(objBusinessHour.MondayEndTime,objBusinessHour.MondayStartTime));
            }
            
            if(fieldName == 'tuesdaystarttime') {
                
                mapObjHoursAgainstDayOfWeek.put('Tuesday',getBusinessHoursPerDay(objBusinessHour.TuesdayEndTime,objBusinessHour.TuesdayStartTime));
            }
            
            if(fieldName == 'wednesdaystarttime') {
                
                mapObjHoursAgainstDayOfWeek.put('Wednesday',getBusinessHoursPerDay(objBusinessHour.WednesdayEndTime,objBusinessHour.WednesdayStartTime));
            }
            
            if(fieldName == 'thursdaystarttime') {
                
                mapObjHoursAgainstDayOfWeek.put('Thursday',getBusinessHoursPerDay(objBusinessHour.ThursdayEndTime,objBusinessHour.ThursdayStartTime));
            }
            
            if(fieldName == 'fridaystarttime') {
                
                mapObjHoursAgainstDayOfWeek.put('Friday',getBusinessHoursPerDay(objBusinessHour.FridayEndTime,objBusinessHour.FridayStartTime));
            }
        }
        
        //Get loggedIn users ContactId
        User loggedInUser = new User();
        loggedInUser = [SELECT Id, Shared_Community_Division__c, Contact.OwnerId
            FROM User
            WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        
        //fetch Meeting Till date
        Integer noOfMonths = Integer.valueOf(objBostonConfig.Boston_No_of_Month_to_Fetch_Meetings__c);
        fetchMeetingsTillDate = fetchMeetingsTillDate.addMonths(noOfMonths);
        
        //get Meeting Records
        for(Meeting__c objFetchMeeting : [  SELECT Id, Name, isAllDayMeeting__c, Start_Time__c, End_Time__c, Meeting_Request_Status__c, AssignedTo__c, Contact__c
                                            FROM Meeting__c
                                            WHERE AssignedTo__c =: loggedInUser.Contact.OwnerId
                                            AND Start_Time__c >= TODAY  
                                            AND Start_Time__c <=: fetchMeetingsTillDate
                                            AND Meeting_Request_Status__c != 'Declined']) {
                    
            if(objFetchMeeting != null) {
                
                //Get hours of Meeting Record
                DateTime startDT = objFetchMeeting.Start_Time__c;
                DateTime endDT = objFetchMeeting.End_Time__c;
                Integer tempHours = Integer.valueof((endDT.getTime() - startDT.getTime())/(60*60));
                Integer hours = Integer.valueOf(tempHours/1000);
                Date startDateFormat = date.newinstance(startDT.year(), startDT.month(), startDT.day());

                //Check date and put Total Hours agianst the Meeting date in Map     
                if(mapObjHoursAgainstDate.containsKey(startDateFormat)) {
                    
                    mapObjHoursAgainstDate.put(startDateFormat,mapObjHoursAgainstDate.get(startDateFormat) + hours);
                }else{
                    
                    mapObjHoursAgainstDate.put(startDateFormat, hours);
                }
               
                //Bind the Meeting data to the Inner class to hold data for passing to the javascript.
                MeetingWrapper myMeeting = New MeetingWrapper();
                myMeeting.title = objFetchMeeting.Name;
                myMeeting.allDay = objFetchMeeting.isAllDayMeeting__c;
                myMeeting.start = startDT.format(dtFormat);
                myMeeting.endString = endDT.format(dtFormat);
                myMeeting.ClassName = 'event-personal';
        
                //Add Inner class instance into the List
                meetings.add(myMeeting);
            }
        }
                
        //Create the NonAvailableDates List to block date slots.
        for(DateTime dt: mapObjHoursAgainstDate.keySet()) {
                    
            String getDayOfWeek = dt.addDays(1).format('EEEE');
            if(mapObjHoursAgainstDate.get(dt) >= mapObjHoursAgainstDayOfWeek.get(getDayOfWeek)) {

                String nonAvailableDateStr = dt.addDays(1).format('YYYY-MM-dd');
                nonAvailableDates.add(nonAvailableDateStr);
                mapObjAvailabilityAgainstDate.put(dt, true);
            }
        }
        
        mapDayToMeetings.put('meetings',meetings);
        mapDayToMeetings.put('nonAvailableDates', nonAvailableDates);
                
        return mapDayToMeetings;
        
    }
    
    @RemoteAction
    public static set<String> getMeetingsOfSelectedDate(string selecteddate){
        
        set<String> singleSet = new set<String>();
        
        User loggedInUser = [   SELECT Id, TimeZoneSidKey,Shared_Community_Division__c, Contact.OwnerId
                                FROM User
                                WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        List<string> dtString = selecteddate.split('T')[0].split('-');
        Date myDate = Date.newInstance(Integer.valueOf(dtString[0]), Integer.valueOf(dtString[1]), Integer.valueOf(dtString[2]));
        
        for(Meeting__c objFetchMeeting : [  SELECT Id, Name, isAllDayMeeting__c, Start_Time__c, End_Time__c, Meeting_Request_Status__c, AssignedTo__c, Contact__c
                                            FROM Meeting__c
                                            WHERE AssignedTo__c =: loggedInUser.Contact.OwnerId
                                            AND DAY_ONLY(Start_Time__c) =: myDate
                                            AND Meeting_Request_Status__c != 'Declined'
                                        ]) {
            
            singleSet.add(objFetchMeeting.Start_Time__c.format('hh:mm a', loggedInUser.TimeZoneSidKey));
            
            if((Integer.valueOf(objFetchMeeting.End_Time__c.hour()) - Integer.valueOf(objFetchMeeting.Start_Time__c.hour())) > 1 ){
                Integer addHr = 1;
                for(Integer i=Integer.valueOf(objFetchMeeting.Start_Time__c.hour()); i< Integer.valueOf(objFetchMeeting.End_Time__c.hour()); i++){
                    singleSet.add(objFetchMeeting.Start_Time__c.addHours(addHr).format('hh:mm a', loggedInUser.TimeZoneSidKey));
                    addHr = addHr+1;
                }
            }
            
            system.debug('******objFetchMeeting'+singleSet);
        }
                
        return singleSet;
    }
    
    /**
        Description: Insert the Meeting record Invoked from the Javascript. 
    */
    @RemoteAction
    global static Meeting__c insertMeeting(String objMeetingToSave) {
        
        Datetime meetingStartDate;
        MeetingRequest objMeetingRequest = New MeetingRequest();
        Map<string,integer> monthNum = new Map<string,integer>{'January' => 1,
                                                                'Febuary' => 2,
                                                                'March' => 3,
                                                                'April' => 4,
                                                                'May' => 5,
                                                                'June' => 6,
                                                                'July' => 7,
                                                                'August' => 8,
                                                                'september' => 9,
                                                                'October' => 10,
                                                                'November' => 11,
                                                                'December' => 12
        };
        
        //Get loggedIn users ContactId
        User loggedInUser = [SELECT Id,TimeZoneSidKey, Shared_Community_Division__c, Contact.OwnerId, ContactId
            FROM User
            WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        //get the deserialized Data of meeting come from the javascript.
        objMeetingRequest = (MeetingRequest)JSON.deserialize(objMeetingToSave, MeetingRequest.class);
        
        if(objMeetingRequest.Start_TimeSelected.split(' ')[1] == 'am' || (objMeetingRequest.Start_TimeSelected.split(' ')[1] == 'pm' &&
            objMeetingRequest.Start_TimeSelected.split(' ')[0] == '12:00')){
                
            meetingStartDate = Datetime.newInstance(Integer.valueOf(objMeetingRequest.Start_DateSelected.split(' ')[2]),
                                        monthNum.get(objMeetingRequest.Start_DateSelected.split(' ')[0]), 
                                        Integer.valueOf(objMeetingRequest.Start_DateSelected.split(' ')[1].replace(',','')), 
                                        Integer.valueOf(objMeetingRequest.Start_TimeSelected.split(':')[0]), 
                                        Integer.valueOf(objMeetingRequest.Start_TimeSelected.split(':')[1].split(' ')[0]), 0);
        } else {
            
            meetingStartDate = Datetime.newInstance(Integer.valueOf(objMeetingRequest.Start_DateSelected.split(' ')[2]),
                                        monthNum.get(objMeetingRequest.Start_DateSelected.split(' ')[0]), 
                                        Integer.valueOf(objMeetingRequest.Start_DateSelected.split(' ')[1].replace(',','')), 
                                        Integer.valueOf(objMeetingRequest.Start_TimeSelected.split(':')[0]) + 12, 
                                        Integer.valueOf(objMeetingRequest.Start_TimeSelected.split(':')[1].split(' ')[0]), 0);
        }
        
                                     
        DateTime meetingEndDate = meetingStartDate.addHours(1);
        
        Meeting__c objInsertMeeting = new Meeting__c();
        
        if(objMeetingRequest != null) {
            
            //Create the Instance of the Meeting Object.
            objInsertMeeting.AppointmentTopic__c = objMeetingRequest.AppointmentTopic;
            objInsertMeeting.Start_Time__c = meetingStartDate;
            objInsertMeeting.End_Time__c = meetingEndDate;
            objInsertMeeting.Contact__c = loggedInUser.ContactId;
            objInsertMeeting.AssignedTo__c = loggedInUser.Contact.OwnerId;
            objInsertMeeting.OwnerId = loggedInUser.Contact.OwnerId;
            objInsertMeeting.Name = 'Community Site Appointment Request';
            objInsertMeeting.Meeting_Request_Status__c = 'Pending';
            objInsertMeeting.Shared_Phone_Number__c = objMeetingRequest.Shared_Phone_Number;
            objInsertMeeting.Shared_Division__c = loggedInUser.Shared_Community_Division__c;
            
            //BSMI-413
            /*
            This was added as Test class was failing as the process builder-flow threw exception                
            */
            if(Test.isRunningTest()){
                
                objInsertMeeting.EventId__c = objMeetingRequest.EventId;
            } else {
                
                // to prevent test coverage failure while running test code, as Contact OWD is private
                Insert objInsertMeeting;
            }
            
            //Insert Meeting Record.
            return objInsertMeeting;
        }
        
        return null;
    }
}
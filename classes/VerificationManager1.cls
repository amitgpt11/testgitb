/**
* Manager class for the Cycle_Count_Line_Item__c SObject
*
* @Author Accenture Services
* @Date 2016-10-10
*/
///*********** Update Verificaitons on missing Serial items with 1445 
public with sharing class VerificationManager1{

	public static final String MISSING_SERIAL = 'Missing (Serial)';
	public List<Order> filteredOrder = new List<Order>();
	public Map<Id,String> SerialNumByItemIdTemp = new Map<Id,String>();
	public Map<Id,String> MaterialNumByItemIdTemp = new Map<Id,String>();
	public List<Verification__c> filteredVerifications = new List<Verification__c>();
	public Map<Id,Cycle_Count_Line_Item__c> ItemsbyId = new Map<Id,Cycle_Count_Line_Item__c>();
	
	public void fetchVerificaitonofMissingItem(List<Verification__c> newVerifications){
		Set<Id> ItemIds = new Set<Id>();
		//Map<Id,Cycle_Count_Line_Item__c> ItemsbyId = new Map<Id,Cycle_Count_Line_Item__c>();
		Map<Verification__c,Cycle_Count_Line_Item__c> verificationBymissingItem = new Map<Verification__c,Cycle_Count_Line_Item__c>();
		
		
		
		For(Verification__c vnew:newVerifications){
            system.debug('Verification##==> '+vnew);
			ItemIds.add(vnew.Cycle_Count_Line_Item__c);
			system.debug('item ids==>'+vnew.Cycle_Count_Line_Item__c);
            system.debug('@@@@@@@@'+ItemIds);
            
		}
		if(!ItemIds.isEmpty()){
            
			for(Cycle_Count_Line_Item__c Item :[Select Id, Inventory_Item__c,Inventory_Item__r.Model_Number__r.UPN_Material_Number__c,Inventory_Item__r.Serial_Number__c,Cycle_Count_Response_Analysis__r.OwnerId,Exception_Reason__c From Cycle_Count_Line_Item__c where Exception_Reason__c =: MISSING_SERIAL AND Id in:ItemIds]){
				system.debug('Items found=='+Item);
                ItemsbyId.put(item.id,item);
			} 
			system.debug('ItemsbyId ==>'+ItemsbyId.keyset());
		}
		
		For(Verification__c vnew:newVerifications){
			if(ItemsbyId.containsKey(vnew.Cycle_Count_Line_Item__c)){
				verificationBymissingItem.put(vnew,ItemsbyId.get(vnew.Cycle_Count_Line_Item__c));
				filteredVerifications.add(vnew);
				system.debug('filteredVerifications==>'+filteredVerifications);
				SerialNumByItemIdTemp.put(vnew.Cycle_Count_Line_Item__c,ItemsbyId.get(vnew.Cycle_Count_Line_Item__c).Inventory_Item__r.Serial_Number__c);
				MaterialNumByItemIdTemp.put(vnew.Cycle_Count_Line_Item__c,ItemsbyId.get(vnew.Cycle_Count_Line_Item__c).Inventory_Item__r.Model_Number__r.UPN_Material_Number__c);
				
			}
		}	
		system.debug('Size of serial ids==>'+SerialNumByItemIdTemp.values());
		system.debug('Size of serial ids==>'+MaterialNumByItemIdTemp.values());
		/*if(SerialNumByItemIdTemp.values().size()>0 && MaterialNumByItemIdTemp.values().size()>0){
			fetchOrderswithCriterion(SerialNumByItemIdTemp,MaterialNumByItemIdTemp);
		}*/
	}

	//** Fetch Related Orders
	public void fetchOrderswithCriterion()
	{
		filteredOrder = [Select Id,Shared_SAP_Order_Number__c,OrderNumber,RecordTypeId,Stage__c,Shipping_Location__c,Shipping_Location__r.Inventory_Data_ID__r.Account__r.Personnel_Id__r.Personnel_ID__c,Shipping_Location__r.Inventory_Data_ID__r.OwnerId,createdDate,(SELECT ID,Item_Status__c,Material_Number_UPN__c,Serial_Number__c from OrderItems where Material_Number_UPN__c IN: MaterialNumByItemIdTemp.values() AND Serial_Number__c IN: SerialNumByItemIdTemp.Values()) From Order Where RecordTypeId In: OrderManager.RECTYPES_NMD_FOR_VERIFICATION AND Stage__c != 'New' ORDER BY createdDate DESC];
		/*for(Order o:[Select Id,Shared_SAP_Order_Number__c,OrderNumber,RecordTypeId,Stage__c,Shipping_Location__c,Shipping_Location__r.Inventory_Data_ID__r.Account__r.Personnel_Id__c,Shipping_Location__r.Inventory_Data_ID__r.Account__r.Personnel_Id__r.Personnel_ID__c,Shipping_Location__r.Inventory_Data_ID__r.OwnerId, (SELECT ID,Item_Status__c,Material_Number_UPN__c,Serial_Number__c from OrderItems where Material_Number_UPN__c IN: MaterialNumByItemIdTemp.values() And Serial_Number__c IN: SerialNumByItemIdTemp.Values()) From Order Where RecordTypeId In: OrderManager.RECTYPES_NMD_FOR_VERIFICATION AND Stage__c !='New' ORDER BY createdDate DESC]){
			filteredOrder.add(o);
		}*/
		
		system.debug('filteredOrder==>'+filteredOrder);	
		//system.debug('filteredOrder.orderItems==>'+filteredOrder.orderItems);	
	}
	//**
	public void updateFileredVerificaitons(List<Verification__c> newVerifications){
		
		if(filteredVerifications.size()>0){
			for(Verification__c v1 : newVerifications){
				system.debug('ItemsbyId.containsKey(v1.Cycle_Count_Line_Item__c ==>'+ItemsbyId.get(v1.Cycle_Count_Line_Item__c));
				if(ItemsbyId.containsKey(v1.Cycle_Count_Line_Item__c)){
					system.debug('Verificaiton==>'+v1);
					OrderRecord finalOrder =findRecentMatchingOrder(v1);
					system.debug('finalOrder==>'+finalOrder	);
					if(finalOrder.recordTypeId == OrderManager.RECTYPE_PATIENT ){
						v1.Verification_Tracking__c = finalOrder.OrderNumber;
						system.debug('Verificaiton==>'+v1);
						v1.Reconcile_Type__c = 'Used in Procedure';  
						v1.Verification__c = 'Used in Surgery';
						v1.Is_Auto_Updated__c = true;
						
					}
					else if(finalOrder.RecordTypeId==OrderManager.RECTYPE_TRANSFER){
						v1.Verification_Tracking__c = finalOrder.OrderNumber;
						v1.Reconcile_Type__c = 'Transfer';
						v1.Verification__c = 'Rep Transfer';
						v1.Is_Auto_Updated__c = true;
						  
					}
					else if(finalOrder.RecordTypeId == OrderManager.RECTYPE_RETURN)
					{
						system.debug('Order ==>'+finalOrder.OrderNumber);
						v1.Verification_Tracking__c = finalOrder.OrderNumber;
						v1.Reconcile_Type__c = 'RMA';
						v1.Verification__c = 'Returned on RMA';
						v1.Is_Auto_Updated__c = true;	
						  
					}
				}
			}
		}
		
	}
	
	public OrderRecord findRecentMatchingOrder(Verification__c v1){
		List<Order> olist = new List<Order>();
		OrderRecord finalOrder = new OrderRecord();
		
		List<OrderRecord> orderRecords = new List<OrderRecord>();
		//olist=ordersOfVerification;
		///*********
		for(Order o:filteredOrder){
              //if(o.Stage__c != 'New')
                system.debug('Order ==>'+o);  
            
              List<OrderItem> oiList = new List<OrderItem>();
              oiList=o.OrderItems;
			  system.debug('oiList'+oiList);
              for(OrderItem oItem:oiList){
					
                  system.debug('shiping lo=='+o.Shipping_Location__r.Inventory_Data_ID__r.OwnerId);
                  system.debug('item owner=='+ItemsbyId.get(v1.Cycle_Count_Line_Item__c).Cycle_Count_Response_Analysis__r.OwnerId);
                  //&& !finalUpdatedVerificaiton.contains(v)
                  if((oItem.Material_Number_UPN__c != null && oItem.Material_Number_UPN__c==MaterialNumByItemIdTemp.get(v1.Cycle_Count_Line_Item__c) && oItem.Serial_Number__c ==SerialNumByItemIdTemp.get(v1.Cycle_Count_Line_Item__c) && oItem.Serial_Number__c !=null) && o.Shipping_Location__c != Null && o.Shipping_Location__r.Inventory_Data_ID__r.OwnerId == ItemsbyId.get(v1.Cycle_Count_Line_Item__c).Cycle_Count_Response_Analysis__r.OwnerId )
                  {
					  system.debug('Order==>'+o);
					if(o.RecordTypeId==OrderManager.RECTYPE_PATIENT && oItem.Item_Status__c !='Explanted'){
						olist.add(o);
					}
					else if(o.RecordTypeId==OrderManager.RECTYPE_TRANSFER || o.RecordTypeId==OrderManager.RECTYPE_RETURN)
					{
						olist.add(o);
					}
				  }
			    }
		    }	  
		///************
		if(olist.size()>0){
			For(Order o:olist){
				OrderRecord tempOr= new OrderRecord(o.Id,o.createdDate,o.recordTypeId,o.OrderNumber);
				orderRecords.add(tempOr);
			}
		}	
		if(orderRecords.size()>0){
			orderRecords.sort();
			finalOrder= orderRecords[0];
		}
		system.debug('Recent Order==>'+finalOrder);
		return finalOrder;
	}
	
	
	
	//****Comprable class below to find most recent order
	public  class OrderRecord implements Comparable{
        public Id recordId;
        public DateTime createdDate;
        public Id recordTypeId;
		public String OrderNumber;
    
        public OrderRecord(){
            recordId= null;
            createdDate=null;
            recordTypeId=null;
			OrderNumber='';
        }
    
        //Constructor
        public OrderRecord(Id recordId, DateTime createdDate, Id recordTypeId,String OrderNumber){
            this.recordId = recordId;
            this.createdDate= createdDate;
            this.recordTypeId=recordTypeId;
			this.OrderNumber = OrderNumber;
        }
        
        public Integer compareTo(Object compareTo) {
            OrderRecord compareToOpty = (OrderRecord)compareTo;
            if (createddate == compareToOpty.createddate) return 0;
            if (createddate > compareToOpty.createddate) return -1;
            return 1;
        }
    
	}
}
/**
* Boston Scientific does not have all the relationships of Accounts to 
* the reps in one single source system. The data is being pulled in 
* manually from SAP and some data will be coming in from Global Sales 
* Reporting(GSR). This data will be merged together in Salesforce in the 
* Personnel_ID and Account_Team_Member__c Objects which will have the 
* mapping of the user to the Account Team and the Role of the User for 
* that Account.
* 
* Once the info is updated in the Personnel_ID object, the Account Teams 
* should be updated using the mapping of Account to User and it's Role in 
* Account Teams.
*
* Access to Accounts should be Read/Write, and access to all other objects should be None (no access)
*
* @Author salesforce Services
* @Date 2015-06-11
*/
global without sharing class AccountTeamMembersBatch implements Database.Batchable<SObject>, Schedulable {

    // holds Role Rankings. ie
    // 'Area Manager' => 3,
    // 'Regional Manager' => 2
    // 'Territory Manager' => 1
    static final Map<String,Integer> roleRanks;

    static {

        // populate Role Rankings.  The rank is inferred by the order of the picklist values.
        roleRanks = new Map<String,Integer>{};

        List<Schema.PicklistEntry> ples = Schema.SObjectType.Account_Team_Member__c.fields.Account_Team_Role__c.getPicklistValues();
        Integer index = ples.size();
        for(Schema.PicklistEntry ple : ples) {
            // invert the index from high to low, so that the first values have higher rankings
            roleRanks.put(ple.getValue(), index--);
        }

    }

    // static entry point
    //>AccountTeamMembersBatch.runNow();
    public static void runNow() {
        new AccountTeamMembersBatch().execute(null);
    }

    private static Integer getRoleRanking(String role) {
        // a valid role will always have a rank > 0
        return roleRanks.containsKey(role) ? roleRanks.get(role) : 0;
    }
    
    global String query =
        'select ' +
            'Account_Team_Role__c, ' +
            'Personnel_ID__r.User_for_account_team__c, ' +
            'Personnel_ID__r.User_for_account_team__r.IsActive, ' +
            'Account_Team_Share__c, ' +
            'Deleted__c ' +
        'from Account_Team_Member__c ' +
        'where Account_Team_Role__c != null ' +
        'and Account_Team_Share__c != null ' +
        'and Personnel_ID__r.User_for_account_team__c != null ';

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }

    global void execute(Database.BatchableContext bc, List<Account_Team_Member__c> members) {

        // get unique sets of accounts and users
        Map<Id,Map<Id,AccountTeamMember>> atmsMap = new Map<Id,Map<Id,AccountTeamMember>>{};
        Set<Id> userIds = new Set<Id>{};
        for (Account_Team_Member__c member : members) {
            atmsMap.put(member.Account_Team_Share__c, new Map<Id,AccountTeamMember>{});
            userIds.add(member.Personnel_ID__r.User_for_account_team__c);
        }

        // fetch AccountTeamMembers and map them by their parent account and user
        for (AccountTeamMember atm : [
            select
                AccountAccessLevel,
                AccountId,
                TeamMemberRole,
                UserId
            from AccountTeamMember
            where AccountId in :atmsMap.keySet()
            and UserId in :userIds
        ]) {
            // the first key is guaranteed to exist
            // atmsMap: { AccountId => { UserId => AccountTeamMember } }
            atmsMap.get(atm.AccountId).put(atm.UserId, atm);
        }

        // Account Team Member records to create
        List<AccountTeamMember> newATMs = new List<AccountTeamMember>{};
        // Account Team Member records to remove
        List<AccountTeamMember> oldATMs = new List<AccountTeamMember>{};

        // compare each set, the custom object is the master
        for (Account_Team_Member__c member : members) {

            //get list of existing members for the parent account
            Map<Id,AccountTeamMember> atms = atmsMap.get(member.Account_Team_Share__c);
            if (atms.containsKey(member.Personnel_ID__r.User_for_account_team__c)) {

                AccountTeamMember atm = atms.get(member.Personnel_ID__r.User_for_account_team__c);
                Integer currentRank = getRoleRanking(atm.TeamMemberRole);
                Integer updatedRank = getRoleRanking(member.Account_Team_Role__c);

                if (member.Deleted__c) {
                    // remove team member if deleted and exists, and is the role is of >= ranking
                    if (updatedRank >= currentRank) {
                        oldATMs.add(atm);
                    }
                }
                else if (updatedRank > currentRank && member.Personnel_ID__r.User_for_account_team__r.IsActive) {
                    // if the existing is a lower rank then update it.
                    atm.TeamMemberRole = member.Account_Team_Role__c;
                    newATMs.add(atm);
                }
            }
            else if (!member.Deleted__c && member.Personnel_ID__r.User_for_account_team__r.IsActive) {
                // create if user does not exist
                newATMs.add(new AccountTeamMember(
                    AccountId = member.Account_Team_Share__c,
                    UserId = member.Personnel_ID__r.User_for_account_team__c,
                    TeamMemberRole = member.Account_Team_Role__c
                ));
            }
        }

        DML.deferLogs();

        // remove any flagged team members
        if (!oldATMs.isEmpty()) {

            Set<Id> acctIds = new Set<Id>{};
            Set<Id> userIds2 = new Set<Id>{};
            Set<String> acctUserCombo = new Set<String>{};
            for (AccountTeamMember atm : oldATMs) {
                acctIds.add(atm.AccountId);
                userIds2.add(atm.UserId);
                // create a custom unique key of AccountId + UserId
                acctUserCombo.add(atm.AccountId + '' + atm.UserId);
            }

            //delete oldATMs;
            DML.remove(this, oldATMS, false);

            // find and remove old shares
            List<AccountShare> acctShares = new List<AccountShare>{};
            for (AccountShare share : [
                select
                    AccountId,
                    UserOrGroupId
                from AccountShare
                where AccountId in :acctIds
                and UserOrGroupId in :userIds2
            ]) {
                // because two arrays are being used in the soql criteria, it's possible that
                // false matches were returned.  so double-check the AccountId/UserId combo
                // to see that it is truly a record mate to a removed team member.
                if (acctUserCombo.contains(share.AccountId + '' + share.UserOrGroupId)) {
                    acctShares.add(share);
                }
            }

            if (!acctShares.isEmpty()) {
                //delete acctShares;
                DML.remove(this, acctShares, false);
            }

        }

        // add any new team members
        if (!newATMs.isEmpty()) {

            // create corresponding shares to the new team members
            List<AccountShare> acctShares = new List<AccountShare>{};
            for (AccountTeamMember atm : newATMs) {
                acctShares.add(new AccountShare(
                    AccountId = atm.AccountId,
                    UserOrGroupId = atm.UserId,
                    AccountAccessLevel = 'Edit',
                    OpportunityAccessLevel = 'None'
                ));
            }

            //upsert newATMs;
            DML.evaluateResult(this, Database.upsert(newATMs, false));
            //insert acctShares;
            DML.save(this, acctShares, false);

        }

        DML.flushLogs(false);

    }

    global void execute(SchedulableContext sc) {
        
        String className = String.valueOf(this).split(':')[0];
        Integer runAllCount = 0;
        Integer runSameCount = 0;

        // check for any other batches that may be running
        for (AsyncApexJob job : [
            select ApexClass.Name
            from AsyncApexJob
            where Status in ('Preparing', 'Processing')
            and JobType in ('BatchApex', 'BatchApexWorker')
        ]) {
            runAllCount++;
            if (job.ApexClass.Name == className) {
                runSameCount++;
            }
        }

        // only run if there is an available slot, and if no other of the same kind are
        if (runAllCount < 5 && runSameCount == 0) {
            Database.executeBatch(this, 2000);
        }

    }

    global void finish(Database.BatchableContext sc) {
        //
    }

}
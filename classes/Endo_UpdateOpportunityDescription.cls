/*
 * User Story: SFDC-11/715, AC1: Need to update opportunity descriptions for all GI/PULM - All under ENUS, When Data come from HANA fro Territoy Model (Custom Object).
 * Created By: Shashank Gupta (Accenture Team)
 * Creatd Date: 5th Dec, 2016
 * Schedulable class : ScheduleEndo_UpdateTerritoryDescription
*/
global class Endo_UpdateOpportunityDescription implements Database.Batchable<sObject>
{
    public String UltimateParentTerritory = System.Label.ENUSTerritory;
    public string str = '\'%'+UltimateParentTerritory+'%\'';
    public string str1 = '\''+UltimateParentTerritory+'\'';
    public String query = 'SELECT Id,Description,DeveloperName,LastModifiedDate,Name,Endo_Territory__c,ParentTerritory2Id,ParentTerritory2.Description,ParentTerritory2.ParentTerritory2Id,ParentTerritory2.ParentTerritory2.Description, ParentTerritory2.Name,ParentTerritory2.ParentTerritory2.Name,ENDO_Territory_Description__c,No_Of_Parents_7_To_10__c,No_Of_Parents_1_To_6__c FROM Territory2 Where No_Of_Parents_1_To_6__c LIKE  '+str+ ' OR Id = '+str1;
    
    public string EndoCapitalBusinessRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Endo Capital Business').getRecordTypeId();
    public string EndoNewDisposableBusinessRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Endo New Disposable Business').getRecordTypeId();
    
    public Map<string, Territory2> territoryIdMap = new Map<string, Territory2>();
    public Map<string, Territory2> territoryNameMap = new Map<string, Territory2>();
    
    public Set<string> territoryIdSet = new Set<string>();
    public Set<string> territoryNameSet = new Set<string>();

    public List<Opportunity> oppList = new List<Opportunity>();
    public Set<Opportunity> oppSet = new Set<Opportunity>();
    public List<Opportunity> oppListToCommit = new List<Opportunity>();
        
    
    // Constructor
    global Endo_UpdateOpportunityDescription ()
    {} 
    /****************************************************************
*  start(Database.BatchableContext BC)
*****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('Inside Strat method');
        system.debug('str'+str);
        system.debug('Query'+query);        
        return Database.getQueryLocator(this.query);
    } 
    
    /*********************************************************************
*  execute(Database.BatchableContext BC, List scope)
**********************************************************************/
    global void execute(Database.BatchableContext BC, List<Territory2> scope)
    {                
       system.debug('**InTrue**'+scope.size());
       
        for(Territory2 tm: scope){
            territoryIdMap.put(tm.Id, tm);
            territoryNameMap.put(tm.Name, tm);
            
            territoryIdSet.add(tm.Id);
            territoryNameSet.add(tm.Name);
                
        }
        system.debug('territoryIdMap--> '+territoryIdMap.size());
        system.debug('territoryNameMap--> '+territoryNameMap.size());
        
        //if((!territoryIdMap.isEmpty() && territoryIdMap != null) || (!territoryNameMap.isEmpty() && territoryNameMap != null)){
        if(territoryIdSet.size() > 0 || territoryNameSet.size() > 0){
            oppList = [Select Id,RecordTypeId , Name, Territory2Id, Territory2.Name, GI_Parent__c, GI_Parent_Parent__c, Parent_Of_Secondary_Territory__c, 
                              PULM_Parent__c, PULM_Parent_Parent__c, ENDO_Secondary_Territory__c, Territory2.ParentTerritory2Id, Territory2.ParentTerritory2.Description,
                              Territory2.ParentTerritory2.ParentTerritory2Id, Territory2.ParentTerritory2.ParentTerritory2.Description,Territory2.Description,
                              Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2.Description,GI_Region_Description__c,GI_Area_Description__c,Territory2.ParentTerritory2.Name,
                              SecondaryTerritoryDescription__c,SecondaryTerritoryRegion__c,Secondary_Territory_Area__c,Territory2.ParentTerritory2.ParentTerritory2.Name
                              From Opportunity 
                              Where (RecordTypeId =:EndoCapitalBusinessRecordTypeId OR RecordTypeId =:EndoNewDisposableBusinessRecordTypeId) AND
                                    (Territory2Id =:territoryIdSet OR Territory2.ParentTerritory2Id =:territoryIdSet
                                    OR Territory2.ParentTerritory2.ParentTerritory2Id =:territoryIdSet
                                    OR ENDO_Secondary_Territory__c =:territoryNameSet OR PULM_Parent__c =:territoryNameSet OR PULM_Parent_Parent__c =:territoryNameSet)];    
            system.debug('********oppList********'+oppList.size());
        }
        
        if(!oppList.isEmpty() && oppList != null){       
            for(Opportunity op : oppList){                 
                           
                if(territoryIdMap.containsKey(op.GI_Parent__c)){
                    op.GI_Region_Description__c = territoryIdMap.get(op.GI_Parent__c).Description;
                }
                if(territoryIdMap.containsKey(op.GI_Parent_Parent__c)){
                    op.GI_Area_Description__c = territoryIdMap.get(op.GI_Parent_Parent__c).Description;
                }
                if(territoryNameMap.containsKey(op.ENDO_Secondary_Territory__c)){
                    op.SecondaryTerritoryDescription__c = territoryNameMap.get(op.ENDO_Secondary_Territory__c).Description;
                }
                if(territoryNameMap.containsKey(op.PULM_Parent__c)){
                    op.SecondaryTerritoryRegion__c = territoryNameMap.get(op.PULM_Parent__c).Description;
                }
                if(territoryNameMap.containsKey(op.PULM_Parent_Parent__c)){
                    op.Secondary_Territory_Area__c = territoryNameMap.get(op.PULM_Parent_Parent__c).Description;
                }   
                oppSet.add(op);
                                    
            }
            if(oppSet.size() > 0){
                oppListToCommit.addAll(oppSet);  
            }                       
        }
        system.debug('oppListToCommit'+oppListToCommit);
        
        if(!oppListToCommit.isEmpty() && oppListToCommit != null){
            DML.evaluateResults(this, Database.update(oppListToCommit, false));
            oppListToCommit.clear();
        }

    }
    
    /****************************************************
*  finish(Database.BatchableContext BC)
*****************************************************/
    
    global void finish(Database.BatchableContext BC) 
    {     
        system.debug('Inside Finish method');
    }
}
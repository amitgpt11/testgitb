/**
* Batch class, update Inventory Items per their matching QN Add/Delete List(s)
*
* @Author salesforce Services
* @Date 2015-06-03
*/
global without sharing class NMD_QNProductsBatch implements Database.Batchable<SObject>, Schedulable {

    // QN Types
    final static String QN_ADD = 'QN Add';
    final static String QN_DEL = 'QN Delete';

    // Dispositions
    final static String AVAILABLE = 'Available';
    final static String IN_TRANSIT = 'In Transit';
    
    // Schedule Name
    final static String CRONJOB_NAME = 'NMD_QNProductsBatch - Hourly';
    
    // Scope size
    final static Integer BATCH_SIZE = 100; // intentionally kept at half of default scope, as each record may affect >1 record
    
    // Quality Notification List__c Record Type
    @TestVisible
    final static Id RECTYPE_QN = (Schema.SObjectType.List__c.getRecordTypeInfosByName().get('Quality Notification').getRecordTypeId());

    /**
    * Static entry point, ie: NMD_QNProductsBatch.createHourlySchedule();
    */
    public static Id createHourlySchedule() {

        Integer count = [
            select count() 
            from CronJobDetail
            where Name = :CRONJOB_NAME
            and JobType = '7' // scheduled apex
        ];

        Id scheduleId;

        if (count == 0) {
            // create a schedule to run hourly
            scheduleId = System.schedule(CRONJOB_NAME, '0 0 * * * ?', new NMD_QNProductsBatch());
            System.debug('Cron Job created: ' + scheduleId);
            
        }
        else {
            System.debug('Cron Job already scheduled.');
        }

        return scheduleId;

    }

    global String query = 
        'select ' +
            'Lot_Number__c, ' +
            'Model_Number__c, ' +
            'Serial_Number__c, ' +
            'Type__c ' +
        'from List__c ' +
        'where RecordTypeId = :RECTYPE_QN ' +
        'and Model_Number__c != null ' +
        'and Type__c in (:QN_ADD, :QN_DEL) ';

    global NMD_QNProductsBatch() {
        if (Test.isRunningTest()) {
            query += 'order by CreatedDate desc limit ' + BATCH_SIZE;
        }
    }

    /**
    *  @desc    Database.Batchable method, returns the next record set
    */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }

    /**
    *  @desc    Database.Batchable method, executes the main logic per batch
    */
    global void execute(Database.BatchableContext bc, List<List__c> scope) {

        // what are the relevant codes
        Set<Id> modelNumbers = new Set<Id>();
        Set<String> lotNumbers = new Set<String>();
        Set<String> serialNumbers = new Set<String>();
        // do work before hand to store the QN Type (Add or Delete)
        Map<Id,List__c> prodQNs = new Map<Id,List__c>();
        Map<String,List__c> lotQNs = new Map<String,List__c>();
        Map<String,List__c> serialQNs = new Map<String,List__c>();
        Map<String,List__c> bothQNs = new Map<String,List__c>();

        // for each list, find matching Inventory Items and Products
        List<List__c> removals = new List<List__c>();
        for (List__c l : scope) {

            // all lists will have a model number per the batch query
            modelNumbers.add(l.Model_Number__c);

            if (l.Type__c == QN_DEL) {
                removals.add(l);
            }

            if (String.isNotBlank(l.Lot_Number__c)) {
                lotQNs.put(l.Model_Number__c + l.Lot_Number__c, l);
                lotNumbers.add(l.Lot_Number__c);
            }

            if (String.isNotBlank(l.Serial_Number__c)) {
                serialQNs.put(l.Model_Number__c + l.Serial_Number__c, l);
                serialNumbers.add(l.Serial_Number__c);
            }

            if (String.isBlank(l.Lot_Number__c) && String.isBlank(l.Serial_Number__c)) {
                prodQNs.put(l.Model_Number__c, l);
            }

        }

        // update the relevant product records to reflect the matching List
        if (!prodQNs.isEmpty()) {

            List<Product2> prods = new List<Product2>();
            for (Product2 prod : [
                select QN__c
                from Product2
                where Id in :prodQNs.keySet()
            ]) {
                prod.QN__c = prodQNs.get(prod.Id).Type__c == QN_ADD;
                prods.add(prod);
            }
            update prods;

        }

        // update the relevant inventory items to reflect the matching List
        if (!lotNumbers.isEmpty() || !serialNumbers.isEmpty()) {

            List<Inventory_Item__c> items = new List<Inventory_Item__c>();
            for (Inventory_Item__c item : [
                select
                    Is_QN_Lot__c,
                    Lot_Number__c,
                    Model_Number__c,
                    Serial_Number__c
                from Inventory_Item__c
                where RecordTypeId = :InventoryItemManager.RECTYPE_NMD
                and Disposition__c in (:AVAILABLE, :IN_TRANSIT)
                and Model_Number__c in :modelNumbers
                and (Lot_Number__c in :lotNumbers or Serial_Number__c in :serialNumbers)
            ]) {

                Boolean hasUpdate = false;

                if (String.isNotBlank(item.Lot_Number__c)
                 && lotQNs.containsKey(item.Model_Number__c + item.Lot_Number__c)
                ) {
                    List__c l = lotQNs.get(item.Model_Number__c + item.Lot_Number__c);
                    if (String.isBlank(l.Serial_Number__c)) {
                        item.Is_QN_Lot__c = l.Type__c == QN_ADD;
                        hasUpdate = true;
                    }
                }

                if (String.isNotBlank(item.Serial_Number__c) && serialQNs.containsKey(item.Model_Number__c + item.Serial_Number__c)) {
                    List__c l = serialQNs.get(item.Model_Number__c + item.Serial_Number__c);
                    item.Is_QN_Serial__c = l.Type__c == QN_ADD;
                    hasUpdate = true;
                }

                if (hasUpdate) {
                    items.add(item);
                }

            }

            if (!items.isEmpty()) {
                update items;
            }

        }

        // remove the list records
        delete removals;

    }

    /**
    *  @desc    Schedulable method, initiates the Apex Job
    */
    global void execute(SchedulableContext context) {

        String className = String.valueOf(this).split(':')[0];
        Integer runAllCount = 0;
        Integer runSameCount = 0;

        // check for any other batches that may be running
        for (AsyncApexJob job : [
            select ApexClass.Name
            from AsyncApexJob
            where Status in ('Preparing', 'Processing')
        ]) {
            runAllCount++;
            if (job.ApexClass.Name == className) {
                runSameCount++;
            }
        }

        // check if there are any List__c records that can be evaluated.
        // if none exist then there is no reason for the batch to run.
        Boolean hasLists = 0 != [
            select count()
            from List__c
            where RecordTypeId = :RECTYPE_QN
            limit 1
        ];

        // only run if there are lists, there is an available slot, and if no other of the same kind are
        if (hasLists && runAllCount < 5 && runSameCount == 0) {
            Database.executeBatch(new NMD_QNProductsBatch(), BATCH_SIZE);
        }

    }

    /**
    *  @desc    Database.Batchable method, runs when the batch finishes
    */
    global void finish(Database.BatchableContext bc) {}
    
}
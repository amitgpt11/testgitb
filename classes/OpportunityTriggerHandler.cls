/**
* Test class for the Opportunity trigger
*
* @Author salesforce Services
* @Date 2015/04/22
*
*
* @ModifiedDate 16JUNE2016
* @author Mike Lankfer
* @description added before insert call to the AutoAssignOpportunityPricebook.assignPricebook method
*/
public with sharing class OpportunityTriggerHandler extends TriggerHandler {
    
    //final OpportunityManager manager = new OpportunityManager();
    final OpportunityManager manager = OpportunityManager.getInstance();
    final AssignTerritorytoOpportunityHandlerBulk oppyTerritory = new AssignTerritorytoOpportunityHandlerBulk();
    final UtilForAssignTerritorytoOpportunity utilAssignTerritory = new UtilForAssignTerritorytoOpportunity();
    final AutoAssignOpportunityPricebook pricebookAssignment = new AutoAssignOpportunityPricebook();  //added 16JUNE2016 - Mike Lankfer
    private static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();    
    private static final Id RECTYPE_NMD_COOL_OPTY = RECTYPES.get('NMD Cool').getRecordTypeId();
    private static final Id RECTYPE_NMD_REPROGRAMMING_OPTY = RECTYPES.get('NMD Reprogramming').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_EXPLANT_OPTY = RECTYPES.get('NMD SCS Explant').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_IMPLANT_OPTY = RECTYPES.get('NMD SCS Implant').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_REVISION_OPTY = RECTYPES.get('NMD SCS Revision').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_TRIAL_OPTY = RECTYPES.get('NMD SCS Trial').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_TRIAL_IMPLANT_OPTY = RECTYPES.get('NMD SCS Trial-Implant').getRecordTypeId();
    private static final Id RECTYPE_ENDO_DISPOSABLE_OPTY = RECTYPES.get('Endo New Disposable Business').getRecordTypeId();
    private static final Id RECTYPE_ENDO_CAPITAL_OPTY = RECTYPES.get('Endo Capital Business').getRecordTypeId();
    
    
    //CreateRevenueDetailonOpptyCrtnController revenue = new CreateRevenueDetailonOpptyCrtnController(); /**Shashank Gupta SFDC-714**on 8thSep,2016**/
    
    private static Set<Id> NMD_ALL_OPTY = new Set<Id>{
        RECTYPE_NMD_COOL_OPTY, RECTYPE_NMD_REPROGRAMMING_OPTY, RECTYPE_NMD_SCS_EXPLANT_OPTY,
        RECTYPE_NMD_SCS_IMPLANT_OPTY, RECTYPE_NMD_SCS_REVISION_OPTY, RECTYPE_NMD_SCS_TRIAL_OPTY, 
        RECTYPE_NMD_SCS_TRIAL_IMPLANT_OPTY
    };
     /*
    * LastMadification:
    *   8/24/2016 For Too Many Soql:
    *       Added lines manager.fetchCurrentProfileName(),manager.fetchTerritories(opptys) in trigger.isinsert if
    */
    public override void bulkBefore() { 
        
        List<Opportunity> nonNMDOptys = new List<Opportunity>((List<Opportunity>)Trigger.new);
        List<Opportunity> oldopptys = (List<Opportunity>)trigger.old;
        //string ENDOUsrRoles = 'US Endo - East PULM Region,US Endo - Midwest PULM Region,US Endo - Northeast PULM Region,US Endo-PULM,US Endo - Southwest PULM Region,US Endo - West PULM Region,US Endo - East PULM Region - East PULM TM,US Endo - Midwest PULM Region - Midwest PULM TM,  US Endo - Northeast PULM Region - Northeast PULM TM,US Endo - Southwest PULM Region - Southwest PULM TM,US Endo - West PULM Region - West PULM TM';
        //list<string> rolesLst = ENDOUsrRoles.SPLIT(',');
        //set<string> ENDORoleSet = new set<string>();
        //ENDORoleSet.addAll(rolesLst);
        //string userRole = UserInfo.getUserRoleId();
        
       for(Integer i = (nonNMDOptys.size() - 1); i >= 0; i--){
            if(NMD_ALL_OPTY.contains(nonNMDOptys.get(i).RecordTypeId)){
                nonNMDOptys.remove(i);
            }
        } 
       
        
        List<Opportunity> opptys = (List<Opportunity>)trigger.new;
        manager.FetchOpptyOwnerList(opptys);/**Shashank Gupta SFDC-1431**on 21stSep,2016**/
        List<Opportunity> opptys1 = new List<Opportunity>(); 
        List<Opportunity> opptysFrSecTUpdate = new List<Opportunity>(); 
        if(trigger.isDelete){
            manager.fetchCurrentProfileName();
            manager.fetchOpportunitiesWithOrders(trigger.oldMap.keyset());
        } else if(trigger.isInsert){
            manager.fetchCurrentProfileName();
            manager.fetchPhysicianAccounts(opptys);
            
            oppyTerritory.assignTerritoryForOpportunity(true);            
            pricebookAssignment.assignPricebook(nonNMDOptys);  //added 16JUNE2016 - Mike Lankfer
           
            manager.fetchOpptyAccounts(opptys);  // **added SFDC-1364 -Amitabh
                        
            //US2528 --> added 13JUL2016 - Mayuri --- 
            list<Opportunity> optLst = new list<Opportunity>();
            
            for(Opportunity o: opptys){
                
                if(o.recordTypeId != null && (o.recordTypeId == RECTYPE_ENDO_DISPOSABLE_OPTY || o.recordTypeId == RECTYPE_ENDO_CAPITAL_OPTY)){
                        optLst.add(o);
                } 
            }
            system.debug(optLst.size()+'-1234@@');
            if(optLst.size() >0){
                system.debug('test-->');
                utilAssignTerritory.assignENDOSTerritory(optLst,false);
            }
            //US2528 ends ----
            
        } else if(trigger.isUpdate){
            manager.fetchPhysicianAccounts(nonNMDOptys);
            manager.FetchOpptyTerritoryandOpptyTeamList(oldopptys);
            
            for(Opportunity o: opptys){
                Id oldOwnerId = ((Opportunity)Trigger.oldMap.get(o.Id)).OwnerId;
                Id oldAccountId = ((Opportunity)Trigger.oldMap.get(o.Id)).AccountId;
                if(o.OwnerId != ((Opportunity)Trigger.oldMap.get(o.Id)).OwnerId || o.AccountId != ((Opportunity)Trigger.oldMap.get(o.Id)).AccountId){
                // Added - 715 - If owner of oppy changed and only endo oppy 
                    if(o.recordTypeId != RECTYPE_ENDO_DISPOSABLE_OPTY && o.recordTypeId != RECTYPE_ENDO_CAPITAL_OPTY){
                        opptys1.add(o);
                    }
                }
                //US2528 --> added 13JUL2016 - Mayuri --- 
                //if(o.Territory2Id !=((Opportunity)Trigger.oldMap.get(o.Id)).Territory2Id || o.ENDO_Secondary_Territory__c !=((Opportunity)Trigger.oldMap.get(o.Id)).ENDO_Secondary_Territory__c || o.AccountId != ((Opportunity)Trigger.oldMap.get(o.Id)).AccountId || o.OwnerId != ((Opportunity)Trigger.oldMap.get(o.Id)).OwnerId && o.recordTypeId != null && (o.recordTypeId == RECTYPE_ENDO_DISPOSABLE_OPTY || o.recordTypeId == RECTYPE_ENDO_CAPITAL_OPTY)){
                    // changed to handle territory flag on endo opportunity
                    if(o.recordTypeId != null && (o.recordTypeId == RECTYPE_ENDO_DISPOSABLE_OPTY || o.recordTypeId == RECTYPE_ENDO_CAPITAL_OPTY)){
                    opptysFrSecTUpdate.add(o);
                    }
                //}               
            }
            if(opptys1.size()>0 && opptys1 != null)
                utilAssignTerritory.utilassignTerritoryForOpportunity(false,opptys1);
                //US2528 --> added 13JUL2016 - Mayuri --- 
            if(opptysFrSecTUpdate!= null && opptysFrSecTUpdate.size() > 0){
                utilAssignTerritory.assignENDOSTerritory(opptysFrSecTUpdate,true);
            }
            
        } else {
            manager.fetchPhysicianAccounts(opptys);
        }
        
    }
    
    public override void bulkAfter() {
        List<Opportunity> opptys = (List<Opportunity>)trigger.new;
        List<Opportunity> oldopptys = (List<Opportunity>)trigger.old;
        manager.fetchPatients(opptys);
        manager.fetchNMDProducts();
      //  manager.fetchTerritories(opptys);
        manager.fetchOpptyAccounts(opptys);  // **added SFDC-1364 -Amitabh
        manager.FetchRevenueList(opptys);/**Shashank Gupta SFDC-714**on 8thSep,2016**/
        manager.FetchOpptyTerritoryandOpptyTeamList(oldopptys); /**Shashank Gupta SFDC-715**on 23rdSep,2016**/
        manager.fetchOpportunitiesWithOrders(trigger.newMap.keyset());
        manager.fetchOpportunityTerritoryUsers(Trigger.newMap, Trigger.oldMap);
        if (trigger.isInsert) {
            manager.opportunitySharing(opptys);
        }
        if(Trigger.isUpdate){
            manager.fetchOpportunityProducts(opptys);/**Dipil Jain SFDC-1366**on 14Oct,2016**/
        }
        
        /*if(trigger.IsUpdate){
        List<Opportunity> trialNMDOptys = new List<Opportunity>((List<Opportunity>)trigger.new);
            for(Integer i = (trialNMDOptys.size() - 1); i >= 0; i--){
                system.debug('PRE NMD CONDITION :'+trialNMDOptys.get(i).RecordTypeId);
                if(!RECTYPE_NMD_SCS_TRIAL_OPTY.equals(trialNMDOptys.get(i).RecordTypeId)){
                    trialNMDOptys.remove(i) ;
                }
            }
            
          manager.CreateImplantOptyFromTrial(trialNMDOptys);
        }*/
    }
    
    public override void beforeInsert(SObject obj) {
         System.debug('BeforeInsert*********:');
        Opportunity oppty = (Opportunity)obj;
        manager.updateOpportunityAccountId(null, oppty);
        manager.OpportunityRevenueUpdates(oppty);
        // oppyTerritory.assignTerritoryForOpportunity(true);
        manager.opptyDivisionDetail(oppty);/**Shashank Gupta SFDC-1431**on 21stSep,2016**/
        
    }
    
    public override void beforeUpdate(SObject oldObj, SObject obj) {
        
        Opportunity oldOppty = (Opportunity)oldObj;
        Opportunity oppty = (Opportunity)obj;
        System.debug('BeforeUpdate*********:');
        manager.updateOpportunityAccountId(oldOppty, oppty);
        manager.deleteOpptyTeamForOldSecTerritoryForCheckBox(oldOppty, oppty); /**Shashank Gupta SFDC-715**on 23rdSep,2016**/
        //if(oldOppty.OwnerId!=oppty.OwnerId || oldoppty.AccountId!=oppty.AccountId)
        //oppyTerritory.assignTerritoryForOpportunity(false);
        manager.opptyDivisionDetailBeforUpdate(oldOppty, oppty);/**Shashank Gupta SFDC-1431**on 21stSep,2016**/
        manager.updateIsAmountChangeManuallyIdentifier(oldOppty, oppty);/**Dipil Jain SFDC-1366**on 14Oct,2016**/
    }
     
    public override void afterInsert(SObject obj) {
        
        Opportunity oppty = (Opportunity)obj;
        System.debug('afterInsert*********:'+oppty.id);
        manager.createOpportunityProduct(null, oppty);  
       // manager.createClinicalDataSummary(oppty);
        manager.updatePatientPainArea(oppty);
        manager.createRevenueDetail(oppty); /**Shashank Gupta SFDC-714**on 8thSep,2016**/
        
          // manager.OpportunityRevenueUpdates(oppty);
    }
    
    public override void afterUpdate(SObject oldObj, SObject obj) {
     
        Opportunity oldOppty = (Opportunity)oldObj;
        Opportunity oppty = (Opportunity)obj;
        System.debug('afterUpdate*********:'+oppty.id);
        manager.createOpportunityProduct(oldOppty, oppty);
        manager.updatePatientPhysician(oldOppty, oppty);
        manager.updatePatientPainArea(oppty);
        manager.checkForProspectPhysicians(oldOppty, oppty);
        manager.updateOrderAccountOrContact(oldOppty, oppty);
        manager.deleteRevenueDetail(oldOppty, oppty); /**Shashank Gupta SFDC-714**on 15thSep,2016**/
        manager.deleteOpptyTeamForOldSecTerritory(oldOppty, oppty); /**Shashank Gupta SFDC-715**on 23rdSep,2016**/
        Manager.updateOptyAmount(oldOppty,oppty);/**Dipil Jain SFDC-1366**on 14Oct,2016**/
    }
    
    public override void beforeDelete(SObject obj) {
        Opportunity oppty = (Opportunity)obj;
        
        manager.preventOpportunityWithOrderDelete(oppty);
        
    }
    
    public override void andFinally() { 
        
        manager.commitOpportunityProducts();
       // manager.commitClinicalDataSummaries();
        manager.commitPatientUpdateList();
        manager.commitOrders();
        manager.applyOpportunityShare();
        if (trigger.isAfter) {
            manager.queueQuincyEmails();
        }
        manager.commitRevenueDetailList(); /**Shashank Gupta SFDC-714**on 8thSep,2016**/
        manager.commitopptyTeamList(); /**Shashank Gupta SFDC-715**on 23rdSep,2016**/
    }
    
}
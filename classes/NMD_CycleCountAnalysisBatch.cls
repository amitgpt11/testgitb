/**
* Batch class to create cycle count line items based on start date of cycle count response anlysis records.
*
* @author   Salesforce services
* @date     2015-07-20
*/
global without sharing class NMD_CycleCountAnalysisBatch implements Database.Batchable<sObject>, Schedulable {

    static final Set<Id> RECTYPE_LISTS = new Set<Id> {
        Schema.SObjectType.List__c.getRecordTypeInfosByName().get('Cycle Count Product').getRecordTypeId()
    }; 

    /***  @desc Immediately executes an instance of this batch*/
    public static Id runNow() {
        return Database.executeBatch(new NMD_CycleCountAnalysisBatch(), 100);
    }

    global String query = 
        'select ' +
            'Name, ' +
            'Cycle_Count__c, ' +
            'Cycle_Count__r.End_Date__c, ' +
            'OwnerId ' +
        'from Cycle_Count_Response_Analysis__c ' + 
        'where Start_Date__c = TODAY ';

    global NMD_CycleCountAnalysisBatch() {
        if (Test.isRunningTest()) {
            this.query += 'order by CreatedDate desc limit 100 ';
        }
    } 

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }
    
    /**
    *  @desc    Database.Bachable Executable method
    *  @param   LIST Cycle_Count_Response_Analysis__c scope
    */
    global void execute(Database.BatchableContext bc, List<Cycle_Count_Response_Analysis__c> scope) {

        List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();

        Set<Id> ccIds = new Set<Id>();
        List<Cycle_Count_Line_Item__c> ccliList = new List<Cycle_Count_Line_Item__c>();
        Map<Id,Map<Id,Cycle_Count_Response_Analysis__c>> ccli2ToCcraMap = new Map<Id,Map<Id,Cycle_Count_Response_Analysis__c>>();
        Map<Id,Id> list2CcMap = new Map<Id,Id>();
        Map<Id,List<List__c>> list2ProductMap = new Map<Id,List<List__c>>();
        Set<Id> allUserIds = new Set<Id>();
        Map<Id,Set<Id>> cycleCountUsers = new Map<Id,Set<Id>>();
        
        //get cycle count analysis object ids
        for(Cycle_Count_Response_Analysis__c ccra : scope) {
            
            allUserIds.add(ccra.OwnerId);
            if (ccIds.add(ccra.Cycle_Count__c)) {
                cycleCountUsers.put(ccra.Cycle_Count__c, new Set<Id> { ccra.OwnerId });
                ccli2ToCcraMap.put(ccra.Cycle_Count__c, new Map<Id,Cycle_Count_Response_Analysis__c> { ccra.OwnerId => ccra });
            }
            else {
                cycleCountUsers.get(ccra.Cycle_Count__c).add(ccra.OwnerId);
                ccli2ToCcraMap.get(ccra.Cycle_Count__c).put(ccra.OwnerId, ccra);
            }

        }
        
        //get product ref
        for(List__c li : [
            select  
                Model_Number__c, 
                Cycle_Count__c 
            from List__c 
            where Cycle_Count__c = :ccli2ToCcraMap.keySet()
            and RecordTypeId in :RECTYPE_LISTS
            and Model_Number__c != null
        ]) {
        
            list2CcMap.put(li.Id, li.Cycle_Count__c);

            if (list2ProductMap.containsKey(li.Model_Number__c)) {
                list2ProductMap.get(li.Model_Number__c).add(li);
            }
            else {
                list2ProductMap.put(li.Model_Number__c, new List<List__c> { li });
            }

        }
        
        //get list objects
        for(Inventory_Item__c ii : [
            select
                Inventory_Location__r.Inventory_Data_ID__r.OwnerId,
                Model_Number__c
            from Inventory_Item__c
            where Model_Number__c = :list2ProductMap.keySet()
            and Inventory_Location__r.Inventory_Data_ID__r.OwnerId in :allUserIds
            and SAP_Quantity__c > 0
        ]) {  
            for(List__c li : list2ProductMap.get(ii.Model_Number__c)) {

                Cycle_Count_Response_Analysis__c ccra = ccli2ToCcraMap.get(li.Cycle_Count__c).get(ii.Inventory_Location__r.Inventory_Data_ID__r.OwnerId);
                if (ccra != null
                 && cycleCountUsers.get(ccra.Cycle_Count__c).contains(ii.Inventory_Location__r.Inventory_Data_ID__r.OwnerId)) {

                    Cycle_Count_Line_Item__c ccli = new Cycle_Count_Line_Item__c(
                        Inventory_Item__c = ii.Id,
                        Is_Snapshot__c = true,
                        Cycle_Count_Response_Analysis__c = ccra.Id
                    );
                    ccliList.add(ccli);
                }

            }
        }
        
        //List<Database.SaveResult> ccliSaveResults = Database.insert(ccliList, false);
        DML.deferLogs();
        DML.save(this, ccliList, false);


        Map<Id,Cycle_Count_Response_Analysis__c> ccraList = new Map<Id,Cycle_Count_Response_Analysis__c>();
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        //String todayFormatted = System.today().format();
        
        
        for (Cycle_Count_Response_Analysis__c ccraTemp : scope) {
            String todayFormatted;
            if(ccraTemp.Cycle_Count__r.End_Date__c != null){
            	 todayFormatted = ccraTemp.Cycle_Count__r.End_Date__c.format();
            }else{
                todayFormatted = ''; 
            }
            ccraTemp.Status__c = 'Ready';
            ccraList.put(ccraTemp.Id, ccraTemp);
            
            SynergyPageReference spr = new SynergyPageReference('/cyclecounts/view');
            spr.parameters.put('id', ccraTemp.Id);
            PageReference pr = spr.getRedirectPageReference();
            
            
            
            String messageString = String.format('\nYou have been assigned an Inventory Count. Your participation is mandatory.  Please complete by the assigned due date of {0}.  If you have any questions, please contact your FSR.\n\n{1}', new List<String> {
                todayFormatted,
                (URL.getSalesforceBaseUrl().toExternalForm() + pr.getUrl()) 
            });
            
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            mentionSegmentInput.id = ccraTemp.OwnerId;

            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            textSegmentInput.text = messageString;
            
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            messageBodyInput.messageSegments.add(textSegmentInput);
            
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = ccraTemp.Id;
            
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);

            if (batchInputs.size() == 500) {
                if(!Test.isRunningTest()){
                	ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
                    }
                batchInputs.clear();                
            }

        }

        if (!batchInputs.isEmpty()) {
             if(!Test.isRunningTest()){
                	ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
             }
        }
        
        DML.evaluateResult(this, Database.upsert(ccraList.values(), false));
        //List<Database.UpsertResult> ccraSaveResults = Database.upsert(ccraList.values(), false);
        //for (Database.UpsertResult result : ccraSaveResults) {
        //    if (!result.isSuccess()) {
        //        for (Database.Error err : result.getErrors()) {
        //            logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Insert of Cycle Count Response Analysis Obj On NMD_CycleCountAnalysisBatch.execute'));
        //        }
        //    }
        //}

        DML.flushLogs(false);
        //if (!logs.isEmpty()) {
        //    Logger.logMessage(logs);
        //}
      
    }  
         
    /***  @desc Schedulable method, executes the class instance*/
    global void execute(SchedulableContext context) {
        NMD_CycleCountAnalysisBatch.runNow();
    }

    global void finish(Database.BatchableContext bc) {
        
    }   
}
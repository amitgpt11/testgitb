@isTest
private class OrdersDetailController_test {

	private static testMethod void test() {
	    
	    Test.startTest();
        
        User objUser = Test_DataCreator.createCommunityUser(); 
        
        system.runAs(objUser) {
            
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            objOrder.Status__c = 'Open';
            update objOrder;
            
            Product2 objProduct = Test_DataCreator.createProduct('Test product');
            
            Shared_Community_Order_Item__c objOrderItem = Test_DataCreator.createOrderItem(objOrder.Id, objProduct.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.OrdersDetail')); 
            System.currentPageReference().getParameters().put('Id', objOrder.Id);
            OrdersDetailController objOrdersDetailController = new OrdersDetailController();
            objOrdersDetailController.init();
            system.assertNotEquals(objOrdersDetailController.objCommunityOrder,null);
            
            objOrdersDetailController = new OrdersDetailController();
            objOrder.Status__c = 'Processing';
            update objOrder;
            system.assertEquals(objOrdersDetailController.reOrder().getURL(),Page.Orders.getURL());
        }
        
        Test.stopTest();
	}

}
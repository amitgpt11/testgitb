/**
* Extension class for the AnnotateImage page/app
*
* @Author salesforce Services
* @Date 03/20/2015
*/
public without sharing class NMD_AnnotateImageExtension extends NMD_ExtensionBase {

	final static String MISC = 'Word Excel PDF';

	final ApexPages.StandardController sc;
	
	public List<FeedItem> images { get; private set; }

	/**
    * Contructor.  Fetch the relevant data summary and attachments records
    * 
    * @param void
    */
	public NMD_AnnotateImageExtension(ApexPages.StandardController sc) {
		
		Id patientId = ((Opportunity)sc.getRecord()).Patient__c;

		this.sc = sc;
		this.images = new List<FeedItem>{};

		// only allow attachments with an active value to be returned
		Set<String> classifications = new Set<String>{};
		for (PatientImageClassifications__c pic : PatientImageClassifications__c.getAll().values()) {
			if (pic.Is_Active__c && pic.Name != MISC) {
				classifications.add(pic.Name);
			}
		}

		for(FeedItem item : [
            select 
            	RelatedRecordId,
            	Title, 
            	ContentDescription
            from FeedItem
            where ParentId = :patientId
            and Type = 'ContentPost'
            order by CreatedDate desc
        ]) {
        	// ContentDescription and RelatedRecordId cannot be filtered in a query call
        	if (String.isNotBlank(item.ContentDescription)
        	 && classifications.contains(item.ContentDescription)
        	 && item.RelatedRecordId != null
        	) {
				this.images.add(item);
			}
		}

	}

	/**
    * Called when the page loads, redirects to the desktop version if not in SF1
    * 
    * @param void
    */
	public PageReference checkRedirect() {

		PageReference pr;
		if (ApexPages.currentPage().getParameters().get('isdtp') != 'p1') {
			pr = Page.AnnotateImage;
			pr.getParameters().put('id', this.sc.getId());
		}

		return pr;
	}

}
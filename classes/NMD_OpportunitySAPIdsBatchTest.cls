@IsTest(SeeAllData=false)
private class NMD_OpportunitySAPIdsBatchTest {

    static final String ACCTNAME = 'AcctName';
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
    public static final Id RECTYPE_TRIAL = RECTYPES_OPPTY.get('NMD SCS Trial-Implant').getRecordTypeId();
    
    @TestSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        //create account
        Account acct = td.createConsignmentAccount();
        acct.Name = ACCTNAME;
        insert acct;

        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        insert territory;

        Patient__c patient = new Patient__c(
            RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
            Territory_ID__c = territory.Id,
            SAP_ID__c = td.randomString5()
        );
        insert patient;

        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.Patient__c = patient.Id;
        oppty.recordTypeId = RECTYPE_TRIAL;
        insert oppty;

        oppty.Patient_SAP_Id__c = '';
        update oppty;

    }

    static testMethod void test_OpportunitySAPIdsBatch() {

        Opportunity oppty = [select Patient_SAP_Id__c from Opportunity where Account.Name = :ACCTNAME];
        System.assert(String.isBlank(oppty.Patient_SAP_Id__c));

        Test.startTest();
        {

            NMD_OpportunitySAPIdsBatch.runNow();

        }
        Test.stopTest();

        oppty = [select Patient_SAP_Id__c, Patient__r.SAP_ID__c from Opportunity where Id = :oppty.Id];
        System.assert(String.isNotBlank(oppty.Patient__r.SAP_Id__c));
        System.assertEquals(oppty.Patient__r.SAP_ID__c, oppty.Patient_SAP_Id__c);

    }

}
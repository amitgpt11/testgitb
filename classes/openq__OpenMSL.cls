/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OpenMSL {
    global OpenMSL() {

    }
global class ADD_REMOVE_KOL_REQUEST_element {
    @WebService
    webService Boolean KEY_CONTACTS_UPDATE_TYPE;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService String PROFILE_LIST_UPDATE_TYPE;
    @WebService
    webService String UUID;
    global ADD_REMOVE_KOL_REQUEST_element() {

    }
}
global class ADD_REMOVE_KOL_RESPONSE_element {
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global ADD_REMOVE_KOL_RESPONSE_element() {

    }
}
global class ASSET_SLIDE_PRESENTATION_element {
    @WebService
    webService Integer ASSET_SLIDE_PRESENTATION_TIME;
    @WebService
    webService Integer SLIDE_ID;
    global ASSET_SLIDE_PRESENTATION_element() {

    }
}
global class AUTHENTICATE_REQUEST_element {
    @WebService
    webService String DEVICE_MODEL;
    @WebService
    webService String USER_PASSWORD;
    @WebService
    webService String USERNAME;
    @WebService
    webService String UUID;
    global AUTHENTICATE_REQUEST_element() {

    }
}
global class AUTHENTICATE_RESPONSE_element {
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    @WebService
    webService String SESSION_TOKEN;
    global AUTHENTICATE_RESPONSE_element() {

    }
}
global class CREATE_KOL_REQUEST_element {
    @WebService
    webService openq.OpenMSL.HCP_PHOTO_element HCP_PHOTO;
    @WebService
    webService openq.OpenMSL.PROFILE_DATA_RECORD_element PROFILE_DATA_RECORD;
    @WebService
    webService String UUID;
    global CREATE_KOL_REQUEST_element() {

    }
}
global class CREATE_KOL_RESPONSE_element {
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global CREATE_KOL_RESPONSE_element() {

    }
}
global class CUSTOMER_LOGO_element {
    @WebService
    webService String CUSTOMER_LOGO_FILE;
    @WebService
    webService String FILE_MIME_TYPE;
    @WebService
    webService String FILE_NAME;
    @WebService
    webService String IMAGE_HEIGHT;
    @WebService
    webService String IMAGE_WIDTH;
    global CUSTOMER_LOGO_element() {

    }
}
global class CUSTOMER_PROPERTIES_REQUEST_element {
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global CUSTOMER_PROPERTIES_REQUEST_element() {

    }
}
global class CUSTOMER_PROPERTIES_RESPONSE_element {
    @WebService
    webService String APPLICATION_VERSION_NUMBER;
    @WebService
    webService String CUSTOMER_COLOR_SCHEME_ID;
    @WebService
    webService openq.OpenMSL.CUSTOMER_LOGO_element CUSTOMER_LOGO;
    @WebService
    webService String EMPOWER_URL;
    @WebService
    webService Boolean INTERACTIONS_MODULE_ENABLED;
    @WebService
    webService Boolean MIRF_MODULE_ENABLED;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    @WebService
    webService List<openq.OpenMSL.TASK_LOV_DEFINITION_element> TASK_LOV_DEFINITION;
    @WebService
    webService openq.OpenMSL.USER_PROFILE_element USER_PROFILE;
    global CUSTOMER_PROPERTIES_RESPONSE_element() {

    }
}
global class ENGAGEMENT_PLAN_element {
    @WebService
    webService Integer ENGAGEMENT_PLAN_ID;
    @WebService
    webService String ENGAGEMENT_PLAN_NAME;
    global ENGAGEMENT_PLAN_element() {

    }
}
global class EVENT_DATA_element {
    @WebService
    webService List<openq.OpenMSL.EVENT_element> EVENT;
    global EVENT_DATA_element() {

    }
}
global class EVENT_LOCATION_element {
    @WebService
    webService String EVENT_CITY;
    @WebService
    webService String EVENT_COUNTRY;
    @WebService
    webService String EVENT_STATE;
    global EVENT_LOCATION_element() {

    }
}
global class EVENT_element {
    @WebService
    webService Date EVENT_DATE;
    @WebService
    webService Integer EVENT_ID;
    @WebService
    webService openq.OpenMSL.EVENT_LOCATION_element EVENT_LOCATION;
    @WebService
    webService String EVENT_NAME;
    @WebService
    webService String EVENT_PRODUCT;
    @WebService
    webService String EVENT_TA;
    global EVENT_element() {

    }
}
global class FORM_DEFINITION_REQUEST_element {
    @WebService
    webService String SESSION_TOKEN;
    global FORM_DEFINITION_REQUEST_element() {

    }
}
global class FORM_DEFINITION_RESPONSE_element {
    @WebService
    webService String INTERACTION_FORM_DEFINITION_FILE;
    @WebService
    webService String MIRF_FORM_DEFINITION_FILE;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global FORM_DEFINITION_RESPONSE_element() {

    }
}
global class HCP_ATTRIBUTE_element {
    @WebService
    webService String HCP_ATTRIBUTE_NAME;
    @WebService
    webService String HCP_ATTRIBUTE_VALUE;
    global HCP_ATTRIBUTE_element() {

    }
}
global class HCP_INFORMATION_element {
    @WebService
    webService List<openq.OpenMSL.HCP_ATTRIBUTE_element> HCP_ATTRIBUTE;
    @WebService
    webService String OPENQ_PROFILE_ID;
    global HCP_INFORMATION_element() {

    }
}
global class HCP_PHOTO_element {
    @WebService
    webService String FILE_MIME_TYPE;
    @WebService
    webService String FILE_NAME;
    @WebService
    webService String HCP_PHOTO_FILE;
    @WebService
    webService String IMAGE_HEIGHT;
    @WebService
    webService String IMAGE_WIDTH;
    global HCP_PHOTO_element() {

    }
}
global class INTERACTION_CATEGORY_ATTRIBUTE_element {
    @WebService
    webService Integer DISPLAY_ORDER;
    @WebService
    webService String INTERACTION_ATTRIBUTE_HELP_TEXT;
    @WebService
    webService Integer INTERACTION_ATTRIBUTE_ID;
    @WebService
    webService String INTERACTION_ATTRIBUTE_LABEL;
    @WebService
    webService Integer INTERACTION_ATTRIBUTE_LENGTH;
    @WebService
    webService String INTERACTION_ATTRIBUTE_NAME;
    @WebService
    webService List<String> INTERACTION_ATTRIBUTE_PERMISSION;
    @WebService
    webService Boolean INTERACTION_ATTRIBUTE_REQUIRED;
    @WebService
    webService String INTERACTION_ATTRIBUTE_TYPE;
    @WebService
    webService openq.OpenMSL.LOV_element LOV;
    @WebService
    webService String UPDATE_TYPE;
    @WebService
    webService Boolean VIEWABLE_FLAG;
    global INTERACTION_CATEGORY_ATTRIBUTE_element() {

    }
}
global class INTERACTION_CATEGORY_DEFINITION_element {
    @WebService
    webService Integer DISPLAY_ORDER;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_CATEGORY_ATTRIBUTE_element> INTERACTION_CATEGORY_ATTRIBUTE;
    @WebService
    webService Integer INTERACTION_CATEGORY_ID;
    @WebService
    webService String INTERACTION_CATEGORY_LABEL;
    @WebService
    webService String INTERACTION_CATEGORY_NAME;
    @WebService
    webService openq.OpenMSL.INTERACTION_CATEGORY_PARENT_element INTERACTION_CATEGORY_PARENT;
    @WebService
    webService List<String> INTERACTION_CATEGORY_PERMISSION;
    @WebService
    webService String INTERACTION_CATEGORY_TYPE;
    @WebService
    webService String UPDATE_TYPE;
    @WebService
    webService Boolean VIEWABLE_FLAG;
    global INTERACTION_CATEGORY_DEFINITION_element() {

    }
}
global class INTERACTION_CATEGORY_PARENT_element {
    @WebService
    webService String INTERACTION_CATEGORY_PARENT_DISPLAY_ORDER;
    @WebService
    webService String INTERACTION_CATEGORY_PARENT_LABEL;
    global INTERACTION_CATEGORY_PARENT_element() {

    }
}
global class INTERACTION_CREATE_REQUEST_element {
    @WebService
    webService Integer EVENT_ID;
    @WebService
    webService List<Integer> INTERACTION_ATTENDEE_PROFILE_ID;
    @WebService
    webService List<Id> INTERACTION_ATTENDEE_SFDC_PROFILE_ID;
    @WebService
    webService String INTERACTION_CREATE_DATETIME;
    @WebService
    webService String INTERACTION_DATE;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_MODIFIED_TASK_element> INTERACTION_MODIFIED_TASK;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_OTHER_ATTENDEES_element> INTERACTION_OTHER_ATTENDEES;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_PRESENTATION_element> INTERACTION_PRESENTATION;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_SECTION_DATA_RECORD_element> INTERACTION_SECTION_DATA_RECORD;
    @WebService
    webService Integer LOCAL_CLIENT_INTERACTION_ID;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global INTERACTION_CREATE_REQUEST_element() {

    }
}
global class INTERACTION_CREATE_RESPONSE_element {
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global INTERACTION_CREATE_RESPONSE_element() {

    }
}
global class INTERACTION_DATA_RETRIEVE_REQUEST_element {
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global INTERACTION_DATA_RETRIEVE_REQUEST_element() {

    }
}
global class INTERACTION_DATA_RETRIEVE_RESPONSE_element {
    @WebService
    webService openq.OpenMSL.EVENT_DATA_element EVENT_DATA;
    @WebService
    webService openq.OpenMSL.LOV_DATA_element LOV_DATA;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global INTERACTION_DATA_RETRIEVE_RESPONSE_element() {

    }
}
global class INTERACTION_FORM_DEFINITION_REQUEST_element {
    @WebService
    webService Datetime LAST_SYNC_DATETIME;
    @WebService
    webService String SESSION_TOKEN;
    global INTERACTION_FORM_DEFINITION_REQUEST_element() {

    }
}
global class INTERACTION_FORM_DEFINITION_RESPONSE_element {
    @WebService
    webService Integer CUSTOMER_ID;
    @WebService
    webService String DESCRIPTION;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_CATEGORY_DEFINITION_element> INTERACTION_CATEGORY_DEFINITION;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global INTERACTION_FORM_DEFINITION_RESPONSE_element() {

    }
}
global class INTERACTION_MODIFIED_TASK_element {
    @WebService
    webService String TASK_COMMENTS;
    @WebService
    webService Integer TASK_ID;
    @WebService
    webService Integer TASK_STATUS_LOV_ID;
    global INTERACTION_MODIFIED_TASK_element() {

    }
}
global class INTERACTION_OTHER_ATTENDEES_element {
    @WebService
    webService Integer INTERACTION_OTHER_ATTENDEE_COUNT;
    @WebService
    webService String INTERACTION_OTHER_ATTENDEE_TYPE;
    global INTERACTION_OTHER_ATTENDEES_element() {

    }
}
global class INTERACTION_PRESENTATION_element {
    @WebService
    webService String ASSET_ID;
    @WebService
    webService List<openq.OpenMSL.ASSET_SLIDE_PRESENTATION_element> ASSET_SLIDE_PRESENTATION;
    @WebService
    webService Integer ASSET_TOTAL_PRESENTATION_TIME;
    @WebService
    webService Integer CUSTOM_PRESENTATION_ID;
    @WebService
    webService String PRESENTATION_NAME;
    global INTERACTION_PRESENTATION_element() {

    }
}
global class INTERACTION_RECORD_RETRIEVE_REQUEST_element {
    @WebService
    webService String INTERACTION_ID;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global INTERACTION_RECORD_RETRIEVE_REQUEST_element() {

    }
}
global class INTERACTION_RECORD_RETRIEVE_RESPONSE_element {
    @WebService
    webService openq.OpenMSL.INTERACTION_RECORD_element INTERACTION_RECORD;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global INTERACTION_RECORD_RETRIEVE_RESPONSE_element() {

    }
}
global class INTERACTION_RECORD_element {
    @WebService
    webService openq.OpenMSL.EVENT_element EVENT;
    @WebService
    webService List<String> INTERACTION_ATTENDEE_NAME;
    @WebService
    webService Datetime INTERACTION_CREATE_DATETIME;
    @WebService
    webService Datetime INTERACTION_DATE;
    @WebService
    webService String INTERACTION_ID;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_OTHER_ATTENDEES_element> INTERACTION_OTHER_ATTENDEES;
    @WebService
    webService String INTERACTION_OWNER_NAME;
    @WebService
    webService String INTERACTION_OWNER_USERNAME;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_SECTION_DATA_RECORD_element> INTERACTION_SECTION_DATA_RECORD;
    @WebService
    webService String INTERACTION_VISIBILITY_SCOPE;
    global INTERACTION_RECORD_element() {

    }
}
global class INTERACTION_SECTION_DATA_RECORD_element {
    @WebService
    webService String INTERACTION_SECTION_DATA_NAME;
    @WebService
    webService List<String> INTERACTION_SECTION_DATA_VALUE;
    global INTERACTION_SECTION_DATA_RECORD_element() {

    }
}
global class INTERACTION_UPDATE_SUMMARY_element {
    @WebService
    webService String INTERACTION_ID;
    @WebService
    webService String UPDATE_TYPE;
    global INTERACTION_UPDATE_SUMMARY_element() {

    }
}
global class KOL_SEARCH_REQUEST_element {
    @WebService
    webService String SEARCH_TERM;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String USERNAME;
    @WebService
    webService String UUID;
    global KOL_SEARCH_REQUEST_element() {

    }
}
global class LOV_DATA_element {
    @WebService
    webService List<openq.OpenMSL.LOV_element> LOV;
    global LOV_DATA_element() {

    }
}
global class LOV_VALUE_OPTION_element {
    @WebService
    webService Integer DISPLAY_ORDER;
    @WebService
    webService Boolean IS_DEFAULT;
    @WebService
    webService Integer OPTION_ID;
    @WebService
    webService String OPTION_VALUE;
    @WebService
    webService Integer PARENT_OPTION_ID;
    @WebService
    webService String UPDATE_TYPE;
    global LOV_VALUE_OPTION_element() {

    }
}
global class LOV_element {
    @WebService
    webService String LOV_DISPLAY_NAME;
    @WebService
    webService Integer LOV_ID;
    @WebService
    webService List<openq.OpenMSL.LOV_VALUE_OPTION_element> LOV_VALUE_OPTIONS;
    @WebService
    webService Integer PARENT_LOV_ID;
    @WebService
    webService String UPDATE_TYPE;
    global LOV_element() {

    }
}
global class MIRF_CREATE_REQUEST_element {
    @WebService
    webService openq.OpenMSL.HCP_INFORMATION_element HCP_INFORMATION;
    @WebService
    webService Integer LOCAL_CLIENT_MIRF_ID;
    @WebService
    webService Datetime MIRF_CREATE_DATETIME;
    @WebService
    webService Date MIRF_DATE;
    @WebService
    webService List<openq.OpenMSL.MIRF_SECTION_DATA_RECORD_element> MIRF_SECTION_DATA_RECORD;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String SIGNATURE_IMAGE;
    @WebService
    webService String UUID;
    global MIRF_CREATE_REQUEST_element() {

    }
}
global class MIRF_CREATE_RESPONSE_element {
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global MIRF_CREATE_RESPONSE_element() {

    }
}
global class MIRF_DATA_RETRIEVE_REQUEST_element {
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global MIRF_DATA_RETRIEVE_REQUEST_element() {

    }
}
global class MIRF_DATA_RETRIEVE_RESPONSE_element {
    @WebService
    webService openq.OpenMSL.LOV_DATA_element LOV_DATA;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global MIRF_DATA_RETRIEVE_RESPONSE_element() {

    }
}
global class MIRF_RECORD_RETRIEVE_REQUEST_element {
    @WebService
    webService Integer MIRF_ID;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global MIRF_RECORD_RETRIEVE_REQUEST_element() {

    }
}
global class MIRF_RECORD_RETRIEVE_RESPONSE_element {
    @WebService
    webService openq.OpenMSL.MIRF_RECORD_element MIRF_RECORD;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global MIRF_RECORD_RETRIEVE_RESPONSE_element() {

    }
}
global class MIRF_RECORD_element {
    @WebService
    webService openq.OpenMSL.HCP_INFORMATION_element HCP_INFORMATION;
    @WebService
    webService Integer LOCAL_CLIENT_MIRF_ID;
    @WebService
    webService Datetime MIRF_CREATE_DATETIME;
    @WebService
    webService Date MIRF_DATE;
    @WebService
    webService Integer MIRF_ID;
    @WebService
    webService String MIRF_OWNER_NAME;
    @WebService
    webService List<openq.OpenMSL.MIRF_SECTION_DATA_RECORD_element> MIRF_SECTION_DATA_RECORD;
    @WebService
    webService String MIRF_VISIBILITY_SCOPE;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService String SIGNATURE_IMAGE;
    global MIRF_RECORD_element() {

    }
}
global class MIRF_SECTION_DATA_RECORD_element {
    @WebService
    webService String MIRF_SECTION_DATA_NAME;
    @WebService
    webService List<String> MIRF_SECTION_DATA_VALUE;
    global MIRF_SECTION_DATA_RECORD_element() {

    }
}
global class MIRF_UPDATE_SUMMARY_element {
    @WebService
    webService Integer MIRF_ID;
    @WebService
    webService String UPDATE_TYPE;
    global MIRF_UPDATE_SUMMARY_element() {

    }
}
global class PROFILE_ATTRIBUTE_DATA_element {
    @WebService
    webService Integer PROFILE_ATTRIBUTE_Id;
    @WebService
    webService String PROFILE_ATTRIBUTE_NAME;
    @WebService
    webService String PROFILE_ATTRIBUTE_TYPE;
    @WebService
    webService String PROFILE_ATTRIBUTE_VALUE;
    @WebService
    webService String PROFILE_BINARY_ATTRIBUTE_VALUE;
    global PROFILE_ATTRIBUTE_DATA_element() {

    }
}
global class PROFILE_CATEGORY_ATTRIBUTE_element {
    @WebService
    webService Integer DISPLAY_ORDER;
    @WebService
    webService openq.OpenMSL.LOV_element LOV;
    @WebService
    webService Integer PROFILE_ATTRIBUTE_ID;
    @WebService
    webService String PROFILE_ATTRIBUTE_LABEL;
    @WebService
    webService Integer PROFILE_ATTRIBUTE_LENGTH;
    @WebService
    webService String PROFILE_ATTRIBUTE_NAME;
    @WebService
    webService List<String> PROFILE_ATTRIBUTE_PERMISSION;
    @WebService
    webService Boolean PROFILE_ATTRIBUTE_REQUIRED;
    @WebService
    webService String PROFILE_ATTRIBUTE_TYPE;
    @WebService
    webService String UPDATE_TYPE;
    @WebService
    webService Boolean VIEWABLE_FLAG;
    global PROFILE_CATEGORY_ATTRIBUTE_element() {

    }
}
global class PROFILE_CATEGORY_DATA_UPDATE_RESULT_element {
    @WebService
    webService String EXTERNAL_DATA_RECORD_ID;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService String PROFILE_DATA_RECORD_ID;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global PROFILE_CATEGORY_DATA_UPDATE_RESULT_element() {

    }
}
global class PROFILE_CATEGORY_DATA_element {
    @WebService
    webService String PROFILE_CATEGORY_ID;
    @WebService
    webService String PROFILE_CATEGORY_NAME;
    @WebService
    webService List<openq.OpenMSL.PROFILE_DATA_RECORD_element> PROFILE_DATA_RECORD;
    global PROFILE_CATEGORY_DATA_element() {

    }
}
global class PROFILE_CATEGORY_DEFINITION_element {
    @WebService
    webService Integer DISPLAY_ORDER;
    @WebService
    webService List<openq.OpenMSL.PROFILE_CATEGORY_ATTRIBUTE_element> PROFILE_CATEGORY_ATTRIBUTE;
    @WebService
    webService Integer PROFILE_CATEGORY_ID;
    @WebService
    webService String PROFILE_CATEGORY_LABEL;
    @WebService
    webService String PROFILE_CATEGORY_NAME;
    @WebService
    webService openq.OpenMSL.PROFILE_CATEGORY_PARENT_element PROFILE_CATEGORY_PARENT;
    @WebService
    webService List<String> PROFILE_CATEGORY_PERMISSION;
    @WebService
    webService String PROFILE_CATEGORY_TYPE;
    @WebService
    webService String UPDATE_TYPE;
    @WebService
    webService Boolean VIEWABLE_FLAG;
    global PROFILE_CATEGORY_DEFINITION_element() {

    }
}
global class PROFILE_CATEGORY_PARENT_element {
    @WebService
    webService String PROFILE_CATEGORY_PARENT_DISPLAY_ORDER;
    @WebService
    webService String PROFILE_CATEGORY_PARENT_LABEL;
    global PROFILE_CATEGORY_PARENT_element() {

    }
}
global class PROFILE_DATA_DEFINITION_REQUEST_element {
    @WebService
    webService Datetime LAST_SYNC_DATETIME;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global PROFILE_DATA_DEFINITION_REQUEST_element() {

    }
}
global class PROFILE_DATA_DEFINITION_RESPONSE_element {
    @WebService
    webService List<openq.OpenMSL.PROFILE_CATEGORY_DEFINITION_element> PROFILE_CATEGORY_DEFINITION;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    @WebService
    webService Integer ROOT_LEVEL_NODE_ID;
    global PROFILE_DATA_DEFINITION_RESPONSE_element() {

    }
}
global class PROFILE_DATA_RECORD_element {
    @WebService
    webService String EXTERNAL_DATA_RECORD_ID;
    @WebService
    webService String LAST_MODIFIED_DATETIME;
    @WebService
    webService List<openq.OpenMSL.PROFILE_ATTRIBUTE_DATA_element> PROFILE_ATTRIBUTE_DATA;
    @WebService
    webService String PROFILE_DATA_RECORD_ID;
    @WebService
    webService String UPDATE_TYPE;
    global PROFILE_DATA_RECORD_element() {

    }
}
global class PROFILE_DATA_RETRIEVE_REQUEST_element {
    @WebService
    webService String LAST_SYNC_DATETIME;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UPDATE_TYPE;
    @WebService
    webService String UUID;
    global PROFILE_DATA_RETRIEVE_REQUEST_element() {

    }
}
global class PROFILE_DATA_RETRIEVE_RESPONSE_element {
    @WebService
    webService openq.OpenMSL.HCP_PHOTO_element HCP_PHOTO;
    @WebService
    webService List<openq.OpenMSL.INTERACTION_UPDATE_SUMMARY_element> INTERACTION_UPDATE_SUMMARY;
    @WebService
    webService List<openq.OpenMSL.MIRF_UPDATE_SUMMARY_element> MIRF_UPDATE_SUMMARY;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService List<openq.OpenMSL.PROFILE_CATEGORY_DATA_element> PROFILE_CATEGORY_DATA;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    @WebService
    webService List<openq.OpenMSL.TASK_UPDATE_SUMMARY_element> TASK_UPDATE_SUMMARY;
    global PROFILE_DATA_RETRIEVE_RESPONSE_element() {

    }
}
global class PROFILE_SEARCH_RESULT_RESPONSE_element {
    @WebService
    webService List<openq.OpenMSL.PROFILE_SEARCH_RESULT_element> PROFILE_SEARCH_RESULT;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global PROFILE_SEARCH_RESULT_RESPONSE_element() {

    }
}
global class PROFILE_SEARCH_RESULT_element {
    @WebService
    webService String FIRST_NAME;
    @WebService
    webService String LAST_NAME;
    @WebService
    webService String MIDDLE_NAME;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService String PHYSICIAN_LOCATION;
    @WebService
    webService String PHYSICIAN_SPECIALTIES;
    @WebService
    webService Id SALESFORCE_ID;
    global PROFILE_SEARCH_RESULT_element() {

    }
}
global class PROFILE_SYNC_REQUEST_element {
    @WebService
    webService String LAST_SYNC_DATETIME;
    @WebService
    webService List<openq.OpenMSL.PROFILE_element> PROFILE;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String UUID;
    global PROFILE_SYNC_REQUEST_element() {

    }
}
global class PROFILE_SYNC_RESPONSE_element {
    @WebService
    webService List<openq.OpenMSL.PROFILE_CATEGORY_DATA_UPDATE_RESULT_element> PROFILE_CATEGORY_DATA_UPDATE_RESULT;
    @WebService
    webService List<openq.OpenMSL.PROFILE_UPDATE_SUMMARY_element> PROFILE_UPDATE_SUMMARY;
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global PROFILE_SYNC_RESPONSE_element() {

    }
}
global class PROFILE_TASK_DATA_RETRIEVE_REQUEST_element {
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService List<Integer> TASK_ID;
    @WebService
    webService String UUID;
    global PROFILE_TASK_DATA_RETRIEVE_REQUEST_element() {

    }
}
global class PROFILE_TASK_DATA_RETRIEVE_RESPONSE_element {
    @WebService
    webService List<openq.OpenMSL.TASK_element> TASK;
    global PROFILE_TASK_DATA_RETRIEVE_RESPONSE_element() {

    }
}
global class PROFILE_UPDATE_SUMMARY_element {
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService String UPDATE_TYPE;
    global PROFILE_UPDATE_SUMMARY_element() {

    }
}
global class PROFILE_element {
    @WebService
    webService openq.OpenMSL.HCP_PHOTO_element HCP_PHOTO;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Id OPENQ_SFDC_PROFILE_ID;
    @WebService
    webService List<openq.OpenMSL.PROFILE_CATEGORY_DATA_element> PROFILE_CATEGORY_DATA;
    @WebService
    webService List<openq.OpenMSL.TASK_element> TASK;
    @WebService
    webService String UPDATE_TYPE;
    global PROFILE_element() {

    }
}
global class SEND_USER_PINCODE_REQUEST_element {
    @WebService
    webService String SESSION_TOKEN;
    @WebService
    webService String USER_PINCODE;
    global SEND_USER_PINCODE_REQUEST_element() {

    }
}
global class SEND_USER_PINCODE_RESPONSE_element {
    @WebService
    webService String RESPONSE_CODE_DESCRIPTION;
    @WebService
    webService Integer RESPONSE_CODE_ID;
    global SEND_USER_PINCODE_RESPONSE_element() {

    }
}
global class TASK_CREATOR_element {
    @WebService
    webService String USER_FIRST_NAME;
    @WebService
    webService Integer USER_ID;
    @WebService
    webService String USER_LAST_NAME;
    global TASK_CREATOR_element() {

    }
}
global class TASK_LOV_DEFINITION_element {
    @WebService
    webService String LOV_DISPLAY_NAME;
    @WebService
    webService Integer LOV_ID;
    @WebService
    webService List<openq.OpenMSL.LOV_VALUE_OPTION_element> LOV_VALUE_OPTION;
    @WebService
    webService Integer PARENT_LOV_ID;
    global TASK_LOV_DEFINITION_element() {

    }
}
global class TASK_OWNER_element {
    @WebService
    webService String USER_FIRST_NAME;
    @WebService
    webService Integer USER_ID;
    @WebService
    webService String USER_LAST_NAME;
    @WebService
    webService String USERNAME;
    global TASK_OWNER_element() {

    }
}
global class TASK_UPDATE_SUMMARY_element {
    @WebService
    webService Integer TASK_ID;
    @WebService
    webService String UPDATE_TYPE;
    global TASK_UPDATE_SUMMARY_element() {

    }
}
global class TASK_element {
    @WebService
    webService openq.OpenMSL.ENGAGEMENT_PLAN_element ENGAGEMENT_PLAN;
    @WebService
    webService String OPENQ_PROFILE_ID;
    @WebService
    webService Integer PRODUCT_LOV_ID;
    @WebService
    webService Integer TASK_ACTIVITY_LOV_ID;
    @WebService
    webService String TASK_COMMENTS;
    @WebService
    webService openq.OpenMSL.TASK_CREATOR_element TASK_CREATOR;
    @WebService
    webService Date TASK_DUE_DATE;
    @WebService
    webService Integer TASK_ID;
    @WebService
    webService Integer TASK_OBJECTIVE_LOV_ID;
    @WebService
    webService openq.OpenMSL.TASK_OWNER_element TASK_OWNER;
    @WebService
    webService Integer TASK_STATUS_LOV_ID;
    @WebService
    webService String UPDATE_TYPE;
    global TASK_element() {

    }
}
global class USER_PROFILE_element {
    @WebService
    webService String USER_ADDRESS_1;
    @WebService
    webService String USER_ADDRESS_2;
    @WebService
    webService String USER_ADDRESS_CITY;
    @WebService
    webService String USER_ADDRESS_COUNTRY;
    @WebService
    webService String USER_ADDRESS_POSTAL_CODE;
    @WebService
    webService String USER_ADDRESS_STATE;
    @WebService
    webService String USER_EMAIL;
    @WebService
    webService String USER_FIRST_NAME;
    @WebService
    webService String USER_ID;
    @WebService
    webService String USER_LAST_NAME;
    @WebService
    webService String USER_MOBILE;
    @WebService
    webService String USER_PHONE;
    @WebService
    webService String USERNAME;
    global USER_PROFILE_element() {

    }
}
}

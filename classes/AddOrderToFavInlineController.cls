/*
@CreatedDate     12Sept2016
@author          Ashish
@Description     Controller for AddOrderToFavInline VF Page: Determines whether a user had added the selected record as a favorite, for inline display of the favorites notification
@Requirement Id  User Story : SFDC - 1490
*/

public class AddOrderToFavInlineController {
    List<Order_Favorite__c> lstFavOrder;
    public boolean showAddButton{get;set;}
    public String messageText{get;set;}
    Id ordId;
    Id orderFavId;
    public AddOrderToFavInlineController (ApexPages.StandardController controller){
        ordId = controller.getId();
        showOrderFavorites();
        
    }
    
    public void showOrderFavorites(){
        lstFavOrder = [Select id, Name, Uro_PH_Order__c, Uro_PH_User__c from Order_Favorite__c where Uro_PH_Order__c = :ordId and Uro_PH_User__c = :UserInfo.getUserId() ];
        if(lstFavOrder.size() > 0){
            orderFavId = lstFavOrder.get(0).id;
            showAddButton = false;
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'This record is added to your favorites.'));
            messageText = 'This record is added to your favorites.';
        }else{
            showAddButton = true;
        }
    }
    
}
/*
 @CreatedDate     16 June 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Apex class 'sortSRAIDetailComponentController'
 
 */

@isTest
public class sortSRAIDetailComponentControllerTest{

    static TestMethod void TestsortSRAIDetailComponentController(){ 
        date todayDate = date.today();
        Map<integer,String> mapMonthNumber = TestDataFactory.getMapMonthNumber();  
        PI_Lutonix_SRAI__c headObj = TestDataFactory.createTestLutonixHeader(mapMonthNumber.get(todayDate.Month()));
        PI_Lutonix_SRAI_Detail__c detObj = TestDataFactory.createTestLutonixDetail(headObj.Id,todayDate);
        
        Test.StartTest();
        sortSRAIDetailComponentController detailCntrl = new sortSRAIDetailComponentController();
        detailCntrl.SRAIHeaderId = headObj.Id;
        detailCntrl.SRAIHeaderMonth = headObj.PI_Month__c;
        detailCntrl.SRAIHeaderYear = headObj.PI_Year__c;
        list<PI_Lutonix_SRAI_Detail__c> recFromController = detailCntrl.getlstSRAIDetail();
        
        //integer daysInMonth = date.daysInMonth(todayDate.Month(), todayDate.Month());
        System.assertequals(recFromController.size(), 31);
        Test.StopTest();
        
    }
}
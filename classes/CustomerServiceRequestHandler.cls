global class CustomerServiceRequestHandler {
    
   
    public static List<Application_Log__c> appLogs  = new List<Application_Log__c>();
    
    Webservice static List<CustomerServiceResponse.ShipToAddress> CallCustomerService(String sapAccountNumber,String partnerAddressRequired, String salesOrg)
    {
    
        CustomerServiceIntegrationDetail__c CSI = CustomerServiceIntegrationDetail__c.getValues('Shipping Address Detail');
        if(CSI == null || String.IsBlank(CSI.URL__c) || String.IsBlank(CSI.Username__c) || String.IsBlank(CSI.Password__c) || String.IsBlank(CSI.Certificate__c)){
           // return;    
        }
    
        List<CustomerServiceResponse.ShipToAddress> st1 = new List<CustomerServiceResponse.ShipToAddress>();
        CustomerJsonRequest custRequest = new CustomerJsonRequest();
        CustomerJsonRequest.CustomerInfo custInfo = new CustomerJsonRequest.CustomerInfo();
        custInfo.soldToAccountNumber = sapAccountNumber;
        custInfo.partnerAddressDetail = partnerAddressRequired;
        custInfo.salesOrganization  = salesOrg;
        custRequest.CustomerInfo = custInfo;
        
        JSONGenerator generateRequest = JSON.createGenerator(true);
        generateRequest.writeStartObject();
        generateRequest.writeFieldName('CustomerInfo');
        generateRequest.writeObject(custRequest);
        generateRequest.writeEndObject();
        System.debug('REST REQUEST:'+generateRequest.getAsString());
        
        String reqString = JSON.serialize(custRequest);
        System.debug('reqString-------'+reqString);
        String CertificateName= Label.CertificateInfo;
        HttpRequest req = new HttpRequest();
        Application_Log__c appLog= new Application_Log__c();
        appLog.Record_Object_Type__c = 'Customer';
        appLog.Source_Class__c = 'CustomerServiceRequestHandler';
        appLog.Source_Function__c = 'SendHttpRequest';
        appLog.Source__c = 'SF';
        appLog.Timestamp__c =  System.now();
        applog.Integration_Payload__c= reqString;
        HTTPResponse resp= null;
        Http http = new Http();
        req.setMethod('POST');
              
        String url = CSI.URL__c;
        String integrationUsername = CSI.Username__c; 
        String integrationPassword = CSI.Password__c;
        
        if(Test.isRunningTest())
        {
            req.setEndpoint('https://login.salesforce.com');
        }
        else
        {
            req.setEndpoint(url);
        }
        appLog.Integration_Resource__c= url;
        
        
        Blob headerValue = Blob.valueOf(integrationUsername+ ':' + integrationPassword);
        String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('content-type', 'application/json');
        req.setHeader('Accept', 'application/json'); 
       
        req.setClientCertificateName(CSI.Certificate__c);
        req.setBody(reqString);
        System.debug('**********REQUEST to EAI :'+reqString);
        System.debug('********** Complete REQUEST '+req);
        if (!Test.isRunningTest()){
            resp = http.send(req);
            
            system.debug('--------'+resp.getBody());
            
           CustomerServiceResponse custResponse = CustomerServiceResponse.parse(resp.getBody());
           
           System.debug('====custResponse===='+custResponse);
        /* A ==Rohit's Code Start ==  
           Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(resp.getBody());
           Map<String, Object> results1 = (Map<String, Object>)results.get('CustomerResponse');
           system.debug('Results1---'+results1.values());
           Map<String, Object> results2 = (Map<String, Object>)results1.get('CustomerPartners');
           system.debug('Results2---'+results2.values());
           CustomerServiceResponse.SalesOrganizationPartners stPart= (CustomerServiceResponse.SalesOrganizationPartners)JSON.deserialize(String.Valueof(results2),CustomerServiceResponse.SalesOrganizationPartners.class);
           //List<CustomerServiceResponse.SalesOrganizationPartners> results3 = (List<CustomerServiceResponse.SalesOrganizationPartners>)results2.get('SalesOrganizationPartners');
           system.debug('Results3---'+stPart);
           //Map<String, Object> results4 = (Map<String, Object>)results3.get('ShipToPartner');
           //system.debug('----ShipToPartner--'+results4.values());
           
           
            
            List<Object> custRespList = (List<Object>)results.get('ShipToPartner');
            System.debug('-----------custRespList'+custRespList);
            CustomerServiceResponse.SalesOrganizationPartners localSalesOrg = 
            (CustomerServiceResponse.SalesOrganizationPartners)results2.get('SalesOrganizationPartners');
            List<CustomerServiceResponse.ShipToPartner> localShiptoPartnerlst = 
            (List<CustomerServiceResponse.ShipToPartner>)localSalesOrg.ShipToPartner;
            
            system.debug('Passed list'+localShiptoPartnerlst); 
           == Rohit's Code End== A */
            
        /*A    CustomerServiceResponse.ShipToPartner localShiptoPartner = custResponse.CustomerResponse.CustomerPartners.SalesOrganizationPartners.ShipToPartner;
            
            st.addressLine1 = localShiptoPartner.PartnerAddress.addressLine1;
            st.addressLine2= localShiptoPartner.PartnerAddress.addressLine2;
            st.cityName= localShiptoPartner.PartnerAddress.cityName;
            st.stateCode = localShiptoPartner.PartnerAddress.stateCode;
            st.postalCode= localShiptoPartner.PartnerAddress.postalCode;
            st.countryCode= localShiptoPartner.PartnerAddress.countryCode;
            st.PartnerID = localShiptoPartner.PartnerId.Id;
            st.PartnerName= localShiptoPartner.PartnerName.FirstName;*/
           
           
          /*A String respString = '{"CustomerResponse":{"CustomerPartners":{"SalesOrganizationPartners":{"SalesOrganization":"US10","StockLocationPartner":{"partnerID":{"ID":"0000127290"},"partnerName":{"firstName":"Todd Lossman, FE"},"partnerAddress":{"addressLine1":"","addressLine2":"","cityName":"Natick","stateCode":"MA","countryCode":"US","postalCode":""},"defaultPartner":"","legalStatus":"LK"},"StockLocationPartner":{"partnerID":{"ID":"0000127289"},"partnerName":{"firstName":"Todd, Lossman, CI"},"partnerAddress":{"addressLine1":"","addressLine2":"","cityName":"Natick","stateCode":"MA","countryCode":"US","postalCode":""},"defaultPartner":"","legalStatus":"LK"},"ShipToPartner":[{"partnerID":{"ID":"0000127289"},"partnerName":{"firstName":"Todd, Lossman, CI"},"partnerAddress":{"addressLine1":"","addressLine2":"","cityName":"Natick","stateCode":"MA","countryCode":"US","postalCode":""},"defaultPartner":"","legalStatus":"LK"},{"partnerID":{"ID":"0000127288"},"partnerName":{"firstName":"John11"},"partnerAddress":{"addressLine1":"","addressLine2":"","cityName":"Valencia","stateCode":"","countryCode":"US","postalCode":"91355"},"defaultPartner":"","legalStatus":"LK"}]}}}}';
           CustomerServiceResponse custResponse = CustomerServiceResponse.parse(respString);
            list<CustomerServiceResponse.ShipToPartner> localShiptoPartnerlst1 = custResponse.CustomerResponse.CustomerPartners.SalesOrganizationPartners.ShipToPartner;
           CustomerServiceResponse.ShipToAddress st = new CustomerServiceResponse.ShipToAddress(); */
           
           CustomerServiceResponse.ShipToAddress st = new CustomerServiceResponse.ShipToAddress();
           for(CustomerServiceResponse.SalesOrganizationPartners SOP : custResponse.CustomerResponse.CustomerPartners.SalesOrganizationPartners){
               for(CustomerServiceResponse.ShipToPartner localShiptoPartner : SOP.ShipToPartner){
                    st = new CustomerServiceResponse.ShipToAddress();
                    st.addressLine1 = localShiptoPartner.PartnerAddress.addressLine1;
                    st.addressLine2= localShiptoPartner.PartnerAddress.addressLine2;
                    st.cityName= localShiptoPartner.PartnerAddress.cityName;
                    st.stateCode = localShiptoPartner.PartnerAddress.stateCode;
                    st.postalCode= localShiptoPartner.PartnerAddress.postalCode;
                    st.countryCode= localShiptoPartner.PartnerAddress.countryCode;
                    st.PartnerID = localShiptoPartner.PartnerId.Id;
                    st.PartnerName= localShiptoPartner.PartnerName.FirstName;
                    st1.add(st);
               }
           }
           
        /*A Ashish Code   list<CustomerServiceResponse.ShipToPartner> localShiptoPartnerlst = custResponse.CustomerResponse.CustomerPartners.SalesOrganizationPartners.ShipToPartner;
           
            for(CustomerServiceResponse.ShipToPartner localShiptoPartner : localShiptoPartnerlst){
            
                st = new CustomerServiceResponse.ShipToAddress();
                st.addressLine1 = localShiptoPartner.PartnerAddress.addressLine1;
                st.addressLine2= localShiptoPartner.PartnerAddress.addressLine2;
                st.cityName= localShiptoPartner.PartnerAddress.cityName;
                st.stateCode = localShiptoPartner.PartnerAddress.stateCode;
                st.postalCode= localShiptoPartner.PartnerAddress.postalCode;
                st.countryCode= localShiptoPartner.PartnerAddress.countryCode;
                st.PartnerID = localShiptoPartner.PartnerId.Id;
                st.PartnerName= localShiptoPartner.PartnerName.FirstName;
                st1.add(st);
            }   Ashish Code A */
           //A st1.add(st);
           
        }
        else
        {
            resp = new HTTPResponse();
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"ResponseDescription":"Success","Acknowledgement":"Error"}');
            resp.setStatusCode(200);            
        }
     
     //logging response
     appLog.Severity__c = 'info';
     Schema.DescribeFieldResult aField = Application_Log__c.Message__c.getDescribe();
     Integer lengthOfField = aField.getLength();
     System.debug('Length of Message Field :'+lengthOfField);
     appLog.Message__c = (resp.getBody().Length()>lengthOfField)?resp.getBody().subString(0,lengthOfField):resp.getBody();
     appLogs.add(appLog);
    //A insert appLogs;
     //CustomerServiceResponse.ShipToAddress st = new CustomerServiceResponse.ShipToAddress();
     
    return st1;
   
}    
     
}
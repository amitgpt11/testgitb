/**
* Trigger Handler class for the RevenueSchedule Sobject
*
* @Author Vikas Malik
* @Date 2016-07-01
*/
public with sharing class RevenueScheduleTriggerHandler extends TriggerHandler{
	
	final RevenueScheduleManager manager = RevenueScheduleManager.getInstance();
	
	/**
    *  @desc    Bulk before method
    */
    public override void bulkBefore() {
    }

    /**
    *  @desc    before Insert method
    */
    public override void beforeInsert(SObject obj) {
    }
    
  /**
    * @desc after Insert method
    */    
    
    public override void afterInsert(SObject newObj) {
    }/**/   
    
    /**
    *  @desc    before Delete method
    */
    public override void beforeDelete(SObject obj) {
    }   
    
    /**
    *  @desc    after Delete method
    */
    public override void afterDelete(SObject obj) {
    }   
    
    /**
    *  @desc    after undelete method
    */
    public override void afterUndelete(SObject obj) {
    }   
    
   
    

    /**
    * @desc before Update method
    */
    public override void beforeUpdate(SObject oldObj, SObject newObj) {
    }

    public override void bulkAfter() {
    	// Call only when the schedule is being inserted manually and not via the Rev Detail Trigger
    	if ((!RevenueDetailsManager.isRevDtlMgr || OpportunityManager.updateRevRecord) && !Trigger.isDelete){
    		manager.updateRevenueAmount(Trigger.newMap,Trigger.oldMap);
    	}
    	
    	if (!RevenueDetailsManager.isRevDtlMgr && Trigger.isDelete){
    		manager.processSchDelete(Trigger.old);
    	}
    }
    
    /**
    * @desc after Update method
    */
    public override void afterUpdate(SObject oldObj, SObject newObj) {
    }   
    
    /**
    * Called at end of trigger
    * 
    * @param void
    */
    public override void andFinally() {
    	manager.commitRevenueDetails();
    }
}
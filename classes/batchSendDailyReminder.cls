/*
 @CreatedDate     16 June 2016                                  
 @author          Ashish-Accenture
 @Description     Batch class is used to send daily reminders
       
*/

public class batchSendDailyReminder implements Database.Batchable<sObject>, Schedulable, database.stateful{
    
    // Schedulable method, executes the class instance
    
    Public void execute(SchedulableContext context) {
        Database.executeBatch(new batchSendDailyReminder(), 200);
    }
    
    Set<Id> UserIdset;
    Map<String, list<PI_Lutonix_SRAI__c>> mapOwnerHeader;
    map<String,list<PI_Lutonix_SRAI_Detail__c>> mapHeaderDetail;
    
    public batchSendDailyReminder(){
        UserIdset = new Set<Id>();
        UserIdset = UtilityController.GetUserIdsFromGroup(label.Public_Group_For_Daily_Reminder);
         
        Map<integer, string> mapMonthNumber =  UtilityController.getMapMonthNumber();
        
        Date todaydate = Date.Today();
        String CurrentMonth = mapMonthNumber.get(todaydate.month());
        String currentYear = String.valueOf(todaydate.Year());
        Map<id, PI_Lutonix_SRAI__c> mapHeaderRec = new Map<id, PI_Lutonix_SRAI__c>([Select id,CreatedDate, ownerid,PI_Partial_Month__c,PI_Month__c,PI_Year__c from PI_Lutonix_SRAI__c where ownerId in: UserIdset AND PI_Month__c =:CurrentMonth AND PI_Year__c =: currentYear ]);
        
        Map<id,PI_Lutonix_SRAI_Detail__c> mapDetailRec = new Map<id,PI_Lutonix_SRAI_Detail__c>([Select id, PI_Date__c,PI_Lutonix_SRAI__c,PI_Recorded_Temperature__c,PI_Temperature_Scale__c,PI_Units_of_Trunk_Stock__c from PI_Lutonix_SRAI_Detail__c where PI_Lutonix_SRAI__c=: mapHeaderRec.keyset() AND PI_Date__c =:todaydate ]);
        
        mapOwnerHeader = new Map<String, list<PI_Lutonix_SRAI__c>>();
        for(PI_Lutonix_SRAI__c head : mapHeaderRec.Values()){
            if(mapOwnerHeader.get(head.ownerId) != null){
                list<PI_Lutonix_SRAI__c> templist = new list<PI_Lutonix_SRAI__c>();
                templist = mapOwnerHeader.get(head.ownerId);
                templist.add(head);
                mapOwnerHeader.put(head.ownerId,templist);
            }else{
                mapOwnerHeader.put(head.ownerId,new list<PI_Lutonix_SRAI__c>{head});
            }
        }
        
        mapHeaderDetail = new Map<String,list<PI_Lutonix_SRAI_Detail__c>>();
        for(PI_Lutonix_SRAI_Detail__c det : mapDetailRec.values()){
            if(mapHeaderDetail.get(det.PI_Lutonix_SRAI__c) != null){
                list<PI_Lutonix_SRAI_Detail__c> templist = new list<PI_Lutonix_SRAI_Detail__c>();
                templist = mapHeaderDetail.get(det.PI_Lutonix_SRAI__c);
                templist.add(det);
                mapHeaderDetail.put(det.PI_Lutonix_SRAI__c,templist);
            }else{
                mapHeaderDetail.put(det.PI_Lutonix_SRAI__c, new list<PI_Lutonix_SRAI_Detail__c>{det});
            }
        }
    }
    
    //Start method, fetch user records from public group 
    public Database.QueryLocator start(Database.BatchableContext BC) {
       
       String query = 'Select id,name,email from User where id in : UserIdSet';
        return database.getquerylocator(query);
    }
    
    //Identifies whether rep has created today's detail record. If not created, System will send reminder mail
    public void execute(Database.BatchableContext BC, List<User> scope) {
         
         list<EmailTemplate> emailTempl = [ select id, name, body, subject, HTMLValue  from EmailTemplate where name='Daily Reminder For SRAI Temperature Log'];
        boolean returnFlag;
        if(emailTempl.size() > 0){
            list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();    
            Messaging.SingleEmailMessage mail;
            for(User u : scope){
                
                boolean isMailNeeded = false;
                
                if(mapOwnerHeader != null && mapOwnerHeader.get(u.id) != null){
                    for(PI_Lutonix_SRAI__c head : mapOwnerHeader.get(u.id)){
                        if(mapHeaderDetail == null || mapHeaderDetail.get(head.id) == null){
                            isMailNeeded = true;
                            break;
                        }
                        
                    }
                }else{
                    isMailNeeded = false;
                }
                if(!isMailNeeded){
                    Continue;
                }
                
                mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(emailTempl.get(0).id);
                mail.setTargetObjectId(u.Id);
                mail.setSaveAsActivity(false);
                mail.setSenderDisplayName('Bsci Admin');
                mails.add(mail);
            }
            
            if(mails.Size() > 0){
            List<Messaging.SendEmailResult> results;
                try{
                    results = Messaging.sendEmail(mails);
                    System.debug('======results ===='+results );
                    
                }Catch(exception e){
                    System.debug('####===e==='+e);
                }
            }
                 
       }
         
     }
     
      public void finish(Database.BatchableContext BC){
    
      }
    
}
public with sharing class ContactRedirectIfLicensed_Local {

    public Contact c {get;set;}

    public ContactRedirectIfLicensed_Local(ApexPages.StandardController controller){
    
        c = (Contact) controller.getRecord();
    }
    
    public PageReference redirect(){
        
        if(c != null){
            
            Map<String, openq__Standard_Layout_Contact_Record_Types__c> stConRts = openq__Standard_Layout_Contact_Record_Types__c.getAll();
            
            List<RecordType> rt = [Select Name from RecordType where Id = :c.RecordTypeId limit 1];
            
            List<openq__Category__c> cat = [Select Id from openq__Category__c where openq__Api_Name__c = 'Contact' limit 1];
            
            if(!cat.isEmpty() && !rt.isEmpty()){
                
                String rtName = rt.get(0).Name;
                
                List<openq__Page_Layout__c> pls = [Select Id from openq__Page_Layout__c where openq__Category__c = :cat.get(0).Id and openq__Record_Type_Name__c = :rtName limit 1];
                
                if(!pls.isEmpty()){
                
                    if(UserInfo.isCurrentUserLicensed('openq') && !rt.isEmpty() && !stConRts.keySet().contains(rt.get(0).Name)){
                        
                        PageReference visualforceContactDetail = new PageReference('/apex/openq__CoDi');
                        visualforceContactDetail.getParameters().put('id',c.Id);
                        visualforceContactDetail.setRedirect(true);
                        return visualforceContactDetail;            
                    }
                }
            }
        }
            
       return null;
    }
}
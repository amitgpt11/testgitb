public with sharing class AssignTerritorytoOpportunityHandlerBulk extends TriggerHandler {
     //public List<Opportunity> lstoldopptys = (List<Opportunity>)trigger.old;
        
    public List<Opportunity> lstnewopptys = (List<Opportunity>)trigger.new;
        
    public void assignTerritoryForOpportunity(boolean isInsert) {
    
    
    UtilForAssignTerritorytoOpportunity utilAssignTerritory = new UtilForAssignTerritorytoOpportunity();
    utilAssignTerritory.utilassignTerritoryForOpportunity(isInsert,lstnewopptys);

    }
}
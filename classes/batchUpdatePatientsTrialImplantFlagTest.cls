@isTest(SeeAllData=false)
@testVisible
public class batchUpdatePatientsTrialImplantFlagTest {
    
    public static List<opportunity> setupOppty(){
        NMD_TestDataManager td= new NMD_TestDataManager();
        Account acc = td.newAccount('TestAcc');
        insert acc;
        Patient__c p = td.newPatient('Sam','Ronny');
        p.Trial_Yes__c= false;
        p.Implant_Yes__c= false;
        insert p;
        Opportunity testopp1 = td.newOpportunity(acc.id);
        testopp1.Patient__c =p.Id;
        testopp1.RecordTypeId= OpportunityManager.RECTYPE_TRIAL_NEW;
        testopp1.Actual_Trial_Date__c = date.today();         
        
        
        
        Opportunity testopp2 = td.newOpportunity(acc.id);
               
        testopp2.Patient__c = p.Id;
        testopp2.Actual_Procedure_Date__c= date.today();
        testopp2.RecordTypeId=OpportunityManager.RECTYPE_IMPLANT;
        
        List<opportunity> oList= new List<Opportunity> { testopp1, testopp2 };
            insert oList;
			return oList;        
        }
    
      static testMethod void test_PatientInfo() {
          List<Opportunity> opptys=setupOppty();
          Test.StartTest();
              batchUpdatePatientsTrialImplantFlag buptir = new batchUpdatePatientsTrialImplantFlag();
              Database.executeBatch(buptir);
          Test.StopTest(); 
      }
          
      
  

}
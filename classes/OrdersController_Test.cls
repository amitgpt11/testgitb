@isTest
private class OrdersController_Test {

	private static testMethod void test() {
	    
	    
	    system.Test.setCurrentPage(Page.Orders);
	    OrdersController objOrdersController = new OrdersController();
	    
	    //Create Products
        Test_DataCreator.createProducts();
        
        User objUser = Test_DataCreator.createCommunityUser(); 
        
        system.runAs(objUser) {
            
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            objOrder.Status__c = 'Open';
            update objOrder;
            
    	    Id productId = [Select Id From Product2 Limit 1].Id;
    	    Shared_Community_Order_Item__c objOrderItem = Test_DataCreator.createOrderItem(objOrder.Id, productId);
    	    
    	    objOrdersController.init();
            system.debug('## objOrdersController.lstOrderItem : '+objOrdersController.lstOrderItem);
    	    system.assertNotEquals(0, objOrdersController.lstOrderItem.size());
    	    
    	    system.assertequals(objOrdersController.goToCheckout().getUrl(),'/Checkout?orderId='+objOrder.Id);
    	    
    	    objOrdersController.updateOrderItem();
    	    
    	    objOrdersController.index = 0;
    	    objOrdersController.deleteOrderItem();
    	    system.assertEquals(1, objOrdersController.lstOrderItemToDelete.size());
        }
	}
}
@IsTest(SeeAllData=false)
private class InventoryItemTriggerHandlerTest {

	final static NMD_TestDataManager td = new NMD_TestDataManager();
	final static String ACCTNAME = 'ACCTNAME';
	
	@TestSetup
	static void setup() {

		Product2 prod0 = td.newProduct();
	    insert prod0;

		Account acct1 = td.newAccount(ACCTNAME);
		acct1.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
		insert acct1;

		// create one inv data per acct
		Inventory_Data__c invData1 = new Inventory_Data__c(
			Account__c = acct1.Id,
			Customer_Class__c = 'YY'
		);
		insert invData1;

		// create one inv location per inv data
		Inventory_Location__c invLoc1 = new Inventory_Location__c(
			Inventory_Data_ID__c = invData1.Id
		);
		insert invLoc1;

		// create one inventory item per product, alternative between inv locations
		Inventory_Item__c invItem1 = new Inventory_Item__c(
			Inventory_Location__c = invLoc1.Id,
			Model_Number__c = prod0.Id,
			Serial_Number__c = 'SERIAL1'
		);
		insert invItem1;

		// cycle count tree
		Cycle_Count_Response_Analysis__c ccra = new Cycle_Count_Response_Analysis__c(
			Name = 'CCRANAME',
			Status__c = 'Scanning'
		);
		insert ccra;

		Cycle_Count_Line_Item__c ccItem0 = new Cycle_Count_Line_Item__c(
			Inventory_Item__c = invItem1.Id,
			Cycle_Count_Response_Analysis__c = ccra.Id
		);
		Cycle_Count_Line_Item__c ccItem1 = new Cycle_Count_Line_Item__c(
			Inventory_Item__c = invItem1.Id,
			Cycle_Count_Response_Analysis__c = ccra.Id
		);
		insert new List<Cycle_Count_Line_Item__c> { ccItem0, ccItem1 };

	}

	static testMethod void test_InventoryItemTriggerHandler() {

		Inventory_Item__c item = [
			select Id, SAP_Quantity__c 
			from Inventory_Item__c 
			where Inventory_Location__r.Inventory_Data_ID__r.Account__r.Name like :(ACCTNAME + '%')
		];

		item.SAP_Quantity__c = 10;
		update item;

	}

}
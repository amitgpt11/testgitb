/*
 * Description : To Fetch and show the Feed Attachment from divisional attchment object and also if we delete any attachment it will delete the feed item too.
 * Created By : Accenture Team
 * Created Date : 13th Oct, 2016
*/
public without Sharing class Shared_ChatterUploadController{
    public string cid {get; set;}    
    public id docid {get; set;}
    public boolean isDivisional {get; set;}
    public List<FeedItem> feeditemlist {get; set;}
    public List<FeedItem> feeditemlistDeleteFinal = new List<FeedItem>();
    //public List<FeedAttachment> feedAttachlist = new List<FeedAttachment>();
    //public list<Shared_Private_Attachments__c> shareAttList = new List<Shared_Private_Attachments__c>();
    //public set<Id> feeditemlistToDelete = new set<Id>();
    public map<Id,Shared_Private_Attachments__c> divAttchMap;
    public set<string> divisionSet = new set<string>();
    
    public Shared_ChatterUploadController(ApexPages.StandardController controller){
        cid = ApexPages.currentPage().getParameters().get('id');
        system.debug('****Account or Divisional***'+cid);
        
        feeditemlist = new list<FeedItem>();
        DivisionalAttachmentConfiguration__c DAC;
        if(DivisionalAttachmentConfiguration__c.getValues(UserInfo.getUserId()) != null){
            DAC = DivisionalAttachmentConfiguration__c.getValues(UserInfo.getUserId());
        }else if(DivisionalAttachmentConfiguration__c.getValues(UserInfo.getProfileId()) != null){
            DAC = DivisionalAttachmentConfiguration__c.getValues(UserInfo.getProfileId());
        }else{
            DAC = DivisionalAttachmentConfiguration__c.getOrgDefaults();
        }
        
        if(DAC != null && String.isNotBlank(DAC.PI_Division__c)){            
            for(String val: DAC.PI_Division__c.Split(';')){
                if(String.isNotBlank(val)){
                    divisionSet.add(val);
                }
            }
             
        }
        system.debug('@@@divisionSet-->'+divisionSet);  
        if(cid != null && divisionSet != null){  
            divAttchMap = new map<Id,Shared_Private_Attachments__c>([SELECT Id,Account__c,ICPI_Division__c FROM Shared_Private_Attachments__c WHERE Account__c =: cid AND ICPI_Division__c IN: divisionSet]);
        }
        system.debug('****divAttchMap ***'+divAttchMap );
        system.debug('****divAttchMap size***'+divAttchMap.size() );
        
        if(divAttchMap != null && divAttchMap.size() >0){
            feeditemlist = [SELECT Id, ParentId, Title, CreatedBy.Name,(SELECT Id, FeedEntityId, RecordId, IsDeleted FROM FeedAttachments where RecordId != null), LastModifiedDate, RelatedRecordId FROM FeedItem where ParentId IN:divAttchMap.keyset()  AND IsDeleted = false];
            system.debug('***Item**'+feeditemlist);
        }
        
            //feedAttachlist = [SELECT Id, FeedEntityId, RecordId, IsDeleted FROM FeedAttachment where FeedEntityId =:feeditemlist];
            //system.debug('***feedAttachlist**'+feedAttachlist);
    }
    
    /*public Void insertinCustom(){}
    
    public PageReference insertinCustom1(){
        return new pageReference('/'+cid);
    }*/
    
    public void onLoad(){
        if(!feeditemlist.isEmpty()){
            for(FeedItem f : feeditemlist){
                if(f.FeedAttachments.isEmpty()){
                    feeditemlistDeleteFinal.add(f);
                }
            }
            system.debug('***feeditemlistDeleteFinal**'+feeditemlistDeleteFinal);
        }
        if(!feeditemlistDeleteFinal.isEmpty()){
            Database.delete (feeditemlistDeleteFinal, false);
        }
    }
}
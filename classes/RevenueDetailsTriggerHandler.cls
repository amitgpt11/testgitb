/**
* Trigger Handler class for the RevenueDetails Sobject
*
* @Author Vikas Malik
* @Date 2016-06-08
*/
public with sharing class RevenueDetailsTriggerHandler extends TriggerHandler{
	
	final RevenueDetailsManager manager = RevenueDetailsManager.getInstance();
	
	/**
    *  @desc    Bulk before method
    */
    public override void bulkBefore() {
    	manager.fetchDetailsPerOpportunity(Trigger.new);
    }

    /**
    *  @desc    before Insert method
    */
    public override void beforeInsert(SObject obj) {
    	manager.validateInsert((Opportunity_Revenue_Schedule__c) obj);
    }
    
  /**
    * @desc after Insert method
    */    
    
    public override void afterInsert(SObject newObj) {
    	manager.processScheduleDetails((Opportunity_Revenue_Schedule__c) newObj,null);
    	manager.updateOpportunityAmount((Opportunity_Revenue_Schedule__c) newObj,null);
    }/**/   
    
    /**
    *  @desc    before Delete method
    */
    public override void beforeDelete(SObject obj) {
    }   
    
    /**
    *  @desc    after Delete method
    */
    public override void afterDelete(SObject obj) {

        
        
    }   
    
    /**
    *  @desc    after undelete method
    */
    public override void afterUndelete(SObject obj) {
    }   
    
   
    

    /**
    * @desc before Update method
    */
    public override void beforeUpdate(SObject oldObj, SObject newObj) {
    }

    public override void bulkAfter() {
    	manager.fetchScheduleDetails(Trigger.new);
    }
    
    /**
    * @desc after Update method
    */
    public override void afterUpdate(SObject oldObj, SObject newObj) {
    	// Only process creation od schedule records if the revenue amount is changed on the Detail record not on the shedule record
    	if ((!RevenueScheduleManager.isScheduleUpdate && !RevenueScheduleManager.isScheduleDelete) || OpportunityManager.updateRevRecord){
    		manager.processScheduleDetails((Opportunity_Revenue_Schedule__c) newObj,(Opportunity_Revenue_Schedule__c) oldObj);
    	}
    	manager.updateOpportunityAmount((Opportunity_Revenue_Schedule__c) newObj,(Opportunity_Revenue_Schedule__c) oldObj);
    }   
    
    /**
    * Called at end of trigger
    * 
    * @param void
    */
    public override void andFinally() {
    	manager.deleteSchedules();
    	manager.commitSchedules();
    	manager.commitOpportunities();
    	manager.commitOpportunityLineItems();
    }
    
}
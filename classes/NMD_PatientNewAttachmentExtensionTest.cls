/**
* Test class for the VF/Apex combo PatientNewAttachment/PatientNewAttachmentExtension
*
* @Author salesforce Services
* @Date 2015-03-04
*/
@isTest(SeeAllData=false)
private class NMD_PatientNewAttachmentExtensionTest {

	final static String FNAME = 'FNAME';
	final static String LNAME = 'LNAME';
	
	@testSetup
	static void setup() {
		
		NMD_TestDataManager tdm = new NMD_TestDataManager();

		Patient__c patient = tdm.newPatient(FNAME, LNAME);
		insert patient;

		Account acct = tdm.createConsignmentAccount();
		acct.Name = FNAME;
		insert acct;

		Opportunity oppty = tdm.newOpportunity(acct.Id);
		oppty.Patient__c = patient.Id;
		insert oppty;

	}

	static testMethod void test_OpportunityRedirect() {

		Opportunity oppty = [select Patient__c from Opportunity where Account.Name = :FNAME];

		PageReference pr = Page.PatientNewAttachment;
		pr.getParameters().put('id', oppty.Id);
		Test.setCurrentPage(pr);

		NMD_PatientNewAttachmentExtension ext = new NMD_PatientNewAttachmentExtension(
			new ApexPages.StandardController(oppty)
		);

		ext.doRedirect();

	}

	static testMethod void test_PatientNewAttachment() {

		Patient__c patient = [select Id from Patient__c where Patient_Last_Name__c = :LNAME];

		PageReference pr = Page.PatientNewAttachment;
		pr.getParameters().put('id', patient.Id);
		pr.getParameters().put('retURL', '/home/home.jsp');
		Test.setCurrentPage(pr);

		NMD_PatientNewAttachmentExtension ext = new NMD_PatientNewAttachmentExtension(
			new ApexPages.StandardController(patient)
		);

		ext.checkRedirect();

		ext.classification = 'classification';

	}

}
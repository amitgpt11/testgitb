/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
    public String loginError{get;set;}

    global PageReference login() {
        
        loginError = '';
        PageReference result = null;
        
        if(String.isBlank(username) || String.isBlank(password)){
            loginError = Label.LoginError;
            return result;
        }
        else{
        
            String startUrl = System.currentPageReference().getParameters().get('startURL');
            result =  Site.login(username, password, startUrl);
            
            if(result == null && String.isBlank(loginError)){
                loginError = Label.Login_attempt_has_failed;
            }
            
            return result;
        }
    }
    global SiteLoginController(){
    }
}
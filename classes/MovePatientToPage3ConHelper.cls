global class MovePatientToPage3ConHelper {
static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
public static final Id RECTYPE_Trial_Implant = RECTYPES_OPPTY.get('NMD SCS Trial-Implant').getRecordTypeId();
Public static final String EDITACCESS = 'Edit';

  //Patients Record Types
       public static final Set<Id> RECTYPES_NMD_PATIENTS = new Set<Id> {
            PatientManager.RECTYPE_CUSTOMER, PatientManager.RECTYPE_PROSPECT
        };  


//Lead Record Types
        static final Map<String,Schema.RecordTypeInfo> RECTYPES_LEAD = Schema.SObjectType.Lead.getRecordTypeInfosByName();  
        
        public static final Id RECTYPE_NMD_PATIENTS = RECTYPES_LEAD.get('NMD Patient').getRecordTypeId();
       
           static final Set<Id> RECTYPE_NMD_PATIENTS_LEAD = new Set<Id> {
            RECTYPE_NMD_PATIENTS
        };
        
        static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};  
        

/*----------- patient , lead  update and oppyrtunity insertion ---------------------------------------------------*/

 static public void movePatientsOnly(set<id>  contactIds1)
{
    
    List<Patient__c> pNewList = new List<Patient__c>();
    List<Patient__c> patientList = new List<Patient__c>();
    Map<Id,Patient__c> pNewMap = new Map<Id,Patient__c>();
    Map<Id,Id> patientToTerritoryMap = new Map<Id,Id>();
    List<Opportunity> newOpptyList = new List<Opportunity>();
    List<Lead> newLeadList = new List<Lead>();
    Map<Id,Id> pOldTerritoryIdbyPId = new Map<Id,Id>();
    set<Id> LeadOldTerritory = new Set<Id>();
    set<Id> OpptyOldTerritory = new Set<Id>();
    Map<id,List<Assignee__c>> mapTerritoryWiseAssignee = new Map<id,List<Assignee__c>>();
    //Set<id> ptAssigneeList = new Set<id>(); 
    Set<id> patientAssigneeUserList = new Set<id>();
    Set<id> opttyAssigneeUserList = new Set<id>();
    Set<id> leadAssigneeUserList = new Set<id>();
    set<id> patientAssigneeIdSet = new set<id>();
    set<id> opttyAssigneeIdSet = new set<id>();
    set<id> leadAssigneeIdSet = new set<id>();
    Map<Id,Id> terrIdbyContactId = new Map<Id,Id>(); 
    set<id> selectedPateintTerritoryId = new set<id>();
    Map<id,id> selectdPatientNewOwnerMap = new  Map<id,id>();
    
   // patientList =[SELECT Id, name,Territory_ID__c,Territory_ID__r.Current_TM1_Assignee__c, Physician_of_Record__c,Physician_of_Record__r.Territory_ID__c,Physician_of_Record__r.Territory_ID__r.Current_TM1_Assignee__c,ownerid FROM Patient__c where Physician_of_Record__c in :contactIds1 and RecordTypeId in : RECTYPES_NMD_PATIENTS ];
   
   patientList =[SELECT Id, name,Territory_ID__c,Territory_ID__r.Current_TM1_Assignee__c, Physician_of_Record__c,Physician_of_Record__r.Territory_ID__c,Physician_of_Record__r.Territory_ID__r.Current_TM1_Assignee__c,ownerid FROM Patient__c where Physician_of_Record__c in :contactIds1 ];
    //Prepare contact map contactMap
    //terrIdbyContactId =new Map<Id,Id>([Select Id,Territory_ID__c From Contact where Id in:contactIds1 ]);
    

// Rowcause logic starts        
    
RowCauseObjects__c RowCauseObj = RowCauseObjects__c.getInstance('Opportunity');
String      RowCauseName      = RowCauseObj.RowCauses__c;
system.debug('___RowCauseName__'+RowCauseName);
List<String>     lstRowCauseOppy=  RowCauseName.split(',');
//lstRowCauseOppy.add(RowCauseName);
system.debug('======lstRowCauseOppy'+lstRowCauseOppy); 

//  Patient 
RowCauseObjects__c RowCauseObjPat = RowCauseObjects__c.getInstance('Patient');
String      RowCauseNamePatient      = RowCauseObjPat.RowCauses__c;
system.debug('RowCauseNamePatient'+RowCauseNamePatient);
List<String>     lstRowCausePatient= RowCauseNamePatient.split(',');
//lstRowCausePatient.add(RowCauseNamePatient);
system.debug('======lstRowCausePatient'+lstRowCausePatient); 

//Lead 

RowCauseObjects__c RowCauseObjLead = RowCauseObjects__c.getInstance('Lead');
String      RowCauseNameLead      = RowCauseObjLead.RowCauses__c;
system.debug('___RowCauseNameLead__'+RowCauseNameLead);
List<String>     lstRowCauseLead = RowCauseNameLead.split(',');
//lstRowCauseLead.add(RowCauseNameLead);
system.debug('======lstRowCauseLead'+lstRowCauseLead); 

//Contact
RowCauseObjects__c RowCauseObjContact = RowCauseObjects__c.getInstance('Contact');
String      RowCauseNameContact      = RowCauseObjContact.RowCauses__c;
system.debug('___RowCauseNameContact__'+RowCauseNameContact);
List<String>     lstRowCauseContact =  RowCauseNameContact.split(',');
//lstRowCauseContact.add(RowCauseNameContact);
system.debug('======lstRowCauseContact'+lstRowCauseContact); 


// Rowcause logic ends      
    
    
    if(!patientList.isEmpty()){
        for(Patient__c p:patientList){
            pOldTerritoryIdbyPId.put(p.Id,p.Territory_ID__c);
            p.Territory_ID__c =p.Physician_of_Record__r.Territory_ID__c;  // terrIdbyContactId.get(p.Physician_of_Record__c);
            if(p.Physician_of_Record__r.Territory_ID__r.Current_TM1_Assignee__c!=null)
                p.OwnerId= p.Physician_of_Record__r.Territory_ID__r.Current_TM1_Assignee__c;
            pNewList.add(p);
            patientAssigneeIdSet.add(p.id);
            //selectdPatientMap.put(Pobj.id,Pobj ); 
        }
    }   
    if(pNewList.size()>0){
        
        update pNewList;
        
    }
    for(Patient__c p1:pNewList){
        pNewMap.put(p1.id,p1 );
        patientToTerritoryMap.put(p1.id,p1.Territory_ID__c );
        selectdPatientNewOwnerMap.put(p1.id,p1.Territory_ID__r.Current_TM1_Assignee__c);
        selectedPateintTerritoryId.add(p1.Territory_ID__c);
    }
    //below code to insert  patients related opptys with patients new territory into Opportunity_Territory_Realignment__c object 
    list<Opportunity_Territory_Realignment__c> otrList = new list<Opportunity_Territory_Realignment__c>();
    for (Opportunity oppty : [SELECT Id, Territory_ID__c, Patient__c, Patient__r.Physician_of_Record__c ,Patient__r.Territory_ID__r.Current_TM1_Assignee__c,ownerid FROM Opportunity where Patient__c in :pNewMap.keySet()   ])  //and RecordTypeId != :  RECTYPE_Trial_Implant
    {
        if(patientToTerritoryMap.containsKey(oppty.Patient__c)){
           /* OpptyOldTerritory.add(oppty.Territory_ID__c);
            oppty.Territory_ID__c = patientToTerritoryMap.get(oppty.Patient__c);
            //Code added by amitabh to update owner as per new territory**********
           // if(selectdPatientNewOwnerMap.get(oppty.Patient__c)!= null){
           
            if(oppty.Patient__r.Territory_ID__r.Current_TM1_Assignee__c!= null){
               // oppty.OwnerId = selectdPatientNewOwnerMap.get(oppty.Patient__c);
                oppty.OwnerId = oppty.Patient__r.Territory_ID__r.Current_TM1_Assignee__c;
            }
            newOpptyList.add(oppty); */
            
                        Opportunity_Territory_Realignment__c otsIns= new Opportunity_Territory_Realignment__c();
                    
                    otsIns.New_Territory_Patient__c = patientToTerritoryMap.get(oppty.Patient__c);
                    otsIns.Old_Territory_Patient__c=oppty.Territory_ID__c;
                    otsIns.Opportunity_Id__c =oppty.id;
                    otsIns.Patient_Id__c =oppty.Patient__c;
                    otsIns.Record_Status__c ='pending';
                    //otsIns.Remark__c  
                    otrList.add(otsIns);
            
        }   
    }
                        system.debug('***************opprotunity owner update otrList '+otrList);
                      
    if(otrList.size()>0){
    
        List<Database.SaveResult> oppourtunitySaveResults = Database.insert(otrList, false);
        for (Database.SaveResult result : oppourtunitySaveResults) {
                        //system.debug('====result oppourtunitySaveResults  =='+result );
            if (!result.isSuccess()) {
                for (Database.Error err : result.getErrors()) {
                    logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Update of Multi Physicians child opportunity Obj On NMD_ObjectTerritoryByContactBatch.execute'));
                    system.debug('***************opprotunity update error'+err);
                }
            }
        } 
        
        For(Opportunity o:newOpptyList){
            opttyAssigneeIdSet.add(o.Id);
        }
    }     /**/
    //below code to update patients related Lead with patients new territory
    for (Lead LeadObj : [SELECT Id, name, Status, Territory_ID__c ,Patient__c,Referring_Physician__c,isconverted,ownerid FROM lead where Patient__c in :pNewMap.keySet() and RecordTypeId in :RECTYPE_NMD_PATIENTS_LEAD and isconverted  = false  ]) 
    {
        if(patientToTerritoryMap.containsKey(LeadObj.Patient__c)){
                LeadOldTerritory.add(LeadObj.Territory_ID__c);
                LeadObj.Territory_ID__c = patientToTerritoryMap.get(LeadObj.Patient__c);
                //Code added by amitabh to update owner as per new territory**********
                if(selectdPatientNewOwnerMap.get(LeadObj.Patient__c)!= null){
                    LeadObj.OwnerId = selectdPatientNewOwnerMap.get(LeadObj.Patient__c);
                }
                //system.debug('~!@#$%^  leadTerritory ===='+LeadObj.Territory_ID__c );                          
                // LeadObj.Territory_ID__c = SelectedItem4;
                newLeadList.add(LeadObj);           
        }           
    }   
    if(newLeadList.size()>0){
        List<Database.SaveResult> leadSaveResults = Database.update(newLeadList, false);
        for (Database.SaveResult result : leadSaveResults) {
                  //  system.debug('====result leadSaveResults  =='+result );
            if (!result.isSuccess()) {
                for (Database.Error err : result.getErrors()) {
                    logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Update of Multi Physicians child Leads in movePatientsOpptyLead '));
                }
            }
        }   
        For(Lead l:newLeadList){
            leadAssigneeIdSet.add(l.Id);
        }
    }
    /*-----Patient sharing logic-----*/
    List<Assignee__c> ptAssigneeList1= [Select id,Name,Territory__c,Assignee__c From  Assignee__c where Territory__c in : pOldTerritoryIdbyPId.keySet()  and Assignee__r.isActive =true];
    for(Assignee__c PasObj : ptAssigneeList1)
    {
        patientAssigneeUserList.add(PasObj.Assignee__c);
            
    }
    List<Patient__Share> lstPSDel1= [SELECT AccessLevel,Id,LastModifiedById,ParentId,RowCause,UserOrGroupId FROM  Patient__Share where UserOrGroupId in : patientAssigneeUserList and ParentId in : patientAssigneeIdSet and RowCause not in : lstRowCausePatient]; 
        if(!lstPSDel1.isEmpty()){
          delete lstPSDel1;
        }
        
    //Insert Patient Share record
     set<id> pIds = new set<id>();
     list<Patient__c> pListNew = new list<Patient__c> ();
    for (Patient__c ptObj1 : [SELECT Id, name,Territory_ID__c, Physician_of_Record__c,ownerid FROM Patient__c where Physician_of_Record__c in :contactIds1 and  Territory_ID__c in : selectedPateintTerritoryId ]) {
      //  if(contactMap.containsKey(ptObj1.Physician_of_Record__c)){
          // LeadObj.Territory_ID__c = contactMap.get(LeadObj.Physician_of_Record__c).Territory_ID__c;
          //  LeadObj.Territory_ID__c = SelectedItem4;
           pIds .add(ptObj1.id);           
           pListNew .add(ptObj1);           
          
        //}           
    }
    List<Assignee__c> ptAssigneeList= [Select id,Name,Territory__c,Assignee__c From  Assignee__c where Territory__c =: selectedPateintTerritoryId and Assignee__r.isActive =true and REP_DESIGNATION__C!='TM1'];  

    set<id> ptAssigneeIdSet = new set<id>();
    List<Patient__Share> ptShareIntList = new  List<Patient__Share>();
        for(Assignee__c  assi:ptAssigneeList)
        {

            ptAssigneeIdSet.add(assi.Assignee__c);
        }
  
    List<Patient__Share > lstNewPTShareIns= [SELECT AccessLevel,Id,LastModifiedById,ParentId,RowCause,UserOrGroupId FROM  Patient__Share where UserOrGroupId in : ptAssigneeIdSet and ParentId in : pIds and RowCause not in : lstRowCausePatient];     

     
     system.debug('~~~~~~~~~~~~~~~~ptAssigneeList !!!!!!  '+ptAssigneeList+'====size==='+ptAssigneeList.size());
     system.debug('~~~~~~~~~~~~~~~~lstNewPTShareIns !!!!!!  '+lstNewPTShareIns+'====size==='+lstNewPTShareIns.size());
     
     // new code 
    for(Assignee__c asbj : ptAssigneeList){

        
        for(Patient__c l : pListNew){
        
           if(l.Territory_ID__c==asbj.Territory__c){
           Patient__Share single = new Patient__Share();

        //single.RowCause = Schema.Patient__Share.RowCause.NMDTerritoryChange; //'NMDTerritoryChange';
        single.ParentId = l.id; //'a0L11000006G6ECEA0';
        single.AccessLevel = EDITACCESS;
        single.UserOrGroupId = asbj.Assignee__c ;//'005o0000002oDQRAA2';

        ptShareIntList.add(single);
        }
             
        }

    }
    
    if(!ptShareIntList.isEmpty())
    {

    insert ptShareIntList;

    }   

    /*-----Lead sharing logic-----*/
    List<Assignee__c> ldAssigneeList= [Select id,Name,Territory__c,Assignee__c From  Assignee__c where Territory__c in : LeadOldTerritory  and Assignee__r.isActive =true];
    for(Assignee__c PasObj : ldAssigneeList)
    {
        leadAssigneeUserList.add(PasObj.Assignee__c);
            
    }
    List<LeadShare> lstPSDel2= [SELECT LeadAccessLevel,Id,LastModifiedById,LeadId,RowCause,UserOrGroupId FROM  LeadShare where UserOrGroupId in : leadAssigneeUserList and RowCause not in : lstRowCauseLead]; 
        if(!lstPSDel2.isEmpty()){
        
              delete lstPSDel2;
        }
    
    //Insert lead Share record  
    
    set<id> lIds = new set<id>();
    list<Lead> newListsLead = new list<Lead>();
    for (Lead leadObj1 : [SELECT Id, name, Status, Territory_ID__c ,Patient__c,Referring_Physician__c,ownerid,isconverted FROM lead where Patient__c in :pNewMap.keySet() and RecordTypeId in :RECTYPE_NMD_PATIENTS_LEAD and isconverted  = false and Territory_ID__c in : selectedPateintTerritoryId ]) {
        if(pNewMap.containsKey(leadObj1.Patient__c)){
          // LeadObj.Territory_ID__c = contactMap.get(LeadObj.Physician_of_Record__c).Territory_ID__c;
          //  LeadObj.Territory_ID__c = SelectedItem4;
           lIds.add(leadObj1.id);           
           newListsLead.add(leadObj1);           
          
        }           
    }
    List<Assignee__c> ledAssigneeList= [Select id,Name,Territory__c,Assignee__c From  Assignee__c where Territory__c =: selectedPateintTerritoryId and Assignee__r.isActive =true and REP_DESIGNATION__C!='TM1'];  

    set<id> ledAssigneeIdSet = new set<id>();
    List<LeadShare> ledShareIntList = new  List<LeadShare>();
        for(Assignee__c  assi:ledAssigneeList)
        {

            ledAssigneeIdSet.add(assi.Assignee__c);
        }
  
    List<LeadShare > lstNewLeadShareIns= [SELECT LeadAccessLevel,Id,LastModifiedById,LeadId,RowCause,UserOrGroupId FROM  LeadShare where UserOrGroupId in : leadAssigneeIdSet and LeadId in :lIds and RowCause not in : lstRowCauseLead];     

     
   
     //system.debug('~~~~~~~~~~~~~~~~lstNewOpptyShareIns !!!!!!  '+lstNewOpptyShareIns+'====size==='+lstNewOpptyShareIns.size());
     
     /* new code */
    for(Assignee__c asbj : ledAssigneeList){

        
        for(Lead l : newListsLead){
              if(l.Territory_ID__c==asbj.Territory__c){
        //single.RowCause = Schema.Patient__Share.RowCause.NMDTerritoryChange; //'NMDTerritoryChange';
        LeadShare single = new LeadShare();
        single.LeadId = l.id; //'a0L11000006G6ECEA0';
        single.LeadAccessLevel = EDITACCESS;
        single.UserOrGroupId = asbj.Assignee__c ;//'005o0000002oDQRAA2';

        ledShareIntList.add(single);
        }
        
        }

    }
    
    if(!ledShareIntList.isEmpty())
    {

    insert ledShareIntList;
    }

    
         List<Physician_Territory_Realignment__c> updateTerrRealList = new List<Physician_Territory_Realignment__c>();
            
            
        List<Physician_Territory_Realignment__c> ptrListUpdate =    [SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c = 'readyforpatient' and Contact__c in :contactIds1];
        
        if(!ptrListUpdate.isEmpty()){
             for(Physician_Territory_Realignment__c pr : ptrListUpdate){
             
            
                pr.Realignment_Status__c   = 'Moving To SAP'; //multiplePhysicianUpdate Moving To SAP
                
                updateTerrRealList.add(pr);
             
             }
             
             }
             if(!updateTerrRealList.isEmpty())
             update updateTerrRealList;
    
    

    }

    
}
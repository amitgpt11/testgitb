/* @ModifiedDate    4AUG2016
@author          Ashish Master
@Description     Test class for cloneOrderProcedureController */

@isTest
public class cloneOrderProcedureControllerTest {
    public static list<Account> testAccLst = new list<Account>();
    public Static Shared_Patient_Case__c UMHPatientCaseobj;
    public Static list<Order> ordlst = new list<Order>();
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Shared_Patient_Case__c.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROMENSHEALTH = RECTYPES.get('Urology Men\'s Health').getRecordTypeId();
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPESORD = Schema.SObjectType.Order.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROPHLOANERKIT = RECTYPESORD.get('Uro/PH Loaner Kit').getRecordTypeId();
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPESPRD = Schema.SObjectType.Product2.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROPHLOANERKITPRD = RECTYPESPRD.get('Uro/PH Loaner Kit').getRecordTypeId();
    
    @testSetup
    static void setupMockData(){
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId(),Account_Number__c = 'Test123',shippingCountry='US'); 
        testAccLst.add(acct1);
        insert testAccLst;   
        
         UMHPatientCaseobj = new Shared_Patient_Case__c(Shared_Facility__c = testAccLst[0].Id,Shared_Start_Date_Time__c = system.today()+6,Shared_end_Date_Time__c = system.today()+8,
                                                          RecordTypeId = RECTYPE_UROMENSHEALTH);
         insert UMHPatientCaseobj;
         
          Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
         Product2 prod = new Product2(Name = 'LaptopX200', RecordTypeId = RECTYPE_UROPHLOANERKITPRD,
                                    Family = 'Hardware',UPN_Material_Number__c = 'Test123');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                                    UnitPrice = 12000, IsActive = true);
        insert customPrice;
          
        
         Order ordObj = new Order(AccountId = testAccLst.get(0).id,Uro_PH_Patient_Case__c = UMHPatientCaseobj.id, RecordTypeId = RECTYPE_UROPHLOANERKIT,
                                  EffectiveDate= date.Today(),Status='Draft', PriceBook2Id = customPB.Id ); 
         ordlst.add(ordObj);
         Insert ordlst;
         
         OrderItem Oi = new OrderItem(Quantity = 2, OrderId = OrdObj.id, PricebookEntryId = customPrice.id,UnitPrice= 10);
         Insert Oi;
         
         UtilForUnitTestDataSetup.createBusinessCloseTimeCountry(); // Insert Custom setting Business Close Time
         UtilForUnitTestDataSetup.createCustomerServiceIntegrationDetail(); // Insert Custom setting Customer Service Integration
    }
    
    static testMethod void testMethod1(){  
        Test.StartTest();
        ordlst = [Select id,Accountid from Order];
        System.debug('===ordlst==='+ordlst);
        ApexPages.StandardController controller = new ApexPages.StandardController(ordlst.get(0));
        cloneOrderProcedureController cln = new cloneOrderProcedureController(controller);
        cln.saveClone();
        cln.callOrderRESTService();
        cln.procedureObjDef.Shared_Start_Date_Time__c = system.today();
        cln.procedureObjDef.Shared_end_Date_Time__c = system.today()+8;
        cln.saveClone();
        cln.callOrderRESTService();
        
        list<order> clonnedOrder = [Select id, AccountId from Order where id != :ordlst.get(0).id];
        System.assert(clonnedOrder.size() != 0);
        
        Profile p = [Select Id from profile where name = 'System Administrator'];
        list<User> lstUser = TestDataFactory.createTestUsers(1,p);
        insert lstUser;
        System.RunAs(lstUser.get(0)){
            RestrictedLoanerKitProduct__c res = new RestrictedLoanerKitProduct__c();
            res.Name = 'Test123';
            insert res;
            cln = new cloneOrderProcedureController(controller);
            cln.procedureObjDef.Shared_Start_Date_Time__c = system.today();
            cln.procedureObjDef.Shared_end_Date_Time__c = system.today()+8;
            cln.saveClone();
            list<orderItem> lstOrderItem = [select id from orderitem where orderid !=: ordlst.get(0).id AND orderid !=: clonnedOrder.get(0).id];
            System.assert(lstOrderItem.size() == 0);
        }
        cln.calcShippingDate();
        
        cln.procedureObjDef.Shared_Start_Date_Time__c = system.today();
        cln.procedureObjDef.Shared_end_Date_Time__c = system.today()-1;
        cln.saveClone();
        
        cln.procedureObjDef.Shared_Start_Date_Time__c = system.today()+8;
        cln.procedureObjDef.Shared_end_Date_Time__c = system.today() +9;
        cln.calcShippingDate();
       Test.StopTest(); 
    }
}
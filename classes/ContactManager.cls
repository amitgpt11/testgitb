/**
* Manager class for the Contact object
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class ContactManager {

    static final Map < String, Schema.RecordTypeInfo > RECTYPES = Schema.SObjectType.Contact.getRecordTypeInfosByName();
    static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};
    static final Set<String> profileNames = new Set<String> {
         'NMD Read Only', 'NMD Inside Sales','NMD AVP/Executive','NMD Field Rep','NMD Field Support','NMD IBU','NMD IBU-PC','NMD RBM','NMD RMM'
    };

    public static final Id RECTYPE_CUSTOMER = RECTYPES.get('Customer Physician').getRecordTypeId();
    public static final Id RECTYPE_PROSPECT = RECTYPES.get('Prospect Physician').getRecordTypeId();
    public static final Set<Id> RECTYPES_NMD = new Set<Id> {
        RECTYPE_CUSTOMER, RECTYPE_PROSPECT
    };
    
    // SFDC-1317 Start
    Private List<Id> OwnerIdList = new List<Id>();
    Public static List<User> UserDivisionList = new List<User>();
    
    static final Set<String> divisionNames = new Set<String> {
        'CA','CRM','Endo','EP','IC','LAAC','NM','PI','Uro/WH'
    };
    
    static final Set<String> ProfileNamesforEurope = new Set<String> {
        'EU Agents External','EU Customer Service','EU Field Clinical Specialists','EU HCP Manager','EU Marketing User','EU Sales Manager/Executive','EU Sales Rep','EU Sys Admin','EU Technical Support Rep','EU Telesales Rep'
    };
    // SFDC-1317 End
    
    private static String contactProfileName;
    
    private Map<Id,Integer> patientCountMap = new Map<Id,Integer>();
    private Map<Id,User> userMap = new Map<Id,User>();
    private Map<Id,Account> accountMap = new Map <Id,Account>();
    private Set<Id> updatedSAPIDs = new Set<Id>();
    
    public List<contactEmailObject> contactEmailList = new List<contactEmailObject>();

    public class contactEmailObject {
        public String contactEmailSubject { get; set; }
        public String contactEmailBody { get; set; }
        public String contactEmailAdd { get; set; }

        public contactEmailObject() {}
        public contactEmailObject(String emailSubject, String emailBody, String emailAdd) {
            contactEmailSubject = emailSubject;
            contactEmailBody = emailBody;
            contactEmailAdd = emailAdd;
        }
    }

    private NMD_AutoSubmitManager submitManager = null;


    /**
    * Initialize the NMD_AutoSubmitManager if there are records that need to be validated and submitted
    * 
    * @param void
    */  
    public void initSubmissionManager(){
        List<SObject> contactProspects = new List<SObject>();
        for(SObject obj : trigger.new){
            Contact con = (Contact)obj;
            if(con.RecordTypeId == RECTYPE_PROSPECT
                && con.Sent_to_Quincy__c == false
            ){
                contactProspects.add(obj);
            }
        }

        if(!contactProspects.isEmpty()){
            submitManager = new NMD_AutoSubmitManager();
            submitManager.init(Contact.getSObjectType(), contactProspects);
        }
    }

    /**
    * If the NMD_AutoSubmitManager is initialized, check the provided record to see if it is valid
    * 
    * @param SObject obj
    */  
    public void checkForAutoSubmit(SObject oldObj, SObject obj){
        Contact oldContact = (Contact) oldObj;
        System.debug('***Old sent: ' + oldContact.Sent_to_Quincy__c);
        if(submitManager != null
            && oldContact.Sent_to_Quincy__c != true
        ){
            submitManager.validateRecord(obj);
        }
    }

    /**
    * If the NMD_AutoSubmitManager is initialized, submit all valid records to quincy
    * 
    * @param void
    */  
    public void autoSubmitRecords(){
        if(submitManager != null){
            submitManager.queueQuincyEmails();
        }
    }


    //get related Patients
    public void fetchPatientCounts(List < Contact > contacts) {
        List < Id > contactIdList = new List < Id > ();
        for (Contact con: contacts) {
            contactIdList.add(con.Id);
        }
        if (!contactIdList.isEmpty()) {
            for (AggregateResult agr: [SELECT Physician_of_Record__c, Count(Id) FROM Patient__c where Physician_of_Record__c IN: contactIdList GROUP BY Physician_of_Record__c]) {               
                Id tempId = (Id) agr.get('Physician_of_Record__c');
                Integer tempInt = (Integer) agr.get('expr0');
                patientCountMap.put(tempId, tempInt);
            }
        }
    }
    
    // get email ID of contact owner's manager
    public void fetchContactsOwnerManager(List<Contact> contacts) {

        Set<Id> contactOwnerIds = new Set<Id>();
        for (Contact con: contacts) {
            contactOwnerIds.add(con.OwnerId);
        }
        
        if (!contactOwnerIds.isEmpty()) {
            userMap = new Map<Id,User>([SELECT Id, User_s_Manager_Email__c, name FROM USER WHERE Id in :contactOwnerIds]);
        }

    }

    // get name of realted Account
    public void fetchContactsAccounts(List < Contact > contacts) {
        List < Id > accountIdList = new List < Id > ();
        for (Contact con: contacts) {
            accountIdList.add(con.AccountId);
        }
        if (!accountIdList.isEmpty()) {
            accountMap = new Map < Id, Account > ([SELECT Id, name FROM Account WHERE Id in : accountIdList]);
        }
    }
    
    // fetch Profile Name
    public void fetchCurrentProfileName(){
        Id profileId = userInfo.getProfileId();
       contactProfileName = [Select Id, Name from Profile where Id = : profileId limit 1].Name;   
         
    }

    // Validation error when Territory ID=Null and Number of related Patients > 0
    public void checkContact(Contact oldContact, Contact contact) {
        if ((contact.Territory_ID__c == null && profileNames.contains(contactProfileName))
         && (patientCountMap.get(contact.Id) > 0)) {
            contact.Territory_ID__c.addError('To remove a Physician from your territory, all Physician of Record fields for this Physician\'s Patients must be updated.');
        }
    }

    public void checkSAPID(Contact oldContact, Contact newContact) {
        if (oldContact.SAP_ID__c != newContact.SAP_ID__c) {
            this.updatedSAPIDs.add(newContact.Id);
        }
    }

    // Send an Email to the Manager of the record owner
    public void checkContactAndSendEmail(Contact oldContact, Contact contact) {
        if ((contact.Territory_ID__c == null && oldContact.Territory_ID__c != null)
         && (patientCountMap.get(contact.Id) == null && userMap.containsKey(contact.OwnerId))) {
            //send email 
            String baseUrl = System.URL.getSalesforceBaseUrl().getHost();
            baseUrl = 'https://' + baseUrl + '/' + contact.Id;
            String emailBody = 'Please be advised that the following Physician contact has no Territory ID assigned.\n' +
                ' Note: if Physician is moving the user is responsible for manually updating open opportunities with a new Trialing and/or Procedure Physician.\n' +
                'Contact Name: ' + contact.FirstName + ' ' + contact.LastName + '\n' +
                'Account Name: ' + accountMap.get(contact.AccountId).name + '\n' +
                'Contact Owner: ' + userMap.get(contact.OwnerId).name + '\n' +
                'Contact Mailing City: ' + contact.MailingCity + '\n' +
                'Contact Mailing State: ' + contact.MailingState + '\n' +
                baseUrl;
            contactEmailList.add(new contactEmailObject('Physician Removed from Territory', emailBody, userMap.get(contact.OwnerId).User_s_Manager_Email__c));
        }
    }

    public void cascadeSAPIDInfo() {
        if (this.updatedSAPIDs.isEmpty()) return;

        List<Opportunity> opptys = new List<Opportunity>();
        Set<Id> orderIds = new Set<Id>();
        for (Opportunity oppty : [
            select 
                Trialing_Physician__c,
                Trialing_Physician__r.SAP_ID__c,
                Trialing_Physician_SAP_ID__c,
                Procedure_Physician__c,
                Procedure_Physician__r.SAP_ID__c,
                Procedure_Physician_SAP_ID__c,
                (
                    select Id
                    from Orders
                    where Stage__c = 'Pending SAP ID'
                )
            from Opportunity
            where Trialing_Physician__c in :this.updatedSAPIDs
            or Procedure_Physician__c in :this.updatedSAPIDs
        ]) {

            Boolean hasUpdate = false;

            if (this.updatedSAPIDs.contains(oppty.Trialing_Physician__c)
             && oppty.Trialing_Physician_SAP_ID__c != oppty.Trialing_Physician__r.SAP_ID__c
            ) {
                oppty.Trialing_Physician_SAP_ID__c = oppty.Trialing_Physician__r.SAP_ID__c;
                hasUpdate = true;
            }

            if (this.updatedSAPIDs.contains(oppty.Procedure_Physician__c)
             && oppty.Procedure_Physician_SAP_ID__c != oppty.Procedure_Physician__r.SAP_ID__c
            ) {
                oppty.Procedure_Physician_SAP_ID__c = oppty.Procedure_Physician__r.SAP_ID__c;
                hasUpdate = true;
            }

            if (hasUpdate) {
                opptys.add(oppty);

            }

            if (!oppty.Orders.isEmpty()) {
                orderIds.addAll(new Map<Id,Order>(oppty.Orders).keySet());
            }

        }

        if (!opptys.isEmpty()) {
            //update opptys;
            DML.save(this, opptys, false);
        }
        
        // Uncommented for SFDC 4305
        if (!orderIds.isEmpty()) {
            OrderManager.confirmPatientOrders(orderIds); // @future
        }

    }

    @future
    public static void sendEmailFuture(String jsonData) {


        List<contactEmailObject> contactEmailList = (List<contactEmailObject>) JSON.deserialize(jsonData, List<contactEmailObject>.class);

        for (contactEmailObject contactEmail: contactEmailList) {

            SendEmailManager emailManager = new SendEmailManager(contactEmail.contactEmailSubject, contactEmail.contactEmailBody, contactEmail.contactEmailAdd);
            String errorMessage = '';
            try {
                Messaging.SendEmailResult emailResult = emailManager.sendEmail();
                if (!emailResult.isSuccess()) {
                    errorMessage = (emailResult.getErrors()[0]).getMessage();
                }
            } catch (System.EmailException ee) {
                errorMessage = ee.getMessage();
            }

        }
    }

    public void sendEmailList() {
        if (!contactEmailList.isEmpty()) {
            sendEmailFuture(JSON.serialize(contactEmailList));
        }
    }

    //recalculate sharing table 
    public void contactSharing(List<Contact> contacts) {

        // just build the sets of expected users for each contact
        Map<Id,Set<Id>> userIdMap = new Map<Id,Set<Id>>();
        Map<Id,List<Contact>> contactsByTtyId = new Map<Id,List<Contact>>();

        // map each contact by their territory
        for (Contact contact : contacts) {
            if (!RECTYPES_NMD.contains(contact.RecordTypeId)) continue;
            if (contact.Territory_ID__c == null) continue;

            userIdMap.put(contact.Id, new Set<Id> {});

            if (contactsByTtyId.containsKey(contact.Territory_ID__c)) {
                contactsByTtyId.get(contact.Territory_ID__c).add(contact);
            }
            else {
                contactsByTtyId.put(contact.Territory_ID__c, new List<Contact> { contact });
            }
            
        }

        if (userIdMap.isEmpty()) return;

        // queue a share for each active user (assignee) in the territory
        for (Assignee__c assignee : [
            select
                Assignee__c,
                Territory__c
            from Assignee__c
            where Territory__c in :contactsByTtyId.keySet()
            and Assignee__r.IsActive = true
        ]) {

            for (Contact contact : contactsByTtyId.get(assignee.Territory__c)) {
                // add assignee to contact mapping
                userIdMap.get(contact.Id).add(assignee.Assignee__c);
            }

        }

        // apply delta shares.  this will add/remove shares as needed   
        new SObjectSharingManager(ContactShare.getSObjectType(), userIdMap.keySet()).applyDeltaShares(userIdMap);

    }
    
     /* SFDC-1317 : Update Contact division based on owner division if division is blank.
        Created By : Shashank Gupta (Accenture Team)
        Created date :  29/9/2016
    */
    // Code Starts 
    
    public void fetchEUContact(List<Contact> contacts){
         if(contacts != null){
            for(Contact con : contacts){
                OwnerIdList.add(con.OwnerId);
            }
            //UserDivisionList = [SELECT Profile.Name, Division, Name, Id, Division_Multiselect__c FROM User WHERE Id =:OwnerIdList AND Profile.Name LIKE 'EU%' AND Division_Multiselect__c=:divisionNames];
            UserDivisionList = [SELECT Profile.Name, ProfileId, Division, Name, Id, Division_Multiselect__c FROM User WHERE Id =:OwnerIdList];
         }
    }
    
     Public void contactDivisionDetailInsert(Contact newCont){ 
     system.debug('***UserDivisionList***'+UserDivisionList);
        if(newCont.Division__c == null){
            if(!UserDivisionList.isEmpty()){
                for(User U: UserDivisionList){
                    if(newCont.OwnerId == U.Id && ProfileNamesforEurope.contains(U.Profile.Name) && divisionNames.contains(U.Division_Multiselect__c)){
                        newCont.Division__c = U.Division_Multiselect__c;
                    }
                    else{
                        if(!Test.isRunningTest()){
                            //for(User U1: UserDivisionList){
                                if(ProfileNamesforEurope.contains(U.Profile.Name)){
                                    newCont.Division__c.addError('You must select a Division');
                                }
                           // }
                        }
                    }
                }
            }
        }
     }
     
     Public void contactDivisionDetailUpdate(Contact oldCont , Contact newCont){ 
     system.debug('***UserDivisionList***'+UserDivisionList);
        if((oldCont.Division__c != newCont.Division__c) && newCont.Division__c == null){
            if(!UserDivisionList.isEmpty()){
                for(User U: UserDivisionList){
                    if(newCont.OwnerId == U.Id && ProfileNamesforEurope.contains(U.Profile.Name) && divisionNames.contains(U.Division_Multiselect__c)){
                        newCont.Division__c = U.Division_Multiselect__c;
                    }
                    else{
                        if(!Test.isRunningTest()){
                            //for(User U1: UserDivisionList){
                                if(ProfileNamesforEurope.contains(U.Profile.Name)){
                                    newCont.Division__c.addError('You must select a Division');
                                }
                            //}
                        }
                    }
                }
            }
        }
     }
     // Code Starts
     
     //Q4 2016 - SFDC 4780
     Public void assignDefaultRecordTypeFrRIVA(List<Contact> newContacts){
         map<Id,User> contactOwnerMap = new map<Id,User>();
         Map<String, RIVAProfileContactRecordTypeSync__c> customSettingEntries = RIVAProfileContactRecordTypeSync__c.getAll();

         system.debug('UserDivisionList--'+UserDivisionList);
         if(UserDivisionList!= null && UserDivisionList.size() > 0){
             for(User u :UserDivisionList){
                 contactOwnerMap.put(u.Id,u);
             }
         }
         if(newContacts != null && newContacts.size() > 0){   
             if(userinfo.getUserName().contains('api')){         
                 for(Contact c : newContacts){
                     if(c.OwnerId != null && contactOwnerMap.containsKey(c.OwnerId)){
                         string profileId = contactOwnerMap.get(c.OwnerId).ProfileId;
                         if(customSettingEntries != null && customSettingEntries.containsKey(profileId)){
                             if(c.RecordTypeId != null && c.RecordTypeId != customSettingEntries.get(profileId).DefaultRecordTypeId__c){
                                 c.RecordTypeId = customSettingEntries.get(profileId).DefaultRecordTypeId__c;
                             }
                         }
                     }
                 }
             }             
         }
     }
}
/*
 * Name : TableauConsignmentReportControllerTest
 * Author : Juianne Diamond
  * Description : Test class used for testing the TableauConsignmentReportController
 * Date : 6Jul2016
 */
@isTest
private class TableauConsignmentReportControllerTest{ 

    static testMethod void testMethod1(){      
      
      // Setup test data
      // This code runs as the system user
      Profile p = [SELECT Id FROM Profile WHERE Name='US IC User']; 
      User u = new User(Alias = 'testUser', Email='testUser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='testUser@unitTest.com');
      
      test.startTest();
      System.runAs(u){
          Account acct1 = new Account(Name = 'ACCT1NAME',Account_Number__c='897654',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  
          insert acct1; 
          system.assertEquals(acct1.name,'ACCT1NAME');
          TableauConsignmentReport__c rp1 = new TableauConsignmentReport__c();
          rp1.Name = 'IC';
          rp1.Profile_Names__c = 'US IC User';
          rp1.Tableau_Link__c = 'https://tableau.bsci.com/#/site/SalesReporting/views/SFDCPilotUniversityofWashingtonAccountLevelReport/Rolling12bysize?:iid=1';
          insert rp1;
          
          ApexPages.currentPage().getParameters().put('id', acct1.Id);
          ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
          TableauConsignmentReportController controller =  new  TableauConsignmentReportController(ctr);        
          Test.setCurrentPage(Page.TableauConsignmentReport);        
          controller.getTableauLinkInfo();
      }
      test.stopTest();
    }    

}
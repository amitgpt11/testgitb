/**
* Batch class, Change Object Owner Based on Territory Id 
*
* @author   Salesforce services
* @date     2014-04-03
*/
global class NMD_PatientOwnerByTerritorySchedule implements Schedulable{
	
	/**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        NMD_ObjectOwnerByTerritoryBatch.runNowPatients();
    }

}
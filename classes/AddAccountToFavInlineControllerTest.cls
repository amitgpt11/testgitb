/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Tests Controller for AddAccountToFavInline VF Page
@Requirement Id  User Story : US2674
*/

@isTest

public class AddAccountToFavInlineControllerTest {
    
    static testmethod void addAccountToFavorites(){
        
        Account acct = new Account(Name = 'Test Account');
        insert acct;
        
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
        
        AddAccountToFavInlineController controller = new AddAccountToFavInlineController(sc);
        
        controller.showAccountFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }  
    
    static testmethod void addAccountToFavorites2(){
        
        Account acct = new Account(Name = 'Test Account');
        insert acct;
        
        Account_Favorite__c accountFavorite = new Account_Favorite__c(Account__c = acct.Id, User__c = UserInfo.getUserId());
        insert accountFavorite;
        
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
        
        AddAccountToFavInlineController controller = new AddAccountToFavInlineController(sc);
        
        controller.showAccountFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }
}
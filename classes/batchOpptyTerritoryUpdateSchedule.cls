/**
* Batch class, Change Territory on Opportunity based on Territory change on Account and User
* @author   Mayuri - Accenture
* @date     26APR2016
*/
global class batchOpptyTerritoryUpdateSchedule implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        integer batchSize = Integer.valueOf(system.Label.ETM_OpptyTerritory_Update_Batch_Size);
        batchOpptyTerritoryUpdate b = new batchOpptyTerritoryUpdate();
        database.executebatch(b,batchSize);

    }

}
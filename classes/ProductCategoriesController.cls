public class ProductCategoriesController extends AuthorizationUtil{
    
    public List<ProductCategory> lstProductCategories {get; set;}
    
    public class ProductCategory {
        
        public String productCategoryName {get;set;}
        public Integer productCount {get;set;}
        public String strChildImageURL {get;set;}
        public String strAlignType {get;set;}
        
        public ProductCategory(){}
        
        public ProductCategory(String productCategoryName, Integer productCount) {
            
            this.productCategoryName = productCategoryName;
            this.productCount = productCount;
        }
    }
    
    /**
* Description: method to query product categories
*/
    public override void fetchRequestedData() {
        
        lstProductCategories = new List<ProductCategory>();
        Set<String> setProductFamilies = new Set<String>();
        Map<String,Set<String>> mapDocumentUniqueNameToCategories = new Map<String,Set<String>>();
        Map<String,String> mapProductFamilyToChildImage = new Map<String,String>();
        Map<String,String> mapProductFamilyToAlignType = new Map<String,String>();
        Map<String,ProductCategory> mapFamilyProdCategory = new Map<String,ProductCategory>();
        Set<String> setUserSharedSalesOrganization = new Set<String>();
        Set<String> setParentProducts = new Set<String>();        
        
        User objUser = [Select Shared_Community_Division__c, LanguageLocaleKey, Contact.AccountId
                        From User 
                        Where Id =: UserInfo.getUserId()];
        
        String aggregateResultQuery = 'SELECT count(Id) Total, toLabel(Family) ProductCategory'+
                                      ' FROM Product2'+
                                      ' Where IsActive = true'+
                                      ' AND Division__c = \''+ objUser.Shared_Community_Division__c +'\''+
                                      ' AND Shared_Parent_Product__c = null'+
                                      ' AND Family <> null';
        
        if(BSC_Util.isCommunityUser()){
            
            setUserSharedSalesOrganization = BSC_Util.fetchSharedOrgForCommunityUser(objUser.Contact.AccountId);  
            system.debug('setUserSharedSalesOrganization========='+setUserSharedSalesOrganization);
            
            for (Product2 availableProducts : [SELECT Id, toLabel(Family), Shared_Parent_Product__c
                                               FROM Product2
                                               WHERE Family <> null
                                               AND Division__c =: objUser.Shared_Community_Division__c
                                               AND IsActive = TRUE
                                               AND Id IN :BSC_Util.fetchProductExtensionsForCommunityUser(setUserSharedSalesOrganization)]){
                                                              
                setParentProducts.add(availableProducts.Shared_Parent_Product__c);
            }
            
            aggregateResultQuery += ' AND Id IN :setParentProducts';
        }
        aggregateResultQuery += ' group by Family order by Family';
        
        system.debug('aggregateResultQuery======='+aggregateResultQuery);
        
        for(AggregateResult productCategory : Database.query(aggregateResultQuery)) {
                                                       
            lstProductCategories.add(new ProductCategory(String.valueOf(productCategory.get('ProductCategory')), Integer.valueOf(productCategory.get('Total'))));
            setProductFamilies.add(String.valueOf(productCategory.get('ProductCategory')));
        }
        
        //get custom settings
        for(Product_Category_Image_Mapping__c objImageMapping : [SELECT Name, Document_Unique_Name__c 
                                                                 FROM Product_Category_Image_Mapping__c
                                                                 WHERE Name =: setProductFamilies]){
                                                                     
            if(!mapDocumentUniqueNameToCategories.containsKey(objImageMapping.Document_Unique_Name__c)){
                                                                         
                mapDocumentUniqueNameToCategories.put(objImageMapping.Document_Unique_Name__c, new Set<String>{objImageMapping.Name});    
            }else{
                                                                         
                mapDocumentUniqueNameToCategories.get(objImageMapping.Document_Unique_Name__c).add(objImageMapping.Name);
            }
        }
        
        
        //get the images from documents                                       
        for(Document objDocument : [SELECT id, Name, DeveloperName, Keywords
                                    FROM Document 
                                    WHERE Folder.DeveloperName = 'Product_Category_Images'
                                    AND DeveloperName =: mapDocumentUniqueNameToCategories.keySet()]){
                                        
                                        for(String strCategory : mapDocumentUniqueNameToCategories.get(objDocument.DeveloperName)){
                                            
                                            mapProductFamilyToChildImage.put(strCategory,'/servlet/servlet.FileDownload?file='+objDocument.Id);
                                            mapProductFamilyToAlignType.put(strCategory,objDocument.Keywords);
                                        }
                                    }
        
        for(ProductCategory objProduct : lstProductCategories){
            
            ProductCategory objProductCategories = new ProductCategory();
            objProductCategories = objProduct;
            
            if(mapProductFamilyToChildImage.containsKey(objProduct.productCategoryName)){
                
                objProductCategories.strChildImageURL = mapProductFamilyToChildImage.get(objProduct.productCategoryName);
                objProductCategories.strAlignType = mapProductFamilyToAlignType.get(objProduct.productCategoryName);
            }
            
            mapFamilyProdCategory.put(objProduct.productCategoryName, objProductCategories);
        }                   
        
        system.debug('## lstProductCategories : '+lstProductCategories);
    }
}
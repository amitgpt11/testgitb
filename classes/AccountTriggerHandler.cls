/**
* Handler class for the Account trigger
*
* @Author salesforce Services
* @Date 2015/06/11
*/
public with sharing class AccountTriggerHandler extends TriggerHandler {

    final AccountManager manager = new AccountManager();
   
    /**
    * method to aggregate inventory data for account update
    * 
    * @param void
    */
    public override void bulkBefore() {
        List<Account> accounts = (List<Account>)trigger.new;
        manager.fetchPersonnelIdRecords(accounts);
    }

    /**
    * method to aggregate inventory data for account update
    * 
    * @param void
    */
    public override void bulkAfter() {
        List<Account> accounts = (List<Account>)trigger.new;
        Set<Id> accountIds = trigger.newMap.keySet();
        manager.fetchInventoryDataForAccounts(accountIds);

        if(trigger.isUpdate){
            manager.initSubmissionManager();
        }

    }

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void beforeUpdate(SObject oldObj, SObject obj) {
        Account account = (Account)obj;
        Account oldAccount = (Account)oldObj;
        manager.updateConsignmentAccountOwnerWithPersonnelID(account);
        manager.formatNaming(oldAccount, account);

    }

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void afterUpdate(SObject oldObj, SObject obj) {
        Account account = (Account)obj;
        Account oldAccount = (Account)oldObj;
        
        manager.updateInventoryDataForAccounts(oldAccount, account);
        manager.createInventoryDataForAccounts(oldAccount, account);
        manager.checkSAPID(oldAccount, account);
        manager.checkForAutoSubmit(oldObj, obj);

    }

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void beforeInsert(SObject obj) {
        Account account = (Account)obj;
        manager.updateConsignmentAccountOwnerWithPersonnelID(account);
        manager.formatNaming(account);

    }      

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void afterInsert(SObject obj) {
        Account account = (Account)obj;
        manager.createInventoryDataForAccounts(account);

    }      

    /**
    * Called at end of trigger
    * 
    * @param void
    */  
    public override void andFinally() {
        //manager.sendEmailListFuture();
        manager.commitInventoryDataObjects();
        manager.cascadeSAPIDInfo();

        if(trigger.isAfter && trigger.isUpdate){
            manager.autoSubmitRecords();
        }

    }   
}
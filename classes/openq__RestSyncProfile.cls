/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/syncprofile/*')
global class RestSyncProfile {
    global RestSyncProfile() {

    }
    @HttpPost
    global static openq.OpenMSL.PROFILE_SYNC_RESPONSE_element SyncProfile(openq.OpenMSL.PROFILE_SYNC_REQUEST_element request_x) {
        return null;
    }
}

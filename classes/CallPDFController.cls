public class CallPDFController {
     public pagereference callPdfPage(){      
        id reqId = apexpages.currentpage().getparameters().get('id');
        System.debug('===in call====='+apexpages.currentpage().getparameters().get('id'));
        //list<Shared_Request_Form__c> lstObj = [Select id,name,Requesters_Name__r.Name,Onbehalf_Of__r.Name,Ship_to_Name__c,Cost_Center__c,SAP_Account_Number__c,Shipping_Address__c,City__c,State__c,Zip_Code__c from Shared_Request_Form__c where id=:reqid ];
        list<Shared_Request_Form__c> lstObj = [Select Id,Name,Requesters_Name__c,Requesters_Name__r.Name from Shared_Request_Form__c   where id=:reqid  limit 1];
          
        if(lstObj.size()>0){  
           Shared_Request_Form__c reqForm =lstObj.get(0);
            PageReference pdf = Page.DemoRequestFormPDF;
            // add parent id to the parameters for standardcontroller
            system.debug('<<<>>>' + pdf);
            pdf.getParameters().put('id',reqId);
            System.debug('PDF url : '+pdf);
            // the contents of the attachment from the pdf
            Blob body;
            pdf.setRedirect(true);
            try {
        
              // returns the output of the page as a PDF
           body = pdf.getContentAsPDF();
              System.debug('===body==='+body);      
        
            // need to pass unit test -- current bug  
            } catch (Exception e) {
              body = Blob.valueOf('Some Text');
              system.debug('<<>>>'+ e);
            }
            
             Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.setContentType('application/pdf');
            attach.setFileName(reqForm.Name+'.pdf');
            attach.setInline(false);
            attach.Body = body;
        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { 'Quincy-DemoRequests@bsci.com'  });// Story#1941  { 'Susannah.st-germain@bsci.com' });
            mail.setCcAddresses(new String[] { UserInfo.getUserEmail() });
            mail.setSubject('Demo Request Form');
            mail.setHtmlBody('Hello Quincy, <br/><br/>Please find attachment for demo request form - '+reqForm.Name+' <br/><br/> Thanks <br/>'+reqForm.Requesters_Name__r.Name);

            mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
    
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }  
        return new PageReference('/'+reqId);
        
    }

    
}
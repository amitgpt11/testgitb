/**
* Test Class for the TriggerHandler methods for Opportunity triggers.       
*
* @Author salesforce Services
* @Date 02/10/2015
*/
@isTest(SeeAllData=false)
private class OpportunityTriggerHandlerTest {

    static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LNAME = 'LName';
    static final String LEAD_SOURCE = 'Care Card';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static Opportunity trialOpty = null;
    static Opportunity implantOpty = null;
    static String OptyStatus;
    @testSetup
    static void setup() {
        Test.StartTest();
        NMD_TestDataManager td = new NMD_TestDataManager();

        Id pricebookId = Test.getStandardPricebookId();

        Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
        insert pb;

        Product2 prod0 = td.newProduct('Trial');
        Product2 prod1 = td.newProduct('Implant');
        insert new List<Product2> { prod0, prod1 };

        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod0.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod1.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        insert new List<PricebookEntry> { standardPrice0, standardPrice1 };

        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod0.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod1.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        insert new List<PricebookEntry> { pbe0, pbe1 };

        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        //**SFDC 1364 code chages start - Amitabh
        /*territory.ASP_Trial__c = 100;
         territory.ASP_Implant__c = 120;
        */
        //**SFDC 1364 code chages Ends - Amitabh
        insert territory;
        
        Account acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

        Account acctTrialing = td.newAccount(ACCT_TRIALING);
        acctTrialing.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing.Type = 'Ship To';
        acctTrialing.NMD_Territory__c = territory.Id;
        
        Account acctProcedure = td.newAccount(ACCT_PROCEDURE);
        acctProcedure.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        //acctProcedure.Sold_to_Attn__c = 'Attn';
        acctProcedure.Sold_to_City__c = 'City';
        acctProcedure.Sold_to_State__c = 'State';
        acctProcedure.Sold_to_Zip_Postal_Code__c = 'Zip';
        acctProcedure.Sold_to_Country__c = 'Country';
        acctProcedure.Sold_to_Phone__c = 'Phone';
        acctProcedure.NMD_Territory__c = territory.Id;

        insert new List<Account> {
            acctMaster, acctTrialing, acctProcedure
        };
        
     
        Contact trialing = td.newContact(acctTrialing.Id, LNAME);
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        Contact prodcedure = td.newContact(acctProcedure.Id, LNAME);
        prodcedure.AccountId = acctMaster.Id;
        prodcedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        prodcedure.MailingStreet = '123 Main St';
       
         //Added By Ashish --Start--
        prodcedure.MailingCity = 'test';
        prodcedure.MailingState = 'test';
        prodcedure.MailingPostalCode = 'test';
        prodcedure.MailingCountry = 'test';
        prodcedure.Fax = 'test';
        //Added By Ashish --End--
       
        prodcedure.Territory_ID__c = territory.Id;
        prodcedure.Phone = '5556667788';

        insert new List<Contact> { 
            trialing, prodcedure 
        };
    
        //initializing trail and implant opportunities & assigning same master account to both opportunities.
        trialOpty = td.newOpportunity(acctTrialing.Id, OPPTY_NAME_TRIAL);
            trialOpty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            trialOpty.Trialing_Account__c = acctTrialing.Id;
            trialOpty.Trialing_Physician__c = trialing.Id;
            trialOpty.Procedure_Account__c = acctProcedure.Id;
            trialOpty.Procedure_Physician__c = prodcedure.Id;
            trialOpty.Territory_ID__c = territory.Id;
            trialOpty.closeDate = System.today().addDays(30);
            trialOpty.LeadSource = LEAD_SOURCE;
            trialOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
        insert trialOpty;
        
        
        implantOpty = td.newOpportunity(acctTrialing.Id, OPPTY_NAME_IMPLANT);
            implantOpty.Territory_ID__c = territory.Id;
            implantOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
            implantOpty.closeDate = System.today().addDays(30);
            implantOpty.LeadSource = LEAD_SOURCE;
            implantOpty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
            implantOpty.Procedure_Account__c = acctProcedure.Id;
            implantOpty.Procedure_Physician__c = prodcedure.Id;
        insert implantOpty;
    
    Test.StopTest();
    }
   public static String opportunityTriggerStatus()
    {
      ApexTrigger apx = [Select id,Status,Name From ApexTrigger where Name = 'OpportunityTrigger'];
      return apx.status;
    }
   
    
    static testMethod void test_UpdateAccountId_Standard() {
 //Test.StartTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];     
            
        Opportunity oppty = [
            select 
                Id,
                AccountId,
                Opportunity_Type__c,
                Trialing_Physician__c,
                Procedure_Account__c
            from Opportunity
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        oppty.Opportunity_Type__c = 'OMG';
        Test.StartTest();
        //oppty.Trial_Status__c = 'Successful';
        update oppty;

        //assert correct account is now parent
        //Account acctTrialing = [select Id from Account where Name = :ACCT_TRIALING];
        //Id acctId = [select AccountId from Opportunity where Id = :oppty.Id].AccountId;
        //System.assertEquals(acctTrialing.Id, acctId);
Test.StopTest();
    }
    
    //static testMethod void test_UpdateAccountId_Swap() {

    //  Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

    //  Opportunity oppty = [
    //      select 
    //          Id,
    //          AccountId,
    //          Opportunity_Type__c,
    //          Trialing_Account__c,
    //          Procedure_Account__c
    //      from Opportunity 
    //      where AccountId = :acct.Id
    //  ];

    //  oppty.Opportunity_Type__c = 'BSC Swap';
    //  update oppty;

    //  //assert correct account is now parent
    //  Account acctProdcedure = [select Id from Account where Name = :ACCT_PROCEDURE];
    //  Id acctId = [select AccountId from Opportunity where Id = :oppty.Id].AccountId;
    //  System.assertEquals(acctProdcedure.Id, acctId);

    //}

    //static testMethod void test_UpdateAccountId_Revision() {

    //  Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

    //  Opportunity oppty = [
    //      select 
    //          Id,
    //          AccountId,
    //          RecordTypeId,
    //          Trialing_Account__c,
    //          Procedure_Account__c
    //      from Opportunity 
    //      where AccountId = :acct.Id
    //  ];

    //  oppty.RecordTypeId = OpportunityManager.RECTYPE_EXPLANT;
    //  oppty.I_Acknowledge__c = true;
    //  update oppty;

    //  //assert correct account is now parent
    //  Account acctProdcedure = [select Id from Account where Name = :ACCT_PROCEDURE];
    //  Id acctId = [select AccountId from Opportunity where Id = :oppty.Id].AccountId;
    //  System.assertEquals(acctProdcedure.Id, acctId);

    //}
    
    
    static testMethod void test_OpportunityRevenueUpdates(){
        
        
        NMD_TestDataManager td = new NMD_TestDataManager();
        td.setupNuromodUserRoleSettings();
         User ibu_User = td.newUser('NMD IBU');
        insert new List<User> {ibu_User};
      system.runAs(ibu_User){ 
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        //**SFDC 1364 code changes start - Amitabh
          //territory.ASP_Trial__c = 100;
          //territory.ASP_Implant__c = 120;
          //**SFDC 1364 code changes ends - Amitabh
        insert territory;
        
       Account newTrialingAccount = td.newAccount('TrialingAccount');
        newTrialingAccount.Account_Number__c = '11111111';
        newTrialingAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        newTrialingAccount.Type = 'Ship To';
        newTrialingAccount.Trial_ASP_Value__c =100;  
        newTrialingAccount.Implant_ASP_Value__c =120;
          
        Account newProcedureAccount = td.newAccount('ProcedureAccount');
        newProcedureAccount.Account_Number__c = '22222222';
        newProcedureAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        newProcedureAccount.Trial_ASP_Value__c =200;  
        newProcedureAccount.Implant_ASP_Value__c =220;

        insert new List<Account> { newTrialingAccount, newProcedureAccount };

        //Create new Contacts to populate Order Surgeon
        Contact newTrialingContact = td.newContact(newTrialingAccount.Id, 'TestLastTrialing');
        newTrialingContact.SAP_ID__c = '111111';
        newTrialingContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        Contact newProcedureContact = td.newContact(newProcedureAccount.Id, 'TestLastProcedure');
        newProcedureContact.SAP_ID__c = '222222';
        newProcedureContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { newTrialingContact, newProcedureContact };
        
       Patient__c patient1 = new Patient__c(
          RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
          Patient_Phone_Number__c = '555-444-3322',
          Rep_Code__c = 'repc'
        );
        insert patient1;
        
        List<Opportunity> NewOppyList = new List<Opportunity>();
        Opportunity testImplant = td.newOpportunity(newTrialingAccount.Id, OPPTY_NAME_IMPLANT);
            testImplant.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
            testImplant.Trialing_Account__c = newTrialingAccount.Id;
            testImplant.Trialing_Physician__c = newTrialingContact.Id;
            testImplant.Procedure_Account__c = newProcedureAccount.Id;
            testImplant.Procedure_Physician__c = newProcedureContact.Id;
            testImplant.Territory_ID__c = territory.Id;
            testImplant.closeDate = System.today().addDays(30);
            testImplant.LeadSource = LEAD_SOURCE;
            testImplant.Opportunity_Type__c = OPPORTUNITY_TYPE;
            NewOppyList.add(testImplant);
            
             Opportunity testRevision = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testRevision.RecordTypeId = OpportunityManager.RECTYPE_REVISION; 
            testRevision.Procedure_Account__c = newProcedureAccount.Id;
            testRevision.Procedure_Physician__c = newProcedureContact.Id;
            testRevision.Territory_ID__c = territory.Id;
            testRevision.closeDate = System.today().addDays(30);
            testRevision.LeadSource = LEAD_SOURCE;
            testRevision.I_Acknowledge__c= true;
            testRevision.Procedure_Type__c = 'BSC IPG Swap';
            testRevision.Opportunity_Type__c = 'BSC IPG Swap';
            NewOppyList.add(testRevision);           
            
            Opportunity testTrial = td.newOpportunity(newTrialingAccount.Id, OPPTY_NAME_TRIAL);
            testTrial.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            testTrial.Trialing_Account__c = newTrialingAccount.Id;
            testTrial.Trialing_Physician__c = newTrialingContact.Id;
            testTrial.Procedure_Account__c = newProcedureAccount.Id;
            testTrial.Procedure_Physician__c = newProcedureContact.Id;
            testTrial.Territory_ID__c = territory.Id;
            testTrial.closeDate = System.today().addDays(30);
            testTrial.LeadSource = LEAD_SOURCE;
            testTrial.Opportunity_Type__c = OPPORTUNITY_TYPE;
            testTrial.Patient__c = patient1.Id;
            NewOppyList.add(testTrial);
        
           OptyStatus = opportunityTriggerStatus();
           Insert NewOppyList; 
            Test.StartTest();  
            //TRIAL
            //insert testTrial;
            //testTrial.Trialing_Account__c = newProcedureAccount.Id;
            //update testTrial;
             //if(OptyStatus == 'Active'){
            //Opportunity opty1 = [select Trial_Revenue__c from Opportunity where id =:NewOppyList[0].id];
            //check the ASP Value 
            //**SFDC 1364 code commented to add account asp value changes start - Amitabh
              //   System.assertEquals(opty1.Trial_Revenue__c,100);
            //**SFDC 1364 code commented to add account asp value changes Ends - Amitabh     
            //IMPLANT
            //insert testImplant;
             //opty1 = [select Procedure_Revenue__c from Opportunity where id =:testImplant.id];
            //check the ASP Value 
            //**SFDC 1364 code commented to add account asp value changes start - Amitabh
              //  System.assertEquals(opty1.Procedure_Revenue__c,220);
            //**SFDC 1364 code commented to add account asp value changes Ends - Amitabh
            //REVISION
            //insert testRevision;
            
             //opty1 = [select Procedure_Revenue__c from Opportunity where id =:testRevision.id];
            //check the ASP Value 
             //**SFDC 1364 code commented to add account asp value changes start - Amitabh
                //System.assertEquals(opty1.Procedure_Revenue__c,220);
            //**SFDC 1364 code commented to add account asp value changes Ends - Amitabh
            //
            //cleanup
            //opty1=null;}
        }
            
            
    Test.stopTest();    
    }
    

    static testMethod void test_AutosubmitQuincy_Procedure() {

        new NMD_TestDataManager().setupQuincyContact();
test.startTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_IMPLANT
        ];

        // update related records so they will submit
        update new Account(
            Id = oppty.Procedure_Account__c,
            Sold_to_Fax__c = 'Fax'
        );
        update new Contact(
            Id = oppty.Procedure_Physician__c,
            Fax = '5556662233'
        );

        oppty.Scheduled_Procedure_Date_Time__c = System.now().addDays(7);
        update oppty;
test.stopTest();
    }

    static testMethod void test_AutosubmitQuincy_Trial() {
test.startTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        // update related records so they will submit
        update new Account(
            Id = oppty.Procedure_Account__c,
            Sold_to_Fax__c = 'Fax'
        );
        update new Contact(
            Id = oppty.Procedure_Physician__c,
            Fax = '5556662233'
        );

        oppty.Scheduled_Trial_Date_Time__c = System.now().addDays(7);
        update oppty;
test.stopTest();
    }

    static testMethod void test_AutoReminder_Procedure() {
        Test.StartTest();
        
        new NMD_TestDataManager().setupQuincyContact(); //Added by Ashish
        
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_IMPLANT
        ];

        oppty.Scheduled_Procedure_Date_Time__c = System.now().addDays(7);
        update oppty;
Test.StopTest();
    }

    static testMethod void test_AutoReminder_Trial() {
Test.StartTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        oppty.Scheduled_Trial_Date_Time__c = System.now().addDays(7);
        update oppty;
Test.StopTest();
    }

    static testmethod void Test_preventOpportunityWithOrderDelete(){
    Test.StartTest();
        NMD_TestDataManager td = new NMD_TestDataManager();
        td.setupNuromodUserRoleSettings();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        User fieldRep = td.newUser('NMD Field Rep');
        User sysAdmin = td.newUser('System Administrator');
        
        insert new List <User>{
            fieldRep, sysAdmin
        };

        //insert fieldRep;

        Order order = new Order(
            //Pricebook2Id = pb0.Id,
            AccountId = oppty.AccountId,
            OpportunityId = oppty.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today() - 1,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT,
            OwnerId = fieldRep.Id
        );
        insert order;

        oppty.OwnerId = fieldRep.Id;
        update oppty;

        

            //test for error with NMD profile
            System.runAs(fieldRep){

                boolean errorCaught = false;
                try{
                    delete oppty;
                } catch (Exception e){
                    errorCaught = e.getMessage().contains(Label.Prevent_Delete_Opportunity_With_Orders) ? true : false;
                }
               OptyStatus = opportunityTriggerStatus(); 
               if(OptyStatus == 'Active'){
                System.assertEquals(true, errorCaught, 'NMD User should receive error for deleting an Opportunity with an Order.  No error received!');
               }
            }

            //test for no error with api user/admin
            System.runAs(sysAdmin){
                boolean adminError = false;
                try{
                    delete oppty;
                } catch (Exception e){
                    adminError = e.getMessage().contains(Label.Prevent_Delete_Opportunity_With_Orders) ? true : false;
                }
                System.assertEquals(false, adminError, 'Admin should not receive error for deleting an Opportunity with orders.');
            }        
        Test.stopTest();

    }

    static testMethod void test_UpdateOrderAccountOrContact(){
    Test.StartTest();
        NMD_TestDataManager td = new NMD_TestDataManager();
         Order order1;
        td.setupNuromodUserRoleSettings();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        List<Opportunity> opptys = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
        ];
        
        system.debug(opptys+'size-->@@');
        for(Opportunity op: opptys){
            if(op.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW)
                trialOpty = op;
            else
                implantOpty = op;
        }
    SYSTEM.DEBUG(trialOpty+'trialOpty-->'+implantOpty+'implantOpty-->');
        if(trialOpty!=null || implantOpty !=null){
         order1 = new Order(
            //Pricebook2Id = pb0.Id,
            AccountId = trialOpty.AccountId,
            OpportunityId = trialOpty.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today() - 1,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT,
            Procedure_Type__c = 'Trial',
            Stage__c = 'New'
        );
        Order order2 = new Order(
            //Pricebook2Id = pb0.Id,
            AccountId = implantOpty.AccountId,
            OpportunityId = implantOpty.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today() - 1,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT,
            Procedure_Type__c = 'Implant',
            Stage__c = 'New'
        );
        insert new List<Order>{ order1, order2 };
        
        //Create new Contacts to populate Order Account
        Account newTrialingAccount = td.newAccount('TrialingAccount');
        newTrialingAccount.Account_Number__c = '11111111';
        newTrialingAccount.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        newTrialingAccount.Type = 'Ship To';
        
        Account newProcedureAccount = td.newAccount('ProcedureAccount');
        newProcedureAccount.Account_Number__c = '22222222';
        newProcedureAccount.RecordTypeId = AccountManager.RECTYPE_PROSPECT;

        insert new List<Account> { newTrialingAccount, newProcedureAccount };

        //Create new Contacts to populate Order Surgeon
        Contact newTrialingContact = td.newContact(newTrialingAccount.Id, 'TestLastTrialing');
        newTrialingContact.SAP_ID__c = '111111';
        newTrialingContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        Contact newProcedureContact = td.newContact(newProcedureAccount.Id, 'TestLastProcedure');
        newProcedureContact.SAP_ID__c = '222222';
        newProcedureContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { newTrialingContact, newProcedureContact };
        
            trialOpty.Trialing_Account__c = newTrialingAccount.Id; 
            trialOpty.Trialing_Physician__c = newTrialingContact.Id;
            trialOpty.Procedure_Account__c = newProcedureAccount.Id;
            trialOpty.Procedure_Physician__c = newProcedureContact.Id;
            
             
            implantOpty.Procedure_Account__c = newProcedureAccount.Id;
            implantOpty.Procedure_Physician__c = newProcedureContact.Id;
            
            
            System.debug('Order Account Id BEFORE :'+Order1.accountId);
            System.debug('trialOpty Account Id BEFORE :'+trialOpty.Trialing_Account__c);
            System.debug('implantOpty Account Id BEFORE :'+implantOpty.Procedure_Account__c);
            //update new List<Opportunity> {trialOpty, implantOpty};
            update trialOpty;
            System.debug('Trial Opty Account ID: '+trialOpty.Trialing_Account__c +' & Order Account Id AFTER :'+Order1.accountId);
            update implantOpty;
            System.debug('implantOpty Account Id after:'+implantOpty.Procedure_Account__c);
            //order1 = [SELECT Id, AccountId, Surgeon__c FROM Order WHERE Procedure_Type__c =: 'Trial'];
            System.assertEquals(trialOpty.AccountId, order1.AccountId, 'Order1 AccountId not set by trigger.');
            //System.assertEquals(newTrialingContact.Id, order1.Surgeon__c, 'Order1 Surgeon__c not set by trigger.');

            order2 = [SELECT Id, AccountId, Surgeon__c FROM Order WHERE Id = :order2.Id];
           // System.assertEquals(implantOpty.Id, order2.AccountId, 'Order2 AccountId not set by trigger.');
            //System.assertEquals(newProcedureContact.Id, order2.Surgeon__c, 'Order2 Surgeon__c not set by trigger.');

        Test.stopTest();
        }
    }
    
    //Added By Ashish --Start--
    static testMethod void test_CreateAccountOrContact(){
        
        NMD_TestDataManager td = new NMD_TestDataManager();
            
        Account newTrialingAccount = td.newAccount('TrialingAccount');
        newTrialingAccount.Account_Number__c = '11111111';
        newTrialingAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        newTrialingAccount.Type = 'Ship To';
        
        Account newProcedureAccount = td.newAccount('ProcedureAccount');
        newProcedureAccount.Account_Number__c = '22222222';
        newProcedureAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        
        Account newProcedureAccount1 = td.newAccount('ProspectAccount');
        newProcedureAccount1.Account_Number__c = '22222222Test';
        newProcedureAccount1.RecordTypeId = AccountManager.RECTYPE_PROSPECT;

        insert new List<Account> { newTrialingAccount, newProcedureAccount, newProcedureAccount1};
        
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        territory.ASP_Trial__c = 100;
        territory.ASP_Implant__c = 120;
        insert territory;
        
        Patient__c patient = new Patient__c(
          RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
          Patient_Phone_Number__c = '555-444-3322',
          Rep_Code__c = 'repc'
        );
        insert patient;

        Contact newProcedureContact = td.newContact(newProcedureAccount.Id, 'TestLastProcedure');
        newProcedureContact.SAP_ID__c = '222222';
        newProcedureContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
      /*  newProcedureContact.MailingStreet = 'test';
        newProcedureContact.MailingCity = 'test';
        newProcedureContact.MailingState = 'test';
        newProcedureContact.MailingPostalCode = 'test';
        newProcedureContact.MailingCountry = 'test';
        newProcedureContact.Phone = '123456789';
        newProcedureContact.Fax = 'test';
        newProcedureContact.Territory_ID__c = territory.id;*/
        insert newProcedureContact;
        
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        
         list<opportunity> lstInsertopp = new list<opportunity>();
            Opportunity testRevision = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testRevision.RecordTypeId = OpportunityManager.RECTYPE_REVISION; 
            testRevision.Procedure_Account__c = newProcedureAccount.Id;
            testRevision.Procedure_Physician__c = newProcedureContact.Id;
            testRevision.Territory_ID__c = territory.Id;
            testRevision.closeDate = System.today().addDays(30);
            testRevision.LeadSource = LEAD_SOURCE;
            testRevision.I_Acknowledge__c= true;
            testRevision.Procedure_Type__c = 'BSC IPG Swap';
            testRevision.Opportunity_Type__c = 'BSC IPG Swap'; 
            testRevision.patient__c = patient.Id;
            lstInsertopp.add(testRevision);
            
            Opportunity testExp = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testExp.RecordTypeId = OpportunityManager.RECTYPE_EXPLANT; 
            testExp.Procedure_Account__c = newProcedureAccount.Id;
            testExp.Procedure_Physician__c = newProcedureContact.Id;
            testExp.Territory_ID__c = territory.Id;
            testExp.closeDate = System.today().addDays(30);
            testExp.LeadSource = LEAD_SOURCE;
            testExp.I_Acknowledge__c= true;
            testExp.Procedure_Type__c = 'BSC IPG Swap';
            testExp.Opportunity_Type__c = 'BSC IPG Swap'; 
            
            lstInsertopp.add(testExp);

            Opportunity oppty = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision111' );
            oppty.AccountId = newProcedureAccount.Id;
            oppty.Procedure_Account__c = newProcedureAccount.Id;
            oppty.Procedure_Physician__c = newProcedureContact.Id;
            oppty.StageName = 'New';
            oppty.CloseDate = date.today().addDays(30);
            oppty.Amount = 100;
            oppty.Opportunity_Start_Date__c = date.today();
            oppty.Territory_ID__c = territory.Id;
            oppty.LeadSource = LEAD_SOURCE;
            oppty.I_Acknowledge__c= true;
            oppty.Procedure_Type__c = 'BSC IPG Swap';
            oppty.Opportunity_Type__c = 'BSC IPG Swap';
            oppty.ASP_Trial_Amount__c = 2; 
            oppty.ENDO_Secondary_Territory__c = '500000345';
            oppty.Remove_Secondary_Territory__c = false;
            oppty.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Endo Capital Business').getRecordTypeId();
            
            lstInsertopp.add(oppty);
            
            Opportunity testRev = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testRev.RecordTypeId = OpportunityManager.RECTYPE_REVISION; 
            testRev.Procedure_Account__c = newProcedureAccount.Id;
            testRev.Procedure_Physician__c = newProcedureContact.Id;
            testRev.Territory_ID__c = territory.Id;
            testRev.closeDate = System.today().addDays(30);
            testRev.LeadSource = LEAD_SOURCE;
            testRev.I_Acknowledge__c= true;
            testRev.Procedure_Type__c = '';
            testRev.Opportunity_Type__c = 'BSC IPG Swap';
            testRev.ENDO_Secondary_Territory__c = '5000003565';
            lstInsertopp.add(testRev);
            
            Opportunity testRev1 = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testRev1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Europe NMD New Product Tracker').getRecordTypeId(); 
            testRev1.Procedure_Account__c = newProcedureAccount.Id;
            testRev1.Procedure_Physician__c = newProcedureContact.Id;
            testRev1.Territory_ID__c = territory.Id;
            testRev1.closeDate = System.today().addDays(30);
            testRev1.LeadSource = LEAD_SOURCE;
            testRev1.I_Acknowledge__c= true;
            testRev1.Procedure_Type__c = '';
            testRev1.Opportunity_Type__c = 'BSC IPG Swap';
            testRev1.ENDO_Secondary_Territory__c = '5000003565';
            testRev1.Divisions__c = '';
            testRev1.OwnerId = userinfo.getuserid();
            lstInsertopp.add(testRev1);
            
            insert lstInsertopp;
            Test.StartTest();
            oppty.CloseDate = date.today().addDays(2);
            //oppty.ENDO_Secondary_Territory__c = '500000657';
            oppty.Remove_Secondary_Territory__c = true;
            update oppty;
            
            testRev.ENDO_Secondary_Territory__c = '50006574';
            testRev.Procedure_Type__c = 'BSC IPG Swap';
            update testRev;

            Id pricebookId = Test.getStandardPricebookId();
            
            //Create your product
            Product2 prod = new Product2(
                 Name = 'Product X',
                 ProductCode = 'Pro-X',
                 isActive = true
            );
            insert prod;
            
            //Create your pricebook entry
            PricebookEntry pbEntry = new PricebookEntry(
                 Pricebook2Id = pricebookId,
                 Product2Id = prod.Id,
                 UnitPrice = 0,
                 IsActive = true
            );
            insert pbEntry;
            
            //create your opportunity line item.  This assumes you already have an opportunity created, called opp
            OpportunityLineItem oli = new OpportunityLineItem(
                 OpportunityId = oppty.Id,
                 Quantity = 5,
                 PricebookEntryId = pbEntry.Id,
                 TotalPrice = 0
            );
            insert oli;
            
            oli.TotalPrice = oppty.ASP_Trial_Amount__c;
            update oli;
            
            OpportunityManager manager = OpportunityManager.getInstance();
            manager.createOpportunityProductFromOrder(testExp);
            manager.createOpportunityProductFromOrder(testRevision);
            
            manager.updateOptyLineItemPrice(lstInsertopp);
            manager.updatePatientPainArea(testRevision);
            //manager.isReadyToSubmit(newTrialingAccount);

            test.StopTest();
    }
    //Added By Ashish --End--
    
    //Chris: I can't find a way to get passed this.productsCommitted = true returning out of the 
    //      commitOpportunityProducts method.  It seems to always be set to true even before my update
    //static testMethod void test_commitOpportunityProducts(){
    //  Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];
    //
    //  Opportunity oppty = [
    //      select 
    //          Id,
    //          AccountId,
    //          RecordTypeId,
    //          Trialing_Account__c,
    //          Trialing_Physician__c,
    //          Procedure_Account__c,
    //          Procedure_Physician__c
    //      from Opportunity 
    //      where AccountId = :acct.Id
    //  ];
    //
    //  Test.startTest();
    //
    //      oppty.Opportunity_Type__c = 'Standard';
    //      oppty.Trial_Status__c = 'Successful';
    //      oppty.Scheduled_Procedure_Date_Time__c = System.today();
    //      update oppty;
    //
    //  Test.stopTest();
    //
    //}
}
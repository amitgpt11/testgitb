/**
* Test class for NMD_ObjectOwnerByTerritoryBatch
*
* @author   Salesforce services
* @date     2014-04-13
*/
@isTest(SeeAllData=true) 
private class NMD_ObjectOwnerByTerritoryBatchTest {

    static testMethod void test_NMD_ObjectOwnerByTerritoryBatch() {

        NMD_TestDataManager testData = new NMD_TestDataManager();
        final Integer HISTLEN = 1;
                
        //create user 
        User user = testData.newUser('System Administrator');
        insert user;

        System.Test.startTest();
        //system.runAs(user){
                
        //create account
        Account acc = testData.createConsignmentAccount();
        insert acc;
        
        //create seller hierarchy
        Seller_Hierarchy__c sellerH = testData.newSellerHierarchy('TEST_SH');
        insert sellerH;
        Seller_Hierarchy__c sellerH1 = testData.newSellerHierarchy('TEST_SH1');
        insert sellerH1;

        Lead lead = testData.newLead();
        lead.Territory_ID__c = sellerH.Id;
        insert lead;
        
        //create contacts
        List<Contact> contacts = new List<Contact>{};
        
        for(Integer i = 0; i < HISTLEN; i++) {
            contacts.add(testData.newContact(acc.Id));
        }
        insert contacts;  
        
        //update Seller Hierarchy
        sellerH.Current_TM1_Assignee__c = user.Id;
        sellerH1.Current_TM1_Assignee__c = user.Id;
        update sellerH;
        update sellerH1;      
        
        //update contact
        for(Contact c : contacts){
            c.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            c.Territory_ID__c = sellerH1.Id;
        }
        update contacts;
        
        //create patient
        Patient__c patient = testData.newPatient();
        insert patient;
        
        //update patient
        patient.Physician_of_Record__c = contacts.get(0).Id;
        patient.RecordTypeId = PatientManager.RECTYPE_PROSPECT;
        patient.Territory_ID__c = sellerH.Id;
        update patient;
        
        //create opportunity
        Opportunity oppo = testData.newOpportunity(acc.Id);
        insert oppo;
        
        //update opportunity
        oppo.Patient__c = patient.Id;
        oppo.RecordTypeId = OpportunityManager.RECTYPE_REVISION;
        oppo.I_Acknowledge__c = true;
        update oppo;
        
        //contact ids
        Set<String> contactIds = new Set<String>();
        for(Contact c : contacts){
            contactIds.add(c.Id);
        }
        System.Test.stopTest();     
        
        NMD_ObjectOwnerByTerritoryBatch.runNowPatients();
        NMD_ObjectOwnerByTerritoryBatch.runNowContacts();
        NMD_ObjectOwnerByTerritoryBatch.runNowLeads();
        NMD_ObjectOwnerByTerritoryBatch.runNowOpportunities();
        NMD_ObjectOwnerByTerritoryBatch.runNowOrders();

    }
    
    static testMethod void test_NMD_OrderSharingByTerritorySchedule(){
        new NMD_OrderSharingByTerritorySchedule().execute(null);
    }

}
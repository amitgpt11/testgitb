/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_CycleCountProductsExtensionTest {

    static final String CYCLECOUNTNAME = 'Cycle Count';
    
    @TestSetup
    static void setup() {

        insert new Product2(
            Name = 'Name',
            ProductCode = 'ProductCode',
            Description = 'Description',
            Family = 'Family',
            Product_Hierarchy__c = '1234512789012345678',
            UPN_Material_Number__c = '1234567',
            EAN_UPN__c = '1234'
        );

        insert new Cycle_Count__c(
            Name = CYCLECOUNTNAME,
            Status__c = 'Draft'
        );

    }

    static testMethod void test_CycleCountProducts() {

        Cycle_Count__c cycleCount = [select Status__c from Cycle_Count__c where Name = :CYCLECOUNTNAME];
        
        PageReference pr = Page.CycleCountProducts;
        pr.getParameters().put('id', cycleCount.Id);
        Test.setCurrentPage(pr);

        NMD_CycleCountProductsExtension ext = new NMD_CycleCountProductsExtension(
            new ApexPages.StandardController(cycleCount)
        );

        ext.firstPage();
        ext.nextPage();
        ext.prevPage();
        ext.lastPage();

        ext.selectedItems.add(ext.availableItems[0]);
        ext.addSelectedItems();

        ext.savedItems[0].selected = true;
        ext.removeSelectedItems();

    }

}
/* @ModifiedDate    5Oct2016
@author          Ashish Master
@Description     Test class for batchLoanerKitRelUpdate */

@isTest
public class batchLoanerKitRelUpdateTest {

    static final Map<String,Schema.RecordTypeInfo> RECTYPESPRD = Schema.SObjectType.Product2.getRecordTypeInfosByName();    
    public static final Id RECTYPE_UROPHLOANERKITPRD = RECTYPESPRD.get('Uro/PH Loaner Kit').getRecordTypeId();
    public static final Id RECTYPE_Level5PRD = RECTYPESPRD.get('Standard Product Hierarchy - Level 5').getRecordTypeId();
    
    static TestMethod void TestbatchLoanerKitRelUpdate(){ 
        
        list<Product2> lstprd = new list<Product2>();
        Product2 prd;
        for(integer i = 0 ; i<150;i++){
            prd = new Product2();
            prd.Name = 'TestName'+i;
            prd.UPN_Material_Number__c = 'TestP'+i;
            prd.RecordtypeId = RECTYPE_Level5PRD;
            lstprd.add(prd);
        }
        insert lstprd;
        
        list<Product2> lstprdlk = new list<Product2>();
        for(integer i = 0 ; i<150;i++){
            prd = new Product2();
            prd.Name = 'TestNameLK'+i;
            prd.Uro_PH_Loaner_Kit_Id__c = 'Test'+i;
            prd.RecordtypeId = RECTYPE_UROPHLOANERKITPRD;
            lstprdlk.add(prd);
        }
        System.debug('======'+lstprd);
        insert lstprdlk;
        
        list<Uro_PH_Loaner_Kit_Relationship__c> lstlkrel= new list<Uro_PH_Loaner_Kit_Relationship__c>();
        Uro_PH_Loaner_Kit_Relationship__c lkrel;
        for(integer i= 0 ; i<3 ; i++){
             lkrel= new Uro_PH_Loaner_Kit_Relationship__c();
             lkrel.Uro_PH_Loaner_Kit__c = lstprdlk.get(1).id;
             lkrel.Uro_PH_Product__c = lstprd.get(i).id;
             lkrel.Uro_PH_Quantity__c = 2;
             lstlkrel.add(lkrel);
         }    
         insert lstlkrel;
         
        createStagingTable();
        
        Test.StartTest();
           ID batchprocessid = Database.executeBatch(new batchLoanerKitRelUpdate());
            System.assertNotEquals(batchprocessid,null);
            
           //Execute Schedulable Method
            batchLoanerKitRelUpdate sh1 = new batchLoanerKitRelUpdate();
            String sch = '0 0 23 * * ?'; 
            String jobId = system.schedule('Loaner Kit Update', sch, sh1); 
            System.assertNotEquals(jobId,null); 
        Test.StopTest();
        
        list<Uro_PH_Loaner_Kit_Staging_Table__c> lststageTable = [select id from Uro_PH_Loaner_Kit_Staging_Table__c];
        System.assert(lststageTable.size() == 0);
        list<Uro_PH_Loaner_Kit_Relationship__c> lstLoanerKitRel = [select id from Uro_PH_Loaner_Kit_Relationship__c];
        System.assert(lstLoanerKitRel.size() == 150);
    }
    
    public static void createStagingTable(){
        list<Uro_PH_Loaner_Kit_Staging_Table__c> lstStage = new list<Uro_PH_Loaner_Kit_Staging_Table__c>();
        Uro_PH_Loaner_Kit_Staging_Table__c stage;
        for(integer i= 0; i < 150 ; i++ ){
            stage = new Uro_PH_Loaner_Kit_Staging_Table__c();
            stage.Uro_PH_Loaner_Kit_Id__c = 'Test'+i;
            stage.Uro_PH_Product_Id__c  = 'testP'+i;
            stage.Uro_PH_Quantity__c = 1;
            lstStage.add(stage);
        }
        Insert lstStage;
    }
    
}
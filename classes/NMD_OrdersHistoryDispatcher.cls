public with sharing class NMD_OrdersHistoryDispatcher implements IRestHandler {
	
	final static String URI_MAPPING = '/nmd/orders/history';

	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		caller.setResponse(200, JSON.serialize(NMD_OrderHistoryManager.getOrdersHistory()));

	}

}
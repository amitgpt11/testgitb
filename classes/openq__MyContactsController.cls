/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MyContactsController {
    @RemoteAction
    global static openq.MyContactsController.ContactPopupWrapper getContact(Id contactId) {
        return null;
    }
global class ContactPopupWrapper {
}
}

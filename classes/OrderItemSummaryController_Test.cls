@isTest
private class OrderItemSummaryController_Test {

    private static testMethod void test() {

        //Create Products
        Test_DataCreator.createProducts();
        
        User objUser = Test_DataCreator.createCommunityUser(); 
        
        system.runAs(objUser) {
            
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            objOrder.Status__c = 'Open';
            update objOrder;
            
            Id productId = [Select Id From Product2 Limit 1].Id;
            Shared_Community_Order_Item__c objOrderItem = Test_DataCreator.createOrderItem(objOrder.Id, productId);
            
            OrderItemSummaryController orderItemObj = new OrderItemSummaryController();
            orderItemObj.receivedOrder = objOrder;
            Map<string,Shared_Community_Order_Item__c>orderItemMap = orderItemObj.getOrderLineItems();
            
            system.assertNotEquals(orderItemMap,null);
        }
    }
}
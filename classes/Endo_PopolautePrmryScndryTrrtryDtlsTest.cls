@isTest(SeeAllData=false)
public class Endo_PopolautePrmryScndryTrrtryDtlsTest {  
    public static testmethod void updateTerrAndOppyTeam(){
        
        Test.startTest();
               User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        Map<String,Schema.RecordTypeInfo> RECTYPES_Oppty = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
            Id RECTYPE_ENDO = RECTYPES_Oppty.get('Endo New Disposable Business').getRecordTypeId();
        
            Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
            Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
            list<Account> alist= new List<Account>{a1, a2};
            insert alist;
            
            //Prepare Users as Owner to Opportunity
            User u1 = UtilForUnitTestDataSetup.newUser();
            u1.Username = 'UserNameC@boston.com';
            u1.IsActive= True;
            User u2 = UtilForUnitTestDataSetup.newUser();
            u2.Username = 'UserNameR@boston.com';
            u2.IsActive= True;
             List<User> ulist=new list<User>{u1,u2};
            insert ulist;
            
            
            Id urId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE Name Like : 'US Endo%' limit 1].Id;
            
            User userC = UtilForUnitTestDataSetup.newUser('US ENDO User');
            userC.Username = 'EndoUserC1@xyz.com';
            userC.UserRoleId = urId;
            insert userC;
            
            List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type1();
            Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
            
            territory2 t = [Select Id from territory2 where Territory2TypeId =: Ttype[0].Id limit 1];
            
            Territory2 t3 = UtilForUnitTestDataSetup.newTerritory21('900',Territory2ModelIDActive, Ttype[0].Id, t.id); //For ENDO GI
            
            Territory2 t4 = UtilForUnitTestDataSetup.newTerritory21('500',Territory2ModelIDActive, Ttype[0].Id, t.id); //For ENDO PULM
            
            
            List<Territory2> trList=new List<Territory2> {t3,t4};
            insert trList;
            
            ObjectTerritory2Association OTAsso =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[0].Id, trList[0].Id);
            ObjectTerritory2Association OTAsso1 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[1].Id, trList[1].Id);
            ObjectTerritory2Association OTAsso2 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[0].Id, trList[1].Id);
            ObjectTerritory2Association OTAsso3 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[1].Id, trList[0].Id);
            insert new List<ObjectTerritory2Association> {OTAsso, OTAsso1,OTAsso2,OTAsso3};
            
            UserTerritory2Association UTAsso1 = UtilForUnitTestDataSetup.newUserTerritory2Association1(trList[1].Id, ulist[1].Id);
            UserTerritory2Association UTAsso2 = UtilForUnitTestDataSetup.newUserTerritory2Association2(trList[1].Id, ulist[0].Id);
            
            insert new List<UserTerritory2Association> {UTAsso1, UTAsso2};
            
            Opportunity oppty1 =UtilForUnitTestDataSetup.newOpportunity(alist[0].Id, 'Opty1');
            oppty1.territory2Id =trList[0].Id;
            oppty1.OwnerId =ulist[0].Id;
            oppty1.Territory_Assigned__c = False;
            
            insert new List<Opportunity> {oppty1};
                  
        PageReference pg = Page.Endo_UpdatePrimarySecondaryPage;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('Id',oppty1.Id);
        
        opportunity ent = new opportunity();
        
        ApexPages.StandardController st = new ApexPages.standardController(ent);
        Endo_PopolautePrimarySecondaryTrrtryDtls e = new Endo_PopolautePrimarySecondaryTrrtryDtls(st);
        
        e.updateGITerritoryBtn();
        e.updatePULMTerritoryBtn();
                        
        Test.stopTest();
    }
}
}
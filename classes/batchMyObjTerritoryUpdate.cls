/*
 @CreatedDate     08APR2016                                  
 @ModifiedDate    08APR2016                                  
 @author          Mayuri-Accenture
 @Description     This Batch Apex class checks for Territory single match/multiple match on Opportunity object from Opportunity Owner's territory and Opportunity's Account territory.
                  If single match found, assign the territory to Oppty and mark the Territory_Assigned__c checkbox to TRUE.
                  If multiple/no matches are found leave the Territory field on Oppty as is and mark Territory_Assigned__c checkbox to FALSE.
                  This happens when Account_Team_Member__c are added to Salesforce fom SAP. 
 @Methods         start(),Execute(),Finish() 
 @Requirement Id  
 */
 
 global class batchMyObjTerritoryUpdate implements Database.Batchable<sObject> { 
     
    public static final set<string> recordTypeNameSet = MyObjectivesRecordType__c.getall().keyset();
    static final Map<Id,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.My_Objectives__c.getRecordTypeInfosById();
    global Database.QueryLocator start(Database.BatchableContext BC) {
    Date dt;
    if(Test.isRunningTest()){   
        dt = system.today();
    }
    else{
        dt = system.today()-Integer.valueOf(system.Label.ETM_Batch_Integer_Subtractor_For_Date);
    }
    String query =  'select ' +
                    'Account_Team_Role__c, ' +
                    'Personnel_ID__r.User_for_account_team__c, ' +
                    'Personnel_ID__r.User_for_account_team__r.IsActive, ' +
                    'Account_Team_Share__c, ' +
                    'Deleted__c, ' +
                    'Territory_ID__c ' +
                    'from Account_Team_Member__c ' +
                    'where Account_Team_Role__c != null ' +
                    'and Account_Team_Share__c != null ' +
                    'and Personnel_ID__r.User_for_account_team__c != null ' +
                    'and Territory_ID__c != null ';
          if(Test.isRunningTest()){            
               query += 'and LastModifiedDate =: dt';
       }
       else{
             query += 'and LastModifiedDate >: dt';
       }           
                       
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account_Team_Member__c> scope) {
        set<Id> userIdLst = new set<Id>();
        set<Id> accountIdLst = new set<Id>();
        list<My_Objectives__c> myObjLst = new list<My_Objectives__c>();
        list<My_Objectives__c> myObjMainLst = new list<My_Objectives__c>();
        list<ObjectTerritory2Association> acctTerritoryLst = new list<ObjectTerritory2Association>();
        list<UserTerritory2Association> userTerritoryLst = new list<UserTerritory2Association>();
        set<string> terrotoryNamelst = new set<string>();
        map<string,Id> territoryNameIdMap = new map<string,Id>();
        map<Id,Territory2> territory2Map;
        UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();         
        map<id,set<Id>> acctTerritorymap = new map<id,set<Id>>();
        map<id,set<Id>> userTerritorymap = new map<id,set<Id>>();
        map<Id,Opportunity> opptyMap;
        list<Opportunity> opptyLst = new list<Opportunity>();
        set<Id> userIdLst1 = new set<Id>();
        set<Id> accountIdLst1 = new set<Id>();            
        
        for(Account_Team_Member__c a: scope){
            userIdLst.add(a.Personnel_ID__r.User_for_account_team__c);          //create a list of all the User Ids
            accountIdLst.add(a.Account_Team_Share__c);                          //Create a list of all Account Ids
        }
        
        //Query on My_Objectives__c whose owner is in UserList OR whose Account is in AccountList created above
        if(accountIdLst.size()>0 && userIdLst.size()>0){
            myObjMainLst = [SELECT Account_Performance__c,Contact__c,Division__c,End_Date__c,Id,Name,OwnerId,Products__c,Related_to_Account__c,Status__c,RecordTypeId,
                        Territory_Assigned__c,Territory_Realignment_Status__c,Territory__c 
                        FROM My_Objectives__c
                        WHERE Status__c != 'Completed' AND ( OwnerId IN : userIdLst OR Related_to_Account__c IN : accountIdLst )];
                                                
        }     
        system.debug(myObjMainLst+'--myObjMainLst--'); 
        if(myObjMainLst.size() > 0){ 
            for(My_Objectives__c mobj: myObjMainLst){
                if(mobj.RecordTypeId == null){
                     myObjLst.add(mobj);
                 }
                 else{
                     if(recordTypeNameSet.contains(RECTYPES.get(mobj.RecordTypeId).Name) == false){
                         myObjLst.add(mobj);
                     }
                 }
            } 
        } 
        system.debug(myObjLst+'--myObjLst--');  
        
        if(myObjLst.size() > 0){
            for(My_Objectives__c mo : myObjLst){
                terrotoryNamelst.add(mo.Territory__c);              //Create a list of all Territory Names
                if(mo.Related_to_Account__c != null){
                    accountIdLst1.add(mo.Related_to_Account__c);
                }
                if(mo.OwnerId != null){
                    userIdLst1.add(mo.OwnerId);
                }
            }
        }  
        if(terrotoryNamelst.size()>0){
            //Query Id and other fields related to Territory2 object
            territory2Map = new map<Id,Territory2>();
            territory2Map = util.getTerritoryDetailList(terrotoryNamelst);          //Since My_Objectives__c has Territory__c field, which is a text field and not look-up, send name list
                                                                                    //which gets back with the Territory details map
         }                  
         if(territory2Map != null){
             for(Id tId : territory2Map.keyset()){
                 territoryNameIdMap.put(territory2Map.get(tId).name,tId);           // Create a map of Territory Name-->Territory Id, which will help us to search for Territory match later.
             }
         }  
        system.debug(userIdLst+'--userIdLst--');
        system.debug(accountIdLst+'--accountIdLst--');
        //Query On ObjectTerritory2Association for all the Territories assigned to Accounts mentioned in accountIdLst
        if(accountIdLst1!=null && accountIdLst1.size()>0){
                                                                                                    //Call a Utility method to query ObjectTerritory2Association records 
            acctTerritorymap = util.getAcctnTeritryAssctnList(accountIdLst1);                       //-which returns map of Account Id-->List of Territories
        }
        //Query On UserTerritory2Association for all the Territories assigned to Users mentioned in userIdLst
        map<Id,string> userTerrDeatilsMap = new map<Id,string>();
        if(userIdLst1!=null && userIdLst1.size()>0){
            userTerritoryLst = util.getUserTeritryAssctnList(userIdLst1,true);                     //Call a Utility method to query UserTerritory2Association records            
            if(userTerritoryLst != null && userTerritoryLst.size() > 0){
                for(UserTerritory2Association ut : userTerritoryLst){
                    if(userTerritorymap.containsKey(ut.UserId)){
                        userTerritorymap.get(ut.UserId).add(ut.Territory2Id);                   //Create a map of User Id-->List of Territories
                    }
                    else{
                        userTerritorymap.put(ut.UserId,new set<Id>{ut.Territory2Id});
                    }
                    userTerrDeatilsMap.put(ut.Territory2Id,ut.Territory2.name);
                }
                
            }       
        }
    
        
        map<Id,Id> myObjTerAssignmap = new map<Id,Id>();
        map<Id,Id>  tempMyObjLst;       
        set<Id> myObjIdLstWthFalseCheckbx = new set<Id>();
        for(My_Objectives__c opt : myObjLst){       
            if(userTerritorymap.keyset().Contains(opt.OwnerId) && acctTerritorymap.keyset().Contains(opt.Related_to_Account__c)){       //If User to Territories map doesn't contain My_Objective's owner and Account to Territories map doesn't contain My_Objective's Account
                if(userTerritorymap.get(opt.OwnerId).size() > 0 && acctTerritorymap.get(opt.Related_to_Account__c).size() > 0){         //If User to Territories map doesn't have territories for My_Objective's owner and Account to Territories map doesn't have territories for My_Objective's Account                       
                    //Find a match for Territory                
                    integer cnt = 0;
                    for(Id tId : userTerritorymap.get(opt.OwnerId)){
                        if(acctTerritorymap.get(opt.Related_to_Account__c).contains(tId)){
                            cnt++;                                                                              //Increment the count when multiple matches are found
                            if(cnt == 1){
                                tempMyObjLst = new map<Id,Id>();
                                tempMyObjLst.put(opt.Id,tId);
                            }
                        }                           
                    }
                    if(cnt == 1){                                                                           //If only one match is found put it in a map of <My_Objective Id-->Territory id>
                        myObjTerAssignmap.putAll(tempMyObjLst);
                    }
                    if(cnt>1){
                        myObjIdLstWthFalseCheckbx.add(opt.id);                                              //If multiple matches are found put Oppty id in another map
                    } 
                    
                    if(opt.Territory__c  != null){
                        if(territoryNameIdMap.get(opt.Territory__c) != null){                                                                                       //If My_Objective's Territory field is not null--
                            if(userTerritorymap.get(opt.OwnerId).Contains(territoryNameIdMap.get(opt.Territory__c)) == false &&  acctTerritorymap.get(opt.Related_to_Account__c).Contains(territoryNameIdMap.get(opt.Territory__c)) == false){      //--and the two maps does not conatin the Oppty's Territory mark the checkbox on My_Objective to false--
                                if(!myObjTerAssignmap.keyset().contains(opt.id)){
                                    myObjIdLstWthFalseCheckbx.add(opt.id);  
                                }                                                                                                                                                               //--if it has then do nothing
                            }
                        }
                    }                       
                }
                else{
                    myObjIdLstWthFalseCheckbx.add(opt.id);                          //If both UserTerritoryMap and AccountTerritoryMap haveno Territories for Oppty owner and Oppty Account in it, mark the checkbox in Oppty to false
                }
            }
            else{
                    myObjIdLstWthFalseCheckbx.add(opt.id);                          //If both UserTerritoryMap and AccountTerritoryMap have no Oppty owner and no Oppty Account in it, mark the checkbox in Oppty to false
            }           
        }
        
        system.debug('--myObjTerAssignmap--'+myObjTerAssignmap);
        system.debug('--myObjIdLstWthFalseCheckbx--'+myObjTerAssignmap);
        list<My_Objectives__c> updatedMyObjLst = new list<My_Objectives__c>();
        system.debug('--userTerrDeatilsMap--'+userTerrDeatilsMap);
        system.debug('--myObjTerAssignmap--'+myObjTerAssignmap);
        if(myObjTerAssignmap != null && myObjTerAssignmap.keyset().size() > 0){
            for(Id objId : myObjTerAssignmap.keyset()){ 
                My_Objectives__c newMyObj = new My_Objectives__c(); 
                newMyObj.Id =  objId;           
                newMyObj.Territory__c = userTerrDeatilsMap.get(myObjTerAssignmap.get(objId));
                newMyObj.Territory_Assigned__c = true;
                updatedMyObjLst.add(newMyObj);
            }
        }
        system.debug('--updatedMyObjLst--'+updatedMyObjLst);
        if(myObjIdLstWthFalseCheckbx.size()>0){
            for(Id obj : myObjIdLstWthFalseCheckbx){
                My_Objectives__c newMyObj = new My_Objectives__c();
                newMyObj.Id =  obj;  
                newMyObj.Territory_Assigned__c = false;
                updatedMyObjLst.add(newMyObj);
            }
        }
        
        system.debug('--updatedMyObjLst before update--'+updatedMyObjLst);
        if(updatedMyObjLst != NULL && updatedMyObjLst.size()>0){
                    Database.SaveResult[] InsertResult = Database.update(updatedMyObjLst,TRUE);
                            
                    // Error handling code Iterate through each returned result
                    for (Database.SaveResult er : InsertResult) {
                        if (er.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully updated the Record. Record ID: ' + er.getId());
                            
                        }
                        else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : er.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Record fields that affected this error: ' + err.getFields());
                            }
                        }
                    }                   
             } 
             
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}
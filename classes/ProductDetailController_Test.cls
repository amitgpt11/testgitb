@isTest
private class ProductDetailController_Test {

    private static testMethod void test() {
    
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        Test_DataCreator.createUserSalesOrganisation([SELECT contact.AccountId FROM User WHERE id=: objUser.Id].contact.AccountId);
        
        Product2 objProduct = Test_DataCreator.createProduct('Test');
        objProduct.ProductCode = '';
        objProduct.Shared_Includes_Handle_Break__c = '';
        objProduct.Shared_Filter_Attribute_APIs__c = 'Shared_Working_Length_cm__c,Shared_Includes_Handle_Break__c';
        objProduct.Division__c = 'Endo';
        objProduct.Shared_Top_Level_Parent__c = true;
        update objProduct;
        Test_DataCreator.createSharedProductExtension(objProduct.Id);
        
        Product2 objProduct2 = Test_DataCreator.createProduct('Shared Test');
        objProduct2.Shared_Parent_Product__c = objProduct.Id;
        objProduct2.Shared_Includes_Handle_Break__c = 'Yes';
        objProduct2.Shared_Tip_Diameter_F_mm__c = '4.5|1.4';
        // objProduct2.Shared_Filter_Attribute_APIs__c = 'Shared_Working_Length_cm__c,Shared_Includes_Handle_Break__c';
        objProduct.Division__c = 'Endo';
        update objProduct2;
        Test_DataCreator.createSharedProductExtension(objProduct2.Id);
        
        Product2 objProduct3 = Test_DataCreator.createProduct('Shared Test 3');
        objProduct3.Shared_Parent_Product__c = objProduct.Id;
        objProduct3.Shared_Includes_Handle_Break__c = 'Yes';
        objProduct3.Shared_Working_Length_cm__c = 135;
        objProduct3.Shared_Tip_Diameter_F_mm__c = '4.5|1.2';
        
        // objProduct3.Shared_Filter_Attribute_APIs__c = 'Shared_Working_Length_cm__c,Shared_Includes_Handle_Break__c';
        objProduct.Division__c = 'Endo';
        update objProduct3;
        Test_DataCreator.createSharedProductExtension(objProduct3.Id);
        
        Test.StartTest();
        
        system.runAs(objUser){
            
            PageReference myVfPage = Page.ProductDetail;
            Test.setCurrentPageReference(myVfPage); 
            ProductDetailController objProductDetailController = new ProductDetailController();
            objProductDetailController.init();
            
            /*objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id','');
            objProductDetailController.fetchRequestedData();
            system.assertNotEquals(objProductDetailController.errorMessage,null);
            
            objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id','testabc');
            objProductDetailController.fetchRequestedData();
            system.assertNotEquals(objProductDetailController.errorMessage,null);*/
            
            objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id',objProduct2.Id);
            objProductDetailController.fetchRequestedData();
            
            
            objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id',objProduct.Id);
            objProductDetailController.fetchRequestedData();
            system.assertNotEquals(objProductDetailController.errorMessage,null);
            
            ApexPages.currentPage().getParameters().put('attributeName','Shared_Includes_Handle_Break__c');
            ApexPages.currentPage().getParameters().put('attributeName','Shared_Tip_Diameter_F_mm__c');
           /* system.debug('mapAttributeToValues=============='+objProductDetailController.mapAttributeToValues);
            objProductDetailController.mapAttributeToValues.get('Shared_Includes_Handle_Break__c').attributeSelectedOption = 'yes';
            objProductDetailController.filterProductAttribute(); ApexPages.currentPage().getParameters().put('attributeName','Shared_Working_Length_cm__c');
            objProductDetailController.mapAttributeToValues.get('shared_working_length_cm__c').attributeSelectedOption = '135.0';
            objProductDetailController.filterProductAttribute(); 
           
            objProductDetailController.AddItems();*/
            
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            objOrder.Status__c = 'Open';
            update objOrder;
            objProductDetailController.filterProductAttribute();
            objProductDetailController.AddItems();
            
           /* ApexPages.currentPage().getParameters().put('attributeName','Shared_Includes_Handle_Break__c');
            objProductDetailController.mapAttributeToValues.get('Shared_Includes_Handle_Break__c').attributeSelectedOption = 'yes';
            objProductDetailController.filterProductAttribute();
            
            ApexPages.currentPage().getParameters().put('attributeName','Shared_Working_Length_cm__c');
            objProductDetailController.mapAttributeToValues.get('shared_working_length_cm__c').attributeSelectedOption = '135.0';
            objProductDetailController.filterProductAttribute();*/
           
            objProductDetailController.AddItems();
           
            objProductDetailController.clearFilter();
        }
        
       /* 
        
        User objLocaleUser = Test_DataCreator.createCommunityUserWithLocale();
        Test_DataCreator.createUserSalesOrganisation([SELECT contact.AccountId FROM User WHERE id=: objUser.Id].contact.AccountId);
        
        
        system.runAs(objLocaleUser){
            
            ProductDetailController objProductDetailController = new ProductDetailController();
            
            Shared_Community_Product_Translation__c objSharedTranslation = Test_DataCreator.createProductTranslationRecord(objProduct.Id,objLocaleUser.Shared_Community_Division__c);
            objSharedTranslation.Shared_Language__c = objProductDetailController.mapLocaleToLanguage.get(objLocaleUser.LanguageLocaleKey);
            update objSharedTranslation;
            
            ApexPages.currentPage().getParameters().put('Id',objProduct.Id);
            objProductDetailController.fetchRequestedData();
            
        }
        
        Test.StopTest();*/
    }
}
/**
* Trigger for the Assignee custom object
*
* @Author salesforce Services
* @Date 04/07/2015
*/
@isTest(SeeAllData=false)
private class AssigneeTriggerHandlerTest {
	
	static final String TERRITORY = 'TERRITORY';

	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Seller_Hierarchy__c seller = td.newSellerHierarchy(TERRITORY);
		insert seller;

	}

	static testMethod void test_AssigneeTriggerTM1() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Seller_Hierarchy__c seller = [select Id from Seller_Hierarchy__c where Name = :TERRITORY];

		Assignee__c assignee = td.newAssignee('Name', UserInfo.getUserId(), seller.Id);
		insert assignee;

		assignee.Rep_Designation__c = AssigneeManager.TM1;
		update assignee;

		seller = [select Current_TM1_Assignee__c from Seller_Hierarchy__c where Id = :seller.Id];
		System.assertEquals(UserInfo.getUserId(), seller.Current_TM1_Assignee__c);

		Assignee__c assignee2 = td.newAssignee('Name2', UserInfo.getUserId(), seller.Id);
		assignee2.Rep_Designation__c = AssigneeManager.TM1;
		insert assignee2;

		delete assignee2;

	}

	static testMethod void test_AssigneeTriggerTM2() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Seller_Hierarchy__c seller = [select Id from Seller_Hierarchy__c where Name = :TERRITORY];

		Assignee__c assignee = td.newAssignee('Name', UserInfo.getUserId(), seller.Id);
		insert assignee;

		assignee.Rep_Designation__c = AssigneeManager.TM2;
		update assignee;

		seller = [select Current_TM2_Assignee__c from Seller_Hierarchy__c where Id = :seller.Id];
		System.assertEquals(UserInfo.getUserId(), seller.Current_TM2_Assignee__c);

		Assignee__c assignee2 = td.newAssignee('Name2', UserInfo.getUserId(), seller.Id);
		assignee2.Rep_Designation__c = AssigneeManager.TM2;
		insert assignee2;

		delete assignee2;

	}

}
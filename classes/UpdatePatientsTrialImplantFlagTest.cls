@isTest(SeeAllData=false)
public class UpdatePatientsTrialImplantFlagTest {
    
    public static List<opportunity> setupOppty(){
        NMD_TestDataManager td= new NMD_TestDataManager();
        Account acc = td.newAccount('TestAcc');
        insert acc;
        Patient__c p = td.newPatient('Sam','Ronny');
        p.Trial_Yes__c= false;
        p.Implant_Yes__c= false;
        insert p;
        Opportunity testopp1 = td.newOpportunity(acc.id);
        testopp1.Patient__c =p.Id;
        testopp1.RecordTypeId= OpportunityManager.RECTYPE_TRIAL_NEW;
        testopp1.Actual_Trial_Date__c = date.today();         
        
        
        
        Opportunity testopp2 = td.newOpportunity(acc.id);
               
        testopp2.Patient__c = p.Id;
        testopp2.Actual_Procedure_Date__c= date.today();
        testopp2.RecordTypeId=OpportunityManager.RECTYPE_IMPLANT;
        
        List<opportunity> oList= new List<Opportunity> { testopp1, testopp2 };
            insert oList;
			return oList;        
        }
        
    static testMethod void test_PatientInfo(){
         //setupOppty();
         list<opportunity> opptyList= new List<Opportunity>();
        opptyList=setupOppty();
        Test.startTest();
            UpdatePatientsTrialImplantFlag trialimp = new UpdatePatientsTrialImplantFlag(); 
           
             trialimp.PatientInfo(opptyList);
        	Patient__c p=[Select Id,Trial_Yes__c from Patient__c where Patient_First_Name__c='Sam'];
        	
            System.assertEquals(true,p.Trial_Yes__c);
      	Test.stopTest();
       
    
    }
}
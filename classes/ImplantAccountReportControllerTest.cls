/**
* Test Class for ImplantAccountReport controller and ImplantReportsExtensionAccount page.       
*     
* @CreatedBy   Payal Ahuja
* @CreatedDate 28/09/2016
*/
@isTest(SeeAllData=false)
public with sharing class ImplantAccountReportControllerTest {
    
    public static testmethod void createAccountTestMethod(){   
       
Test.startTest();
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        
       //@TestSetup
        NMD_TestDataManager td = new NMD_TestDataManager();
        
        Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
        Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
        list<Account> alist= new List<Account>{a1, a2};
        insert alist; 
        
      //create contact records     
        
        Contact trialing = td.newContact(a1.Id, 'TestP');
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        
        Contact procedure = td.newContact(a2.Id, 'Testp2');
        procedure.AccountId = a1.Id;
        procedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        procedure.MailingStreet = '123 Main St';
        procedure.MailingCity = 'test';
        procedure.MailingState = 'test';
        procedure.MailingPostalCode = 'test';
        procedure.MailingCountry = 'test';
        procedure.Fax = 'test';       
        procedure.Phone = '5556667788';
        
        Contact implant = td.newContact(a1.Id, 'TestP3');
        implant.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { 
            trialing, procedure , implant
        };
        System.debug('My debug statement');
        system.assertEquals(1,1);
        
        Shared_Patient_Case__c primaryProcedure = new Shared_Patient_Case__c();
        primaryProcedure.Shared_Facility__c = a1.Id ;
        primaryProcedure.Shared_Start_Date_Time__c = Date.newInstance(2016,9,26);
        insert primaryProcedure;
        
        
        //create test implant records
        LAAC_Implant__c primaryImplant = new LAAC_Implant__c();
        primaryImplant.LAAC_Implant_Patient_Case__c = primaryProcedure.Id;
        primaryImplant.LAAC_Case_Aborted__c=false;
        insert primaryImplant;
        
        //create test procedure records
        Shared_Patient_Case__c primaryProcedure1 = new Shared_Patient_Case__c();
        primaryProcedure1.Shared_Facility__c = a1.Id ;
        primaryProcedure1.Shared_Start_Date_Time__c = Date.newInstance(2016,9,26);
        primaryProcedure1.Shared_End_Date_Time__c = Date.newInstance(2016,9,30);
        insert primaryProcedure1;

        
        Shared_Patient_Case__c primaryProcedure2 = new Shared_Patient_Case__c();
        primaryProcedure2.Shared_Facility__c = a1.Id ;      
        primaryProcedure.Shared_Start_Date_Time__c = Date.parse('12/10/16');
        primaryProcedure.Shared_End_Date_Time__c = Date.parse('12/10/16');
        insert primaryProcedure2;

        //build page
        PageReference pg = Page.ImplantReportsExtensionAccount;
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('Id',trialing.Id);
        
        Account ent = new Account();
        ApexPages.StandardController st = new ApexPages.standardController(ent);
        ImplantAccountReportController e = new ImplantAccountReportController(st);                           

Test.stopTest();
        
     }
    }
}
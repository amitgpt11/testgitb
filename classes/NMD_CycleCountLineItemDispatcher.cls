/**
* Custom REST dispatcher for Cycle Count Line Items
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class NMD_CycleCountLineItemDispatcher implements IRestHandler {
    
    @TestVisible
    final static String KEY_ACTION = 'action';
    
    // mapping, ie: /nmd/products/{pricebookName}
    final static String URI_MAPPING = (String.format('/nmd/ccitems/\'{\'{0}\'}\'', new List<String> {
        KEY_ACTION
    }));

    final static String ACTION_INSERT = 'insert';
    final static String ACTION_UPDATE = 'update';
    final static String ACTION_UPSERT = 'upsert';
    final static String ACTION_VERIFY = 'verify';

    @TestVisible
    final static String PARAM_CCRAID = 'cid';

    public String getURIMapping() {
        return URI_MAPPING;
    }

    public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

        // check for valid action
        String action = parameters.get(KEY_ACTION);
        Set<Id> itemIds = new Set<Id>();

        if (action == ACTION_UPDATE || action == ACTION_INSERT || action == ACTION_UPSERT) {    

            // check for a request body
            if (requestBody == null || requestBody.size() == 0) {
                caller.setResponse(400, 'Missing Request Body');
                return;
            }

            try {
                
                // attempt to cast the body into a list of records
                List<Cycle_Count_Line_Item__c> items = (List<Cycle_Count_Line_Item__c>)JSON.deserialize(requestBody.toString(), List<Cycle_Count_Line_Item__c>.class);

                // perform requested DML
                if (action == ACTION_INSERT || action == ACTION_UPDATE) {
                    DML.save(this, items);
                }   
                else if (action == ACTION_UPSERT) {
                    //upsert items;
                    DML.evaluateResult(this, Database.upsert(items));
                }

                itemIds.addAll(new Map<Id,Cycle_Count_Line_Item__c>(items).keySet());

            }
            catch(System.JSONException je) {
                // return JSON error
                caller.setResponse(400, je.getMessage());
                return;
            }
            catch (System.DMLException de) {
                // return DML error
                caller.setResponse(400, de.getDmlMessage(0));
                return;
            }

        }
        else if (action == ACTION_VERIFY) {

            String param = parameters.get(PARAM_CCRAID);
            Id ccraId;

            if (String.isNotBlank(param)) {
                try {
                    ccraId = (Id)param;
                }
                catch (System.TypeException te) {}
            }

            if (ccraId == null) {
                caller.setResponse(400, 'Invalid/Missing Parameter: `' + PARAM_CCRAID + '`.');
                return;
            }
            else {
                itemIds.addAll(CycleCountLineItemManager.createVerifications(ccraId));
            }

        }
        else {
            caller.setResponse(400, 'Invalid/Missing Action.');
            return;
        }

        // return back the list of records, but with their child verifications embedded
        caller.setResponse(200, JSON.serialize(new List<Cycle_Count_Line_Item__c>([
            select
                Name,
                Inventory_Item__r.Model_Number__r.Name,
                Inventory_Item__r.Model_Number__r.EAN_UPN__c,
                Inventory_Item__r.Model_Number__r.ProductCode,
                Inventory_Item__r.Product_Family__c,
                Inventory_Item__r.Material_Number_UPN__c,
                Inventory_Item__r.Serial_Number__c,
                Inventory_Item__r.Lot_Number__c,
                Other_Inventory_Item__r.Model_Number__r.Name,
                Other_Inventory_Item__r.Model_Number__r.EAN_UPN__c,
                Other_Inventory_Item__r.Model_Number__r.ProductCode,
                Other_Inventory_Item__r.Product_Family__c,
                Other_Inventory_Item__r.Material_Number_UPN__c,
                Other_Inventory_Item__r.Serial_Number__c,
                Other_Inventory_Item__r.Lot_Number__c,
                Surgery_Item__r.Model_Number__r.Name,
                Surgery_Item__r.Model_Number__r.Family,
                Entry_Method__c,
                Exception_Notes__c,
                Exception_Reason__c,
                Reconcile_Type__c,
                Scanned_Expiration_Date__c,
                Scanned_Lot_Number__c,
                Scanned_Model_Number__c,
                Scanned_Serial_Number__c,
                Scanned_Quantity__c,
                (
                    select
                        Actual_Quantity__c,
                        Cycle_Count_Line_Item__c,
                        Lot_Number__c,
                        Model_Number__c,
                        Notes__c,
                        Quantity__c,
                        Reconcile_Type__c,
                        Serial_Number__c,
                        Verification__c,
                    	Is_Auto_Updated__c
                    from Verifications__r
                )
            from Cycle_Count_Line_Item__c
            where Id in :itemIds
        ])));

    }

}
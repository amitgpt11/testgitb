/**
* Class Name : ContractListController
* Serves as controller for ContractList Page
* Provides contacts for Users division
*
* Only meant to be used for page where Standard Controller is Account
*
* Created : 15-9-2016
*
**/
public with sharing class ContractListController{
    /*
    * List of contracts for this account, of the EP division
    */
    public List<Contract> contracts { get;  set; }
    public boolean active{get;set;}
    public boolean Inactive{get;set;}
    private static final String EP_DIVISION = 'EP';
    private static final String EXPIRED_STATUS = 'Expired';
    private static final String IMPLEMENTED_STATUS = 'Implemented';
    private static final List<String> STATUS_LIST = new List<String>{EXPIRED_STATUS,IMPLEMENTED_STATUS};
    public String Field_Label {get;set;}
    public List<ContractRelatedListFields__c> fieldLabels{get;set;}
    /*
    * Constructor
    * Initializes the contract list
    */
    public ContractListController(ApexPages.StandardController sc) {
       active=false;
       Inactive = false;
       fieldLabels = new List<ContractRelatedListFields__c>();
       try{
            this.contracts = new List<Contract>();
            Id accId = sc.getId();
            fieldLabels = [select Name,FieldApi__c ,Field_Sequence__c,Field_Label__c From ContractRelatedListFields__c Order By Field_Sequence__c ASC];
            String query  = 'Select id,Name';
            for(ContractRelatedListFields__c cont : fieldLabels  )
            {  
              if(cont.Field_Sequence__c > 1){
                  query+=','+cont.FieldApi__c;
              }
            }
            query += ' from Contract '+
                    'where AccountId =:accId AND status IN : STATUS_LIST';
            System.debug('query:'+query);     
            
            List<String> spliDiv = new List<String>();    
            List<Contract> contList = new List<Contract>();
            if (accId != null) {
                System.debug('accId:'+accId);
                contList = Database.query(query); 
                if(!contList.isEmpty()){
                    for(Contract cont : contList )
                    {
                        String divisions = cont.Shared_Divisions__c;
                        if(null != divisions ){
                        System.debug('divisions:'+divisions);
                        spliDiv = divisions.split(';');
                        System.debug('spliDiv :'+spliDiv );
                        Set<String> spliDivSet = new Set<String>(spliDiv);  
                        System.debug('spliDivSet :'+spliDivSet );   
                        if(!spliDivSet.isEmpty()  && spliDivSet.contains('EP'))
                        {
                           this.contracts.add(cont);
                        }   
                       }        
                   } 
                }               
                System.debug('Contracts:'+this.contracts);
                if(!this.contracts.isEmpty()){
                   active = true;}
                 else{
                   Inactive = true;}
                }
          }
          catch(Exception e)
          {
              System.debug('Exception e :'+e);
          }   
    }
}
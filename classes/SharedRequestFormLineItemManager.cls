/*
* Manager class for the SharedRequestFormLineItemHandler
*
* @Author Accenture Services
* @Date 10OCT2016
*/

public class SharedRequestFormLineItemManager {
    
public static map<Id,Shared_Request_Form__c> parentMap = new map<Id,Shared_Request_Form__c>();
list<Shared_Request_Form__c> parentLstToBeUpdated = new list<Shared_Request_Form__c>();
list<Shared_Request_Form_Line_Item__c> toBeDeletedLst = new list<Shared_Request_Form_Line_Item__c>();    
static final Map<String,Schema.RecordTypeInfo> RECTYPES_RequestForm = Schema.SObjectType.Shared_Request_Form__c.getRecordTypeInfosByName();
public static final Id RECTYPE_Sample_Request = RECTYPES_RequestForm.get('PI Sample Request Form').getRecordTypeId();
    
    
public void preventChildDeletion(Shared_Request_Form_Line_Item__c lineItem){
    system.debug('parentMap-->'+parentMap);        
    
    if(lineItem.Request_Form__c != null && parentMap.containsKey(lineItem.Request_Form__c)){
        if(parentMap.get(lineItem.Request_Form__c).PI_Approval_Status__c == 'Approved' || parentMap.get(lineItem.Request_Form__c).PI_Approval_Status__c  == 'Rejected'){
            lineItem.addError('You cannot delete the record, since the parent Sample Request Form is in Approved/Rejected Status');
            system.debug('You cannot delete the record, since the parent Sample Request Form is in Approved/Rejected Status');
        }
        else{
            toBeDeletedLst.add(lineItem);
        }
    }
    
}
    
    
//Fetch all parents' approval status and create a map
public void fetchParentIdStatusMap(list<Shared_Request_Form_Line_Item__c> newRecords){
    set<Id> parentIdSet = new set<Id>();
    for(Shared_Request_Form_Line_Item__c cId : newRecords){
        if(cId.Request_Form__c != null){
            parentIdSet.add(cId.Request_Form__c);
        }
   }
   if(parentIdSet != null && parentIdSet.size() > 0){
       for(Shared_Request_Form__c parent: [Select Id,PI_Approval_Status__c,PI_Total_Price_Of_Line_Items__c,RecordTypeId FROM Shared_Request_Form__c WHERE Id IN:parentIdSet]){
          parentMap.put(parent.Id,parent);
       }           
   }
   system.debug('parentMap--'+parentMap);
    
} 
    
    
public void updateParentTotalSamplePrice(list<Shared_Request_Form_Line_Item__c> newRecords){
    set<Id> parentIdSet = new set<Id>();
    if(newRecords != null && newRecords.size() > 0){
        for (Shared_Request_Form_Line_Item__c rItem : newRecords) { 
            if(parentMap != null && parentMap.containsKey(rItem.Request_Form__c)){            
                if(parentMap.get(rItem.Request_Form__c).RecordTypeId == RECTYPE_Sample_Request && rItem.PI_Total_Price_Of_Line_Item__c != null){                
                    parentMap.get(rItem.Request_Form__c).PI_Total_Price_Of_Line_Items__c  += rItem.PI_Total_Price_Of_Line_Item__c;
                    if(!parentIdSet.contains(parentMap.get(rItem.Request_Form__c).Id)){
                        parentLstToBeUpdated.add(parentMap.get(rItem.Request_Form__c));
                        parentIdSet.add(parentMap.get(rItem.Request_Form__c).Id); 
                    }
                    
                }  
            }      
        }
    }
    system.debug('parentLstToBeUpdated--'+parentLstToBeUpdated);
}
    
public void commitRecordDeletion(){
    if(toBeDeletedLst != null && toBeDeletedLst.size() > 0){
        try{
            delete toBeDeletedLst ;
        }catch(Exception e){
            system.debug('exception caught--'+e);
        }
    }
}


public void commitParentRecords(){
    if(parentLstToBeUpdated != null && parentLstToBeUpdated.size() > 0){
        try{
            update parentLstToBeUpdated;
        }catch(Exception e){
            system.debug('exception caught--'+e);
        }
    }
}
    
}
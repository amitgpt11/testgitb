/**
* Manager class for the Patient__c object
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class PatientManager {
  
  static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Patient__c.getRecordTypeInfosByName();

  static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};
  public static final Id RECTYPE_CUSTOMER = RECTYPES.get('Patient Customer').getRecordTypeId();
  public static final Id RECTYPE_PROSPECT = RECTYPES.get('Patient Prospect').getRecordTypeId();
  public static final Id RECTYPE_ANZ = RECTYPES.get('ANZ Patient').getRecordTypeId();
  static final Set<String> rowCauseType = new Set<String>{'Manual'};  

  static final Set<String> CARE_SOURCES = new Set<String> {
    'CARE Card', 'CARE Online'
  };
  public static final Set<Id> RECTYPES_NMD = new Set<Id> {
    PatientManager.RECTYPE_CUSTOMER, PatientManager.RECTYPE_PROSPECT
  };  

  private Map<String,Seller_Hierarchy__c> territoriesByRepCode = new Map<String,Seller_Hierarchy__c>{};
  private Map<String,Seller_Hierarchy__c> territoriesByPostalCode = new Map<String,Seller_Hierarchy__c>{};
  private Map<Id,Seller_Hierarchy__c> territoriesById = new Map<Id,Seller_Hierarchy__c>{};  
  private Set<Id> updatedSAPIDs = new Set<Id>();
  private NMD_Patient_Lead_Preform_Settings__c preformValues = NMD_Patient_Lead_Preform_Settings__c.getInstance();
  private List<Lead> nmdLeads = new List<Lead>{};  
  private List<Clinical_Data_Summary__c> clinicalDataSummaries = new List<Clinical_Data_Summary__c>{};
  private Patient__c PatientListAll = new Patient__c();
  private Boolean clinicalDataSummariesCommitted = false;

  private NMD_AutoSubmitManager submitManager = null;

    /**
    * Initialize the NMD_AutoSubmitManager if there are records that need to be validated and submitted
    * 
    * @param void
    */  
  public void initSubmissionManager(){
    List<SObject> patientProspects = new List<SObject>();
    for(SObject obj : trigger.new){
      Patient__c patient = (Patient__c)obj;
      if(patient.RecordTypeId == RECTYPE_PROSPECT
        && patient.is_Submitted_to_SAP__c == false
      ){
        patientProspects.add(obj);
      }
    }

    if(!patientProspects.isEmpty()){
      submitManager = new NMD_AutoSubmitManager();
      submitManager.init(Patient__c.getSObjectType(), patientProspects);
    }
  }

    /**
    * If the NMD_AutoSubmitManager is initialized, check the provided record to see if it is valid
    * 
    * @param SObject obj
    */  
  public void checkForAutoSubmit(SObject oldObj, SObject obj){
    Patient__c oldPatient = (Patient__c) oldObj;

    if(submitManager != null
      && oldPatient.BSN_Update__c != 'Request SAP ID'
    ){
      submitManager.validateRecord(obj);
    }
  }

    /**
    * If the NMD_AutoSubmitManager is initialized, submit all valid records to quincy
    * 
    * @param void
    */  
  public void autoSubmitRecords(){
    if(submitManager != null){
      submitManager.queueQuincyEmails();
    }
  }

  public void findTerritories() {

    Set<Id> patientTerritoryIds = new Set<Id>{};
    for (Patient__c patient : (List<Patient__c>)trigger.new) {
      if (patient.Territory_ID__c == null) {
        if (String.isNotBlank(patient.Rep_Code__c)) {
          territoriesByRepCode.put(patient.Rep_Code__c, null);
        }
        if (String.isNotBlank(patient.Zip__c)) {
          territoriesByPostalCode.put(patient.Zip__c, null);
        }
      }
      if (patient.Territory_ID__c != null) {
        patientTerritoryIds.add(patient.Territory_ID__c);
      }              
    }

    if (!territoriesByRepCode.isEmpty()) {

      for (Seller_Hierarchy__c territory : [
        select 
          Current_TM1_Assignee__c, 
          Rep_Code__c
        from Seller_Hierarchy__c
        where Rep_Code__c in :territoriesByRepCode.keySet()
        and Current_TM1_Assignee__c != null
      ]) {
        territoriesByRepCode.put(territory.Rep_Code__c, territory);
      }

    }

    if (!territoriesByPostalCode.isEmpty()) {

      for (Postal_Code_Relationship__c pcr : [
        select
          Postal_Code_Reference__r.Postal_Code__c,
          Territory_ID__c,
          Territory_ID__r.Current_TM1_Assignee__c
        from Postal_Code_Relationship__c
        where Postal_Code_Reference__r.Postal_Code__c in :territoriesByPostalCode.keySet()
        and Territory_ID__r.Current_TM1_Assignee__c != null
        and Division__c = 'NM'
      ]) {
        territoriesByPostalCode.put(pcr.Postal_Code_Reference__r.Postal_Code__c, new Seller_Hierarchy__c(
          Id = pcr.Territory_ID__c,
          Current_TM1_Assignee__c = pcr.Territory_ID__r.Current_TM1_Assignee__c
        ));
      }

    }
    
    if (!patientTerritoryIds.isEmpty() && trigger.isUpdate) {
      this.territoriesById.putAll([
        select Current_TM1_Assignee__c
        from Seller_Hierarchy__c
        where Id in :patientTerritoryIds
        and Current_TM1_Assignee__c != null
      ]);
    }
  }
    //format phone number that is missing formatting
  public void formatPhone(Patient__c patient) {

    patient.Patient_Phone_Number__c = formatPhoneNumber(patient.Patient_Phone_Number__c);
    patient.Patient_Mobile_Number__c = formatPhoneNumber(patient.Patient_Mobile_Number__c);
    patient.Patient_Other_Number__c = formatPhoneNumber(patient.Patient_Other_Number__c);

  }

  private String formatPhoneNumber(String rawNumber) {

    // keep the original value
    String orgNumber = rawNumber;

    if (String.isNotBlank(rawNumber)) {
      // remove all non-numeric characters
      rawNumber = rawNumber.replaceAll('\\D+', '');

      // if 11 digits with a leading 1, then remove the leading 1
      if (rawNumber.length() == 11 && rawNumber.startsWith('1')) {
        rawNumber = rawNumber.substring(1);
      }

      // if it's 10 digits then format
      if (rawNumber.length() == 10) {
        rawNumber = String.format('({0}) {1}-{2}', new List<String> {
          rawNumber.substring(0, 3),
          rawNumber.substring(3, 6),
          rawNumber.substring(6)
        });
      }
      // otherwise set back to the original value
      else {
        rawNumber = orgNumber;
      }

    }

    return rawNumber;

  }

  public void assignTerritory(Patient__c patient) {
Patient.PainMapReparented__c =true;
    if (patient.Territory_ID__c == null) {

      Seller_Hierarchy__c territory;

      if (String.isBlank(patient.Rep_Code__c)) {
        //if (territoriesByPostalCode.containsKey(patient.Zip__c)) {
          territory = territoriesByPostalCode.get(patient.Zip__c);
        //}
      }
      else {
        //if (territoriesByRepCode.containsKey(patient.Rep_Code__c)) {
          territory = territoriesByRepCode.get(patient.Rep_Code__c);
        //}
      }

      if (territory != null) {
        patient.Territory_ID__c = territory.Id;
        patient.OwnerId = territory.Current_TM1_Assignee__c;
      }

    }
    else {
      // TODO: set owner if Territory is already set
    }

  }
  //update patient record owner
  public void updatePatientOwner(Patient__c oldPatient, Patient__c patient){
    if (((patient.RecordTypeId == RECTYPE_CUSTOMER 
      || patient.RecordTypeId == RECTYPE_PROSPECT)        
      && oldPatient.Territory_ID__c != patient.Territory_ID__c
    ) && (patient.Territory_ID__c != null && territoriesById.containsKey(patient.Territory_ID__c))) {
      patient.OwnerId = territoriesById.get(patient.Territory_ID__c).Current_TM1_Assignee__c;
    }
  }
  
  public void checkSAPID(Patient__c oldPatient, Patient__c newPatient) {
    if (oldPatient.SAP_ID__c != newPatient.SAP_ID__c && newPatient.SAP_ID__c != null) {
            this.updatedSAPIDs.add(newPatient.Id);
        }
  }

  public void cascadeSAPIDInfo() {

    if (this.updatedSAPIDs.isEmpty()) return;

    Set<Id> orderIds = new Set<Id>();
        List<Opportunity> opptys = new List<Opportunity>();
        for (Opportunity oppty : [
            select
                Patient__r.SAP_ID__c,
                Patient_SAP_ID__c,
                (
                  select Id
                  from Orders
                  where RecordTypeId = :OrderManager.RECTYPE_PATIENT
                  and Stage__c = 'Pending SAP ID'
                )
            from Opportunity
            where Patient__c in :this.updatedSAPIDs
        ]) {
            
            if (oppty.Patient_SAP_ID__c != oppty.Patient__r.SAP_ID__c) {
                
                oppty.Patient_SAP_ID__c = oppty.Patient__r.SAP_ID__c;
                opptys.add(oppty);

            }

            if (!oppty.Orders.isEmpty()) {
              orderIds.addAll(new Map<Id,Order>(oppty.Orders).keySet());
            }

        }

        if (!opptys.isEmpty()) {
          DML.save(this, opptys, false);
            //update opptys;
        }

        if (!orderIds.isEmpty()) {
          OrderManager.confirmPatientOrders(orderIds); //@future
        }

  }
    
    //recalculate sharing table  
  public void patientSharing(List<Patient__c> patients) {

    // just build the sets of expected users for each patient
    Map<Id,Set<Id>> userIdMap = new Map<Id,Set<Id>>();
    Map<Id,List<Patient__c>> patientsByTtyId = new Map<Id,List<Patient__c>>();

    // map each patient by their territory
    for (Patient__c patient : patients) {
      if (!RECTYPES_NMD.contains(patient.RecordTypeId)) continue;
      if (patient.Territory_ID__c == null) continue;

      userIdMap.put(patient.Id, new Set<Id> {});

      if (patientsByTtyId.containsKey(patient.Territory_ID__c)) {
        patientsByTtyId.get(patient.Territory_ID__c).add(patient);
      }
      else {
        patientsByTtyId.put(patient.Territory_ID__c, new List<Patient__c> { patient });
      }
      
    }

    if (userIdMap.isEmpty()) return;

    // queue a share for each active user (assignee) in the territory
    for (Assignee__c assignee : [
      select
        Assignee__c,
        Territory__c
      from Assignee__c
      where Territory__c in :patientsByTtyId.keySet()
      and Assignee__r.IsActive = true
    ]) {

      for (Patient__c patient : patientsByTtyId.get(assignee.Territory__c)) {
        // add assignee to patient mapping
        userIdMap.get(patient.Id).add(assignee.Assignee__c);
      }

    }

    // apply delta shares.  this will add/remove shares as needed
    new SObjectSharingManager(Patient__Share.getSObjectType(), userIdMap.keySet()).applyDeltaShares(userIdMap);

  }    

  public void createNmdLead(Patient__c patient) {

    if (CARE_SOURCES.contains(patient.Source__c)) {
      nmdLeads.add(new Lead(
        Company = preformValues.Default_Company__c,
        LeadSource = patient.Source__c,
        LastName = preformValues.Default_LastName__c,
        RecordTypeId = preformValues.NMD_Patient_RecordTypeId__c,
        OwnerId = patient.OwnerId,
        Patient__c = patient.Id,
            CARE_Card_Physician__c = patient.CARE_Card_Physician__c,
            Trialing_Physician__c = patient.Physician_of_Record__c,
            Territory_ID__c = patient.Territory_ID__c
      ));
    }

  }

  public void commitNmdLeads() {
    if (!nmdLeads.isEmpty()) {
      //sinsert nmdLeads;

      //insert leads and use DML Options to trigger assignment rules when leads are inserted
      Database.DMLOptions dmlo = new Database.DMLOptions();
      dmlo.assignmentRuleHeader.useDefaultRule = true;
      DML.evaluateResults(this, Database.insert(nmdLeads, dmlo));

    }
  }
  
 public void createClinicalDataSummary(Patient__c  Patient) 
 {
    system.debug('inside Clinical Data Summaries to be added :'+Patient+' >>>>> '+RECTYPE_ANZ);
      
        if(  Patient.RecordTypeId == RECTYPE_ANZ)
            {
             this.clinicalDataSummaries.addAll(new List<Clinical_Data_Summary__c> {
                new Clinical_Data_Summary__c(
            Patient__c = Patient.Id,
              IsActive__c =true,
            RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANZPAINMAP 
                ),
                new Clinical_Data_Summary__c(
                    Patient__c = Patient.Id,
                      IsActive__c =false,
                    RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS
                )
                
            });
            
            }
    else if (RECTYPES_NMD.contains(Patient.RecordTypeId))
            {
            
             this.clinicalDataSummaries.addAll(new List<Clinical_Data_Summary__c> {
                new Clinical_Data_Summary__c(
            Patient__c = Patient.Id,
            IsActive__c =true,
            RecordTypeId = ClinicalDataSummaryManager.RECTYPE_PAINMAP 
                ),
                new Clinical_Data_Summary__c(
                    Patient__c = Patient.Id,
                    IsActive__c =false,
                    RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS
                )
                
            });
      }
        system.debug('Clinical Data Summaries to be added :'+clinicalDataSummaries);
        
        
        

    }
    
    
    
    public void commitClinicalDataSummaries() {

        if (!this.clinicalDataSummaries.isEmpty() && !this.clinicalDataSummariesCommitted) {

            try {
            system.debug('2__PatientListAll__'+this.PatientListAll);
                this.clinicalDataSummariesCommitted = true;
                DML.save(this, this.clinicalDataSummaries);
               
                //insert this.clinicalDataSummaries;
            }
            catch (System.DmlException de) {
                System.debug('commitClinicalDataSummaries:'+de);
            }

        }

    }   
    
    
    
  

}
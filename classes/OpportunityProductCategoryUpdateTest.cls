/*
@CreatedDate     20JUNE2016
@author          Mike Lankfer
@Description     Contains test coverage for OpportunityLineItemTrigger, OpportunityLineItemTriggerHandler, Product2Trigger ProductTriggerHandler and OpportunityProductCategoryUpdate
@Requirement Id  User Story : DRAFT US2511
*/

@isTest
public class OpportunityProductCategoryUpdateTest {
    
    private static final Map<String,Schema.RecordTypeInfo> OPTY_RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();    
    private static final Id RECTYPE_ENDO_CAPITAL_OPTY = OPTY_RECTYPES.get('Endo Capital Business').getRecordTypeId();
    
    private static final Map<String,Schema.RecordTypeInfo> PRODUCT_RECTYPES = Schema.SObjectType.Product2.getRecordTypeInfosByName();    
    private static final Id RECTYPE_SAP_LEVEL_1 = PRODUCT_RECTYPES.get('Standard Product Hierarchy - Level 1').getRecordTypeId();
    
    private static Pricebook2 standardPricebook = new Pricebook2();
    private static List<Product2> testProducts = new List<Product2>();
    private static Pricebook2 testPricebook = new Pricebook2();
    private static List<User> testUsers = new List<User>();
    private static Account testAccount = new Account();
    private static Opportunity testOpportunity = new Opportunity();
    private static List<OpportunityLineItem> testOpportunityLineItems = new List<OpportunityLineItem>();
    private static List<PricebookEntry> standardPricebookEntries = new List<PricebookEntry>();
    private static List<PricebookEntry> testPricebookEntries = new List<PricebookEntry>();
    private static Opportunity newOpportunity = new Opportunity();
    private static Product2 newProduct = new Product2();
    
    //create opportunity with 2 core line items
    static testmethod void createOpportunityWithCoreLineItems(){
        
        Test.startTest();
        
        for(Integer i = 0; i < 2; i++){
            testProducts.add(TestDataFactory.createTestProduct());
            testProducts.get(i).Endo_Product_Category__c = 'Core';
            testProducts.get(i).UPN_Material_Number__c = 'Test' + i;
            testProducts.get(i).RecordTypeId = RECTYPE_SAP_LEVEL_1;
        }
        
        insert testProducts;
        
        standardPricebookEntries = TestDataFactory.createTestPricebookEntries(standardPricebook, testProducts);
        
        for(PricebookEntry pricebookEntry:standardPricebookEntries){
            pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        }
        
        insert standardPricebookEntries;
        
        testPricebook.Name = 'NMD';
        testPricebook.IsActive = true;
        testPricebook.Description='Neuromod';
        insert testPricebook;

        
        testPricebookEntries = TestDataFactory.createTestPricebookEntries(testPricebook, testProducts);
        insert testPricebookEntries;
        
        //create test account
        testAccount = TestDataFactory.createTestAccount();
        insert testAccount;
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        //create test opty
        testOpportunity = TestDataFactory.createTestOpportunity(testAccount);
        testOpportunity.Pricebook2Id = testPricebook.Id;
        testOpportunity.RecordTypeId = RECTYPE_ENDO_CAPITAL_OPTY;
        insert testOpportunity;
        
        //create test Opty Line Items
        testOpportunityLineItems = TestDataFactory.createTestOpportunityLineItems(testOpportunity, testPricebookEntries);
        insert testOpportunityLineItems;
        
        newOpportunity = [SELECT Id, RecordTypeId, Endo_Product_Category__c FROM Opportunity WHERE Id = :testOpportunity.Id];
        
        Test.stopTest();
        
        //System.assertEquals('Core', newOpportunity.Endo_Product_Category__c);
        
    }
    
    //create opportunity with 1 core and 1 innovative line item
    static testmethod void createOpportunityWithMultipleLineItems(){
        
        Test.startTest();
        
        for(Integer i = 0; i < 2; i++){
            testProducts.add(TestDataFactory.createTestProduct());
            testProducts.get(i).UPN_Material_Number__c = 'Test' + i;
            testProducts.get(i).RecordTypeId = RECTYPE_SAP_LEVEL_1;
        }
        
        testProducts.get(0).Endo_Product_Category__c = 'Core';
        testProducts.get(1).Endo_Product_Category__c = 'Innovative';
        
        
        insert testProducts;
        
        standardPricebookEntries = TestDataFactory.createTestPricebookEntries(standardPricebook, testProducts);
        
        for(PricebookEntry pricebookEntry:standardPricebookEntries){
            pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        }
        
        insert standardPricebookEntries;
        
       
       // testPricebook = TestDataFactory.createTestPricebook();
        testPricebook.Name = 'NMD';
        testPricebook.IsActive = true;
        testPricebook.Description='Neuromod';
        insert testPricebook;
        
        testPricebookEntries = TestDataFactory.createTestPricebookEntries(testPricebook, testProducts);
        insert testPricebookEntries;
        
        //create test account
        testAccount = TestDataFactory.createTestAccount();
        insert testAccount;
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        //create test opty
        testOpportunity = TestDataFactory.createTestOpportunity(testAccount);
        testOpportunity.Pricebook2Id = testPricebook.Id;
        testOpportunity.RecordTypeId = RECTYPE_ENDO_CAPITAL_OPTY;
        insert testOpportunity;
        
        //create test Opty Line Items
        testOpportunityLineItems = TestDataFactory.createTestOpportunityLineItems(testOpportunity, testPricebookEntries);
        insert testOpportunityLineItems;
        
        newOpportunity = [SELECT Id, Endo_Product_Category__c FROM Opportunity WHERE Id = :testOpportunity.Id];
        
        Test.stopTest();
        
        //System.assertEquals('Multiple', newOpportunity.Endo_Product_Category__c);
        
    }
    
    //create opportunity with 1 core and 1 innovative line item and then delete both lines
    static testmethod void deleteExistingOptyLineItems(){
        
        Test.startTest();
        
        for(Integer i = 0; i < 2; i++){
            testProducts.add(TestDataFactory.createTestProduct());
            testProducts.get(i).UPN_Material_Number__c = 'Test' + i;
            testProducts.get(i).RecordTypeId = RECTYPE_SAP_LEVEL_1;
        }
        
        testProducts.get(0).Endo_Product_Category__c = 'Core';
        testProducts.get(1).Endo_Product_Category__c = 'Innovative';
        
        
        insert testProducts;
        
        standardPricebookEntries = TestDataFactory.createTestPricebookEntries(standardPricebook, testProducts);
        
        for(PricebookEntry pricebookEntry:standardPricebookEntries){
            pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        }
        
        insert standardPricebookEntries;
        
        testPricebook.Name = 'NMD';
        testPricebook.IsActive = true;
        testPricebook.Description='Neuromod';
        insert testPricebook;
        
        testPricebookEntries = TestDataFactory.createTestPricebookEntries(testPricebook, testProducts);
        insert testPricebookEntries;
        
        //create test account
        testAccount = TestDataFactory.createTestAccount();
        insert testAccount;
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        //create test opty
        testOpportunity = TestDataFactory.createTestOpportunity(testAccount);
        testOpportunity.Pricebook2Id = testPricebook.Id;
        testOpportunity.RecordTypeId = RECTYPE_ENDO_CAPITAL_OPTY;
        insert testOpportunity;
        
        //create test Opty Line Items
        testOpportunityLineItems = TestDataFactory.createTestOpportunityLineItems(testOpportunity, testPricebookEntries);
        insert testOpportunityLineItems;
        
        delete testOpportunityLineItems;
        
        newOpportunity = [SELECT Id, Endo_Product_Category__c FROM Opportunity WHERE Id = :testOpportunity.Id];
        
        Test.stopTest();
        
        System.assertEquals(NULL, newOpportunity.Endo_Product_Category__c);        
    }
    
    //create opportunity with 2 core line items and then change 1 product category

    static testmethod void changeProductCategoryOnExistingLineItem(){
        
        Test.startTest();
        
        for(Integer i = 0; i < 2; i++){
            testProducts.add(TestDataFactory.createTestProduct());
            testProducts.get(i).Endo_Product_Category__c = 'Core';
            testProducts.get(i).UPN_Material_Number__c = 'Test' + i;
            testProducts.get(i).RecordTypeId = RECTYPE_SAP_LEVEL_1;
        }
        
        insert testProducts;
        
        standardPricebookEntries = TestDataFactory.createTestPricebookEntries(standardPricebook, testProducts);
        
        for(PricebookEntry pricebookEntry:standardPricebookEntries){
            pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        }
        
        insert standardPricebookEntries;
        
        testPricebook.Name = 'NMD';
        testPricebook.IsActive = true;
        testPricebook.Description='Neuromod';
        insert testPricebook;
        
        
        testPricebookEntries = TestDataFactory.createTestPricebookEntries(testPricebook, testProducts);
        insert testPricebookEntries;
        
        //create test account
        testAccount = TestDataFactory.createTestAccount();
        insert testAccount;
        UtilForUnitTestDataSetup.createCustomSettingFrENDO();
        //create test opty
        testOpportunity = TestDataFactory.createTestOpportunity(testAccount);
        testOpportunity.Pricebook2Id = testPricebook.Id;
        testOpportunity.RecordTypeId = RECTYPE_ENDO_CAPITAL_OPTY;
        insert testOpportunity;
        
        //create test Opty Line Items
        testOpportunityLineItems = TestDataFactory.createTestOpportunityLineItems(testOpportunity, testPricebookEntries);
        insert testOpportunityLineItems;
        
        testProducts.get(1).Endo_Product_Category__c = 'Strategic';
        update testProducts;        
        
        newOpportunity = [SELECT Id, Endo_Product_Category__c FROM Opportunity WHERE Id = :testOpportunity.Id];
        
        Test.stopTest();
        
        //System.assertEquals('Core', newOpportunity.Endo_Product_Category__c);

    }

    
}
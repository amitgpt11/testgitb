@isTest
private class EventsController_Test {

	private static testMethod void test() {
	    
	    //Create Guest User
        User objUser = Test_DataCreator.createGuestUser();
        
        Map<Integer, String> mapIntToMonth = new Map<Integer, String>{1 => 'January',
            2 => 'Feburary',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'};
            
        Test.startTest();
        
        EventsController objEventsController = new EventsController(); 
        system.Test.setCurrentPage(Page.Events);
        
        system.runAs(objUser){
            
            system.assertNotEquals(null, objEventsController.init());
        }
        
        //create Boston scientific cofig test data using Test_DataCreator class
        Date dt = Date.Today().adddays(2);
        Community_Event__c objEvent = Test_DataCreator.createCommunityEvent(dt);

        Test_DataCreator.cerateBostonCSConfigTestData(objEvent.Id);
        ApexPages.currentPage().getParameters().put('feId',objEvent.Id);
        
        objEventsController = new EventsController();
        objEventsController.init();
        objEventsController.selectedMonth = mapIntToMonth.get(Date.Today().month());
        objEventsController.objFeaturedEvent.Shared_Country__c = 'Germany';
        objEventsController.applyFilters();
        objEventsController.clearFilters();
        system.assert(String.isBlank(objEventsController.selectedMonth));
        
        Test.stopTest();
	}
}
/**
* Test class, for the one-time batch to convert existing Attachments into FeedItems
* This test uses the ConnectApi, which is not supported in data siloed tests.
*
* @author   Salesforce services
* @date     2015-02-06
*/
@IsTest(SeeAllData=true)
private class NMD_Attachment2FeedItemTest {

    static testMethod void test_Attachment2FeedItemBatch() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        Patient__c patient = td.newPatient();
        insert patient;

        Account acct = td.createConsignmentAccount();
        insert acct;

        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        oppty.Patient__c = patient.Id;
        insert oppty;

        /*// automatically created in the opportunity trigger
        Clinical_Data_Summary__c cds = [
            select Id 
            from Clinical_Data_Summary__c 
            where Opportunity__c = :oppty.Id
            and RecordTypeId = :ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS
        ];*/

        Clinical_Data_Summary__c cds = new Clinical_Data_Summary__c();
        cds.Opportunity__c = oppty.id;
        cds.RecordTypeId = ClinicalDataSummaryManager.RECTYPE_ANNOTATIONS;
        insert cds;
       
        PatientImageClassifications__c imageClassification = new PatientImageClassifications__c(
            Name = 'Image Classification',
            Order__c = 0,
            Is_Active__c = true
        );
        insert imageClassification;

        Attachment attach1 = td.newAttachment(cds.Id, 'image.png', imageClassification.Name, Blob.valueOf('foo'));
        Attachment attach2 = td.newAttachment(patient.Id, 'image.jpg', imageClassification.Name, Blob.valueOf('foo'));
        insert new List<Attachment> { attach1, attach2 };

        Test.startTest(); 
        {

            NMD_Attachment2FeedItemBatch.runNow();

        } 
        Test.stopTest();

    }

}
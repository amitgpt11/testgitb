/**
* Class for implementing TriggerHandler methods for User Assignment triggers.       
*
* @Author salesforce Services
* @Date 21/09/2016
*/

public with sharing class UserAssignmentTriggerHandler extends TriggerHandler {

   final UATriggerHandlerManager manager = new UATriggerHandlerManager();  
   
/*
@CreatedDate     22SEP016
@author          Payal - Accenture
@Description     Executes bulk actions before UserAssignment DML
@Requirement Id  SFDC-1035
*/     
    public override void bulkBefore(){
    List<User_Assignment__c> UserAssignmentListInitial = (List<User_Assignment__c>) Trigger.new;        
        if (Trigger.isInsert || Trigger.isUpdate){
            List<User_Assignment__c> UserAssignmentList = (List<User_Assignment__c>) Trigger.new;
        }
        //manager.accessRecordstoUser(UserAssignmentListInitial); 
    } 
        
/*
@CreatedDate     22SEP2016
@author          Payal
@Description     Executes bulk actions after UserAssignment DML
@Requirement Id  SFDC- 1035
*/            
    public override void bulkAfter() {
    List<User_Assignment__c> UserAssignmentListAfter= (List<User_Assignment__c>) Trigger.new;
    List<User_Assignment__c> UserAssignmentListAfterold= (List<User_Assignment__c>) Trigger.old;
        if (Trigger.isInsert){        
            try{
                manager.accessRecordstoUser(UserAssignmentListAfter);
                //manager.fetchShareRecordToUpdate(UserAssignmentListAfter);
            }
            catch (Exception e){
                System.debug('The following exception has been caught by the UserAssignmentTriggerHandler.BulkAfter method: ' + e);
            }
        }
        if (Trigger.isDelete || Trigger.isUpdate){
            manager.fetchShareRecordToDelete(UserAssignmentListAfterold);
        }
    }
    
    public override void beforeInsert(SObject obj) {
        User_Assignment__c uAssignNew = (User_Assignment__c)obj;
    }
    
    public override void beforeUpdate(SObject oldObj, SObject obj) {        
        User_Assignment__c uAssignOld = (User_Assignment__c)oldObj;
        User_Assignment__c uAssignNew = (User_Assignment__c)obj; 
    }
    
    public override void beforeDelete(SObject obj) {
        User_Assignment__c uAssignNewDel = (User_Assignment__c)obj;
    }
     
    public override void afterInsert(SObject obj) {       
        User_Assignment__c uAssignNew = (User_Assignment__c)obj;
        manager.insertShareTableRecords(uAssignNew);
    }
    
    public override void afterUpdate(SObject oldObj, SObject obj) {
        User_Assignment__c uAssignOld = (User_Assignment__c)oldObj;
        User_Assignment__c uAssignNew = (User_Assignment__c)obj;
        manager.updateShareTableRecords(uAssignOld, uAssignNew);
    }
        
    public override void afterDelete(SObject obj) {
        User_Assignment__c uAssignNewoldDel = (User_Assignment__c)obj;
        manager.deleteShareTableRecords(uAssignNewoldDel);
    }
                               
    public override void andFinally() { 
        manager.commitShareRecords();
        manager.deleteShareRecords();
        manager.updateShareRecords();
    }
           
}
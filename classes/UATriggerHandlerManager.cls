/**
* Manager class for the User Assignment object in below following scenarios:
    Insert: When User Assignment record created then it will share all the procedure of that selected account with the selected user in record.
    Delete: When User Assignment record deleted it will only delete the relation record of procedure share with that user and account and procedure.
    Update: When only user change from User Assignment record it delete the procedure share record of previous user and add the share record for new user.          
* @Author : Payal Ahuja (Accenture Team)
* @Date 22/09/2016
**/

public with Sharing class UATriggerHandlerManager {

    private Set<id> UserIdSet = new Set<Id>();
    private Set<Id> AccountIdSet = new Set<Id>();
    private List<User> userList = new List<User>();
    private List<Shared_Patient_Case__c> procedureList = new List<Shared_Patient_Case__c>();
    private List<Shared_Patient_Case__Share> shareNewList = new List<Shared_Patient_Case__Share>();
       
    private List<Shared_Patient_Case__Share> shareListToDelete = new List<Shared_Patient_Case__Share>();
    private List<Shared_Patient_Case__Share> deleteShareList = new List<Shared_Patient_Case__Share>();
    
    /****** Insert Functionality ****Start *****/
    // Method For Insert Sharing Record to Procedure 
    Public void accessRecordstoUser(List<User_Assignment__c> userAssignmentList){  
        for(User_Assignment__c Uassng: userAssignmentList) {  
           AccountIdSet.add(Uassng.Account__c);
           UserIdSet.add(Uassng.User__c);
        } 
        system.debug('****AccountIdSet****'+AccountIdSet);
        system.debug('****UserIdSet****'+UserIdSet);
        
        procedureList = [SELECT Id, Shared_Facility__c FROM Shared_Patient_Case__c WHERE Shared_Facility__c IN : AccountIdSet];
        system.debug('****procedureList****'+procedureList);
        userList = [Select id, name FROM User Where Id IN : UserIdSet];
        system.debug('****userList****'+userList);
        
        shareListToDelete = [SELECT AccessLevel,Id,ParentId,RowCause,UserOrGroupId FROM Shared_Patient_Case__Share WHERE ParentId IN : procedureList];
        system.debug('****shareListToDelete**after**'+shareListToDelete);
    }
    
    Public void insertShareTableRecords(User_Assignment__c User) {
    system.debug('****Inside**procedureList****'+procedureList);
    system.debug('****Inside**userList****'+userList);
        for(Shared_Patient_Case__c pc: procedureList){
            for(User u: userList){
                Shared_Patient_Case__Share share = new Shared_Patient_Case__Share();
                share.UserOrGroupId = u.Id;
                share.RowCause = 'Manual';
                share.AccessLevel = 'Read';
                share.ParentId = pc.Id;
                shareNewList.add(share);
            } 
        }   
    }
    
    Public void commitShareRecords(){
        if(!shareNewList.isEmpty()){
            Database.Insert(shareNewList , false);
        }
    }
    /****** Insert Functionality ****End *****/
    
    /****** Delete Functionality ****Start *****/
    //Method For Delete Sharing Record from Procedure
    Public void fetchShareRecordToDelete(List<User_Assignment__c> userAssignmentListtoDelete){
        for(User_Assignment__c Uassng1: userAssignmentListtoDelete) {  
           AccountIdSet.add(Uassng1.Account__c);
           UserIdSet.add(Uassng1.User__c);
        } 
        system.debug('****AccountIdSet****'+AccountIdSet);
        system.debug('****UserIdSet****'+UserIdSet);
        
        procedureList = [SELECT Id, Shared_Facility__c FROM Shared_Patient_Case__c WHERE Shared_Facility__c IN : AccountIdSet];
        system.debug('****procedureList****'+procedureList);
        userList = [Select id, name FROM User Where Id IN : UserIdSet];
        system.debug('****userList****'+userList);
        
        shareListToDelete = [SELECT AccessLevel,Id,ParentId,RowCause,UserOrGroupId FROM Shared_Patient_Case__Share WHERE ParentId IN : procedureList AND UserOrGroupId IN :userList];
        system.debug('****shareListToDelete**after**'+shareListToDelete);
        
    }
    
    Public void deleteShareTableRecords(User_Assignment__c newUser) {
    system.debug('****shareListToDelete**Inside Delete***'+shareListToDelete);
        for(Shared_Patient_Case__Share pc: shareListToDelete){
            for(User u: userList){
                if(pc.UserOrGroupId == u.Id){
                    deleteShareList.add(pc);
                }
            } 
        }  
    }
    
    Public void deleteShareRecords(){
        if(!deleteShareList.isEmpty()){
            Database.Delete(deleteShareList , false);
        }
    }
    /****** Delete Functionality ****End *****/
    
    /****** Update Functionality ****Start *****/
    //Method For Update Sharing Record from Procedure
    Public void updateShareTableRecords(User_Assignment__c oldUser, User_Assignment__c newUser) {    
        if((oldUser.User__c != newUser.User__c) && newUser.User__c != null){
            for(Shared_Patient_Case__Share pc: shareListToDelete){
                if(pc.UserOrGroupId != newUser.User__c){
                    deleteShareList.add(pc);
                }
            }  
        
            for(Shared_Patient_Case__c pc1: procedureList){
                Shared_Patient_Case__Share share = new Shared_Patient_Case__Share();
                share.UserOrGroupId = newUser.User__c;
                share.RowCause = 'Manual';
                share.AccessLevel = 'Read';
                share.ParentId = pc1.Id;
                shareNewList.add(share);
            }
        }     
    }
    
    Public void updateShareRecords(){
        if(!deleteShareList.isEmpty()){
            Database.Delete(deleteShareList , false);
        }
        if(!shareNewList.isEmpty()){
            Database.Insert(shareNewList, false);
        }
    }
    /****** Update Functionality ****End *****/
}
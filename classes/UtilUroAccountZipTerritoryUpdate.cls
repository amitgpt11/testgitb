/*
 @CreatedDate     30JUN2016                                  
 @ModifiedDate    30JUN2016                                    
 @author          Mayuri-Accenture
 @Description     Utility class used by batch classe BatchAcctTerritoryUpdateOnZip.
 @Methods         UroAccountTerritoryUpdate,getAcctnTeritryAssctnMap,createObjectTerrAscRecord,getAccountMap 
 @Requirement Id  Q3 2016 - US2701 - Update account territory association based on zip codes
 */
public without sharing class UtilUroAccountZipTerritoryUpdate{

    /*
    @MethodName    UroaccountTerritoryUpdate
    @Parameters    removeList: List of Zip_To_Territory__c records which will help to remove existing account territory association.
                   AddList: List of Zip_To_Territory__c records which will help to add new account territory association.
    @Return Type   boolean
    */
    public boolean UroAccountTerritoryUpdate(set<Zip_To_Territory__c> removeList,set<Zip_To_Territory__c> addList,boolean isRemove)
    {
        boolean isSuccess;
        set<Zip_To_Territory__c> remAcctTerritoryLst = new set<Zip_To_Territory__c>();
        set<Zip_To_Territory__c> nonRemAcctTerritoryLst = new set<Zip_To_Territory__c>();
        map<string,set<string>> zipTerritoryMap = new map<string,set<string>>();
        UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
        list<ObjectTerritory2Association> assignAcctToTerritory = new list<ObjectTerritory2Association>();         
        set<string> zipcodes = new set<string>();
        set<string> territoryNameSet = new set<string>();
        map<Id, Account> accountMap = new map<Id, Account>();
        map<Id,ObjectTerritory2Association> acctTerrMap = new map<Id,ObjectTerritory2Association>();
        map<Id,ObjectTerritory2Association> onjTerrMap = new map<Id,ObjectTerritory2Association>();
        map<Id,Territory2> territory2Map = new map<Id,Territory2>();
        map<string,Id> territoryNameIdMap = new map<string,Id>();
        map<string,set<string>> acctTerrDupeMap = new map<string,set<string>>();
        
         //remove existing account territory association records
         if(isRemove == true){         
             if(removeList.size() > 0){
                 for(Zip_To_Territory__c zp : removeList){
                     if(zp.UroPH_Zip_Codes__c != null){                     
                         zipcodes.addAll(zp.UroPH_Zip_Codes__c.SPLIT(';')); //set of zipcodes
                     }
                     if(zp.UroPH_Territory_Name__c != null){
                         territoryNameSet.add(zp.UroPH_Territory_Name__c);  //set of territory names
                     }
                 }
             }
             if(zipcodes.size() > 0){
                 accountMap = getAccountMap(zipcodes);      //get map of accounts from zipcodes collected above
             }
             if(accountMap != null && accountMap.size() >0){
                 acctTerrMap = getAcctnTeritryAssctnMap(accountMap.keyset(),territoryNameSet);        //get map of ObjectTerritory2Association records to be deleted based on accounts and territory names
                 system.debug(acctTerrMap+'@789'); 
                 if(acctTerrMap != null && acctTerrMap.size() > 0){
                     list<Id> tempT2ObjIdLst = new list<Id>();
                     tempT2ObjIdLst.addAll(acctTerrMap.keyset());
                     if(tempT2ObjIdLst.size() > 0){                     
                         isSuccess = util.DeleteShareRecords(tempT2ObjIdLst);                    //call Delete method from util class to delete associations
                     }
                 }                                                                                          
             }
         
         }
         //Create new account territory association records
         else{
            //System.debug('addList:'+addList);
            if(addList != null && addList.size() > 0){
               for(Zip_To_Territory__c zp : addList){
                   if(zp.UroPH_Zip_Codes__c != null){                     
                      zipcodes.addAll(zp.UroPH_Zip_Codes__c.SPLIT(';')); //set of zipcodes
                     //System.debug('Zipcodes:'+zipcodes);
                     territoryNameSet.add(zp.UroPH_Territory_Name__c);  //set of territory names
                     // System.debug('territoryNameSet:'+territoryNameSet);
                     Set<String> zipCodeSet;
                     if(zp.UroPH_Zip_Codes__c.contains(';')){
                       zipCodeSet = new Set<String>(zp.UroPH_Zip_Codes__c.SPLIT(';')); 
                     }else{
                         zipCodeSet = new Set<String>();
                         zipCodeSet.add(zp.UroPH_Zip_Codes__c); 
                     }
                     //System.debug('zipCodeSet:'+zipCodeSet);
                     if(zipCodeSet!=null && zipCodeSet.size()>0){
                         for(String zipStr : zipCodeSet){
                        // System.debug('zipStr :'+zipStr );
                        // System.debug('zipStr_zp.UroPH_Territory_Name__c :'+zp.UroPH_Territory_Name__c );
                             if(zipTerritoryMap.containsKey(zipStr)){
                                 zipTerritoryMap.get(zipStr ).add(zp.UroPH_Territory_Name__c);}
                             else{
                                  zipTerritoryMap.put(zipStr ,new set<string>{zp.UroPH_Territory_Name__c});        //map of zip codes and its corrsponding set of territory names
                             }
                         }//For loop end
                     }
                     //System.debug('zipTerritoryMap:'+zipTerritoryMap);
                  }   
               }//For loop end
            }
             /*if(addList != null && addList.size() > 0){
                 for(Zip_To_Territory__c zp : addList){
                 
                     if(zp.UroPH_Zip_Codes__c != null)
                     {                     
                 
                         zipcodes.addAll(zp.UroPH_Zip_Codes__c.SPLIT(';'));     //set of zipcodes
                 
                     }
                     System.debug('Zipcodes:'+zipcodes);
                     if(zp.UroPH_Territory_Name__c != null)
                     {
                         territoryNameSet.add(zp.UroPH_Territory_Name__c);        //set of territory names
                     }
                     System.debug('territoryNameSet:'+territoryNameSet);
                     if(zipcodes.size() > 0)
                     {
                         for(string s : zipcodes)
                         {
                             if(zipTerritoryMap.containsKey(s))
                             {
                                 zipTerritoryMap.get(s).add(zp.UroPH_Territory_Name__c); 
                             }
                             else
                             {
                                  zipTerritoryMap.put(s,new set<string>{zp.UroPH_Territory_Name__c});        //map of zip codes and its corrsponding set of territory names
                             }
                         }
                     } 
                     System.debug('zipTerritoryMap:'+zipTerritoryMap) ;               
                 }
             }*/
             system.debug(zipcodes+'@123');
             if(zipcodes.size() > 0){
                 accountMap = getAccountMap(zipcodes);      //get map of accounts from zipcodes collected above
             }
             system.debug(accountMap+'@321');
             if(territoryNameSet.size() > 0){
                 territory2Map  = util.getTerritoryDetailList(territoryNameSet);        //call mathod from util class to get territory Ids and its details based on territory names
                 if(territory2Map.size() > 0){
                     for(Id t2 : territory2Map.keyset()){
                         territoryNameIdMap.put(territory2Map.get(t2).Name,t2);      //create a map of territory name and its Id           
                     }
                 }
             }
             if(accountMap.size() > 0){                 
                 onjTerrMap = getAcctnTeritryAssctnMap(accountMap.keyset(),territoryNameSet);        
                 if(onjTerrMap != null && onjTerrMap.size() > 0){
                     for(Id act : onjTerrMap.keyset()){
                         if(acctTerrDupeMap.containsKey(onjTerrMap.get(act).ObjectId)){
                             acctTerrDupeMap.get(onjTerrMap.get(act).ObjectId).add(onjTerrMap.get(act).territory2Id);
                         }else{
                             acctTerrDupeMap.put(onjTerrMap.get(act).ObjectId,new set<string>{onjTerrMap.get(act).territory2Id});    //create account Id and its list of territory Id to check for duplicates
                         }
                     }
                 
                 }
                 for(Id a : accountMap.keyset()){
                 System.debug('zipTerritoryMap:'+zipTerritoryMap);
                     if(zipTerritoryMap.containsKey(accountMap.get(a).ShippingPostalCode)){
                         if(zipTerritoryMap.get(accountMap.get(a).ShippingPostalCode).size() > 0){
                             for(string s : zipTerritoryMap.get(accountMap.get(a).ShippingPostalCode)){
                                ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
                                accountAssociation.ObjectId = a;  
                                System.debug('territoryNameIdMap:'+territoryNameIdMap);                              
                                if(territoryNameIdMap.containskey(s)){
                                    if(acctTerrDupeMap.containskey(a) == false){
                                        accountAssociation.Territory2Id = territoryNameIdMap.get(s);
                                        accountAssociation.AssociationCause = 'Territory2Manual';
                                        System.debug('accountAssociation:'+accountAssociation);
                                        assignAcctToTerritory.add(accountAssociation);
                                    }
                                    else{
                                        if(acctTerrDupeMap.get(a).contains(territoryNameIdMap.get(s)) == false){
                                            accountAssociation.Territory2Id = territoryNameIdMap.get(s);
                                            accountAssociation.AssociationCause = 'Territory2Manual';
                                            assignAcctToTerritory.add(accountAssociation);
                                        }
                                    }
                                }                                
                             }
                         }
                     }                 
                 }         
             }
             system.debug('assignAcctToTerritory --'+assignAcctToTerritory +'--');
            if(assignAcctToTerritory != NULL && assignAcctToTerritory.size()>0){
                 isSuccess =  createObjectTerrAscRecord(assignAcctToTerritory);                
             } 
             
         
         }
        return isSuccess;  
    }
    /*
    @MethodName    getAccountMap
    @Parameters    zipcodes: set of zipcodes
    @Return Type   map of account Id and account
    */
    public map<Id, Account> getAccountMap(set<string> zipcodes){
        map<Id,Account> accountMap;
        if(zipcodes.size() > 0){
             accountMap = new map<Id,Account>([SELECT Id,name,ShippingPostalCode,UroPH_Account__c
                                                           FROM Account
                                                           WHERE ShippingPostalCode IN : zipcodes AND UroPH_Account__c = true]);
         }
        return accountMap;
    }
    
    /*
    @MethodName    getAcctnTeritryAssctnMap                                  
    @Parameters    acctIdSet : Set of Account ids
                   territoryNameSet : Set of Territory names
    @Return Type   map of ObjectTerritory2Association records.
    @Description   Method to query list of ObjectTerritory2Association records based on the Id set provided.
    */
    public map<Id,ObjectTerritory2Association> getAcctnTeritryAssctnMap(set<Id> acctIdSet,set<string> territoryNameSet){
        map<Id,ObjectTerritory2Association> objAssociationMap;
        if(acctIdSet != null && territoryNameSet != null){
            objAssociationMap = new map<Id,ObjectTerritory2Association>([SELECT Id,ObjectId,Territory2.name
                                                                         FROM ObjectTerritory2Association
                                                                         WHERE ObjectId IN : acctIdSet AND Territory2.name IN : territoryNameSet]);
        }
        return objAssociationMap;
    
    
    }
    /*
    @MethodName    createObjectTerrAscRecord
    @Parameters    recordList : list of ObjectTerritory2Association records to be created
    @Return Type   boolean
    @Description   Method to create ObjectTerritory2Association records based on the list provided.
    */
    public boolean createObjectTerrAscRecord(list<ObjectTerritory2Association> recordList){
        boolean isCreated = false;
        integer successCount = 0;
        System.debug('recordList:'+recordList);
            if(recordList != NULL && recordList.size()>0){
                    Database.SaveResult[] AccountInsertResult;
                    try{
                     AccountInsertResult = Database.insert(recordList,TRUE);
                     System.debug('AccountInsertResult :'+AccountInsertResult );
                    }catch(exception e){
                        system.debug(e+'ee');
                    }
                            
                    // Error handling code Iterate through each returned result
                    for (Database.SaveResult er : AccountInsertResult) {
                        if (er.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully inserted Share Record. Record ID: ' + er.getId());
                            successCount++;
                        }
                        else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : er.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Record fields that affected this error: ' + err.getFields());
                            }
                        }
                    }                   
             } 
             else{
                 isCreated = false;
             }
             if(successCount == recordList.size()){
                 isCreated = true;
             } 
             else{
                isCreated = false;
             } 
        return  isCreated;      
    }
    
    
}
/**
* Batch class, Change Territory on Account based on Zip_To_Territory__c values
* @author   Mayuri - Accenture
* @date     30JUN2016
*/
global class BatchAcctTerritoryUpdateOnZipSchedule implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
    integer batchSize = Integer.valueOf(system.Label.AcctTerritoryUpdateOnZipBatchSize);
    
    try{
        BatchAcctTerritoryUpdateOnZip b = new BatchAcctTerritoryUpdateOnZip();
        database.executebatch(b,batchSize);
    }catch(Exception e){
        system.debug(e+'e');
    }

    }

}
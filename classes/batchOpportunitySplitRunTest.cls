/**
* Test Class for the batchOpportunitySplitRun.       
*
* @Author Accenture Services
* @Date 07/29/2016
*/
@isTest(SeeAllData=false)
@testVisible
public class batchOpportunitySplitRunTest {

    static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LNAME = 'LName';
    static final String LEAD_SOURCE = 'Care Card';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static Opportunity trialOpty = null;
    static Opportunity implantOpty = null;
    static DateTime dT = System.now();
    static Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
    
   // @testSetup
    static void setup() {
    
        NMD_TestDataManager td = new NMD_TestDataManager();
        
        
        Seller_Hierarchy__c seller =td.newSellerHierarchy();
        insert seller;
        
        Account acc = td.newAccount();
        insert acc;
        
        Contact cont = td.newContact(acc.id);
        cont.Territory_ID__c = seller.id;
        cont.RecordTypeId = ContactManager.RECTYPE_PROSPECT; 
        insert cont;
        
		Patient__c patients = new Patient__c(
        Physician_of_Record__c=cont.id,
        Territory_ID__c=seller.id);
        insert patients;
		
		List<Opportunity> olist = new List<Opportunity>();
		for(integer i=0;i<5;i++){
			Opportunity o =td.newOpportunity(acc.Id);
			o.CloseDate = System.today().addDays(7);
			o.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
			o.StageName = 'Scheduled Implant';
			o.Type = 'Standard';
			o.Opportunity_Type__c = 'Standard';
			o.Competitor_Name__c = 'Other';
			o.Product_System__c = 'Spectra';
			o.Patient__c =patients.id;
			o.Opportunity_Split__c = false;
			o.Scheduled_Trial_Date__c = date.newInstance(2016, 02, 06);
			o.Trial_Order_Number__c = '12345';
			o.Trial_Status__c = 'Successful';
			olist.add(o);
       }
        
		insert olist;
    
    }
    
    static testMethod void test_method_1() {
       setup();
   List<Opportunity> phys = [SELECT Id,Name,CloseDate,RecordTypeId,StageName,Type,Opportunity_Type__c,Opportunity_Split__c  FROM Opportunity where Opportunity_Split__c=false  and StageName not in('Complete (Closed Won)','Lost (Closed Lost)','Cancelled (Lost)') and recordtype.developerName  IN ('NMD_SCS_Trial_Implant')];
	Test.StartTest();
   
		ID batchprocessid = Database.executeBatch(new batchOpportunitySplitRun());
    Test.StopTest();   
    }
    
}
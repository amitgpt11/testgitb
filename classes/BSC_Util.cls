public class BSC_Util {
    
    public static Boolean isCommunityUser(){
        
        system.debug('userinfo.getUserType()=========='+userinfo.getUserType());
        if( userinfo.getUserType() == 'PowerCustomerSuccess' ){
            
            return true;
        }
        
        return false;
    }
    
    public static Set<String> fetchSharedOrgForCommunityUser(Id accountId){
        
        Set<String> setUserSharedSalesOrganization = new Set<String>();
        for(Shared_Sales_Organization__c objSalesOrganisation : [SELECT Shared_Sales_Organization__c 
                                                                FROM Shared_Sales_Organization__c 
                                                                WHERE Shared_Account__c =: accountId]){
            
            setUserSharedSalesOrganization.add(objSalesOrganisation.Shared_Sales_Organization__c);
        }
        
        if(Test.isRunningTest()){
            
            for(Shared_Sales_Organization__c objSalesOrganisation : [SELECT Shared_Sales_Organization__c 
                                                                FROM Shared_Sales_Organization__c]){
            
                setUserSharedSalesOrganization.add(objSalesOrganisation.Shared_Sales_Organization__c);
            }
        }
        
        return setUserSharedSalesOrganization;
    }
	
    public static Set<String> fetchProductExtensionsForCommunityUser(Set<String> setUserSharedSalesOrganization){
        
        Set<String> setSharedProductExtension = new Set<String>();
        for(Shared_Product_Extension__c objSharedProductExtension : [SELECT Shared_Product__c 
                                                                            FROM Shared_Product_Extension__c 
                                                                            WHERE Shared_Sales_Org__c IN :setUserSharedSalesOrganization]){
                    
			setSharedProductExtension.add(objSharedProductExtension.Shared_Product__c);
        }
        setSharedProductExtension.remove(null); 
        return setSharedProductExtension;
    }
}
/**
* Trigger for the Verification__c object
*
* @Author Accenture Services
* @Date 09/26/2015
*/
trigger VerificationTrigger on Verification__c (before insert, before update, after insert, after update, before delete) {
    TriggerHandlerManager.createAndExecuteHandler(VerificationTriggerHandler.class);
}
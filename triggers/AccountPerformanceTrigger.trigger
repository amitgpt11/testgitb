/*
-------10/29/2015 - Mike Lankfer - Created Account Performance Trigger, added method calls to AccountPerformanceTriggerMethods.UpdateParentAccount to after Insert, after Update, after delete, and after Undelete sections-------
*/

trigger AccountPerformanceTrigger on Account_Performance__c (before insert, before update, before delete, after insert,after update, after delete, after undelete) {
   
   Type t = AccountPerformanceTriggerMethods.class;
   String handlerName = t.getName(); 
   
   if (TriggerHandlerManager.isOkayToRun(handlerName)){ 
        //before insert
        if ((Trigger.isBefore) && (Trigger.isInsert)){       
        }
        
        //before update
        if ((Trigger.isBefore) && (Trigger.isUpdate)){       
        }
        
        
        //before delete
        if ((Trigger.isBefore) && (Trigger.isDelete)){       
        }
        
        
        //after insert
        if ((Trigger.isAfter) && (Trigger.isInsert)){
            AccountPerformanceTriggerMethods.updateParentAccount(Trigger.new);
        }
        
        
        //after update
        if ((Trigger.isAfter) && (Trigger.isUpdate)){
            AccountPerformanceTriggerMethods.updateParentAccount(Trigger.new);        
        }
        
        
        //after delete
        if ((Trigger.isAfter) && (Trigger.isDelete)){
            AccountPerformanceTriggerMethods.updateParentAccount(Trigger.old);        
        }
        
        
        //after undelete
        if ((Trigger.isAfter) && (Trigger.isUndelete)){
            AccountPerformanceTriggerMethods.updateParentAccount(Trigger.new);        
        }
    } 
}
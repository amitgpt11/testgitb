/**
* RevenueDetailsTrigger trigger
*
* @Author Vikas Malik
* @Date 2016-06-08
*/
trigger RevenueDetailsTrigger on Opportunity_Revenue_Schedule__c (after insert, after update, before insert, before update) {
	
	TriggerHandlerManager.createAndExecuteHandler(RevenueDetailsTriggerHandler.class);
    
}
/*
-------Created by Susannah St-Germain on 9/26/16 to handle before insert actions.-------
*/

trigger PotentialTrigger on Potential__c (before insert, before update, before delete, after insert,after update, after delete, after undelete) {
   
   PotentialTriggerHandler handler = new PotentialTriggerHandler();
   Type t = MyObjectivesTriggerHandler.class;
     String handlerName = t.getName(); 
   
     if (TriggerHandlerManager.isOkayToRun(handlerName)){ 
        //before insert
        if ((Trigger.isBefore) && (Trigger.isInsert)){
            handler.OnBeforeInsert(Trigger.new);
        }
        
        //before update
        if ((Trigger.isBefore) && (Trigger.isUpdate)){       
        }
        
        
        //before delete
        if ((Trigger.isBefore) && (Trigger.isDelete)){       
        }
        
        
        //after insert
        if ((Trigger.isAfter) && (Trigger.isInsert)){
        }
        
        
        //after update
        if ((Trigger.isAfter) && (Trigger.isUpdate)){       
        }
        
        
        //after delete
        if ((Trigger.isAfter) && (Trigger.isDelete)){      
        }
        
        
        //after undelete
        if ((Trigger.isAfter) && (Trigger.isUndelete)){       
        }
    }
 }
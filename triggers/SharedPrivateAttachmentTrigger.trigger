/*
* Trigger for the Private Attachment object
*
* @Author Accenture Services
* @Date 19SEPT2016
*/

trigger SharedPrivateAttachmentTrigger on Shared_Private_Attachments__c (before insert, before update, after insert, after update, before delete, after delete) {
TriggerHandlerManager.createAndExecuteHandler(PrivateAttachmentTriggerHandler.class);
}
trigger DeleteMeetings on Event (after delete) {
    
    set<id> eventIds = new set<id>();
    for(Event eventObj : Trigger.old){
        eventIds.add(eventObj.Id);
    }
    
    delete [select id from Meeting__c where EventId__c IN : eventIds];
}
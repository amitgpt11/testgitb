/*
@CreatedDate     29SEPT2016
@author          Dipil Jain
@Description     The trigger for the Cycle Count Response Analysis object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : SFDC-1110
*/

trigger CycleCountResponseAnalysisTrigger on Cycle_Count_Response_Analysis__c (before insert, before update, after insert, after update, before delete, after delete) {
    TriggerHandlerManager.createAndExecuteHandler(CycleCountResponseAnalysisTriggerHandler.class);
}
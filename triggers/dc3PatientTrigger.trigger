// dc3PatientTrigger: a trigger that allows the Duplicate Check app to function on the Patient object created by Susannah St-Germain (Boston Scientific)
trigger dc3PatientTrigger on Patient__c(after delete, after insert, after undelete, after update, before insert, before update) 

{ 
  public static final String Name = 'dc3PatientTrigger';
   
   if (TriggerHandlerManager.isOkayToRun(Name)){  
    dupcheck.dc3Trigger triggerTool = new dupcheck.dc3Trigger(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete, 'dc3Disable_Duplicate_Check__c');
    String errorString = triggerTool.processTrigger(trigger.oldMap, trigger.new); 

    if (String.isNotEmpty(errorString)) { trigger.new[0].addError(errorString,false); 
    }
   }  
}
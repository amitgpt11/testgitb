/**
 * Name : MyObjectivesTrigger
 * Description : MyObjectives Trigger
 * Date : 4/6/16
 
 * Modified : 21JUNE2016
 * Requirement : Q3 216 - US2528 -> Added new recrdtype(ANZ) on this object. Below methods are modified to include a new custom setting 'MyObjectivesRecordType__c'. 
 *               This custom setting will have the record types for which user does not want to run the territory related batches/changes.In this case, ANZ user does not
 *               want to run the territory batches for ANZ records. Hence the modification.
 */
trigger MyObjectivesTrigger on My_Objectives__c (before insert, before update,after insert,after update) 
{
     MyObjectivesTriggerHandler myObjHandler = new MyObjectivesTriggerHandler();
     list<My_Objectives__c> newMyObjLst = new list<My_Objectives__c>();
     list<My_Objectives__c> oldMyObjLst = new list<My_Objectives__c>();
     map<Id,My_Objectives__c> newMyObjMap = new map<Id,My_Objectives__c>();
     map<Id,My_Objectives__c> oldMyObjMap = new map<Id,My_Objectives__c>();
     public static final set<string> recordTypeNameSet = MyObjectivesRecordType__c.getall().keyset();
     static final Map<Id,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.My_Objectives__c.getRecordTypeInfosById();     
     
     Type t = MyObjectivesTriggerHandler.class;
     String handlerName = t.getName(); 
   
     if (TriggerHandlerManager.isOkayToRun(handlerName)){ 
        if(Trigger.isInsert)
         {
             for(My_Objectives__c mo : Trigger.new){
                 if(mo.RecordTypeId == null){
                     newMyObjLst.add(mo);
                 }
                 else{
                     if(recordTypeNameSet.contains(RECTYPES.get(mo.RecordTypeId).Name) == false){
                         newMyObjLst.add(mo);
                     }
                 }
             }
             if(newMyObjLst.size() > 0){
                 myObjHandler.onRecordInsert(newMyObjLst);
             }
            
         }//END of If
         if(Trigger.isUpdate)
         {
             for(My_Objectives__c mo : Trigger.new){
                 if(mo.RecordTypeId == null){
                     newMyObjLst.add(mo);
                     newMyObjMap.put(mo.Id,mo);
                 }
                 else{
                     if(recordTypeNameSet.contains(RECTYPES.get(mo.RecordTypeId).Name) == false){
                         system.debug('inside if loop-->1');
                         newMyObjLst.add(mo);
                         newMyObjMap.put(mo.Id,mo);
                     }
                 }
             }
             for(My_Objectives__c mo : Trigger.old){
                 if(mo.RecordTypeId == null){
                     oldMyObjLst.add(mo);
                     oldMyObjMap.put(mo.Id,mo);
                 }
                 else{
                     if(recordTypeNameSet.contains(RECTYPES.get(mo.RecordTypeId).Name) == false){
                         system.debug('inside if loop-->2');
                         oldMyObjLst.add(mo);
                         oldMyObjMap.put(mo.Id,mo);
                     }
                 }
             }
             if(newMyObjLst.size() > 0 && oldMyObjLst.size() > 0 && newMyObjMap.size() > 0 && oldMyObjMap.size() > 0){
                 myObjHandler.onRecordUpdate(oldMyObjLst,oldMyObjMap,newMyObjLst,newMyObjMap);     
             }
               
         }//END of If
     }
}
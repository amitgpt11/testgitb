/*
* Trigger for the Shared_Request_Form__c object
*
* @Author Accenture Services
* @Date 10OCT2016
*/

trigger SharedRequestFormTrigger on Shared_Request_Form__c (before insert, before update, after insert, after update, before delete, after delete) {
TriggerHandlerManager.createAndExecuteHandler(SharedRequestFormHandler.class);
}
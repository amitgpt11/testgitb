trigger InventoryItemTrigger on Inventory_Item__c (after update) {
	TriggerHandlerManager.createAndExecuteHandler(InventoryItemTriggerHandler.class);
}
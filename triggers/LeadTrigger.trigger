/**
* Trigger for the Lead object
*
* @Author salesforce Services
* @Date 03/23/2015
*/
trigger LeadTrigger on Lead (before insert, after insert, before update,after update) {

    TriggerHandlerManager.createAndExecuteHandler(LeadTriggerHandler.class);

}
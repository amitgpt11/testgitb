/*
* Trigger for the Contact object
*
* @Author salesforce Services
* @Date 04/16/2015
*/
trigger ContactTrigger on Contact (after update, after insert, before update, before Insert) { // Added before insert for SFDC-1317
    TriggerHandlerManager.createAndExecuteHandler(ContactTriggerHandler.class);
}
/**
* Order trigger
*
* @Author salesforce Services
* @Date 2015-06-08
*/
trigger OrderTrigger on Order (before insert, before update, before delete, after insert, after update) {

	TriggerHandlerManager.createAndExecuteHandler(OrderTriggerHandler.class);

}
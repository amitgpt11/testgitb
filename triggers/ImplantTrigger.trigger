/*
@CreatedDate     21JUNE2016
@author          Mike Lankfer
@Description     The trigger for the Implant custom object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : DRAFT US2484
@Modified Date   15SEP2016
@Modified By     Payal Ahuja
*/
trigger ImplantTrigger on LAAC_Implant__c (before insert, after insert, before update, after update) {
    
    TriggerHandlerManager.createAndExecuteHandler(ImplantTriggerHandler.class);

}
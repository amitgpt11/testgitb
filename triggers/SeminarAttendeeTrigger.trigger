/**
* Trigger for the Seminar Attendee object
*
* @Author salesforce Services
* @Date 05/10/2016
*/
trigger SeminarAttendeeTrigger on Shared_Seminar_Attendee__c(before insert,after insert) {
     
     if(Trigger.isbefore){
        
         SeminarAttendeeTriggerMethods.mapPatientToSeminarAttandee(trigger.new);
     }
     
     if(Trigger.isafter){
         SeminarAttendeeTriggerMethods.DeleteDummySeminar(trigger.new);
     }

}
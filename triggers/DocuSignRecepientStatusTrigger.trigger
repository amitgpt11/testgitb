/**
* DocuSignRecepientStatusTrigger trigger
*
* @Author Vikas Malik
* @Date 2016-07-05
*/
trigger DocuSignRecepientStatusTrigger on dsfs__DocuSign_Recipient_Status__c (after insert, after update) {
    
    //For Managed Package Objects the Handler Design Pattern is not working hence calling the Manager class directly from Trigger
    // Vikas: Adding the check to validate if this Trigger needs to be executed or not
    if (TriggerHandlerManager.isOkayToRun('DocuSignRecepientStatusTrigger')){
	    if (Trigger.isAfter){
	        DocuSignManager manager = DocuSignManager.getInstance();
	        for (dsfs__DocuSign_Recipient_Status__c newRec : Trigger.new){
	            if (Trigger.isInsert){
	                manager.processRecepientStatus(newRec,null);
	            }else{
	                manager.processRecepientStatus(newRec,(dsfs__DocuSign_Recipient_Status__c)Trigger.oldMap.get(newRec.Id));
	            }
	        }
	        manager.updateContacts();
	    }
    }
}
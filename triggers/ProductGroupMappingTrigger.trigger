/*
 @CreatedDate     30 June 2016                                  
 @author          Ashish-Accenture
 @Description     Trigger to update contract division based on the Product group mapping records
       
*/

trigger ProductGroupMappingTrigger on Shared_Product_Group_Mapping__c (after insert, after update, after delete, after undelete) {
    
   Type t = ProductGroupMappingTriggerMethods.class;
   String handlerName = t.getName(); 
   
   if (TriggerHandlerManager.isOkayToRun(handlerName)){ 
       ProductGroupMappingTriggerMethods.updateContractDivision();
   }    
}
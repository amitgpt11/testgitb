/**
* Trigger for the User Assignment object
*
* @Author salesforce Services
* @Date 21/09/2016
*/
trigger UserAssignmentTrigger on User_Assignment__c (before insert, after insert, before update, after update, before delete, after delete) {
    
     TriggerHandlerManager.createAndExecuteHandler(UserAssignmentTriggerHandler.class);

}
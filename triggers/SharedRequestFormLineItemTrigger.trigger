/*
* Trigger for the Shared_Request_Form_Line_Item__c object
*
* @Author Accenture Services
* @Date 10OCT2016
*/
trigger SharedRequestFormLineItemTrigger  on Shared_Request_Form_Line_Item__c (before insert, before update, after insert, after update, before delete, after delete) {
TriggerHandlerManager.createAndExecuteHandler(SharedRequestFormLineItemHandler .class);
}
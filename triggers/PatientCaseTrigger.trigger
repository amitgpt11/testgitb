/*
-------01/04/2016 - Naveen Gondha - Created Patient Case Trigger, added method calls to PatientCaseTriggerMethods.CreateAccountEvent to after Insert, after Update, after delete, and after Undelete sections-------

@ModifiedDate    2MAR2016
@author          Mayuri-Accenture
@Description     Trigger on custom object Shared_Patient_Case__c. This trigger calls the class 'PatientCaseTriggerMethods' for afterInsert, afterUpdate, beforeDelete 
                 and afterUndelete events.

@ModifiedDate    9MAR2016
@author          Mike Lankfer
@Description     Added After Insert call to the PatientCaseTriggerMethods.CreatePatientCaseClones method as part of US2240


*/

trigger PatientCaseTrigger on Shared_Patient_Case__c (before insert, before update, before delete, after insert,after update, after delete, after undelete) {
    
    
   Type t = PatientCaseTriggerMethods.class;
   String handlerName = t.getName(); 
   
   if (TriggerHandlerManager.isOkayToRun(handlerName)){ 
        //before insert
        if ((Trigger.isBefore) && (Trigger.isInsert)){     
            System.debug('in before insert');
            PatientCaseTriggerMethods.UpdateTMBfrInsert(Trigger.new);  //Q3 2016 -US2489 
            PatientCaseTriggerMethods.checkBusinessCloseTime(Trigger.new);
            
            PatientCaseTriggerMethods.calculateStartDateOwnerTimeZone(Trigger.new);
        }
        
        //before update
        if ((Trigger.isBefore) && (Trigger.isUpdate)){  
            list<Shared_Patient_Case__c> PCLst = (list<Shared_Patient_Case__c>)trigger.new;
            list<Shared_Patient_Case__c> updatePCLst = new list<Shared_Patient_Case__c>();
           
            for(Shared_Patient_Case__c pc : PCLst){
                Id oldAccountId = ((Shared_Patient_Case__c)Trigger.oldMap.get(pc.Id)).Shared_Facility__c;
                if(pc.Shared_Facility__c != oldAccountId){
                  updatePCLst.add(pc);              
                }
            }  
            
            if(updatePCLst!= null && updatePCLst.size() > 0){
                PatientCaseTriggerMethods.UpdateTMBfrInsert(Trigger.new);
            }
            PatientCaseTriggerMethods.checkProcedureUpdateAvailability(Trigger.new);
            PatientCaseTriggerMethods.checkBusinessCloseTime(Trigger.new);
            PatientCaseTriggerMethods.calculateStartDateOwnerTimeZone(Trigger.new);
        }
        
        
        //before delete
        if ((Trigger.isBefore) && (Trigger.isDelete)){  
                 PatientCaseTriggerMethods.DeleteEvents(Trigger.old); // User Stories : DRAFT US-2452 and DRAFT US-2453
        }
        
        
        //after insert
        if ((Trigger.isAfter) && (Trigger.isInsert)){   
            PatientCaseTriggerMethods.CreateEventForEPMember(Trigger.new,Trigger.newMap,Trigger.old,Trigger.oldMap);     
            PatientCaseTriggerMethods.CreateEventForWatchmanMember(Trigger.new); // User Stories : DRAFT US-2452 and DRAFT US-2453
            PatientCaseTriggerMethods.CreatePatientCaseClones(Trigger.new); //US2240
        }
        
        
        //after update
          if ((Trigger.isAfter) && (Trigger.isUpdate)){
          PatientCaseTriggerMethods.CreateEventForEPMember(Trigger.new,Trigger.newMap,Trigger.old,Trigger.oldMap);
          PatientCaseTriggerMethods.UpdateEventForWatchmanMember(Trigger.newMap,Trigger.oldMap); // User Stories : DRAFT US-2452 and DRAFT US-2453
     
       }
            
        
        
        //after delete
        if ((Trigger.isAfter) && (Trigger.isDelete)){              
        }
        
        
        //after undelete
        if ((Trigger.isAfter) && (Trigger.isUndelete)){       
            PatientCaseTriggerMethods.CreateEventForWatchmanMember(Trigger.new); // User Stories : DRAFT US-2452 and DRAFT US-2453      
            PatientCaseTriggerMethods.checkBusinessCloseTime(Trigger.new);
        }
  }   
}
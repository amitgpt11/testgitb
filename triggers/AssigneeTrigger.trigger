/**
* Trigger for the Assignee custom object
*
* @Author salesforce Services
* @Date 04/07/2015
*/
trigger AssigneeTrigger on Assignee__c (after insert, after update, after delete) {

	TriggerHandlerManager.createAndExecuteHandler(AssigneeTriggerHandler.class);

}
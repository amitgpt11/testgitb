/**
* Trigger for the MC_Email object
* Owner ID of MC_Email will be replaced with opportunity Owner ID
* @Author Naveen
* @Date 10/28/2015
*/

Trigger MCEmailTrigger on MC_Email__c (before insert, before update){ 

   public static final String TRIGGER_NAME = 'MCEmailTrigger';
   if (TriggerHandlerManager.isOkayToRun(TRIGGER_NAME)){ 
    
    set<id> opptyIds = new Set<Id>();
    set<id> pIds = new Set<Id>();
   //Get the Opportunity IDs    
   for(MC_Email__c obj : trigger.new){  
       if(obj.Related_Opportunity__c != null)
       opptyIds.add(obj.Related_Opportunity__c);
       else
       pIds.add(obj.PatientID__c);
   }
   
   // Get the Owner Id of the Opportunity ID's
   List<Opportunity> opptys = [Select id, ownerid from Opportunity where Id IN :opptyIds];
  
   Map<id, id> opptyOwnerMap = new Map<id, id>();
   
   for(Opportunity o : opptys)
   {
       if( o.ownerId != null )   
       opptyOwnerMap.put(o.Id, o.ownerId);
   } 
   
  // Get the Owner Id of the Patient ID's
   List<Patient__c> patients = [Select id, ownerid from Patient__c where Id IN :pIds];
   
   Map<id,Id> patientOwnerMap = new Map<id,Id>();
   
    for(Patient__c p : patients )
   {
       if( p.ownerId != null )   
       patientOwnerMap.put(p.Id, p.ownerId);
   } 
   
   // For each Owner of MC_Email owner updating with Opportunity Owner
   for(MC_Email__c obj : trigger.new){
       system.debug('obj.Related_Opportunity__c'+ obj.Related_Opportunity__c);
       system.debug('obj.OwnerId'+obj.OwnerId);
       if(obj.Related_Opportunity__c != null && null != opptyOwnerMap.get(obj.Related_Opportunity__c)){ 
        obj.OwnerId = opptyOwnerMap.get(obj.Related_Opportunity__c);
       }
       else{
        if(obj.PatientID__c !=null && null != patientOwnerMap.get(obj.PatientID__c))
           obj.OwnerId  = patientOwnerMap.get(obj.PatientID__c);
        }
   }
 }  
}
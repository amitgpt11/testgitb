/*
 @CreatedDate     20 Sept 2016                                  
 @author          Ashish-Accenture
 @Description     Trigger to check Duplicate records using Duplicate Check Application
 
 */
trigger NonWorkingDayCalendarTrigger on Uro_PH_Non_Working_Day_Calendar__c (after delete, after insert, after undelete, after update, before insert, before update) {
    dupcheck.dc3Trigger triggerTool = new dupcheck.dc3Trigger(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete);
    String errorString = triggerTool.processTrigger(trigger.oldMap, trigger.new); 

    if (String.isNotEmpty(errorString)) { 
        trigger.new[0].addError(errorString,false); 
    }
    Type t = NonWorkingDayCalendarTriggerMethods.class;
    String handlerName = t.getName();
   if (TriggerHandlerManager.isOkayToRun(handlerName)){ 
        if ((Trigger.isAfter) && (Trigger.isInsert)){ 
            NonWorkingDayCalendarTriggerMethods.CreateWeekendChildRec(Trigger.new);
        }
        if ((Trigger.isAfter) && (Trigger.isUpdate)){ 
            NonWorkingDayCalendarTriggerMethods.CreateWeekendChildRecOnUpdate(Trigger.newMap,Trigger.oldMap);
        }
    }
}
/**
* Trigger for the Opportunity object
*
* @Author salesforce Services
* @Date 02/10/2015
*/
trigger OpportunityTrigger on Opportunity (before insert, before update, after insert, after update, before delete) {
    TriggerHandlerManager.createAndExecuteHandler(OpportunityTriggerHandler.class);
}
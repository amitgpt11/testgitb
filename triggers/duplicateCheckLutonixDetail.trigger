/*
 @CreatedDate     22 June 2016                                  
 @author          Ashish-Accenture
 @Description     Trigger to check Duplicate records using Duplicate Check Application
 
 */

trigger duplicateCheckLutonixDetail on PI_Lutonix_SRAI_Detail__c (after delete, after insert, after undelete, after update, before insert, before update) {
   
    public static final String Name = 'duplicateCheckLutonixDetail';
   
  if (TriggerHandlerManager.isOkayToRun(Name)){  
   dupcheck.dc3Trigger triggerTool = new dupcheck.dc3Trigger(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete);
    String errorString = triggerTool.processTrigger(trigger.oldMap, trigger.new); 
   
    if (String.isNotEmpty(errorString)) { 
        trigger.new[0].addError(errorString,false); 
    }
 }  
}
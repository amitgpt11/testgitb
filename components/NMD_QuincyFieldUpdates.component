<!--                                                                                                              
	NMD_QuincyFieldUpdates.component
	
	@author Salesforce services
	@date	2015-02-11
	@desc 	Presents a UI of fields to submit an update to Quincy.
-->
<apex:component selfClosing="true">
	<apex:attribute name="record" type="SObject" required="true" description="The record returned by the Standard Controller"/>
	<apex:attribute name="fieldset" type="Schema.FieldSetMember[]" required="true" description="FieldSet of field to update."/>
	<apex:attribute name="filename" type="String" required="false" default="" description="Name of attached file."/>
	<apex:attribute name="filebody" type="Blob" required="false" default="" description="Name of attached file."/>
	<apex:attribute name="editmode" type="Boolean" required="true" description="True|False"/>
	<apex:attribute name="filerequired" type="Boolean" required="false" default="true" description="Is proof required"/> 
	<apex:pageBlockSection title="{!$Label.NMD_Enter_Field_Updates}" columns="1" collapsible="false"> 
		<apex:repeat var="f" value="{!fieldset}">
			<apex:inputField value="{!record[f]}" required="false" rendered="{!editmode}"/>
			<apex:outputField value="{!record[f]}" rendered="{!NOT(editmode)}"/>
		</apex:repeat>
	</apex:pageBlockSection>
	<apex:pageBlockSection title="{!$Label.NMD_Attach_Proof}" columns="1" collapsible="false" rendered="{!filerequired}">
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="{!$Label.NMD_Must_Provide_Proof}"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="{!$Label.NMD_Attachment_5MB}"/>
			<apex:outputPanel >
				<apex:inputFile value="{!filebody}" filename="{!filename}" rendered="{!editmode}"/>
				<apex:outputText value="{!filename}" rendered="{!NOT(editmode)}"/>
			</apex:outputPanel>
		</apex:pageBlockSectionItem>
	</apex:pageBlockSection>
</apex:component>